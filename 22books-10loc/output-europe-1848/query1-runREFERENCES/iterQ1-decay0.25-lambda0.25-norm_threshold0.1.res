###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 500.621, activation diff: 498.222, ratio: 0.995
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 2127.506, activation diff: 1629.374, ratio: 0.766
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 4519.430, activation diff: 2391.937, ratio: 0.529
#   pulse 4: activated nodes: 11243, borderline nodes: 869, overall activation: 5619.376, activation diff: 1099.945, ratio: 0.196
#   pulse 5: activated nodes: 11317, borderline nodes: 194, overall activation: 6023.049, activation diff: 403.673, ratio: 0.067
#   pulse 6: activated nodes: 11330, borderline nodes: 65, overall activation: 6161.963, activation diff: 138.914, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 6162.0
#   number of spread. activ. pulses: 6
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99979776   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99956954   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   5   0.9995624   REFERENCES
1   Q1   politicalsketche00retsrich_96   6   0.9995521   REFERENCES
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES
1   Q1   essentialsinmod01howegoog_463   9   0.9995104   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   10   0.999509   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   11   0.9995078   REFERENCES
1   Q1   essentialsinmod01howegoog_255   12   0.9995011   REFERENCES
1   Q1   essentialsinmod01howegoog_320   13   0.99948364   REFERENCES
1   Q1   essentialsinmod01howegoog_12   14   0.9994835   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.9994828   REFERENCES
1   Q1   europesincenapol00leveuoft_318   16   0.99940646   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.999385   REFERENCES
1   Q1   encyclopediaame28unkngoog_276   18   0.99937004   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   19   0.9993431   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   20   0.9993302   REFERENCES
