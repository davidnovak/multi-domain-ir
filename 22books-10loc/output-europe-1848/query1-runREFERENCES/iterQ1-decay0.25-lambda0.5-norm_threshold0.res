###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 366.742, activation diff: 362.212, ratio: 0.988
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1399.221, activation diff: 1032.479, ratio: 0.738
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 3135.458, activation diff: 1736.238, ratio: 0.554
#   pulse 4: activated nodes: 11294, borderline nodes: 546, overall activation: 4426.698, activation diff: 1291.239, ratio: 0.292
#   pulse 5: activated nodes: 11333, borderline nodes: 39, overall activation: 5235.067, activation diff: 808.370, ratio: 0.154
#   pulse 6: activated nodes: 11333, borderline nodes: 39, overall activation: 5712.181, activation diff: 477.113, ratio: 0.084
#   pulse 7: activated nodes: 11333, borderline nodes: 39, overall activation: 5986.318, activation diff: 274.138, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 5986.3
#   number of spread. activ. pulses: 7
#   running time: 536

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99795824   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.9957944   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9956229   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9952061   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9921186   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.9903061   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.99025023   REFERENCES
1   Q1   politicalsketche00retsrich_96   8   0.9901062   REFERENCES
1   Q1   essentialsinmod01howegoog_14   9   0.99003226   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   10   0.9898629   REFERENCES
1   Q1   politicalsketche00retsrich_159   11   0.9898261   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   12   0.98975027   REFERENCES
1   Q1   essentialsinmod01howegoog_463   13   0.9897374   REFERENCES
1   Q1   essentialsinmod01howegoog_12   14   0.98972774   REFERENCES
1   Q1   essentialsinmod01howegoog_255   15   0.98970133   REFERENCES
1   Q1   politicalsketche00retsrich_148   16   0.9895934   REFERENCES
1   Q1   essentialsinmod01howegoog_320   17   0.98957396   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9894955   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   19   0.98932964   REFERENCES
1   Q1   europesincenapol00leveuoft_318   20   0.98917174   REFERENCES
