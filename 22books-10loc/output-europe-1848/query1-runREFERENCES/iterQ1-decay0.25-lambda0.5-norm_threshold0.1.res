###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 336.414, activation diff: 332.148, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1239.980, activation diff: 903.566, ratio: 0.729
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 2927.409, activation diff: 1687.428, ratio: 0.576
#   pulse 4: activated nodes: 11222, borderline nodes: 1388, overall activation: 4231.717, activation diff: 1304.309, ratio: 0.308
#   pulse 5: activated nodes: 11308, borderline nodes: 359, overall activation: 5065.654, activation diff: 833.936, ratio: 0.165
#   pulse 6: activated nodes: 11318, borderline nodes: 177, overall activation: 5564.482, activation diff: 498.828, ratio: 0.090
#   pulse 7: activated nodes: 11329, borderline nodes: 81, overall activation: 5854.091, activation diff: 289.609, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 5854.1
#   number of spread. activ. pulses: 7
#   running time: 569

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99771297   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.99547756   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99524486   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947381   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99132025   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.99012595   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.99005055   REFERENCES
1   Q1   politicalsketche00retsrich_96   8   0.9898715   REFERENCES
1   Q1   essentialsinmod01howegoog_14   9   0.989769   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   10   0.9895705   REFERENCES
1   Q1   politicalsketche00retsrich_159   11   0.9895407   REFERENCES
1   Q1   essentialsinmod01howegoog_12   12   0.98945045   REFERENCES
1   Q1   essentialsinmod01howegoog_463   13   0.9894266   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   14   0.9894177   REFERENCES
1   Q1   essentialsinmod01howegoog_255   15   0.9893826   REFERENCES
1   Q1   politicalsketche00retsrich_148   16   0.9892789   REFERENCES
1   Q1   essentialsinmod01howegoog_320   17   0.98920894   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9891851   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   19   0.98897266   REFERENCES
1   Q1   europesincenapol00leveuoft_318   20   0.98877025   REFERENCES
