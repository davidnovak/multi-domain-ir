###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 546.112, activation diff: 543.318, ratio: 0.995
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 2435.095, activation diff: 1890.602, ratio: 0.776
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 5068.306, activation diff: 2633.211, ratio: 0.520
#   pulse 4: activated nodes: 11294, borderline nodes: 546, overall activation: 6257.440, activation diff: 1189.134, ratio: 0.190
#   pulse 5: activated nodes: 11333, borderline nodes: 39, overall activation: 6691.089, activation diff: 433.649, ratio: 0.065
#   pulse 6: activated nodes: 11341, borderline nodes: 8, overall activation: 6839.739, activation diff: 148.650, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6839.7
#   number of spread. activ. pulses: 6
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99981797   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99960124   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.999581   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   5   0.99957937   REFERENCES
1   Q1   politicalsketche00retsrich_96   6   0.9995719   REFERENCES
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954915   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   10   0.9995407   REFERENCES
1   Q1   essentialsinmod01howegoog_463   11   0.99953973   REFERENCES
1   Q1   essentialsinmod01howegoog_255   12   0.9995331   REFERENCES
1   Q1   essentialsinmod01howegoog_320   13   0.999521   REFERENCES
1   Q1   essentialsinmod01howegoog_12   14   0.9995178   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.999514   REFERENCES
1   Q1   europesincenapol00leveuoft_318   16   0.9994586   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.9994303   REFERENCES
1   Q1   encyclopediaame28unkngoog_276   18   0.9994222   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   19   0.9994   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993822   REFERENCES
