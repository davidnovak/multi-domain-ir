###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 546.112, activation diff: 543.318, ratio: 0.995
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 2346.679, activation diff: 1802.186, ratio: 0.768
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 4729.356, activation diff: 2382.677, ratio: 0.504
#   pulse 4: activated nodes: 11294, borderline nodes: 546, overall activation: 5775.390, activation diff: 1046.034, ratio: 0.181
#   pulse 5: activated nodes: 11333, borderline nodes: 39, overall activation: 6149.620, activation diff: 374.230, ratio: 0.061
#   pulse 6: activated nodes: 11333, borderline nodes: 39, overall activation: 6275.726, activation diff: 126.107, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 6275.7
#   number of spread. activ. pulses: 6
#   running time: 550

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99981797   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99960124   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.999581   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   5   0.99957937   REFERENCES
1   Q1   politicalsketche00retsrich_96   6   0.9995718   REFERENCES
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954915   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   10   0.9995407   REFERENCES
1   Q1   essentialsinmod01howegoog_463   11   0.9995395   REFERENCES
1   Q1   essentialsinmod01howegoog_255   12   0.9995316   REFERENCES
1   Q1   essentialsinmod01howegoog_320   13   0.9995207   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.9995118   REFERENCES
1   Q1   essentialsinmod01howegoog_12   15   0.9995113   REFERENCES
1   Q1   europesincenapol00leveuoft_318   16   0.9994529   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.99942434   REFERENCES
1   Q1   encyclopediaame28unkngoog_276   18   0.99942195   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   19   0.99938524   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993673   REFERENCES
