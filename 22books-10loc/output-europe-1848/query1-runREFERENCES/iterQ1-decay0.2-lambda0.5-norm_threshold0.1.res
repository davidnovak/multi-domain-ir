###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 336.414, activation diff: 332.148, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1276.510, activation diff: 940.096, ratio: 0.736
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 3107.466, activation diff: 1830.957, ratio: 0.589
#   pulse 4: activated nodes: 11230, borderline nodes: 1259, overall activation: 4546.249, activation diff: 1438.782, ratio: 0.316
#   pulse 5: activated nodes: 11309, borderline nodes: 293, overall activation: 5475.141, activation diff: 928.892, ratio: 0.170
#   pulse 6: activated nodes: 11322, borderline nodes: 153, overall activation: 6035.055, activation diff: 559.914, ratio: 0.093
#   pulse 7: activated nodes: 11330, borderline nodes: 61, overall activation: 6362.255, activation diff: 327.200, ratio: 0.051
#   pulse 8: activated nodes: 11333, borderline nodes: 39, overall activation: 6550.380, activation diff: 188.125, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 6550.4
#   number of spread. activ. pulses: 8
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99885607   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9976219   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99734807   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.99506295   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   6   0.9950253   REFERENCES
1   Q1   politicalsketche00retsrich_96   7   0.99493587   REFERENCES
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES
1   Q1   politicalsketche00retsrich_159   10   0.9947475   REFERENCES
1   Q1   essentialsinmod01howegoog_12   11   0.9947152   REFERENCES
1   Q1   essentialsinmod01howegoog_463   12   0.994714   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   13   0.9947089   REFERENCES
1   Q1   essentialsinmod01howegoog_255   14   0.9946908   REFERENCES
1   Q1   essentialsinmod01howegoog_320   15   0.9946053   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   16   0.99459   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.9945774   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945232   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   19   0.9944172   REFERENCES
1   Q1   europesincenapol00leveuoft_318   20   0.9943837   REFERENCES
