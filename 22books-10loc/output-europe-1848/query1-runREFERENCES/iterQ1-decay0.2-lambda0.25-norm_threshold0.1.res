###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 500.621, activation diff: 498.222, ratio: 0.995
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 2204.213, activation diff: 1706.081, ratio: 0.774
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 4836.659, activation diff: 2632.460, ratio: 0.544
#   pulse 4: activated nodes: 11243, borderline nodes: 869, overall activation: 6081.865, activation diff: 1245.205, ratio: 0.205
#   pulse 5: activated nodes: 11323, borderline nodes: 122, overall activation: 6547.588, activation diff: 465.723, ratio: 0.071
#   pulse 6: activated nodes: 11332, borderline nodes: 52, overall activation: 6710.597, activation diff: 163.009, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6710.6
#   number of spread. activ. pulses: 6
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99979776   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99956954   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   5   0.9995624   REFERENCES
1   Q1   politicalsketche00retsrich_96   6   0.99955213   REFERENCES
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES
1   Q1   essentialsinmod01howegoog_463   9   0.99951065   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   10   0.999509   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   11   0.9995079   REFERENCES
1   Q1   essentialsinmod01howegoog_255   12   0.99950266   REFERENCES
1   Q1   essentialsinmod01howegoog_12   13   0.99949074   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.99948525   REFERENCES
1   Q1   essentialsinmod01howegoog_320   15   0.99948394   REFERENCES
1   Q1   europesincenapol00leveuoft_318   16   0.9994129   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.9993916   REFERENCES
1   Q1   encyclopediaame28unkngoog_276   18   0.9993703   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   19   0.99935955   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993356   REFERENCES
