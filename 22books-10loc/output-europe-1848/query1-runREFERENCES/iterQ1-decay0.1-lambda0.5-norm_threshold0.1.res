###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 336.414, activation diff: 332.148, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1349.569, activation diff: 1013.155, ratio: 0.751
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 3481.732, activation diff: 2132.163, ratio: 0.612
#   pulse 4: activated nodes: 11232, borderline nodes: 1214, overall activation: 5206.907, activation diff: 1725.175, ratio: 0.331
#   pulse 5: activated nodes: 11312, borderline nodes: 230, overall activation: 6338.871, activation diff: 1131.964, ratio: 0.179
#   pulse 6: activated nodes: 11327, borderline nodes: 77, overall activation: 7029.615, activation diff: 690.743, ratio: 0.098
#   pulse 7: activated nodes: 11334, borderline nodes: 33, overall activation: 7437.560, activation diff: 407.945, ratio: 0.055
#   pulse 8: activated nodes: 11334, borderline nodes: 20, overall activation: 7674.561, activation diff: 237.001, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7674.6
#   number of spread. activ. pulses: 8
#   running time: 485

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99885607   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9976219   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973483   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.99506295   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   6   0.9950253   REFERENCES
1   Q1   politicalsketche00retsrich_96   7   0.99493605   REFERENCES
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES
1   Q1   politicalsketche00retsrich_159   10   0.9947539   REFERENCES
1   Q1   essentialsinmod01howegoog_12   11   0.99473184   REFERENCES
1   Q1   essentialsinmod01howegoog_463   12   0.99471545   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   13   0.99470896   REFERENCES
1   Q1   essentialsinmod01howegoog_255   14   0.9946954   REFERENCES
1   Q1   essentialsinmod01howegoog_320   15   0.9946071   REFERENCES
1   Q1   politicalsketche00retsrich_148   16   0.99459326   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   17   0.9945903   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   18   0.99455976   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   19   0.99445474   REFERENCES
1   Q1   europesincenapol00leveuoft_318   20   0.99439967   REFERENCES
