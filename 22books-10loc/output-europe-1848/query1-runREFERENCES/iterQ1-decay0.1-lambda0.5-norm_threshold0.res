###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1534, borderline nodes: 1526, overall activation: 366.742, activation diff: 362.212, ratio: 0.988
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1532.706, activation diff: 1165.965, ratio: 0.761
#   pulse 3: activated nodes: 10748, borderline nodes: 3747, overall activation: 3744.780, activation diff: 2212.074, ratio: 0.591
#   pulse 4: activated nodes: 11294, borderline nodes: 546, overall activation: 5464.818, activation diff: 1720.037, ratio: 0.315
#   pulse 5: activated nodes: 11333, borderline nodes: 39, overall activation: 6568.903, activation diff: 1104.085, ratio: 0.168
#   pulse 6: activated nodes: 11341, borderline nodes: 8, overall activation: 7232.722, activation diff: 663.820, ratio: 0.092
#   pulse 7: activated nodes: 11341, borderline nodes: 0, overall activation: 7620.397, activation diff: 387.674, ratio: 0.051
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 7843.545, activation diff: 223.148, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7843.5
#   number of spread. activ. pulses: 8
#   running time: 480

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99897873   REFERENCES
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99781096   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99758434   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.99515307   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   6   0.9951251   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   7   0.9950926   REFERENCES
1   Q1   politicalsketche00retsrich_96   8   0.9950534   REFERENCES
1   Q1   essentialsinmod01howegoog_14   9   0.99501616   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES
1   Q1   politicalsketche00retsrich_159   11   0.9948981   REFERENCES
1   Q1   englishcyclopae05kniggoog_212   12   0.9948752   REFERENCES
1   Q1   essentialsinmod01howegoog_463   13   0.9948704   REFERENCES
1   Q1   essentialsinmod01howegoog_12   14   0.9948695   REFERENCES
1   Q1   essentialsinmod01howegoog_255   15   0.9948543   REFERENCES
1   Q1   essentialsinmod01howegoog_320   16   0.994789   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.9947548   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9947177   REFERENCES
1   Q1   ouroldworldbackg00bearrich_395   19   0.99463576   REFERENCES
1   Q1   europesincenapol00leveuoft_318   20   0.9945985   REFERENCES
