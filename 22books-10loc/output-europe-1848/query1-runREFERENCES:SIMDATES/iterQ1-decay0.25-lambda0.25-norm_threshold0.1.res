###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 500.866, activation diff: 498.468, ratio: 0.995
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 2132.549, activation diff: 1634.172, ratio: 0.766
#   pulse 3: activated nodes: 10781, borderline nodes: 3780, overall activation: 4530.246, activation diff: 2397.705, ratio: 0.529
#   pulse 4: activated nodes: 11249, borderline nodes: 867, overall activation: 5636.736, activation diff: 1106.490, ratio: 0.196
#   pulse 5: activated nodes: 11323, borderline nodes: 199, overall activation: 6047.085, activation diff: 410.348, ratio: 0.068
#   pulse 6: activated nodes: 11337, borderline nodes: 64, overall activation: 6190.258, activation diff: 143.173, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 6190.3
#   number of spread. activ. pulses: 6
#   running time: 463

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9997992   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99957013   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   5   0.9995625   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   6   0.9995521   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   9   0.9995145   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   10   0.9995104   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   11   0.9995078   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   12   0.9995011   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   13   0.9994916   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.99948597   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   15   0.9994835   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   16   0.99940646   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.999385   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_276   18   0.99937004   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   19   0.9993431   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   20   0.9993302   REFERENCES:SIMDATES
