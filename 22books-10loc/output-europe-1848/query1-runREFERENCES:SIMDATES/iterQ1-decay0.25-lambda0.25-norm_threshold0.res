###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 546.395, activation diff: 543.601, ratio: 0.995
#   pulse 2: activated nodes: 7059, borderline nodes: 5518, overall activation: 2351.956, activation diff: 1807.160, ratio: 0.768
#   pulse 3: activated nodes: 10790, borderline nodes: 3731, overall activation: 4748.547, activation diff: 2396.591, ratio: 0.505
#   pulse 4: activated nodes: 11313, borderline nodes: 523, overall activation: 5805.022, activation diff: 1056.475, ratio: 0.182
#   pulse 5: activated nodes: 11342, borderline nodes: 29, overall activation: 6186.869, activation diff: 381.847, ratio: 0.062
#   pulse 6: activated nodes: 11342, borderline nodes: 29, overall activation: 6317.239, activation diff: 130.370, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6317.2
#   number of spread. activ. pulses: 6
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9998188   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9996016   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.99958104   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   5   0.9995794   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   6   0.9995718   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954915   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   10   0.9995445   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   11   0.9995395   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   12   0.9995316   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   13   0.9995263   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.99951404   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   15   0.9995113   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   16   0.9994529   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.99942434   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_276   18   0.99942195   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   19   0.99938524   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993673   REFERENCES:SIMDATES
