###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 546.395, activation diff: 543.601, ratio: 0.995
#   pulse 2: activated nodes: 7059, borderline nodes: 5518, overall activation: 2440.315, activation diff: 1895.520, ratio: 0.777
#   pulse 3: activated nodes: 10790, borderline nodes: 3731, overall activation: 5086.258, activation diff: 2645.943, ratio: 0.520
#   pulse 4: activated nodes: 11313, borderline nodes: 523, overall activation: 6286.486, activation diff: 1200.229, ratio: 0.191
#   pulse 5: activated nodes: 11342, borderline nodes: 29, overall activation: 6729.039, activation diff: 442.553, ratio: 0.066
#   pulse 6: activated nodes: 11348, borderline nodes: 6, overall activation: 6882.799, activation diff: 153.760, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6882.8
#   number of spread. activ. pulses: 6
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9998188   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9996016   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.99958104   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   5   0.9995794   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   6   0.9995719   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954915   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   10   0.9995445   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   11   0.99953973   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   12   0.9995331   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   13   0.9995266   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   14   0.9995178   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.99951625   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   16   0.9994586   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.9994303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_276   18   0.9994222   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   19   0.9994   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993822   REFERENCES:SIMDATES
