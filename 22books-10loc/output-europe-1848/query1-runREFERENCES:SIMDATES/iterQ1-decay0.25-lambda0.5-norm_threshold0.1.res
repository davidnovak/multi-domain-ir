###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 336.577, activation diff: 332.312, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1242.928, activation diff: 906.350, ratio: 0.729
#   pulse 3: activated nodes: 10781, borderline nodes: 3780, overall activation: 2934.569, activation diff: 1691.642, ratio: 0.576
#   pulse 4: activated nodes: 11228, borderline nodes: 1387, overall activation: 4242.872, activation diff: 1308.303, ratio: 0.308
#   pulse 5: activated nodes: 11314, borderline nodes: 362, overall activation: 5081.448, activation diff: 838.577, ratio: 0.165
#   pulse 6: activated nodes: 11324, borderline nodes: 183, overall activation: 5584.620, activation diff: 503.172, ratio: 0.090
#   pulse 7: activated nodes: 11336, borderline nodes: 82, overall activation: 5877.808, activation diff: 293.188, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 5877.8
#   number of spread. activ. pulses: 7
#   running time: 688

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99773526   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.99547756   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99526036   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947381   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99132025   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.9901284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.9900533   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.9898715   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   9   0.989769   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   10   0.9895756   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   11   0.9895705   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   12   0.989466   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   13   0.98945045   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   14   0.9894266   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   15   0.9893826   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   16   0.9892789   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   17   0.9892702   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9891851   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.98897266   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.98877025   REFERENCES:SIMDATES
