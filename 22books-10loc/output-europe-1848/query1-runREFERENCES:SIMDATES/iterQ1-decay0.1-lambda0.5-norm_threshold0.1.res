###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 336.577, activation diff: 332.312, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1352.318, activation diff: 1015.740, ratio: 0.751
#   pulse 3: activated nodes: 10781, borderline nodes: 3780, overall activation: 3488.213, activation diff: 2135.896, ratio: 0.612
#   pulse 4: activated nodes: 11240, borderline nodes: 1215, overall activation: 5218.628, activation diff: 1730.414, ratio: 0.332
#   pulse 5: activated nodes: 11318, borderline nodes: 236, overall activation: 6358.116, activation diff: 1139.489, ratio: 0.179
#   pulse 6: activated nodes: 11335, borderline nodes: 81, overall activation: 7056.232, activation diff: 698.115, ratio: 0.099
#   pulse 7: activated nodes: 11340, borderline nodes: 37, overall activation: 7470.248, activation diff: 414.016, ratio: 0.055
#   pulse 8: activated nodes: 11341, borderline nodes: 23, overall activation: 7711.804, activation diff: 241.556, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7711.8
#   number of spread. activ. pulses: 8
#   running time: 480

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9988673   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99762964   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973483   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.9950642   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   6   0.9950267   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   7   0.99493605   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   10   0.99477124   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   11   0.9947331   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   12   0.99473184   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   13   0.99471545   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   14   0.9946954   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   15   0.9946376   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   16   0.99459326   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   17   0.9945903   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.99455976   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.99445474   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.99439967   REFERENCES:SIMDATES
