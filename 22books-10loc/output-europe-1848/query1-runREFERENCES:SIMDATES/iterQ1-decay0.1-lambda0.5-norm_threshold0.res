###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 366.930, activation diff: 362.401, ratio: 0.988
#   pulse 2: activated nodes: 7059, borderline nodes: 5518, overall activation: 1535.641, activation diff: 1168.710, ratio: 0.761
#   pulse 3: activated nodes: 10790, borderline nodes: 3731, overall activation: 3754.054, activation diff: 2218.413, ratio: 0.591
#   pulse 4: activated nodes: 11313, borderline nodes: 523, overall activation: 5481.551, activation diff: 1727.497, ratio: 0.315
#   pulse 5: activated nodes: 11342, borderline nodes: 29, overall activation: 6594.192, activation diff: 1112.641, ratio: 0.169
#   pulse 6: activated nodes: 11348, borderline nodes: 6, overall activation: 7265.539, activation diff: 671.347, ratio: 0.092
#   pulse 7: activated nodes: 11348, borderline nodes: 0, overall activation: 7659.019, activation diff: 393.480, ratio: 0.051
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 7886.332, activation diff: 227.313, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7886.3
#   number of spread. activ. pulses: 8
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99898624   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9978164   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99758434   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.995154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   6   0.99512607   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   7   0.9950926   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.9950534   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   9   0.99501616   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   11   0.9949113   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   12   0.9948932   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   13   0.9948704   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   14   0.9948695   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   15   0.9948543   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   16   0.99481225   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.9947548   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9947177   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.99463576   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.9945985   REFERENCES:SIMDATES
