###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 366.930, activation diff: 362.401, ratio: 0.988
#   pulse 2: activated nodes: 7059, borderline nodes: 5518, overall activation: 1402.332, activation diff: 1035.402, ratio: 0.738
#   pulse 3: activated nodes: 10790, borderline nodes: 3731, overall activation: 3147.363, activation diff: 1745.031, ratio: 0.554
#   pulse 4: activated nodes: 11313, borderline nodes: 523, overall activation: 4446.545, activation diff: 1299.181, ratio: 0.292
#   pulse 5: activated nodes: 11342, borderline nodes: 29, overall activation: 5261.924, activation diff: 815.379, ratio: 0.155
#   pulse 6: activated nodes: 11342, borderline nodes: 29, overall activation: 5744.607, activation diff: 482.683, ratio: 0.084
#   pulse 7: activated nodes: 11342, borderline nodes: 29, overall activation: 6022.875, activation diff: 278.268, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6022.9
#   number of spread. activ. pulses: 7
#   running time: 565

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99797314   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.9957944   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9956337   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9952061   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9921186   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.99030805   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.99025214   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.9901062   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   9   0.99003226   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   10   0.9898629   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   11   0.9898528   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   12   0.98978627   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   13   0.9897374   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   14   0.98972774   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   15   0.98970133   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   16   0.98962045   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.9895934   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9894955   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.98932964   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.98917174   REFERENCES:SIMDATES
