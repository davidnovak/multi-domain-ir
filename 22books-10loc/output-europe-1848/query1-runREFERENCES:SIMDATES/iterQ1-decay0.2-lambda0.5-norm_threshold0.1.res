###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 336.577, activation diff: 332.312, ratio: 0.987
#   pulse 2: activated nodes: 7001, borderline nodes: 5467, overall activation: 1279.391, activation diff: 942.813, ratio: 0.737
#   pulse 3: activated nodes: 10781, borderline nodes: 3780, overall activation: 3114.335, activation diff: 1834.944, ratio: 0.589
#   pulse 4: activated nodes: 11236, borderline nodes: 1260, overall activation: 4557.430, activation diff: 1443.095, ratio: 0.317
#   pulse 5: activated nodes: 11315, borderline nodes: 296, overall activation: 5491.788, activation diff: 934.358, ratio: 0.170
#   pulse 6: activated nodes: 11328, borderline nodes: 155, overall activation: 6056.969, activation diff: 565.180, ratio: 0.093
#   pulse 7: activated nodes: 11338, borderline nodes: 61, overall activation: 6388.558, activation diff: 331.589, ratio: 0.052
#   pulse 8: activated nodes: 11339, borderline nodes: 42, overall activation: 6580.041, activation diff: 191.483, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 6580.0
#   number of spread. activ. pulses: 8
#   running time: 501

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9988673   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99762964   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99734807   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.9950642   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   6   0.9950267   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   7   0.99493587   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   10   0.9947649   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   11   0.9947331   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   12   0.9947152   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   13   0.994714   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   14   0.9946908   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   15   0.99463594   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   16   0.99459   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.9945774   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945232   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.9944172   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.9943837   REFERENCES:SIMDATES
