###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1541, borderline nodes: 1533, overall activation: 366.930, activation diff: 362.401, ratio: 0.988
#   pulse 2: activated nodes: 7059, borderline nodes: 5518, overall activation: 1446.769, activation diff: 1079.838, ratio: 0.746
#   pulse 3: activated nodes: 10790, borderline nodes: 3731, overall activation: 3344.894, activation diff: 1898.125, ratio: 0.567
#   pulse 4: activated nodes: 11313, borderline nodes: 523, overall activation: 4781.275, activation diff: 1436.381, ratio: 0.300
#   pulse 5: activated nodes: 11342, borderline nodes: 29, overall activation: 5691.583, activation diff: 910.308, ratio: 0.160
#   pulse 6: activated nodes: 11348, borderline nodes: 6, overall activation: 6234.461, activation diff: 542.878, ratio: 0.087
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 6549.439, activation diff: 314.978, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6549.4
#   number of spread. activ. pulses: 7
#   running time: 470

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99797314   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   0.9957944   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9956337   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99520624   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9921187   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.99030805   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.99025214   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.99010646   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   9   0.99003226   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   10   0.9898629   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   11   0.9898565   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_212   12   0.9897864   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   13   0.9897401   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_463   14   0.9897388   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_255   15   0.98970544   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_320   16   0.98962224   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.98960245   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.98951775   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_395   19   0.98935264   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_318   20   0.9891859   REFERENCES:SIMDATES
