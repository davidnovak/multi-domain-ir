###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 500.803, activation diff: 498.404, ratio: 0.995
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 2143.052, activation diff: 1644.738, ratio: 0.767
#   pulse 3: activated nodes: 11255, borderline nodes: 4075, overall activation: 4748.555, activation diff: 2605.509, ratio: 0.549
#   pulse 4: activated nodes: 11429, borderline nodes: 406, overall activation: 6092.257, activation diff: 1343.702, ratio: 0.221
#   pulse 5: activated nodes: 11449, borderline nodes: 112, overall activation: 6627.758, activation diff: 535.500, ratio: 0.081
#   pulse 6: activated nodes: 11452, borderline nodes: 34, overall activation: 6818.069, activation diff: 190.311, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6818.1
#   number of spread. activ. pulses: 6
#   running time: 593

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99979776   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9995699   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995624   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.99955225   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   9   0.99951065   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   10   0.99950933   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.99950886   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.9995011   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   13   0.99948364   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.9994835   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   15   0.9994828   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.99940646   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9993856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   18   0.99937004   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   19   0.9993431   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.9993302   REFERENCES:SIMLOC
