###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 336.535, activation diff: 332.270, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1246.484, activation diff: 909.949, ratio: 0.730
#   pulse 3: activated nodes: 11253, borderline nodes: 4112, overall activation: 3019.265, activation diff: 1772.781, ratio: 0.587
#   pulse 4: activated nodes: 11411, borderline nodes: 741, overall activation: 4487.565, activation diff: 1468.300, ratio: 0.327
#   pulse 5: activated nodes: 11444, borderline nodes: 194, overall activation: 5484.520, activation diff: 996.955, ratio: 0.182
#   pulse 6: activated nodes: 11449, borderline nodes: 68, overall activation: 6095.693, activation diff: 611.173, ratio: 0.100
#   pulse 7: activated nodes: 11452, borderline nodes: 41, overall activation: 6452.705, activation diff: 357.013, ratio: 0.055
#   pulse 8: activated nodes: 11453, borderline nodes: 28, overall activation: 6656.605, activation diff: 203.900, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6656.6
#   number of spread. activ. pulses: 8
#   running time: 590

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99885607   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9976232   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973532   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.99506295   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9950253   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.99493694   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.99474406   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   11   0.9947133   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.99471045   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.9947046   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946876   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.99460405   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   16   0.99459   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9945718   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945023   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99439573   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.99437237   REFERENCES:SIMLOC
