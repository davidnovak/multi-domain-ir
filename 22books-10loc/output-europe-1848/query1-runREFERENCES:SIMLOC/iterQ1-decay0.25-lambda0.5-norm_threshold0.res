###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 367.057, activation diff: 362.527, ratio: 0.988
#   pulse 2: activated nodes: 7274, borderline nodes: 5732, overall activation: 1410.266, activation diff: 1043.209, ratio: 0.740
#   pulse 3: activated nodes: 11258, borderline nodes: 3984, overall activation: 3254.496, activation diff: 1844.230, ratio: 0.567
#   pulse 4: activated nodes: 11443, borderline nodes: 185, overall activation: 4718.223, activation diff: 1463.726, ratio: 0.310
#   pulse 5: activated nodes: 11455, borderline nodes: 12, overall activation: 5675.884, activation diff: 957.661, ratio: 0.169
#   pulse 6: activated nodes: 11455, borderline nodes: 12, overall activation: 6248.124, activation diff: 572.240, ratio: 0.092
#   pulse 7: activated nodes: 11455, borderline nodes: 12, overall activation: 6576.710, activation diff: 328.586, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6576.7
#   number of spread. activ. pulses: 7
#   running time: 821

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99795824   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9957944   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99562466   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9952136   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9921187   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9903061   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.99025023   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.990108   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   9   0.99003226   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   10   0.9898629   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   11   0.9898261   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.9897666   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.98973763   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.98972774   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   15   0.98970133   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   16   0.9895977   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   17   0.98957396   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9894955   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.98932964   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.98917174   REFERENCES:SIMLOC
