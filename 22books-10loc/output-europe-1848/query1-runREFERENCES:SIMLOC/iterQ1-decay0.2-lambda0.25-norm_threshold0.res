###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 546.585, activation diff: 543.791, ratio: 0.995
#   pulse 2: activated nodes: 7274, borderline nodes: 5732, overall activation: 2459.583, activation diff: 1914.618, ratio: 0.778
#   pulse 3: activated nodes: 11258, borderline nodes: 3984, overall activation: 5373.048, activation diff: 2913.465, ratio: 0.542
#   pulse 4: activated nodes: 11443, borderline nodes: 185, overall activation: 6823.339, activation diff: 1450.291, ratio: 0.213
#   pulse 5: activated nodes: 11455, borderline nodes: 12, overall activation: 7369.933, activation diff: 546.595, ratio: 0.074
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7555.720, activation diff: 185.787, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7555.7
#   number of spread. activ. pulses: 6
#   running time: 655

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99981797   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9996014   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.999581   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.99957937   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.99957204   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954987   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   10   0.9995425   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   11   0.99953985   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.9995331   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   13   0.999521   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.9995178   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   15   0.999514   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.9994586   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9994307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   18   0.9994222   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   19   0.9994   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993822   REFERENCES:SIMLOC
