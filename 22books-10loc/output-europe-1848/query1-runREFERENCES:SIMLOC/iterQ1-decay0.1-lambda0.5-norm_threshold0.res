###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 367.057, activation diff: 362.527, ratio: 0.988
#   pulse 2: activated nodes: 7274, borderline nodes: 5732, overall activation: 1545.697, activation diff: 1178.640, ratio: 0.763
#   pulse 3: activated nodes: 11258, borderline nodes: 3984, overall activation: 3924.372, activation diff: 2378.675, ratio: 0.606
#   pulse 4: activated nodes: 11443, borderline nodes: 185, overall activation: 5914.706, activation diff: 1990.334, ratio: 0.337
#   pulse 5: activated nodes: 11455, borderline nodes: 12, overall activation: 7226.140, activation diff: 1311.434, ratio: 0.181
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 8001.107, activation diff: 774.967, ratio: 0.097
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8440.787, activation diff: 439.680, ratio: 0.052
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 8685.989, activation diff: 245.202, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8686.0
#   number of spread. activ. pulses: 8
#   running time: 591

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99897873   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.997812   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9975881   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.99515307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9951251   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.99509263   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.99505424   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   9   0.99501616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   11   0.9948981   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.99488336   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.9948705   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.99486953   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   15   0.9948543   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   16   0.994789   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99475694   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9947177   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99463576   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.9945985   REFERENCES:SIMLOC
