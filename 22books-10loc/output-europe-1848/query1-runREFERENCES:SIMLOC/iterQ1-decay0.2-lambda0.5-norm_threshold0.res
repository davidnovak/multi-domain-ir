###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 367.057, activation diff: 362.527, ratio: 0.988
#   pulse 2: activated nodes: 7274, borderline nodes: 5732, overall activation: 1455.410, activation diff: 1088.353, ratio: 0.748
#   pulse 3: activated nodes: 11258, borderline nodes: 3984, overall activation: 3471.863, activation diff: 2016.453, ratio: 0.581
#   pulse 4: activated nodes: 11443, borderline nodes: 185, overall activation: 5104.973, activation diff: 1633.111, ratio: 0.320
#   pulse 5: activated nodes: 11455, borderline nodes: 12, overall activation: 6179.282, activation diff: 1074.309, ratio: 0.174
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6819.751, activation diff: 640.469, ratio: 0.094
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 7186.208, activation diff: 366.457, ratio: 0.051
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 7392.048, activation diff: 205.839, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7392.0
#   number of spread. activ. pulses: 8
#   running time: 708

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99897873   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9978119   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99758804   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.99515307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9951251   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.9950925   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.9950541   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   9   0.9950161   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   11   0.99489236   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.9948833   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.9948694   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.9948547   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   15   0.99485016   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   16   0.99478763   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99474275   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99468493   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99460214   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.9945844   REFERENCES:SIMLOC
