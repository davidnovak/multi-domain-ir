###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 336.535, activation diff: 332.270, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1357.192, activation diff: 1020.656, ratio: 0.752
#   pulse 3: activated nodes: 11253, borderline nodes: 4112, overall activation: 3621.904, activation diff: 2264.712, ratio: 0.625
#   pulse 4: activated nodes: 11414, borderline nodes: 603, overall activation: 5609.650, activation diff: 1987.746, ratio: 0.354
#   pulse 5: activated nodes: 11448, borderline nodes: 138, overall activation: 6979.972, activation diff: 1370.322, ratio: 0.196
#   pulse 6: activated nodes: 11451, borderline nodes: 41, overall activation: 7810.943, activation diff: 830.971, ratio: 0.106
#   pulse 7: activated nodes: 11454, borderline nodes: 15, overall activation: 8289.703, activation diff: 478.759, ratio: 0.058
#   pulse 8: activated nodes: 11454, borderline nodes: 11, overall activation: 8559.700, activation diff: 269.998, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8559.7
#   number of spread. activ. pulses: 8
#   running time: 703

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99885607   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99762326   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973533   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.99506295   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9950253   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.9949373   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.9947539   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   11   0.9947319   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   12   0.9947156   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   13   0.99471056   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946954   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.9946071   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   16   0.99459606   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   17   0.99459034   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99455976   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99445474   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.99439967   REFERENCES:SIMLOC
