###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 336.535, activation diff: 332.270, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1283.387, activation diff: 946.851, ratio: 0.738
#   pulse 3: activated nodes: 11253, borderline nodes: 4112, overall activation: 3214.343, activation diff: 1930.957, ratio: 0.601
#   pulse 4: activated nodes: 11412, borderline nodes: 648, overall activation: 4848.842, activation diff: 1634.498, ratio: 0.337
#   pulse 5: activated nodes: 11446, borderline nodes: 176, overall activation: 5968.228, activation diff: 1119.387, ratio: 0.188
#   pulse 6: activated nodes: 11449, borderline nodes: 60, overall activation: 6653.334, activation diff: 685.105, ratio: 0.103
#   pulse 7: activated nodes: 11453, borderline nodes: 28, overall activation: 7051.984, activation diff: 398.650, ratio: 0.057
#   pulse 8: activated nodes: 11454, borderline nodes: 14, overall activation: 7278.738, activation diff: 226.754, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7278.7
#   number of spread. activ. pulses: 8
#   running time: 709

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99885607   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99762326   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99735326   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.99506295   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9950253   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.99493706   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.9947475   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   11   0.9947153   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   12   0.99471414   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   13   0.9947105   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946908   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.9946053   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   16   0.9945901   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99458027   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945232   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.9944172   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.9943837   REFERENCES:SIMLOC
