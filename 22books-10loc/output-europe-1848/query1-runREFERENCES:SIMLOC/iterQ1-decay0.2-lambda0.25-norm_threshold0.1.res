###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1542, borderline nodes: 1534, overall activation: 500.803, activation diff: 498.404, ratio: 0.995
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 2220.686, activation diff: 1722.373, ratio: 0.776
#   pulse 3: activated nodes: 11255, borderline nodes: 4075, overall activation: 5103.833, activation diff: 2883.153, ratio: 0.565
#   pulse 4: activated nodes: 11429, borderline nodes: 400, overall activation: 6632.419, activation diff: 1528.586, ratio: 0.230
#   pulse 5: activated nodes: 11449, borderline nodes: 94, overall activation: 7241.701, activation diff: 609.283, ratio: 0.084
#   pulse 6: activated nodes: 11454, borderline nodes: 22, overall activation: 7456.108, activation diff: 214.406, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7456.1
#   number of spread. activ. pulses: 6
#   running time: 779

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99979776   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9995699   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995624   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.9995523   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   9   0.99951077   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   10   0.99950933   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.99950886   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.99950266   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.99949074   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   14   0.99948525   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.99948394   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.9994129   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9993922   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   18   0.9993703   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   19   0.99935955   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993356   REFERENCES:SIMLOC
