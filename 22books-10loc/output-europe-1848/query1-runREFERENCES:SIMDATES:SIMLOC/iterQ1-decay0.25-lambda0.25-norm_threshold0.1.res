###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 501.048, activation diff: 498.650, ratio: 0.995
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 2148.094, activation diff: 1649.535, ratio: 0.768
#   pulse 3: activated nodes: 11288, borderline nodes: 4108, overall activation: 4759.224, activation diff: 2611.131, ratio: 0.549
#   pulse 4: activated nodes: 11435, borderline nodes: 404, overall activation: 6109.589, activation diff: 1350.365, ratio: 0.221
#   pulse 5: activated nodes: 11455, borderline nodes: 117, overall activation: 6651.757, activation diff: 542.168, ratio: 0.082
#   pulse 6: activated nodes: 11459, borderline nodes: 33, overall activation: 6846.302, activation diff: 194.545, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 6846.3
#   number of spread. activ. pulses: 6
#   running time: 560

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9997992   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9995704   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995625   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.99955225   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   9   0.9995148   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   10   0.99951065   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.99950886   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.9995011   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   13   0.9994916   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   14   0.99948597   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   15   0.9994835   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.99940646   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9993856   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   18   0.99937004   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   19   0.9993431   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.9993302   REFERENCES:SIMDATES:SIMLOC
