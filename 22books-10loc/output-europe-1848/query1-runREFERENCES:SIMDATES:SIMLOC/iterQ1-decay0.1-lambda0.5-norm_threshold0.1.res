###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 336.699, activation diff: 332.433, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1359.940, activation diff: 1023.241, ratio: 0.752
#   pulse 3: activated nodes: 11286, borderline nodes: 4145, overall activation: 3628.341, activation diff: 2268.402, ratio: 0.625
#   pulse 4: activated nodes: 11422, borderline nodes: 604, overall activation: 5621.426, activation diff: 1993.084, ratio: 0.355
#   pulse 5: activated nodes: 11454, borderline nodes: 144, overall activation: 6999.225, activation diff: 1377.799, ratio: 0.197
#   pulse 6: activated nodes: 11459, borderline nodes: 45, overall activation: 7837.492, activation diff: 838.267, ratio: 0.107
#   pulse 7: activated nodes: 11460, borderline nodes: 19, overall activation: 8322.253, activation diff: 484.760, ratio: 0.058
#   pulse 8: activated nodes: 11461, borderline nodes: 14, overall activation: 8596.748, activation diff: 274.495, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 8596.7
#   number of spread. activ. pulses: 8
#   running time: 598

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9988673   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99763095   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973533   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.9950642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9950267   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.9949373   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.99477124   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   11   0.9947346   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   12   0.9947319   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.9947156   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946954   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.9946376   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   16   0.99459606   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   17   0.9945904   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99455976   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99445474   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.99439967   REFERENCES:SIMDATES:SIMLOC
