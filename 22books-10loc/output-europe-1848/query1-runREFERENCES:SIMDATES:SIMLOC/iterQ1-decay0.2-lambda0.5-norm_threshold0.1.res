###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 336.699, activation diff: 332.433, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1286.267, activation diff: 949.569, ratio: 0.738
#   pulse 3: activated nodes: 11286, borderline nodes: 4145, overall activation: 3221.176, activation diff: 1934.909, ratio: 0.601
#   pulse 4: activated nodes: 11418, borderline nodes: 649, overall activation: 4860.055, activation diff: 1638.879, ratio: 0.337
#   pulse 5: activated nodes: 11452, borderline nodes: 180, overall activation: 5984.887, activation diff: 1124.832, ratio: 0.188
#   pulse 6: activated nodes: 11455, borderline nodes: 62, overall activation: 6675.222, activation diff: 690.335, ratio: 0.103
#   pulse 7: activated nodes: 11459, borderline nodes: 28, overall activation: 7078.220, activation diff: 402.998, ratio: 0.057
#   pulse 8: activated nodes: 11460, borderline nodes: 17, overall activation: 7308.295, activation diff: 230.074, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 7308.3
#   number of spread. activ. pulses: 8
#   running time: 799

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9988673   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99763095   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99735326   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.9950642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.9950267   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.99493706   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.9947649   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   11   0.9947345   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   12   0.9947153   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.99471414   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946908   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.99463594   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   16   0.9945901   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99458027   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945232   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.9944172   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.9943837   REFERENCES:SIMDATES:SIMLOC
