###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 367.246, activation diff: 362.716, ratio: 0.988
#   pulse 2: activated nodes: 7332, borderline nodes: 5783, overall activation: 1548.630, activation diff: 1181.385, ratio: 0.763
#   pulse 3: activated nodes: 11291, borderline nodes: 3959, overall activation: 3933.554, activation diff: 2384.924, ratio: 0.606
#   pulse 4: activated nodes: 11461, borderline nodes: 170, overall activation: 5931.282, activation diff: 1997.728, ratio: 0.337
#   pulse 5: activated nodes: 11464, borderline nodes: 3, overall activation: 7251.062, activation diff: 1319.780, ratio: 0.182
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8033.354, activation diff: 782.292, ratio: 0.097
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8478.681, activation diff: 445.327, ratio: 0.053
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 8727.939, activation diff: 249.258, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8727.9
#   number of spread. activ. pulses: 8
#   running time: 619

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99898624   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99781734   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9975881   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.995154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.99512607   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.99509263   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.99505424   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   9   0.99501616   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   11   0.9949113   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.9949009   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.9948705   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   14   0.99486953   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   15   0.9948543   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   16   0.99481225   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99475694   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9947177   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99463576   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.9945985   REFERENCES:SIMDATES:SIMLOC
