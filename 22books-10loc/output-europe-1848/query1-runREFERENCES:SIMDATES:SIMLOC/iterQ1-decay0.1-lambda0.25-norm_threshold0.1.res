###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 501.048, activation diff: 498.650, ratio: 0.995
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 2380.785, activation diff: 1882.226, ratio: 0.791
#   pulse 3: activated nodes: 11288, borderline nodes: 4108, overall activation: 5854.973, activation diff: 3474.188, ratio: 0.593
#   pulse 4: activated nodes: 11435, borderline nodes: 380, overall activation: 7776.686, activation diff: 1921.713, ratio: 0.247
#   pulse 5: activated nodes: 11457, borderline nodes: 64, overall activation: 8540.638, activation diff: 763.952, ratio: 0.089
#   pulse 6: activated nodes: 11461, borderline nodes: 18, overall activation: 8806.759, activation diff: 266.121, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 8806.8
#   number of spread. activ. pulses: 6
#   running time: 655

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9997992   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.99957657   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9995704   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.9995649   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995625   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.99955237   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995442   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.9995257   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   9   0.9995148   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   10   0.9995109   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.99950886   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.99950486   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.9995015   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   14   0.999493   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.99949217   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.9994213   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99940443   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.999388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   19   0.9993704   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   20   0.99936426   REFERENCES:SIMDATES:SIMLOC
