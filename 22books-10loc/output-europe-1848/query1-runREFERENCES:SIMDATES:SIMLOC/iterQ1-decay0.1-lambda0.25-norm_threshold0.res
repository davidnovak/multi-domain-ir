###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 546.868, activation diff: 544.074, ratio: 0.995
#   pulse 2: activated nodes: 7332, borderline nodes: 5783, overall activation: 2644.302, activation diff: 2099.034, ratio: 0.794
#   pulse 3: activated nodes: 11291, borderline nodes: 3959, overall activation: 6180.330, activation diff: 3536.027, ratio: 0.572
#   pulse 4: activated nodes: 11461, borderline nodes: 170, overall activation: 8005.108, activation diff: 1824.779, ratio: 0.228
#   pulse 5: activated nodes: 11464, borderline nodes: 3, overall activation: 8690.534, activation diff: 685.426, ratio: 0.079
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8921.900, activation diff: 231.366, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8921.9
#   number of spread. activ. pulses: 6
#   running time: 1367

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9998188   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99960184   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.99958104   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995794   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.99957204   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954987   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   10   0.99954605   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   11   0.99954   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.9995351   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.99952745   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   14   0.99952686   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   15   0.9995203   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.99946624   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99944174   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99942577   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   19   0.9994223   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   20   0.99940807   REFERENCES:SIMDATES:SIMLOC
