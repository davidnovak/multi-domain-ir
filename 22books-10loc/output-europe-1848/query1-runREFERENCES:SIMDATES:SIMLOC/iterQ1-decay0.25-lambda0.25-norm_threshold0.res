###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 546.868, activation diff: 544.074, ratio: 0.995
#   pulse 2: activated nodes: 7332, borderline nodes: 5783, overall activation: 2375.051, activation diff: 1829.782, ratio: 0.770
#   pulse 3: activated nodes: 11291, borderline nodes: 3959, overall activation: 5010.734, activation diff: 2635.683, ratio: 0.526
#   pulse 4: activated nodes: 11461, borderline nodes: 170, overall activation: 6294.435, activation diff: 1283.701, ratio: 0.204
#   pulse 5: activated nodes: 11464, borderline nodes: 3, overall activation: 6782.446, activation diff: 488.011, ratio: 0.072
#   pulse 6: activated nodes: 11464, borderline nodes: 3, overall activation: 6951.330, activation diff: 168.884, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6951.3
#   number of spread. activ. pulses: 6
#   running time: 605

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9998188   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.999606   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9996018   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.99958104   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   5   0.9995794   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.9995719   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.9995667   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   8   0.99955213   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   9   0.99954987   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   10   0.99954605   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   11   0.9995397   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   12   0.9995316   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   13   0.9995263   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   14   0.99951404   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   15   0.9995113   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   16   0.9994529   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.99942476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_276   18   0.99942195   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   19   0.99938524   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   20   0.9993673   REFERENCES:SIMDATES:SIMLOC
