###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 336.699, activation diff: 332.433, ratio: 0.987
#   pulse 2: activated nodes: 7274, borderline nodes: 5733, overall activation: 1249.431, activation diff: 912.732, ratio: 0.731
#   pulse 3: activated nodes: 11286, borderline nodes: 4145, overall activation: 3026.393, activation diff: 1776.962, ratio: 0.587
#   pulse 4: activated nodes: 11417, borderline nodes: 740, overall activation: 4498.741, activation diff: 1472.348, ratio: 0.327
#   pulse 5: activated nodes: 11450, borderline nodes: 197, overall activation: 5500.328, activation diff: 1001.587, ratio: 0.182
#   pulse 6: activated nodes: 11455, borderline nodes: 74, overall activation: 6115.819, activation diff: 615.491, ratio: 0.101
#   pulse 7: activated nodes: 11459, borderline nodes: 40, overall activation: 6476.387, activation diff: 360.568, ratio: 0.056
#   pulse 8: activated nodes: 11459, borderline nodes: 25, overall activation: 6682.996, activation diff: 206.609, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 6683.0
#   number of spread. activ. pulses: 8
#   running time: 646

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.9988673   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9977388   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9976309   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9973532   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.9950642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.99502665   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.99493694   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   8   0.9948845   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   9   0.99478525   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   10   0.9947616   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   11   0.9947345   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   12   0.9947133   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.9947046   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9946876   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   15   0.9946347   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   16   0.99459   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9945718   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9945023   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.99439573   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.99437237   REFERENCES:SIMDATES:SIMLOC
