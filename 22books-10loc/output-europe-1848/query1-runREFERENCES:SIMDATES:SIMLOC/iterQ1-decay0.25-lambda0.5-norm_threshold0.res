###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1
#   europe, 1, LOCATION
#   1848, 1, DATE

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8, borderline nodes: 8, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 1549, borderline nodes: 1541, overall activation: 367.246, activation diff: 362.716, ratio: 0.988
#   pulse 2: activated nodes: 7332, borderline nodes: 5783, overall activation: 1413.377, activation diff: 1046.132, ratio: 0.740
#   pulse 3: activated nodes: 11291, borderline nodes: 3959, overall activation: 3266.347, activation diff: 1852.970, ratio: 0.567
#   pulse 4: activated nodes: 11461, borderline nodes: 170, overall activation: 4737.861, activation diff: 1471.514, ratio: 0.311
#   pulse 5: activated nodes: 11464, borderline nodes: 3, overall activation: 5702.275, activation diff: 964.414, ratio: 0.169
#   pulse 6: activated nodes: 11464, borderline nodes: 3, overall activation: 6279.831, activation diff: 577.556, ratio: 0.092
#   pulse 7: activated nodes: 11464, borderline nodes: 3, overall activation: 6612.342, activation diff: 332.512, ratio: 0.050
#   pulse 8: activated nodes: 11464, borderline nodes: 3, overall activation: 6800.342, activation diff: 188.000, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6800.3
#   number of spread. activ. pulses: 8
#   running time: 634

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_449   1   0.99898624   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   2   0.9978972   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9978173   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.997588   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.995154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.99512607   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.9950924   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.995054   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   9   0.9950161   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   10   0.99493146   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   11   0.99490273   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_212   12   0.9949008   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_463   13   0.99486864   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_255   14   0.9948473   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   15   0.9948452   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_320   16   0.99480987   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.9947352   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.9946662   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_395   19   0.994583   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_318   20   0.99457437   REFERENCES:SIMDATES:SIMLOC
