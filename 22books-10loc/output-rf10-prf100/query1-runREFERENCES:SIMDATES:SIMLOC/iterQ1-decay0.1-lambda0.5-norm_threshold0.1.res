###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.673001142967
#   timetable1974752univ_231, 0.671524932007
#   essentialsinmod01howegoog_452, 0.653548766036
#   millsapscollegec1996mill_75, 0.650953810905
#   frenchschoolsth02instgoog_149, 0.650495550737
#   europesincenapol00leveuoft_193, 0.639766884267
#   cu31924030898922_52, 0.639214730104
#   politicalsketche00retsrich_92, 0.635277905486
#   bostoncollegebul0405bost_132, 0.62467224869
#   miningcampsstudy00shin_121, 0.605119403694
#   bookman44unkngoog_855, 0.575814568699
#   neweasterneurop00butlgoog_162, 0.562275683559
#   essentialsinmod01howegoog_259, 0.520795845881
#   englishcyclopae05kniggoog_41, 0.518145261913
#   youngmidshipman00morrgoog_87, 0.515917560124
#   bostoncollegebul0405bost_129, 0.50547114494
#   lifeofstratfordc02laneuoft_191, 0.49954122475
#   essentialsinmod01howegoog_143, 0.471021279575
#   encyclopediaame28unkngoog_321, 0.44769463336
#   essentialsinmod01howegoog_388, 0.420011194626
#   lifeofstratfordc02laneuoft_193, 0.38832973142
#   shorthistoryofmo00haslrich_264, 0.385879134748
#   essentialsinmod01howegoog_142, 0.339255107097
#   graduateschoolof9192bran_117, 0.324525122343
#   europesincenapol00leveuoft_344, 0.315522454409
#   europesincenapol00leveuoft_57, 0.28124996696
#   essentialsinmod01howegoog_450, 0.2638698591
#   essentialsinmod01howegoog_12, 0.261790758418
#   politicalsketche00retsrich_96, 0.258507355762
#   millsapscollegec1996mill_74, 0.255067530887
#   timetable1974752univ_232, 0.244548190961
#   ouroldworldbackg00bearrich_276, 0.23558912877
#   essentialsinmod01howegoog_208, 0.214821924149
#   youngmidshipman00morrgoog_89, 0.203757898746
#   neweasterneurop00butlgoog_55, 0.190243832417
#   graduateschoolof9192bran_99, 0.190030127678
#   essentialsinmod01howegoog_11, 0.189660176934
#   essentialsinmod01howegoog_244, 0.17083409884
#   bostoncollegebul0405bost_133, 0.169055480629
#   lifeofstratfordc02laneuoft_15, 0.166702641066
#   essentialsinmod01howegoog_140, 0.165633331194
#   shorthistoryofmo00haslrich_29, 0.164788678328
#   essentialsinmod01howegoog_45, 0.163577870057
#   essentialsinmod01howegoog_166, 0.161220164326
#   shorthistoryofmo00haslrich_153, 0.161012599905
#   graduateschoolof9192bran_69, 0.160141840522
#   essentialsinmod01howegoog_170, 0.15722711446
#   essentialsinmod01howegoog_110, 0.15450414794
#   graduateschoolof9192bran_79, 0.152732388451
#   shorthistoryofmo00haslrich_105, 0.138589149671
#   shorthistoryofmo00haslrich_85, 0.134043403451
#   essentialsinmod01howegoog_214, 0.132333480891
#   politicalsketche00retsrich_48, 0.117348869502
#   politicalsketche00retsrich_108, 0.109832578616
#   lifeofstratfordc02laneuoft_199, 0.109133178825
#   shorthistoryofmo00haslrich_80, 0.108719066591
#   essentialsinmod01howegoog_269, 0.0913934354122
#   essentialsinmod01howegoog_282, 0.0907843647074
#   essentialsinmod01howegoog_169, 0.0882895794365
#   lifeofstratfordc02laneuoft_144, 0.0878153381771
#   bostoncollegebul0405bost_29, 0.0872844379168
#   europesincenapol00leveuoft_263, 0.0864168367995
#   essentialsinmod01howegoog_243, 0.0850265631764
#   timetable1974752univ_49, 0.0796825214546
#   essentialsinmod01howegoog_319, 0.0778253397107
#   politicalsketche00retsrich_41, 0.075585194629
#   neweasterneurop00butlgoog_38, 0.0755090438474
#   essentialsinmod01howegoog_111, 0.0710407632233
#   shorthistoryofmo00haslrich_242, 0.0642411680342
#   shorthistoryofmo00haslrich_23, 0.060634402429
#   politicalsketche00retsrich_148, 0.0596702912433
#   millsapscollegec1996mill_49, 0.0509416061764
#   europesincenapol00leveuoft_318, 0.0490010489506
#   essentialsinmod01howegoog_451, 0.0479114475662
#   generalhistoryfo00myerrich_761, 0.0445862283982
#   essentialsinmod01howegoog_390, 0.0397137305335
#   essentialsinmod01howegoog_364, 0.0358749476751
#   politicalsketche00retsrich_40, 0.0354479719507
#   ouroldworldbackg00bearrich_354, 0.0344862599619
#   lifeofstratfordc02laneuoft_187, 0.0326474585063
#   essentialsinmod01howegoog_334, 0.0311249986208
#   englishcyclopae05kniggoog_42, 0.0282928618632
#   englishcyclopae05kniggoog_39, 0.0237366377947
#   cu31924030898922_55, 0.0225854299129
#   neweasterneurop00butlgoog_15, 0.0217128135252
#   millsapscollegec1996mill_113, 0.0144181298176
#   politicalsketche00retsrich_149, 0.00854154770649
#   shorthistoryofmo00haslrich_10, 0.00750948638399
#   shorthistoryofmo00haslrich_14, 0.000335182721076
#   shorthistoryofmo00haslrich_185, 0.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 95, borderline nodes: 95, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 406, borderline nodes: 315, overall activation: 51.141, activation diff: 51.141, ratio: 1.000
#   pulse 2: activated nodes: 8231, borderline nodes: 7903, overall activation: 1409.218, activation diff: 1360.555, ratio: 0.965
#   pulse 3: activated nodes: 10536, borderline nodes: 3741, overall activation: 2922.184, activation diff: 1513.049, ratio: 0.518
#   pulse 4: activated nodes: 11395, borderline nodes: 1701, overall activation: 5028.397, activation diff: 2106.215, ratio: 0.419
#   pulse 5: activated nodes: 11451, borderline nodes: 187, overall activation: 6593.401, activation diff: 1565.004, ratio: 0.237
#   pulse 6: activated nodes: 11458, borderline nodes: 55, overall activation: 7573.255, activation diff: 979.855, ratio: 0.129
#   pulse 7: activated nodes: 11460, borderline nodes: 21, overall activation: 8146.204, activation diff: 572.949, ratio: 0.070
#   pulse 8: activated nodes: 11460, borderline nodes: 14, overall activation: 8472.158, activation diff: 325.955, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8472.2
#   number of spread. activ. pulses: 8
#   running time: 947

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960934   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9959731   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9957042   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.995554   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_13   5   0.99448264   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_855   6   0.99433774   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_121   7   0.99433154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_321   8   0.9938234   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_193   9   0.99381495   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_388   10   0.99379486   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_142   11   0.9933872   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.9931762   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   13   0.9929534   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_41   14   0.9926499   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_208   15   0.9926077   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_92   16   0.9925807   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_132   17   0.99249625   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_264   18   0.9924449   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_133   19   0.99243855   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_29   20   0.99238527   REFERENCES:SIMDATES:SIMLOC
