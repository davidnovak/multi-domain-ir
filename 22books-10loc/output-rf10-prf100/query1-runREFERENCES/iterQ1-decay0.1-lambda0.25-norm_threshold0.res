###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.673001142967
#   timetable1974752univ_231, 0.671524932007
#   essentialsinmod01howegoog_452, 0.653548766036
#   millsapscollegec1996mill_75, 0.650953810905
#   frenchschoolsth02instgoog_149, 0.650495550737
#   europesincenapol00leveuoft_193, 0.639766884267
#   cu31924030898922_52, 0.639214730104
#   politicalsketche00retsrich_92, 0.635277905486
#   bostoncollegebul0405bost_132, 0.62467224869
#   miningcampsstudy00shin_121, 0.605119403694
#   bookman44unkngoog_855, 0.575814568699
#   neweasterneurop00butlgoog_162, 0.562275683559
#   essentialsinmod01howegoog_259, 0.520795845881
#   englishcyclopae05kniggoog_41, 0.518145261913
#   youngmidshipman00morrgoog_87, 0.515917560124
#   bostoncollegebul0405bost_129, 0.50547114494
#   lifeofstratfordc02laneuoft_191, 0.49954122475
#   essentialsinmod01howegoog_143, 0.471021279575
#   encyclopediaame28unkngoog_321, 0.44769463336
#   essentialsinmod01howegoog_388, 0.420011194626
#   lifeofstratfordc02laneuoft_193, 0.38832973142
#   shorthistoryofmo00haslrich_264, 0.385879134748
#   essentialsinmod01howegoog_142, 0.339255107097
#   graduateschoolof9192bran_117, 0.324525122343
#   europesincenapol00leveuoft_344, 0.315522454409
#   europesincenapol00leveuoft_57, 0.28124996696
#   essentialsinmod01howegoog_450, 0.2638698591
#   essentialsinmod01howegoog_12, 0.261790758418
#   politicalsketche00retsrich_96, 0.258507355762
#   millsapscollegec1996mill_74, 0.255067530887
#   timetable1974752univ_232, 0.244548190961
#   ouroldworldbackg00bearrich_276, 0.23558912877
#   essentialsinmod01howegoog_208, 0.214821924149
#   youngmidshipman00morrgoog_89, 0.203757898746
#   neweasterneurop00butlgoog_55, 0.190243832417
#   graduateschoolof9192bran_99, 0.190030127678
#   essentialsinmod01howegoog_11, 0.189660176934
#   essentialsinmod01howegoog_244, 0.17083409884
#   bostoncollegebul0405bost_133, 0.169055480629
#   lifeofstratfordc02laneuoft_15, 0.166702641066
#   essentialsinmod01howegoog_140, 0.165633331194
#   shorthistoryofmo00haslrich_29, 0.164788678328
#   essentialsinmod01howegoog_45, 0.163577870057
#   essentialsinmod01howegoog_166, 0.161220164326
#   shorthistoryofmo00haslrich_153, 0.161012599905
#   graduateschoolof9192bran_69, 0.160141840522
#   essentialsinmod01howegoog_170, 0.15722711446
#   essentialsinmod01howegoog_110, 0.15450414794
#   graduateschoolof9192bran_79, 0.152732388451
#   shorthistoryofmo00haslrich_105, 0.138589149671
#   shorthistoryofmo00haslrich_85, 0.134043403451
#   essentialsinmod01howegoog_214, 0.132333480891
#   politicalsketche00retsrich_48, 0.117348869502
#   politicalsketche00retsrich_108, 0.109832578616
#   lifeofstratfordc02laneuoft_199, 0.109133178825
#   shorthistoryofmo00haslrich_80, 0.108719066591
#   essentialsinmod01howegoog_269, 0.0913934354122
#   essentialsinmod01howegoog_282, 0.0907843647074
#   essentialsinmod01howegoog_169, 0.0882895794365
#   lifeofstratfordc02laneuoft_144, 0.0878153381771
#   bostoncollegebul0405bost_29, 0.0872844379168
#   europesincenapol00leveuoft_263, 0.0864168367995
#   essentialsinmod01howegoog_243, 0.0850265631764
#   timetable1974752univ_49, 0.0796825214546
#   essentialsinmod01howegoog_319, 0.0778253397107
#   politicalsketche00retsrich_41, 0.075585194629
#   neweasterneurop00butlgoog_38, 0.0755090438474
#   essentialsinmod01howegoog_111, 0.0710407632233
#   shorthistoryofmo00haslrich_242, 0.0642411680342
#   shorthistoryofmo00haslrich_23, 0.060634402429
#   politicalsketche00retsrich_148, 0.0596702912433
#   millsapscollegec1996mill_49, 0.0509416061764
#   europesincenapol00leveuoft_318, 0.0490010489506
#   essentialsinmod01howegoog_451, 0.0479114475662
#   generalhistoryfo00myerrich_761, 0.0445862283982
#   essentialsinmod01howegoog_390, 0.0397137305335
#   essentialsinmod01howegoog_364, 0.0358749476751
#   politicalsketche00retsrich_40, 0.0354479719507
#   ouroldworldbackg00bearrich_354, 0.0344862599619
#   lifeofstratfordc02laneuoft_187, 0.0326474585063
#   essentialsinmod01howegoog_334, 0.0311249986208
#   englishcyclopae05kniggoog_42, 0.0282928618632
#   englishcyclopae05kniggoog_39, 0.0237366377947
#   cu31924030898922_55, 0.0225854299129
#   neweasterneurop00butlgoog_15, 0.0217128135252
#   millsapscollegec1996mill_113, 0.0144181298176
#   politicalsketche00retsrich_149, 0.00854154770649
#   shorthistoryofmo00haslrich_10, 0.00750948638399
#   shorthistoryofmo00haslrich_14, 0.000335182721076
#   shorthistoryofmo00haslrich_185, 0.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 95, borderline nodes: 95, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 406, borderline nodes: 315, overall activation: 72.769, activation diff: 85.354, ratio: 1.173
#   pulse 2: activated nodes: 6947, borderline nodes: 6545, overall activation: 2780.648, activation diff: 2763.893, ratio: 0.994
#   pulse 3: activated nodes: 10420, borderline nodes: 3473, overall activation: 4364.297, activation diff: 1705.391, ratio: 0.391
#   pulse 4: activated nodes: 11167, borderline nodes: 747, overall activation: 6512.413, activation diff: 2148.116, ratio: 0.330
#   pulse 5: activated nodes: 11324, borderline nodes: 157, overall activation: 7446.362, activation diff: 933.949, ratio: 0.125
#   pulse 6: activated nodes: 11335, borderline nodes: 11, overall activation: 7791.048, activation diff: 344.686, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 7791.0
#   number of spread. activ. pulses: 6
#   running time: 2092

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99926746   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99922615   REFERENCES
1   Q1   bookman44unkngoog_855   3   0.9991374   REFERENCES
1   Q1   essentialsinmod01howegoog_388   4   0.99911714   REFERENCES
1   Q1   encyclopediaame28unkngoog_321   5   0.9990994   REFERENCES
1   Q1   ouroldworldbackg00bearrich_13   6   0.99909925   REFERENCES
1   Q1   politicalsketche00retsrich_96   7   0.99908054   REFERENCES
1   Q1   essentialsinmod01howegoog_142   8   0.9990716   REFERENCES
1   Q1   miningcampsstudy00shin_121   9   0.99906546   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   10   0.9990628   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   11   0.9990169   REFERENCES
1   Q1   essentialsinmod01howegoog_12   12   0.99901307   REFERENCES
1   Q1   bostoncollegebul0405bost_29   13   0.99901134   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.99900687   REFERENCES
1   Q1   essentialsinmod01howegoog_319   15   0.99900556   REFERENCES
1   Q1   cambridgemodern09protgoog_449   16   0.9989978   REFERENCES
1   Q1   essentialsinmod01howegoog_208   17   0.9989712   REFERENCES
1   Q1   bostoncollegebul0405bost_133   18   0.998943   REFERENCES
1   Q1   essentialsinmod01howegoog_282   19   0.99892914   REFERENCES
1   Q1   essentialsinmod01howegoog_45   20   0.9989076   REFERENCES
