###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 705.264, activation diff: 687.193, ratio: 0.974
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1412.380, activation diff: 707.117, ratio: 0.501
#   pulse 4: activated nodes: 10790, borderline nodes: 2813, overall activation: 2870.081, activation diff: 1457.700, ratio: 0.508
#   pulse 5: activated nodes: 11240, borderline nodes: 1107, overall activation: 3980.452, activation diff: 1110.372, ratio: 0.279
#   pulse 6: activated nodes: 11312, borderline nodes: 381, overall activation: 4696.001, activation diff: 715.549, ratio: 0.152
#   pulse 7: activated nodes: 11318, borderline nodes: 201, overall activation: 5129.482, activation diff: 433.481, ratio: 0.085
#   pulse 8: activated nodes: 11323, borderline nodes: 132, overall activation: 5384.437, activation diff: 254.955, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 5384.4
#   number of spread. activ. pulses: 8
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99566525   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9952085   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9948098   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9911033   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.982052   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79368126   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.793583   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.79349387   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7934699   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7932923   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7932271   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7932266   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.7931329   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.79312766   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.7931155   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.7931061   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.7931051   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.79309726   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.7930585   REFERENCES
