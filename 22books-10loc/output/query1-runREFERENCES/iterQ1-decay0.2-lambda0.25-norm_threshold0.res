###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1595.262, activation diff: 1591.057, ratio: 0.997
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2281.002, activation diff: 902.853, ratio: 0.396
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4355.953, activation diff: 2074.951, ratio: 0.476
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5307.221, activation diff: 951.267, ratio: 0.179
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5656.493, activation diff: 349.272, ratio: 0.062
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 5776.996, activation diff: 120.503, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5777.0
#   number of spread. activ. pulses: 7
#   running time: 620

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9997392   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99966073   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995826   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756926   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99372905   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7997892   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79977053   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7997553   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.79974514   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7997136   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7997136   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7997128   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.79969937   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7996953   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7996929   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.79968715   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.799687   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   19   0.7996869   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.79968286   REFERENCES
