###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 820.018, activation diff: 800.153, ratio: 0.976
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1661.237, activation diff: 841.219, ratio: 0.506
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3129.531, activation diff: 1468.293, ratio: 0.469
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4206.340, activation diff: 1076.810, ratio: 0.256
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4885.938, activation diff: 679.597, ratio: 0.139
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 5291.844, activation diff: 405.906, ratio: 0.077
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 5527.942, activation diff: 236.098, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5527.9
#   number of spread. activ. pulses: 8
#   running time: 487

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99581003   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9955376   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9951489   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99179405   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9840834   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7937107   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79363513   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7935853   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7935792   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7934681   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7933707   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.79337037   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.7933438   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.79331625   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.79330266   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7932867   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   18   0.7932855   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.79328406   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.7932695   REFERENCES
