###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 21.619, activation diff: 21.542, ratio: 0.996
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 811.132, activation diff: 789.624, ratio: 0.973
#   pulse 3: activated nodes: 12194, borderline nodes: 6642, overall activation: 2329.856, activation diff: 1518.724, ratio: 0.652
#   pulse 4: activated nodes: 12936, borderline nodes: 2756, overall activation: 4830.144, activation diff: 2500.288, ratio: 0.518
#   pulse 5: activated nodes: 13009, borderline nodes: 701, overall activation: 6950.976, activation diff: 2120.832, ratio: 0.305
#   pulse 6: activated nodes: 13031, borderline nodes: 215, overall activation: 8382.748, activation diff: 1431.772, ratio: 0.171
#   pulse 7: activated nodes: 13047, borderline nodes: 135, overall activation: 9252.076, activation diff: 869.328, ratio: 0.094
#   pulse 8: activated nodes: 13048, borderline nodes: 105, overall activation: 9746.365, activation diff: 494.289, ratio: 0.051
#   pulse 9: activated nodes: 13048, borderline nodes: 88, overall activation: 10021.801, activation diff: 275.436, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13048
#   final overall activation: 10021.8
#   number of spread. activ. pulses: 9
#   running time: 1505

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980448   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9979348   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99792624   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9978308   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99767566   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.99622536   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   7   0.99616635   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9961621   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9961382   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.99600214   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   11   0.9959707   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.99595195   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   13   0.99591726   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   14   0.9958873   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.9958836   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   16   0.9958781   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.9958607   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99585885   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9958082   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   20   0.995798   REFERENCES:SIMDATES:SIMLOC:SIMDOC
