###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 27.614, activation diff: 27.388, ratio: 0.992
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 967.752, activation diff: 940.138, ratio: 0.971
#   pulse 3: activated nodes: 12575, borderline nodes: 6189, overall activation: 2920.956, activation diff: 1953.204, ratio: 0.669
#   pulse 4: activated nodes: 13039, borderline nodes: 464, overall activation: 5516.627, activation diff: 2595.670, ratio: 0.471
#   pulse 5: activated nodes: 13072, borderline nodes: 33, overall activation: 7544.037, activation diff: 2027.411, ratio: 0.269
#   pulse 6: activated nodes: 13072, borderline nodes: 0, overall activation: 8822.587, activation diff: 1278.549, ratio: 0.145
#   pulse 7: activated nodes: 13072, borderline nodes: 0, overall activation: 9558.574, activation diff: 735.987, ratio: 0.077
#   pulse 8: activated nodes: 13072, borderline nodes: 0, overall activation: 9976.454, activation diff: 417.880, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13072
#   final overall activation: 9976.5
#   number of spread. activ. pulses: 8
#   running time: 1489

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99622315   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9961431   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   historyofuniteds07good_239   3   0.9960919   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.99586844   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9957895   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9932505   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   7   0.9927347   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9926074   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   9   0.99256873   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9924855   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   11   0.99227697   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   12   0.9922701   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.99221313   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.99216855   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.99215436   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   16   0.99211085   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.9920973   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   18   0.9920938   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   19   0.9920849   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.99195284   REFERENCES:SIMDATES:SIMLOC:SIMDOC
