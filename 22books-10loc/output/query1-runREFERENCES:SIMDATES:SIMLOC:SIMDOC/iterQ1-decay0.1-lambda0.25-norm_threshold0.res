###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 38.421, activation diff: 41.081, ratio: 1.069
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 1884.769, activation diff: 1851.962, ratio: 0.983
#   pulse 3: activated nodes: 12575, borderline nodes: 6189, overall activation: 5134.948, activation diff: 3250.213, ratio: 0.633
#   pulse 4: activated nodes: 13039, borderline nodes: 464, overall activation: 8274.238, activation diff: 3139.290, ratio: 0.379
#   pulse 5: activated nodes: 13072, borderline nodes: 33, overall activation: 9709.983, activation diff: 1435.745, ratio: 0.148
#   pulse 6: activated nodes: 13072, borderline nodes: 0, overall activation: 10215.486, activation diff: 505.503, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13072
#   final overall activation: 10215.5
#   number of spread. activ. pulses: 6
#   running time: 2441

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9993311   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99929637   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9992717   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   historyofuniteds07good_239   4   0.9992676   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.99925363   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9991556   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9991326   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.9991071   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   9   0.9990938   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9990824   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   11   0.99907684   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   12   0.9990751   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   13   0.99907136   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   14   0.9990638   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   15   0.9990581   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.9990568   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   17   0.99905264   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   18   0.99905   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   19   0.99903816   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   20   0.9990365   REFERENCES:SIMDATES:SIMLOC:SIMDOC
