###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 29.428, activation diff: 32.313, ratio: 1.098
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 1484.591, activation diff: 1467.132, ratio: 0.988
#   pulse 3: activated nodes: 12242, borderline nodes: 6593, overall activation: 3577.890, activation diff: 2094.886, ratio: 0.586
#   pulse 4: activated nodes: 13002, borderline nodes: 1922, overall activation: 6161.282, activation diff: 2583.391, ratio: 0.419
#   pulse 5: activated nodes: 13017, borderline nodes: 379, overall activation: 7494.051, activation diff: 1332.769, ratio: 0.178
#   pulse 6: activated nodes: 13045, borderline nodes: 158, overall activation: 8050.678, activation diff: 556.627, ratio: 0.069
#   pulse 7: activated nodes: 13048, borderline nodes: 121, overall activation: 8255.322, activation diff: 204.645, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13048
#   final overall activation: 8255.3
#   number of spread. activ. pulses: 7
#   running time: 1467

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99981856   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   historyofuniteds07good_239   2   0.9998169   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9998086   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99980164   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.9997995   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9997781   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.99977225   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.99976724   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9997614   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.99975735   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   11   0.99975723   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   12   0.9997546   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.9997541   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.9997522   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   15   0.9997516   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   16   0.99975145   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.99975055   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   18   0.9997486   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9997463   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   20   0.9997459   REFERENCES:SIMDATES:SIMLOC:SIMDOC
