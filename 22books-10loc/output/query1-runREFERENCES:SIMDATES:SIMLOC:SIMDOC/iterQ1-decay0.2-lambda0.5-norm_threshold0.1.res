###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 21.619, activation diff: 21.542, ratio: 0.996
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 729.396, activation diff: 707.888, ratio: 0.971
#   pulse 3: activated nodes: 12194, borderline nodes: 6642, overall activation: 1989.976, activation diff: 1260.579, ratio: 0.633
#   pulse 4: activated nodes: 12934, borderline nodes: 2925, overall activation: 3957.811, activation diff: 1967.835, ratio: 0.497
#   pulse 5: activated nodes: 13009, borderline nodes: 809, overall activation: 5595.210, activation diff: 1637.399, ratio: 0.293
#   pulse 6: activated nodes: 13024, borderline nodes: 285, overall activation: 6708.910, activation diff: 1113.700, ratio: 0.166
#   pulse 7: activated nodes: 13033, borderline nodes: 153, overall activation: 7407.308, activation diff: 698.398, ratio: 0.094
#   pulse 8: activated nodes: 13047, borderline nodes: 133, overall activation: 7819.229, activation diff: 411.922, ratio: 0.053
#   pulse 9: activated nodes: 13048, borderline nodes: 113, overall activation: 8054.431, activation diff: 235.202, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13048
#   final overall activation: 8054.4
#   number of spread. activ. pulses: 9
#   running time: 1541

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980448   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99793476   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99792624   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9978308   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99767566   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9962238   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   7   0.99616605   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9961604   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9961382   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.99600184   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   11   0.9959695   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.99595135   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   13   0.99591357   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   14   0.9958852   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.9958806   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   16   0.99587214   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.9958596   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   18   0.99585706   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9958062   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   20   0.9957967   REFERENCES:SIMDATES:SIMLOC:SIMDOC
