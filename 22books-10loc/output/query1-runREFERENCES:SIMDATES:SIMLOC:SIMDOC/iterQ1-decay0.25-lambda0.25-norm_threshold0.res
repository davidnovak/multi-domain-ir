###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 38.421, activation diff: 41.081, ratio: 1.069
#   pulse 2: activated nodes: 6386, borderline nodes: 6124, overall activation: 1593.241, activation diff: 1560.433, ratio: 0.979
#   pulse 3: activated nodes: 12575, borderline nodes: 6189, overall activation: 3905.835, activation diff: 2312.623, ratio: 0.592
#   pulse 4: activated nodes: 13039, borderline nodes: 464, overall activation: 5945.228, activation diff: 2039.394, ratio: 0.343
#   pulse 5: activated nodes: 13072, borderline nodes: 33, overall activation: 6915.725, activation diff: 970.497, ratio: 0.140
#   pulse 6: activated nodes: 13072, borderline nodes: 33, overall activation: 7292.467, activation diff: 376.742, ratio: 0.052
#   pulse 7: activated nodes: 13072, borderline nodes: 33, overall activation: 7426.661, activation diff: 134.194, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13072
#   final overall activation: 7426.7
#   number of spread. activ. pulses: 7
#   running time: 1484

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9998324   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9998239   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9998176   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   historyofuniteds07good_239   4   0.9998169   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.99981344   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9997889   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.99978316   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.9997768   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_446   9   0.9997734   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9997706   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   11   0.9997692   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   12   0.99976873   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_17   13   0.99976784   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   14   0.99976593   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   15   0.99976456   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.9997641   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   17   0.9997631   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   18   0.9997625   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   19   0.9997594   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.9997591   REFERENCES:SIMDATES:SIMLOC:SIMDOC
