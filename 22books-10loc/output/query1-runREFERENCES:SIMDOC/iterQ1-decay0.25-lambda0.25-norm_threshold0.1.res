###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 29.428, activation diff: 32.313, ratio: 1.098
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 1384.497, activation diff: 1369.228, ratio: 0.989
#   pulse 3: activated nodes: 12088, borderline nodes: 6759, overall activation: 3157.159, activation diff: 1774.894, ratio: 0.562
#   pulse 4: activated nodes: 12888, borderline nodes: 1963, overall activation: 5125.487, activation diff: 1968.336, ratio: 0.384
#   pulse 5: activated nodes: 13004, borderline nodes: 806, overall activation: 6116.349, activation diff: 990.862, ratio: 0.162
#   pulse 6: activated nodes: 13026, borderline nodes: 228, overall activation: 6536.785, activation diff: 420.437, ratio: 0.064
#   pulse 7: activated nodes: 13043, borderline nodes: 173, overall activation: 6699.382, activation diff: 162.597, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13043
#   final overall activation: 6699.4
#   number of spread. activ. pulses: 7
#   running time: 1396

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99981797   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   2   0.9998169   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99980444   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99980164   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.99979246   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9997776   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9997721   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.9997672   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9997614   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.9997573   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   11   0.9997566   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   12   0.9997544   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.99975395   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.9997522   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   15   0.9997515   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   16   0.99975127   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.9997504   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   18   0.9997485   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9997463   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   20   0.9997456   REFERENCES:SIMDOC
