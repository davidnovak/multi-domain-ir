###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 29.428, activation diff: 32.313, ratio: 1.098
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 1638.091, activation diff: 1622.822, ratio: 0.991
#   pulse 3: activated nodes: 12088, borderline nodes: 6759, overall activation: 4167.667, activation diff: 2531.821, ratio: 0.607
#   pulse 4: activated nodes: 12906, borderline nodes: 1897, overall activation: 7056.523, activation diff: 2888.873, ratio: 0.409
#   pulse 5: activated nodes: 13009, borderline nodes: 547, overall activation: 8578.473, activation diff: 1521.950, ratio: 0.177
#   pulse 6: activated nodes: 13043, borderline nodes: 163, overall activation: 9197.634, activation diff: 619.161, ratio: 0.067
#   pulse 7: activated nodes: 13046, borderline nodes: 103, overall activation: 9415.127, activation diff: 217.492, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13046
#   final overall activation: 9415.1
#   number of spread. activ. pulses: 7
#   running time: 1735

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99981797   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   2   0.9998169   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9998045   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99980164   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.99979246   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.99977785   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9997723   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.99976724   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9997614   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.99975735   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   11   0.99975705   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   12   0.99975467   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.9997541   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.9997522   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   15   0.9997516   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   16   0.9997515   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.9997506   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   18   0.99974865   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9997464   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   20   0.999746   REFERENCES:SIMDOC
