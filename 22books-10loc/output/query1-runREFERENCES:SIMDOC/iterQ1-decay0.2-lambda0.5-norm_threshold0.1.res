###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 21.619, activation diff: 21.542, ratio: 0.996
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 724.382, activation diff: 702.899, ratio: 0.970
#   pulse 3: activated nodes: 12082, borderline nodes: 6781, overall activation: 1950.820, activation diff: 1226.438, ratio: 0.629
#   pulse 4: activated nodes: 12753, borderline nodes: 2894, overall activation: 3771.273, activation diff: 1820.453, ratio: 0.483
#   pulse 5: activated nodes: 12984, borderline nodes: 1325, overall activation: 5232.664, activation diff: 1461.391, ratio: 0.279
#   pulse 6: activated nodes: 13016, borderline nodes: 398, overall activation: 6212.457, activation diff: 979.793, ratio: 0.158
#   pulse 7: activated nodes: 13028, borderline nodes: 196, overall activation: 6830.356, activation diff: 617.899, ratio: 0.090
#   pulse 8: activated nodes: 13043, borderline nodes: 160, overall activation: 7197.791, activation diff: 367.435, ratio: 0.051
#   pulse 9: activated nodes: 13045, borderline nodes: 129, overall activation: 7408.946, activation diff: 211.155, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13045
#   final overall activation: 7408.9
#   number of spread. activ. pulses: 9
#   running time: 1369

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980448   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9979317   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9979181   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9978188   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9976756   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9962199   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   7   0.99616593   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.99615955   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9961381   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.9960017   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   11   0.99596864   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.9959513   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   13   0.9959091   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   14   0.99588424   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.99587953   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.99585885   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.99585545   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.9958058   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   19   0.99579644   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   20   0.99578166   REFERENCES:SIMDOC
