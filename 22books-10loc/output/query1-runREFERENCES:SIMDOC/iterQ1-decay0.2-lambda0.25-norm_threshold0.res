###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 38.421, activation diff: 41.081, ratio: 1.069
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 1662.395, activation diff: 1630.945, ratio: 0.981
#   pulse 3: activated nodes: 12207, borderline nodes: 6252, overall activation: 4187.552, activation diff: 2525.187, ratio: 0.603
#   pulse 4: activated nodes: 13029, borderline nodes: 822, overall activation: 6256.987, activation diff: 2069.435, ratio: 0.331
#   pulse 5: activated nodes: 13070, borderline nodes: 41, overall activation: 7225.234, activation diff: 968.248, ratio: 0.134
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 7592.342, activation diff: 367.108, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 7592.3
#   number of spread. activ. pulses: 6
#   running time: 1252

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9993307   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992941   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9992717   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   4   0.9992676   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.9992512   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9991555   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9991325   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.9991071   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   9   0.99909353   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9990824   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   11   0.9990767   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   12   0.999075   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   13   0.99907136   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   14   0.99906373   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   15   0.9990581   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.99905664   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   17   0.9990525   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   18   0.99905   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   19   0.99903786   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.9990364   REFERENCES:SIMDOC
