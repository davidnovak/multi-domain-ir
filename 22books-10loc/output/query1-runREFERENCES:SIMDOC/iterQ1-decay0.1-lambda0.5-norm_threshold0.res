###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 27.614, activation diff: 27.388, ratio: 0.992
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 953.711, activation diff: 926.097, ratio: 0.971
#   pulse 3: activated nodes: 12207, borderline nodes: 6252, overall activation: 2857.132, activation diff: 1903.421, ratio: 0.666
#   pulse 4: activated nodes: 13029, borderline nodes: 822, overall activation: 5204.463, activation diff: 2347.331, ratio: 0.451
#   pulse 5: activated nodes: 13070, borderline nodes: 41, overall activation: 6990.671, activation diff: 1786.208, ratio: 0.256
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 8130.282, activation diff: 1139.611, ratio: 0.140
#   pulse 7: activated nodes: 13070, borderline nodes: 0, overall activation: 8799.437, activation diff: 669.154, ratio: 0.076
#   pulse 8: activated nodes: 13070, borderline nodes: 0, overall activation: 9187.488, activation diff: 388.051, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 9187.5
#   number of spread. activ. pulses: 8
#   running time: 1397

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.996221   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9961376   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   3   0.9960919   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.99586284   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9957895   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99313194   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   7   0.9927335   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9926072   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   9   0.99256873   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9924855   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   11   0.9922769   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   12   0.9922683   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.9922129   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.99216855   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.9921541   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   16   0.9921108   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.99209666   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   18   0.99209344   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   19   0.9920845   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.99195266   REFERENCES:SIMDOC
