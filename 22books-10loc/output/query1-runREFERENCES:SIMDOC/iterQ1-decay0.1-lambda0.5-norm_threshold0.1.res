###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 21.619, activation diff: 21.542, ratio: 0.996
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 805.560, activation diff: 784.077, ratio: 0.973
#   pulse 3: activated nodes: 12082, borderline nodes: 6781, overall activation: 2288.325, activation diff: 1482.765, ratio: 0.648
#   pulse 4: activated nodes: 12774, borderline nodes: 2737, overall activation: 4568.022, activation diff: 2279.697, ratio: 0.499
#   pulse 5: activated nodes: 12986, borderline nodes: 1175, overall activation: 6436.238, activation diff: 1868.217, ratio: 0.290
#   pulse 6: activated nodes: 13024, borderline nodes: 291, overall activation: 7702.693, activation diff: 1266.455, ratio: 0.164
#   pulse 7: activated nodes: 13042, borderline nodes: 165, overall activation: 8487.481, activation diff: 784.787, ratio: 0.092
#   pulse 8: activated nodes: 13046, borderline nodes: 117, overall activation: 8942.379, activation diff: 454.898, ratio: 0.051
#   pulse 9: activated nodes: 13046, borderline nodes: 95, overall activation: 9200.326, activation diff: 257.947, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13046
#   final overall activation: 9200.3
#   number of spread. activ. pulses: 9
#   running time: 1387

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980448   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9979317   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9979182   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9978188   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99767566   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9962216   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   7   0.9961663   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9961613   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99613816   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.996002   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   11   0.9959699   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.9959519   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   13   0.99591273   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   14   0.99588656   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.99588263   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.99585986   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.9958574   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.9958079   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   19   0.99579775   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   20   0.99578774   REFERENCES:SIMDOC
