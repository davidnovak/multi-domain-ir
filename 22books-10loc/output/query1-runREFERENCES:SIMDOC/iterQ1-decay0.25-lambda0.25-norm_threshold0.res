###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 38.421, activation diff: 41.081, ratio: 1.069
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 1566.895, activation diff: 1535.445, ratio: 0.980
#   pulse 3: activated nodes: 12207, borderline nodes: 6252, overall activation: 3794.915, activation diff: 2228.049, ratio: 0.587
#   pulse 4: activated nodes: 13029, borderline nodes: 822, overall activation: 5571.683, activation diff: 1776.768, ratio: 0.319
#   pulse 5: activated nodes: 13070, borderline nodes: 41, overall activation: 6392.419, activation diff: 820.736, ratio: 0.128
#   pulse 6: activated nodes: 13070, borderline nodes: 41, overall activation: 6709.690, activation diff: 317.271, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 6709.7
#   number of spread. activ. pulses: 6
#   running time: 1372

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9993307   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992941   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9992717   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   4   0.9992676   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.9992512   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.99915546   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9991325   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.9991071   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   9   0.9990935   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9990824   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   11   0.9990767   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   12   0.99907494   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   13   0.99907136   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   14   0.99906373   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   15   0.9990581   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.9990566   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   17   0.99905246   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   18   0.99904996   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   19   0.9990377   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.9990364   REFERENCES:SIMDOC
