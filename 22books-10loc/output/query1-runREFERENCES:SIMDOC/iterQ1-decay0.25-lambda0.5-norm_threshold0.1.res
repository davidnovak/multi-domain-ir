###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 21.619, activation diff: 21.542, ratio: 0.996
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 683.793, activation diff: 662.310, ratio: 0.969
#   pulse 3: activated nodes: 12082, borderline nodes: 6781, overall activation: 1790.193, activation diff: 1106.401, ratio: 0.618
#   pulse 4: activated nodes: 12739, borderline nodes: 3030, overall activation: 3397.504, activation diff: 1607.311, ratio: 0.473
#   pulse 5: activated nodes: 12980, borderline nodes: 1407, overall activation: 4664.605, activation diff: 1267.101, ratio: 0.272
#   pulse 6: activated nodes: 13005, borderline nodes: 487, overall activation: 5507.438, activation diff: 842.833, ratio: 0.153
#   pulse 7: activated nodes: 13026, borderline nodes: 257, overall activation: 6037.355, activation diff: 529.917, ratio: 0.088
#   pulse 8: activated nodes: 13041, borderline nodes: 215, overall activation: 6356.267, activation diff: 318.912, ratio: 0.050
#   pulse 9: activated nodes: 13043, borderline nodes: 187, overall activation: 6541.700, activation diff: 185.433, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13043
#   final overall activation: 6541.7
#   number of spread. activ. pulses: 9
#   running time: 1362

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980448   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9979317   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9979181   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9978188   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9976756   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9962189   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   7   0.99616575   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.99615854   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9961381   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.9960015   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   11   0.99596786   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.99595094   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   13   0.9959071   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   14   0.99588287   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.99587786   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   16   0.9958583   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.99585426   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.9958044   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   19   0.9957958   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_69   20   0.9957782   REFERENCES:SIMDOC
