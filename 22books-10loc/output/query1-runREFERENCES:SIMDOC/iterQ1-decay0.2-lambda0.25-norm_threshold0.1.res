###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 29.428, activation diff: 32.313, ratio: 1.098
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 1469.028, activation diff: 1453.760, ratio: 0.990
#   pulse 3: activated nodes: 12088, borderline nodes: 6759, overall activation: 3482.429, activation diff: 2015.631, ratio: 0.579
#   pulse 4: activated nodes: 12897, borderline nodes: 1942, overall activation: 5745.264, activation diff: 2262.846, ratio: 0.394
#   pulse 5: activated nodes: 13005, borderline nodes: 710, overall activation: 6908.887, activation diff: 1163.623, ratio: 0.168
#   pulse 6: activated nodes: 13041, borderline nodes: 199, overall activation: 7401.636, activation diff: 492.750, ratio: 0.067
#   pulse 7: activated nodes: 13044, borderline nodes: 140, overall activation: 7584.483, activation diff: 182.847, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13044
#   final overall activation: 7584.5
#   number of spread. activ. pulses: 7
#   running time: 1411

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99981797   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   2   0.9998169   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99980444   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99980164   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.99979246   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.9997777   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   7   0.9997722   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   8   0.99976724   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9997614   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   10   0.99975735   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   11   0.99975675   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   12   0.99975455   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.999754   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.9997522   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   15   0.99975157   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   16   0.9997514   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   17   0.9997505   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   18   0.9997486   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.9997463   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   20   0.9997457   REFERENCES:SIMDOC
