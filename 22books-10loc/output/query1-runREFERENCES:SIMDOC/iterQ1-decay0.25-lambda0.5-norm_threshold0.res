###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 262, borderline nodes: 256, overall activation: 27.614, activation diff: 27.388, ratio: 0.992
#   pulse 2: activated nodes: 5955, borderline nodes: 5693, overall activation: 809.358, activation diff: 781.744, ratio: 0.966
#   pulse 3: activated nodes: 12207, borderline nodes: 6252, overall activation: 2215.762, activation diff: 1406.403, ratio: 0.635
#   pulse 4: activated nodes: 13029, borderline nodes: 822, overall activation: 3839.616, activation diff: 1623.854, ratio: 0.423
#   pulse 5: activated nodes: 13070, borderline nodes: 41, overall activation: 5039.294, activation diff: 1199.679, ratio: 0.238
#   pulse 6: activated nodes: 13070, borderline nodes: 41, overall activation: 5808.178, activation diff: 768.883, ratio: 0.132
#   pulse 7: activated nodes: 13070, borderline nodes: 41, overall activation: 6270.193, activation diff: 462.015, ratio: 0.074
#   pulse 8: activated nodes: 13070, borderline nodes: 41, overall activation: 6537.137, activation diff: 266.944, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 6537.1
#   number of spread. activ. pulses: 8
#   running time: 1413

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99622095   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99613756   REFERENCES:SIMDOC
1   Q1   historyofuniteds07good_239   3   0.9960919   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.99586284   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9957895   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99312234   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   7   0.99273187   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   8   0.9926056   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_16   9   0.9925686   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   10   0.9924855   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_17   11   0.9922768   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_446   12   0.992265   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   13   0.99221194   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   14   0.99216825   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   15   0.9921509   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_329   16   0.992108   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   17   0.9920943   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_187   18   0.99209166   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   19   0.99208176   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.9919501   REFERENCES:SIMDOC
