###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 13.124, activation diff: 15.784, ratio: 1.203
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 36.046, activation diff: 25.191, ratio: 0.699
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 101.150, activation diff: 65.417, ratio: 0.647
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 266.087, activation diff: 164.949, ratio: 0.620
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 616.977, activation diff: 350.890, ratio: 0.569
#   pulse 6: activated nodes: 8978, borderline nodes: 10, overall activation: 1271.213, activation diff: 654.236, ratio: 0.515
#   pulse 7: activated nodes: 8978, borderline nodes: 0, overall activation: 2080.799, activation diff: 809.586, ratio: 0.389
#   pulse 8: activated nodes: 8978, borderline nodes: 0, overall activation: 2967.866, activation diff: 887.067, ratio: 0.299
#   pulse 9: activated nodes: 8978, borderline nodes: 0, overall activation: 3914.439, activation diff: 946.572, ratio: 0.242
#   pulse 10: activated nodes: 8978, borderline nodes: 0, overall activation: 4623.531, activation diff: 709.092, ratio: 0.153
#   pulse 11: activated nodes: 8978, borderline nodes: 0, overall activation: 5013.552, activation diff: 390.021, ratio: 0.078
#   pulse 12: activated nodes: 8978, borderline nodes: 0, overall activation: 5212.283, activation diff: 198.731, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 5212.3
#   number of spread. activ. pulses: 12
#   running time: 1967

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999986   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.99999857   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.99999857   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.99999857   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.99999845   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.99999833   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999833   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999982   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999981   SIMDOC
1   Q1   europesincenapol00leveuoft_45   10   0.99998647   SIMDOC
1   Q1   europesincenapol00leveuoft_17   11   0.999985   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   12   0.9999822   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   13   0.9999813   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   14   0.99998075   SIMDOC
1   Q1   cambridgemodern09protgoog_737   15   0.9999776   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   16   0.99997544   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   17   0.9999686   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   18   0.9999677   SIMDOC
1   Q1   generalhistoryfo00myerrich_791   19   0.9999659   SIMDOC
1   Q1   cambridgemodern09protgoog_418   20   0.99996185   SIMDOC
