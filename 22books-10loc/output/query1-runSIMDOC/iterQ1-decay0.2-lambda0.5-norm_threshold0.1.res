###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.869, activation diff: 5.792, ratio: 0.987
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 4.629, activation diff: 2.343, ratio: 0.506
#   pulse 3: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.653, activation diff: 1.700, ratio: 0.465
#   pulse 4: activated nodes: 2100, borderline nodes: 1894, overall activation: 3.385, activation diff: 1.824, ratio: 0.539
#   pulse 5: activated nodes: 2261, borderline nodes: 2034, overall activation: 4.833, activation diff: 2.857, ratio: 0.591
#   pulse 6: activated nodes: 2528, borderline nodes: 2260, overall activation: 10.279, activation diff: 6.314, ratio: 0.614
#   pulse 7: activated nodes: 2794, borderline nodes: 2487, overall activation: 23.174, activation diff: 13.329, ratio: 0.575
#   pulse 8: activated nodes: 2987, borderline nodes: 2646, overall activation: 43.119, activation diff: 20.162, ratio: 0.468
#   pulse 9: activated nodes: 3293, borderline nodes: 2890, overall activation: 64.308, activation diff: 21.297, ratio: 0.331
#   pulse 10: activated nodes: 3666, borderline nodes: 3169, overall activation: 83.782, activation diff: 19.528, ratio: 0.233
#   pulse 11: activated nodes: 4168, borderline nodes: 3541, overall activation: 101.280, activation diff: 17.525, ratio: 0.173
#   pulse 12: activated nodes: 4515, borderline nodes: 3754, overall activation: 117.228, activation diff: 15.961, ratio: 0.136
#   pulse 13: activated nodes: 4847, borderline nodes: 3967, overall activation: 132.287, activation diff: 15.066, ratio: 0.114
#   pulse 14: activated nodes: 5084, borderline nodes: 4075, overall activation: 147.432, activation diff: 15.149, ratio: 0.103
#   pulse 15: activated nodes: 5337, borderline nodes: 4177, overall activation: 164.425, activation diff: 16.995, ratio: 0.103
#   pulse 16: activated nodes: 5602, borderline nodes: 4256, overall activation: 187.688, activation diff: 23.264, ratio: 0.124
#   pulse 17: activated nodes: 5842, borderline nodes: 4304, overall activation: 224.940, activation diff: 37.252, ratio: 0.166
#   pulse 18: activated nodes: 6130, borderline nodes: 4422, overall activation: 271.196, activation diff: 46.255, ratio: 0.171
#   pulse 19: activated nodes: 6409, borderline nodes: 4544, overall activation: 318.898, activation diff: 47.702, ratio: 0.150
#   pulse 20: activated nodes: 6669, borderline nodes: 4613, overall activation: 367.617, activation diff: 48.720, ratio: 0.133

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6669
#   final overall activation: 367.6
#   number of spread. activ. pulses: 20
#   running time: 824

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.99995494   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.99995184   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.9999517   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.99995136   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.9999509   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.9999478   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.9999475   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999463   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.9999456   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99986935   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99978614   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9997784   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9997524   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.9996343   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.9995829   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.9995827   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.99951947   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9985515   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.998443   SIMDOC
1   Q1   cambridgemodern09protgoog_731   20   0.9971999   SIMDOC
