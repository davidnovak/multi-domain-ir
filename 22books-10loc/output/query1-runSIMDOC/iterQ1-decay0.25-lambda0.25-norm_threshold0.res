###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 13.124, activation diff: 15.784, ratio: 1.203
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 32.988, activation diff: 22.132, ratio: 0.671
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 83.296, activation diff: 50.643, ratio: 0.608
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 194.637, activation diff: 111.356, ratio: 0.572
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 391.251, activation diff: 196.614, ratio: 0.503
#   pulse 6: activated nodes: 8968, borderline nodes: 300, overall activation: 697.069, activation diff: 305.818, ratio: 0.439
#   pulse 7: activated nodes: 8968, borderline nodes: 300, overall activation: 1082.353, activation diff: 385.284, ratio: 0.356
#   pulse 8: activated nodes: 8968, borderline nodes: 300, overall activation: 1447.681, activation diff: 365.328, ratio: 0.252
#   pulse 9: activated nodes: 8968, borderline nodes: 300, overall activation: 1748.236, activation diff: 300.555, ratio: 0.172
#   pulse 10: activated nodes: 8968, borderline nodes: 300, overall activation: 1993.043, activation diff: 244.807, ratio: 0.123
#   pulse 11: activated nodes: 8968, borderline nodes: 300, overall activation: 2191.612, activation diff: 198.569, ratio: 0.091
#   pulse 12: activated nodes: 8968, borderline nodes: 300, overall activation: 2355.228, activation diff: 163.616, ratio: 0.069
#   pulse 13: activated nodes: 8968, borderline nodes: 300, overall activation: 2488.345, activation diff: 133.117, ratio: 0.053
#   pulse 14: activated nodes: 8968, borderline nodes: 300, overall activation: 2590.860, activation diff: 102.516, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8968
#   final overall activation: 2590.9
#   number of spread. activ. pulses: 14
#   running time: 1079

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.99999976   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999976   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99999964   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999917   SIMDOC
1   Q1   bookman44unkngoog_753   10   0.99999404   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   11   0.99999285   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   12   0.9999773   SIMDOC
1   Q1   europesincenapol00leveuoft_45   13   0.9999719   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   14   0.99996895   SIMDOC
1   Q1   europesincenapol00leveuoft_17   15   0.99995524   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   16   0.99994886   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   17   0.99994534   SIMDOC
1   Q1   cambridgemodern09protgoog_737   18   0.99993545   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   19   0.9999013   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   20   0.9998852   SIMDOC
