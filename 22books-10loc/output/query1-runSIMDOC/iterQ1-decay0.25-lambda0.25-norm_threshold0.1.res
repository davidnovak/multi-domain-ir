###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.803, activation diff: 8.688, ratio: 1.497
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.642, activation diff: 3.114, ratio: 0.855
#   pulse 3: activated nodes: 2088, borderline nodes: 1881, overall activation: 2.975, activation diff: 2.368, ratio: 0.796
#   pulse 4: activated nodes: 2231, borderline nodes: 2003, overall activation: 5.256, activation diff: 3.939, ratio: 0.749
#   pulse 5: activated nodes: 2538, borderline nodes: 2263, overall activation: 15.743, activation diff: 11.097, ratio: 0.705
#   pulse 6: activated nodes: 2846, borderline nodes: 2522, overall activation: 41.165, activation diff: 25.574, ratio: 0.621
#   pulse 7: activated nodes: 3180, borderline nodes: 2800, overall activation: 71.814, activation diff: 30.687, ratio: 0.427
#   pulse 8: activated nodes: 3593, borderline nodes: 3112, overall activation: 95.992, activation diff: 24.188, ratio: 0.252
#   pulse 9: activated nodes: 4288, borderline nodes: 3612, overall activation: 114.908, activation diff: 18.918, ratio: 0.165
#   pulse 10: activated nodes: 4731, borderline nodes: 3890, overall activation: 130.825, activation diff: 15.918, ratio: 0.122
#   pulse 11: activated nodes: 5076, borderline nodes: 4077, overall activation: 145.970, activation diff: 15.145, ratio: 0.104
#   pulse 12: activated nodes: 5306, borderline nodes: 4144, overall activation: 163.174, activation diff: 17.204, ratio: 0.105
#   pulse 13: activated nodes: 5579, borderline nodes: 4221, overall activation: 188.325, activation diff: 25.151, ratio: 0.134
#   pulse 14: activated nodes: 5874, borderline nodes: 4320, overall activation: 228.685, activation diff: 40.360, ratio: 0.176
#   pulse 15: activated nodes: 6101, borderline nodes: 4375, overall activation: 273.780, activation diff: 45.096, ratio: 0.165
#   pulse 16: activated nodes: 6388, borderline nodes: 4510, overall activation: 316.133, activation diff: 42.353, ratio: 0.134
#   pulse 17: activated nodes: 6603, borderline nodes: 4543, overall activation: 355.650, activation diff: 39.518, ratio: 0.111
#   pulse 18: activated nodes: 6865, borderline nodes: 4596, overall activation: 392.921, activation diff: 37.271, ratio: 0.095
#   pulse 19: activated nodes: 7068, borderline nodes: 4596, overall activation: 429.896, activation diff: 36.975, ratio: 0.086
#   pulse 20: activated nodes: 7231, borderline nodes: 4529, overall activation: 471.513, activation diff: 41.617, ratio: 0.088

###################################
# spreading activation process summary: 
#   final number of activated nodes: 7231
#   final overall activation: 471.5
#   number of spread. activ. pulses: 20
#   running time: 1000

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.99999976   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999964   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999995   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999887   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9999279   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9998909   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9998321   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.999823   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.99969625   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   15   0.999636   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   16   0.99963546   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.99953455   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9985101   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.99842715   SIMDOC
1   Q1   cambridgemodern09protgoog_742   20   0.9973371   SIMDOC
