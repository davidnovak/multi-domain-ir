###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 13.124, activation diff: 15.784, ratio: 1.203
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 34.007, activation diff: 23.152, ratio: 0.681
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 89.001, activation diff: 55.321, ratio: 0.622
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 216.359, activation diff: 127.371, ratio: 0.589
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 455.928, activation diff: 239.570, ratio: 0.525
#   pulse 6: activated nodes: 8978, borderline nodes: 10, overall activation: 853.204, activation diff: 397.275, ratio: 0.466
#   pulse 7: activated nodes: 8978, borderline nodes: 10, overall activation: 1364.858, activation diff: 511.654, ratio: 0.375
#   pulse 8: activated nodes: 8978, borderline nodes: 10, overall activation: 1840.467, activation diff: 475.609, ratio: 0.258
#   pulse 9: activated nodes: 8978, borderline nodes: 10, overall activation: 2274.920, activation diff: 434.452, ratio: 0.191
#   pulse 10: activated nodes: 8978, borderline nodes: 10, overall activation: 2675.294, activation diff: 400.374, ratio: 0.150
#   pulse 11: activated nodes: 8978, borderline nodes: 10, overall activation: 3029.703, activation diff: 354.410, ratio: 0.117
#   pulse 12: activated nodes: 8978, borderline nodes: 10, overall activation: 3290.517, activation diff: 260.814, ratio: 0.079
#   pulse 13: activated nodes: 8978, borderline nodes: 10, overall activation: 3451.033, activation diff: 160.516, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 3451.0
#   number of spread. activ. pulses: 13
#   running time: 1083

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   0.99999964   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   0.99999964   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.99999964   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.99999964   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999995   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.9999995   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999995   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999994   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999992   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   10   0.9999893   SIMDOC
1   Q1   bookman44unkngoog_753   11   0.99998474   SIMDOC
1   Q1   europesincenapol00leveuoft_45   12   0.9999821   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   13   0.9999798   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   14   0.9999796   SIMDOC
1   Q1   europesincenapol00leveuoft_17   15   0.9999752   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   16   0.99996495   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   17   0.99996376   SIMDOC
1   Q1   cambridgemodern09protgoog_737   18   0.9999564   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   19   0.9999343   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   20   0.99992275   SIMDOC
