###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 10.749, activation diff: 10.523, ratio: 0.979
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 22.537, activation diff: 14.094, ratio: 0.625
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 49.608, activation diff: 27.944, ratio: 0.563
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 109.443, activation diff: 60.111, ratio: 0.549
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 228.566, activation diff: 119.173, ratio: 0.521
#   pulse 6: activated nodes: 8978, borderline nodes: 10, overall activation: 451.808, activation diff: 223.242, ratio: 0.494
#   pulse 7: activated nodes: 8978, borderline nodes: 0, overall activation: 848.256, activation diff: 396.447, ratio: 0.467
#   pulse 8: activated nodes: 8978, borderline nodes: 0, overall activation: 1352.424, activation diff: 504.169, ratio: 0.373
#   pulse 9: activated nodes: 8978, borderline nodes: 0, overall activation: 1923.102, activation diff: 570.678, ratio: 0.297
#   pulse 10: activated nodes: 8978, borderline nodes: 0, overall activation: 2578.935, activation diff: 655.833, ratio: 0.254
#   pulse 11: activated nodes: 8978, borderline nodes: 0, overall activation: 3282.892, activation diff: 703.957, ratio: 0.214
#   pulse 12: activated nodes: 8978, borderline nodes: 0, overall activation: 3910.521, activation diff: 627.629, ratio: 0.160
#   pulse 13: activated nodes: 8978, borderline nodes: 0, overall activation: 4386.712, activation diff: 476.191, ratio: 0.109
#   pulse 14: activated nodes: 8978, borderline nodes: 0, overall activation: 4720.973, activation diff: 334.261, ratio: 0.071
#   pulse 15: activated nodes: 8978, borderline nodes: 0, overall activation: 4949.713, activation diff: 228.739, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 4949.7
#   number of spread. activ. pulses: 15
#   running time: 1263

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.999819   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.9998135   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.9998127   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.9998115   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.99980354   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.9997971   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9997952   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99979246   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99979126   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9997091   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9996774   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.99965906   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9996467   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   14   0.9996098   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.99957055   SIMDOC
1   Q1   cambridgemodern09protgoog_729   16   0.99956906   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   17   0.9995612   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   18   0.9995558   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   19   0.99955237   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   20   0.99952424   SIMDOC
