###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.869, activation diff: 5.792, ratio: 0.987
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 4.629, activation diff: 2.343, ratio: 0.506
#   pulse 3: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.655, activation diff: 1.702, ratio: 0.466
#   pulse 4: activated nodes: 2100, borderline nodes: 1894, overall activation: 3.413, activation diff: 1.850, ratio: 0.542
#   pulse 5: activated nodes: 2261, borderline nodes: 2034, overall activation: 5.006, activation diff: 3.002, ratio: 0.600
#   pulse 6: activated nodes: 2533, borderline nodes: 2263, overall activation: 11.026, activation diff: 6.888, ratio: 0.625
#   pulse 7: activated nodes: 2794, borderline nodes: 2487, overall activation: 25.492, activation diff: 14.901, ratio: 0.585
#   pulse 8: activated nodes: 2996, borderline nodes: 2652, overall activation: 48.146, activation diff: 22.871, ratio: 0.475
#   pulse 9: activated nodes: 3348, borderline nodes: 2934, overall activation: 72.848, activation diff: 24.811, ratio: 0.341
#   pulse 10: activated nodes: 3805, borderline nodes: 3274, overall activation: 96.927, activation diff: 24.132, ratio: 0.249
#   pulse 11: activated nodes: 4309, borderline nodes: 3629, overall activation: 120.422, activation diff: 23.522, ratio: 0.195
#   pulse 12: activated nodes: 4741, borderline nodes: 3903, overall activation: 144.243, activation diff: 23.835, ratio: 0.165
#   pulse 13: activated nodes: 5144, borderline nodes: 4127, overall activation: 170.578, activation diff: 26.342, ratio: 0.154
#   pulse 14: activated nodes: 5550, borderline nodes: 4314, overall activation: 205.622, activation diff: 35.047, ratio: 0.170
#   pulse 15: activated nodes: 5827, borderline nodes: 4312, overall activation: 268.361, activation diff: 62.742, ratio: 0.234
#   pulse 16: activated nodes: 6364, borderline nodes: 4577, overall activation: 361.185, activation diff: 92.824, ratio: 0.257
#   pulse 17: activated nodes: 6737, borderline nodes: 4694, overall activation: 464.959, activation diff: 103.774, ratio: 0.223
#   pulse 18: activated nodes: 7030, borderline nodes: 4583, overall activation: 586.404, activation diff: 121.445, ratio: 0.207
#   pulse 19: activated nodes: 7451, borderline nodes: 4463, overall activation: 749.142, activation diff: 162.738, ratio: 0.217
#   pulse 20: activated nodes: 7773, borderline nodes: 4262, overall activation: 943.563, activation diff: 194.421, ratio: 0.206

###################################
# spreading activation process summary: 
#   final number of activated nodes: 7773
#   final overall activation: 943.6
#   number of spread. activ. pulses: 20
#   running time: 955

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999566   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999536   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995345   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.9999534   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.9999526   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.9999503   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.9999502   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999486   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.9999478   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99990845   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9998679   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.99985325   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.99985033   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   14   0.9997622   SIMDOC
1   Q1   cambridgemodern09protgoog_729   15   0.9997612   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.9997233   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9997034   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9990257   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.99899036   SIMDOC
1   Q1   cambridgemodern09protgoog_742   20   0.99791753   SIMDOC
