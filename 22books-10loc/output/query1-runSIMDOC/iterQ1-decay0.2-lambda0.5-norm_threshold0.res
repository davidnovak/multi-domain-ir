###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 10.749, activation diff: 10.523, ratio: 0.979
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 21.630, activation diff: 13.187, ratio: 0.610
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 45.052, activation diff: 24.301, ratio: 0.539
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 93.423, activation diff: 48.659, ratio: 0.521
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 181.641, activation diff: 88.276, ratio: 0.486
#   pulse 6: activated nodes: 8978, borderline nodes: 10, overall activation: 327.699, activation diff: 146.061, ratio: 0.446
#   pulse 7: activated nodes: 8978, borderline nodes: 10, overall activation: 557.849, activation diff: 230.150, ratio: 0.413
#   pulse 8: activated nodes: 8978, borderline nodes: 10, overall activation: 875.312, activation diff: 317.463, ratio: 0.363
#   pulse 9: activated nodes: 8978, borderline nodes: 10, overall activation: 1215.341, activation diff: 340.029, ratio: 0.280
#   pulse 10: activated nodes: 8978, borderline nodes: 10, overall activation: 1549.765, activation diff: 334.424, ratio: 0.216
#   pulse 11: activated nodes: 8978, borderline nodes: 10, overall activation: 1875.504, activation diff: 325.739, ratio: 0.174
#   pulse 12: activated nodes: 8978, borderline nodes: 10, overall activation: 2189.950, activation diff: 314.446, ratio: 0.144
#   pulse 13: activated nodes: 8978, borderline nodes: 10, overall activation: 2490.778, activation diff: 300.827, ratio: 0.121
#   pulse 14: activated nodes: 8978, borderline nodes: 10, overall activation: 2767.863, activation diff: 277.085, ratio: 0.100
#   pulse 15: activated nodes: 8978, borderline nodes: 10, overall activation: 3002.943, activation diff: 235.080, ratio: 0.078
#   pulse 16: activated nodes: 8978, borderline nodes: 10, overall activation: 3186.964, activation diff: 184.021, ratio: 0.058
#   pulse 17: activated nodes: 8978, borderline nodes: 10, overall activation: 3324.096, activation diff: 137.133, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 3324.1
#   number of spread. activ. pulses: 17
#   running time: 1158

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999535   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.9999521   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995196   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.9999516   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.99994934   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.99994767   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999472   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99994636   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99994576   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99989057   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9998735   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   12   0.9998599   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9998325   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   14   0.99982893   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   15   0.9998127   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   16   0.9998045   SIMDOC
1   Q1   europesincenapol00leveuoft_45   17   0.9997913   SIMDOC
1   Q1   europesincenapol00leveuoft_17   18   0.9997724   SIMDOC
1   Q1   generalhistoryfo00myerrich_791   19   0.9997343   SIMDOC
1   Q1   cambridgemodern09protgoog_729   20   0.9997283   SIMDOC
