###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.869, activation diff: 5.792, ratio: 0.987
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 4.629, activation diff: 2.343, ratio: 0.506
#   pulse 3: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.652, activation diff: 1.699, ratio: 0.465
#   pulse 4: activated nodes: 2100, borderline nodes: 1894, overall activation: 3.370, activation diff: 1.811, ratio: 0.537
#   pulse 5: activated nodes: 2261, borderline nodes: 2034, overall activation: 4.748, activation diff: 2.787, ratio: 0.587
#   pulse 6: activated nodes: 2528, borderline nodes: 2260, overall activation: 9.919, activation diff: 6.039, ratio: 0.609
#   pulse 7: activated nodes: 2794, borderline nodes: 2487, overall activation: 22.069, activation diff: 12.585, ratio: 0.570
#   pulse 8: activated nodes: 2977, borderline nodes: 2638, overall activation: 40.747, activation diff: 18.895, ratio: 0.464
#   pulse 9: activated nodes: 3275, borderline nodes: 2877, overall activation: 60.377, activation diff: 19.739, ratio: 0.327
#   pulse 10: activated nodes: 3559, borderline nodes: 3086, overall activation: 77.938, activation diff: 17.615, ratio: 0.226
#   pulse 11: activated nodes: 4108, borderline nodes: 3505, overall activation: 93.110, activation diff: 15.198, ratio: 0.163
#   pulse 12: activated nodes: 4456, borderline nodes: 3726, overall activation: 106.312, activation diff: 13.215, ratio: 0.124
#   pulse 13: activated nodes: 4708, borderline nodes: 3881, overall activation: 118.032, activation diff: 11.727, ratio: 0.099
#   pulse 14: activated nodes: 4886, borderline nodes: 3962, overall activation: 128.849, activation diff: 10.821, ratio: 0.084
#   pulse 15: activated nodes: 5108, borderline nodes: 4073, overall activation: 139.434, activation diff: 10.586, ratio: 0.076
#   pulse 16: activated nodes: 5289, borderline nodes: 4139, overall activation: 150.729, activation diff: 11.296, ratio: 0.075
#   pulse 17: activated nodes: 5463, borderline nodes: 4178, overall activation: 164.568, activation diff: 13.839, ratio: 0.084
#   pulse 18: activated nodes: 5702, borderline nodes: 4265, overall activation: 184.304, activation diff: 19.737, ratio: 0.107
#   pulse 19: activated nodes: 5905, borderline nodes: 4336, overall activation: 210.868, activation diff: 26.563, ratio: 0.126
#   pulse 20: activated nodes: 6081, borderline nodes: 4384, overall activation: 239.401, activation diff: 28.533, ratio: 0.119

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6081
#   final overall activation: 239.4
#   number of spread. activ. pulses: 20
#   running time: 789

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.99995404   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999509   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995077   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.9999502   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.99994993   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.99994624   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.99994564   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999449   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.9999442   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9998349   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   11   0.9997209   SIMDOC
1   Q1   cambridgemodern09protgoog_737   12   0.99971026   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.99966604   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.9995407   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   15   0.99948615   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   16   0.99943805   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9993835   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9982456   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.9980688   SIMDOC
1   Q1   cambridgemodern09protgoog_731   20   0.99676746   SIMDOC
