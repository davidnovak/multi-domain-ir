###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 10.749, activation diff: 10.523, ratio: 0.979
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 21.177, activation diff: 12.734, ratio: 0.601
#   pulse 3: activated nodes: 5973, borderline nodes: 3898, overall activation: 42.885, activation diff: 22.591, ratio: 0.527
#   pulse 4: activated nodes: 8668, borderline nodes: 2695, overall activation: 86.235, activation diff: 43.645, ratio: 0.506
#   pulse 5: activated nodes: 8968, borderline nodes: 300, overall activation: 162.038, activation diff: 75.865, ratio: 0.468
#   pulse 6: activated nodes: 8968, borderline nodes: 300, overall activation: 280.522, activation diff: 118.489, ratio: 0.422
#   pulse 7: activated nodes: 8968, borderline nodes: 300, overall activation: 455.346, activation diff: 174.824, ratio: 0.384
#   pulse 8: activated nodes: 8968, borderline nodes: 300, overall activation: 689.418, activation diff: 234.072, ratio: 0.340
#   pulse 9: activated nodes: 8968, borderline nodes: 300, overall activation: 954.735, activation diff: 265.317, ratio: 0.278
#   pulse 10: activated nodes: 8968, borderline nodes: 300, overall activation: 1210.048, activation diff: 255.313, ratio: 0.211
#   pulse 11: activated nodes: 8968, borderline nodes: 300, overall activation: 1443.142, activation diff: 233.094, ratio: 0.162
#   pulse 12: activated nodes: 8968, borderline nodes: 300, overall activation: 1652.427, activation diff: 209.285, ratio: 0.127
#   pulse 13: activated nodes: 8968, borderline nodes: 300, overall activation: 1837.122, activation diff: 184.695, ratio: 0.101
#   pulse 14: activated nodes: 8968, borderline nodes: 300, overall activation: 1998.961, activation diff: 161.839, ratio: 0.081
#   pulse 15: activated nodes: 8968, borderline nodes: 300, overall activation: 2140.873, activation diff: 141.912, ratio: 0.066
#   pulse 16: activated nodes: 8968, borderline nodes: 300, overall activation: 2265.137, activation diff: 124.264, ratio: 0.055
#   pulse 17: activated nodes: 8968, borderline nodes: 300, overall activation: 2372.982, activation diff: 107.845, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8968
#   final overall activation: 2373.0
#   number of spread. activ. pulses: 17
#   running time: 1183

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999528   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.99995136   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995124   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.9999509   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.9999485   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.9999467   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99994636   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99994516   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99994457   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99986684   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9998425   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   12   0.9998344   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.99978626   SIMDOC
1   Q1   cambridgemodern09protgoog_418   14   0.99978113   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   15   0.9997617   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   16   0.9997543   SIMDOC
1   Q1   europesincenapol00leveuoft_45   17   0.9997494   SIMDOC
1   Q1   europesincenapol00leveuoft_17   18   0.9997117   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   19   0.9996624   SIMDOC
1   Q1   cambridgemodern09protgoog_729   20   0.99965894   SIMDOC
