###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.803, activation diff: 8.688, ratio: 1.497
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.645, activation diff: 3.117, ratio: 0.855
#   pulse 3: activated nodes: 2088, borderline nodes: 1881, overall activation: 2.991, activation diff: 2.382, ratio: 0.796
#   pulse 4: activated nodes: 2231, borderline nodes: 2003, overall activation: 5.375, activation diff: 4.045, ratio: 0.753
#   pulse 5: activated nodes: 2538, borderline nodes: 2263, overall activation: 16.416, activation diff: 11.652, ratio: 0.710
#   pulse 6: activated nodes: 2846, borderline nodes: 2522, overall activation: 43.478, activation diff: 27.214, ratio: 0.626
#   pulse 7: activated nodes: 3181, borderline nodes: 2799, overall activation: 76.534, activation diff: 33.094, ratio: 0.432
#   pulse 8: activated nodes: 3638, borderline nodes: 3139, overall activation: 103.925, activation diff: 27.401, ratio: 0.264
#   pulse 9: activated nodes: 4401, borderline nodes: 3691, overall activation: 126.991, activation diff: 23.069, ratio: 0.182
#   pulse 10: activated nodes: 4902, borderline nodes: 4009, overall activation: 148.453, activation diff: 21.462, ratio: 0.145
#   pulse 11: activated nodes: 5240, borderline nodes: 4154, overall activation: 172.173, activation diff: 23.720, ratio: 0.138
#   pulse 12: activated nodes: 5610, borderline nodes: 4286, overall activation: 207.431, activation diff: 35.258, ratio: 0.170
#   pulse 13: activated nodes: 5899, borderline nodes: 4311, overall activation: 271.619, activation diff: 64.188, ratio: 0.236
#   pulse 14: activated nodes: 6282, borderline nodes: 4465, overall activation: 347.456, activation diff: 75.837, ratio: 0.218
#   pulse 15: activated nodes: 6697, borderline nodes: 4609, overall activation: 421.668, activation diff: 74.212, ratio: 0.176
#   pulse 16: activated nodes: 7032, borderline nodes: 4621, overall activation: 497.017, activation diff: 75.350, ratio: 0.152
#   pulse 17: activated nodes: 7329, borderline nodes: 4483, overall activation: 579.989, activation diff: 82.971, ratio: 0.143
#   pulse 18: activated nodes: 7553, borderline nodes: 4284, overall activation: 688.778, activation diff: 108.789, ratio: 0.158
#   pulse 19: activated nodes: 7782, borderline nodes: 4056, overall activation: 829.142, activation diff: 140.364, ratio: 0.169
#   pulse 20: activated nodes: 8042, borderline nodes: 3825, overall activation: 995.775, activation diff: 166.632, ratio: 0.167

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8042
#   final overall activation: 995.8
#   number of spread. activ. pulses: 20
#   running time: 1063

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.9999998   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99999976   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999934   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9999553   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9999343   SIMDOC
1   Q1   cambridgemodern09protgoog_418   12   0.99989164   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.99988437   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.9997778   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.9997628   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.9997237   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9996635   SIMDOC
1   Q1   historyofuniteds07good_71   18   0.9992329   SIMDOC
1   Q1   cambridgemodern09protgoog_744   19   0.9988079   SIMDOC
1   Q1   cambridgemodern09protgoog_747   20   0.9987782   SIMDOC
