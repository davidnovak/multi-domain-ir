###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (6): 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 199, borderline nodes: 193, overall activation: 5.803, activation diff: 8.688, ratio: 1.497
#   pulse 2: activated nodes: 2075, borderline nodes: 1876, overall activation: 3.650, activation diff: 3.122, ratio: 0.855
#   pulse 3: activated nodes: 2088, borderline nodes: 1881, overall activation: 3.021, activation diff: 2.409, ratio: 0.797
#   pulse 4: activated nodes: 2247, borderline nodes: 2018, overall activation: 5.617, activation diff: 4.261, ratio: 0.759
#   pulse 5: activated nodes: 2541, borderline nodes: 2265, overall activation: 17.812, activation diff: 12.809, ratio: 0.719
#   pulse 6: activated nodes: 2855, borderline nodes: 2529, overall activation: 48.350, activation diff: 30.691, ratio: 0.635
#   pulse 7: activated nodes: 3213, borderline nodes: 2821, overall activation: 86.783, activation diff: 38.471, ratio: 0.443
#   pulse 8: activated nodes: 3817, borderline nodes: 3280, overall activation: 122.065, activation diff: 35.291, ratio: 0.289
#   pulse 9: activated nodes: 4560, borderline nodes: 3790, overall activation: 156.631, activation diff: 34.569, ratio: 0.221
#   pulse 10: activated nodes: 5206, borderline nodes: 4181, overall activation: 197.000, activation diff: 40.370, ratio: 0.205
#   pulse 11: activated nodes: 5670, borderline nodes: 4335, overall activation: 264.262, activation diff: 67.262, ratio: 0.255
#   pulse 12: activated nodes: 6197, borderline nodes: 4478, overall activation: 401.675, activation diff: 137.413, ratio: 0.342
#   pulse 13: activated nodes: 6691, borderline nodes: 4633, overall activation: 567.777, activation diff: 166.102, ratio: 0.293
#   pulse 14: activated nodes: 7268, borderline nodes: 4686, overall activation: 766.931, activation diff: 199.154, ratio: 0.260
#   pulse 15: activated nodes: 7708, borderline nodes: 4308, overall activation: 1063.005, activation diff: 296.074, ratio: 0.279
#   pulse 16: activated nodes: 8074, borderline nodes: 3878, overall activation: 1435.150, activation diff: 372.145, ratio: 0.259
#   pulse 17: activated nodes: 8420, borderline nodes: 3263, overall activation: 1909.272, activation diff: 474.122, ratio: 0.248
#   pulse 18: activated nodes: 8651, borderline nodes: 2593, overall activation: 2500.947, activation diff: 591.675, ratio: 0.237
#   pulse 19: activated nodes: 8776, borderline nodes: 1904, overall activation: 3129.521, activation diff: 628.574, ratio: 0.201
#   pulse 20: activated nodes: 8863, borderline nodes: 1360, overall activation: 3690.004, activation diff: 560.484, ratio: 0.152

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8863
#   final overall activation: 3690.0
#   number of spread. activ. pulses: 20
#   running time: 1175

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_730   2   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_749   4   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   5   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_743   7   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99998254   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9999761   SIMDOC
1   Q1   cambridgemodern09protgoog_418   12   0.99995804   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.99994534   SIMDOC
1   Q1   historyofuniteds07good_71   14   0.9999431   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.99990284   SIMDOC
1   Q1   cambridgemodern09protgoog_729   16   0.99988234   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   17   0.9998491   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   18   0.9998301   SIMDOC
1   Q1   cambridgemodern09protgoog_744   19   0.99928284   SIMDOC
1   Q1   cambridgemodern09protgoog_747   20   0.9992629   SIMDOC
