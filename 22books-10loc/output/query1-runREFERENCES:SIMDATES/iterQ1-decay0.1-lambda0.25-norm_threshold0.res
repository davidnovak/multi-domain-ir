###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1794.155, activation diff: 1788.711, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2791.054, activation diff: 1205.436, ratio: 0.432
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 5560.603, activation diff: 2769.549, ratio: 0.498
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 6871.040, activation diff: 1310.437, ratio: 0.191
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 7367.744, activation diff: 496.704, ratio: 0.067
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7544.869, activation diff: 177.125, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7544.9
#   number of spread. activ. pulses: 7
#   running time: 1190

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99974805   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9997192   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995827   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756944   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9938133   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8997728   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.899742   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8997383   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.8997174   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.8997132   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.89969116   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.899678   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.89967775   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.8996743   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.8996661   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.89965785   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.8996558   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.8996531   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.8996483   REFERENCES:SIMDATES
