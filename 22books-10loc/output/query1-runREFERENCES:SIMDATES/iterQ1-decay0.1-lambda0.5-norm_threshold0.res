###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 920.519, activation diff: 900.654, ratio: 0.978
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1972.972, activation diff: 1052.453, ratio: 0.533
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3929.505, activation diff: 1956.533, ratio: 0.498
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5385.088, activation diff: 1455.584, ratio: 0.270
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6313.062, activation diff: 927.974, ratio: 0.147
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 6871.874, activation diff: 558.812, ratio: 0.081
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 7199.640, activation diff: 327.766, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7199.6
#   number of spread. activ. pulses: 8
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99581754   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9955956   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99515   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9917967   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9842398   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89293104   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89283967   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8927945   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.89277655   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.8926631   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.89254993   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.8925423   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.89254194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.8925165   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.89248073   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.8924787   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.89245653   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.89244735   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.8924322   REFERENCES:SIMDATES
