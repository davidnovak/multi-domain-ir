###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 933.896, activation diff: 914.031, ratio: 0.979
#   pulse 3: activated nodes: 9664, borderline nodes: 3877, overall activation: 2041.049, activation diff: 1107.153, ratio: 0.542
#   pulse 4: activated nodes: 11396, borderline nodes: 1732, overall activation: 4274.386, activation diff: 2233.336, ratio: 0.522
#   pulse 5: activated nodes: 11463, borderline nodes: 67, overall activation: 5994.456, activation diff: 1720.070, ratio: 0.287
#   pulse 6: activated nodes: 11464, borderline nodes: 1, overall activation: 7086.517, activation diff: 1092.061, ratio: 0.154
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7727.357, activation diff: 640.841, ratio: 0.083
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 8091.846, activation diff: 364.489, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8091.8
#   number of spread. activ. pulses: 8
#   running time: 621

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608874   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99584013   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99559593   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99519646   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99180543   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.984555   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89293385   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8928479   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89279675   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.892785   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8926756   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89257103   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.8925649   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.89255655   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8925518   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.8925087   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.89250696   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.8924979   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.8924845   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8924531   REFERENCES:SIMDATES:SIMLOC
