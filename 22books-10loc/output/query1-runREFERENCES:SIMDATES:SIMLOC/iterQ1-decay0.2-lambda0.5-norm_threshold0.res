###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 832.684, activation diff: 812.819, ratio: 0.976
#   pulse 3: activated nodes: 9664, borderline nodes: 3877, overall activation: 1738.047, activation diff: 905.363, ratio: 0.521
#   pulse 4: activated nodes: 11396, borderline nodes: 1732, overall activation: 3436.009, activation diff: 1697.962, ratio: 0.494
#   pulse 5: activated nodes: 11463, borderline nodes: 67, overall activation: 4734.647, activation diff: 1298.637, ratio: 0.274
#   pulse 6: activated nodes: 11464, borderline nodes: 1, overall activation: 5575.605, activation diff: 840.958, ratio: 0.151
#   pulse 7: activated nodes: 11464, borderline nodes: 1, overall activation: 6079.437, activation diff: 503.833, ratio: 0.083
#   pulse 8: activated nodes: 11464, borderline nodes: 1, overall activation: 6370.876, activation diff: 291.439, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6370.9
#   number of spread. activ. pulses: 8
#   running time: 664

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608874   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9958401   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99559593   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99519587   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.991803   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9845518   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79371864   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7936419   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79359674   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.79358613   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7934885   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7933941   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.79338884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.79337966   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.79337883   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.79333854   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.79333735   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.793326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.7933183   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7932892   REFERENCES:SIMDATES:SIMLOC
