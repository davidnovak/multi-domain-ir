###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 1450.441, activation diff: 1447.190, ratio: 0.998
#   pulse 3: activated nodes: 9353, borderline nodes: 3774, overall activation: 2039.731, activation diff: 901.517, ratio: 0.442
#   pulse 4: activated nodes: 11349, borderline nodes: 2753, overall activation: 4639.231, activation diff: 2600.073, ratio: 0.560
#   pulse 5: activated nodes: 11444, borderline nodes: 259, overall activation: 5868.433, activation diff: 1229.202, ratio: 0.209
#   pulse 6: activated nodes: 11455, borderline nodes: 75, overall activation: 6351.700, activation diff: 483.268, ratio: 0.076
#   pulse 7: activated nodes: 11459, borderline nodes: 34, overall activation: 6525.879, activation diff: 174.179, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 6525.9
#   number of spread. activ. pulses: 7
#   running time: 679

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99972475   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99961656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995158   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972502   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99315935   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7997884   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7997526   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7997254   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7996875   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.7996844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.79967916   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.7996657   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.79965997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.79965925   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.79965675   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7996559   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.79964316   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.79963607   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.79963565   REFERENCES:SIMDATES:SIMLOC
