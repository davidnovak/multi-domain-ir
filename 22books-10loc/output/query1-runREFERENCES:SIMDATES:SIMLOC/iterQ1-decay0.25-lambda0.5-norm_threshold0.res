###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 782.078, activation diff: 762.213, ratio: 0.975
#   pulse 3: activated nodes: 9664, borderline nodes: 3877, overall activation: 1591.211, activation diff: 809.134, ratio: 0.509
#   pulse 4: activated nodes: 11396, borderline nodes: 1732, overall activation: 3045.818, activation diff: 1454.607, ratio: 0.478
#   pulse 5: activated nodes: 11463, borderline nodes: 67, overall activation: 4146.488, activation diff: 1100.670, ratio: 0.265
#   pulse 6: activated nodes: 11463, borderline nodes: 67, overall activation: 4861.619, activation diff: 715.131, ratio: 0.147
#   pulse 7: activated nodes: 11463, borderline nodes: 67, overall activation: 5294.022, activation diff: 432.404, ratio: 0.082
#   pulse 8: activated nodes: 11463, borderline nodes: 67, overall activation: 5546.455, activation diff: 252.433, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11463
#   final overall activation: 5546.5
#   number of spread. activ. pulses: 8
#   running time: 590

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608874   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99583995   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99559593   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99519545   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99180114   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98454916   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74411106   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74403906   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74399674   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7439866   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.743895   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.74380565   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.7438009   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.74379236   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7437916   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7437537   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7437527   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.74374044   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.7437352   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7437073   REFERENCES:SIMDATES:SIMLOC
