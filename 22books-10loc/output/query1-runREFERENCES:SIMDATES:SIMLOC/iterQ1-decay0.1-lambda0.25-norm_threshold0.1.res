###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 1629.568, activation diff: 1626.316, ratio: 0.998
#   pulse 3: activated nodes: 9353, borderline nodes: 3774, overall activation: 2453.224, activation diff: 1169.461, ratio: 0.477
#   pulse 4: activated nodes: 11349, borderline nodes: 2743, overall activation: 5896.294, activation diff: 3443.685, ratio: 0.584
#   pulse 5: activated nodes: 11444, borderline nodes: 216, overall activation: 7509.890, activation diff: 1613.596, ratio: 0.215
#   pulse 6: activated nodes: 11458, borderline nodes: 44, overall activation: 8116.952, activation diff: 607.062, ratio: 0.075
#   pulse 7: activated nodes: 11461, borderline nodes: 16, overall activation: 8327.436, activation diff: 210.484, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 8327.4
#   number of spread. activ. pulses: 7
#   running time: 721

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99972475   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99961656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99951583   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972503   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931594   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997624   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89972275   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8996911   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.89965   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.8996493   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89964175   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.8996266   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.8996202   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.89961684   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.89961594   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.8996153   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.89961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   19   0.89959604   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.8995951   REFERENCES:SIMDATES:SIMLOC
