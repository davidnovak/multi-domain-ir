###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 796.153, activation diff: 777.933, ratio: 0.977
#   pulse 3: activated nodes: 9161, borderline nodes: 3661, overall activation: 1711.445, activation diff: 915.292, ratio: 0.535
#   pulse 4: activated nodes: 11337, borderline nodes: 3204, overall activation: 3870.418, activation diff: 2158.973, ratio: 0.558
#   pulse 5: activated nodes: 11434, borderline nodes: 356, overall activation: 5626.917, activation diff: 1756.499, ratio: 0.312
#   pulse 6: activated nodes: 11455, borderline nodes: 124, overall activation: 6792.798, activation diff: 1165.880, ratio: 0.172
#   pulse 7: activated nodes: 11459, borderline nodes: 37, overall activation: 7494.042, activation diff: 701.244, ratio: 0.094
#   pulse 8: activated nodes: 11460, borderline nodes: 17, overall activation: 7899.210, activation diff: 405.167, ratio: 0.051
#   pulse 9: activated nodes: 11461, borderline nodes: 15, overall activation: 8129.200, activation diff: 229.991, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 8129.2
#   number of spread. activ. pulses: 9
#   running time: 609

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99785584   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9976523   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9974177   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9944931   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98840797   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.896452   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8963951   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8963481   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8963302   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.89624417   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8962042   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.8961936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.89617264   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8961569   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.8961393   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.8961384   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.89613247   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.89612955   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.8961272   REFERENCES:SIMDATES:SIMLOC
