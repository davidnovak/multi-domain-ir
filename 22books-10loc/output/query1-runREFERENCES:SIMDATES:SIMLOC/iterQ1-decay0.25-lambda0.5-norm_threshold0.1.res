###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 666.951, activation diff: 648.731, ratio: 0.973
#   pulse 3: activated nodes: 9161, borderline nodes: 3661, overall activation: 1337.840, activation diff: 670.889, ratio: 0.501
#   pulse 4: activated nodes: 11335, borderline nodes: 3315, overall activation: 2745.387, activation diff: 1407.547, ratio: 0.513
#   pulse 5: activated nodes: 11426, borderline nodes: 495, overall activation: 3854.632, activation diff: 1109.245, ratio: 0.288
#   pulse 6: activated nodes: 11450, borderline nodes: 213, overall activation: 4601.285, activation diff: 746.653, ratio: 0.162
#   pulse 7: activated nodes: 11453, borderline nodes: 105, overall activation: 5065.074, activation diff: 463.789, ratio: 0.092
#   pulse 8: activated nodes: 11454, borderline nodes: 91, overall activation: 5341.133, activation diff: 276.059, ratio: 0.052
#   pulse 9: activated nodes: 11456, borderline nodes: 86, overall activation: 5501.898, activation diff: 160.765, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 5501.9
#   number of spread. activ. pulses: 9
#   running time: 611

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99785566   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9976523   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9974168   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99448943   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74704325   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74699545   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7469567   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.74694157   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.74686944   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7468352   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74682796   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7468088   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7467956   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.74678195   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7467791   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7467765   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7467735   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.74677145   REFERENCES:SIMDATES:SIMLOC
