###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 710.019, activation diff: 691.798, ratio: 0.974
#   pulse 3: activated nodes: 9161, borderline nodes: 3661, overall activation: 1459.585, activation diff: 749.566, ratio: 0.514
#   pulse 4: activated nodes: 11336, borderline nodes: 3248, overall activation: 3099.824, activation diff: 1640.239, ratio: 0.529
#   pulse 5: activated nodes: 11429, borderline nodes: 446, overall activation: 4412.472, activation diff: 1312.649, ratio: 0.297
#   pulse 6: activated nodes: 11451, borderline nodes: 170, overall activation: 5297.821, activation diff: 885.349, ratio: 0.167
#   pulse 7: activated nodes: 11455, borderline nodes: 70, overall activation: 5843.127, activation diff: 545.306, ratio: 0.093
#   pulse 8: activated nodes: 11457, borderline nodes: 47, overall activation: 6164.501, activation diff: 321.374, ratio: 0.052
#   pulse 9: activated nodes: 11459, borderline nodes: 32, overall activation: 6349.926, activation diff: 185.425, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 6349.9
#   number of spread. activ. pulses: 9
#   running time: 688

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9978557   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9976523   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99741715   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.994491   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9884035   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79684615   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79679537   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7967539   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7967378   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.796661   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7966248   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79661655   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79659665   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7965827   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.79656744   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7965658   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.79656184   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.79655874   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.7965569   REFERENCES:SIMDATES:SIMLOC
