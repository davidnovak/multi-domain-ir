###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5787, borderline nodes: 5718, overall activation: 1360.878, activation diff: 1357.626, ratio: 0.998
#   pulse 3: activated nodes: 9353, borderline nodes: 3774, overall activation: 1841.187, activation diff: 775.436, ratio: 0.421
#   pulse 4: activated nodes: 11348, borderline nodes: 2758, overall activation: 4059.149, activation diff: 2218.497, ratio: 0.547
#   pulse 5: activated nodes: 11443, borderline nodes: 289, overall activation: 5096.588, activation diff: 1037.439, ratio: 0.204
#   pulse 6: activated nodes: 11454, borderline nodes: 109, overall activation: 5511.224, activation diff: 414.636, ratio: 0.075
#   pulse 7: activated nodes: 11456, borderline nodes: 85, overall activation: 5663.984, activation diff: 152.760, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 5664.0
#   number of spread. activ. pulses: 7
#   running time: 584

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99972475   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99961656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995158   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972501   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931593   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7498014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74976754   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74974245   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7497066   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.749702   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.749698   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.74968535   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.74968046   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.74968004   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.74967754   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.74967605   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.74966043   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.74965864   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.7496583   REFERENCES:SIMDATES:SIMLOC
