###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 795.857, activation diff: 777.705, ratio: 0.977
#   pulse 3: activated nodes: 9151, borderline nodes: 3655, overall activation: 1703.549, activation diff: 907.692, ratio: 0.533
#   pulse 4: activated nodes: 11299, borderline nodes: 3166, overall activation: 3850.570, activation diff: 2147.021, ratio: 0.558
#   pulse 5: activated nodes: 11428, borderline nodes: 357, overall activation: 5600.104, activation diff: 1749.535, ratio: 0.312
#   pulse 6: activated nodes: 11449, borderline nodes: 119, overall activation: 6759.345, activation diff: 1159.240, ratio: 0.172
#   pulse 7: activated nodes: 11452, borderline nodes: 34, overall activation: 7454.673, activation diff: 695.328, ratio: 0.093
#   pulse 8: activated nodes: 11454, borderline nodes: 15, overall activation: 7855.040, activation diff: 400.368, ratio: 0.051
#   pulse 9: activated nodes: 11454, borderline nodes: 12, overall activation: 8081.416, activation diff: 226.375, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8081.4
#   number of spread. activ. pulses: 9
#   running time: 589

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9978509   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99760425   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9974177   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9944931   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98832315   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89644843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8963951   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8963409   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8963302   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8962349   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8962042   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.8961936   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.89617264   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89615154   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8961384   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.89613247   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.89612794   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.8961272   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.89612424   REFERENCES:SIMLOC
