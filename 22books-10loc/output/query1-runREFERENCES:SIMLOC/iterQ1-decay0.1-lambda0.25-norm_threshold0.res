###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1822.333, activation diff: 1816.522, ratio: 0.997
#   pulse 3: activated nodes: 9480, borderline nodes: 3759, overall activation: 2938.432, activation diff: 1273.889, ratio: 0.434
#   pulse 4: activated nodes: 11369, borderline nodes: 1889, overall activation: 6264.196, activation diff: 3325.764, ratio: 0.531
#   pulse 5: activated nodes: 11448, borderline nodes: 79, overall activation: 7719.916, activation diff: 1455.721, ratio: 0.189
#   pulse 6: activated nodes: 11457, borderline nodes: 9, overall activation: 8233.354, activation diff: 513.438, ratio: 0.062
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8403.432, activation diff: 170.078, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8403.4
#   number of spread. activ. pulses: 7
#   running time: 1387

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998138   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999766   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99966216   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996254   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99758005   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99388766   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.899753   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89972883   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8997255   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.8997083   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89970684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.8996991   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.89969426   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8996909   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.89968944   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.89968413   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8996732   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   19   0.89967036   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.89967024   REFERENCES:SIMLOC
