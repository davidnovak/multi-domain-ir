###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1622.032, activation diff: 1616.221, ratio: 0.996
#   pulse 3: activated nodes: 9480, borderline nodes: 3759, overall activation: 2444.918, activation diff: 966.398, ratio: 0.395
#   pulse 4: activated nodes: 11369, borderline nodes: 1889, overall activation: 4941.327, activation diff: 2496.409, ratio: 0.505
#   pulse 5: activated nodes: 11448, borderline nodes: 79, overall activation: 6053.856, activation diff: 1112.529, ratio: 0.184
#   pulse 6: activated nodes: 11457, borderline nodes: 9, overall activation: 6462.922, activation diff: 409.066, ratio: 0.063
#   pulse 7: activated nodes: 11457, borderline nodes: 9, overall activation: 6603.435, activation diff: 140.513, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6603.4
#   number of spread. activ. pulses: 7
#   running time: 555

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998138   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999766   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99966216   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996253   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99758005   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99388766   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79979503   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79977983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7997588   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.79975533   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7997376   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7997369   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.799731   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.79972726   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.79972357   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.79972357   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.79971313   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7997072   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.79970694   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.79970276   REFERENCES:SIMLOC
