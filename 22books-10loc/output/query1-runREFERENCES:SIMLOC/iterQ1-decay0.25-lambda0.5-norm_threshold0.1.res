###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 666.663, activation diff: 648.511, ratio: 0.973
#   pulse 3: activated nodes: 9151, borderline nodes: 3655, overall activation: 1325.962, activation diff: 659.298, ratio: 0.497
#   pulse 4: activated nodes: 11297, borderline nodes: 3277, overall activation: 2717.160, activation diff: 1391.198, ratio: 0.512
#   pulse 5: activated nodes: 11420, borderline nodes: 505, overall activation: 3818.079, activation diff: 1100.919, ratio: 0.288
#   pulse 6: activated nodes: 11436, borderline nodes: 222, overall activation: 4559.734, activation diff: 741.655, ratio: 0.163
#   pulse 7: activated nodes: 11438, borderline nodes: 120, overall activation: 5020.143, activation diff: 460.408, ratio: 0.092
#   pulse 8: activated nodes: 11439, borderline nodes: 107, overall activation: 5293.827, activation diff: 273.684, ratio: 0.052
#   pulse 9: activated nodes: 11442, borderline nodes: 99, overall activation: 5452.918, activation diff: 159.091, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5452.9
#   number of spread. activ. pulses: 9
#   running time: 609

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9978508   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99760425   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9974168   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99448943   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9883151   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7470403   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74699545   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7469507   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.74694157   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7468617   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7468352   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74682796   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.74680877   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7467911   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.7467819   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.7467765   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.74677074   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7467694   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.74676716   REFERENCES:SIMLOC
