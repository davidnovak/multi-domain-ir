###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1360.080, activation diff: 1358.008, ratio: 0.998
#   pulse 3: activated nodes: 9281, borderline nodes: 3717, overall activation: 1812.749, activation diff: 773.150, ratio: 0.427
#   pulse 4: activated nodes: 11312, borderline nodes: 2725, overall activation: 4008.838, activation diff: 2197.286, ratio: 0.548
#   pulse 5: activated nodes: 11434, borderline nodes: 293, overall activation: 5044.491, activation diff: 1035.653, ratio: 0.205
#   pulse 6: activated nodes: 11442, borderline nodes: 133, overall activation: 5456.798, activation diff: 412.307, ratio: 0.076
#   pulse 7: activated nodes: 11445, borderline nodes: 100, overall activation: 5607.768, activation diff: 150.970, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 5607.8
#   number of spread. activ. pulses: 7
#   running time: 550

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99971366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995158   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99950534   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972501   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9930719   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7497903   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74976754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74972427   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7497066   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.749698   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.74968463   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74968046   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.74967617   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.74967545   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.74965835   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7496569   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.74965614   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7496496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.74964863   REFERENCES:SIMLOC
