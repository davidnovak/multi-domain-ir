###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1628.728, activation diff: 1626.656, ratio: 0.999
#   pulse 3: activated nodes: 9281, borderline nodes: 3717, overall activation: 2428.379, activation diff: 1176.385, ratio: 0.484
#   pulse 4: activated nodes: 11313, borderline nodes: 2709, overall activation: 5849.222, activation diff: 3422.694, ratio: 0.585
#   pulse 5: activated nodes: 11435, borderline nodes: 230, overall activation: 7465.598, activation diff: 1616.376, ratio: 0.217
#   pulse 6: activated nodes: 11451, borderline nodes: 40, overall activation: 8068.113, activation diff: 602.515, ratio: 0.075
#   pulse 7: activated nodes: 11454, borderline nodes: 15, overall activation: 8274.316, activation diff: 206.203, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8274.3
#   number of spread. activ. pulses: 7
#   running time: 640

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99971366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99951583   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99950534   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972503   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.993072   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.899749   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89972275   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8996693   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8996493   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89964175   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.89963037   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.89961684   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.8996159   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.89961505   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.8995941   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.89959073   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.89959   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.8995882   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8995817   REFERENCES:SIMLOC
