###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 933.100, activation diff: 913.236, ratio: 0.979
#   pulse 3: activated nodes: 9480, borderline nodes: 3759, overall activation: 2029.119, activation diff: 1096.018, ratio: 0.540
#   pulse 4: activated nodes: 11369, borderline nodes: 1889, overall activation: 4247.070, activation diff: 2217.951, ratio: 0.522
#   pulse 5: activated nodes: 11448, borderline nodes: 79, overall activation: 5957.505, activation diff: 1710.435, ratio: 0.287
#   pulse 6: activated nodes: 11457, borderline nodes: 9, overall activation: 7041.759, activation diff: 1084.254, ratio: 0.154
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 7676.514, activation diff: 634.755, ratio: 0.083
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 8036.514, activation diff: 360.000, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8036.5
#   number of spread. activ. pulses: 8
#   running time: 577

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608874   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99583393   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.995538   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99519646   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99180543   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.984413   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8929285   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8928477   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8927861   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.89278495   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.892665   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89257103   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.892561   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89255154   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8925087   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.8924997   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.89248383   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.8924717   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.89245605   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8924531   REFERENCES:SIMLOC
