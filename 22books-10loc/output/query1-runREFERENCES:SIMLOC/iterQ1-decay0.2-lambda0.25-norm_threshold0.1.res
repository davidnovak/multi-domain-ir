###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1449.629, activation diff: 1447.558, ratio: 0.999
#   pulse 3: activated nodes: 9281, borderline nodes: 3717, overall activation: 2012.510, activation diff: 902.311, ratio: 0.448
#   pulse 4: activated nodes: 11313, borderline nodes: 2720, overall activation: 4590.322, activation diff: 2579.236, ratio: 0.562
#   pulse 5: activated nodes: 11435, borderline nodes: 266, overall activation: 5818.862, activation diff: 1228.541, ratio: 0.211
#   pulse 6: activated nodes: 11449, borderline nodes: 72, overall activation: 6299.007, activation diff: 480.145, ratio: 0.076
#   pulse 7: activated nodes: 11453, borderline nodes: 36, overall activation: 6470.535, activation diff: 171.528, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6470.5
#   number of spread. activ. pulses: 7
#   running time: 622

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99980485   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99971366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995158   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99950534   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99725014   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9930719   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79977655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7997526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.799706   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7996875   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.79967916   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7996663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79965925   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.799656   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7996552   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.799636   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.7996341   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7996341   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.79963005   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7996268   REFERENCES:SIMLOC
