###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 709.728, activation diff: 691.576, ratio: 0.974
#   pulse 3: activated nodes: 9151, borderline nodes: 3655, overall activation: 1449.022, activation diff: 739.294, ratio: 0.510
#   pulse 4: activated nodes: 11298, borderline nodes: 3210, overall activation: 3074.441, activation diff: 1625.419, ratio: 0.529
#   pulse 5: activated nodes: 11423, borderline nodes: 459, overall activation: 4379.181, activation diff: 1304.739, ratio: 0.298
#   pulse 6: activated nodes: 11445, borderline nodes: 167, overall activation: 5258.988, activation diff: 879.807, ratio: 0.167
#   pulse 7: activated nodes: 11449, borderline nodes: 64, overall activation: 5800.107, activation diff: 541.120, ratio: 0.093
#   pulse 8: activated nodes: 11452, borderline nodes: 47, overall activation: 6118.315, activation diff: 318.208, ratio: 0.052
#   pulse 9: activated nodes: 11453, borderline nodes: 34, overall activation: 6301.400, activation diff: 183.084, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6301.4
#   number of spread. activ. pulses: 9
#   running time: 721

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980414   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9978508   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99760425   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99741715   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.994491   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9883187   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79684305   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79679537   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79674745   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7967378   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7966528   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7966248   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79661655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79659665   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.79657793   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.79656744   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.79656184   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.79655653   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.79655546   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7965528   REFERENCES:SIMLOC
