###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 1521.882, activation diff: 1516.070, ratio: 0.996
#   pulse 3: activated nodes: 9480, borderline nodes: 3759, overall activation: 2206.886, activation diff: 821.192, ratio: 0.372
#   pulse 4: activated nodes: 11369, borderline nodes: 1889, overall activation: 4322.276, activation diff: 2115.390, ratio: 0.489
#   pulse 5: activated nodes: 11448, borderline nodes: 79, overall activation: 5262.054, activation diff: 939.777, ratio: 0.179
#   pulse 6: activated nodes: 11448, borderline nodes: 79, overall activation: 5614.205, activation diff: 352.151, ratio: 0.063
#   pulse 7: activated nodes: 11448, borderline nodes: 79, overall activation: 5737.827, activation diff: 123.623, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5737.8
#   number of spread. activ. pulses: 7
#   running time: 578

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998138   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999766   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99966216   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996253   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99757993   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9938876   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7498077   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74979323   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74977374   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7497702   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.749753   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.74975145   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.74974704   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7497438   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7497406   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.74974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.74972796   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7497252   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.7497243   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.749721   REFERENCES:SIMLOC
