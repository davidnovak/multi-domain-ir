###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   historyofuniteds07good_239, 1
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5721, borderline nodes: 5652, overall activation: 831.948, activation diff: 812.083, ratio: 0.976
#   pulse 3: activated nodes: 9480, borderline nodes: 3759, overall activation: 1724.335, activation diff: 892.387, ratio: 0.518
#   pulse 4: activated nodes: 11369, borderline nodes: 1889, overall activation: 3401.536, activation diff: 1677.201, ratio: 0.493
#   pulse 5: activated nodes: 11448, borderline nodes: 79, overall activation: 4687.065, activation diff: 1285.529, ratio: 0.274
#   pulse 6: activated nodes: 11457, borderline nodes: 9, overall activation: 5519.199, activation diff: 832.134, ratio: 0.151
#   pulse 7: activated nodes: 11457, borderline nodes: 9, overall activation: 6017.112, activation diff: 497.913, ratio: 0.083
#   pulse 8: activated nodes: 11457, borderline nodes: 9, overall activation: 6304.592, activation diff: 287.481, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6304.6
#   number of spread. activ. pulses: 8
#   running time: 615

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608874   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9958338   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.995538   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99519587   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.991803   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9844098   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79371405   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7936418   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79358745   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.793586   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7934791   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7933941   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.79338574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7933787   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.79333854   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7933308   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.79331326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.793308   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7932935   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7932892   REFERENCES:SIMLOC
