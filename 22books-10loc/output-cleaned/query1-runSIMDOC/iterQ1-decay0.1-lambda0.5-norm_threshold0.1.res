###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.448, activation diff: 4.371, ratio: 0.983
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 3.192, activation diff: 1.901, ratio: 0.595
#   pulse 3: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.397, activation diff: 1.481, ratio: 0.618
#   pulse 4: activated nodes: 1888, borderline nodes: 1727, overall activation: 2.496, activation diff: 1.504, ratio: 0.602
#   pulse 5: activated nodes: 2052, borderline nodes: 1870, overall activation: 4.454, activation diff: 2.620, ratio: 0.588
#   pulse 6: activated nodes: 2337, borderline nodes: 2114, overall activation: 10.725, activation diff: 6.602, ratio: 0.616
#   pulse 7: activated nodes: 2618, borderline nodes: 2356, overall activation: 25.300, activation diff: 14.740, ratio: 0.583
#   pulse 8: activated nodes: 2836, borderline nodes: 2537, overall activation: 48.007, activation diff: 22.789, ratio: 0.475
#   pulse 9: activated nodes: 3204, borderline nodes: 2835, overall activation: 72.744, activation diff: 24.778, ratio: 0.341
#   pulse 10: activated nodes: 3674, borderline nodes: 3188, overall activation: 96.837, activation diff: 24.114, ratio: 0.249
#   pulse 11: activated nodes: 4199, borderline nodes: 3564, overall activation: 120.326, activation diff: 23.499, ratio: 0.195
#   pulse 12: activated nodes: 4645, borderline nodes: 3852, overall activation: 144.108, activation diff: 23.787, ratio: 0.165
#   pulse 13: activated nodes: 5056, borderline nodes: 4084, overall activation: 170.347, activation diff: 26.242, ratio: 0.154
#   pulse 14: activated nodes: 5480, borderline nodes: 4289, overall activation: 205.176, activation diff: 34.831, ratio: 0.170
#   pulse 15: activated nodes: 5770, borderline nodes: 4302, overall activation: 267.490, activation diff: 62.314, ratio: 0.233
#   pulse 16: activated nodes: 6330, borderline nodes: 4588, overall activation: 359.709, activation diff: 92.220, ratio: 0.256
#   pulse 17: activated nodes: 6711, borderline nodes: 4719, overall activation: 462.528, activation diff: 102.820, ratio: 0.222
#   pulse 18: activated nodes: 7010, borderline nodes: 4615, overall activation: 582.412, activation diff: 119.884, ratio: 0.206
#   pulse 19: activated nodes: 7434, borderline nodes: 4506, overall activation: 742.305, activation diff: 159.892, ratio: 0.215
#   pulse 20: activated nodes: 7748, borderline nodes: 4307, overall activation: 931.732, activation diff: 189.427, ratio: 0.203

###################################
# spreading activation process summary: 
#   final number of activated nodes: 7748
#   final overall activation: 931.7
#   number of spread. activ. pulses: 20
#   running time: 928

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.99995655   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999535   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.9999534   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.99995327   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.99995255   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.9999502   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.99995005   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999485   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.99994767   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9999082   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9998677   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9998531   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.99985003   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   14   0.99976194   SIMDOC
1   Q1   cambridgemodern09protgoog_729   15   0.999761   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.9997231   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.99970317   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9990252   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.9989898   SIMDOC
1   Q1   cambridgemodern09protgoog_731   20   0.9979155   SIMDOC
