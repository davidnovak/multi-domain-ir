###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.448, activation diff: 4.371, ratio: 0.983
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 3.192, activation diff: 1.901, ratio: 0.595
#   pulse 3: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.394, activation diff: 1.478, ratio: 0.617
#   pulse 4: activated nodes: 1888, borderline nodes: 1727, overall activation: 2.453, activation diff: 1.464, ratio: 0.597
#   pulse 5: activated nodes: 2052, borderline nodes: 1870, overall activation: 4.197, activation diff: 2.406, ratio: 0.573
#   pulse 6: activated nodes: 2324, borderline nodes: 2102, overall activation: 9.621, activation diff: 5.755, ratio: 0.598
#   pulse 7: activated nodes: 2618, borderline nodes: 2356, overall activation: 21.884, activation diff: 12.428, ratio: 0.568
#   pulse 8: activated nodes: 2815, borderline nodes: 2521, overall activation: 40.619, activation diff: 18.817, ratio: 0.463
#   pulse 9: activated nodes: 3125, borderline nodes: 2772, overall activation: 60.287, activation diff: 19.709, ratio: 0.327
#   pulse 10: activated nodes: 3425, borderline nodes: 2997, overall activation: 77.869, activation diff: 17.603, ratio: 0.226
#   pulse 11: activated nodes: 3989, borderline nodes: 3431, overall activation: 93.041, activation diff: 15.182, ratio: 0.163
#   pulse 12: activated nodes: 4347, borderline nodes: 3663, overall activation: 106.220, activation diff: 13.185, ratio: 0.124
#   pulse 13: activated nodes: 4609, borderline nodes: 3827, overall activation: 117.884, activation diff: 11.667, ratio: 0.099
#   pulse 14: activated nodes: 4792, borderline nodes: 3914, overall activation: 128.594, activation diff: 10.711, ratio: 0.083
#   pulse 15: activated nodes: 5018, borderline nodes: 4030, overall activation: 139.001, activation diff: 10.408, ratio: 0.075
#   pulse 16: activated nodes: 5196, borderline nodes: 4094, overall activation: 150.022, activation diff: 11.021, ratio: 0.073
#   pulse 17: activated nodes: 5390, borderline nodes: 4154, overall activation: 163.446, activation diff: 13.424, ratio: 0.082
#   pulse 18: activated nodes: 5646, borderline nodes: 4263, overall activation: 182.585, activation diff: 19.139, ratio: 0.105
#   pulse 19: activated nodes: 5841, borderline nodes: 4328, overall activation: 208.359, activation diff: 25.774, ratio: 0.124
#   pulse 20: activated nodes: 6021, borderline nodes: 4384, overall activation: 235.912, activation diff: 27.553, ratio: 0.117

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6021
#   final overall activation: 235.9
#   number of spread. activ. pulses: 20
#   running time: 760

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.999954   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.99995077   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995065   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.99995005   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.9999498   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.9999461   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.9999456   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999448   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.9999441   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9998346   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   11   0.99972063   SIMDOC
1   Q1   cambridgemodern09protgoog_737   12   0.99970996   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.99966466   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.9995402   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   15   0.9994858   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   16   0.99943745   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9993831   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9982449   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.9980681   SIMDOC
1   Q1   cambridgemodern09protgoog_731   20   0.9967666   SIMDOC
