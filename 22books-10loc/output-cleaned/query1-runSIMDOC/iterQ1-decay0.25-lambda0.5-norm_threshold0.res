###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 8.260, activation diff: 8.034, ratio: 0.973
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 16.474, activation diff: 10.239, ratio: 0.622
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 34.337, activation diff: 18.738, ratio: 0.546
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 70.796, activation diff: 36.788, ratio: 0.520
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 134.523, activation diff: 63.802, ratio: 0.474
#   pulse 6: activated nodes: 8967, borderline nodes: 380, overall activation: 233.531, activation diff: 99.024, ratio: 0.424
#   pulse 7: activated nodes: 8967, borderline nodes: 380, overall activation: 380.981, activation diff: 147.450, ratio: 0.387
#   pulse 8: activated nodes: 8967, borderline nodes: 380, overall activation: 581.743, activation diff: 200.762, ratio: 0.345
#   pulse 9: activated nodes: 8967, borderline nodes: 380, overall activation: 814.660, activation diff: 232.916, ratio: 0.286
#   pulse 10: activated nodes: 8967, borderline nodes: 380, overall activation: 1057.984, activation diff: 243.324, ratio: 0.230
#   pulse 11: activated nodes: 8967, borderline nodes: 380, overall activation: 1287.497, activation diff: 229.513, ratio: 0.178
#   pulse 12: activated nodes: 8967, borderline nodes: 380, overall activation: 1494.716, activation diff: 207.220, ratio: 0.139
#   pulse 13: activated nodes: 8967, borderline nodes: 380, overall activation: 1678.006, activation diff: 183.290, ratio: 0.109
#   pulse 14: activated nodes: 8967, borderline nodes: 380, overall activation: 1837.873, activation diff: 159.867, ratio: 0.087
#   pulse 15: activated nodes: 8967, borderline nodes: 380, overall activation: 1976.610, activation diff: 138.737, ratio: 0.070
#   pulse 16: activated nodes: 8967, borderline nodes: 380, overall activation: 2096.583, activation diff: 119.973, ratio: 0.057
#   pulse 17: activated nodes: 8967, borderline nodes: 380, overall activation: 2199.810, activation diff: 103.227, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8967
#   final overall activation: 2199.8
#   number of spread. activ. pulses: 17
#   running time: 1121

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999528   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.9999513   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99995124   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.9999508   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.9999485   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.9999466   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99994624   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99994516   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99994445   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9998658   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99984103   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   12   0.999826   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.99978507   SIMDOC
1   Q1   cambridgemodern09protgoog_418   14   0.9997777   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   15   0.99974424   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   16   0.99974084   SIMDOC
1   Q1   cambridgemodern09protgoog_729   17   0.9996563   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   18   0.9996285   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   19   0.9996227   SIMDOC
1   Q1   generalhistoryfo00myerrich_790   20   0.9996129   SIMDOC
