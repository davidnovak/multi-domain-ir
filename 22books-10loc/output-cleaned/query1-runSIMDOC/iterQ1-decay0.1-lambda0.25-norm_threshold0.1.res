###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.172, activation diff: 6.557, ratio: 1.572
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.241, activation diff: 2.641, ratio: 1.179
#   pulse 3: activated nodes: 1869, borderline nodes: 1710, overall activation: 2.130, activation diff: 1.892, ratio: 0.888
#   pulse 4: activated nodes: 2031, borderline nodes: 1850, overall activation: 5.289, activation diff: 3.686, ratio: 0.697
#   pulse 5: activated nodes: 2343, borderline nodes: 2115, overall activation: 17.722, activation diff: 12.565, ratio: 0.709
#   pulse 6: activated nodes: 2685, borderline nodes: 2407, overall activation: 48.312, activation diff: 30.623, ratio: 0.634
#   pulse 7: activated nodes: 3056, borderline nodes: 2712, overall activation: 86.761, activation diff: 38.457, ratio: 0.443
#   pulse 8: activated nodes: 3680, borderline nodes: 3191, overall activation: 122.034, activation diff: 35.275, ratio: 0.289
#   pulse 9: activated nodes: 4455, borderline nodes: 3733, overall activation: 156.540, activation diff: 34.507, ratio: 0.220
#   pulse 10: activated nodes: 5114, borderline nodes: 4137, overall activation: 196.722, activation diff: 40.182, ratio: 0.204
#   pulse 11: activated nodes: 5605, borderline nodes: 4318, overall activation: 263.498, activation diff: 66.776, ratio: 0.253
#   pulse 12: activated nodes: 6151, borderline nodes: 4479, overall activation: 399.892, activation diff: 136.393, ratio: 0.341
#   pulse 13: activated nodes: 6663, borderline nodes: 4654, overall activation: 564.062, activation diff: 164.171, ratio: 0.291
#   pulse 14: activated nodes: 7251, borderline nodes: 4724, overall activation: 759.553, activation diff: 195.491, ratio: 0.257
#   pulse 15: activated nodes: 7684, borderline nodes: 4347, overall activation: 1047.452, activation diff: 287.899, ratio: 0.275
#   pulse 16: activated nodes: 8047, borderline nodes: 3914, overall activation: 1407.139, activation diff: 359.687, ratio: 0.256
#   pulse 17: activated nodes: 8414, borderline nodes: 3309, overall activation: 1867.936, activation diff: 460.797, ratio: 0.247
#   pulse 18: activated nodes: 8643, borderline nodes: 2647, overall activation: 2444.055, activation diff: 576.119, ratio: 0.236
#   pulse 19: activated nodes: 8765, borderline nodes: 1935, overall activation: 3059.754, activation diff: 615.699, ratio: 0.201
#   pulse 20: activated nodes: 8862, borderline nodes: 1393, overall activation: 3617.381, activation diff: 557.627, ratio: 0.154

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8862
#   final overall activation: 3617.4
#   number of spread. activ. pulses: 20
#   running time: 1438

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_730   2   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_749   4   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   5   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_743   7   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9999825   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9999759   SIMDOC
1   Q1   cambridgemodern09protgoog_418   12   0.9999577   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.9999453   SIMDOC
1   Q1   historyofuniteds07good_71   14   0.9999413   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.9999025   SIMDOC
1   Q1   cambridgemodern09protgoog_729   16   0.99988204   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   17   0.99984884   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   18   0.99982995   SIMDOC
1   Q1   cambridgemodern09protgoog_744   19   0.9992796   SIMDOC
1   Q1   cambridgemodern09protgoog_747   20   0.999262   SIMDOC
