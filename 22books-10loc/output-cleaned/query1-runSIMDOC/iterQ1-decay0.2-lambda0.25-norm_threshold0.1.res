###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.172, activation diff: 6.557, ratio: 1.572
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.240, activation diff: 2.640, ratio: 1.179
#   pulse 3: activated nodes: 1869, borderline nodes: 1710, overall activation: 2.102, activation diff: 1.865, ratio: 0.887
#   pulse 4: activated nodes: 2015, borderline nodes: 1835, overall activation: 5.048, activation diff: 3.472, ratio: 0.688
#   pulse 5: activated nodes: 2340, borderline nodes: 2113, overall activation: 16.326, activation diff: 11.410, ratio: 0.699
#   pulse 6: activated nodes: 2651, borderline nodes: 2376, overall activation: 43.441, activation diff: 27.147, ratio: 0.625
#   pulse 7: activated nodes: 3021, borderline nodes: 2687, overall activation: 76.514, activation diff: 33.081, ratio: 0.432
#   pulse 8: activated nodes: 3499, borderline nodes: 3048, overall activation: 103.897, activation diff: 27.385, ratio: 0.264
#   pulse 9: activated nodes: 4288, borderline nodes: 3626, overall activation: 126.907, activation diff: 23.011, ratio: 0.181
#   pulse 10: activated nodes: 4804, borderline nodes: 3960, overall activation: 148.212, activation diff: 21.305, ratio: 0.144
#   pulse 11: activated nodes: 5149, borderline nodes: 4112, overall activation: 171.585, activation diff: 23.373, ratio: 0.136
#   pulse 12: activated nodes: 5536, borderline nodes: 4262, overall activation: 206.155, activation diff: 34.570, ratio: 0.168
#   pulse 13: activated nodes: 5843, borderline nodes: 4306, overall activation: 269.165, activation diff: 63.010, ratio: 0.234
#   pulse 14: activated nodes: 6238, borderline nodes: 4473, overall activation: 343.153, activation diff: 73.988, ratio: 0.216
#   pulse 15: activated nodes: 6659, borderline nodes: 4631, overall activation: 414.764, activation diff: 71.611, ratio: 0.173
#   pulse 16: activated nodes: 6992, borderline nodes: 4659, overall activation: 486.554, activation diff: 71.790, ratio: 0.148
#   pulse 17: activated nodes: 7298, borderline nodes: 4542, overall activation: 563.558, activation diff: 77.003, ratio: 0.137
#   pulse 18: activated nodes: 7501, borderline nodes: 4353, overall activation: 661.131, activation diff: 97.573, ratio: 0.148
#   pulse 19: activated nodes: 7754, borderline nodes: 4163, overall activation: 784.235, activation diff: 123.104, ratio: 0.157
#   pulse 20: activated nodes: 8015, borderline nodes: 3929, overall activation: 930.938, activation diff: 146.704, ratio: 0.158

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8015
#   final overall activation: 930.9
#   number of spread. activ. pulses: 20
#   running time: 1161

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   1.0   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.9999998   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99999976   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999934   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99995494   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99993396   SIMDOC
1   Q1   cambridgemodern09protgoog_418   12   0.99989057   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   13   0.999884   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.99977714   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.9997617   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.99972343   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9996627   SIMDOC
1   Q1   historyofuniteds07good_71   18   0.99918145   SIMDOC
1   Q1   cambridgemodern09protgoog_744   19   0.9988059   SIMDOC
1   Q1   cambridgemodern09protgoog_747   20   0.99877644   SIMDOC
