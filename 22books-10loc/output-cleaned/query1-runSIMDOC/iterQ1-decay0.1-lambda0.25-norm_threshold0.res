###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 9.890, activation diff: 12.051, ratio: 1.218
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 28.506, activation diff: 20.736, ratio: 0.727
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 83.164, activation diff: 55.029, ratio: 0.662
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 223.448, activation diff: 140.307, ratio: 0.628
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 524.406, activation diff: 300.958, ratio: 0.574
#   pulse 6: activated nodes: 8978, borderline nodes: 11, overall activation: 1095.691, activation diff: 571.285, ratio: 0.521
#   pulse 7: activated nodes: 8978, borderline nodes: 0, overall activation: 1892.300, activation diff: 796.610, ratio: 0.421
#   pulse 8: activated nodes: 8978, borderline nodes: 0, overall activation: 2764.455, activation diff: 872.155, ratio: 0.315
#   pulse 9: activated nodes: 8978, borderline nodes: 0, overall activation: 3700.785, activation diff: 936.330, ratio: 0.253
#   pulse 10: activated nodes: 8978, borderline nodes: 0, overall activation: 4463.636, activation diff: 762.851, ratio: 0.171
#   pulse 11: activated nodes: 8978, borderline nodes: 0, overall activation: 4899.148, activation diff: 435.511, ratio: 0.089
#   pulse 12: activated nodes: 8978, borderline nodes: 0, overall activation: 5122.098, activation diff: 222.950, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 5122.1
#   number of spread. activ. pulses: 12
#   running time: 3210

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999986   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.99999857   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.99999857   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.99999857   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.99999845   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.99999833   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999833   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999982   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999981   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.999982   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   11   0.9999803   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   12   0.9999779   SIMDOC
1   Q1   cambridgemodern09protgoog_737   13   0.9999774   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   14   0.9999753   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   15   0.9999661   SIMDOC
1   Q1   generalhistoryfo00myerrich_791   16   0.9999633   SIMDOC
1   Q1   cambridgemodern09protgoog_418   17   0.99996156   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   18   0.9999612   SIMDOC
1   Q1   bookman44unkngoog_753   19   0.99995935   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   20   0.99995536   SIMDOC
