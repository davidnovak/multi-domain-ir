###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 8.260, activation diff: 8.034, ratio: 0.973
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 17.634, activation diff: 11.399, ratio: 0.646
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 40.131, activation diff: 23.364, ratio: 0.582
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 90.939, activation diff: 51.119, ratio: 0.562
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 192.358, activation diff: 101.478, ratio: 0.528
#   pulse 6: activated nodes: 8978, borderline nodes: 11, overall activation: 382.014, activation diff: 189.667, ratio: 0.496
#   pulse 7: activated nodes: 8978, borderline nodes: 0, overall activation: 718.583, activation diff: 336.569, ratio: 0.468
#   pulse 8: activated nodes: 8978, borderline nodes: 0, overall activation: 1198.626, activation diff: 480.044, ratio: 0.400
#   pulse 9: activated nodes: 8978, borderline nodes: 0, overall activation: 1750.945, activation diff: 552.319, ratio: 0.315
#   pulse 10: activated nodes: 8978, borderline nodes: 0, overall activation: 2383.748, activation diff: 632.802, ratio: 0.265
#   pulse 11: activated nodes: 8978, borderline nodes: 0, overall activation: 3076.908, activation diff: 693.160, ratio: 0.225
#   pulse 12: activated nodes: 8978, borderline nodes: 0, overall activation: 3725.816, activation diff: 648.908, ratio: 0.174
#   pulse 13: activated nodes: 8978, borderline nodes: 0, overall activation: 4233.270, activation diff: 507.454, ratio: 0.120
#   pulse 14: activated nodes: 8978, borderline nodes: 0, overall activation: 4593.561, activation diff: 360.291, ratio: 0.078
#   pulse 15: activated nodes: 8978, borderline nodes: 0, overall activation: 4841.532, activation diff: 247.971, ratio: 0.051
#   pulse 16: activated nodes: 8978, borderline nodes: 0, overall activation: 5010.480, activation diff: 168.948, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 5010.5
#   number of spread. activ. pulses: 16
#   running time: 1278

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999094   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.99990666   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.99990624   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.9999057   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.9999017   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.9998985   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9998975   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99989617   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9998955   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9998467   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99982864   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9998058   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9998051   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   14   0.99978614   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   15   0.99974877   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   16   0.9997473   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   17   0.9997442   SIMDOC
1   Q1   cambridgemodern09protgoog_729   18   0.99973285   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   19   0.9997195   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   20   0.9996925   SIMDOC
