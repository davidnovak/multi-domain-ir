###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.448, activation diff: 4.371, ratio: 0.983
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 3.192, activation diff: 1.901, ratio: 0.595
#   pulse 3: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.395, activation diff: 1.479, ratio: 0.617
#   pulse 4: activated nodes: 1888, borderline nodes: 1727, overall activation: 2.467, activation diff: 1.477, ratio: 0.599
#   pulse 5: activated nodes: 2052, borderline nodes: 1870, overall activation: 4.281, activation diff: 2.476, ratio: 0.578
#   pulse 6: activated nodes: 2337, borderline nodes: 2114, overall activation: 9.981, activation diff: 6.030, ratio: 0.604
#   pulse 7: activated nodes: 2618, borderline nodes: 2356, overall activation: 22.987, activation diff: 13.171, ratio: 0.573
#   pulse 8: activated nodes: 2825, borderline nodes: 2529, overall activation: 42.987, activation diff: 20.083, ratio: 0.467
#   pulse 9: activated nodes: 3144, borderline nodes: 2786, overall activation: 64.213, activation diff: 21.267, ratio: 0.331
#   pulse 10: activated nodes: 3530, borderline nodes: 3079, overall activation: 83.706, activation diff: 19.513, ratio: 0.233
#   pulse 11: activated nodes: 4051, borderline nodes: 3469, overall activation: 101.200, activation diff: 17.505, ratio: 0.173
#   pulse 12: activated nodes: 4414, borderline nodes: 3699, overall activation: 117.119, activation diff: 15.924, ratio: 0.136
#   pulse 13: activated nodes: 4752, borderline nodes: 3918, overall activation: 132.108, activation diff: 14.992, ratio: 0.113
#   pulse 14: activated nodes: 4997, borderline nodes: 4034, overall activation: 147.123, activation diff: 15.016, ratio: 0.102
#   pulse 15: activated nodes: 5257, borderline nodes: 4144, overall activation: 163.893, activation diff: 16.771, ratio: 0.102
#   pulse 16: activated nodes: 5534, borderline nodes: 4236, overall activation: 186.779, activation diff: 22.886, ratio: 0.123
#   pulse 17: activated nodes: 5785, borderline nodes: 4298, overall activation: 223.455, activation diff: 36.676, ratio: 0.164
#   pulse 18: activated nodes: 6088, borderline nodes: 4430, overall activation: 268.940, activation diff: 45.484, ratio: 0.169
#   pulse 19: activated nodes: 6363, borderline nodes: 4551, overall activation: 315.587, activation diff: 46.647, ratio: 0.148
#   pulse 20: activated nodes: 6634, borderline nodes: 4634, overall activation: 362.941, activation diff: 47.355, ratio: 0.130

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6634
#   final overall activation: 362.9
#   number of spread. activ. pulses: 20
#   running time: 824

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.9999549   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999517   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.9999516   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   4   0.99995124   SIMDOC
1   Q1   cambridgemodern09protgoog_748   5   0.99995077   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   6   0.99994767   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.99994737   SIMDOC
1   Q1   cambridgemodern09protgoog_730   8   0.9999462   SIMDOC
1   Q1   cambridgemodern09protgoog_749   9   0.9999454   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9998691   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.9997859   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9997782   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9997516   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.99963397   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   15   0.9995825   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   16   0.9995824   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9995191   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9985508   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.99844223   SIMDOC
1   Q1   cambridgemodern09protgoog_731   20   0.99719906   SIMDOC
