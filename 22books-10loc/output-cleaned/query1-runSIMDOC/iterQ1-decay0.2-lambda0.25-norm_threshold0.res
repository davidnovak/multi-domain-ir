###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 9.890, activation diff: 12.051, ratio: 1.218
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 26.767, activation diff: 18.997, ratio: 0.710
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 72.670, activation diff: 46.292, ratio: 0.637
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 180.080, activation diff: 107.436, ratio: 0.597
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 382.184, activation diff: 202.104, ratio: 0.529
#   pulse 6: activated nodes: 8978, borderline nodes: 11, overall activation: 726.792, activation diff: 344.609, ratio: 0.474
#   pulse 7: activated nodes: 8978, borderline nodes: 11, overall activation: 1187.006, activation diff: 460.214, ratio: 0.388
#   pulse 8: activated nodes: 8978, borderline nodes: 11, overall activation: 1665.956, activation diff: 478.950, ratio: 0.287
#   pulse 9: activated nodes: 8978, borderline nodes: 11, overall activation: 2099.263, activation diff: 433.306, ratio: 0.206
#   pulse 10: activated nodes: 8978, borderline nodes: 11, overall activation: 2490.746, activation diff: 391.483, ratio: 0.157
#   pulse 11: activated nodes: 8978, borderline nodes: 11, overall activation: 2837.393, activation diff: 346.647, ratio: 0.122
#   pulse 12: activated nodes: 8978, borderline nodes: 11, overall activation: 3111.371, activation diff: 273.978, ratio: 0.088
#   pulse 13: activated nodes: 8978, borderline nodes: 11, overall activation: 3290.829, activation diff: 179.458, ratio: 0.055
#   pulse 14: activated nodes: 8978, borderline nodes: 11, overall activation: 3396.933, activation diff: 106.104, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 3396.9
#   number of spread. activ. pulses: 14
#   running time: 1382

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.9999998   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.99999976   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.9999996   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   10   0.9999961   SIMDOC
1   Q1   bookman44unkngoog_753   11   0.9999957   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   12   0.99998665   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   13   0.99998343   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   14   0.9999659   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   15   0.9999647   SIMDOC
1   Q1   cambridgemodern09protgoog_737   16   0.9999583   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   17   0.9999342   SIMDOC
1   Q1   generalhistoryfo00myerrich_791   18   0.99992514   SIMDOC
1   Q1   cambridgemodern09protgoog_418   19   0.99992406   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   20   0.9999238   SIMDOC
