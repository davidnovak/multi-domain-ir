###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 8.260, activation diff: 8.034, ratio: 0.973
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 16.860, activation diff: 10.625, ratio: 0.630
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 36.203, activation diff: 20.216, ratio: 0.558
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 77.025, activation diff: 41.145, ratio: 0.534
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 151.528, activation diff: 74.572, ratio: 0.492
#   pulse 6: activated nodes: 8978, borderline nodes: 11, overall activation: 274.471, activation diff: 122.958, ratio: 0.448
#   pulse 7: activated nodes: 8978, borderline nodes: 11, overall activation: 469.538, activation diff: 195.067, ratio: 0.415
#   pulse 8: activated nodes: 8978, borderline nodes: 11, overall activation: 742.240, activation diff: 272.702, ratio: 0.367
#   pulse 9: activated nodes: 8978, borderline nodes: 11, overall activation: 1064.469, activation diff: 322.228, ratio: 0.303
#   pulse 10: activated nodes: 8978, borderline nodes: 11, overall activation: 1390.727, activation diff: 326.259, ratio: 0.235
#   pulse 11: activated nodes: 8978, borderline nodes: 11, overall activation: 1709.729, activation diff: 319.002, ratio: 0.187
#   pulse 12: activated nodes: 8978, borderline nodes: 11, overall activation: 2016.621, activation diff: 306.892, ratio: 0.152
#   pulse 13: activated nodes: 8978, borderline nodes: 11, overall activation: 2307.447, activation diff: 290.826, ratio: 0.126
#   pulse 14: activated nodes: 8978, borderline nodes: 11, overall activation: 2577.742, activation diff: 270.295, ratio: 0.105
#   pulse 15: activated nodes: 8978, borderline nodes: 11, overall activation: 2815.783, activation diff: 238.040, ratio: 0.085
#   pulse 16: activated nodes: 8978, borderline nodes: 11, overall activation: 3009.447, activation diff: 193.664, ratio: 0.064
#   pulse 17: activated nodes: 8978, borderline nodes: 11, overall activation: 3157.552, activation diff: 148.106, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 3157.6
#   number of spread. activ. pulses: 17
#   running time: 1124

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   0.99995345   SIMDOC
1   Q1   cambridgemodern09protgoog_748   2   0.999952   SIMDOC
1   Q1   cambridgemodern09protgoog_743   3   0.9999519   SIMDOC
1   Q1   cambridgemodern09protgoog_750   4   0.99995154   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   5   0.99994934   SIMDOC
1   Q1   cambridgemodern09protgoog_730   6   0.99994755   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.9999472   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999463   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99994576   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.99988997   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99987257   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   12   0.9998528   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9998308   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   14   0.9998282   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   15   0.9997984   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   16   0.9997963   SIMDOC
1   Q1   cambridgemodern09protgoog_729   17   0.99972653   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   18   0.99972284   SIMDOC
1   Q1   generalhistoryfo00myerrich_791   19   0.99970794   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   20   0.99970245   SIMDOC
