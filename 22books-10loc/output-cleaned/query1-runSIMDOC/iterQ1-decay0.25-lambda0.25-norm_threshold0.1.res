###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 4.172, activation diff: 6.557, ratio: 1.572
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 2.239, activation diff: 2.640, ratio: 1.179
#   pulse 3: activated nodes: 1869, borderline nodes: 1710, overall activation: 2.089, activation diff: 1.851, ratio: 0.886
#   pulse 4: activated nodes: 2015, borderline nodes: 1835, overall activation: 4.930, activation diff: 3.367, ratio: 0.683
#   pulse 5: activated nodes: 2340, borderline nodes: 2113, overall activation: 15.654, activation diff: 10.855, ratio: 0.693
#   pulse 6: activated nodes: 2651, borderline nodes: 2376, overall activation: 41.129, activation diff: 25.507, ratio: 0.620
#   pulse 7: activated nodes: 3019, borderline nodes: 2687, overall activation: 71.795, activation diff: 30.675, ratio: 0.427
#   pulse 8: activated nodes: 3453, borderline nodes: 3020, overall activation: 95.967, activation diff: 24.174, ratio: 0.252
#   pulse 9: activated nodes: 4168, borderline nodes: 3540, overall activation: 114.836, activation diff: 18.869, ratio: 0.164
#   pulse 10: activated nodes: 4629, borderline nodes: 3836, overall activation: 130.627, activation diff: 15.792, ratio: 0.121
#   pulse 11: activated nodes: 4978, borderline nodes: 4029, overall activation: 145.514, activation diff: 14.887, ratio: 0.102
#   pulse 12: activated nodes: 5226, borderline nodes: 4113, overall activation: 162.252, activation diff: 16.738, ratio: 0.103
#   pulse 13: activated nodes: 5503, borderline nodes: 4199, overall activation: 186.613, activation diff: 24.361, ratio: 0.131
#   pulse 14: activated nodes: 5806, borderline nodes: 4307, overall activation: 225.769, activation diff: 39.156, ratio: 0.173
#   pulse 15: activated nodes: 6047, borderline nodes: 4376, overall activation: 269.220, activation diff: 43.451, ratio: 0.161
#   pulse 16: activated nodes: 6329, borderline nodes: 4512, overall activation: 309.533, activation diff: 40.313, ratio: 0.130
#   pulse 17: activated nodes: 6566, borderline nodes: 4579, overall activation: 346.634, activation diff: 37.100, ratio: 0.107
#   pulse 18: activated nodes: 6820, borderline nodes: 4638, overall activation: 380.965, activation diff: 34.331, ratio: 0.090
#   pulse 19: activated nodes: 7025, borderline nodes: 4649, overall activation: 413.789, activation diff: 32.824, ratio: 0.079
#   pulse 20: activated nodes: 7163, borderline nodes: 4596, overall activation: 448.524, activation diff: 34.735, ratio: 0.077

###################################
# spreading activation process summary: 
#   final number of activated nodes: 7163
#   final overall activation: 448.5
#   number of spread. activ. pulses: 20
#   running time: 1013

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_144   1   1.0   SIMDOC
1   Q1   cambridgemodern09protgoog_750   2   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.99999976   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999964   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999995   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999887   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   10   0.9999274   SIMDOC
1   Q1   cambridgemodern09protgoog_737   11   0.99989057   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   12   0.9998318   SIMDOC
1   Q1   cambridgemodern09protgoog_418   13   0.9998206   SIMDOC
1   Q1   cambridgemodern09protgoog_729   14   0.99969524   SIMDOC
1   Q1   encyclopediaame28unkngoog_466   15   0.99963564   SIMDOC
1   Q1   encyclopediaame28unkngoog_777   16   0.99963415   SIMDOC
1   Q1   encyclopediaame28unkngoog_779   17   0.9995337   SIMDOC
1   Q1   cambridgemodern09protgoog_744   18   0.9985084   SIMDOC
1   Q1   cambridgemodern09protgoog_747   19   0.9984248   SIMDOC
1   Q1   cambridgemodern09protgoog_742   20   0.997326   SIMDOC
