###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 9.890, activation diff: 12.051, ratio: 1.218
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 25.897, activation diff: 18.127, ratio: 0.700
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 67.746, activation diff: 42.247, ratio: 0.624
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 161.210, activation diff: 93.492, ratio: 0.580
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 325.445, activation diff: 164.235, ratio: 0.505
#   pulse 6: activated nodes: 8967, borderline nodes: 380, overall activation: 588.316, activation diff: 262.871, ratio: 0.447
#   pulse 7: activated nodes: 8967, borderline nodes: 380, overall activation: 929.406, activation diff: 341.090, ratio: 0.367
#   pulse 8: activated nodes: 8967, borderline nodes: 380, overall activation: 1277.979, activation diff: 348.573, ratio: 0.273
#   pulse 9: activated nodes: 8967, borderline nodes: 380, overall activation: 1583.623, activation diff: 305.643, ratio: 0.193
#   pulse 10: activated nodes: 8967, borderline nodes: 380, overall activation: 1830.679, activation diff: 247.056, ratio: 0.135
#   pulse 11: activated nodes: 8967, borderline nodes: 380, overall activation: 2027.990, activation diff: 197.311, ratio: 0.097
#   pulse 12: activated nodes: 8967, borderline nodes: 380, overall activation: 2186.517, activation diff: 158.528, ratio: 0.073
#   pulse 13: activated nodes: 8967, borderline nodes: 380, overall activation: 2313.271, activation diff: 126.754, ratio: 0.055
#   pulse 14: activated nodes: 8967, borderline nodes: 380, overall activation: 2412.445, activation diff: 99.174, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8967
#   final overall activation: 2412.4
#   number of spread. activ. pulses: 14
#   running time: 1100

###################################
# top k results in TREC format: 

1   Q1   cambridgemodern09protgoog_750   1   0.9999999   SIMDOC
1   Q1   englishcyclopae05kniggoog_144   2   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_748   3   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_743   4   0.9999999   SIMDOC
1   Q1   cambridgemodern09protgoog_730   5   0.9999998   SIMDOC
1   Q1   englishcyclopae05kniggoog_145   6   0.99999976   SIMDOC
1   Q1   cambridgemodern09protgoog_749   7   0.99999976   SIMDOC
1   Q1   englishcyclopae05kniggoog_146   8   0.9999996   SIMDOC
1   Q1   englishcyclopae05kniggoog_147   9   0.99999917   SIMDOC
1   Q1   bookman44unkngoog_753   10   0.99999356   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_186   11   0.9999927   SIMDOC
1   Q1   ouroldworldbackg00bearrich_424   12   0.999973   SIMDOC
1   Q1   shorthistoryofmo00haslrich_101   13   0.99996704   SIMDOC
1   Q1   generalhistoryfo00myerrich_788   14   0.99994856   SIMDOC
1   Q1   englishcyclopae05kniggoog_134   15   0.99994457   SIMDOC
1   Q1   cambridgemodern09protgoog_737   16   0.9999343   SIMDOC
1   Q1   ouroldworldbackg00bearrich_385   17   0.9998932   SIMDOC
1   Q1   ouroldworldbackg00bearrich_384   18   0.99988437   SIMDOC
1   Q1   cambridgemodern09protgoog_418   19   0.99988097   SIMDOC
1   Q1   encyclopediaame28unkngoog_778   20   0.99987197   SIMDOC
