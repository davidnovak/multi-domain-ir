###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.347, activation diff: 19.121, ratio: 0.988
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 663.956, activation diff: 644.609, ratio: 0.971
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 2267.744, activation diff: 1603.788, ratio: 0.707
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 4636.678, activation diff: 2368.934, ratio: 0.511
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 6536.130, activation diff: 1899.452, ratio: 0.291
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 7788.857, activation diff: 1252.726, ratio: 0.161
#   pulse 7: activated nodes: 13070, borderline nodes: 0, overall activation: 8537.842, activation diff: 748.985, ratio: 0.088
#   pulse 8: activated nodes: 13070, borderline nodes: 0, overall activation: 8972.959, activation diff: 435.118, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 8973.0
#   number of spread. activ. pulses: 8
#   running time: 1311

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99610156   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99607766   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9958118   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.995558   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99257964   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9923606   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   7   0.99234736   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   8   0.9917139   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.99170667   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.99163485   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   11   0.9916034   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.9915894   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   13   0.99156106   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   14   0.99154556   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.9914734   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.9911648   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.99097383   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99089855   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.99088   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.9907638   REFERENCES:SIMDOC
