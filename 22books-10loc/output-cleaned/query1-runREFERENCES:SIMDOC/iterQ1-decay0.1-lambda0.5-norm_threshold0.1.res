###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 14.780, activation diff: 14.703, ratio: 0.995
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 542.689, activation diff: 528.023, ratio: 0.973
#   pulse 3: activated nodes: 11492, borderline nodes: 7256, overall activation: 1660.596, activation diff: 1117.907, ratio: 0.673
#   pulse 4: activated nodes: 12534, borderline nodes: 3618, overall activation: 3918.674, activation diff: 2258.078, ratio: 0.576
#   pulse 5: activated nodes: 12969, borderline nodes: 1734, overall activation: 5917.732, activation diff: 1999.057, ratio: 0.338
#   pulse 6: activated nodes: 13019, borderline nodes: 404, overall activation: 7300.102, activation diff: 1382.370, ratio: 0.189
#   pulse 7: activated nodes: 13040, borderline nodes: 186, overall activation: 8178.494, activation diff: 878.392, ratio: 0.107
#   pulse 8: activated nodes: 13045, borderline nodes: 136, overall activation: 8695.416, activation diff: 516.923, ratio: 0.059
#   pulse 9: activated nodes: 13046, borderline nodes: 99, overall activation: 8990.051, activation diff: 294.635, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13046
#   final overall activation: 8990.1
#   number of spread. activ. pulses: 9
#   running time: 1370

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99787307   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99779797   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9977515   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9974833   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9960944   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9960329   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.99562985   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99557465   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.9955682   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9955579   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.9955206   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.9955014   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.99549925   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9954795   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.9951464   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.99511707   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   17   0.9950515   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99505067   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.9949828   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   20   0.9949587   REFERENCES:SIMDOC
