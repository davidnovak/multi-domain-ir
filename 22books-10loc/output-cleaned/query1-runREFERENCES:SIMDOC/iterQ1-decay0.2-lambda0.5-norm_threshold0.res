###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.347, activation diff: 19.121, ratio: 0.988
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 596.615, activation diff: 577.268, ratio: 0.968
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 1899.058, activation diff: 1302.443, ratio: 0.686
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 3740.052, activation diff: 1840.994, ratio: 0.492
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 5188.846, activation diff: 1448.794, ratio: 0.279
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 6140.796, activation diff: 951.950, ratio: 0.155
#   pulse 7: activated nodes: 13070, borderline nodes: 0, overall activation: 6722.371, activation diff: 581.575, ratio: 0.087
#   pulse 8: activated nodes: 13070, borderline nodes: 0, overall activation: 7060.996, activation diff: 338.625, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 7061.0
#   number of spread. activ. pulses: 8
#   running time: 1345

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9961015   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99607766   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9958118   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99555796   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9925768   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9923605   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   7   0.9923358   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   8   0.9917104   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.9917028   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9916293   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   11   0.9915921   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.9915868   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   13   0.99155486   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   14   0.99153614   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.99146765   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.99115884   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.99095047   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99088836   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.99085534   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.9907428   REFERENCES:SIMDOC
