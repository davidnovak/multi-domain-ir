###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.347, activation diff: 19.121, ratio: 0.988
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 562.945, activation diff: 543.598, ratio: 0.966
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 1724.401, activation diff: 1161.456, ratio: 0.674
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 3320.436, activation diff: 1596.035, ratio: 0.481
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 4555.500, activation diff: 1235.064, ratio: 0.271
#   pulse 6: activated nodes: 13070, borderline nodes: 51, overall activation: 5360.502, activation diff: 805.002, ratio: 0.150
#   pulse 7: activated nodes: 13070, borderline nodes: 51, overall activation: 5853.700, activation diff: 493.198, ratio: 0.084
#   pulse 8: activated nodes: 13070, borderline nodes: 51, overall activation: 6144.093, activation diff: 290.393, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 6144.1
#   number of spread. activ. pulses: 8
#   running time: 1310

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9961015   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9960776   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9958118   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9955579   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99257517   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9923604   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   7   0.9923291   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   8   0.9917084   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.9917004   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.99162596   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   11   0.99158543   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.991585   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   13   0.99155116   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   14   0.9915306   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.991464   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.99115515   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.9909364   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.9908822   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9908411   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.9907301   REFERENCES:SIMDOC
