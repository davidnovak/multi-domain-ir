###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 14.780, activation diff: 14.703, ratio: 0.995
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 460.148, activation diff: 445.483, ratio: 0.968
#   pulse 3: activated nodes: 11492, borderline nodes: 7256, overall activation: 1284.914, activation diff: 824.766, ratio: 0.642
#   pulse 4: activated nodes: 12430, borderline nodes: 3933, overall activation: 2840.005, activation diff: 1555.090, ratio: 0.548
#   pulse 5: activated nodes: 12956, borderline nodes: 1986, overall activation: 4141.174, activation diff: 1301.169, ratio: 0.314
#   pulse 6: activated nodes: 13002, borderline nodes: 721, overall activation: 5030.887, activation diff: 889.713, ratio: 0.177
#   pulse 7: activated nodes: 13022, borderline nodes: 390, overall activation: 5589.253, activation diff: 558.366, ratio: 0.100
#   pulse 8: activated nodes: 13037, borderline nodes: 316, overall activation: 5928.420, activation diff: 339.168, ratio: 0.057
#   pulse 9: activated nodes: 13038, borderline nodes: 286, overall activation: 6128.832, activation diff: 200.411, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13038
#   final overall activation: 6128.8
#   number of spread. activ. pulses: 9
#   running time: 1372

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99787295   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99779785   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9977515   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99748325   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99608696   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9960315   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.9956193   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99556196   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.9955585   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.99554014   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.99550426   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   12   0.9954846   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   13   0.9954786   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9954547   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.99512374   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.99507606   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   17   0.9950313   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99502516   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.9949325   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   20   0.9949194   REFERENCES:SIMDOC
