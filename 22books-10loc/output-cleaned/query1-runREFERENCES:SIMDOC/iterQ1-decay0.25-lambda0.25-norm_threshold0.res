###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 26.520, activation diff: 28.681, ratio: 1.081
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 1130.934, activation diff: 1109.399, ratio: 0.981
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 3137.224, activation diff: 2006.291, ratio: 0.640
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 5034.367, activation diff: 1897.143, ratio: 0.377
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 5946.844, activation diff: 912.477, ratio: 0.153
#   pulse 6: activated nodes: 13070, borderline nodes: 51, overall activation: 6315.437, activation diff: 368.592, ratio: 0.058
#   pulse 7: activated nodes: 13070, borderline nodes: 51, overall activation: 6452.508, activation diff: 137.072, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 6452.5
#   number of spread. activ. pulses: 7
#   running time: 1293

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9998287   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99982196   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9998108   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99981046   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99978524   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9997693   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   7   0.99975204   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   8   0.9997512   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   9   0.9997486   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   10   0.9997481   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   11   0.9997464   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   12   0.99974597   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   13   0.9997457   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.99974227   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.99972737   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9997167   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   17   0.99971366   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.9997131   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.9997083   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.99970084   REFERENCES:SIMDOC
