###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.670, activation diff: 22.055, ratio: 1.121
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 977.964, activation diff: 968.364, ratio: 0.990
#   pulse 3: activated nodes: 11507, borderline nodes: 7246, overall activation: 2398.641, activation diff: 1422.553, ratio: 0.593
#   pulse 4: activated nodes: 12771, borderline nodes: 2621, overall activation: 4498.941, activation diff: 2100.311, ratio: 0.467
#   pulse 5: activated nodes: 12987, borderline nodes: 1327, overall activation: 5627.482, activation diff: 1128.540, ratio: 0.201
#   pulse 6: activated nodes: 13024, borderline nodes: 319, overall activation: 6104.071, activation diff: 476.589, ratio: 0.078
#   pulse 7: activated nodes: 13039, borderline nodes: 217, overall activation: 6299.549, activation diff: 195.478, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13039
#   final overall activation: 6299.5
#   number of spread. activ. pulses: 7
#   running time: 1298

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99980646   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997896   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9997879   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9997737   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9997687   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9997586   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.99973404   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99972975   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.99972844   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9997276   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.99972516   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   12   0.9997233   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   13   0.9997228   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9997175   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.99968946   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9996811   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   17   0.9996748   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   18   0.9996706   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9996606   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.9996586   REFERENCES:SIMDOC
