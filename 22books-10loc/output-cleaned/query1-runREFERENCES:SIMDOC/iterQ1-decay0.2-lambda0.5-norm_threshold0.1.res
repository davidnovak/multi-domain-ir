###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 14.780, activation diff: 14.703, ratio: 0.995
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 487.662, activation diff: 472.996, ratio: 0.970
#   pulse 3: activated nodes: 11492, borderline nodes: 7256, overall activation: 1405.384, activation diff: 917.722, ratio: 0.653
#   pulse 4: activated nodes: 12461, borderline nodes: 3813, overall activation: 3182.985, activation diff: 1777.601, ratio: 0.558
#   pulse 5: activated nodes: 12958, borderline nodes: 1896, overall activation: 4707.344, activation diff: 1524.359, ratio: 0.324
#   pulse 6: activated nodes: 13005, borderline nodes: 557, overall activation: 5753.998, activation diff: 1046.654, ratio: 0.182
#   pulse 7: activated nodes: 13026, borderline nodes: 234, overall activation: 6418.767, activation diff: 664.768, ratio: 0.104
#   pulse 8: activated nodes: 13041, borderline nodes: 169, overall activation: 6821.967, activation diff: 403.200, ratio: 0.059
#   pulse 9: activated nodes: 13044, borderline nodes: 141, overall activation: 7057.471, activation diff: 235.505, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13044
#   final overall activation: 7057.5
#   number of spread. activ. pulses: 9
#   running time: 1317

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99787295   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9977979   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9977515   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99748325   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9960896   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.996032   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.99562335   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99556684   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.995562   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9955465   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.9955102   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   12   0.99549013   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   13   0.99548686   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.99546343   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.9951318   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9950912   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   17   0.9950388   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99503446   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.99495065   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   20   0.9949335   REFERENCES:SIMDOC
