###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 26.520, activation diff: 28.681, ratio: 1.081
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 1338.935, activation diff: 1317.400, ratio: 0.984
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 4237.865, activation diff: 2898.931, ratio: 0.684
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 7173.283, activation diff: 2935.418, ratio: 0.409
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 8642.534, activation diff: 1469.251, ratio: 0.170
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 9210.922, activation diff: 568.387, ratio: 0.062
#   pulse 7: activated nodes: 13070, borderline nodes: 0, overall activation: 9420.283, activation diff: 209.362, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 9420.3
#   number of spread. activ. pulses: 7
#   running time: 2263

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9998287   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99982196   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9998108   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99981046   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9997853   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9997693   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   7   0.9997521   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   8   0.99975127   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   9   0.9997487   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   10   0.99974835   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   11   0.9997464   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   12   0.99974614   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   13   0.9997459   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.9997423   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.99972737   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.99972034   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   17   0.9997162   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99971324   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.9997088   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.99970126   REFERENCES:SIMDOC
