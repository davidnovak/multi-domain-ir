###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.670, activation diff: 22.055, ratio: 1.121
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 1038.114, activation diff: 1028.514, ratio: 0.991
#   pulse 3: activated nodes: 11507, borderline nodes: 7246, overall activation: 2664.777, activation diff: 1628.541, ratio: 0.611
#   pulse 4: activated nodes: 12821, borderline nodes: 2620, overall activation: 5098.835, activation diff: 2434.071, ratio: 0.477
#   pulse 5: activated nodes: 12992, borderline nodes: 1210, overall activation: 6438.089, activation diff: 1339.254, ratio: 0.208
#   pulse 6: activated nodes: 13025, borderline nodes: 230, overall activation: 7018.866, activation diff: 580.778, ratio: 0.083
#   pulse 7: activated nodes: 13043, borderline nodes: 152, overall activation: 7251.733, activation diff: 232.867, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13043
#   final overall activation: 7251.7
#   number of spread. activ. pulses: 7
#   running time: 1304

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99980646   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997896   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9997879   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9997737   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9997691   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9997586   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.99973416   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.9997299   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.99972856   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9997283   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.9997257   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.9997237   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.99972355   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.99971896   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.99969   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9996842   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   17   0.9996753   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   18   0.9996727   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9996635   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.99965966   REFERENCES:SIMDOC
