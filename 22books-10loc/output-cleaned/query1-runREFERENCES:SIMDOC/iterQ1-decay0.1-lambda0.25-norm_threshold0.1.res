###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.670, activation diff: 22.055, ratio: 1.121
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 1158.413, activation diff: 1148.813, ratio: 0.992
#   pulse 3: activated nodes: 11507, borderline nodes: 7246, overall activation: 3231.114, activation diff: 2074.601, ratio: 0.642
#   pulse 4: activated nodes: 12833, borderline nodes: 2551, overall activation: 6378.659, activation diff: 3147.562, ratio: 0.493
#   pulse 5: activated nodes: 12999, borderline nodes: 998, overall activation: 8159.445, activation diff: 1780.786, ratio: 0.218
#   pulse 6: activated nodes: 13041, borderline nodes: 189, overall activation: 8939.224, activation diff: 779.779, ratio: 0.087
#   pulse 7: activated nodes: 13046, borderline nodes: 115, overall activation: 9224.193, activation diff: 284.969, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13046
#   final overall activation: 9224.2
#   number of spread. activ. pulses: 7
#   running time: 1668

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.99980646   REFERENCES:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997896   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9997879   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9997737   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9997699   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.99975866   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.9997344   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99973017   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   9   0.9997295   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   10   0.9997288   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.99972636   REFERENCES:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.999725   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.9997239   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9997215   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.9996909   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9996885   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   17   0.9996759   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.9996759   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9996683   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.99966127   REFERENCES:SIMDOC
