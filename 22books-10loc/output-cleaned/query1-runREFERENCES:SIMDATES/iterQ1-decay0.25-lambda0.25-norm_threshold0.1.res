###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 953.420, activation diff: 951.895, ratio: 0.998
#   pulse 3: activated nodes: 7452, borderline nodes: 3238, overall activation: 1254.781, activation diff: 599.751, ratio: 0.478
#   pulse 4: activated nodes: 10550, borderline nodes: 3543, overall activation: 3057.336, activation diff: 1812.738, ratio: 0.593
#   pulse 5: activated nodes: 11263, borderline nodes: 1152, overall activation: 3935.727, activation diff: 878.402, ratio: 0.223
#   pulse 6: activated nodes: 11282, borderline nodes: 786, overall activation: 4270.771, activation diff: 335.044, ratio: 0.078
#   pulse 7: activated nodes: 11289, borderline nodes: 730, overall activation: 4389.094, activation diff: 118.324, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11289
#   final overall activation: 4389.1
#   number of spread. activ. pulses: 7
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9996165   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99956757   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99927044   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9969286   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9927386   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.7497021   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7495338   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.749531   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7495074   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.7493819   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   11   0.74937046   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   12   0.74935865   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   13   0.7493471   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.7493292   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   15   0.7493292   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.749329   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.749296   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.74927634   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.7492677   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   20   0.7492664   REFERENCES:SIMDATES
