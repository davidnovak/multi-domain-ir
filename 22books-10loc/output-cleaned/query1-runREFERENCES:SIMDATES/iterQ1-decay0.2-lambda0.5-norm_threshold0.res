###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 568.080, activation diff: 554.493, ratio: 0.976
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 1217.215, activation diff: 649.135, ratio: 0.533
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 2658.721, activation diff: 1441.506, ratio: 0.542
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 3758.879, activation diff: 1100.158, ratio: 0.293
#   pulse 6: activated nodes: 11342, borderline nodes: 22, overall activation: 4463.281, activation diff: 704.403, ratio: 0.158
#   pulse 7: activated nodes: 11342, borderline nodes: 22, overall activation: 4886.819, activation diff: 423.537, ratio: 0.087
#   pulse 8: activated nodes: 11342, borderline nodes: 22, overall activation: 5134.167, activation diff: 247.348, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5134.2
#   number of spread. activ. pulses: 8
#   running time: 537

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99573374   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9954957   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9947101   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.99066585   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.983106   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.79343677   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7930149   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.7929102   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7927793   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.7925377   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   11   0.79222405   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.79222083   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   13   0.79213893   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.7921388   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7921372   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   16   0.792107   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.79206795   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.7920371   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.79192686   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.79173005   REFERENCES:SIMDATES
