###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 1290.055, activation diff: 1285.965, ratio: 0.997
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 2111.592, activation diff: 985.647, ratio: 0.467
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 5059.292, activation diff: 2947.699, ratio: 0.583
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 6517.313, activation diff: 1458.022, ratio: 0.224
#   pulse 6: activated nodes: 11342, borderline nodes: 22, overall activation: 7075.331, activation diff: 558.018, ratio: 0.079
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7274.731, activation diff: 199.399, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7274.7
#   number of spread. activ. pulses: 7
#   running time: 1330

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99971783   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9996971   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9994773   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9973483   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99363434   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8997236   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.89957786   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.89957684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89956135   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.8994937   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.89948404   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   12   0.8994539   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   13   0.8994539   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.8994006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.8993951   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.8993951   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   17   0.8993951   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   18   0.89938784   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   19   0.89938414   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.8993714   REFERENCES:SIMDATES
