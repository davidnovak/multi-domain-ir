###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 1148.232, activation diff: 1144.143, ratio: 0.996
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 1733.099, activation diff: 731.597, ratio: 0.422
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 3873.448, activation diff: 2140.350, ratio: 0.553
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 4884.714, activation diff: 1011.266, ratio: 0.207
#   pulse 6: activated nodes: 11342, borderline nodes: 22, overall activation: 5262.127, activation diff: 377.413, ratio: 0.072
#   pulse 7: activated nodes: 11342, borderline nodes: 22, overall activation: 5393.699, activation diff: 131.572, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5393.7
#   number of spread. activ. pulses: 7
#   running time: 511

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9997178   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9996971   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99947715   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.99734807   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99363387   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.7997527   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7996247   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.7996237   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79961014   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.799541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.79953885   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   12   0.79951465   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   13   0.7995146   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.79946244   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   15   0.79946244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.7994623   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   17   0.7994559   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   18   0.7994537   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   19   0.7994411   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.7994324   REFERENCES:SIMDATES
