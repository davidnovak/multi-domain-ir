###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 475.124, activation diff: 462.686, ratio: 0.974
#   pulse 3: activated nodes: 7351, borderline nodes: 3144, overall activation: 1002.645, activation diff: 527.520, ratio: 0.526
#   pulse 4: activated nodes: 10535, borderline nodes: 3979, overall activation: 2393.898, activation diff: 1391.253, ratio: 0.581
#   pulse 5: activated nodes: 11238, borderline nodes: 1379, overall activation: 3502.791, activation diff: 1108.893, ratio: 0.317
#   pulse 6: activated nodes: 11305, borderline nodes: 505, overall activation: 4230.748, activation diff: 727.957, ratio: 0.172
#   pulse 7: activated nodes: 11321, borderline nodes: 249, overall activation: 4675.456, activation diff: 444.708, ratio: 0.095
#   pulse 8: activated nodes: 11328, borderline nodes: 178, overall activation: 4938.204, activation diff: 262.748, ratio: 0.053
#   pulse 9: activated nodes: 11330, borderline nodes: 152, overall activation: 5090.722, activation diff: 152.518, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 5090.7
#   number of spread. activ. pulses: 9
#   running time: 548

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.997767   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9975671   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99706006   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9937442   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9874642   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.7966286   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.79636467   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.7962886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7961917   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.79600656   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   11   0.7958058   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7958057   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.79580456   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   14   0.79579914   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.79579234   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   16   0.795751   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   17   0.79572666   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.7957251   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.795601   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.7955727   REFERENCES:SIMDATES
