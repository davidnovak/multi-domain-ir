###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 1141.750, activation diff: 1140.225, ratio: 0.999
#   pulse 3: activated nodes: 7452, borderline nodes: 3238, overall activation: 1724.323, activation diff: 939.702, ratio: 0.545
#   pulse 4: activated nodes: 10550, borderline nodes: 3541, overall activation: 4699.491, activation diff: 2992.932, ratio: 0.637
#   pulse 5: activated nodes: 11267, borderline nodes: 1106, overall activation: 6250.734, activation diff: 1551.275, ratio: 0.248
#   pulse 6: activated nodes: 11330, borderline nodes: 130, overall activation: 6866.747, activation diff: 616.012, ratio: 0.090
#   pulse 7: activated nodes: 11340, borderline nodes: 47, overall activation: 7091.956, activation diff: 225.210, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 7092.0
#   number of spread. activ. pulses: 7
#   running time: 592

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99961656   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99956757   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992709   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9969296   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9927438   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89964485   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8994404   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.8994376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8994091   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.89925873   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   11   0.8992445   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   12   0.8992303   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   13   0.8992275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.899195   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   15   0.899195   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   16   0.899195   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.8991827   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.89916855   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.8991487   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.8991213   REFERENCES:SIMDATES
