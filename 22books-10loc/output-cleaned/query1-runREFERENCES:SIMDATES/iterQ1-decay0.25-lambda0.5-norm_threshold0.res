###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 533.548, activation diff: 519.961, ratio: 0.975
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 1107.181, activation diff: 573.634, ratio: 0.518
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 2305.938, activation diff: 1198.757, ratio: 0.520
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 3209.802, activation diff: 903.864, ratio: 0.282
#   pulse 6: activated nodes: 11320, borderline nodes: 702, overall activation: 3784.857, activation diff: 575.055, ratio: 0.152
#   pulse 7: activated nodes: 11320, borderline nodes: 702, overall activation: 4128.983, activation diff: 344.125, ratio: 0.083
#   pulse 8: activated nodes: 11320, borderline nodes: 702, overall activation: 4329.079, activation diff: 200.097, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 4329.1
#   number of spread. activ. pulses: 8
#   running time: 566

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9957336   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9954957   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.994709   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9906626   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98309326   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.74384594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.74345136   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.7433518   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7432298   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.7430036   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   11   0.7427087   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.74270517   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   13   0.74262977   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.7426294   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.74262637   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   16   0.7425994   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.74254143   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.7425114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.74243116   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.74224114   REFERENCES:SIMDATES
