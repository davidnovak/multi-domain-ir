###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 1077.321, activation diff: 1073.232, ratio: 0.996
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 1551.273, activation diff: 611.942, ratio: 0.394
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 3322.087, activation diff: 1770.814, ratio: 0.533
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 4137.275, activation diff: 815.188, ratio: 0.197
#   pulse 6: activated nodes: 11320, borderline nodes: 702, overall activation: 4436.530, activation diff: 299.255, ratio: 0.067
#   pulse 7: activated nodes: 11320, borderline nodes: 702, overall activation: 4539.434, activation diff: 102.903, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 4539.4
#   number of spread. activ. pulses: 7
#   running time: 665

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9997177   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9996971   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.999477   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.99734783   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9936335   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.7497673   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7496481   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.749647   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7496344   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.74956644   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.7495653   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   12   0.7495449   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   13   0.74954486   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.749496   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   15   0.749496   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.74949574   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   17   0.7494867   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.74948436   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   19   0.7494665   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.74946463   REFERENCES:SIMDATES
