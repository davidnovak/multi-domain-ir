###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 1016.197, activation diff: 1014.672, ratio: 0.998
#   pulse 3: activated nodes: 7452, borderline nodes: 3238, overall activation: 1406.519, activation diff: 708.322, ratio: 0.504
#   pulse 4: activated nodes: 10550, borderline nodes: 3542, overall activation: 3577.763, activation diff: 2183.841, ratio: 0.610
#   pulse 5: activated nodes: 11265, borderline nodes: 1141, overall activation: 4661.763, activation diff: 1084.018, ratio: 0.233
#   pulse 6: activated nodes: 11320, borderline nodes: 246, overall activation: 5082.149, activation diff: 420.385, ratio: 0.083
#   pulse 7: activated nodes: 11329, borderline nodes: 160, overall activation: 5232.367, activation diff: 150.218, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 5232.4
#   number of spread. activ. pulses: 7
#   running time: 522

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9996165   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99956757   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992706   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.99692905   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.992741   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.799683   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.79950273   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.7995   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79947484   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.79934084   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   11   0.7993285   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   12   0.7993159   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   13   0.7993069   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.7992846   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   15   0.7992846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.79928446   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.7992605   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.7992424   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.799219   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   20   0.7992178   REFERENCES:SIMDATES
