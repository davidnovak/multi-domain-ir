###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 637.144, activation diff: 623.557, ratio: 0.979
#   pulse 3: activated nodes: 7694, borderline nodes: 3445, overall activation: 1445.321, activation diff: 808.177, ratio: 0.559
#   pulse 4: activated nodes: 10618, borderline nodes: 2924, overall activation: 3422.645, activation diff: 1977.324, ratio: 0.578
#   pulse 5: activated nodes: 11320, borderline nodes: 702, overall activation: 4959.903, activation diff: 1537.258, ratio: 0.310
#   pulse 6: activated nodes: 11342, borderline nodes: 22, overall activation: 5952.774, activation diff: 992.871, ratio: 0.167
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 6553.620, activation diff: 600.846, ratio: 0.092
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 6906.789, activation diff: 353.169, ratio: 0.051
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 7111.333, activation diff: 204.544, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7111.3
#   number of spread. activ. pulses: 9
#   running time: 556

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99786645   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9977475   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9973371   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9943645   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9889324   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89630944   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8960709   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.8960128   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89593875   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.89580256   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   11   0.8956276   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8956264   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.89557827   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   14   0.89557827   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.895578   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   16   0.8955605   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.89555   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.8955293   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.8954589   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.89535105   REFERENCES:SIMDATES
