###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 532.744, activation diff: 520.305, ratio: 0.977
#   pulse 3: activated nodes: 7351, borderline nodes: 3144, overall activation: 1189.327, activation diff: 656.583, ratio: 0.552
#   pulse 4: activated nodes: 10537, borderline nodes: 3886, overall activation: 3102.554, activation diff: 1913.227, ratio: 0.617
#   pulse 5: activated nodes: 11245, borderline nodes: 1272, overall activation: 4654.092, activation diff: 1551.538, ratio: 0.333
#   pulse 6: activated nodes: 11317, borderline nodes: 273, overall activation: 5681.701, activation diff: 1027.609, ratio: 0.181
#   pulse 7: activated nodes: 11333, borderline nodes: 88, overall activation: 6313.601, activation diff: 631.900, ratio: 0.100
#   pulse 8: activated nodes: 11339, borderline nodes: 47, overall activation: 6689.551, activation diff: 375.950, ratio: 0.056
#   pulse 9: activated nodes: 11341, borderline nodes: 28, overall activation: 6909.518, activation diff: 219.967, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6909.5
#   number of spread. activ. pulses: 9
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.997767   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9975671   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9970615   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9937483   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98748624   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8962077   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.89591026   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.89582574   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89571613   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.8955075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.8952817   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   12   0.8952817   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.8952813   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   14   0.8952739   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.89526635   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   16   0.89524746   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.8952242   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   18   0.895193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.89505124   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.895023   REFERENCES:SIMDATES
