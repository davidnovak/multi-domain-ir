###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4202, overall activation: 446.315, activation diff: 433.876, ratio: 0.972
#   pulse 3: activated nodes: 7351, borderline nodes: 3144, overall activation: 913.029, activation diff: 466.714, ratio: 0.511
#   pulse 4: activated nodes: 10533, borderline nodes: 4027, overall activation: 2070.830, activation diff: 1157.801, ratio: 0.559
#   pulse 5: activated nodes: 11230, borderline nodes: 1439, overall activation: 2982.287, activation diff: 911.457, ratio: 0.306
#   pulse 6: activated nodes: 11263, borderline nodes: 852, overall activation: 3576.465, activation diff: 594.178, ratio: 0.166
#   pulse 7: activated nodes: 11286, borderline nodes: 771, overall activation: 3937.659, activation diff: 361.194, ratio: 0.092
#   pulse 8: activated nodes: 11287, borderline nodes: 747, overall activation: 4150.204, activation diff: 212.544, ratio: 0.051
#   pulse 9: activated nodes: 11290, borderline nodes: 742, overall activation: 4273.074, activation diff: 122.871, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 4273.1
#   number of spread. activ. pulses: 9
#   running time: 642

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99776685   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99756706   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9970591   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9937413   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9874487   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.74683905   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7465919   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   8   0.74651945   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7464291   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   10   0.7462558   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   11   0.7460675   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.74606735   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.74606526   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   14   0.7460616   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.7460553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   16   0.7459979   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_717   17   0.7459931   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.74597204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.7458757   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.74584544   REFERENCES:SIMDATES
