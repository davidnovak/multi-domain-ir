###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 1094.668, activation diff: 1089.021, ratio: 0.995
#   pulse 3: activated nodes: 8466, borderline nodes: 3931, overall activation: 1670.284, activation diff: 654.847, ratio: 0.392
#   pulse 4: activated nodes: 11349, borderline nodes: 2883, overall activation: 3740.063, activation diff: 2069.780, ratio: 0.553
#   pulse 5: activated nodes: 11463, borderline nodes: 114, overall activation: 4696.366, activation diff: 956.303, ratio: 0.204
#   pulse 6: activated nodes: 11463, borderline nodes: 114, overall activation: 5070.662, activation diff: 374.297, ratio: 0.074
#   pulse 7: activated nodes: 11463, borderline nodes: 114, overall activation: 5206.003, activation diff: 135.341, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11463
#   final overall activation: 5206.0
#   number of spread. activ. pulses: 7
#   running time: 697

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99975216   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9996984   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99953705   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9973663   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99380183   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7497835   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.74970347   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7496872   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   9   0.74966764   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.74964637   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.74964595   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.749633   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.7496012   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.74957085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.749566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.7495512   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.74954975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7495276   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.7495189   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   20   0.7495183   REFERENCES:SIMDATES:SIMLOC
