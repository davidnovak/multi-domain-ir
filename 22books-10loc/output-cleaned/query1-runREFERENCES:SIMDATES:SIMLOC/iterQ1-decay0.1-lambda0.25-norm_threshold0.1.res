###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 1152.869, activation diff: 1149.778, ratio: 0.997
#   pulse 3: activated nodes: 8071, borderline nodes: 3647, overall activation: 1819.937, activation diff: 924.057, ratio: 0.508
#   pulse 4: activated nodes: 11340, borderline nodes: 4036, overall activation: 5325.483, activation diff: 3505.980, ratio: 0.658
#   pulse 5: activated nodes: 11440, borderline nodes: 324, overall activation: 7093.372, activation diff: 1767.890, ratio: 0.249
#   pulse 6: activated nodes: 11455, borderline nodes: 56, overall activation: 7793.144, activation diff: 699.772, ratio: 0.090
#   pulse 7: activated nodes: 11460, borderline nodes: 19, overall activation: 8040.795, activation diff: 247.651, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8040.8
#   number of spread. activ. pulses: 7
#   running time: 873

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9996826   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9995703   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993633   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99695325   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99294865   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8996813   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.8995378   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89950824   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   9   0.8994518   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8994229   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8993961   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.89939606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.8993254   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89929676   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.8992583   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.89925236   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.8992467   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   18   0.89924073   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.89923877   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.8992385   REFERENCES:SIMDATES:SIMLOC
