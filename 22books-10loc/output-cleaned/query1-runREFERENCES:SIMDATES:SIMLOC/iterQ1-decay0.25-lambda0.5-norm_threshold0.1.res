###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 449.336, activation diff: 436.803, ratio: 0.972
#   pulse 3: activated nodes: 7902, borderline nodes: 3537, overall activation: 938.666, activation diff: 489.329, ratio: 0.521
#   pulse 4: activated nodes: 11322, borderline nodes: 4724, overall activation: 2209.367, activation diff: 1270.702, ratio: 0.575
#   pulse 5: activated nodes: 11414, borderline nodes: 767, overall activation: 3251.814, activation diff: 1042.446, ratio: 0.321
#   pulse 6: activated nodes: 11443, borderline nodes: 301, overall activation: 3974.046, activation diff: 722.233, ratio: 0.182
#   pulse 7: activated nodes: 11452, borderline nodes: 169, overall activation: 4433.292, activation diff: 459.246, ratio: 0.104
#   pulse 8: activated nodes: 11454, borderline nodes: 141, overall activation: 4711.276, activation diff: 277.984, ratio: 0.059
#   pulse 9: activated nodes: 11455, borderline nodes: 135, overall activation: 4875.167, activation diff: 163.890, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 4875.2
#   number of spread. activ. pulses: 9
#   running time: 650

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99779296   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9975677   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971086   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99375355   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9876867   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7468531   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7466135   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.7465576   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7464329   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.7463279   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7461437   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7461165   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74610734   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.74609375   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   15   0.7460831   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.746083   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.7460356   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.7459927   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.74598837   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.74590254   REFERENCES:SIMDATES:SIMLOC
