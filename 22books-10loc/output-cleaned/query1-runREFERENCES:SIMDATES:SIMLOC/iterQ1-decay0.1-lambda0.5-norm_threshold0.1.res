###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 536.305, activation diff: 523.771, ratio: 0.977
#   pulse 3: activated nodes: 7902, borderline nodes: 3537, overall activation: 1216.905, activation diff: 680.601, ratio: 0.559
#   pulse 4: activated nodes: 11327, borderline nodes: 4589, overall activation: 3320.310, activation diff: 2103.405, ratio: 0.633
#   pulse 5: activated nodes: 11428, borderline nodes: 546, overall activation: 5120.740, activation diff: 1800.430, ratio: 0.352
#   pulse 6: activated nodes: 11452, borderline nodes: 166, overall activation: 6365.920, activation diff: 1245.180, ratio: 0.196
#   pulse 7: activated nodes: 11455, borderline nodes: 51, overall activation: 7129.599, activation diff: 763.679, ratio: 0.107
#   pulse 8: activated nodes: 11460, borderline nodes: 23, overall activation: 7575.080, activation diff: 445.481, ratio: 0.059
#   pulse 9: activated nodes: 11461, borderline nodes: 15, overall activation: 7829.410, activation diff: 254.330, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 7829.4
#   number of spread. activ. pulses: 9
#   running time: 698

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99779314   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9975677   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971101   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99376035   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98771214   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8962248   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.895939   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.8958764   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8957205   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.8955986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.8953865   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8953503   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.89533633   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.89533085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.89530027   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.89530027   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.8952911   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.89524865   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.8951932   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.89510274   REFERENCES:SIMDATES:SIMLOC
