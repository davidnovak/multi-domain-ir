###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 1310.704, activation diff: 1305.057, ratio: 0.996
#   pulse 3: activated nodes: 8466, borderline nodes: 3931, overall activation: 2261.217, activation diff: 1041.616, ratio: 0.461
#   pulse 4: activated nodes: 11349, borderline nodes: 2883, overall activation: 5765.194, activation diff: 3503.977, ratio: 0.608
#   pulse 5: activated nodes: 11463, borderline nodes: 114, overall activation: 7389.615, activation diff: 1624.421, ratio: 0.220
#   pulse 6: activated nodes: 11464, borderline nodes: 1, overall activation: 7987.261, activation diff: 597.646, ratio: 0.075
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8190.573, activation diff: 203.313, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8190.6
#   number of spread. activ. pulses: 7
#   running time: 1914

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99975216   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9996984   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995371   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9973665   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9938021   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8997431   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.8996524   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89962983   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   9   0.89961857   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.89959085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.8995765   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.8995654   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.899541   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.8995   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.89948964   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.8994849   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.8994695   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.8994386   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8994353   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.8994306   REFERENCES:SIMDATES:SIMLOC
