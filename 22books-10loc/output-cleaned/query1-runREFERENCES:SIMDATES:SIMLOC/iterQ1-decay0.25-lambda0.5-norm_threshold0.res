###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 541.273, activation diff: 527.687, ratio: 0.975
#   pulse 3: activated nodes: 8466, borderline nodes: 3931, overall activation: 1151.193, activation diff: 609.920, ratio: 0.530
#   pulse 4: activated nodes: 11349, borderline nodes: 2883, overall activation: 2495.824, activation diff: 1344.630, ratio: 0.539
#   pulse 5: activated nodes: 11463, borderline nodes: 114, overall activation: 3550.184, activation diff: 1054.361, ratio: 0.297
#   pulse 6: activated nodes: 11463, borderline nodes: 114, overall activation: 4251.963, activation diff: 701.779, ratio: 0.165
#   pulse 7: activated nodes: 11463, borderline nodes: 114, overall activation: 4683.465, activation diff: 431.502, ratio: 0.092
#   pulse 8: activated nodes: 11463, borderline nodes: 114, overall activation: 4938.189, activation diff: 254.724, ratio: 0.052
#   pulse 9: activated nodes: 11463, borderline nodes: 114, overall activation: 5085.489, activation diff: 147.300, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11463
#   final overall activation: 5085.5
#   number of spread. activ. pulses: 9
#   running time: 785

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9978826   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9977478   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99737096   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943702   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98911065   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7469341   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7467466   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.74671274   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.746621   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.74655724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7464327   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7464199   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.74641156   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.74636066   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7463332   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.74632674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7463055   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7462856   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.74624807   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.74617136   REFERENCES:SIMDATES:SIMLOC
