###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 576.298, activation diff: 562.711, ratio: 0.976
#   pulse 3: activated nodes: 8466, borderline nodes: 3931, overall activation: 1264.055, activation diff: 687.758, ratio: 0.544
#   pulse 4: activated nodes: 11349, borderline nodes: 2883, overall activation: 2879.227, activation diff: 1615.171, ratio: 0.561
#   pulse 5: activated nodes: 11463, borderline nodes: 114, overall activation: 4165.667, activation diff: 1286.441, ratio: 0.309
#   pulse 6: activated nodes: 11464, borderline nodes: 1, overall activation: 5023.270, activation diff: 857.603, ratio: 0.171
#   pulse 7: activated nodes: 11464, borderline nodes: 1, overall activation: 5545.906, activation diff: 522.636, ratio: 0.094
#   pulse 8: activated nodes: 11464, borderline nodes: 1, overall activation: 5851.339, activation diff: 305.433, ratio: 0.052
#   pulse 9: activated nodes: 11464, borderline nodes: 1, overall activation: 6026.335, activation diff: 174.996, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6026.3
#   number of spread. activ. pulses: 9
#   running time: 779

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99788266   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9977478   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9973713   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943719   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9891149   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7967305   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7965307   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.7964959   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79639643   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.79632914   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7961997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79618573   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.79617846   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.796121   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7960894   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.79608214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7960613   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.79605424   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.7960166   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.7959245   REFERENCES:SIMDATES:SIMLOC
