###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 646.346, activation diff: 632.759, ratio: 0.979
#   pulse 3: activated nodes: 8466, borderline nodes: 3931, overall activation: 1498.140, activation diff: 851.794, ratio: 0.569
#   pulse 4: activated nodes: 11349, borderline nodes: 2883, overall activation: 3718.968, activation diff: 2220.827, ratio: 0.597
#   pulse 5: activated nodes: 11463, borderline nodes: 114, overall activation: 5514.338, activation diff: 1795.371, ratio: 0.326
#   pulse 6: activated nodes: 11464, borderline nodes: 1, overall activation: 6688.673, activation diff: 1174.334, ratio: 0.176
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7386.582, activation diff: 697.909, ratio: 0.094
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7786.121, activation diff: 399.539, ratio: 0.051
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 8011.146, activation diff: 225.025, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8011.1
#   number of spread. activ. pulses: 9
#   running time: 662

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9978827   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9977478   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9973719   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943743   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9891205   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89632344   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8960992   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.8960626   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89594686   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.8958733   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.89573526   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.895719   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.89571416   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8956419   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8956018   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.8955925   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.8955836   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   18   0.8955734   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.8955476   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.89542365   REFERENCES:SIMDATES:SIMLOC
