###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 962.816, activation diff: 959.725, ratio: 0.997
#   pulse 3: activated nodes: 8071, borderline nodes: 3647, overall activation: 1336.242, activation diff: 593.329, ratio: 0.444
#   pulse 4: activated nodes: 11340, borderline nodes: 4059, overall activation: 3411.213, activation diff: 2075.334, ratio: 0.608
#   pulse 5: activated nodes: 11437, borderline nodes: 423, overall activation: 4434.364, activation diff: 1023.151, ratio: 0.231
#   pulse 6: activated nodes: 11451, borderline nodes: 177, overall activation: 4867.970, activation diff: 433.606, ratio: 0.089
#   pulse 7: activated nodes: 11454, borderline nodes: 129, overall activation: 5033.787, activation diff: 165.817, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 5033.8
#   number of spread. activ. pulses: 7
#   running time: 622

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99968255   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9995703   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993632   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99695265   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99294776   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.749732   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.74960583   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74958515   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   9   0.7495235   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.74951875   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   11   0.7494901   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7494832   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.749418   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7494054   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7493682   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   16   0.74936646   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7493621   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   18   0.7493604   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.7493596   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.74935675   REFERENCES:SIMDATES:SIMLOC
