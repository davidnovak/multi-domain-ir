###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 1026.167, activation diff: 1023.076, ratio: 0.997
#   pulse 3: activated nodes: 8071, borderline nodes: 3647, overall activation: 1492.509, activation diff: 698.724, ratio: 0.468
#   pulse 4: activated nodes: 11340, borderline nodes: 4054, overall activation: 4009.658, activation diff: 2517.541, ratio: 0.628
#   pulse 5: activated nodes: 11438, borderline nodes: 388, overall activation: 5275.491, activation diff: 1265.833, ratio: 0.240
#   pulse 6: activated nodes: 11453, borderline nodes: 122, overall activation: 5804.630, activation diff: 529.139, ratio: 0.091
#   pulse 7: activated nodes: 11459, borderline nodes: 48, overall activation: 6001.624, activation diff: 196.994, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 6001.6
#   number of spread. activ. pulses: 7
#   running time: 846

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99968255   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9995703   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99936324   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99695295   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99294823   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7997151   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.79958284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79955935   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   9   0.799499   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7994869   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   11   0.7994585   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79945374   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.7993864   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.799369   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7993292   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.79932815   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.79932797   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7993198   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   19   0.79931784   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.79931706   REFERENCES:SIMDATES:SIMLOC
