###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4535, borderline nodes: 4488, overall activation: 478.326, activation diff: 465.792, ratio: 0.974
#   pulse 3: activated nodes: 7902, borderline nodes: 3537, overall activation: 1028.977, activation diff: 550.651, ratio: 0.535
#   pulse 4: activated nodes: 11325, borderline nodes: 4683, overall activation: 2554.230, activation diff: 1525.253, ratio: 0.597
#   pulse 5: activated nodes: 11423, borderline nodes: 647, overall activation: 3829.959, activation diff: 1275.730, ratio: 0.333
#   pulse 6: activated nodes: 11449, borderline nodes: 230, overall activation: 4721.937, activation diff: 891.978, ratio: 0.189
#   pulse 7: activated nodes: 11454, borderline nodes: 117, overall activation: 5285.376, activation diff: 563.439, ratio: 0.107
#   pulse 8: activated nodes: 11456, borderline nodes: 56, overall activation: 5622.570, activation diff: 337.195, ratio: 0.060
#   pulse 9: activated nodes: 11459, borderline nodes: 40, overall activation: 5819.179, activation diff: 196.608, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 5819.2
#   number of spread. activ. pulses: 9
#   running time: 944

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.997793   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9975677   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971092   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99375635   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98769724   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7966436   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7963886   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.79633045   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7961956   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.79608476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7958908   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79586077   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7958504   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.79583895   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   15   0.7958224   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.79582226   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.7957905   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.795747   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.7957232   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.7956388   REFERENCES:SIMDATES:SIMLOC
