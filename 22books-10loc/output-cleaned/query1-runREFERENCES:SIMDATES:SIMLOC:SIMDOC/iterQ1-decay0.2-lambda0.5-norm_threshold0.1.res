###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 14.780, activation diff: 14.703, ratio: 0.995
#   pulse 2: activated nodes: 5282, borderline nodes: 5085, overall activation: 491.282, activation diff: 476.585, ratio: 0.970
#   pulse 3: activated nodes: 11600, borderline nodes: 7200, overall activation: 1435.950, activation diff: 944.669, ratio: 0.658
#   pulse 4: activated nodes: 12798, borderline nodes: 3977, overall activation: 3325.728, activation diff: 1889.777, ratio: 0.568
#   pulse 5: activated nodes: 13001, borderline nodes: 1190, overall activation: 4999.235, activation diff: 1673.507, ratio: 0.335
#   pulse 6: activated nodes: 13018, borderline nodes: 400, overall activation: 6177.674, activation diff: 1178.439, ratio: 0.191
#   pulse 7: activated nodes: 13032, borderline nodes: 175, overall activation: 6928.598, activation diff: 750.924, ratio: 0.108
#   pulse 8: activated nodes: 13045, borderline nodes: 139, overall activation: 7381.266, activation diff: 452.668, ratio: 0.061
#   pulse 9: activated nodes: 13047, borderline nodes: 122, overall activation: 7643.971, activation diff: 262.705, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13047
#   final overall activation: 7644.0
#   number of spread. activ. pulses: 9
#   running time: 1415

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9978863   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9978043   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.99777037   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9974836   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9961001   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9960342   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.9956248   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.99556786   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.9955678   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.9955541   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.9955151   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.99549663   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.9954958   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9954669   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   15   0.9952029   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.9951457   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.9951086   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99504095   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.99496555   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.99495894   REFERENCES:SIMDATES:SIMLOC:SIMDOC
