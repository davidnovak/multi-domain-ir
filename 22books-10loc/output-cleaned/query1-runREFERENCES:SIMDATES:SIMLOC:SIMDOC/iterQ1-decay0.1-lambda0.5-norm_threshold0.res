###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.347, activation diff: 19.121, ratio: 0.988
#   pulse 2: activated nodes: 5282, borderline nodes: 5085, overall activation: 673.725, activation diff: 654.378, ratio: 0.971
#   pulse 3: activated nodes: 12226, borderline nodes: 6944, overall activation: 2318.392, activation diff: 1644.667, ratio: 0.709
#   pulse 4: activated nodes: 13035, borderline nodes: 809, overall activation: 4904.594, activation diff: 2586.202, ratio: 0.527
#   pulse 5: activated nodes: 13072, borderline nodes: 37, overall activation: 7049.363, activation diff: 2144.768, ratio: 0.304
#   pulse 6: activated nodes: 13072, borderline nodes: 0, overall activation: 8455.005, activation diff: 1405.643, ratio: 0.166
#   pulse 7: activated nodes: 13072, borderline nodes: 0, overall activation: 9279.529, activation diff: 824.524, ratio: 0.089
#   pulse 8: activated nodes: 13072, borderline nodes: 0, overall activation: 9748.927, activation diff: 469.398, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13072
#   final overall activation: 9748.9
#   number of spread. activ. pulses: 8
#   running time: 1540

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99610937   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.9960814   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9958204   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.995558   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99258256   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9925213   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.99236065   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   8   0.9917156   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.99170715   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.99163646   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   11   0.99160445   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.99158955   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   13   0.9915626   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   14   0.991549   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.99147576   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.9911674   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.9909806   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.9909008   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9908936   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.9907658   REFERENCES:SIMDATES:SIMLOC:SIMDOC
