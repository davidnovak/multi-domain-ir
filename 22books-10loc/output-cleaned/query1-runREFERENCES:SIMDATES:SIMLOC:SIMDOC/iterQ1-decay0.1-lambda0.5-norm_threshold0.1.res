###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 14.780, activation diff: 14.703, ratio: 0.995
#   pulse 2: activated nodes: 5282, borderline nodes: 5085, overall activation: 546.692, activation diff: 531.996, ratio: 0.973
#   pulse 3: activated nodes: 11600, borderline nodes: 7200, overall activation: 1694.570, activation diff: 1147.878, ratio: 0.677
#   pulse 4: activated nodes: 12840, borderline nodes: 3765, overall activation: 4131.316, activation diff: 2436.746, ratio: 0.590
#   pulse 5: activated nodes: 13005, borderline nodes: 1035, overall activation: 6366.612, activation diff: 2235.295, ratio: 0.351
#   pulse 6: activated nodes: 13026, borderline nodes: 292, overall activation: 7928.791, activation diff: 1562.179, ratio: 0.197
#   pulse 7: activated nodes: 13045, borderline nodes: 148, overall activation: 8904.618, activation diff: 975.827, ratio: 0.110
#   pulse 8: activated nodes: 13048, borderline nodes: 117, overall activation: 9468.262, activation diff: 563.644, ratio: 0.060
#   pulse 9: activated nodes: 13048, borderline nodes: 92, overall activation: 9784.214, activation diff: 315.952, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13048
#   final overall activation: 9784.2
#   number of spread. activ. pulses: 9
#   running time: 1508

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99788636   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99780434   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.99777037   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9974836   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.9961047   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.99603486   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.995631   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   8   0.9955755   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   9   0.9955734   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.995565   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.99552476   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.99550974   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.9955038   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.9954827   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   15   0.9952142   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.9951591   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.99513304   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99505675   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   19   0.9949901   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   20   0.9949889   REFERENCES:SIMDATES:SIMLOC:SIMDOC
