###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.347, activation diff: 19.121, ratio: 0.988
#   pulse 2: activated nodes: 5282, borderline nodes: 5085, overall activation: 571.172, activation diff: 551.825, ratio: 0.966
#   pulse 3: activated nodes: 12226, borderline nodes: 6944, overall activation: 1766.091, activation diff: 1194.919, ratio: 0.677
#   pulse 4: activated nodes: 13035, borderline nodes: 809, overall activation: 3475.110, activation diff: 1709.019, ratio: 0.492
#   pulse 5: activated nodes: 13072, borderline nodes: 37, overall activation: 4848.703, activation diff: 1373.593, ratio: 0.283
#   pulse 6: activated nodes: 13072, borderline nodes: 37, overall activation: 5765.688, activation diff: 916.985, ratio: 0.159
#   pulse 7: activated nodes: 13072, borderline nodes: 37, overall activation: 6331.396, activation diff: 565.708, ratio: 0.089
#   pulse 8: activated nodes: 13072, borderline nodes: 37, overall activation: 6664.872, activation diff: 333.476, ratio: 0.050
#   pulse 9: activated nodes: 13072, borderline nodes: 37, overall activation: 6856.758, activation diff: 191.886, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13072
#   final overall activation: 6856.8
#   number of spread. activ. pulses: 9
#   running time: 1532

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99805456   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.99804044   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   3   0.9979102   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9977788   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99628925   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961966   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   7   0.9961803   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   8   0.9958553   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.99585056   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   10   0.995814   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   11   0.99579334   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.9957926   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   13   0.99577665   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   14   0.9957677   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.9957338   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   16   0.9955795   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   17   0.99547   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.9954425   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.9954276   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   20   0.99536645   REFERENCES:SIMDATES:SIMLOC:SIMDOC
