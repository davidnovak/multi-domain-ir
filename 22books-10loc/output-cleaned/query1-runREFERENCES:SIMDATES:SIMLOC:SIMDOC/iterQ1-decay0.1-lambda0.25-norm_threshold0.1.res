###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 19.670, activation diff: 22.055, ratio: 1.121
#   pulse 2: activated nodes: 5282, borderline nodes: 5085, overall activation: 1170.699, activation diff: 1158.922, ratio: 0.990
#   pulse 3: activated nodes: 11673, borderline nodes: 7187, overall activation: 3321.309, activation diff: 2151.711, ratio: 0.648
#   pulse 4: activated nodes: 12981, borderline nodes: 2586, overall activation: 6911.838, activation diff: 3590.528, ratio: 0.519
#   pulse 5: activated nodes: 13015, borderline nodes: 547, overall activation: 8897.915, activation diff: 1986.077, ratio: 0.223
#   pulse 6: activated nodes: 13046, borderline nodes: 146, overall activation: 9738.312, activation diff: 840.397, ratio: 0.086
#   pulse 7: activated nodes: 13048, borderline nodes: 103, overall activation: 10037.083, activation diff: 298.771, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13048
#   final overall activation: 10037.1
#   number of spread. activ. pulses: 7
#   running time: 1603

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.9998082   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997995   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9997879   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   0.9997871   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   5   0.99977136   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   6   0.9997587   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   7   0.99973446   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_323   8   0.99973017   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_318   9   0.99973017   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   10   0.99972904   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_293   11   0.99972665   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_319   12   0.9997258   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.99972403   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   14   0.99972224   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_146   15   0.9996916   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   16   0.9996904   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_67   17   0.9996767   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_790   18   0.99967617   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_111   19   0.99967366   REFERENCES:SIMDATES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_129   20   0.99966186   REFERENCES:SIMDATES:SIMLOC:SIMDOC
