###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 962.037, activation diff: 960.125, ratio: 0.998
#   pulse 3: activated nodes: 7950, borderline nodes: 3537, overall activation: 1310.074, activation diff: 597.267, ratio: 0.456
#   pulse 4: activated nodes: 11267, borderline nodes: 3989, overall activation: 3358.970, activation diff: 2050.068, ratio: 0.610
#   pulse 5: activated nodes: 11428, borderline nodes: 429, overall activation: 4378.995, activation diff: 1020.025, ratio: 0.233
#   pulse 6: activated nodes: 11433, borderline nodes: 216, overall activation: 4809.705, activation diff: 430.710, ratio: 0.090
#   pulse 7: activated nodes: 11437, borderline nodes: 169, overall activation: 4973.440, activation diff: 163.735, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 4973.4
#   number of spread. activ. pulses: 7
#   running time: 563

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99966645   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9994351   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993632   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9969525   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99284744   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.74967563   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.74958515   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.74957496   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.74946856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7494535   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.74941635   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7494054   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.74939775   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.74939585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7493682   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   16   0.74936646   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7493621   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   18   0.7493604   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.7493596   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.7493413   REFERENCES:SIMLOC
