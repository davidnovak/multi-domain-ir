###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 1165.224, activation diff: 1160.816, ratio: 0.996
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 1827.357, activation diff: 768.197, ratio: 0.420
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 4305.584, activation diff: 2478.227, ratio: 0.576
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 5476.321, activation diff: 1170.737, ratio: 0.214
#   pulse 6: activated nodes: 11453, borderline nodes: 12, overall activation: 5925.117, activation diff: 448.796, ratio: 0.076
#   pulse 7: activated nodes: 11453, borderline nodes: 12, overall activation: 6082.701, activation diff: 157.584, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6082.7
#   number of spread. activ. pulses: 7
#   running time: 547

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99974394   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9996278   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995371   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99736637   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99371827   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.79973435   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.799668   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79966724   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.7995963   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   10   0.7995804   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.79957974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.79956865   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7995604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7995403   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7995237   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.79949677   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.79949063   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7994896   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   19   0.7994862   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.7994596   REFERENCES:SIMLOC
