###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 540.668, activation diff: 527.081, ratio: 0.975
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 1138.631, activation diff: 597.963, ratio: 0.525
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 2454.837, activation diff: 1316.206, ratio: 0.536
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 3490.073, activation diff: 1035.236, ratio: 0.297
#   pulse 6: activated nodes: 11441, borderline nodes: 145, overall activation: 4179.513, activation diff: 689.440, ratio: 0.165
#   pulse 7: activated nodes: 11441, borderline nodes: 145, overall activation: 4603.263, activation diff: 423.749, ratio: 0.092
#   pulse 8: activated nodes: 11441, borderline nodes: 145, overall activation: 4853.157, activation diff: 249.895, ratio: 0.051
#   pulse 9: activated nodes: 11441, borderline nodes: 145, overall activation: 4997.455, activation diff: 144.297, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 4997.5
#   number of spread. activ. pulses: 9
#   running time: 811

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99787825   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99771214   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99737096   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943702   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98902667   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7469131   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7467462   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.74670064   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74659634   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.74654806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7464243   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.74636614   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74636066   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.7463472   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7463332   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.74632674   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7463055   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7462856   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.74624807   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.74617136   REFERENCES:SIMLOC
