###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 1093.264, activation diff: 1088.856, ratio: 0.996
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 1636.730, activation diff: 644.127, ratio: 0.394
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 3665.567, activation diff: 2028.838, ratio: 0.553
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 4612.440, activation diff: 946.873, ratio: 0.205
#   pulse 6: activated nodes: 11441, borderline nodes: 145, overall activation: 4982.110, activation diff: 369.670, ratio: 0.074
#   pulse 7: activated nodes: 11441, borderline nodes: 145, overall activation: 5115.109, activation diff: 132.999, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 5115.1
#   number of spread. activ. pulses: 7
#   running time: 578

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9997439   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9996278   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99953705   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99736625   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.99371815   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7497504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7496866   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.749686   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.7496197   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7496054   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   11   0.7495991   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.7495904   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.74958295   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.749566   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7495512   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7495276   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.7495189   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   18   0.7495183   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.7495122   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.7494825   REFERENCES:SIMLOC
