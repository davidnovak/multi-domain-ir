###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 1025.375, activation diff: 1023.464, ratio: 0.998
#   pulse 3: activated nodes: 7950, borderline nodes: 3537, overall activation: 1466.695, activation diff: 705.051, ratio: 0.481
#   pulse 4: activated nodes: 11267, borderline nodes: 3985, overall activation: 3955.172, activation diff: 2489.836, ratio: 0.630
#   pulse 5: activated nodes: 11429, borderline nodes: 396, overall activation: 5218.708, activation diff: 1263.536, ratio: 0.242
#   pulse 6: activated nodes: 11447, borderline nodes: 122, overall activation: 5743.977, activation diff: 525.270, ratio: 0.091
#   pulse 7: activated nodes: 11451, borderline nodes: 48, overall activation: 5937.874, activation diff: 193.897, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5937.9
#   number of spread. activ. pulses: 7
#   running time: 647

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99966645   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9994351   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99936324   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99695283   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.992848   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7996546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.79955935   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.79955035   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.79943573   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7994173   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7993846   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.799369   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.7993678   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7993616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7993292   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.79932815   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.79932797   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7993198   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   19   0.79931784   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.79931056   REFERENCES:SIMLOC
