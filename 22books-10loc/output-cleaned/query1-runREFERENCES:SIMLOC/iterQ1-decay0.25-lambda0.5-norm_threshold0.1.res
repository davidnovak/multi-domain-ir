###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 449.048, activation diff: 436.583, ratio: 0.972
#   pulse 3: activated nodes: 7868, borderline nodes: 3507, overall activation: 928.831, activation diff: 479.782, ratio: 0.517
#   pulse 4: activated nodes: 11243, borderline nodes: 4645, overall activation: 2184.255, activation diff: 1255.424, ratio: 0.575
#   pulse 5: activated nodes: 11408, borderline nodes: 768, overall activation: 3218.466, activation diff: 1034.211, ratio: 0.321
#   pulse 6: activated nodes: 11428, borderline nodes: 328, overall activation: 3935.321, activation diff: 716.855, ratio: 0.182
#   pulse 7: activated nodes: 11432, borderline nodes: 207, overall activation: 4390.704, activation diff: 455.383, ratio: 0.104
#   pulse 8: activated nodes: 11434, borderline nodes: 186, overall activation: 4665.879, activation diff: 275.176, ratio: 0.059
#   pulse 9: activated nodes: 11435, borderline nodes: 175, overall activation: 4827.764, activation diff: 161.884, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11435
#   final overall activation: 4827.8
#   number of spread. activ. pulses: 9
#   running time: 721

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99778587   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99750805   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971086   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9937535   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98758817   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.746827   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7466135   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.74653924   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7463972   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.7463146   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7461437   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.74610734   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   13   0.7460831   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.746083   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.74605435   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.7460356   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.7459927   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.7459923   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.74598837   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.74590254   REFERENCES:SIMLOC
