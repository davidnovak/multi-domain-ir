###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 17.880, activation diff: 20.380, ratio: 1.140
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 1309.144, activation diff: 1304.736, ratio: 0.997
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 2224.583, activation diff: 1032.340, ratio: 0.464
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 5701.007, activation diff: 3476.424, ratio: 0.610
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 7328.394, activation diff: 1627.387, ratio: 0.222
#   pulse 6: activated nodes: 11453, borderline nodes: 12, overall activation: 7922.186, activation diff: 593.792, ratio: 0.075
#   pulse 7: activated nodes: 11455, borderline nodes: 2, overall activation: 8121.631, activation diff: 199.444, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 8121.6
#   number of spread. activ. pulses: 7
#   running time: 1327

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99974394   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9996278   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995371   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9973665   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9937184   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8997025   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   7   0.89963275   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.899629   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.8995499   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   10   0.89954484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.89952815   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.8995266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.8995168   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89948964   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.8994695   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.8994386   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8994353   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   18   0.8994306   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   19   0.89942193   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.89941573   REFERENCES:SIMLOC
