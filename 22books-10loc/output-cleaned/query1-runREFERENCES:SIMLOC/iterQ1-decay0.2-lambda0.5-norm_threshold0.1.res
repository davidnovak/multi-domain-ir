###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 478.035, activation diff: 465.570, ratio: 0.974
#   pulse 3: activated nodes: 7868, borderline nodes: 3507, overall activation: 1019.886, activation diff: 541.851, ratio: 0.531
#   pulse 4: activated nodes: 11246, borderline nodes: 4604, overall activation: 2531.086, activation diff: 1511.199, ratio: 0.597
#   pulse 5: activated nodes: 11416, borderline nodes: 652, overall activation: 3798.993, activation diff: 1267.907, ratio: 0.334
#   pulse 6: activated nodes: 11443, borderline nodes: 229, overall activation: 4684.991, activation diff: 885.998, ratio: 0.189
#   pulse 7: activated nodes: 11448, borderline nodes: 112, overall activation: 5243.679, activation diff: 558.688, ratio: 0.107
#   pulse 8: activated nodes: 11450, borderline nodes: 55, overall activation: 5577.209, activation diff: 333.530, ratio: 0.060
#   pulse 9: activated nodes: 11451, borderline nodes: 40, overall activation: 5771.083, activation diff: 193.874, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5771.1
#   number of spread. activ. pulses: 9
#   running time: 725

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9977859   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99750805   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971092   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9937563   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9875989   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.7966156   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7963886   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.796311   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7961576   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.7960706   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7958908   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7958504   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   13   0.7958224   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.79582226   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7957947   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.7957905   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.795747   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.7957318   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.7957232   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.7956388   REFERENCES:SIMLOC
