###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 12.832, activation diff: 12.832, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 536.009, activation diff: 523.543, ratio: 0.977
#   pulse 3: activated nodes: 7868, borderline nodes: 3507, overall activation: 1209.346, activation diff: 673.338, ratio: 0.557
#   pulse 4: activated nodes: 11249, borderline nodes: 4511, overall activation: 3301.448, activation diff: 2092.102, ratio: 0.634
#   pulse 5: activated nodes: 11421, borderline nodes: 552, overall activation: 5095.607, activation diff: 1794.159, ratio: 0.352
#   pulse 6: activated nodes: 11446, borderline nodes: 160, overall activation: 6333.998, activation diff: 1238.391, ratio: 0.196
#   pulse 7: activated nodes: 11449, borderline nodes: 46, overall activation: 7091.237, activation diff: 757.239, ratio: 0.107
#   pulse 8: activated nodes: 11453, borderline nodes: 21, overall activation: 7531.298, activation diff: 440.061, ratio: 0.058
#   pulse 9: activated nodes: 11454, borderline nodes: 13, overall activation: 7781.435, activation diff: 250.137, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7781.4
#   number of spread. activ. pulses: 9
#   running time: 598

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99778605   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99750805   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9971101   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9937603   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9876141   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8961929   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.895939   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.89585483   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8956777   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.8955828   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.8953865   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89533633   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.89530027   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   14   0.89530027   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.8952911   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.8952768   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.89524865   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.8952123   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   19   0.8951932   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.89510274   REFERENCES:SIMLOC
