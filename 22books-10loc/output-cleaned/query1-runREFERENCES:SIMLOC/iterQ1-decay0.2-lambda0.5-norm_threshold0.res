###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 575.669, activation diff: 562.082, ratio: 0.976
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 1251.828, activation diff: 676.160, ratio: 0.540
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 2841.322, activation diff: 1589.493, ratio: 0.559
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 4110.825, activation diff: 1269.503, ratio: 0.309
#   pulse 6: activated nodes: 11453, borderline nodes: 12, overall activation: 4957.004, activation diff: 846.180, ratio: 0.171
#   pulse 7: activated nodes: 11453, borderline nodes: 12, overall activation: 5472.059, activation diff: 515.055, ratio: 0.094
#   pulse 8: activated nodes: 11453, borderline nodes: 12, overall activation: 5772.478, activation diff: 300.419, ratio: 0.052
#   pulse 9: activated nodes: 11453, borderline nodes: 12, overall activation: 5944.166, activation diff: 171.688, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 5944.2
#   number of spread. activ. pulses: 9
#   running time: 607

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9978783   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99771214   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9973713   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943719   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.98903096   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.79670763   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.79653025   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.79648316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79637015   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.7963195   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.79619026   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7961283   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.796121   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.7961111   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7960894   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.79608214   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7960613   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.79605424   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.7960166   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.7959245   REFERENCES:SIMLOC
