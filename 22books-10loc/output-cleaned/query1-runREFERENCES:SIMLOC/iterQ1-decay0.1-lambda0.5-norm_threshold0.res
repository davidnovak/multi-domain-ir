###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 13.587, activation diff: 13.587, ratio: 1.000
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 645.671, activation diff: 632.084, ratio: 0.979
#   pulse 3: activated nodes: 8187, borderline nodes: 3698, overall activation: 1486.516, activation diff: 840.845, ratio: 0.566
#   pulse 4: activated nodes: 11296, borderline nodes: 3109, overall activation: 3688.925, activation diff: 2202.409, ratio: 0.597
#   pulse 5: activated nodes: 11441, borderline nodes: 145, overall activation: 5473.416, activation diff: 1784.491, ratio: 0.326
#   pulse 6: activated nodes: 11453, borderline nodes: 12, overall activation: 6638.908, activation diff: 1165.492, ratio: 0.176
#   pulse 7: activated nodes: 11455, borderline nodes: 2, overall activation: 7329.815, activation diff: 690.907, ratio: 0.094
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 7724.095, activation diff: 394.279, ratio: 0.051
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 7945.344, activation diff: 221.249, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7945.3
#   number of spread. activ. pulses: 9
#   running time: 573

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.9978784   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99771214   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9973719   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9943742   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9890367   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89629686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89609873   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.89604855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89591736   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   10   0.89586246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.8957235   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8956537   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8956419   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.89564085   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8956018   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   16   0.8955925   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.8955836   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   18   0.8955734   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.8955476   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.89542365   REFERENCES:SIMLOC
