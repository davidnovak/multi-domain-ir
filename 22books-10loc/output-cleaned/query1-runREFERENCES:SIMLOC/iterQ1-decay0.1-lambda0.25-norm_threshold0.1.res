###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 16.748, activation diff: 19.248, ratio: 1.149
#   pulse 2: activated nodes: 4489, borderline nodes: 4442, overall activation: 1152.052, activation diff: 1150.140, ratio: 0.998
#   pulse 3: activated nodes: 7950, borderline nodes: 3537, overall activation: 1794.778, activation diff: 935.153, ratio: 0.521
#   pulse 4: activated nodes: 11268, borderline nodes: 3970, overall activation: 5273.529, activation diff: 3480.440, ratio: 0.660
#   pulse 5: activated nodes: 11431, borderline nodes: 350, overall activation: 7045.822, activation diff: 1772.298, ratio: 0.252
#   pulse 6: activated nodes: 11449, borderline nodes: 52, overall activation: 7740.672, activation diff: 694.850, ratio: 0.090
#   pulse 7: activated nodes: 11454, borderline nodes: 17, overall activation: 7983.233, activation diff: 242.561, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7983.2
#   number of spread. activ. pulses: 7
#   running time: 640

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.99966645   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9994351   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993633   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99695325   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   5   0.9928485   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89961267   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89950824   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.8995023   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   9   0.8993708   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8993446   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.89932317   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   12   0.89931077   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.89929676   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.899295   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.8992583   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.89925236   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.8992467   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   18   0.89924073   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.8992385   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.89923495   REFERENCES:SIMLOC
