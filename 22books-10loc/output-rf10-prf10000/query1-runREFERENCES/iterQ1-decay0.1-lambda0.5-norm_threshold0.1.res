###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (8659): 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.878488169597
#   timetable1974752univ_231, 0.87793961387
#   essentialsinmod01howegoog_452, 0.871259722538
#   millsapscollegec1996mill_75, 0.870295444709
#   frenchschoolsth02instgoog_149, 0.870125156555
#   europesincenapol00leveuoft_193, 0.866138415096
#   cu31924030898922_52, 0.865933236204
#   politicalsketche00retsrich_92, 0.864470323552
#   bostoncollegebul0405bost_132, 0.860529292131
#   miningcampsstudy00shin_121, 0.853263511429
#   bookman44unkngoog_855, 0.842373919422
#   neweasterneurop00butlgoog_162, 0.837342908825
#   essentialsinmod01howegoog_259, 0.821929121001
#   englishcyclopae05kniggoog_41, 0.820944171657
#   youngmidshipman00morrgoog_87, 0.820116364109
#   ...

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8659, borderline nodes: 8659, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 12673, borderline nodes: 4019, overall activation: 2124.111, activation diff: 2124.111, ratio: 1.000
#   pulse 2: activated nodes: 12734, borderline nodes: 109, overall activation: 4291.477, activation diff: 2423.363, ratio: 0.565
#   pulse 3: activated nodes: 12735, borderline nodes: 46, overall activation: 6199.259, activation diff: 2007.644, ratio: 0.324
#   pulse 4: activated nodes: 12735, borderline nodes: 6, overall activation: 7451.196, activation diff: 1300.953, ratio: 0.175
#   pulse 5: activated nodes: 12735, borderline nodes: 5, overall activation: 8212.767, activation diff: 785.956, ratio: 0.096
#   pulse 6: activated nodes: 12735, borderline nodes: 5, overall activation: 8661.490, activation diff: 460.884, ratio: 0.053
#   pulse 7: activated nodes: 12735, borderline nodes: 5, overall activation: 8921.812, activation diff: 266.387, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 12735
#   final overall activation: 8921.8
#   number of spread. activ. pulses: 7
#   running time: 777

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9921875   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9921663   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99216056   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99195904   REFERENCES
1   Q1   ouroldworldbackg00bearrich_13   5   0.9911133   REFERENCES
1   Q1   miningcampsstudy00shin_121   6   0.99104065   REFERENCES
1   Q1   bookman44unkngoog_855   7   0.9909129   REFERENCES
1   Q1   englishcyclopae05kniggoog_41   8   0.9907786   REFERENCES
1   Q1   encyclopediaame28unkngoog_321   9   0.9905716   REFERENCES
1   Q1   essentialsinmod01howegoog_388   10   0.9905037   REFERENCES
1   Q1   europesincenapol00leveuoft_193   11   0.9903223   REFERENCES
1   Q1   essentialsinmod01howegoog_142   12   0.9902648   REFERENCES
1   Q1   politicalsketche00retsrich_96   13   0.99003327   REFERENCES
1   Q1   shorthistoryofmo00haslrich_264   14   0.98999715   REFERENCES
1   Q1   essentialsinmod01howegoog_12   15   0.9899291   REFERENCES
1   Q1   bostoncollegebul0405bost_132   16   0.9899192   REFERENCES
1   Q1   essentialsinmod01howegoog_208   17   0.9898442   REFERENCES
1   Q1   bostoncollegebul0405bost_133   18   0.9897618   REFERENCES
1   Q1   essentialsinmod01howegoog_11   19   0.98962307   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_191   20   0.9896077   REFERENCES
