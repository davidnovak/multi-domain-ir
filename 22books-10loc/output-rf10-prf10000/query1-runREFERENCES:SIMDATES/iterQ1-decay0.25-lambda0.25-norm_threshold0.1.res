###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (8659): 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.878488169597
#   timetable1974752univ_231, 0.87793961387
#   essentialsinmod01howegoog_452, 0.871259722538
#   millsapscollegec1996mill_75, 0.870295444709
#   frenchschoolsth02instgoog_149, 0.870125156555
#   europesincenapol00leveuoft_193, 0.866138415096
#   cu31924030898922_52, 0.865933236204
#   politicalsketche00retsrich_92, 0.864470323552
#   bostoncollegebul0405bost_132, 0.860529292131
#   miningcampsstudy00shin_121, 0.853263511429
#   bookman44unkngoog_855, 0.842373919422
#   neweasterneurop00butlgoog_162, 0.837342908825
#   essentialsinmod01howegoog_259, 0.821929121001
#   englishcyclopae05kniggoog_41, 0.820944171657
#   youngmidshipman00morrgoog_87, 0.820116364109
#   ...

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8659, borderline nodes: 8659, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 12673, borderline nodes: 4019, overall activation: 1858.090, activation diff: 3186.166, ratio: 1.715
#   pulse 2: activated nodes: 12734, borderline nodes: 109, overall activation: 5594.266, activation diff: 4595.042, ratio: 0.821
#   pulse 3: activated nodes: 12735, borderline nodes: 39, overall activation: 7895.690, activation diff: 2360.562, ratio: 0.299
#   pulse 4: activated nodes: 12735, borderline nodes: 6, overall activation: 8804.204, activation diff: 919.463, ratio: 0.104
#   pulse 5: activated nodes: 12735, borderline nodes: 4, overall activation: 9134.282, activation diff: 332.440, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 12735
#   final overall activation: 9134.3
#   number of spread. activ. pulses: 5
#   running time: 627

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970703   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9970692   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9970685   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99701303   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_13   5   0.99693036   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_121   6   0.996927   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_855   7   0.9969128   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_41   8   0.9968951   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_321   9   0.9968694   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_388   10   0.99685985   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_142   11   0.9968304   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   12   0.99680126   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   13   0.99678326   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_208   14   0.99677813   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_133   15   0.99676824   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_29   16   0.9967391   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_282   17   0.99673843   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_105   18   0.99673796   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_110   19   0.9967369   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_319   20   0.9967354   REFERENCES:SIMDATES
