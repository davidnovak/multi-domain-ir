###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (8659): 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.878488169597
#   timetable1974752univ_231, 0.87793961387
#   essentialsinmod01howegoog_452, 0.871259722538
#   millsapscollegec1996mill_75, 0.870295444709
#   frenchschoolsth02instgoog_149, 0.870125156555
#   europesincenapol00leveuoft_193, 0.866138415096
#   cu31924030898922_52, 0.865933236204
#   politicalsketche00retsrich_92, 0.864470323552
#   bostoncollegebul0405bost_132, 0.860529292131
#   miningcampsstudy00shin_121, 0.853263511429
#   bookman44unkngoog_855, 0.842373919422
#   neweasterneurop00butlgoog_162, 0.837342908825
#   essentialsinmod01howegoog_259, 0.821929121001
#   englishcyclopae05kniggoog_41, 0.820944171657
#   youngmidshipman00morrgoog_87, 0.820116364109
#   ...

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8659, borderline nodes: 8659, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 12673, borderline nodes: 4019, overall activation: 2194.020, activation diff: 2194.020, ratio: 1.000
#   pulse 2: activated nodes: 12734, borderline nodes: 66, overall activation: 4504.620, activation diff: 2500.082, ratio: 0.555
#   pulse 3: activated nodes: 12735, borderline nodes: 2, overall activation: 6442.396, activation diff: 2033.323, ratio: 0.316
#   pulse 4: activated nodes: 12735, borderline nodes: 1, overall activation: 7695.436, activation diff: 1301.506, ratio: 0.169
#   pulse 5: activated nodes: 12735, borderline nodes: 1, overall activation: 8451.114, activation diff: 780.687, ratio: 0.092
#   pulse 6: activated nodes: 12735, borderline nodes: 1, overall activation: 8893.398, activation diff: 455.426, ratio: 0.051
#   pulse 7: activated nodes: 12735, borderline nodes: 1, overall activation: 9148.398, activation diff: 262.075, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 12735
#   final overall activation: 9148.4
#   number of spread. activ. pulses: 7
#   running time: 722

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9921875   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9921683   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99216354   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9919821   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_13   5   0.99112606   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_121   6   0.9910407   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_855   7   0.99091756   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_41   8   0.9907798   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_321   9   0.9905728   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_388   10   0.9905037   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_193   11   0.99039984   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_142   12   0.99026525   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_132   13   0.9900605   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_264   14   0.9900358   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.9900334   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   16   0.98994017   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_208   17   0.9898503   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_133   18   0.9897636   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_191   19   0.9897398   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_11   20   0.9896432   REFERENCES:SIMDATES
