###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (8659): 
#   graduateschoolof9192bran_51, 0.0
#   encyclopediaame28unkngoog_326, 0.0
#   youngmidshipman00morrgoog_88, 0.0
#   historyofuniteds07good_239, 1.0
#   generalhistoryfo00myerrich_787, 1.0
#   shorthistoryofmo00haslrich_98, 1.0
#   lifeofstratfordc02laneuoft_185, 1.0
#   englishcyclopae05kniggoog_40, 0.0
#   selwynhouseschoo1940selw_21, 1.0
#   cambridgemodern09protgoog_449, 1.0
#   ouroldworldbackg00bearrich_13, 0.878488169597
#   timetable1974752univ_231, 0.87793961387
#   essentialsinmod01howegoog_452, 0.871259722538
#   millsapscollegec1996mill_75, 0.870295444709
#   frenchschoolsth02instgoog_149, 0.870125156555
#   europesincenapol00leveuoft_193, 0.866138415096
#   cu31924030898922_52, 0.865933236204
#   politicalsketche00retsrich_92, 0.864470323552
#   bostoncollegebul0405bost_132, 0.860529292131
#   miningcampsstudy00shin_121, 0.853263511429
#   bookman44unkngoog_855, 0.842373919422
#   neweasterneurop00butlgoog_162, 0.837342908825
#   essentialsinmod01howegoog_259, 0.821929121001
#   englishcyclopae05kniggoog_41, 0.820944171657
#   youngmidshipman00morrgoog_87, 0.820116364109
#   ...

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 8659, borderline nodes: 8659, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 12673, borderline nodes: 4019, overall activation: 2194.020, activation diff: 2194.020, ratio: 1.000
#   pulse 2: activated nodes: 12735, borderline nodes: 67, overall activation: 5026.396, activation diff: 3021.858, ratio: 0.601
#   pulse 3: activated nodes: 12735, borderline nodes: 1, overall activation: 7251.591, activation diff: 2316.654, ratio: 0.319
#   pulse 4: activated nodes: 12735, borderline nodes: 1, overall activation: 8625.020, activation diff: 1419.013, ratio: 0.165
#   pulse 5: activated nodes: 12735, borderline nodes: 1, overall activation: 9415.110, activation diff: 812.852, ratio: 0.086
#   pulse 6: activated nodes: 12735, borderline nodes: 1, overall activation: 9857.855, activation diff: 454.135, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 12735
#   final overall activation: 9857.9
#   number of spread. activ. pulses: 6
#   running time: 769

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.984375   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98433745   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9843292   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9840142   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_13   5   0.98226917   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_121   6   0.9820814   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_855   7   0.98183906   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_41   8   0.9815599   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_321   9   0.9811459   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_193   10   0.98108375   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_388   11   0.98100746   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_132   12   0.98058456   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_142   13   0.9805305   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_264   14   0.98017776   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.98006684   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_191   16   0.9799881   REFERENCES:SIMDATES:SIMLOC
1   Q1   millsapscollegec1996mill_75   17   0.9799793   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   18   0.979896   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_92   19   0.97988915   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_208   20   0.9797061   REFERENCES:SIMDATES:SIMLOC
