/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import ciir.multi.graph.GraphTools;
import ciir.multi.sa.EmbeddedNeo4jSAProcess;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SANode;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import messif.utility.ExtendedProperties;
import messif.utility.SortedCollection;

/**
 *
 * @author david
 */
public class TrecOutput {
    
    public final static String PARAM_DELIMITER = "-";
    public final static String PARAM_VALUE_DELIMITER = "";
    public final static String FILE_SUFFIX = ".res";
    public final static String FIELD_DELIMITER = "   ";
    
    public final static String SECTION_SEPARATOR = "###################################";
    
    protected final String outDirParent;
    protected final String fileNamePrefix;
    
    protected final int topK;
    
    protected final String queryID;
    protected final String iter;
    protected String runIDPrefix;
    
    public TrecOutput(String outDirParent, String queryID, String iter, String runIDPrefix, int topK) {
        // options.addOption(Option.builder("t").longOpt("trec_output").hasArgs().numberOfArgs(5).argName("output_dir").argName("query_id").argName("iter").argName("run_id").argName("top-k").desc("if used, the output is also written out to separate files in the specified directory in the TREC format").build());
        this.outDirParent = outDirParent;
        this.queryID = queryID;
        this.iter = iter;
        this.runIDPrefix = runIDPrefix;
        this.topK = topK;
        
        this.fileNamePrefix = formatParam("", iter);
    }

    public void printResults(SAConfiguration config, ExtendedProperties settings, List<String []> initialActivatedDocs, EmbeddedNeo4jSAProcess finishedProcess, SortedMap<String, String> currentParams) throws FileNotFoundException {
        for (String nodeTypeToPrint : config.getNodeTypesToPrint()) {
            File outputFile = getOutputFile(currentParams, nodeTypeToPrint);
            printSingleTypeResults(outputFile, currentParams, settings, initialActivatedDocs, finishedProcess, new String [] {nodeTypeToPrint});
        }
        // print out also resutls of ALL the node types at the same time
        if (config.getNodeTypesToPrint().length > 1) {
            File outputFile = getOutputFile(currentParams, String.join("-", config.getNodeTypesToPrint()));
            printSingleTypeResults(outputFile, currentParams, settings, initialActivatedDocs, finishedProcess, config.getNodeTypesToPrint());
            
        }
    }

    protected void printSingleTypeResults(File outputFile, SortedMap<String, String> currentParams, ExtendedProperties settings, List<String[]> initialActivatedDocs, EmbeddedNeo4jSAProcess finishedProcess, String [] nodeTypeToPrint) throws FileNotFoundException {
        try (PrintStream out = new PrintStream(outputFile)) {
            // first print comments with all parameters (both static and variable)
            out.println(SECTION_SEPARATOR);
            out.println("# parameters: ");
            for (Map.Entry<String, String> param : currentParams.entrySet()) {
                out.println("#   " + param.getKey() + ": " + param.getValue());
            }
            
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# configuration: ");
            GraphTools.print(settings, out, "#   ");
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# initial activations (" + initialActivatedDocs.size() + "): ");
            GraphTools.printInitialActivated(initialActivatedDocs, out, "#   ");
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# spreading activation process log: ");
            out.print(finishedProcess.getProcessLog());
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# spreading activation process summary: ");
            out.print(finishedProcess.getProcessInfo("#   "));
            
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# top k results in TREC format: \n");
            printTRECStyle("",finishedProcess.getTopNodes(topK, nodeTypeToPrint), out, currentParams);
            
            out.println("\n" + SECTION_SEPARATOR);
            out.println("# top nodes: \n");
            printTRECStyle("# ",finishedProcess.getTopNodes(topK * 2), out, currentParams);
        }
    }
    
    private static String formatParam(String paramName, String value) {
        return new StringBuilder(paramName).append(PARAM_VALUE_DELIMITER).append(value).toString();
    }

    private String getAllParams(String prefix, SortedMap<String, String> currentParams) {
        StringBuilder builder = new StringBuilder(prefix);
        for (Map.Entry<String, String> param : currentParams.entrySet()) {
            if (! "relationships".equals(param.getKey()) && ! "nodestouse".equals(param.getKey())) {
                builder.append(PARAM_DELIMITER).append(formatParam(param.getKey(), param.getValue()));
            }
        }
        return builder.toString();
    }
    
    private File getOutputFile(SortedMap<String, String> currentParams, String outputNodeType) {
        return new File(prepareDir(currentParams, outputNodeType), getAllParams(fileNamePrefix, currentParams) + FILE_SUFFIX);
    }

    protected File prepareDir(SortedMap<String, String> currentParams, String outputNodeType) {
        // prepare the output directory for the TREC formatted results
        // first level is the type of output nodes (document  or  dates  or  location)
        File trecOutputDir = new File(outDirParent, outputNodeType.toLowerCase());
        
        // second level is the query, e.g. query8
        trecOutputDir = new File(trecOutputDir, formatParam("query", queryID));
        
        // third level is the type of nodes and edges that were used for the SA process
        String nextLevel = "";
        if (currentParams.containsKey("nodestouse")) {
            nextLevel += formatParam("use", currentParams.get("nodestouse"));
        }
        if (currentParams.containsKey("relationships")) {
            nextLevel += formatParam("use", currentParams.get("relationships"));
        }
        if ("".equals(nextLevel)) {
            nextLevel = "useAll";
        }
        trecOutputDir = new File(trecOutputDir, nextLevel);
        
        if (trecOutputDir.exists() && ! trecOutputDir.isDirectory()) {
            throw new IllegalArgumentException("the passed directory name already exists, but it is a file");
        } else {
            if (! trecOutputDir.exists() && trecOutputDir.mkdirs()) {
                System.err.println("creating directory: " + trecOutputDir);
            }
        }
        return trecOutputDir;
    }    
    
    protected void printTRECStyle(String prefix, SortedCollection<Map.Entry<SANode, Float>> topNodes, PrintStream output, SortedMap<String, String> currentParams) {
        //    351   Q0  FR940104-0-00001  1   42.38   run-name
        //    query_id, iter, docno, rank, sim, run_id
        final StringBuilder builder = new StringBuilder();
        String runID = getAllParams(runIDPrefix, currentParams);
        int i = 1;
        for (Map.Entry<SANode, Float> topNode : topNodes) {
            builder.append(prefix).append(queryID).append(FIELD_DELIMITER).append(iter).append(FIELD_DELIMITER)
                    .append(topNode.getKey().getNodeID()).append(FIELD_DELIMITER).append(i).append(FIELD_DELIMITER)
                    .append(topNode.getValue()).append(FIELD_DELIMITER).append(runID).append("\n");
            i ++;
        }
        output.print(builder.toString());
    }
    
}
