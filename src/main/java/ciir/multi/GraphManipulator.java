/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import org.apache.commons.csv.CSVRecord;
import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.schema.IndexDefinition;


public abstract class GraphManipulator {

    protected final GraphDatabaseService graphDb;

    public GraphManipulator(GraphDatabaseService graphDb) {
        this.graphDb = graphDb;
    }
    
    
    /**
     * Finds and returns or creates and returns node of given type with given ID.
     * @param nodeLabel
     * @param idKey
     * @param idValue
     * @return 
     */
    protected Node getOrCreate(Label nodeLabel, String idKey, String idValue) {
        //graphDb.schema().
        Node node = graphDb.findNode(nodeLabel, idKey.intern(), idValue);
        if (node == null) {
            node = graphDb.createNode(nodeLabel);
            node.setProperty(idKey.intern(), idValue);
        }
        return node;
    }
    
    /**
     * Finds and returns or creates and returns node of given type with given ID.
     * @param edgeType type (label) of edge to create
     * @param firstNode first node (direction of the edge is irrelevant)
     * @param secondNode second node (direction of the edge is irrelevant)
     * @return 
     */
    protected final Relationship getOrCreate(RelationshipType edgeType, Node firstNode, Node secondNode) {
        for (Relationship edge : firstNode.getRelationships(edgeType)) {
            if (edge.getOtherNode(firstNode).equals(secondNode)) {
                return edge;
            }
        }
        return create(edgeType, firstNode, secondNode);
    }
    
    protected final Relationship create(RelationshipType edgeType, Node firstNode, Node secondNode) {
        return secondNode.createRelationshipTo(firstNode, edgeType);
    }    
    
    /**
     * Given a parsed header of the CSV file, this method finds all fields marked as "ID" and checks
     *  that unique indexes are created.
     * @param paramsAndTypes
     * @param nodeLabels the node labels of all the IDs in the paramsAndTypes; the number of these labels says the number of expected IDs in the params
     * @return return the indexes of the field marked as "ID"
     * @throws ConstraintViolationException 
     */
    protected int [] createIndexesOnIDs(String[][] paramsAndTypes, Label ... nodeLabels) throws ConstraintViolationException {
        int [] retVal = new int [0];
        try (Transaction tx = graphDb.beginTx()) {
            // create an index on the ID property, if it does not exist
            OuterFor:
            for (int i = 0; i < paramsAndTypes.length; i++) {
                String[] paramsAndType = paramsAndTypes[i];
                if ("id".equals(paramsAndType[1])) {
                    // return this field index
                    retVal = Arrays.copyOf(retVal, retVal.length + 1);
                    retVal[retVal.length - 1] = i;
                    
                    // check that the index exists
                    for (IndexDefinition index : graphDb.schema().getIndexes(nodeLabels[retVal.length - 1])) {
                        if (paramsAndType[0].equals(index.getPropertyKeys().iterator().next())) {
                            continue OuterFor;
                        }
                    }
                    //graphDb.schema().indexFor(nodeLabel).on(paramsAndType[0]).create();
                    graphDb.schema().constraintFor(nodeLabels[retVal.length - 1]).assertPropertyIsUnique(paramsAndType[0].intern()).create();
                    System.out.println("\tcreating unique index on label " + nodeLabels[retVal.length - 1] + " and key " + paramsAndType[0].intern());
                }
            }
            tx.success();
        }
        try (Transaction tx = graphDb.beginTx()) {
            graphDb.schema().awaitIndexesOnline(10, TimeUnit.SECONDS);
            tx.success();
        }
        return retVal;
    }

    protected String[][] parseCSVHeader(Iterator<CSVRecord> inputCSV) {
        CSVRecord csvRecord = inputCSV.next();
        //String[] paramsAndTypes = inputCSV.readNext();
        String[][] retVal = new String[csvRecord.size()][];
        int i = 0;
        for (String paramAndType : csvRecord) {
            retVal[i] = new String[2];
            String[] split = paramAndType.split(":");
            retVal[i][0] = split[0];
            retVal[i][1] = (split.length > 1) ? split[1].toLowerCase() : "string";
            i++;
        }
        return retVal;
    }

    protected Object createProperty(String type, String stringValue) {
        switch (type) {
            case "int":
            case "integer":
                return Integer.valueOf(stringValue);
            case "float":
                return Float.valueOf(stringValue);
            case "label":
                return DynamicLabel.label(stringValue.intern());
            case "string":
            case "id":
            default:
                return stringValue;
        }
    }
}
