/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import org.apache.commons.csv.CSVParser;
import ciir.multi.graph.MultiDomainNode;
import ciir.multi.graph.MultiDomainEdge;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.neo4j.graphdb.Transaction;

/**
 *
 * @author david
 */
public class LoadData extends EmbeddedNeo4jApplication {

    protected final static int TX_BATCH_SIZE = 10000;
    
    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("d").longOpt("documents").hasArg().argName("text_file").desc("creates document nodes for each line in the CSV file").build());
        options.addOption(Option.builder("l").longOpt("locations").hasArg().argName("text_file").desc("creates location nodes with attributes (GPS location)").build());
        options.addOption(Option.builder("o").longOpt("doc_loc").hasArg().argName("text_file").desc("stores location-document pairs, creating corresponding nodes, if necessary").build());
        options.addOption(Option.builder("a").longOpt("doc_date").hasArg().argName("text_file").desc("stores date-document pairs, creating corresponding nodes, if necessary").build());
        options.addOption(Option.builder("e").longOpt("date_date").hasArg().argName("text_file").desc("creates edges between dates that express their level of similarity").build());
        options.addOption(Option.builder("c").longOpt("loc_loc").hasArg().argName("text_file").desc("creates edges between locations that express their level of similarity").build());
        options.addOption(Option.builder("u").longOpt("doc_doc").hasArg().argName("text_file").desc("stores document-document pairs, creating corresponding nodes, if necessary").build());
        options.addOption(Option.builder("i").longOpt("delimiter").hasArg().argName("char").desc("the delimiter of the CSV files passed - default is ','; use TAB for tabulator").build());
        options.addOption(Option.builder("s").longOpt("set_label").hasArg().argName("text_file").desc("loads a CSV file with documenet IDs and labels to set to them").build());
        options.addOption(Option.builder("p").longOpt("page_page").hasArg().argName("text_file").desc("stores page-page SIMPAGE edges, creating corresponding nodes, if necessary").build());
        options.addOption(Option.builder("n").longOpt("new_edges").desc("if used, all edges are considered as new (and unique) and their existence is not checked").build());
        for (Option option : EmbeddedNeo4jApplication.coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    /**
     * The constructor of this object will parse the options and load all specified data files.
     * @param args
     * @throws ParseException 
     */
    public LoadData(String[] args) throws ParseException {
        super(args, options);
    }

    /**
     * Takes the CSV files specified in all optional arguments and reads the data in.
     * @return the total number of lines read in
     * @throws org.apache.commons.cli.ParseException if some of the parameters are wrong
     */
    public int loadAllFiles() throws ParseException {
        int retVal = 0;
        try {
            char delimiter = ',';
            if (lineArguments.hasOption('i')) {
                String value = lineArguments.getOptionValue('i');
                if (value.length() == 1) {
                    delimiter = value.charAt(0);                
                } else if ("tab".equals(value.toLowerCase())) {
                    delimiter = '\t';
                } else {
                    System.out.println("wrong delimiter: '" + value + "'");
                    throw new ParseException("wrong delimiter: '" + value + "'");
                }
                System.out.println("delimiter: '"+delimiter+"'");
            }
            if (lineArguments.hasOption('u')) {
                retVal += loadCVS(lineArguments.getOptionValue('u'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.DOCUMENT, MultiDomainEdge.SIMDOC, MultiDomainNode.DOCUMENT, lineArguments.hasOption('n')));
            }
            if (lineArguments.hasOption('d')) {
                //retVal += loadNodes(MultiDomainNode.DOCUMENT, lineArguments.getOptionValue('d'), delimiter);
                retVal += loadCVS(lineArguments.getOptionValue('d'), delimiter, new SingleNodeLoader(graphDb, MultiDomainNode.DOCUMENT));
            }
            if (lineArguments.hasOption('o')) {
                //retVal += loadNodesAndEdges(MultiDomainNode.LOCATION, MultiDomainEdge.REFERENCES, MultiDomainNode.DOCUMENT, lineArguments.getOptionValue('o'), delimiter);
                retVal += loadCVS(lineArguments.getOptionValue('o'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.LOCATION, MultiDomainEdge.REFERENCES, MultiDomainNode.DOCUMENT, lineArguments.hasOption('n')));
            }
            if (lineArguments.hasOption('l')) {
                retVal += loadCVS(lineArguments.getOptionValue('l'), delimiter, new SingleNodeLoader(graphDb, MultiDomainNode.LOCATION));
            }
            if (lineArguments.hasOption('c')) {
                retVal += loadCVS(lineArguments.getOptionValue('c'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.LOCATION, MultiDomainEdge.SIMLOC, MultiDomainNode.LOCATION, lineArguments.hasOption('n')));
            }
            if (lineArguments.hasOption('a')) {
                retVal += loadCVS(lineArguments.getOptionValue('a'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.DATE, MultiDomainEdge.REFERENCES, MultiDomainNode.DOCUMENT, lineArguments.hasOption('n')));
            }
            if (lineArguments.hasOption('e')) {
                retVal += loadCVS(lineArguments.getOptionValue('e'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.DATE, MultiDomainEdge.SIMDATES, MultiDomainNode.DATE, lineArguments.hasOption('n')));
            }
            if (lineArguments.hasOption('p')) {
                retVal += loadCVS(lineArguments.getOptionValue('p'), delimiter, new SingleEdgeLoader(graphDb, MultiDomainNode.DOCUMENT, MultiDomainEdge.SIMPAGE, MultiDomainNode.DOCUMENT, lineArguments.hasOption('n')));
            }
        } catch (IOException ex) {
            System.out.println("error: " + ex.toString());
        }
        return retVal;
    }
    
    /**
     * 
     * @param fileName
     * @param csvDelimiter char to delimit the CSV columns
     * @param lineLoader
     * @return the number of loaded lines
     * @throws IOException 
     */
    protected final int loadCVS(String fileName, char csvDelimiter, SingleLineLoader lineLoader) throws IOException {
        System.out.println("...loading file " + fileName);
        long readTime = 0L;
        long neo4jTime = 0L;
        int retVal = 0;
        // open the file that should contain the data
        CSVFormat myFormat = CSVFormat.newFormat(csvDelimiter);
        if (csvDelimiter != '\t') {
            myFormat = myFormat.withQuote('"').withQuoteMode(QuoteMode.MINIMAL);
        }
        
        try (CSVParser csvParser = CSVParser.parse(new File(fileName), Charset.defaultCharset(), myFormat);) {
            Iterator<CSVRecord> inputCSV = csvParser.iterator();
            if (! lineLoader.parseHeader(inputCSV)) {
                throw new IllegalArgumentException("the CSV header is wrong for file: " + fileName);
            }
            
            long previousPoint = System.nanoTime();//currentTimeMillis();
            System.out.print("\tloaded entries: ");
            
            //String[] nextLine;
            while (inputCSV.hasNext()) {
                CSVRecord nextLine = inputCSV.next();
                long batchTime = System.currentTimeMillis();
                try (Transaction tx = graphDb.beginTx()) {
                    // read all data files from the file
                    do {
                        long current = System.nanoTime();//currentTimeMillis();
                        readTime += (current - previousPoint);
                        previousPoint = current;
                        
                        lineLoader.loadLine(nextLine);
                        
                        current = System.nanoTime();//currentTimeMillis();
                        neo4jTime += (current - previousPoint);
                        previousPoint = current;
                        
                        retVal ++;
                    } while ((retVal % TX_BATCH_SIZE != 0) && inputCSV.hasNext() && (nextLine = inputCSV.next()) != null);
                    System.out.printf("%s", retVal).flush();
                    tx.success();
                }
                neo4jTime += (System.nanoTime() - previousPoint);
                System.out.printf(" (COMM %s + %s = %sms), ", readTime/1000000L, neo4jTime/1000000L, (System.currentTimeMillis() -  batchTime)).flush();
                readTime = 0L;
                neo4jTime = 0L;
            }
        }
        System.out.println("\n\tfinal count: " + retVal + " entries");
        return retVal;
    }    
    
    public static void main(String[] args) {
        try {
            long startTime = System.currentTimeMillis();
            LoadData dataLoader = new LoadData(args);
            System.out.println("...initialization phase ended (running time: " + (System.currentTimeMillis() - startTime) + "ms)");

            startTime = System.currentTimeMillis();
            int totalLines = dataLoader.loadAllFiles();
            System.out.println("...data successfully loaded using arguments: " + Arrays.deepToString(args));
            System.out.println("...total number of loaded lines: " + totalLines);
            System.out.println("loading time: " + (System.currentTimeMillis() - startTime) + " ms");

            //loadData.printAllNodes(MultiDomainNode.DOCUMENT);
            //loadData.printAllNodes(MultiDomainNode.DATE);
            //loadData.printAllNodes(MultiDomainNode.LOCATION);
        } catch (ParseException ex) {
            System.err.println(ex.toString());
            printUsage(LoadData.class.getSimpleName(), args, options);
            throw new IllegalArgumentException();
        }
    }

}
