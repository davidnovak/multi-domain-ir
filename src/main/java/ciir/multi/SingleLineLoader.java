/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import java.io.IOException;
import java.util.Iterator;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author david
 */
public interface SingleLineLoader {
    
    /**
     * 
     * @param line
     * @return 
     */
    public boolean loadLine(CSVRecord csvRecord);
    
    /**
     * Parses a header of the CSV file.
     * @param inputCSV
     * @return 
     */
    public boolean parseHeader(Iterator<CSVRecord> inputCSV);
}
