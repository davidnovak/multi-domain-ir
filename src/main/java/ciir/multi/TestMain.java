/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import ciir.multi.sa.function.SigmoidFunction;

/**
 *
 * @author david
 */
public class TestMain {

    
    public static void variousSteepness() {
        float [] values = {0.1f, 0.5f, 0.75f, 1.0f, 1.5f, 2.0f, 3.0f, 4.0f, 5.0f, 10.0f};
        System.out.print("steep");
        for (float value : values) {
            System.out.print("    " + value);
        }
        System.out.println();
        for (float steep : new float [] {0.2f, 0.4f, 0.6f, 0.8f, 1.0f, 1.2f, 1.4f, 1.6f, 1.8f, 2.0f}) {
            SigmoidFunction sigmoidFunction = new SigmoidFunction(0.0f, steep);
            System.out.print(steep + "     ");
            for (float value : values) {
                //System.out.println("steepness: " + steep + ", value: " + value + ", result: " + sigmoidFunction.getNormalizedValue(value));
                System.out.print(String.format(" %.3f ", sigmoidFunction.getNormalizedValue(value)));
            }
            System.out.println();
        }        
    }
    
    public static void variousThresholds() {
        float [] values = {0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.7f, 1.0f, 1.5f, 2.0f, 3.0f, 4.0f, 5.0f, 19.0f};
        System.out.print("thresh");
        for (float value : values) {
            System.out.print("    " + value);
        }
        System.out.println();
        for (float threshold : new float [] {0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.7f, 1.0f}) {
            SigmoidFunction sigmoidFunction = new SigmoidFunction(threshold, 2.0f);
            System.out.print(threshold + "     ");
            for (float value : values) {
                //System.out.println("steepness: " + steep + ", value: " + value + ", result: " + sigmoidFunction.getNormalizedValue(value));
                System.out.print(String.format(" %.3f ", sigmoidFunction.getNormalizedValue(value)));
            }
            System.out.println();
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //variousThresholds();
        variousSteepness();
    }
    
}
