/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

/**
 * This interface is to be implemented by functions that map (non-negative) real numbers to [0,1] interval.
 * @author david
 */
public interface NormalizationFunction {
    
    public float getNormalizedValue(float value);
    
}
