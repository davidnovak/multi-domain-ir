/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import java.util.List;
import java.util.Map;
import messif.utility.SortedCollection;

/**
 * Encapsulation of the whole spreading activation process. 
 * @author david
 */
public interface SAProcess {
     
   /**
     * Executes the whole SA process.
     * @param configuration configuration of this SA process
     * @param initialDocs set of initial node IDs, with their initial activations and, optionally,
     *   their type (default: DOCUMENT)
     * @return the number of pulses realized 
     */
    public int execute(SAConfiguration configuration, List<String []> initialDocs);
            
    /**
     * 
     * @param labels the types of the node to get the top activation for; if empty, return all top
     * @param k the number of top activated nodes to be returned
     * @return 
     */
    public SortedCollection<Map.Entry<SANode, Float>> getTopNodes(int k, String ... labels);
    
       /**
     * 
     * @return list of active nodes with their level of activation (output activation) after the process
     */
    public NodeActivation getActivatedNodes();
    
    /**
     * Get the process log with lines starting with #.
     * @return 
     */
    public String getProcessLog();
    
    public String getProcessInfo(String prefix);
    
}
