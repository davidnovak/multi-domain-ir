/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;

/**
 * This interface contains SA configuration parameters that are used by the whole spreading 
 *  activation process and also in individual SA pulses.
 *  
 * @author david
 */
public interface SAConfigurationBuilder {
    
    public SAConfigurationBuilder setIDProperty(Label nodeType, String idProperty);

    public SAConfigurationBuilder setRelationshipsToUse(RelationshipType... types);

    public SAConfigurationBuilder setNodesToUse(String... labels);
    
    public SAConfigurationBuilder setWeightProperty(RelationshipType edgeType, String weightPropertyName);

    public SAConfigurationBuilder setMinWeight(RelationshipType type, float minWeight);

    public SAConfigurationBuilder setSAFunction(SAFunction function);
    
    public SAConfigurationBuilder setNumberPulses(int pulses);
    
    public SAConfigurationBuilder setActDiffRatioLimit(double actDiffRatioLimit);
    
    public SAConfigurationBuilder setActDiffRatioLimitDiff(double actDiffRatioLimitDiff);
    
    public SAConfigurationBuilder setProgressPrefix(String prefix);
    
    public SAConfigurationBuilder addThresholdToSpread(String label, float spreadThreshold);

    public SAConfigurationBuilder setNodesToPrint(String ... labels);
    
    
    public SAConfiguration build();

    // NOT USED BY THE FINAL SA PROCESS 
    
    public SAConfigurationBuilder addLabelIgnoringDecay(Label label);
    
    public SAConfigurationBuilder setLabelsToStopSpread(String... labels);
 
}
