/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import java.util.List;
import java.util.Map;

/**
 * This is an interface for the (intermediate) result of the SA process. It contains current 
 *  activation level for each node ID, SANode objects for each object ID, and list of nodes
 *  activated in the previous pulse (which need to be checked in the next iteration for neighbors).
 * @author david
 */
public interface NodeActivation {
    
    /**
     * Gets the ID-node map of all nodes that have been activated.
     * @return 
     */
    public Map<String, SANode> getActivatedNodes();
    
    public List<SANode> getBorderlineNodes();
    
    /**
     * Given a string ID of a node, this method returns it's current activation or 0f if such node
     *  is not managed by this activation pulse.
     * @param key unique node ID
     * @return
     */
    public Float get(String key);

    /**
     * @param key unique node ID
     * @param activation
     * @param previousActivation
     * @return
     */
    public Float put(String key, Float activation, NodeActivation previousActivation);
    
    /**
     * Returns the number of activated nodes (including those that were considered/tested but they 
     *  have with activation 0).
     * @return 
     */
    public int size();
    
    /**
     * Returns the sum of activations of all nodes.
     * @return 
     */
    public double getOverallActivation();
    
    /**
     * Returns the difference in activation
     * @return 
     */
    public double getActivationDiff();
    
    public double getActDiffRatio();
    
    public int getPulseNumber();
    
    public int getNonzeroCount();
    
}
