/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import org.neo4j.graphdb.Node;

/**
 * An interface of a node in the SA process. It provides access to all node's attributes to be used in the SA process.
 * @author david
 */
public interface SANode {
    
    /**
     * Gets the unique identifier of this node.
     * @return 
     */
    public String getNodeID();
    
    /**
     * Gets the node types (labels).
     * @return 
     */
    public String [] getNodeLabels();
    
    /**
     * Gets the initial activation of this node (0 for other than initial nodes)
     * @return 
     */
    public float getInitialActivation();
    
    /**
     * Number of hops from the initial nodes (may be not considering all intermediate nodes).
     * @return 
     */
    public int getDistFromInitials();
    
    /**
     * Get IDs of the neighbors to be considered.
     * @return 
     */
    public String [] getNeighbors();
    
    /**
     * Get float weights of the edges to corresponding neighboring nodes.
     * @return 
     */
    public float [] getNeighborWeights();
    
    /**
     * 
     * @return 
     */
    public float getNeighborWeightsSum();
    
    /**
     * Return the corresponding list of Neo4j nodes - this can be null.
     * @return 
     */
    public Node[] getNeighborsToProcess();
    
    /**
     * Get the number of neighboring nodes of this node.
     * @return 
     */
    public int getNeighborCount();
    
    /**
     * This method is to be called after the node is processed by the SA algorithm.
     */
    public void setNeighborsProcessed();
    
    /**
     * This method returns the actual Neo4J node representing this node; it might be null.
     * @return the Neo4j node or null
     */
    public Node getNeo4jNode();
    
    /**
     * Returns the activation threshold after which this node can spread its activation further.
     * @return the limit (set per node type)
     */
    public float getSpreadThreshold();
    
}
