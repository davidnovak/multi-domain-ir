/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

/**
 *
 * @author david
 */
public interface SAPulse {
    
    /**
     * Executes next SA iteration (pulse).
     * @param configuration configuration of this SA process
     * @param currentActivations a state of the activation before this pulse that contains: 
     *   set of activated nodes together with their output activation; the already activated nodes;
     *   a list of borderline nodes of the activated part of the graph
     * @return the state of the activation after the process
     */
    public NodeActivation execute(SAConfiguration configuration, NodeActivation currentActivations);
    
}
