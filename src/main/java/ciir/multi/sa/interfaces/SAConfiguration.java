/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

/**
 * This interface contains SA configuration parameters that are used by the whole spreading 
 *  activation process and also in individual SA pulses.
 *  
 * @author david
 */
public interface SAConfiguration {
    
    /**
     * Returns the name of the node property to contain unique node ID.
     * @param label label to get the ID property for
     * @return name of the unique ID property
     */
    public String getIDProperty(Label label);
    
    /**
     * Returns the unique ID of the given node.
     * @param node Neo4j node for which we want to find its unique ID
     * @return the unique ID of given node
     */    
    public String getID(Node node);
    
    /**
     * This method returns the relationships that should be used for given node - either all of its
     *   neighbors or just selected types of relationships.
     * @param node
     * @return  types of relationships to be used by the SA process; if null, use all
     */
    public Iterable<Relationship> getRelationshipsToUse(Node node);
    
    /**
     * Decides if given node should be used by the SA process or not. The decision is done based on the 
     *  list of labels passed in the configuration.
     * @param node node to check
     * @return true/false saying if the node should be used or skipped
     */
    public boolean useNode(Node node);
    
    /**
     * Gets the name of the edge property that should contain the float weight to be used by the SA
     *  process.
     * @param edgeType type of edge (relationship) for which to take the weight property name
     * @return name of the edge property containing the SA weight (float)
     */
    public String getWeightProperty(RelationshipType edgeType);
    
    /**
     * Get the minimum weight which must given type of relationship 
     *  have to be considered by this SA process.
     * @param type edge for which we set the minimum weight
     * @return  minimum weight for given type of relationship
     */
    public float getMinWeight(RelationshipType type);
    
    /**
     * Get the spreading activation function to be used by this spreading activation cycle (usually
     *  it's the same function for the whole process).
     * @return  
     */
    public SAFunction getSAFunction();
    
    /**
     * Returns the number of pulses (iterations) of the SA process.
     * @return 
     */
    public int getMaxPulses();
    
    /**
     * Get the limit of overall activation difference ratio under which the SA process ends.
     * @param pulseNumber actual pulse number
     * @return 
     */
    public double getActDiffRatioLimit(int pulseNumber);
    
    /**
     * Get flag saying if the SA progress should be stored back into the Graph DB.
     * @return 
     */
    public boolean storeProgressIntoDB();
    
    /**
     * If {@link #storeProgressIntoDB() } is true then this method says the prefix of labels
     *  to keep the SA info in the DB.
     * @return 
     */
    public String getProgressPrefix();
        
    /**
     * Gets a threshold for given (type of) node.
     * @param nodeLabels
     * @return 
     */
    public float getThresholdToSpread(String[] nodeLabels);

    /**
     * Returns a list of node types (as Strings) that we want to rank in the end of the SA process.
     *  For each of these types, a separate TREC file is printed out.
     * @return 
     */
    public String [] getNodeTypesToPrint();
    
    /**
     * Finds out if the activation decay works for given node or not
     * @param node Neo4j node 
     * @return true if the distance decay works for given node
     */
    public boolean isNodeDecayed(Node node);
    
    /**
     * Checks, if this node is not a "dead end"; e.g. we might not wanna spread the activation beyond
     *  ANY document node (besides the initial nodes).
     * @param nodeLabels
     * @return 
     */
    public boolean spreadBeyondThisNode(String[] nodeLabels);
    
}
