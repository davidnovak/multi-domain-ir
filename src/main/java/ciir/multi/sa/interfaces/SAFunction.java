/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

/**
 * This is an interface for all functions that implement a single spreading activation atom:
 *  calculate the (output) activation of given node based on previous activations and information
 *  about it's neighbors.
 * @author david
 */
public interface SAFunction {

    /**
     * Calculate the (output) activation of given node based on previous activations and information
     * about it's neighbors.
     *
     * @param node node to calculate the new activation for
     * @param previousActivation
     * @return 
     */
    public float getActivation(SANode node, NodeActivation previousActivation);
    
}
