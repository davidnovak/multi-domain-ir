/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.interfaces;

import messif.utility.ExtendedProperties;

/**
 *  
 * @author david
 */
public interface SAConfigPropertyReader {

    public static final String ID_PROP_PREFIX = "sa.id_property_name.";
    public static final String WEIGHT_PROP_PREFIX = "sa.weight_property.";
    public static final String NODES_TO_USE = "sa.nodes_to_use";
    public static final String RELATIONSHIPS_TO_USE = "sa.relationships_to_use";
    public static final String MIN_WEIGHTS_PREFIX = "sa.min_weights.";    
    public static final String NORM_FUNCTION = "sa.norm_function";    
    public static final String NORM_FUNCTION_INSTANCE = "norm_function";
    public static final String SA_FUNCTION = "sa.function";
    public static final String MAX_PULSES = "sa.max_pulses";
    public static final String ACT_DIFF_RATIO_LIMIT = "sa.act_diff_ratio_limit";
    public static final String ACT_DIFF_RATIO_LIMIT_DIFF = "sa.act_diff_ratio_limit_diff";
    public static final String SPREAD_THRESHOLD_PREFIX = "sa.spread_threshold.";    
    public static final String PROGRESS_PREFIX = "sa.progress_prefix";
    public static final String NODES_TO_PRINT = "sa.nodes_to_print";

    // NOT USED IN THE FINAL SA PROCESS
    public static final String DECAY_SKIP_NODES = "sa.decay_skip";
    public static final String NODES_TO_STOP_SPREAD = "sa.nodes_to_stop_spread";
    
    public void readAndFillConfigutation(ExtendedProperties properties, SAConfigurationBuilder configuration) throws IllegalArgumentException;
    
}
