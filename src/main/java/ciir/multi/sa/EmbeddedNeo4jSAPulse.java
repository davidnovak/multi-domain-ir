/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SAFunction;
import ciir.multi.sa.interfaces.SANode;
import ciir.multi.sa.interfaces.SAPulse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.neo4j.graphdb.Node;

/**
 *
 * @author david
 */
public class EmbeddedNeo4jSAPulse implements SAPulse {

 
    @Override
    public NodeActivation execute(SAConfiguration configuration, NodeActivation previousActivation) {
        // output activated nodes after this pulse
        Map<String, SANode> activatedNodes = previousActivation.getActivatedNodes();
        
        // consider only the nodes newly activated in the previous step (borderline nodes)
        List<SANode> newBorderLine = new ArrayList<>();
        for (SANode borderNode : previousActivation.getBorderlineNodes()) {
            // if the level of activation of the node was zero then stop growing in this direction...
            if (previousActivation.get(borderNode.getNodeID()) <= borderNode.getSpreadThreshold()) {
                // ...but try it next time
                //System.err.println("ADDING THE NODE TO THE BORDERLINE AGAIN " + borderNode.getNodeID());
                newBorderLine.add(borderNode);
                continue;
            }
            // check all neighbors of the border node if they have been already activated
            for (Node neighborNode : borderNode.getNeighborsToProcess()) {
                String neighborID = configuration.getID(neighborNode);
                if (activatedNodes.containsKey(neighborID)) {
                    continue;
                }
                SANode neighbor = new Neo4jSANode(neighborNode, borderNode.getDistFromInitials(), configuration, activatedNodes);
                activatedNodes.put(neighborID, neighbor);
                if (configuration.spreadBeyondThisNode(neighbor.getNodeLabels())) {
                    newBorderLine.add(neighbor);
                }
            }
            
            // mark the node as processed
            borderNode.setNeighborsProcessed();
        }
        
        // new node activation after this activation pulse
        NodeActivation outputActivation = new Neo4jNodeActivation(newBorderLine, activatedNodes, previousActivation.getPulseNumber() + 1);
        
        // recalculation of activation levels for the nodes already activated
        SAFunction function = configuration.getSAFunction();
        for (SANode node : activatedNodes.values()) {
            float activation = function.getActivation(node, previousActivation);
            outputActivation.put(node.getNodeID(), activation, previousActivation);
        }
        
        return outputActivation;
    }

}
