/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.SANode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author david
 */
public class Neo4jNodeActivation extends HashMap<String, Float> implements NodeActivation {

    protected final List<SANode> borderLineNodes;
    
    protected final Map<String, SANode> activatedNodes;

    protected double activationDiff = 0d;
    
    protected double overallActivation = 0d;
    
    protected final int pulseNumber;
    
    /**
     * This constructor is used for the initial activation pulse.
     * @param initialActivations the map of initially activated nodes
     */
    public Neo4jNodeActivation(Map<SANode, Float> initialActivations) {
        this.borderLineNodes = new ArrayList<>(initialActivations.keySet());
        this.activatedNodes = new HashMap<>(initialActivations.size());
        for (Entry<SANode, Float> node : initialActivations.entrySet()) {
            put(node.getKey().getNodeID(), node.getValue());
            this.overallActivation += node.getValue();
            this.activationDiff += node.getValue();
            activatedNodes.put(node.getKey().getNodeID(), node.getKey());
        }
        this.pulseNumber = 0;
    }
    
    /**
     * This constructor is used for every but the first activation pulse.
     * @param borderLineNodes the list of nodes to be checked
     * @param activatedNodes the list of already activated nodes
     */
    public Neo4jNodeActivation(List<SANode> borderLineNodes, Map<String, SANode> activatedNodes, int pulseNumber) {
        this.borderLineNodes = borderLineNodes;
        this.activatedNodes = activatedNodes;
        this.pulseNumber = pulseNumber;
    }

    @Override
    public Float get(String key) {
        Float get = super.get(key);
        return (get == null) ? 0f : get;
    }

    @Override
    public Float put(String key, Float activation, NodeActivation previousActivation) {
        if (activation < 0f) {
            throw new IllegalArgumentException("negative activation for node " + key + ": " + activation);
        }
        this.overallActivation += activation;
        this.activationDiff += Math.abs(activation - previousActivation.get(key));
        return super.put(key, activation);
    }
    
    @Override
    public Map<String, SANode> getActivatedNodes() {
        return activatedNodes;
    }

    @Override
    public List<SANode> getBorderlineNodes() {
        return borderLineNodes;
    }

    @Override
    public double getOverallActivation() {
        return overallActivation;
    }

    @Override
    public double getActivationDiff() {
        return activationDiff;
    }

    @Override
    public double getActDiffRatio() {
        return overallActivation == 0d ? Double.MAX_VALUE : (activationDiff / overallActivation);
    }

    @Override
    public int getPulseNumber() {
        return pulseNumber;
    }

    @Override
    public int getNonzeroCount() {
        int retVal = 0;
        for (Entry<String, Float> idAct : entrySet()) {
            if (idAct.getValue() > 0f) {
                retVal ++;
            }
        }
        return retVal;
    }
    
}
