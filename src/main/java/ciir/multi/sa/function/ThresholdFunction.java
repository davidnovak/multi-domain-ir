/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NormalizationFunction;

/**
 * This is a simple implementation of the normalization function that returns
 *  0 or 1 according to a given threshold.
 * @author david
 */
public class ThresholdFunction implements NormalizationFunction {

    public static float DEFAULT_THRESHOLD = 0.5f;
    
    protected final float threshold;

    public ThresholdFunction(float threshold) {
        this.threshold = threshold;
    }
    
    /**
     * Returns 1 if given value is larger than or equal to the {@link #threshold}, 0 otherwise.
     * @param value
     * @return 
     */
    @Override
    public float getNormalizedValue(float value) {
        return (value >= threshold) ? 1f : 0f;
    }
    
}
