/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SAFunction;
import ciir.multi.sa.interfaces.SANode;
import java.util.Arrays;

/**
 * Implementation of the SA function that considers: 1) lambda - the fraction of the activation
 * level taken from the previous step 2) decay based on the distance from the initial nodes 3)
 * normalization function to put the values in [0,1]
 *
 * @author david
 */
@Deprecated
public class DecayedSumSAFunction implements SAFunction {

    protected final NormalizationFunction normalization;

    protected final float lambda;

    protected final float oneMinusLambda;

    protected final float decay;

    protected float[] decayFactors;

    public DecayedSumSAFunction(NormalizationFunction normalization, float decay, float lambda) {
        this.normalization = normalization;
        this.lambda = lambda;
        this.oneMinusLambda = 1f - lambda;
        this.decay = decay;
        this.decayFactors = new float[]{1f, 1f};
    }

    protected float getDecayFactor(int distance) {
        while (distance >= decayFactors.length) {
            this.decayFactors = Arrays.copyOf(decayFactors, decayFactors.length + 1);
            this.decayFactors[distance] = 1f - (decay * (distance - 1));
        }
        return decayFactors[distance];
    }

    protected float getNeigborActivation(String neighborID, NodeActivation previousActivation) {
        return previousActivation.get(neighborID);
    }

    protected float getSASum(SANode node, NodeActivation previousActivation) {
        float sum = 0f;
        String[] neighbors = node.getNeighbors();
        float[] neighborWeights = node.getNeighborWeights();
        for (int i = 0; i < neighbors.length; i++) {
            Float neighborActivation = getNeigborActivation(neighbors[i], previousActivation);
            if (neighborActivation > 0f) {
                sum += neighborActivation * neighborWeights[i];
            }
        }
        return sum;
    }

    @Override
    public float getActivation(SANode node, NodeActivation previousActivation) {
        // normalization( \sum_{i\in neighbors} ( w_i * (A_i * decayFactor) ) )
        float normalizedSum = normalization.getNormalizedValue(getSASum(node, previousActivation));
        return lambda * previousActivation.get(node.getNodeID())
                + oneMinusLambda * normalizedSum * getDecayFactor(node.getDistFromInitials());
    }

}
