/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SAFunction;
import ciir.multi.sa.interfaces.SANode;
import java.util.Arrays;

/**
 * Implementation of the SA function that uses a growing lambda to manage the SA process.
 *
 * @author david
 */
@Deprecated
public class GrowingLambdaSAFunction implements SAFunction {

    protected final NormalizationFunction normalization;

    protected final float lambdaStep;

    protected float[] lambdas;

    //protected float[] oneMinusLambdas;
    
    public GrowingLambdaSAFunction(NormalizationFunction normalization, float initialLambda, float lambdaStep) {
        this.normalization = normalization;
        this.lambdaStep = lambdaStep;
        this.lambdas = new float[]{initialLambda, initialLambda + lambdaStep};
        //this.oneMinusLambdas = new float[]{1f, 1f - lambdaStep};
    }

    protected float getLambda(int pulse) {
        while (pulse >= lambdas.length) {
            this.lambdas = Arrays.copyOf(lambdas, lambdas.length + 1);
            this.lambdas[pulse] = Math.min(this.lambdas[pulse - 1] + lambdaStep, 1f);
//            if (this.lambdas[pulse] > 1.001f) {
//                throw new IllegalArgumentException("the SA process should have stopped already: lambda is growing over 1: " + this.lambdas[pulse]);
//            }
        }
        return lambdas[pulse];
    }
    
    protected float getSASum(SANode node, NodeActivation previousActivation) {
        float sum = 0f;
        String[] neighbors = node.getNeighbors();
        float[] neighborWeights = node.getNeighborWeights();
        for (int i = 0; i < neighbors.length; i++) {
            Float neighborActivation = previousActivation.get(neighbors[i]);
            if (neighborActivation > 0f) {
                sum += neighborActivation * neighborWeights[i];
            }
        }
        return sum;
    }

    @Override
    public float getActivation(SANode node, NodeActivation previousActivation) {
        // normalization( \sum_{i\in neighbors} ( w_i * (A_i * decayFactor) ) )
        float normalizedSum = normalization.getNormalizedValue(getSASum(node, previousActivation));
        float lambda = getLambda(previousActivation.getPulseNumber());
        return lambda * previousActivation.get(node.getNodeID()) + (1f - lambda) * normalizedSum;
    }

}
