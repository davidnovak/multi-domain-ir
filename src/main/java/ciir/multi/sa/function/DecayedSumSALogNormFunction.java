/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.graph.GraphTools;
import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SANode;

/**
 * Implementation of the SA function that considers:
 * 1) lambda - the fraction of the activation level taken from the previous step
 * 2) decay based on the distance from the initial nodes
 * 3) normalization function to put the values in [0,1]
 * @author david
 */
@Deprecated
public class DecayedSumSALogNormFunction extends DecayedSumSAFunction {

    protected final String labelToLog;
    
    public DecayedSumSALogNormFunction(NormalizationFunction normalization, float decay, float lambda, String labelToLog) {
        super(normalization, decay, lambda);
        this.labelToLog = labelToLog;
    }

    public DecayedSumSALogNormFunction(NormalizationFunction normalization, float decay, float lambda) {
        this(normalization, decay, lambda, null);
    }
    
    @Override
    protected float getSASum(SANode node, NodeActivation previousActivation) {
        if (node.getNeighborCount() <= 0) {
            return 0f;
        }
        float sum = super.getSASum(node, previousActivation);
        if (labelToLog == null || GraphTools.intersect(node.getNodeLabels(), labelToLog)) {
            return sum / (float) Math.log(node.getNeighborCount() + 1);
        }
        return sum;
    }
    
}
