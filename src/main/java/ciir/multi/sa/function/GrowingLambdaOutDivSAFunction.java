/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SANode;

/**
 * Implementation of the SA function that uses a growing lambda to manage the SA process.
 *
 * @author david
 */
@Deprecated
public class GrowingLambdaOutDivSAFunction extends GrowingLambdaSAFunction {

    public GrowingLambdaOutDivSAFunction(NormalizationFunction normalization, float initialLambda, float lambdaStep) {
        super(normalization, initialLambda, lambdaStep);
    }
    
    protected float getNeigborActivation(String neighborID, NodeActivation previousActivation) {
        SANode neighbor = previousActivation.getActivatedNodes().get(neighborID);
        return previousActivation.get(neighborID) / (float) neighbor.getNeighborCount();
    }

    @Override
    protected float getSASum(SANode node, NodeActivation previousActivation) {
        float sum = 0f;
        String[] neighbors = node.getNeighbors();
        float[] neighborWeights = node.getNeighborWeights();
        for (int i = 0; i < neighbors.length; i++) {
            Float neighborActivation = previousActivation.get(neighbors[i]);
            if (neighborActivation > 0f) {
                sum += neighborActivation * (neighborWeights[i] / previousActivation.getActivatedNodes().get(neighbors[i]).getNeighborWeightsSum());
            }
        }
        return sum;
    }
    
}
