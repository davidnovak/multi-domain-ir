/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.graph.GraphTools;
import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SANode;

/**
 * Implementation of the SA function that considers:
 * 1) lambda - the fraction of the activation level taken from the previous step
 * 2) decay based on the distance from the initial nodes
 * 3) normalization function to put the values in [0,1]
 * @author david
 */
@Deprecated
public class DecayedSumOutDivSAFunction extends DecayedSumSAFunction {

    /** List of labels of nodes whose output activation should be divided by their number of neighbors. If null, divide all. */
    protected final String [] labelToDiv;
    
    public DecayedSumOutDivSAFunction(NormalizationFunction normalization, float decay, float lambda, String [] labelsToDiv) {
        super(normalization, decay, lambda);
        this.labelToDiv = labelsToDiv;
    }

    public DecayedSumOutDivSAFunction(NormalizationFunction normalization, float decay, float lambda) {
        this(normalization, decay, lambda, null);
    }
    
    @Override
    protected float getNeigborActivation(String neighborID, NodeActivation previousActivation) {
        SANode neighbor = previousActivation.getActivatedNodes().get(neighborID);
        if (neighbor != null && (labelToDiv == null || GraphTools.intersect(labelToDiv, neighbor.getNodeLabels()))) {
            //System.err.println("dividing");
            return previousActivation.get(neighborID) / (float) neighbor.getNeighborCount();
        }
        //System.err.println("NOT NOT dividing");
        return previousActivation.get(neighborID);
    }

    
}
