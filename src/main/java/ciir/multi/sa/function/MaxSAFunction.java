/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SAFunction;
import ciir.multi.sa.interfaces.SANode;

/**
 * Implementation of the SA function that considers: 1) lambda - the fraction of the activation
 * level taken from the previous step 2) decay based on the distance from the initial nodes 3)
 * normalization function to put the values in [0,1]
 *
 * @author david
 */
public class MaxSAFunction implements SAFunction {

    protected final NormalizationFunction normalization;
    
    public MaxSAFunction(NormalizationFunction normalization) {
        this.normalization = normalization;
    }
   
    protected float getNeigborActivation(String neighborID, NodeActivation previousActivation) {
        return previousActivation.get(neighborID);
    }

    protected float getSASum(SANode node, NodeActivation previousActivation) {
        float sum = node.getInitialActivation();
        String[] neighbors = node.getNeighbors();
        float[] neighborWeights = node.getNeighborWeights();
        for (int i = 0; i < neighbors.length; i++) {
            Float neighborActivation = getNeigborActivation(neighbors[i], previousActivation);
            if (neighborActivation > 0f && 
                    neighborActivation > previousActivation.getActivatedNodes().get(neighbors[i]).getSpreadThreshold()) {
                sum += neighborActivation * neighborWeights[i];
            }
        }
        return sum;
    }

    @Override
    public float getActivation(SANode node, NodeActivation previousActivation) {
        //
        float normalizedSum = normalization.getNormalizedValue(getSASum(node, previousActivation));
        return Math.max(previousActivation.get(node.getNodeID()), normalizedSum);
    }

}
