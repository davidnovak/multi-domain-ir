/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa.function;

import ciir.multi.sa.interfaces.NormalizationFunction;

/**
 * This is an implementation of a parametrized sigmoid function.
 * @author david
 */
public class SigmoidFunction implements NormalizationFunction {

    public static double DEFAULT_THRESHOLD = 0.0f;
    public static double DEFAULT_STEEPNESS = 1.0f;
    
    protected final double threshold;
    protected final double steepness;

    public SigmoidFunction(double threshold, double steepness) {
        this.threshold = threshold;
        this.steepness = - steepness;
    }

    public SigmoidFunction() {
        this(DEFAULT_THRESHOLD, DEFAULT_STEEPNESS);
    }
    
    @Override
    public float getNormalizedValue(float value) {
        // 2/(1+EXP(-steepness*(VALUE-threshold)))-1
        // the result of the function can be negative, if the input is under the threshold
        return Math.max(0f, (float) ((2d / (1d + Math.exp(steepness * (value - threshold)))) - 1d));
    }
    
}
