/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.graph.GraphTools;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SAConfigurationBuilder;
import ciir.multi.sa.interfaces.SAFunction;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

/**
 * This is a common abstract implementation of a spreading activation cycle. It holds mainly 
 *  the current state of the spreading activation process.
 * @author david
 */
public class Neo4jSAConfiguration implements SAConfiguration, SAConfigurationBuilder {

    public static final String DEFAULT_ID_PROP = "id";
    
    public static final String DEFAULT_WEIGHT_PROP = "weight";
    
    public static final float DEFAULT_DECAY = 0.25f;
    
    public static final float DEFAULT_MIN_WEIGHT = 0f;
    
    public static final float DEFAULT_LAMBDA = 0.5f;
    
    public static final int DEFAULT_N_ITER = 10;
    
    
    protected Map<String, String> idProperties = null;
    
    protected Map<String,String> weightProperties = null;
    
    protected RelationshipType [] types = null;
    
    protected Map<String, Float> minWeights = null;

    protected Set<String> nodeLabelsToUse = null;
    
    protected SAFunction function;
    
    /** The decay to be used in each step of the process. The default step decay is {@link #DEFAULT_DECAY}. */
    protected float decay = DEFAULT_DECAY;
    
    protected Set<String> labelsIgnoringDecay = new HashSet<>();
    
    protected float lambda;
    
    protected int numberIterations = DEFAULT_N_ITER;
    
    protected double actDiffRatioLimit;

    protected double actDiffRatioLimitDiff;
    
    protected String progressString = null;
    
    protected Set<String> labelsToStopStread = null;
    
    protected Map<String, Float> spreadThresholds = null;
    
    protected String [] nodesToPrint = null;
    
    /*
     * An empty constructor. All parameters are to be set using the set* methods.
     */
    //public Neo4jSAConfiguration() {
    //}

    @Override
    public SAConfigurationBuilder setIDProperty(Label nodeType, String idProperty) {
        if (idProperties == null) {
            idProperties = new HashMap<>();
        }
        idProperties.put(nodeType.name().intern(), idProperty);
        return this;
    }
    
    @Override
    public SAConfigurationBuilder setWeightProperty(RelationshipType edgeType, String weightPropertyName) {
        if (weightProperties == null) {
            weightProperties = new HashMap<>();
        }
        weightProperties.put(edgeType.name().intern(), weightPropertyName);
        return this;
    }

    @Override
    public SAConfigurationBuilder setRelationshipsToUse(RelationshipType... types) {
        this.types = types;
        return this;
    }

    @Override
    public SAConfigurationBuilder setMinWeight(RelationshipType type, float minWeight) {
        if (minWeights == null) {
            minWeights = new HashMap<>();
        }
        minWeights.put(type.name().intern(), minWeight);
        return this;
    }

    @Override
    public SAConfigurationBuilder setSAFunction(SAFunction function) {
        this.function = function;
        return this;
    }
    
    @Override
    public SAConfigurationBuilder setNumberPulses(int pulses) {
        this.numberIterations = pulses;
        return this;
    }

    @Override
    public SAConfigurationBuilder setActDiffRatioLimit(double actDiffRatioLimit) {
        this.actDiffRatioLimit = actDiffRatioLimit;
        return this;
    }

    @Override
    public SAConfigurationBuilder setActDiffRatioLimitDiff(double actDiffRatioLimitDiff) {
        this.actDiffRatioLimitDiff = actDiffRatioLimitDiff;
        return this;
    }

    @Override
    public SAConfigurationBuilder setProgressPrefix(String prefix) {
        this.progressString = prefix;
        return this;
    }


    @Override
    public SAConfigurationBuilder setNodesToUse(String... labelsToUse) {
        this.nodeLabelsToUse = new HashSet<>(Arrays.asList(labelsToUse));
        return this;
    }
    
    @Override
    public SAConfigurationBuilder addThresholdToSpread(String label, float spreadThreshold) {
        if (spreadThresholds == null) {
            spreadThresholds = new HashMap<>();
        }
        spreadThresholds.put(label, spreadThreshold);
        return this;
    }

    @Override
    public SAConfigurationBuilder setNodesToPrint(String... labels) {
        this.nodesToPrint = labels;
        return this;
    }
    
    @Override
    public SAConfiguration build() {
        return this;
    }

    @Override
    public SAConfigurationBuilder addLabelIgnoringDecay(Label label) {
        labelsIgnoringDecay.add(label.name().intern());
        return this;
    }
    
    @Override
    public SAConfigurationBuilder setLabelsToStopSpread(String... labels) {
        this.labelsToStopStread = new HashSet<>(Arrays.asList(labels));
        return this;
    }

    
    // ********************   IMPLEMENTATION OF SAConfig interface     *********************** //
    
    @Override
    public String getIDProperty(Label label) {
        if (idProperties == null) {
            return DEFAULT_ID_PROP;
        }
        String idProperty = idProperties.get(label.name().intern());
        return idProperty == null ? DEFAULT_ID_PROP : idProperty;
    }
    
    @Override
    public String getID(Node node) {
        if (idProperties == null) {
            return (String) node.getProperty(DEFAULT_ID_PROP);
        }
        for (Label label : node.getLabels()) {
            String idProperty = idProperties.get(label.name().intern());
            if (idProperty == null) {
                continue;
            }
            return (String) node.getProperty(idProperty);
        }
        return (String) node.getProperty(DEFAULT_ID_PROP);
    }

    @Override
    public String getWeightProperty(RelationshipType edgeType) {
        if (weightProperties == null) {
            return DEFAULT_WEIGHT_PROP;
        }
        String weightProperty = weightProperties.get(edgeType.name().intern());
        return (weightProperty == null) ? DEFAULT_WEIGHT_PROP : weightProperty;
    }

    @Override
    public Iterable<Relationship> getRelationshipsToUse(Node node) {
        return (types == null) ? node.getRelationships() : node.getRelationships(types);
    }

    @Override
    public float getMinWeight(RelationshipType type) {
        if (minWeights == null) {
            return DEFAULT_MIN_WEIGHT;
        }
        Float minWeight = minWeights.get(type.name().intern());
        return (minWeight == null) ? DEFAULT_MIN_WEIGHT : minWeight;
    }

    @Override
    public SAFunction getSAFunction() {
        return function;
    }

    @Override
    public int getMaxPulses() {
        return numberIterations;
    }

    @Override
    public double getActDiffRatioLimit(int pulseNumber) {
        return actDiffRatioLimit + actDiffRatioLimitDiff * pulseNumber;
    }

    @Override
    public boolean storeProgressIntoDB() {
        return progressString != null;
    }

    @Override
    public String getProgressPrefix() {
        return progressString;
    }

    @Override
    public boolean useNode(Node node) {
        if (nodeLabelsToUse == null) {
            return true;
        }
        return GraphTools.intersectLabels(nodeLabelsToUse, node);
    }

    @Override
    public float getThresholdToSpread(String[] nodeLabels) {
        if (spreadThresholds == null) {
            return 0f;
        }
        for (String nodeLabel : nodeLabels) {
            Float value = spreadThresholds.get(nodeLabel);
            if (value != null) {
                return value;
            }
        }
        return 0f;
    }

    @Override
    public String[] getNodeTypesToPrint() {
        return nodesToPrint;
    }

    /// **************   NOT USED IN THE FINAL SA PROCESS   *********************** //
    
    @Override
    public boolean isNodeDecayed(Node node) {
        if (labelsIgnoringDecay.isEmpty()) {
            return true;
        }
        return ! GraphTools.intersectLabels(labelsIgnoringDecay, node);
    }

    @Override
    public boolean spreadBeyondThisNode(String[] nodeLabels) {
        if (labelsToStopStread == null) {
            return true;
        }
        return ! GraphTools.intersect(labelsToStopStread, nodeLabels);
    }    
    
}
