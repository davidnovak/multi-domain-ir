/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.graph.GraphTools;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SANode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Relationship;

/**
 *
 * @author david
 */
public class Neo4jSANode implements SANode {
    
    /** The unique ID of this node. */  
    protected final String nodeID;
    
    protected final String [] nodeLabels;
    
    protected final String [] neighborIDs;
    
    protected final float [] neighborWeights;

    protected final float neighborWeightSum;
    
    protected final int distFromInitials; 
    
    /** 
     * The list of neighboring Neo4j nodes that are used in the next pulse after this node is created.
     * Once the neighbors are processed, this array is set to null.
     */
    protected Node [] neigborsToProcess;
    
    protected Node thisNeo4jNode;
    
    protected final float spreadThreshold;
    
    private final float initialActivation;
    
    
    protected Neo4jSANode(Node neo4jNode, SAConfiguration configuration, int distFromInitials, Map<String, SANode> activatedNodes, float initialActivation) throws NotFoundException, ClassCastException {
        // get the node's id
        if (configuration.storeProgressIntoDB()) {
            this.thisNeo4jNode = neo4jNode;
        }
        this.nodeID = configuration.getID(neo4jNode);
        this.nodeLabels = GraphTools.getLabelNames(neo4jNode);
        this.initialActivation = initialActivation;
        
        Iterable<Relationship> relationships = configuration.getRelationshipsToUse(neo4jNode);
        
        List<String> neighborIDList = new ArrayList<>();
        List<Float> weightsList = new ArrayList<>();
        List<Node> neighborsToProcessList = new ArrayList<>();
        for (Relationship edge : relationships) {
            Node otherNode = edge.getOtherNode(neo4jNode);
            String neighborID = configuration.getID(otherNode); 
            float weight = (float) edge.getProperty(configuration.getWeightProperty(edge.getType()));
            
            // first check that the weight is over the limit and that the 
            if (activatedNodes != null && ! activatedNodes.containsKey(neighborID) && 
                    (! configuration.useNode(otherNode) ||  weight < configuration.getMinWeight(edge.getType()))) {
                continue;
            }
            neighborIDList.add(neighborID);
            weightsList.add(weight);
            if (activatedNodes == null || ! activatedNodes.containsKey(neighborID)) {
                neighborsToProcessList.add(otherNode);
            }
        }
        this.neighborIDs = neighborIDList.toArray(new String [neighborIDList.size()]);
        this.neigborsToProcess = neighborsToProcessList.toArray(new Node [neighborsToProcessList.size()]);
        this.neighborWeights = new float [weightsList.size()];
        int i = 0;
        for (Float weight : weightsList) {
            neighborWeights[i ++] = weight;
        }
        this.neighborWeightSum = GraphTools.sumFloats(neighborWeights);
        
        // set the distance from the initial nodes
        this.distFromInitials = distFromInitials;
        
        // sets the activation spread threshold 
        this.spreadThreshold = configuration.getThresholdToSpread(nodeLabels);
    }
    
    /**
     * This constructor is used for any but the initial node.
     * @param neo4jNode
     * @param parentDist this node's parent distance from the initial nodes
     * @param configuration
     * @param activatedNodes the map of already activated nodes
     * @throws NotFoundException if any of the neighbors does not contain ID property or the edge 
     *   does not contain given weight property
     * @throws ClassCastException if any of the checked properties is not of the right type
     */
    public Neo4jSANode(Node neo4jNode, int parentDist, SAConfiguration configuration, Map<String, SANode> activatedNodes) throws NotFoundException, ClassCastException {
        this(neo4jNode, configuration, (configuration.isNodeDecayed(neo4jNode)) ? parentDist + 1: parentDist, activatedNodes, 0f);
    }
    
    /**
     * This constructor is used for the initial set of nodes to be activated.
     * @param neo4jNode
     * @param configuration
     * @param initialActivation
     * @throws NotFoundException
     * @throws ClassCastException 
     */
    public Neo4jSANode(Node neo4jNode, SAConfiguration configuration, float initialActivation) throws NotFoundException, ClassCastException {
        this(neo4jNode, configuration, 0, null, initialActivation);
    }
    
    @Override
    public String[] getNeighbors() {
        return neighborIDs;
    }

    @Override
    public float[] getNeighborWeights() {
        return neighborWeights;
    }

    @Override
    public float getNeighborWeightsSum() {
        return neighborWeightSum;
    }
    
    @Override
    public Node[] getNeighborsToProcess() {
        return neigborsToProcess;
    }
    
    @Override
    public String getNodeID() {
        return nodeID;
    }

    @Override
    public String[] getNodeLabels() {
        return nodeLabels;
    }

    @Override
    public int getNeighborCount() {
        return neighborIDs.length;
    }
    
    @Override
    public void setNeighborsProcessed() {
//        this.neighborsProcessed = true;
        this.neigborsToProcess = null;
    }

    @Override
    public int getDistFromInitials() {
        return distFromInitials;
    }

    /**
     * Can return null, if the process will not store the results back into the MapDB.
     * @return 
     */
    @Override
    public Node getNeo4jNode() {
        return thisNeo4jNode;
    }

    @Override
    public float getSpreadThreshold() {
        return spreadThreshold;
    }

    @Override
    public float getInitialActivation() {
        return initialActivation;
    }
    
}
