/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.graph.MultiDomainNode;
import ciir.multi.graph.GraphTools;
import ciir.multi.graph.NodeActivationPair;
import ciir.multi.sa.interfaces.NodeActivation;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SANode;
import ciir.multi.sa.interfaces.SAProcess;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import messif.utility.SortedCollection;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

/**
 *
 * @author david
 */
public class EmbeddedNeo4jSAProcess implements SAProcess {

    /**
     * The embedded Neo4J DB.
     */
    protected final GraphDatabaseService graphDb;

    protected NodeActivation currentActivation;
    
    protected StringBuilder processingLog = new StringBuilder();
    
    protected long runningTime;
    
    protected int numberPulses = 0;
    
    /**
     * Creates a new Neo4J process given an instance of the Neo4J DB.
     * @param graphDb 
     */
    public EmbeddedNeo4jSAProcess(GraphDatabaseService graphDb) {
        this.graphDb = graphDb;
    }

    private Map<SANode, Float> createInitialNodes(List<String []> initialDocs, SAConfiguration configuration) {
        HashMap<SANode, Float> retVal = new HashMap<>();

        for (String[] initDoc : initialDocs) {
            Label label = initDoc.length > 2 ? DynamicLabel.label(initDoc[2]) : MultiDomainNode.DOCUMENT;
            String idProperty = configuration.getIDProperty(label);
            Node docNode = graphDb.findNode(label, idProperty, initDoc[0]);
            if (docNode == null) {
                System.err.println("CANNOT FIND INITIAL NODE of type " + label.name() + " with " + idProperty + " '" +initDoc[0]+"'");
            } else {
                retVal.put(new Neo4jSANode(docNode, configuration, Float.valueOf(initDoc[1])), Float.valueOf(initDoc[1]));
            }
        }
        return retVal;
    }
                
    
    @Override
    public int execute(SAConfiguration configuration, List<String []> initialDocs) {
        long startTime = System.currentTimeMillis();
        this.numberPulses = 0;
        try (Transaction tx = graphDb.beginTx()) {
            currentActivation = new Neo4jNodeActivation(createInitialNodes(initialDocs, configuration));
            EmbeddedNeo4jSAPulse saPulse = new EmbeddedNeo4jSAPulse();
            while (continueSAProcess(configuration)) {
                currentActivation = saPulse.execute(configuration, currentActivation);
                numberPulses ++;
            }
            tx.success();
        }
        runningTime = System.currentTimeMillis() - startTime;
        return numberPulses;
    }

    private boolean continueSAProcess(SAConfiguration configuration) {
        logAfterPulse(configuration);
        boolean continueProcess;
        if (numberPulses >= configuration.getMaxPulses()) {
            continueProcess = false;
        } else {
            // check the activation diff ratio
            continueProcess = (currentActivation.getActDiffRatio() > configuration.getActDiffRatioLimit(numberPulses));
        }
        if (configuration.storeProgressIntoDB()) {
            storeProgressIntoDB(configuration.getProgressPrefix(), ! continueProcess);
        }        
        return continueProcess;
    }

    protected void logAfterPulse(SAConfiguration configuration) {
        processingLog.append("#   pulse ").append(numberPulses).append(":");
        processingLog.append(" activated: ").append(currentActivation.size());
        processingLog.append(", non-zero: ").append(currentActivation.getNonzeroCount());
        processingLog.append(", borderline: ").append(currentActivation.getBorderlineNodes().size());
        processingLog.append(", overall act: ").append(String.format("%.3f", currentActivation.getOverallActivation()));
        processingLog.append(", act diff: ").append(String.format("%.3f", currentActivation.getActivationDiff()));
        processingLog.append(", ratio: ").append(currentActivation.getActDiffRatio() == Double.MAX_VALUE ? "Infinity" : String.format("%.3f", currentActivation.getActDiffRatio()));
        processingLog.append('\n');        
    }
    
    protected void storeProgressIntoDB(String prefix, boolean lastStep) {
        Label label = lastStep ? DynamicLabel.label(prefix + "ACTIVATED") :
                DynamicLabel.label(prefix + "ACTIVATED_pulse_" + numberPulses);
        String property = lastStep ? prefix + "activation" :
                prefix + "activation_pulse_" + numberPulses;
        for (Entry<String, SANode> node : currentActivation.getActivatedNodes().entrySet()) {
            Node neo4jNode = node.getValue().getNeo4jNode();
            if (neo4jNode == null) {
                continue;
            }
            neo4jNode.addLabel(label);
            neo4jNode.setProperty(property, currentActivation.get(node.getKey()));
        }        
    }
    
    @Override
    public NodeActivation getActivatedNodes() {
        return currentActivation;
    }   

    @Override
    public SortedCollection<Entry<SANode, Float>> getTopNodes(int k, String ... labels) {
        SortedCollection<Entry<SANode, Float>> retVal = new SortedCollection<>(k, k, 
                (Entry<SANode, Float> o1, Entry<SANode, Float> o2) -> o2.getValue().compareTo(o1.getValue()));
        
        for (Entry<String, SANode> node : currentActivation.getActivatedNodes().entrySet()) {
            if (labels.length == 0 || GraphTools.intersect(labels, node.getValue().getNodeLabels())) {
                float activation = currentActivation.get(node.getKey());
                if (! retVal.isFull() || retVal.last().getValue() < activation) {
                    retVal.add(new NodeActivationPair(node.getValue(), activation));
                }
            }
        }
        
        return retVal;
    }

    @Override
    public String getProcessLog() {
        return processingLog.toString();
    }

    @Override
    public String getProcessInfo(String prefix) {
        StringBuilder builder = new StringBuilder();
        builder.append(prefix).append("final number of activated nodes: ").append(currentActivation.size()).append('\n');
        builder.append(prefix).append("final overall activation: ").append(String.format("%.1f", currentActivation.getOverallActivation())).append('\n');
        builder.append(prefix).append("number of spread. activ. pulses: ").append(numberPulses).append('\n');
        builder.append(prefix).append("running time: ").append(runningTime).append('\n');

        return builder.toString();
    }

}
