/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.sa;

import ciir.multi.graph.MultiDomainEdge;
import ciir.multi.graph.MultiDomainNode;
import ciir.multi.sa.interfaces.NormalizationFunction;
import ciir.multi.sa.interfaces.SAConfigPropertyReader;
import ciir.multi.sa.interfaces.SAConfigurationBuilder;
import ciir.multi.sa.interfaces.SAFunction;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.utility.ExtendedProperties;
import messif.utility.reflection.InstantiatorSignature;
import messif.utility.reflection.NoSuchInstantiatorException;
import org.neo4j.graphdb.RelationshipType;


public class SAConfigReaderImpl implements SAConfigPropertyReader {
    
    
    @Override
    public void readAndFillConfigutation(ExtendedProperties properties, SAConfigurationBuilder configuration) throws IllegalArgumentException {
        try {
            // read and set the ID properties for each type of the node
            ExtendedProperties idProperties = ExtendedProperties.restrictProperties(properties, ID_PROP_PREFIX);
            idProperties.stringPropertyNames().stream().forEach((prop) -> {
                configuration.setIDProperty(MultiDomainNode.valueOf(prop), idProperties.getProperty(prop));
            });

            // read and set the properties containing the "weight" for all relationships
            ExtendedProperties weightProps = ExtendedProperties.restrictProperties(properties, WEIGHT_PROP_PREFIX);
            weightProps.stringPropertyNames().stream().forEach((prop) -> {
                configuration.setWeightProperty(MultiDomainEdge.valueOf(prop), weightProps.getProperty(prop));
            });
            
            // restrict the relationships to be used by the SA algorithm
            String relsToUse = properties.getProperty(RELATIONSHIPS_TO_USE);
            if (relsToUse != null) {
                List<RelationshipType> relationships = new ArrayList<>();
                Arrays.stream(relsToUse.split("\\s*:\\s*")).forEach((relation) -> {
                    relationships.add(MultiDomainEdge.valueOf(relation));
                });
                configuration.setRelationshipsToUse(relationships.toArray(new RelationshipType[0]));
            }
            
            // read and set the minimum weights for individual edges
            ExtendedProperties minWeightProps = ExtendedProperties.restrictProperties(properties, MIN_WEIGHTS_PREFIX);
            minWeightProps.stringPropertyNames().stream().forEach((prop) -> {
                configuration.setMinWeight(MultiDomainEdge.valueOf(prop), Float.valueOf(minWeightProps.getProperty(prop)));
            });
            
            // normalization function
            NormalizationFunction normFtion = InstantiatorSignature.createInstanceWithStringArgs(
                    properties.getProperty(NORM_FUNCTION), NormalizationFunction.class, Collections.EMPTY_MAP);
            
            // SA function
            SAFunction function = InstantiatorSignature.createInstanceWithStringArgs(
                    properties.getProperty(SA_FUNCTION), SAFunction.class, Collections.singletonMap(NORM_FUNCTION_INSTANCE, normFtion));
            configuration.setSAFunction(function);

            // set the node types that should be used
            String nodeLabelsToUse = properties.getProperty(NODES_TO_USE);
            if (nodeLabelsToUse != null) {
                String[] labelsToUse = nodeLabelsToUse.split("\\s*:\\s*");
                if (labelsToUse.length > 0) {
                    configuration.setNodesToUse(labelsToUse);
                }
            }
            
            // set the node types that should be ignored by the decay 
            // (they do not count into node distance from initial nodes)
            String nodesToSkipDecay = properties.getProperty(DECAY_SKIP_NODES);
            if (nodesToSkipDecay != null) {
                Arrays.stream(nodesToSkipDecay.split("\\s*:\\s*")).forEach((nodeType) -> {
                    configuration.addLabelIgnoringDecay(MultiDomainNode.valueOf(nodeType));
                });
            }
            
            // set the maximal number of iterations (pulses)
            configuration.setNumberPulses(properties.getIntProperty(MAX_PULSES, 10));
            
            // set the maximal activation difference limit
            configuration.setActDiffRatioLimit(Double.valueOf(properties.getProperty(ACT_DIFF_RATIO_LIMIT, "0.0")));

            // set the maximal activation difference limit
            configuration.setActDiffRatioLimitDiff(Double.valueOf(properties.getProperty(ACT_DIFF_RATIO_LIMIT_DIFF, "0.0")));
            
            // set the prefix for SA monitoring
            String property = properties.getProperty(PROGRESS_PREFIX);
            if (property != null && ! property.equals("") && ! property.equals(" ")) {
                configuration.setProgressPrefix(property);
            }
            
            // set the node types that should stop the spreading activation
            String nodeLabelsToStopSpread = properties.getProperty(NODES_TO_STOP_SPREAD);
            if (nodeLabelsToStopSpread != null) {
                String[] labelsToUse = nodeLabelsToStopSpread.split("\\s*:\\s*");
                if (labelsToUse.length > 0) {
                    configuration.setLabelsToStopSpread(labelsToUse);
                }
            }
            
            // read and set the minimum weights for individual edges
            ExtendedProperties spreadThreshProps = ExtendedProperties.restrictProperties(properties, SPREAD_THRESHOLD_PREFIX);
            spreadThreshProps.stringPropertyNames().stream().forEach((prop) -> {
                configuration.addThresholdToSpread(MultiDomainNode.valueOf(prop).name(), Float.valueOf(spreadThreshProps.getProperty(prop)));
            });
            
            // types of nodes info about which is to be printed out after the SA process ends
            String nodeLabelsToPrint = properties.getProperty(NODES_TO_PRINT);
            if (nodeLabelsToPrint != null) {
                String[] labelsToPrint = nodeLabelsToPrint.split("\\s*:\\s*");
                if (labelsToPrint.length > 0) {
                    configuration.setNodesToPrint(labelsToPrint);
                }
            }
            
            
        } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
            Logger.getLogger(SAConfigReaderImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalArgumentException(ex);
        }
        
    }
    
}
