/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

/**
 *
 * @author david
 */
public abstract class EmbeddedNeo4jApplication {

    protected static final Options coreOptions;

    static {
        coreOptions = new Options();
        coreOptions.addOption(Option.builder("b").longOpt("database").hasArg().argName("directory").desc("directory with the Neo4j database").required().build());
    }

    protected static void printUsage(String javaClassName, String[] args, Options options) {
        HelpFormatter formatter = new HelpFormatter();
        try (PrintWriter printWriter = new PrintWriter(System.err)) {
            formatter.printHelp(printWriter, 100, javaClassName, null, options, HelpFormatter.DEFAULT_LEFT_PAD, HelpFormatter.DEFAULT_DESC_PAD, null, true);
        }
        System.err.println("\nexecuted with arguments: " + Arrays.toString(args));
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }
    

    protected final GraphDatabaseService graphDb;

    protected final CommandLine lineArguments;
    
    public final GraphDatabaseService getGraphDb() {
        return graphDb;
    }

    public EmbeddedNeo4jApplication(String[] args, Options options) throws ParseException {
        lineArguments = new DefaultParser().parse(options, args);
        String dbPath = lineArguments.getOptionValue('b');
        this.graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(new File(dbPath));
        registerShutdownHook(graphDb);
    }

    public EmbeddedNeo4jApplication(String[] args) throws ParseException {
        this(args, coreOptions);
    }
    
    // ********************      Helper methods    ***************************** //

    
    protected void printAllNodes(Label nodeType) {
        try (Transaction tx = graphDb.beginTx()) {
            ResourceIterator<Node> allDocs = graphDb.findNodes(nodeType);
            while (allDocs.hasNext()) {
                System.out.println(nodeToString(allDocs.next()));
            }
            tx.success();
        }
    }
    
    
    public static final String nodeToString(Node node) {
        StringBuilder builder = new StringBuilder();
        node.getAllProperties().entrySet().stream().forEach((entry) -> {
            builder.append(entry.getKey()).append(": ").append(entry.getValue().toString()).append(", ");
        });        
        return builder.toString();
    }
    
}
