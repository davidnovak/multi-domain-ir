/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import ciir.multi.graph.GraphTools;
import ciir.multi.graph.MultiDomainNode;
import ciir.multi.sa.EmbeddedNeo4jSAProcess;
import ciir.multi.sa.Neo4jSAConfiguration;
import ciir.multi.sa.SAConfigReaderImpl;
import ciir.multi.sa.interfaces.SAConfiguration;
import ciir.multi.sa.interfaces.SAConfigurationBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import messif.utility.ExtendedProperties;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author david
 */
public class RunSA extends EmbeddedNeo4jApplication {

    public static final String ITERATIONS_SPLITTER = "_________________________________________________________________\n\n";
    
    protected static final Options options;

    static {
        options = new Options();
        options.addOption(Option.builder("c").longOpt("configuration").hasArg().argName("property_file").desc("configuration of the SA algorithm").required().build());
        options.addOption(Option.builder("a").longOpt("activation").hasArg().argName("text_file").desc("list of activated documents with their initial level of activation").required().build());
        options.addOption(Option.builder("p").longOpt("parameter").hasArgs().numberOfArgs(2).argName("<param_name> <param_values>").desc("sets several values to be tested for a specified parameter\n\tthat is written as <paramer> in the config file").build());
        options.addOption(Option.builder("t").longOpt("trec_output").hasArgs().numberOfArgs(3).argName("output_dir run_id_pref top-k").desc("if used, the output is also written out to separate files in the specified directory in the TREC format").build());
        options.addOption(Option.builder("o").longOpt("text_out").desc("if used, the output is also written to stdout in human readable form").build());
        //    351   Q0  FR940104-0-00001  1   42.38   run-name
        //    query_id, iter, docno, rank, sim, run_id
        for (Option option : EmbeddedNeo4jApplication.coreOptions.getOptions()) {
            options.addOption(option);
        }
    }

    public static final String DEFAULT_CONFIG_PROPS = "default-config.properties";

    protected final ExtendedProperties configProperties;
    
    protected final String [] paramNames;
    
    protected final String [] [] paramValueDomains;
    
    protected final String [] initialFiles;
    
    protected String trecOutDir = null;
    protected String queryID = null;
    protected String trecIter = null;
    protected String runIDPrefix = null;
    protected int trecK;

    protected int [] currentStepParamIndexes;

    protected ExtendedProperties currentStepProps = null;
    
    protected SAConfiguration currentStepSAConfig;

    protected List<String []> currentInitialNodes;
    
    /**
     * The constructor of this object will parse the options and load all specified data files.
     *
     * @param args
     * @throws ParseException
     * @throws java.io.IOException
     */
    public RunSA(String[] args) throws ParseException, IOException, IllegalArgumentException {
        super(args, options);
        try {
            configProperties = readConfigProperties(lineArguments.getOptionValue('c'));
            
            // read the variable values and parse them
            String[] optionValues = lineArguments.getOptionValues('p');
            if (optionValues == null) {
                paramNames = null;
                paramValueDomains = null;
                currentStepParamIndexes = null;
            } else {
                paramNames = new String [optionValues.length / 2];
                paramValueDomains = new String [optionValues.length / 2] [];
                currentStepParamIndexes = new int[optionValues.length / 2];
                for (int i = 0; i < optionValues.length; i++) {
                    if (i % 2 == 0) {
                        paramNames[i / 2] = optionValues[i];
                    } else {
                        paramValueDomains[i / 2] = optionValues[i].split(",");
                        currentStepParamIndexes[i / 2] = 0;
                    }
                }
            }
            
            initialFiles = lineArguments.getOptionValues('a');
            if (lineArguments.hasOption('t')) {
                String [] trecParams = lineArguments.getOptionValues('t');
                trecOutDir = trecParams[0];
                runIDPrefix = trecParams[1];
                trecK = Integer.valueOf(trecParams[2]);
            }
            
        } catch (IOException ex) {
            System.err.println("error: " + ex.toString());
            throw ex;
        }

    }

    
    protected final ExtendedProperties readConfigProperties(String fileName) throws IOException {
        // create the default properties from the inside of the jar file
        Properties defaultProperties = new Properties();
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(DEFAULT_CONFIG_PROPS)) {
            defaultProperties.load(inputStream);
        }

        // read the properties from given file
        ExtendedProperties configProps = new ExtendedProperties(defaultProperties);
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            configProps.load(fileInputStream);
        }
        return configProps;
    }
    
    /**
     * Reads the text file that is expected to have the following format:
     * 1 RF1-1 frenchassemblyof00curt_146 1.0
     * 1 RF1-1 frenchassemblyof00curt_43 1.0
     * 1 RF1-1 1848 1.0 DATE 
     *
     * @param fileName
     * @return
     * @throws java.io.IOException
     * @throws IllegalArgumentException if the data is not of the proper format
     */
    protected final List<String []> readInitialActivations(String fileName) throws IOException, IllegalArgumentException {
        try (CSVParser csvParser = CSVParser.parse(new File(fileName), Charset.defaultCharset(), CSVFormat.newFormat(' '))) {
            
            List<String []> retVal = new ArrayList<>();
            for (CSVRecord record : csvParser) {
                if (record.size() < 4) {
                    throw new IllegalArgumentException("The file name '" + fileName + "' is supposed to have format: query_id iter docno activation [TYPE]: " + record);
                }
                ArrayList<String> arrayList = new ArrayList(record.size() - 2);
                this.queryID = record.get(0);
                this.trecIter = record.get(1);
                int i = 2;
                while (i < record.size()) {
                    arrayList.add(record.get(i ++));
                }
                retVal.add(arrayList.toArray(new String [0]));
            }
            return retVal;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
    
    protected void runAllSACombinations() throws FileNotFoundException, IOException {
        // print info
        System.err.println("...to run spreading activation with the following parameters: ");
        System.err.println("param names: " + Arrays.toString(paramNames));
        System.err.println("param domains: " + Arrays.deepToString(paramValueDomains));
        System.err.println("initial files: " + Arrays.deepToString(initialFiles));
        
        for (String initialFile : initialFiles) {
            this.currentInitialNodes = readInitialActivations(initialFile);
            // prepare the output directory for the TREC formatted results
            TrecOutput trecOutput = trecOutDir == null ? null : 
                    new TrecOutput(trecOutDir, queryID, trecIter, runIDPrefix, trecK);

            if (currentStepParamIndexes != null) {
                Arrays.fill(currentStepParamIndexes, 0);
            }
            currentStepSAConfig = null;
            SortedMap<String, String> currentParams = instantiateNextStepConfig();
            while (currentParams != null) {
                EmbeddedNeo4jSAProcess finishedProcess = runSpreadingActivation();

                printInfoErr(finishedProcess);
                if (lineArguments.hasOption('o')) {
                    printInfoOut(finishedProcess);
                }
                if (trecOutput != null) {
                    trecOutput.printResults(currentStepSAConfig, currentStepProps, currentInitialNodes, finishedProcess, currentParams);
                }

                currentParams = instantiateNextStepConfig();
            }
            
        }
        
    }    

    /**
     * Finds next parameter combination and instantiates the {@link #currentStepProps} and {@link #currentStepSAConfig}.
     * @return the map of current parameters
     */
    protected SortedMap<String,String> instantiateNextStepConfig() {
        SortedMap<String,String> currentParams = new TreeMap<>();
        // if there are no params values to iterate over
        if (paramNames == null) {
            if (currentStepProps == null) {
                currentStepProps = configProperties;
            } else {
                currentStepProps = null;
            }
        } else {
            currentStepProps = null;
            // get next combination of parameters
            int paramIdxToModify = currentStepParamIndexes.length - 1;
            do {
                if (currentStepSAConfig != null && 
                        ++ currentStepParamIndexes[paramIdxToModify] >= paramValueDomains[paramIdxToModify].length) {
                    currentStepParamIndexes[paramIdxToModify] = 0;
                    paramIdxToModify --;
                } else {
                    // prepare the current values of variables as String,String map
                    for (int i = 0; i < paramNames.length; i++) {
                        currentParams.put(paramNames[i], paramValueDomains[i][currentStepParamIndexes[i]]);
                    }
                    currentStepProps = ExtendedProperties.restrictProperties(configProperties, "", currentParams);
                }
            } while (currentStepProps == null && paramIdxToModify >= 0);
        }
        
        if (currentStepProps == null) {
            return null;
        }
        
        // build the configuration object
        SAConfigurationBuilder configBuilder = new Neo4jSAConfiguration();
        new SAConfigReaderImpl().readAndFillConfigutation(currentStepProps, configBuilder);
        System.err.println("using param combination: " + Arrays.toString(currentStepParamIndexes));
        
        this.currentStepSAConfig = configBuilder.build();
        return currentParams;
    }
    
    protected EmbeddedNeo4jSAProcess runSpreadingActivation() {
        EmbeddedNeo4jSAProcess saProcess = new EmbeddedNeo4jSAProcess(graphDb);
        saProcess.execute(currentStepSAConfig, currentInitialNodes);
        
        return saProcess;
    }

    protected void printInfoErr(EmbeddedNeo4jSAProcess finishedProcess) {
        printParamsAndOutput(finishedProcess, System.err);        
        System.err.println(ITERATIONS_SPLITTER);
    }
    
    protected void printInfoOut(EmbeddedNeo4jSAProcess finishedProcess) {
        printParamsAndOutput(finishedProcess, System.out);

        System.out.println("\ntop nodes: ");
        GraphTools.print(finishedProcess.getTopNodes(50), System.out);
        for (String nodeTypeToPrint : currentStepSAConfig.getNodeTypesToPrint()) {
            System.out.println("\ntop " + nodeTypeToPrint.toLowerCase() + ": ");
            GraphTools.print(finishedProcess.getTopNodes(50, nodeTypeToPrint), System.out);
        }
        System.out.println(ITERATIONS_SPLITTER);
    }

    protected void printParamsAndOutput(EmbeddedNeo4jSAProcess finishedProcess, PrintStream output) {
        output.println("SA configuration: ");
        GraphTools.print(currentStepProps, output, "\t");
        
        output.println("\ninitial document activations (" + currentInitialNodes.size() + "): ");
        GraphTools.printInitialActivated(currentInitialNodes, output, "\t");
        
        output.println();
        output.print(finishedProcess.getProcessLog());
        output.println();
        output.print(finishedProcess.getProcessInfo(""));        
    }
    
    
    public static void main(String[] args) {
        try {
            long startTime = System.currentTimeMillis();
            RunSA saRunner = new RunSA(args);
            System.err.println("...initialization phase ended (running time: " + (System.currentTimeMillis() - startTime) + "ms)");
                        
            saRunner.runAllSACombinations();
        //} catch (ParseException | IOException | IllegalArgumentException ex) {
        } catch (ParseException | IOException ex) {
            System.err.println(ex.toString());
            printUsage(RunSA.class.getSimpleName(), args, options);
            throw new IllegalArgumentException();
        }
    }

}
