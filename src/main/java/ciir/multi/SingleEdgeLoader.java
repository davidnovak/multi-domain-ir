/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import java.util.Iterator;
import org.apache.commons.csv.CSVRecord;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

/**
 * Loads in the nodes &amp; edges specified in given CSV file. The following format is assumed:<br/>
 * locID:ID,loc_lat:float,loc_long:float,docID:ID<br/>
 * america,37.09024,-95.712891,frenchassemblyof00curt_108<br/>
 * belgium,50.503887,4.469936,frenchassemblyof00curt_242
 *
 * @author david
 */
public class SingleEdgeLoader extends AbstractLineLoader {

    final Label nodeLabel;
    final RelationshipType edgeType;
    final Label secondNodeLabel;
    
    int fieldNumber = 1;
    
    protected final boolean areEdgesNew;

    /**
     * 
     * @param graphDb
     * @param nodeLabel type of the first node in the edges
     * @param edgeType type of the edge
     * @param secondNodeLabel type of the second node
     */
    public SingleEdgeLoader(GraphDatabaseService graphDb, Label nodeLabel, RelationshipType edgeType, Label secondNodeLabel, boolean areEdgesNew) {
        super(graphDb);
        this.nodeLabel = nodeLabel;
        this.secondNodeLabel = secondNodeLabel;
        this.edgeType = edgeType;
        this.areEdgesNew = areEdgesNew;
    }
    
    @Override
    public boolean parseHeader(Iterator<CSVRecord> inputCSV) {
        this.paramsAndTypes = parseCSVHeader(inputCSV);
        int[] IDFieldIndexes = createIndexesOnIDs(paramsAndTypes, nodeLabel, secondNodeLabel);
        // sanity check: the CSV header must be of the following format:
        // fieldName1:ID,...(some fields in the middle)...,fieldName2:ID
        fieldNumber = paramsAndTypes.length;
        return !(fieldNumber < 2 || IDFieldIndexes.length != 2 || IDFieldIndexes[0] != 0 || IDFieldIndexes[1] != fieldNumber - 1);
    }
    
    @Override
    public boolean loadLine(CSVRecord csvRecord) {
        // get or create the fist and second node of the edge
        Node firstNode = getOrCreate(nodeLabel, paramsAndTypes[0][0], csvRecord.get(0));
        Node secondNode = getOrCreate(secondNodeLabel, paramsAndTypes[fieldNumber - 1][0], csvRecord.get(fieldNumber - 1));
        // create the edge
        Relationship edge = areEdgesNew ? create(edgeType, firstNode, secondNode) : getOrCreate(edgeType, firstNode, secondNode);
        for (int i = 1; i < csvRecord.size() - 1; i++) {
            if (!"".intern().equals(csvRecord.get(i))) {
                edge.setProperty(paramsAndTypes[i][0], createProperty(paramsAndTypes[i][1], csvRecord.get(i)));
            }
        }
        return true;
    }
    
}
