/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.graph;

import org.neo4j.graphdb.Label;

/**
 *
 * @author david
 */
public enum MultiDomainNode implements Label {
    DOCUMENT,
    LOCATION,
    DATE
}
