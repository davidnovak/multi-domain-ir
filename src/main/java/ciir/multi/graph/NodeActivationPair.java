/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.graph;

import ciir.multi.sa.interfaces.SANode;
import java.util.Map;

/**
 *
 * @author david
 */
public class NodeActivationPair implements Map.Entry<SANode, Float> {

    private final SANode node;
    private final float activation;

    public NodeActivationPair(SANode node, float activation) {
        this.node = node;
        this.activation = activation;
    }
    
    @Override
    public SANode getKey() {
        return node;
    }

    @Override
    public Float getValue() {
        return activation;
    }

    @Override
    public Float setValue(Float value) {
        throw new UnsupportedOperationException("Setting value is supported."); 
    }
    
}
