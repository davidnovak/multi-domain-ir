/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi.graph;

import ciir.multi.sa.interfaces.SANode;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import messif.utility.SortedCollection;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

/**
 *
 * @author david
 */
public class GraphTools {

    public static String[] getLabelNames(Node node) {
        List<String> labels = new ArrayList<>();
        for (Label label : node.getLabels()) {
            labels.add(label.name().intern());
        }
        return labels.toArray(new String[labels.size()]);
    }

    public static boolean intersectLabels(String[] labels, Node node) {
        for (Label label : node.getLabels()) {
            for (String str : labels) {
                if (str.equals(label.name().intern())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean intersectLabels(Set<String> labels, Node node) {
        for (Label label : node.getLabels()) {
            if (labels.contains(label.name().intern())) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean intersect(String[] strings1, String[] strings2) {
        for (String str1 : strings1) {
            for (String str2 : strings2) {
                if (str1.equals(str2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean intersect(Set<String> labels, String[] strings) {
        for (String str : strings) {
            if (labels.contains(str)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean intersect(String[] strings1, String strings) {
        for (String str1 : strings1) {
            if (str1.equals(strings)) {
                return true;
            }
        }
        return false;
    }

    public static void print(Properties props, PrintStream out, String linePrefix) {
        List<Object> keys = new ArrayList<>(props.keySet());
        keys.sort(null);
        for (Object key : keys) {
            String value = (String) props.get(key);
            out.print(linePrefix);
            out.println(key + " = " + value);
        }
    }

    public static void print(Map<String, Float> activatedDocs, PrintStream output, final String linePrefix) {
        activatedDocs.entrySet().stream().forEach((topNode) -> {
            output.print(linePrefix);
            output.println(topNode.getKey() + ": " + topNode.getValue());
        });
    }

    public static void printInitialActivated(List<String[]> activatedNodes, PrintStream output, final String linePrefix) {
        int maxCount = 25;
        int count = 0;
        for (String[] topNode : activatedNodes) {
            output.print(linePrefix);
            if (++count > maxCount) {
                output.println("...");
                break;
            }
            String toString = Arrays.toString(topNode);
            output.println(toString.substring(1, toString.length() - 1));
        };
    }

    public static void print(SortedCollection<Map.Entry<SANode, Float>> topNodes, PrintStream output) {
        topNodes.stream().forEach((topNode) -> {
            output.println(topNode.getKey().getNodeID() + ", " + topNode.getValue());
        });
    }

    public static float sumFloats(float [] array) {
        float retval = 0f;
        for (float f : array) {
            retval += f;
        }
        return retval;
    }
    
}
