/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciir.multi;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;

/**
 * Reads in the nodes stored in given text file assuming the following format:
 * docID:ID,name:string,yearPublished:int <br/>
 * book1-page21,"History of wars - page 21",1901<br/>
 * book2-page03,"Revolutionary year 1848 - page 03",1888<br/>
 * book3-page111,"The time of steel - page 211",1900
 *
 * @author david
 */
public class SingleNodeLoader extends AbstractLineLoader {

    final Label nodeLabel;
    
    int idFieldIndex = 0;
    
    public SingleNodeLoader(GraphDatabaseService graphDb, Label nodeLabel) {
        super(graphDb);
        this.nodeLabel = nodeLabel;
    }

    @Override
    public boolean parseHeader(Iterator<CSVRecord> inputCSV) {
        this.paramsAndTypes = parseCSVHeader(inputCSV);
        int[] IDFieldIndexes = createIndexesOnIDs(paramsAndTypes, nodeLabel);
        // sanity check: the CSV header must contain one field marked as ID
        if (IDFieldIndexes.length != 1) {
            System.err.println("the CSV header must contain one field marked as 'ID': " + Arrays.deepToString(paramsAndTypes));
            return false;
            //throw new IllegalArgumentException("the CSV header must contain one field marked as 'ID': " + fileName);
        }
        idFieldIndex = IDFieldIndexes[0];
        return true;
    }
    
    @Override
    public boolean loadLine(CSVRecord csvRecord) {
        Node node = getOrCreate(nodeLabel, paramsAndTypes[idFieldIndex][0], csvRecord.get(idFieldIndex));
        for (int i = 0; i < csvRecord.size(); i++) {
            if (i != idFieldIndex && ! "".intern().equals(csvRecord.get(i))) {
                Object property = createProperty(paramsAndTypes[i][1], csvRecord.get(i));
                if (property instanceof Label) {
                    node.addLabel((Label) property);
                } else {
                    node.setProperty(paramsAndTypes[i][0], property);
                }
            }
        }
        return true;
    }
    
}
