#!/bin/bash

DATASET=${DATASET:-book_collection}

if [ $# -lt 1 ]; then
	PARAMS=" -d $DATASET/data/documents.tsv -a $DATASET/data/doc-date.tsv -e $DATASET/data/date-date.tsv -l $DATASET/data/locations.tsv -o $DATASET/data/doc-loc.tsv -c $DATASET/data/loc-loc.tsv -p $DATASET/data/page-page.tsv -i TAB -n"
else
	PARAMS="$*"
fi

echo running: $PARAMS
java -cp "target/*:target/dependency/*" ciir.multi.LoadData -b databases/$DATASET/ $PARAMS
