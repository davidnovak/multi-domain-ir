###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   iswardiminishin00baltgoog_75, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 118, non-zero: 118, borderline: 117, overall act: 10.185, act diff: 9.185, ratio: 0.902
#   pulse 2: activated: 830034, non-zero: 830034, borderline: 830032, overall act: 81911.232, act diff: 81901.047, ratio: 1.000
#   pulse 3: activated: 830034, non-zero: 830034, borderline: 830032, overall act: 81911.232, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 830034
#   final overall activation: 81911.2
#   number of spread. activ. pulses: 3
#   running time: 113219

###################################
# top k results in TREC format: 

3   rel1-1   england   1   0.39380008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   newton blossomville   2   0.18165141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   filgrave   3   0.17937803   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   turvey   4   0.17515394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   sherington   5   0.17483264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   easton maudit   6   0.17346466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   tyringham   7   0.17307529   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   chicheley   8   0.1712284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   hardmead   9   0.16994967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   bozeat   10   0.16879033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   harrold   11   0.16687736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   paris   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   poitiers   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   france   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   calais   15   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   guyenne   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   1311   17   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   1310   18   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   1319   19   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
3   rel1-1   1318   20   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 3   rel1-1   iswardiminishin00baltgoog_75   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   england   2   0.39380008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_93   3   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   workscomprisingh02cowpuoft_385   4   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_46   5   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_90   6   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_223   7   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   ageneralabridgm30vinegoog_96   8   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   economicreview05unkngoog_70   9   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   memoirnathhawth00hawtrich_308   10   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   economicreview05unkngoog_64   11   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_54   12   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_88   13   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   marriage00welliala_104   14   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   saintjohnsfire00sude_140   15   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   cu31924084839194_405   16   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   ageneralabridgm30vinegoog_61   17   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   marriage00welliala_154   18   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_244   19   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_243   20   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_257   21   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   naturalhistoryof00smitrich_88   22   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   ahistorycritici09saingoog_432   23   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   northmenincumbe00ferggoog_22   24   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   arbordayitshisto00scharich_120   25   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   sirtom00oliprich_25   26   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   sirtom00oliprich_24   27   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_19   28   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   farmersplanterse00johnrich_243   29   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   philosophicalcri02brom_277   30   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   farmersplanterse00johnrich_270   31   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   faithofourfather00gibbrich_33   32   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   historyantiquit02ling_57   33   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_29   34   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   churchinscotland00luck_400   35   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   historyantiquit02ling_85   36   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_38   37   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_37   38   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_31   39   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_30   40   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
