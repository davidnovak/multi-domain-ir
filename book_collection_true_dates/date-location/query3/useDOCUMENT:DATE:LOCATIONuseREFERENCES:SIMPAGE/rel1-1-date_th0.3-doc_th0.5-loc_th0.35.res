###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   iswardiminishin00baltgoog_75, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 120, non-zero: 120, borderline: 119, overall act: 11.109, act diff: 10.109, ratio: 0.910
#   pulse 2: activated: 830024, non-zero: 830024, borderline: 830022, overall act: 81910.341, act diff: 81899.231, ratio: 1.000
#   pulse 3: activated: 830036, non-zero: 830036, borderline: 830032, overall act: 81912.307, act diff: 1.967, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 830036
#   final overall activation: 81912.3
#   number of spread. activ. pulses: 3
#   running time: 124665

###################################
# top k results in TREC format: 

3   rel1-1   england   1   0.6990839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   france   2   0.19814533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   guyenne   3   0.19814533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   paris   4   0.19592431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   italy   5   0.13579832   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   calais   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   poitiers   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   toulouse   8   0.10605503   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1314   9   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1315   10   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1313   11   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1311   12   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1319   13   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1323   14   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1324   15   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1325   16   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1326   17   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1327   18   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1330   19   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
3   rel1-1   1317   20   0.07331817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 3   rel1-1   iswardiminishin00baltgoog_75   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   england   2   0.6990839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   iswardiminishin00baltgoog_74   3   0.58057195   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   iswardiminishin00baltgoog_76   4   0.567966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   iswardiminishin00baltgoog_77   5   0.38947695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   iswardiminishin00baltgoog_73   6   0.3745605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   france   7   0.19814533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   guyenne   8   0.19814533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   paris   9   0.19592431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_93   10   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   workscomprisingh02cowpuoft_385   11   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_46   12   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_90   13   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_223   14   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   ageneralabridgm30vinegoog_96   15   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   economicreview05unkngoog_70   16   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   memoirnathhawth00hawtrich_308   17   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   economicreview05unkngoog_64   18   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_54   19   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   principlesofecon01marsuoft_88   20   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   marriage00welliala_104   21   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   saintjohnsfire00sude_140   22   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   cu31924084839194_405   23   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   ageneralabridgm30vinegoog_61   24   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   marriage00welliala_154   25   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_244   26   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_243   27   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   lyricallifepoems00mass_257   28   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   naturalhistoryof00smitrich_88   29   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   ahistorycritici09saingoog_432   30   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   northmenincumbe00ferggoog_22   31   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   arbordayitshisto00scharich_120   32   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   sirtom00oliprich_25   33   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   sirtom00oliprich_24   34   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   piercepenniless00nashgoog_19   35   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   farmersplanterse00johnrich_243   36   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   philosophicalcri02brom_277   37   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   farmersplanterse00johnrich_270   38   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   faithofourfather00gibbrich_33   39   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 3   rel1-1   historyantiquit02ling_57   40   0.1943943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
