###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 12.567, act diff: 10.567, ratio: 0.841
#   pulse 2: activated: 114015, non-zero: 114015, borderline: 114010, overall act: 10329.187, act diff: 10316.620, ratio: 0.999
#   pulse 3: activated: 114032, non-zero: 114032, borderline: 114026, overall act: 10332.227, act diff: 3.040, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 114032
#   final overall activation: 10332.2
#   number of spread. activ. pulses: 3
#   running time: 74183

###################################
# top k results in TREC format: 

9   rel2-1   austria   1   0.5835202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   prussia   2   0.53043073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   elbe   3   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   saxony   4   0.35464862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   bohemia   5   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   silesia   6   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   italy   7   0.27329844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   holstein   8   0.2611638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   jicin   9   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   dresden   10   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   1863   11   0.25634852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   1862   12   0.25634852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   1852   13   0.25634852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   germany   14   0.24871033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   hungary   15   0.23335627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   acropolis hill   16   0.21755426   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   art gallery   17   0.2167926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   acropolis museum   18   0.21559215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   propylaia   19   0.21526746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
9   rel2-1   acropolis   20   0.21519999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_154   3   0.63275546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   austria   4   0.5835202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_750   5   0.55956495   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_156   6   0.5313244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   prussia   7   0.53043073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_153   8   0.51588535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_748   9   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   elbe   10   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   saxony   11   0.35464862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   marvelousstoryof00nort_227   12   0.31701896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_362   13   0.3108315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   bohemia   14   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924082142666_237   15   0.30218443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_364   16   0.29823935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_352   17   0.29680276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_365   18   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_32   19   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   serbiaeurope00mark_142   20   0.29496095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_96   21   0.29373276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofeuropef00holt_116   22   0.29215991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924082142666_119   23   0.29208982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofcontemp00seiguoft_295   24   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofcontemp00seigrich_299   25   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   europe1789192000turnrich_282   26   0.2903725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   shorthistoryofeu00terr_465   27   0.289929   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   universalantho25garn_415   28   0.289677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   papersrelatingt19statgoog_84   29   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   papersrelatingt04unkngoog_90   30   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   reckoningdiscuss00beckrich_146   31   0.28733686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924031684685_715   32   0.28570086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_357   33   0.2851643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   silesia   34   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_37   35   0.28468382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   shorthistoryofeu00terr_459   36   0.28458318   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_301   37   0.28441188   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   outlineseuropea04beargoog_378   38   0.28393888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   foundationsofger00barkuoft_248   39   0.28354967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 9   rel2-1   storygreatwar04ruhlgoog_345   40   0.28330514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
