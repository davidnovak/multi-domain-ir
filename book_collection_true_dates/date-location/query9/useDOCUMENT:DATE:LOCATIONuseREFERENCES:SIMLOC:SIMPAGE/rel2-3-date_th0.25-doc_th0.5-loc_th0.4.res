###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 59, non-zero: 59, borderline: 57, overall act: 10.868, act diff: 8.868, ratio: 0.816
#   pulse 2: activated: 75194, non-zero: 75194, borderline: 75191, overall act: 5896.687, act diff: 5885.820, ratio: 0.998
#   pulse 3: activated: 75211, non-zero: 75211, borderline: 75206, overall act: 5899.633, act diff: 2.946, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75211
#   final overall activation: 5899.6
#   number of spread. activ. pulses: 3
#   running time: 67770

###################################
# top k results in TREC format: 

9   rel2-3   austria   1   0.5936609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   prussia   2   0.5261141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   saxony   3   0.37216538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   hanover   4   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   elbe   5   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   bavaria   6   0.25934714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   holstein   7   0.24265034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   1863   8   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   1852   9   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   1848   10   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   acropolis hill   11   0.20868057   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   berlin   12   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   moravia   13   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   bad langensalza   14   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   art gallery   15   0.20794804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   acropolis museum   16   0.20679356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   propylaia   17   0.20648134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   acropolis   18   0.20641644   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   acropolis of athens   19   0.20640871   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-3   parthenon   20   0.20635429   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 9   rel2-3   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   austria   3   0.5936609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082142666_15   4   0.54432905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   prussia   5   0.5261141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082142666_17   6   0.5177212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924014592566_748   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924014592566_750   8   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082142666_14   9   0.39242104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   saxony   10   0.37216538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   hanover   11   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082142666_18   12   0.30012435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   elbe   13   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   bavaria   14   0.25934714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   holstein   15   0.24265034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   1863   16   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   1852   17   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   1848   18   0.23558034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924060379777_226   19   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_244   20   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924060379777_250   21   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cavourmakingofmo00orsi_167   22   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924031236031_350   23   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924031236031_358   24   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_232   25   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_226   26   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_256   27   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   secretmemoirsco01orlgoog_446   28   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   newscuttings00gran_30   29   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924027975733_88   30   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   journalseriesage65royauoft_442   31   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   storiesseatoldb00unkngoog_94   32   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924060379777_343   33   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   historyofpopesth00rankuoft_513   34   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924031236031_360   35   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924031236031_366   36   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_260   37   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   cu31924082163829_160   38   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_82   39   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_90   40   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
