###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 76, non-zero: 76, borderline: 73, overall act: 16.052, act diff: 13.052, ratio: 0.813
#   pulse 2: activated: 127584, non-zero: 127584, borderline: 127577, overall act: 15777.731, act diff: 15761.679, ratio: 0.999
#   pulse 3: activated: 127612, non-zero: 127612, borderline: 127602, overall act: 15784.162, act diff: 6.431, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 127612
#   final overall activation: 15784.2
#   number of spread. activ. pulses: 3
#   running time: 76035

###################################
# top k results in TREC format: 

9   rel3-2   austria   1   0.83503735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   prussia   2   0.78329575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   saxony   3   0.5596087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   holstein   4   0.49444064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   1863   5   0.4163009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   1852   6   0.4163009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   elbe   7   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   vienna   8   0.38082668   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   hanover   9   0.36716026   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   jicin   10   0.35851935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   germany   11   0.3279488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   bohemia   12   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   denmark   13   0.2900222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   berlin   14   0.2886825   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   moravia   15   0.2886825   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   silesia   16   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   bavaria   17   0.2823322   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   italy   18   0.278624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   1862   19   0.27309436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
9   rel3-2   1848   20   0.26084217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 9   rel3-2   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   encyclopediabrit03newyrich_155   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924082142666_16   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   austria   4   0.83503735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   prussia   5   0.78329575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   encyclopediabrit03newyrich_154   6   0.7047888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924082142666_15   7   0.65201825   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   encyclopediabrit03newyrich_153   8   0.6417255   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924082142666_17   9   0.63447076   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924082142666_14   10   0.6113875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   encyclopediabrit03newyrich_156   11   0.5887098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   saxony   12   0.5596087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924014592566_750   13   0.55956495   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924082142666_18   14   0.49903348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   holstein   15   0.49444064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924014592566_748   16   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   marvelousstoryof00nort_227   17   0.45021036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   lifetimesofstein02seeluoft_259   18   0.44077384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924014592566_362   19   0.4381511   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   ourchancellorske00buscuoft_403   20   0.43216068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   foundationsofger00barkuoft_97   21   0.43139943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   journalpolitica02socigoog_11   22   0.42867306   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924088053800_143   23   0.42857257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   foundationsofger00barkuoft_248   24   0.42854428   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   menwhohavemaden00stragoog_155   25   0.42679802   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   threegermanysgl00faygoog_392   26   0.42219427   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   outlineseuropea04beargoog_378   27   0.42109182   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   threegermanysgl00faygoog_397   28   0.42103443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   foundationsofger00barkuoft_95   29   0.42039192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   europe1789192000turnrich_277   30   0.418939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   shorthistoryofeu00terr_454   31   0.4173598   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   threegermanysgl00faygoog_358   32   0.41639733   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   foundationsofger00barkuoft_355   33   0.4163128   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   1863   34   0.4163009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   1852   35   0.4163009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   foundationsofger00barkuoft_359   36   0.4161592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   cu31924014592566_291   37   0.41568854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   lifetimesofstein02seeluoft_358   38   0.41501385   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   travelsinnorthg00dwiggoog_356   39   0.4120073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel3-2   ourchancellorske00buscuoft_354   40   0.41015857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
