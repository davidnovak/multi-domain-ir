###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 12.567, act diff: 10.567, ratio: 0.841
#   pulse 2: activated: 75206, non-zero: 75206, borderline: 75203, overall act: 6154.790, act diff: 6142.222, ratio: 0.998
#   pulse 3: activated: 75242, non-zero: 75242, borderline: 75237, overall act: 6159.589, act diff: 4.800, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75242
#   final overall activation: 6159.6
#   number of spread. activ. pulses: 3
#   running time: 78578

###################################
# top k results in TREC format: 

9   rel2-1   austria   1   0.6390331   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   prussia   2   0.51554036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   saxony   3   0.39717904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   elbe   4   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   italy   5   0.3472339   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   germany   6   0.31703663   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   bohemia   7   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   silesia   8   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   hungary   9   0.2743504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   vienna   10   0.2615921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   jicin   11   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   dresden   12   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   holstein   13   0.24733947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   1862   14   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   1863   15   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   1852   16   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   acropolis hill   17   0.21755426   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   art gallery   18   0.2167926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   acropolis museum   19   0.21559215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
9   rel2-1   propylaia   20   0.21526746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   austria   3   0.6390331   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_154   4   0.5613078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_156   5   0.5313244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   prussia   6   0.51554036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924014592566_748   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924014592566_750   8   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_153   9   0.40907127   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   saxony   10   0.39717904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   elbe   11   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   italy   12   0.3472339   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   germany   13   0.31703663   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   bohemia   14   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   silesia   15   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   hungary   16   0.2743504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   vienna   17   0.2615921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_157   18   0.2595839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   jicin   19   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   dresden   20   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   holstein   21   0.24733947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   1862   22   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   1863   23   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   1852   24   0.23958543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924060379777_226   25   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_244   26   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924060379777_250   27   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cavourmakingofmo00orsi_167   28   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924031236031_350   29   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924031236031_358   30   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_232   31   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_226   32   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_256   33   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   secretmemoirsco01orlgoog_446   34   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   newscuttings00gran_30   35   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924027975733_88   36   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   journalseriesage65royauoft_442   37   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   storiesseatoldb00unkngoog_94   38   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   cu31924060379777_343   39   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 9   rel2-1   historyofpopesth00rankuoft_513   40   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
