###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 12.567, act diff: 10.567, ratio: 0.841
#   pulse 2: activated: 75206, non-zero: 75206, borderline: 75203, overall act: 6154.790, act diff: 6142.222, ratio: 0.998
#   pulse 3: activated: 75206, non-zero: 75206, borderline: 75203, overall act: 6154.790, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75206
#   final overall activation: 6154.8
#   number of spread. activ. pulses: 3
#   running time: 41392

###################################
# top k results in TREC format: 

9   rel2-1   austria   1   0.44222224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   elbe   2   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   prussia   3   0.3878032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   bohemia   4   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   saxony   5   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   silesia   6   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   jicin   7   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   dresden   8   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   italy   9   0.22582704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   acropolis hill   10   0.21755426   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   art gallery   11   0.2167926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   acropolis museum   12   0.21559215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   propylaia   13   0.21526746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   acropolis   14   0.21519999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   acropolis of athens   15   0.21519195   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   parthenon   16   0.21513538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   areopagus   17   0.21502744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   erechtheion   18   0.21491915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   pnyx   19   0.21489573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   dhafni   20   0.20887397   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_154   3   0.5613078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   encyclopediabrit03newyrich_156   4   0.5313244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924014592566_748   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924014592566_750   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   austria   7   0.44222224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   elbe   8   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   prussia   9   0.3878032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   bohemia   10   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   saxony   11   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   silesia   12   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   jicin   13   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   dresden   14   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   italy   15   0.22582704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_226   16   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_244   17   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_250   18   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cavourmakingofmo00orsi_167   19   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_350   20   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_358   21   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_232   22   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_226   23   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_256   24   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   secretmemoirsco01orlgoog_446   25   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   newscuttings00gran_30   26   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924027975733_88   27   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   journalseriesage65royauoft_442   28   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   storiesseatoldb00unkngoog_94   29   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_343   30   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   historyofpopesth00rankuoft_513   31   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_360   32   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_366   33   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_260   34   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924082163829_160   35   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   schoolgardenbein00schw_82   36   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   schoolgardenbein00schw_90   37   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   memoirscountess02genlgoog_44   38   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   generalarmoryofe00burk_1337   39   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924027758022_392   40   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
