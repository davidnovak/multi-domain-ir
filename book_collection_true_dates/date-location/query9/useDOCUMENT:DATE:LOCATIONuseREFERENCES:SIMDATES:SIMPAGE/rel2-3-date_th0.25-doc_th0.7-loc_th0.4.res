###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 59, non-zero: 59, borderline: 57, overall act: 10.868, act diff: 8.868, ratio: 0.816
#   pulse 2: activated: 75180, non-zero: 75180, borderline: 75177, overall act: 5893.854, act diff: 5882.986, ratio: 0.998
#   pulse 3: activated: 75180, non-zero: 75180, borderline: 75177, overall act: 5893.854, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75180
#   final overall activation: 5893.9
#   number of spread. activ. pulses: 3
#   running time: 37696

###################################
# top k results in TREC format: 

9   rel2-3   austria   1   0.4236283   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   prussia   2   0.35679588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   saxony   3   0.32253665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   hanover   4   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   elbe   5   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   berlin   6   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   moravia   7   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   bad langensalza   8   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   holstein   9   0.18442257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   bohemia   10   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   bavaria   11   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   hesse   12   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   gottingen   13   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   trutnowo   14   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1848   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1854   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1866   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1856   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1857   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel2-3   1858   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 9   rel2-3   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924082142666_15   3   0.54432905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924082142666_17   4   0.5177212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924014592566_748   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924014592566_750   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   austria   7   0.4236283   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   prussia   8   0.35679588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   saxony   9   0.32253665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   hanover   10   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   elbe   11   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924060379777_226   12   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_244   13   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924060379777_250   14   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cavourmakingofmo00orsi_167   15   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924031236031_350   16   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924031236031_358   17   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_232   18   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_226   19   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_256   20   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   secretmemoirsco01orlgoog_446   21   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   newscuttings00gran_30   22   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924027975733_88   23   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   journalseriesage65royauoft_442   24   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   storiesseatoldb00unkngoog_94   25   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924060379777_343   26   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   historyofpopesth00rankuoft_513   27   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924031236031_360   28   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924031236031_366   29   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_260   30   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924082163829_160   31   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_82   32   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_90   33   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   memoirscountess02genlgoog_44   34   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   generalarmoryofe00burk_1337   35   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924027758022_392   36   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924082163829_142   37   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   cu31924031296340_439   38   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   autobiographycor01deweuoft_203   39   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel2-3   electricfurnace01baurgoog_20   40   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
