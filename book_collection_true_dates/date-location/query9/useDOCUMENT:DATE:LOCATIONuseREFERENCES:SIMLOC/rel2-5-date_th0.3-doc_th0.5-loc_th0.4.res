###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   encyclopediabrit03newyrich_155, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 34, non-zero: 34, borderline: 32, overall act: 7.105, act diff: 5.105, ratio: 0.718
#   pulse 2: activated: 75171, non-zero: 75171, borderline: 75168, overall act: 6355.061, act diff: 6347.957, ratio: 0.999
#   pulse 3: activated: 75171, non-zero: 75171, borderline: 75168, overall act: 6355.061, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75171
#   final overall activation: 6355.1
#   number of spread. activ. pulses: 3
#   running time: 36051

###################################
# top k results in TREC format: 

9   rel2-5   austria   1   0.45714977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   prussia   2   0.38772225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   saxony   3   0.3441148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   holstein   4   0.31223398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   hanover   5   0.23115462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   acropolis hill   6   0.22465234   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   art gallery   7   0.22386755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   acropolis museum   8   0.22263062   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   propylaia   9   0.22229607   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   acropolis   10   0.22222655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   acropolis of athens   11   0.22221826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   parthenon   12   0.22215997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   areopagus   13   0.22204874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   erechtheion   14   0.22193715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   pnyx   15   0.22191302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   dhafni   16   0.21570744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   peronia   17   0.2089604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   trakhones   18   0.2011074   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   berlin   19   0.2010266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
9   rel2-5   moravia   20   0.2010266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 9   rel2-5   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   austria   3   0.45714977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   prussia   4   0.38772225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   saxony   5   0.3441148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   holstein   6   0.31223398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   hanover   7   0.23115462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924060379777_226   8   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   catholicsocialis00nittiala_244   9   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924060379777_250   10   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cavourmakingofmo00orsi_167   11   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924031236031_350   12   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924031236031_358   13   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   catholicsocialis00nittiala_232   14   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   catholicsocialis00nittiala_226   15   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   catholicsocialis00nittiala_256   16   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   secretmemoirsco01orlgoog_446   17   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   newscuttings00gran_30   18   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924027975733_88   19   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   journalseriesage65royauoft_442   20   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   storiesseatoldb00unkngoog_94   21   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924060379777_343   22   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   historyofpopesth00rankuoft_513   23   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924031236031_360   24   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924031236031_366   25   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   catholicsocialis00nittiala_260   26   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924082163829_160   27   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   schoolgardenbein00schw_82   28   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   schoolgardenbein00schw_90   29   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   memoirscountess02genlgoog_44   30   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   generalarmoryofe00burk_1337   31   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924027758022_392   32   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924082163829_142   33   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   cu31924031296340_439   34   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   autobiographycor01deweuoft_203   35   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   electricfurnace01baurgoog_20   36   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   firsteleventheig1871mass_323   37   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   royalfavourites01stongoog_11   38   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   firsteleventheig1871mass_353   39   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 9   rel2-5   familysaveallsup00bouv_586   40   0.22467561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
