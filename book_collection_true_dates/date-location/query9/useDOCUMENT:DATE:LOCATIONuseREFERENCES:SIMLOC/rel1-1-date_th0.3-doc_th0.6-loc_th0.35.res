###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924014592566_749, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 50, non-zero: 50, borderline: 49, overall act: 6.356, act diff: 5.356, ratio: 0.843
#   pulse 2: activated: 50, non-zero: 50, borderline: 49, overall act: 6.356, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 50
#   final overall activation: 6.4
#   number of spread. activ. pulses: 2
#   running time: 23661

###################################
# top k results in TREC format: 

9   rel1-1   elbe   1   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   austria   2   0.21336742   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   prussia   3   0.1845146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   saxony   4   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   bavaria   5   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   bohemia   6   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1853   7   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1851   8   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1859   9   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1857   10   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1856   11   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1858   12   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1860   13   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1861   14   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1862   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1863   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1864   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1865   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1866   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel1-1   1850   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 9   rel1-1   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   elbe   2   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   austria   3   0.21336742   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   prussia   4   0.1845146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   saxony   5   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   bavaria   6   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   bohemia   7   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1853   8   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1851   9   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1859   10   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1857   11   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1856   12   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1858   13   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1865   14   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1863   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1862   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1860   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1869   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1867   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1868   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1848   21   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1849   22   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1870   23   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1861   24   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1864   25   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1866   26   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1850   27   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1852   28   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1854   29   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   1855   30   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   south   31   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   brandenburg   32   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   denmark   33   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   berlin   34   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   gorlitz   35   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   podol   36   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   hanover   37   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   ephesus   38   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   jicin   39   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel1-1   silesia   40   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
