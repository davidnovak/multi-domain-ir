###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_464, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 59, non-zero: 59, borderline: 57, overall act: 10.080, act diff: 8.080, ratio: 0.802
#   pulse 2: activated: 6403, non-zero: 6403, borderline: 6399, overall act: 602.344, act diff: 592.264, ratio: 0.983
#   pulse 3: activated: 6403, non-zero: 6403, borderline: 6399, overall act: 602.344, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6403
#   final overall activation: 602.3
#   number of spread. activ. pulses: 3
#   running time: 17777

###################################
# top k results in TREC format: 

1   rel2-10   lucknow   1   0.40783542   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   gwalior   2   0.37126157   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   china   3   0.3407406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   panjnad river   4   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   cabool   5   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   agra   6   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1858   7   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1859   8   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1868   9   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1860   10   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1862   11   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1863   12   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1857   13   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1855   14   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   delhi   15   0.20101978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   shahabad   16   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   bareilly   17   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   punjaub   18   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   bengal   19   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-10   1865   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-10   newannualarmylis1874hart_464   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   lucknow   3   0.40783542   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   gwalior   4   0.37126157   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   china   5   0.3407406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   cu31924023223872_152   6   0.24959394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   cu31924023968237_278   7   0.2409456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   panjnad river   8   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   cabool   9   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   apersonaljourna00andegoog_125   10   0.22563364   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   hartsannualarmy17hartgoog_256   11   0.22361608   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   agra   12   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   recollectionsofm00adyeiala_162   13   0.21570043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   ourredcoatsbluej00stewuoft_319   14   0.21570043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   ridpathsuniversa15ridp_377   15   0.21570043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   encyclopaediabri14chisrich_499   16   0.21136498   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   newannualarmylis1874hart_174   17   0.20961742   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   mydiaryinindiai00russgoog_45   18   0.20830321   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   newannualarmylis1874hart_329   19   0.20715071   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   newannualarmylis1874hart_467   20   0.20498006   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1859   21   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1863   22   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1857   23   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1858   24   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1868   25   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1860   26   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1862   27   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   1855   28   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   historyofourownt03mccaiala_19   29   0.20316447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   newannualarmylis1874hart_143   30   0.20182034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   fortyoneyearsini00robe_316   31   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_231   32   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_230   33   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   tennysonhisarta01broogoog_251   34   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_279   35   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_267   36   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   memoirofsirwilli00veitrich_344   37   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_211   38   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   reminiscencesofb00edwauoft_234   39   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-10   lifeofisabellath00thobrich_313   40   0.20113748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
