###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_347, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 11.193, act diff: 9.193, ratio: 0.821
#   pulse 2: activated: 132014, non-zero: 132014, borderline: 132010, overall act: 11258.089, act diff: 11246.896, ratio: 0.999
#   pulse 3: activated: 132014, non-zero: 132014, borderline: 132010, overall act: 11258.089, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 132014
#   final overall activation: 11258.1
#   number of spread. activ. pulses: 3
#   running time: 34440

###################################
# top k results in TREC format: 

1   rel2-11   lucknow   1   0.38835582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   china   2   0.3522497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   canton   3   0.3116384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   sebastopol   4   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   gwalior   5   0.23604211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   india   6   0.23083122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   agra   7   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1854   8   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1857   9   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1858   10   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1859   11   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1860   12   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1863   13   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   1855   14   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   shahabad   15   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   bomarsund   16   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   pekin   17   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   crimea   18   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   bengal   19   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-11   heights   20   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel2-11   newannualarmylis1874hart_347   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   lucknow   3   0.38835582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   china   4   0.3522497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   canton   5   0.3116384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   sebastopol   6   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   gwalior   7   0.23604211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   india   8   0.23083122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_470   9   0.2306379   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   lifelettersdiari00vetcuoft_181   10   0.22947483   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   narrativeindia01hebe_574   11   0.22947483   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_439   12   0.22561648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   agra   13   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   cu31924023223872_205   14   0.21496482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_482   15   0.21231265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_480   16   0.21066293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1854   17   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1857   18   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1858   19   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1859   20   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1860   21   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1863   22   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   1855   23   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_251   24   0.20780994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_252   25   0.20754464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_588   26   0.20697373   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   cu31924023223872_163   27   0.20607013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   progressofindiaj00temprich_111   28   0.20543443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   cu31924023223872_117   29   0.20543443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_485   30   0.20496924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   australiandefenc00scra_36   31   0.20426266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_279   32   0.20253286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_483   33   0.19715764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   hartsannualarmy17hartgoog_506   34   0.19537885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   newannualarmylis1874hart_567   35   0.1938297   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   lifeofrichardbri00sanduoft_109   36   0.19177367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   cu31924023968237_12   37   0.19177367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   cu31924023968237_10   38   0.19177367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   collectedverseof01kipluoft_161   39   0.19177367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-11   blackwoodsmagazi84edinuoft_80   40   0.19177367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
