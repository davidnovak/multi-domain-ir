###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_347, 1
#   newannualarmylis1874hart_481, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 69, non-zero: 69, borderline: 67, overall act: 10.702, act diff: 8.702, ratio: 0.813
#   pulse 2: activated: 9239, non-zero: 9239, borderline: 9236, overall act: 810.751, act diff: 800.048, ratio: 0.987
#   pulse 3: activated: 9239, non-zero: 9239, borderline: 9236, overall act: 810.751, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9239
#   final overall activation: 810.8
#   number of spread. activ. pulses: 3
#   running time: 19596

###################################
# top k results in TREC format: 

1   rel2-26   delhi   1   0.4100854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   lucknow   2   0.352682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   agra   3   0.2507578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   sebastopol   4   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   canton   5   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1857   6   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1858   7   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1860   8   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1861   9   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1863   10   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   1859   11   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   cawnpore   12   0.18428202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   gwalior   13   0.18428202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   crimea   14   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   china   15   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   chingli   16   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   meerut   17   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   terai   18   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   heights   19   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
1   rel2-26   india   20   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel2-26   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   newannualarmylis1874hart_347   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   delhi   3   0.4100854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   lucknow   4   0.352682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   agra   5   0.2507578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   sebastopol   6   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   canton   7   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1857   8   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1858   9   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1860   10   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1861   11   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1863   12   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   1859   13   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   manwithoutprofes03rowc_26   14   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   inindia00steeiala_76   15   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   inindia00steeiala_71   16   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   cu31924002829921_117   17   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   reminiscencesofb00edwauoft_229   18   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   overhillsofhomeo00leveuoft_27   19   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   religioussystemss00lond_323   20   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   journalofrouteac00munsrich_241   21   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   cu31924104225630_200   22   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   journaloftravels00wina_286   23   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   journaloftravels00wina_282   24   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   reminiscencesofb00edwauoft_282   25   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   punjabrecordorre50lahouoft_380   26   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   historyofindia00delarich_146   27   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   historyofindia00delarich_147   28   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   pathogenicmicro03krumgoog_785   29   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   mussulman00maddgoog_192   30   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   mussulman00maddgoog_190   31   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   sacredbooksande03horngoog_253   32   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   gospelrmakrishn01abhegoog_412   33   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   gandhianarchy00sankuoft_156   34   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   cu31924021024876_102   35   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   fiftyyearsamongb00mill_19   36   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   lifeofsirhenryla01edwa_211   37   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   phantomrickshaw01kiplgoog_245   38   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   inpermanentwayot00steeiala_266   39   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel2-26   lifeofsirhenryla01edwa_274   40   0.20221671   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
