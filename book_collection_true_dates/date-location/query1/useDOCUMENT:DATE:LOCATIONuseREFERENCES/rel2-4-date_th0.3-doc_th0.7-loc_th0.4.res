###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_480, 1
#   newannualarmylis1874hart_464, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 57, non-zero: 57, borderline: 55, overall act: 10.056, act diff: 8.056, ratio: 0.801
#   pulse 2: activated: 5129, non-zero: 5129, borderline: 5126, overall act: 575.472, act diff: 565.416, ratio: 0.983
#   pulse 3: activated: 5129, non-zero: 5129, borderline: 5126, overall act: 575.472, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5129
#   final overall activation: 575.5
#   number of spread. activ. pulses: 3
#   running time: 18060

###################################
# top k results in TREC format: 

1   rel2-4   lucknow   1   0.47717106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   delhi   2   0.39393383   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   china   3   0.35762215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   bareilly   4   0.29739732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   gwalior   5   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   panjnad river   6   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   cabool   7   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1857   8   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1858   9   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1859   10   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1868   11   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1860   12   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1862   13   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1863   14   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1864   15   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1855   16   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   cawnpore   17   0.18114787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   punjaub   18   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   rooya   19   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-4   1866   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 1   rel2-4   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   newannualarmylis1874hart_464   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   lucknow   3   0.47717106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   delhi   4   0.39393383   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   china   5   0.35762215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   bareilly   6   0.29739732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   apersonaljourna00andegoog_121   7   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   systemsoflandte00cobd_226   8   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reportsofmission1896pres_306   9   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   apersonaljourna00andegoog_110   10   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   tennysonhisarta01broogoog_251   11   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   journalofroyalin7188183roya_413   12   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   phoeberowe01thob_185   13   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   narrativemutini00hutcgoog_6   14   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   fortyoneyearsini00robe_316   15   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   lifeofclaudmarti00hill_182   16   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_279   17   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   libraryofworldsbe22warn_51   18   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_267   19   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   memoirofsirwilli00veitrich_344   20   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   newannualarmylis1898lond_946   21   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_211   22   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_234   23   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_231   24   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   reminiscencesofb00edwauoft_230   25   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   passionsanimals00thomgoog_139   26   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   historyofindianm01forr_16   27   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   historyofindianm01forr_14   28   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   historyofindianm01forr_29   29   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   memorialsoflife02edwa_52   30   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   recordsofindianm06indi_263   31   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   macmillansmagazi43macmuoft_261   32   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   memorialsoflife02edwa_49   33   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   livesofindianoff02kayeiala_346   34   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   livesofindianoff02kayeiala_348   35   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   cu31924022927283_266   36   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   cu31924013563865_47   37   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   lifeofisabellath00thobrich_290   38   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   lifeofisabellath00thobrich_286   39   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-4   troubadourselect00gibbuoft_36   40   0.23415928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
