###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_466, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 50, overall act: 9.282, act diff: 7.282, ratio: 0.785
#   pulse 2: activated: 6396, non-zero: 6396, borderline: 6392, overall act: 663.075, act diff: 653.792, ratio: 0.986
#   pulse 3: activated: 6396, non-zero: 6396, borderline: 6392, overall act: 663.075, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6396
#   final overall activation: 663.1
#   number of spread. activ. pulses: 3
#   running time: 15429

###################################
# top k results in TREC format: 

1   rel2-9   lucknow   1   0.45171002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   gwalior   2   0.40666384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   agra   3   0.37584528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   delhi   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1854   5   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1857   6   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1858   7   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1859   8   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1860   9   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1865   10   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1866   11   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1855   12   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   shahabad   13   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   china   14   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   bengal   15   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   tharyarwady   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1868   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   city   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   1862   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
1   rel2-9   kakrala   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 1   rel2-9   newannualarmylis1874hart_466   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   lucknow   3   0.45171002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   gwalior   4   0.40666384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   agra   5   0.37584528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   cu31924023223872_152   6   0.27432287   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   cu31924023968237_278   7   0.26435682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   apersonaljourna00andegoog_125   8   0.24811654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   hartsannualarmy17hartgoog_256   9   0.24577068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   delhi   10   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   ourredcoatsbluej00stewuoft_319   11   0.23713888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   recollectionsofm00adyeiala_162   12   0.23713888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   ridpathsuniversa15ridp_377   13   0.23713888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   encyclopaediabri14chisrich_499   14   0.23223038   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   newannualarmylis1874hart_174   15   0.23043576   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   mydiaryinindiai00russgoog_45   16   0.22918645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   newannualarmylis1874hart_329   17   0.22827537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   newannualarmylis1874hart_467   18   0.22509332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   historyofourownt03mccaiala_19   19   0.22369263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   fortyoneyearsini00robe_316   20   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_231   21   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_230   22   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   tennysonhisarta01broogoog_251   23   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_279   24   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_267   25   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   memoirofsirwilli00veitrich_344   26   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_211   27   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reminiscencesofb00edwauoft_234   28   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   lifeofisabellath00thobrich_313   29   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   reginaldheberbis00bric_147   30   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   passionsanimals00thomgoog_139   31   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   macmillansmagazi43macmuoft_261   32   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   cu31924022927283_266   33   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   cu31924013563865_47   34   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   lifeofisabellath00thobrich_290   35   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   lifeofisabellath00thobrich_286   36   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   babylonianorient03lond_176   37   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   narrativeajourn10hebegoog_372   38   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   journaloftravels00wina_252   39   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 1   rel2-9   cu31924104225408_546   40   0.22209145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
