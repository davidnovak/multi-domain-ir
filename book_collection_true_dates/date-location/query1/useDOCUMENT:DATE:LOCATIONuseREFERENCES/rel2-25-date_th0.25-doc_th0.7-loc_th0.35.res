###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_464, 1
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 50, overall act: 9.389, act diff: 7.389, ratio: 0.787
#   pulse 2: activated: 5124, non-zero: 5124, borderline: 5121, overall act: 612.258, act diff: 602.869, ratio: 0.985
#   pulse 3: activated: 5124, non-zero: 5124, borderline: 5121, overall act: 612.258, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5124
#   final overall activation: 612.3
#   number of spread. activ. pulses: 3
#   running time: 15129

###################################
# top k results in TREC format: 

1   rel2-25   lucknow   1   0.5093606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   chanda   2   0.23942329   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   panjnad river   3   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   gwalior   4   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   cabool   5   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1848   6   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1855   7   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1846   8   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1847   9   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1857   10   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1858   11   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1859   12   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1849   13   0.2187332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   delhi   14   0.20101978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   arrah   15   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   china   16   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   bareilly   17   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   punjaub   18   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1851   19   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-25   1852   20   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 1   rel2-25   newannualarmylis1874hart_464   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   newannualarmylis1874hart_228   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   lucknow   3   0.5093606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   apersonaljourna00andegoog_121   4   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   systemsoflandte00cobd_226   5   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reportsofmission1896pres_306   6   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   apersonaljourna00andegoog_110   7   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   tennysonhisarta01broogoog_251   8   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   journalofroyalin7188183roya_413   9   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   phoeberowe01thob_185   10   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   narrativemutini00hutcgoog_6   11   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   fortyoneyearsini00robe_316   12   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   lifeofclaudmarti00hill_182   13   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_279   14   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   libraryofworldsbe22warn_51   15   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_267   16   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   memoirofsirwilli00veitrich_344   17   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   newannualarmylis1898lond_946   18   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_211   19   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_234   20   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_231   21   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   reminiscencesofb00edwauoft_230   22   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   passionsanimals00thomgoog_139   23   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   historyofindianm01forr_16   24   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   historyofindianm01forr_14   25   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   historyofindianm01forr_29   26   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   memorialsoflife02edwa_52   27   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   recordsofindianm06indi_263   28   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   macmillansmagazi43macmuoft_261   29   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   memorialsoflife02edwa_49   30   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   livesofindianoff02kayeiala_346   31   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   livesofindianoff02kayeiala_348   32   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   cu31924022927283_266   33   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   cu31924013563865_47   34   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   lifeofisabellath00thobrich_290   35   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   lifeofisabellath00thobrich_286   36   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   troubadourselect00gibbuoft_36   37   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   journaloftravels00wina_252   38   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   journaloftravels00wina_251   39   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-25   lifeofclaudmarti00hill_170   40   0.24931315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
