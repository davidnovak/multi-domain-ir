###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_480, 1
#   newannualarmylis1874hart_410, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 50, non-zero: 50, borderline: 48, overall act: 9.494, act diff: 7.494, ratio: 0.789
#   pulse 2: activated: 5122, non-zero: 5122, borderline: 5119, overall act: 668.222, act diff: 658.728, ratio: 0.986
#   pulse 3: activated: 5122, non-zero: 5122, borderline: 5119, overall act: 668.222, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5122
#   final overall activation: 668.2
#   number of spread. activ. pulses: 3
#   running time: 13205

###################################
# top k results in TREC format: 

1   rel2-1   lucknow   1   0.5575834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   china   2   0.3851386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   agra   3   0.30601773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1859   4   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1863   5   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1865   6   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1866   7   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1857   8   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1858   9   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1868   10   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1860   11   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1862   12   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   1855   13   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   delhi   14   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   shahabad   15   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   pekin   16   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   india   17   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   canton   18   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   cawnpore   19   0.18114787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-1   gwalior   20   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel2-1   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   lucknow   3   0.5575834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   china   4   0.3851386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   agra   5   0.30601773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   apersonaljourna00andegoog_121   6   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   systemsoflandte00cobd_226   7   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reportsofmission1896pres_306   8   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   apersonaljourna00andegoog_110   9   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   tennysonhisarta01broogoog_251   10   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   journalofroyalin7188183roya_413   11   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   phoeberowe01thob_185   12   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   narrativemutini00hutcgoog_6   13   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   fortyoneyearsini00robe_316   14   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   lifeofclaudmarti00hill_182   15   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_279   16   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   libraryofworldsbe22warn_51   17   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_267   18   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   memoirofsirwilli00veitrich_344   19   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   newannualarmylis1898lond_946   20   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_211   21   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_234   22   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_231   23   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   reminiscencesofb00edwauoft_230   24   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   passionsanimals00thomgoog_139   25   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   historyofindianm01forr_16   26   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   historyofindianm01forr_14   27   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   historyofindianm01forr_29   28   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   memorialsoflife02edwa_52   29   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   recordsofindianm06indi_263   30   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   macmillansmagazi43macmuoft_261   31   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   memorialsoflife02edwa_49   32   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   livesofindianoff02kayeiala_346   33   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   livesofindianoff02kayeiala_348   34   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   cu31924022927283_266   35   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   cu31924013563865_47   36   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   lifeofisabellath00thobrich_290   37   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   lifeofisabellath00thobrich_286   38   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   troubadourselect00gibbuoft_36   39   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-1   journaloftravels00wina_252   40   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
