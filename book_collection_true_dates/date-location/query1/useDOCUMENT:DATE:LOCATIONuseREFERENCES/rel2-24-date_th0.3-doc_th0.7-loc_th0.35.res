###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_464, 1
#   newannualarmylis1874hart_481, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 54, non-zero: 54, borderline: 52, overall act: 9.569, act diff: 7.569, ratio: 0.791
#   pulse 2: activated: 13384, non-zero: 13384, borderline: 13380, overall act: 1353.788, act diff: 1344.218, ratio: 0.993
#   pulse 3: activated: 13384, non-zero: 13384, borderline: 13380, overall act: 1353.788, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13384
#   final overall activation: 1353.8
#   number of spread. activ. pulses: 3
#   running time: 15329

###################################
# top k results in TREC format: 

1   rel2-24   delhi   1   0.46302488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   lucknow   2   0.3727845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   gwalior   3   0.32364985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   bareilly   4   0.2548788   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   agra   5   0.2507578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   panjnad river   6   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   cabool   7   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1858   8   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1862   9   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1863   10   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1857   11   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1860   12   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1859   13   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   china   14   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   punjaub   15   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   terai   16   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   chingli   17   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   meerut   18   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1861   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
1   rel2-24   1865   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 1   rel2-24   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   newannualarmylis1874hart_464   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   delhi   3   0.46302488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   lucknow   4   0.3727845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   gwalior   5   0.32364985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   hartsannualarmy17hartgoog_258   6   0.29918525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   hartsannualarmy17hartgoog_280   7   0.28992003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   aroundworld00hend_265   8   0.2883021   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   hartsannualarmy17hartgoog_257   9   0.28624314   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   hartsannualarmy17hartgoog_279   10   0.28493565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   cu31924023968237_411   11   0.2819494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   selfhelpwithillu00smiliala_318   12   0.27261695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   northamericanre21lodggoog_625   13   0.27261695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   redyearstoryofin00traciala_327   14   0.27261695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   livesofindianoff02kayeiala_505   15   0.269816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   narrativeajourn10hebegoog_463   16   0.269816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   cu31924023968237_406   17   0.26843655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   newannualarmylis1874hart_203   18   0.26833534   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   delhisiegeassau00youngoog_39   19   0.2600137   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   handbooktoenglis00dewa_40   20   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   cu31924023968237_215   21   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   narrativemutini00hutcgoog_201   22   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   talesofdaringdan00hentiala_102   23   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   reminiscencesofb00edwauoft_179   24   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   narrativemutini00hutcgoog_70   25   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   cu31924023561933_79   26   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   indianempirehist01mart_50   27   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   essaysforcollege00bowm_180   28   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   redyearstoryofin00traciala_187   29   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   illustrationsofh00thoruoft_338   30   0.2577236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   historyofindianm01forr_17   31   0.25753695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   historyofindianm01forr_276   32   0.25753695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   historyofindianm01forr_281   33   0.25753695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   historyofindianm01forr_567   34   0.25753695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   narrativemutini00hutcgoog_82   35   0.25753695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   newannualarmylis1874hart_175   36   0.25714037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   cu31924023968237_403   37   0.25710875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   aroundworld00hend_275   38   0.25689012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   imperialgazette18huntgoog_460   39   0.25673848   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 1   rel2-24   newannualarmylis1874hart_409   40   0.25570974   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
