###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_463, 1
#   newannualarmylis1874hart_481, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 58, non-zero: 58, borderline: 56, overall act: 9.654, act diff: 7.654, ratio: 0.793
#   pulse 2: activated: 5130, non-zero: 5130, borderline: 5127, overall act: 557.142, act diff: 547.488, ratio: 0.983
#   pulse 3: activated: 5130, non-zero: 5130, borderline: 5127, overall act: 557.142, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5130
#   final overall activation: 557.1
#   number of spread. activ. pulses: 3
#   running time: 13204

###################################
# top k results in TREC format: 

1   rel2-17   lucknow   1   0.46180055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   agra   2   0.3870665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   delhi   3   0.3742353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   cawnpore   4   0.24608733   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1858   5   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1865   6   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1857   7   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1859   8   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   gwalior   9   0.192661   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   bareilly   10   0.192661   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   cabool   11   0.18963519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   chingli   12   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   meerut   13   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   terai   14   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   sobraon   15   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   panjnad river   16   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   punjaub   17   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1851   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1862   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-17   1863   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel2-17   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   newannualarmylis1874hart_463   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   lucknow   3   0.46180055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   agra   4   0.3870665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   delhi   5   0.3742353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   cawnpore   6   0.24608733   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   apersonaljourna00andegoog_121   7   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   systemsoflandte00cobd_226   8   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reportsofmission1896pres_306   9   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   apersonaljourna00andegoog_110   10   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   tennysonhisarta01broogoog_251   11   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   journalofroyalin7188183roya_413   12   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   phoeberowe01thob_185   13   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   narrativemutini00hutcgoog_6   14   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   fortyoneyearsini00robe_316   15   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   lifeofclaudmarti00hill_182   16   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_279   17   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   libraryofworldsbe22warn_51   18   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_267   19   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   memoirofsirwilli00veitrich_344   20   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   newannualarmylis1898lond_946   21   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_211   22   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_234   23   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_231   24   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   reminiscencesofb00edwauoft_230   25   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   passionsanimals00thomgoog_139   26   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   historyofindianm01forr_16   27   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   historyofindianm01forr_14   28   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   historyofindianm01forr_29   29   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   memorialsoflife02edwa_52   30   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   recordsofindianm06indi_263   31   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   macmillansmagazi43macmuoft_261   32   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   memorialsoflife02edwa_49   33   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   livesofindianoff02kayeiala_346   34   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   livesofindianoff02kayeiala_348   35   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   cu31924022927283_266   36   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   cu31924013563865_47   37   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   lifeofisabellath00thobrich_290   38   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   lifeofisabellath00thobrich_286   39   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-17   troubadourselect00gibbuoft_36   40   0.22688246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
