###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_347, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 11.193, act diff: 9.193, ratio: 0.821
#   pulse 2: activated: 71, non-zero: 71, borderline: 69, overall act: 11.193, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 71
#   final overall activation: 11.2
#   number of spread. activ. pulses: 2
#   running time: 19790

###################################
# top k results in TREC format: 

1   rel2-11   lucknow   1   0.38835582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   china   2   0.3522497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   canton   3   0.3116384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   sebastopol   4   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   gwalior   5   0.23604211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   india   6   0.23083122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   agra   7   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1859   8   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1857   9   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1858   10   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1860   11   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1863   12   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1854   13   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   1855   14   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   shahabad   15   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   pekin   16   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   bomarsund   17   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   crimea   18   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   bengal   19   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
1   rel2-11   heights   20   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel2-11   newannualarmylis1874hart_410   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   newannualarmylis1874hart_347   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   lucknow   3   0.38835582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   china   4   0.3522497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   canton   5   0.3116384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   sebastopol   6   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   gwalior   7   0.23604211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   india   8   0.23083122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   agra   9   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1859   10   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1857   11   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1858   12   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1854   13   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1863   14   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1860   15   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1855   16   0.20966806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   shahabad   17   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   pekin   18   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   bomarsund   19   0.181956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   crimea   20   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   bengal   21   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   delhi   22   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   heights   23   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   punjaub   24   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1868   25   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1862   26   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1865   27   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1866   28   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1824   29   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1845   30   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1846   31   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1847   32   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1848   33   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1838   34   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1849   35   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1844   36   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1839   37   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   1861   38   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   sevastopol   39   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel2-11   rowa   40   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
