###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_463, 1
#   newannualarmylis1874hart_347, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 75, non-zero: 75, borderline: 73, overall act: 11.827, act diff: 9.827, ratio: 0.831
#   pulse 2: activated: 5147, non-zero: 5147, borderline: 5144, overall act: 471.231, act diff: 459.403, ratio: 0.975
#   pulse 3: activated: 5147, non-zero: 5147, borderline: 5144, overall act: 471.231, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5147
#   final overall activation: 471.2
#   number of spread. activ. pulses: 3
#   running time: 21544

###################################
# top k results in TREC format: 

1   rel2-16   lucknow   1   0.386605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   punjaub   2   0.2825745   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   sebastopol   3   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   cawnpore   4   0.2349435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   delhi   5   0.23013636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   india   6   0.23013636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   canton   7   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1846   8   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1848   9   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1849   10   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1857   11   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1858   12   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1859   13   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1854   14   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1855   15   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1845   16   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   1844   17   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   cabool   18   0.18963519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   camp   19   0.18124636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-16   gwalior   20   0.18124636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-16   newannualarmylis1874hart_463   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   newannualarmylis1874hart_347   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   lucknow   3   0.386605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   punjaub   4   0.2825745   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   sebastopol   5   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   cawnpore   6   0.2349435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   delhi   7   0.23013636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   india   8   0.23013636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   canton   9   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1848   10   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1849   11   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1857   12   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1858   13   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1859   14   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1854   15   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1855   16   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1845   17   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1846   18   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   1844   19   0.20232438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   apersonaljourna00andegoog_121   20   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   systemsoflandte00cobd_226   21   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reportsofmission1896pres_306   22   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   journalofroyalin7188183roya_413   23   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   apersonaljourna00andegoog_110   24   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   phoeberowe01thob_185   25   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   narrativemutini00hutcgoog_6   26   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   libraryofworldsbe22warn_51   27   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reminiscencesofb00edwauoft_267   28   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   memoirofsirwilli00veitrich_344   29   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   newannualarmylis1898lond_946   30   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reminiscencesofb00edwauoft_211   31   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reminiscencesofb00edwauoft_234   32   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   fortyoneyearsini00robe_316   33   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reminiscencesofb00edwauoft_231   34   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   reminiscencesofb00edwauoft_230   35   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   tennysonhisarta01broogoog_251   36   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   historyofindianm01forr_14   37   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   historyofindianm01forr_29   38   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   memorialsoflife02edwa_52   39   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-16   recordsofindianm06indi_263   40   0.1909303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
