###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_463, 1
#   newannualarmylis1874hart_466, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 50, overall act: 9.902, act diff: 7.902, ratio: 0.798
#   pulse 2: activated: 5124, non-zero: 5124, borderline: 5121, overall act: 543.688, act diff: 533.786, ratio: 0.982
#   pulse 3: activated: 5124, non-zero: 5124, borderline: 5121, overall act: 543.688, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5124
#   final overall activation: 543.7
#   number of spread. activ. pulses: 3
#   running time: 12694

###################################
# top k results in TREC format: 

1   rel2-14   lucknow   1   0.45006868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   gwalior   2   0.3577472   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   delhi   3   0.33280504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   agra   4   0.31201416   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   cawnpore   5   0.25410175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   punjaub   6   0.25410175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1849   7   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1845   8   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1846   9   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1848   10   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1865   11   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1857   12   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1858   13   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1859   14   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1853   15   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1854   16   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1855   17   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   1843   18   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   camp   19   0.20087916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-14   cabool   20   0.18963519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 1   rel2-14   newannualarmylis1874hart_463   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   lucknow   3   0.45006868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   gwalior   4   0.3577472   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   delhi   5   0.33280504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   agra   6   0.31201416   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   cawnpore   7   0.25410175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   punjaub   8   0.25410175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   apersonaljourna00andegoog_121   9   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   systemsoflandte00cobd_226   10   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reportsofmission1896pres_306   11   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   apersonaljourna00andegoog_110   12   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   tennysonhisarta01broogoog_251   13   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   journalofroyalin7188183roya_413   14   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   phoeberowe01thob_185   15   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   narrativemutini00hutcgoog_6   16   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_279   17   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   libraryofworldsbe22warn_51   18   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_267   19   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   memoirofsirwilli00veitrich_344   20   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   newannualarmylis1898lond_946   21   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_211   22   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_234   23   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   fortyoneyearsini00robe_316   24   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_231   25   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   reminiscencesofb00edwauoft_230   26   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   historyofindianm01forr_16   27   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   historyofindianm01forr_14   28   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   historyofindianm01forr_29   29   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   memorialsoflife02edwa_52   30   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   recordsofindianm06indi_263   31   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   macmillansmagazi43macmuoft_261   32   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   memorialsoflife02edwa_49   33   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   livesofindianoff02kayeiala_346   34   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   livesofindianoff02kayeiala_348   35   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   cu31924022927283_266   36   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   cu31924013563865_47   37   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   lifeofisabellath00thobrich_290   38   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   lifeofisabellath00thobrich_286   39   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-14   troubadourselect00gibbuoft_36   40   0.22131112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
