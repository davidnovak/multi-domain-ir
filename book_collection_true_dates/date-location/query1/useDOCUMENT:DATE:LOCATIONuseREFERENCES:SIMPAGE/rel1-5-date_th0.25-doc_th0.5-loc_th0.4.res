###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   newannualarmylis1874hart_464, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 39, non-zero: 39, borderline: 38, overall act: 6.265, act diff: 5.265, ratio: 0.840
#   pulse 2: activated: 39, non-zero: 39, borderline: 38, overall act: 6.265, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 39
#   final overall activation: 6.3
#   number of spread. activ. pulses: 2
#   running time: 11650

###################################
# top k results in TREC format: 

1   rel1-5   cabool   1   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   panjnad river   2   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   gwalior   3   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   delhi   4   0.20101978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   china   5   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   lucknow   6   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   punjaub   7   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   bareilly   8   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   mulka   9   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   chandakhar   10   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   cholin   11   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   shanghai   12   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   neemuch   13   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   kading   14   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1864   15   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1841   16   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1863   17   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1842   18   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1843   19   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
1   rel1-5   1859   20   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel1-5   newannualarmylis1874hart_464   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   newannualarmylis1874hart_463   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   newannualarmylis1874hart_465   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   panjnad river   4   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   cabool   5   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   gwalior   6   0.23228055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   delhi   7   0.20101978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   lucknow   8   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   china   9   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   punjaub   10   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   bareilly   11   0.16011405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   chandakhar   12   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   mulka   13   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   cholin   14   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   shanghai   15   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   neemuch   16   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   kading   17   0.10154631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1847   18   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1868   19   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1846   20   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1869   21   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1863   22   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1848   23   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1849   24   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1860   25   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1862   26   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1840   27   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1841   28   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1857   29   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1839   30   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1838   31   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1843   32   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1842   33   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1864   34   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1858   35   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1859   36   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1871   37   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1872   38   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 1   rel1-5   1855   39   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
