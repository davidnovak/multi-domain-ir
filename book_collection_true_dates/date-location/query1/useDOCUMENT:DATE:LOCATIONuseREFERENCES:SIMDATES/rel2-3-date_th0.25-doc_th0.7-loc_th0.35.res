###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_480, 1
#   newannualarmylis1874hart_466, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 50, overall act: 9.271, act diff: 7.271, ratio: 0.784
#   pulse 2: activated: 13382, non-zero: 13382, borderline: 13378, overall act: 1463.036, act diff: 1453.765, ratio: 0.994
#   pulse 3: activated: 13382, non-zero: 13382, borderline: 13378, overall act: 1463.036, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13382
#   final overall activation: 1463.0
#   number of spread. activ. pulses: 3
#   running time: 18870

###################################
# top k results in TREC format: 

1   rel2-3   lucknow   1   0.5176684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   delhi   2   0.43224394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   cawnpore   3   0.2829063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   gwalior   4   0.27135015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   agra   5   0.25638592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1859   6   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1857   7   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1858   8   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1866   9   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1860   10   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1865   11   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1855   12   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   china   13   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   bareilly   14   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   rooya   15   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1856   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1868   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   city   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   1862   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
1   rel2-3   kakrala   20   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 1   rel2-3   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   lucknow   3   0.5176684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   delhi   4   0.43224394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   hartsannualarmy17hartgoog_258   5   0.34015757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   hartsannualarmy17hartgoog_280   6   0.32678685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   hartsannualarmy17hartgoog_279   7   0.32585508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   hartsannualarmy17hartgoog_257   8   0.32007864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   aroundworld00hend_265   9   0.31391555   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_411   10   0.31306368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_406   11   0.30869815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   newannualarmylis1874hart_203   12   0.30785134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   apersonaljourna00andegoog_38   13   0.30597967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_371   14   0.30597967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_282   15   0.3059095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   historyofindianm01forr_281   16   0.30332932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   historyofindianm01forr_567   17   0.30332932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   historyofindianm01forr_17   18   0.30332932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   historyofindianm01forr_276   19   0.30332932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   narrativemutini00hutcgoog_82   20   0.30332932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_403   21   0.29730222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924088466333_201   22   0.29545137   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   hartsannualarmy17hartgoog_256   23   0.29364625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   livesofindianoff02kayeiala_505   24   0.29194364   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   narrativeajourn10hebegoog_463   25   0.29194364   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   redyearstoryofin00traciala_187   26   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   narrativemutini00hutcgoog_70   27   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   handbooktoenglis00dewa_40   28   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023561933_79   29   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   essaysforcollege00bowm_180   30   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   reminiscencesofb00edwauoft_179   31   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   narrativemutini00hutcgoog_201   32   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   talesofdaringdan00hentiala_102   33   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   indianempirehist01mart_50   34   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   cu31924023968237_215   35   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   illustrationsofh00thoruoft_338   36   0.29100507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   imperialgazette18huntgoog_460   37   0.2899073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   selfhelpwithillu00smiliala_318   38   0.2891433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   northamericanre21lodggoog_625   39   0.2891433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel2-3   redyearstoryofin00traciala_327   40   0.2891433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
