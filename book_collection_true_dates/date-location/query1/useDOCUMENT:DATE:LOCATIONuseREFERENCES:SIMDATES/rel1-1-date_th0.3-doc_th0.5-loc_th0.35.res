###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   newannualarmylis1874hart_480, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 33, non-zero: 33, borderline: 32, overall act: 4.797, act diff: 3.797, ratio: 0.792
#   pulse 2: activated: 33, non-zero: 33, borderline: 32, overall act: 4.797, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 33
#   final overall activation: 4.8
#   number of spread. activ. pulses: 2
#   running time: 11537

###################################
# top k results in TREC format: 

1   rel1-1   lucknow   1   0.34328455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   delhi   2   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   china   3   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   cawnpore   4   0.18114787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   rooya   5   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   bareilly   6   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1858   7   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1859   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1857   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1868   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1856   11   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1860   12   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1865   13   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1862   14   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1863   15   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1864   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1866   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   1855   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   sinho   19   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
1   rel1-1   pekin   20   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel1-1   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   lucknow   2   0.34328455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   delhi   3   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   china   4   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   cawnpore   5   0.18114787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   rooya   6   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   bareilly   7   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1859   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1858   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1856   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1866   11   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1864   12   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1863   13   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1860   14   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1862   15   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1868   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1865   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1857   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   1855   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   sinho   20   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   tangku   21   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   allahabad   22   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   agra   23   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   canton   24   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   grand trunk road   25   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   moradabad   26   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   rekta   27   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   sunga   28   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   pekin   29   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   kirwee   30   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   benares   31   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   india   32   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel1-1   jhansi   33   0.09132942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
