###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_480, 1
#   newannualarmylis1874hart_410, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 50, non-zero: 50, borderline: 48, overall act: 9.494, act diff: 7.494, ratio: 0.789
#   pulse 2: activated: 131993, non-zero: 131993, borderline: 131989, overall act: 12450.217, act diff: 12440.723, ratio: 0.999
#   pulse 3: activated: 131993, non-zero: 131993, borderline: 131989, overall act: 12450.217, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 131993
#   final overall activation: 12450.2
#   number of spread. activ. pulses: 3
#   running time: 41009

###################################
# top k results in TREC format: 

1   rel2-1   lucknow   1   0.5575834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   china   2   0.3851386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   agra   3   0.30601773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1858   4   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1859   5   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1862   6   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1863   7   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1865   8   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1866   9   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1857   10   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1868   11   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1860   12   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   1855   13   0.21207042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   delhi   14   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   shahabad   15   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   india   16   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   canton   17   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   pekin   18   0.18610351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   cawnpore   19   0.18114787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-1   bengal   20   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel2-1   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   lucknow   3   0.5575834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   china   4   0.3851386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   agra   5   0.30601773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   hartsannualarmy17hartgoog_470   6   0.3027293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   hartsannualarmy17hartgoog_439   7   0.30261284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   lifelettersdiari00vetcuoft_181   8   0.28892753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   narrativeindia01hebe_574   9   0.28892753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   cu31924023223872_205   10   0.28231767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   lifeofrichardbri00sanduoft_109   11   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   cu31924023968237_12   12   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   cu31924023968237_10   13   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   collectedverseof01kipluoft_161   14   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   blackwoodsmagazi84edinuoft_80   15   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   reportsofmission1896pres_306   16   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   aroundworld00hend_263   17   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   aroundworld00hend_259   18   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   etoninforties02colegoog_107   19   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   ibis731897brit_621   20   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   ibis731897brit_625   21   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   dailylifeofourfa00beevrich_222   22   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   dailylifeofourfa00beevrich_202   23   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   ibis731897brit_619   24   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   ibis731897brit_617   25   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   mutinyofbengalar00mallrich_129   26   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   cityofrefuge00besarich_108   27   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   mysteriousindiai00chau_204   28   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   recordsofindianm06indi_263   29   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   fiftyyearsofengl02randuoft_14   30   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   n02ontariosession27ontauoft_497   31   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   historyofindianm01forr_16   32   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   historyofindianm01forr_14   33   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   historyofindianm01forr_29   34   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   bishopsconversio00maxwiala_50   35   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   bishopsconversio00maxwiala_11   36   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   bishopsconversio00maxwiala_40   37   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   bishopsconversio00maxwiala_32   38   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   cu31924013493840_133   39   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-1   inscriptionsonta00jerviala_191   40   0.2717864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
