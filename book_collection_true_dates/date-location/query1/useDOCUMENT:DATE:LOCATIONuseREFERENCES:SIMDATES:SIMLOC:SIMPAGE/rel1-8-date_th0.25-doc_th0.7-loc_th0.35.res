###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 26, non-zero: 26, borderline: 25, overall act: 5.025, act diff: 4.025, ratio: 0.801
#   pulse 2: activated: 5112, non-zero: 5112, borderline: 5110, overall act: 458.823, act diff: 453.799, ratio: 0.989
#   pulse 3: activated: 5112, non-zero: 5112, borderline: 5110, overall act: 458.823, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5112
#   final overall activation: 458.8
#   number of spread. activ. pulses: 3
#   running time: 6863

###################################
# top k results in TREC format: 

1   rel1-8   lucknow   1   0.3802589   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   chanda   2   0.23942329   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   arrah   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   chaudhary charan singh airport   4   0.15642719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   bijnaur   5   0.15106827   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   dhurwa   6   0.15071145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   chinhat   7   0.14915691   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   malhaur   8   0.14885111   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   kamlapur   9   0.1477892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   lucknow district   10   0.14699621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   bara banki   11   0.14699621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   jagannathganj   12   0.1387593   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   daya ram   13   0.1372418   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   newan   14   0.13089   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   sohawa   15   0.13081303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   1847   16   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   1848   17   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   1845   18   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   1846   19   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
1   rel1-8   1849   20   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 1   rel1-8   newannualarmylis1874hart_228   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   newannualarmylis1874hart_227   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   newannualarmylis1874hart_229   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   lucknow   4   0.3802589   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   chanda   5   0.23942329   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   apersonaljourna00andegoog_121   6   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   systemsoflandte00cobd_226   7   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reportsofmission1896pres_306   8   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   apersonaljourna00andegoog_110   9   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   tennysonhisarta01broogoog_251   10   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   journalofroyalin7188183roya_413   11   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   phoeberowe01thob_185   12   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   narrativemutini00hutcgoog_6   13   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   fortyoneyearsini00robe_316   14   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   lifeofclaudmarti00hill_182   15   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_279   16   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   libraryofworldsbe22warn_51   17   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_267   18   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   memoirofsirwilli00veitrich_344   19   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   newannualarmylis1898lond_946   20   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_211   21   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_234   22   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_231   23   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   reminiscencesofb00edwauoft_230   24   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   passionsanimals00thomgoog_139   25   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   historyofindianm01forr_16   26   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   historyofindianm01forr_14   27   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   historyofindianm01forr_29   28   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   memorialsoflife02edwa_52   29   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   recordsofindianm06indi_263   30   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   macmillansmagazi43macmuoft_261   31   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   memorialsoflife02edwa_49   32   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   livesofindianoff02kayeiala_346   33   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   livesofindianoff02kayeiala_348   34   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   cu31924022927283_266   35   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   cu31924013563865_47   36   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   lifeofisabellath00thobrich_290   37   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   lifeofisabellath00thobrich_286   38   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   troubadourselect00gibbuoft_36   39   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 1   rel1-8   journaloftravels00wina_252   40   0.18787108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
