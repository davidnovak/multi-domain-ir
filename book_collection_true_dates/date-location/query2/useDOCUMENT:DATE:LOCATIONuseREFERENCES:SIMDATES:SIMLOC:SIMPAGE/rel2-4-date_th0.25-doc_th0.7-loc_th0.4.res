###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.356, act diff: 5.356, ratio: 0.728
#   pulse 2: activated: 221896, non-zero: 221896, borderline: 221893, overall act: 30778.838, act diff: 30771.482, ratio: 1.000
#   pulse 3: activated: 221896, non-zero: 221896, borderline: 221893, overall act: 30778.838, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221896
#   final overall activation: 30778.8
#   number of spread. activ. pulses: 3
#   running time: 60955

###################################
# top k results in TREC format: 

2   rel2-4   ireland   1   0.55900216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   london   2   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   dublin   3   0.3125406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   clifton house   4   0.2670752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   cangort   5   0.26053482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   cangort park   6   0.25689435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   sopwell hall   7   0.25559682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   kilcomin   8   0.25226468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   ballincor   9   0.2498633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   corolanty   10   0.2478623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   loughkeen   11   0.24684286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   ivy hall   12   0.24592662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   shinrone   13   0.24558857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   modreeny   14   0.24120934   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   cloughjordan   15   0.2380296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   fort nisbett   16   0.23649755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   strafford   17   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   england   18   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   borrisokane   19   0.23102096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-4   1641   20   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel2-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_96   3   0.5858838   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   ireland   4   0.55900216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   cu31924029563875_156   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   cu31924029563875_158   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_98   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   london   8   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   dublin   9   0.3125406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   creelofirishstor00barliala_138   10   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_8   11   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_82   12   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   britishmammalsat00johnrich_278   13   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_209   14   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_4   15   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_143   16   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   lettersandjourna02bailuoft_176   17   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_155   18   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_23   19   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_11   20   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_17   21   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_13   22   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_44   23   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_43   24   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   naturalistscabin02smituoft_223   25   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_194   26   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_211   27   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   speeches00philiala_92   28   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_210   29   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_206   30   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_274   31   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_272   32   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_271   33   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_270   34   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   lettersconcerni00clargoog_278   35   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_278   36   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_277   37   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_262   38   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   sexagenarianorre01belo_202   39   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_268   40   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
