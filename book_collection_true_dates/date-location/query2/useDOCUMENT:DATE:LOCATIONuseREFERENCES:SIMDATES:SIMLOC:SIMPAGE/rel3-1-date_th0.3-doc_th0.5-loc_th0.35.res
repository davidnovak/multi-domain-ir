###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 9.762, act diff: 6.762, ratio: 0.693
#   pulse 2: activated: 819391, non-zero: 819391, borderline: 819385, overall act: 104646.213, act diff: 104636.451, ratio: 1.000
#   pulse 3: activated: 819488, non-zero: 819488, borderline: 819426, overall act: 104686.899, act diff: 40.686, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819488
#   final overall activation: 104686.9
#   number of spread. activ. pulses: 3
#   running time: 185149

###################################
# top k results in TREC format: 

2   rel3-1   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   london   3   0.9619621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   clifton house   4   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cangort   5   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cangort park   6   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   shinrone   7   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   ballincor   8   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   corolanty   9   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   ivy hall   10   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cloughjordan   11   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   kilcomin   12   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   sopwell hall   13   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   loughkeen   14   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   england   15   0.7483106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   modreeny   16   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   fort nisbett   17   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   sharavogue   18   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   miltown park   19   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   brosna   20   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ireland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   1641   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_354   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   london   6   0.9619621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   clifton house   7   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cangort   8   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cangort park   9   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   shinrone   10   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ballincor   11   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   corolanty   12   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ivy hall   13   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cloughjordan   14   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   kilcomin   15   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   sopwell hall   16   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   loughkeen   17   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   england   18   0.7483106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   modreeny   19   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_355   20   0.68008155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   fort nisbett   21   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   constitutionalhi03hall_415   22   0.67299247   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_431   23   0.65208036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_96   24   0.63442165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   historicalreview02plow_30   25   0.59899795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   olivercromwell00rossgoog_133   26   0.593303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_131   27   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   fastiecclesiaeh02cottgoog_21   28   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   englishinireland03frou_308   29   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   economichistoryo00obri_320   30   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   fastiecclesiaeh02cottgoog_19   31   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_414   32   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   calendarstatepa12offigoog_336   33   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   sharavogue   34   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   historyirelanda00smilgoog_119   35   0.5785611   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   miltown park   36   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   brosna   37   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   constitutionalhi03hall_413   38   0.5711393   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   carrig   39   0.5709184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_432   40   0.5692939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
