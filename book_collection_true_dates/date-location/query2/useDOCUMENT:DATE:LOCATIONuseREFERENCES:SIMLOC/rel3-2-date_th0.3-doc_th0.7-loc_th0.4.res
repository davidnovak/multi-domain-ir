###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 7.352, act diff: 4.352, ratio: 0.592
#   pulse 2: activated: 264020, non-zero: 264020, borderline: 264014, overall act: 46264.676, act diff: 46257.324, ratio: 1.000
#   pulse 3: activated: 264020, non-zero: 264020, borderline: 264014, overall act: 46264.676, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264020
#   final overall activation: 46264.7
#   number of spread. activ. pulses: 3
#   running time: 66773

###################################
# top k results in TREC format: 

2   rel3-2   ireland   1   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   1641   2   0.62436765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   dublin   3   0.39932328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   clifton house   4   0.36210883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   london   5   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   cangort   6   0.3536148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   cangort park   7   0.34887496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   sopwell hall   8   0.3471836   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   kilcomin   9   0.3428352   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   ballincor   10   0.33969706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   corolanty   11   0.33707952   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   loughkeen   12   0.335745   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   ivy hall   13   0.33454508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   shinrone   14   0.33410218   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   modreeny   15   0.32835904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   cloughjordan   16   0.32418177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   fort nisbett   17   0.32216698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   borrisokane   18   0.31495354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   1845   19   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   england   20   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel3-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   ireland   4   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   1641   5   0.62436765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_291   6   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_411   7   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_130   8   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   fastiecclesiaeh02cottgoog_20   9   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   compendiumofhist02lawl_76   10   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_345   11   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   calendarstatepa12offigoog_335   12   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   castlesofireland00adamiala_301   13   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   confederationofk00meeh_10   14   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   englishinireland03frou_309   15   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   englishinireland03frou_118   16   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_413   17   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   economichistoryo00obri_321   18   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   statechurcheskin00allerich_587   19   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   revolutionaryir00mahagoog_100   20   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historymoderneu11russgoog_386   21   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   memoirsandcorre02castgoog_42   22   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyofdiocese02healiala_23   23   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyofireland3_00will_307   24   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   irishtangleandwa1920john_53   25   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   constitutionalhi03hall_414   26   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   proceedingsofro22roya_307   27   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_319   28   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_286   29   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091786628_78   30   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_421   31   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   politicalstudies00broduoft_370   32   0.59268457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   politicalstudies00broduoft_382   33   0.588027   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_249   34   0.5802522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyirelanda00smilgoog_120   35   0.57843274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_873   36   0.57843274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_315   37   0.5761701   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   irelandsaintpatr00morriala_17   38   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_432   39   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   constitutionalhi03hall_415   40   0.5689248   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
