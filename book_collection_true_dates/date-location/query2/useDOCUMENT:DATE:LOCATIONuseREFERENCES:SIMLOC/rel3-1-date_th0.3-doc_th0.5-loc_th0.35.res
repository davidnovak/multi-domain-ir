###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 6.989, act diff: 3.989, ratio: 0.571
#   pulse 2: activated: 819387, non-zero: 819387, borderline: 819381, overall act: 104643.273, act diff: 104636.284, ratio: 1.000
#   pulse 3: activated: 819433, non-zero: 819433, borderline: 819375, overall act: 104660.142, act diff: 16.869, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819433
#   final overall activation: 104660.1
#   number of spread. activ. pulses: 3
#   running time: 135482

###################################
# top k results in TREC format: 

2   rel3-1   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   london   3   0.9309292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   clifton house   4   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cangort   5   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cangort park   6   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   shinrone   7   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   ballincor   8   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   corolanty   9   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   ivy hall   10   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   cloughjordan   11   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   kilcomin   12   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   sopwell hall   13   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   loughkeen   14   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   modreeny   15   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   fort nisbett   16   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   england   17   0.6520776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   sharavogue   18   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   miltown park   19   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-1   brosna   20   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ireland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   1641   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_354   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   london   6   0.9309292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   clifton house   7   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cangort   8   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cangort park   9   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   shinrone   10   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ballincor   11   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   corolanty   12   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   ivy hall   13   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cloughjordan   14   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   kilcomin   15   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   sopwell hall   16   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   loughkeen   17   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   modreeny   18   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   fort nisbett   19   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   england   20   0.6520776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   sharavogue   21   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   miltown park   22   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   brosna   23   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   carrig   24   0.5709184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_432   25   0.5692939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   historyofireland02daltuoft_409   26   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   olivercromwell00rossgoog_134   27   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cu31924091770861_319   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_413   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   statechurcheskin00allerich_587   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   memoirsandcorre02castgoog_42   31   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   proceedingsofro22roya_307   32   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   compendiumofhist02lawl_76   33   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_345   34   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_291   35   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_411   36   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   irishtangleandwa1920john_53   37   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_130   38   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   cu31924091770861_286   39   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_421   40   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
