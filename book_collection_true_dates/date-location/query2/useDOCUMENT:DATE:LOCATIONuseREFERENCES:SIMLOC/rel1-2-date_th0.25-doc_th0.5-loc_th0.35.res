###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924029563875_157, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 16, overall act: 3.614, act diff: 2.614, ratio: 0.723
#   pulse 2: activated: 627190, non-zero: 627190, borderline: 627188, overall act: 59858.283, act diff: 59854.669, ratio: 1.000
#   pulse 3: activated: 627190, non-zero: 627190, borderline: 627188, overall act: 59858.283, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 627190
#   final overall activation: 59858.3
#   number of spread. activ. pulses: 3
#   running time: 90271

###################################
# top k results in TREC format: 

2   rel1-2   london   1   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   ireland   2   0.27465868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   charing cross   3   0.1780408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   national portrait gallery   4   0.17765059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   trafalgar square   5   0.1776237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   national gallery   6   0.17758803   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   chelsea embankment   7   0.17720437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   haymarket hotel   8   0.17693986   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   haymarket theatre   9   0.17678222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   covent garden market   10   0.17672506   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   hungerford bridge   11   0.17661966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   royal opera house   12   0.17638865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   dublin   13   0.17114265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1690   14   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1699   15   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1701   16   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1689   17   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1651   18   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1641   19   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel1-2   1643   20   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel1-2   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   london   2   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   ireland   3   0.27465868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_86   4   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   prabacteriology00stitrich_6   5   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   masterofmarton01tabo_65   6   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   forum04unkngoog_180   7   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   ethicsofdemocrac00postrich_67   8   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   forum04unkngoog_190   9   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   methodisthymnboo00stev_462   10   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_99   11   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   fourlettersonpro00burkrich_111   12   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   lifeandlettersg02towngoog_168   13   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_381   14   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   herberthooverman00lckell_89   15   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   herberthooverman00lckell_84   16   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_397   17   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_395   18   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_41   19   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   lifeandlettersg02towngoog_140   20   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_29   21   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_378   22   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   ageneralabridgm30vinegoog_86   23   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   universalanthol11brangoog_66   24   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   journalseriesage65royauoft_360   25   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_27   26   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   marriage00welliala_157   27   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   marriage00welliala_151   28   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   textbookonroadsp01spal_421   29   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   saintjohnsfire00sude_109   30   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   marriage00welliala_143   31   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   cu31924020159368_219   32   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   georgeselwyn00selw_326   33   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_79   34   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_74   35   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_73   36   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   practicalobserva00wild_504   37   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   practicalobserva00wild_502   38   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   ageneralabridgm30vinegoog_90   39   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel1-2   marriage00welliala_123   40   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
