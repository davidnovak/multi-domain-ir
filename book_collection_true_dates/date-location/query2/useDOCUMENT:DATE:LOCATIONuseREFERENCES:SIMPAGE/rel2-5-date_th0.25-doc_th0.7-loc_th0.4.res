###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.736, act diff: 5.736, ratio: 0.741
#   pulse 2: activated: 264008, non-zero: 264008, borderline: 264003, overall act: 30608.221, act diff: 30600.485, ratio: 1.000
#   pulse 3: activated: 264008, non-zero: 264008, borderline: 264003, overall act: 30608.221, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264008
#   final overall activation: 30608.2
#   number of spread. activ. pulses: 3
#   running time: 70862

###################################
# top k results in TREC format: 

2   rel2-5   ireland   1   0.48682883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1641   2   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   dublin   3   0.39932328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   london   4   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1845   5   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   england   6   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1690   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1699   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1701   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1689   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1651   11   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   1643   12   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   shoreditch   13   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   town   14   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   europe   15   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   city   16   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   limerick   17   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel2-5   peacock   18   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel2-5   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_601   3   0.5904117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_599   4   0.5631442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   ireland   5   0.48682883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924029563875_156   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924029563875_158   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   1641   8   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_291   9   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_411   10   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_130   11   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   fastiecclesiaeh02cottgoog_20   12   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   compendiumofhist02lawl_76   13   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   englishinireland03frou_309   14   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   englishinireland03frou_118   15   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_345   16   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   calendarstatepa12offigoog_335   17   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_413   18   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   economichistoryo00obri_321   19   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   revolutionaryir00mahagoog_100   20   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   historymoderneu11russgoog_386   21   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   statechurcheskin00allerich_587   22   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   memoirsandcorre02castgoog_42   23   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   historyofireland3_00will_307   24   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   historyofdiocese02healiala_23   25   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   proceedingsofro22roya_307   26   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   irishtangleandwa1920john_53   27   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   constitutionalhi03hall_414   28   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924091770861_319   29   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924091770861_286   30   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924091786628_78   31   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   castlesofireland00adamiala_301   32   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   confederationofk00meeh_10   33   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_421   34   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_370   35   0.42702368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_382   36   0.4233431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_249   37   0.41723633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   historyirelanda00smilgoog_120   38   0.41581392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   cu31924091770861_873   39   0.41581392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_315   40   0.4140484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
