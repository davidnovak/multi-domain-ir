###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1052126, non-zero: 1052126, borderline: 1052118, overall act: 147135.786, act diff: 147127.328, ratio: 1.000
#   pulse 3: activated: 1052145, non-zero: 1052145, borderline: 1052129, overall act: 147147.985, act diff: 12.199, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052145
#   final overall activation: 147148.0
#   number of spread. activ. pulses: 3
#   running time: 130941

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.9890727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   clifton house   2   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   cangort   3   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   cangort park   4   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   shinrone   5   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   cloughjordan   6   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   kilcomin   7   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   corolanty   8   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   sopwell hall   9   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   ballincor   10   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   loughkeen   11   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   ivy hall   12   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   modreeny   13   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   fort nisbett   14   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   england   15   0.6306924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   dublin   16   0.5747046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   1641   17   0.5120209   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   borrisokane   18   0.46007994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   strafford   19   0.45624992   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   miltown park   20   0.45539412   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   ireland   4   0.9890727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   clifton house   5   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   cangort   6   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   cangort park   7   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   shinrone   8   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   cloughjordan   9   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   kilcomin   10   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   corolanty   11   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   sopwell hall   12   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_601   13   0.728054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_96   14   0.71948135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   ballincor   15   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   loughkeen   16   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   ivy hall   17   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   modreeny   18   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_355   19   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   fort nisbett   20   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_599   21   0.6604816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_602   22   0.63622767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   23   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   england   24   0.6306924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   25   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   26   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   27   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   28   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   29   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   30   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   31   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   32   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   33   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_249   34   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_382   35   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_229   36   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_172   37   0.60681516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_21   38   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_120   39   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   redeemerstearsw00urwigoog_21   40   0.60359144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
