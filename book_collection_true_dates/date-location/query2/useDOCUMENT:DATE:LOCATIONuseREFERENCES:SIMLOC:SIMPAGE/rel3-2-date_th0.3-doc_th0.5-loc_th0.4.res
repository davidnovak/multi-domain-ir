###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 10.124, act diff: 7.124, ratio: 0.704
#   pulse 2: activated: 264023, non-zero: 264023, borderline: 264017, overall act: 46267.209, act diff: 46257.085, ratio: 1.000
#   pulse 3: activated: 264263, non-zero: 264263, borderline: 264087, overall act: 46369.927, act diff: 102.718, ratio: 0.002

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264263
#   final overall activation: 46369.9
#   number of spread. activ. pulses: 3
#   running time: 114253

###################################
# top k results in TREC format: 

2   rel3-2   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   ireland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   england   3   0.999999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   dublin   4   0.99886674   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   london   5   0.9126157   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   scotland   6   0.8146025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   munster   7   0.75455904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   strafford   8   0.6967355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   fairfax   9   0.6442473   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   kingdom   10   0.54630965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   france   11   0.5162392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   limerick   12   0.4837414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   rome   13   0.43045634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   spain   14   0.41885406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   1640   15   0.4075367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   clifton house   16   0.36210883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   cangort   17   0.3536148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   europe   18   0.35298395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   cangort park   19   0.34887496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-2   sopwell hall   20   0.3471836   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-2   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   1641   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   england   6   0.999999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   dublin   7   0.99886674   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   london   8   0.9126157   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   scotland   9   0.8146025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   constitutionalhi03hall_414   10   0.75478363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   munster   11   0.75455904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   constitutionalhi03hall_415   12   0.7388591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_432   13   0.72697335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_431   14   0.7081613   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   strafford   15   0.6967355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   englandsfightwit00wals_355   16   0.6719688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   revolutionaryir00mahagoog_145   17   0.6709003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   politicalstudies00broduoft_381   18   0.6655252   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   historyforreadyr03larnuoft_236   19   0.66535723   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_601   20   0.6548349   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   historicalreview02plow_30   21   0.64999264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   irelandbookoflig00reiduoft_150   22   0.64921826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   fairfax   23   0.6442473   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924091770861_892   24   0.6396174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_613   25   0.63868344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_599   26   0.61609036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924091770861_298   27   0.6096785   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_345   28   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   calendarstatepa12offigoog_335   29   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   statechurcheskin00allerich_587   30   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924091770861_319   31   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924091770861_286   32   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   memoirsandcorre02castgoog_42   33   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_413   34   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   cu31924091786628_78   35   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   historyofireland3_00will_307   36   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   castlesofireland00adamiala_301   37   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   confederationofk00meeh_10   38   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   englishinireland03frou_309   39   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-2   englishinireland03frou_118   40   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
