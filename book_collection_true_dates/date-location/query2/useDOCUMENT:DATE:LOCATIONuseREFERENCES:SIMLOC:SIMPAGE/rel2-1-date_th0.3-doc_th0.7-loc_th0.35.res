###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 20, overall act: 7.070, act diff: 5.070, ratio: 0.717
#   pulse 2: activated: 819386, non-zero: 819386, borderline: 819381, overall act: 96386.272, act diff: 96379.202, ratio: 1.000
#   pulse 3: activated: 819386, non-zero: 819386, borderline: 819381, overall act: 96386.272, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819386
#   final overall activation: 96386.3
#   number of spread. activ. pulses: 3
#   running time: 116481

###################################
# top k results in TREC format: 

2   rel2-1   ireland   1   0.65379345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   1641   2   0.39411858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   london   3   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   clifton house   4   0.30962056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   cangort   5   0.30216825   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   cangort park   6   0.29801595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   sopwell hall   7   0.29653528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   kilcomin   8   0.2927311   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   ballincor   9   0.28998804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   corolanty   10   0.2877014   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   loughkeen   11   0.28653607   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   ivy hall   12   0.28548852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   shinrone   13   0.28510198   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   modreeny   14   0.28009242   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   cloughjordan   15   0.27645242   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   fort nisbett   16   0.27469793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   borrisokane   17   0.26842207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   charing cross   18   0.1780408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   national portrait gallery   19   0.17765059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel2-1   trafalgar square   20   0.1776237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel2-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   englandsfightwit00wals_354   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   ireland   3   0.65379345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   englandsfightwit00wals_355   4   0.644176   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   cu31924029563875_158   5   0.5628241   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_432   6   0.5202243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   cu31924029563875_156   7   0.5116861   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   historyofireland02daltuoft_409   8   0.5051707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   olivercromwell00rossgoog_134   9   0.5051707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   bibliothecagrenv03grenrich_31   10   0.4830366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   cu31924091770861_286   11   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   historyofdiocese02healiala_23   12   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   castlesofireland00adamiala_301   13   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   englishinireland03frou_118   14   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   economichistoryo00obri_321   15   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   fastiecclesiaeh02cottgoog_20   16   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_413   17   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_411   18   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_130   19   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_421   20   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   historyofireland3_00will_307   21   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   revolutionaryir00mahagoog_100   22   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   historymoderneu11russgoog_386   23   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   constitutionalhi03hall_414   24   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   confederationofk00meeh_10   25   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   statechurcheskin00allerich_587   26   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   memoirsandcorre02castgoog_42   27   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   proceedingsofro22roya_307   28   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   compendiumofhist02lawl_76   29   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_345   30   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_291   31   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   cu31924091770861_319   32   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   irishtangleandwa1920john_53   33   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   cu31924091786628_78   34   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   englishinireland03frou_309   35   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   calendarstatepa12offigoog_335   36   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   historyofclareda00whit_272   37   0.4753641   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   calendarstatepa12offigoog_475   38   0.4700415   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   politicalstudies00broduoft_370   39   0.46916416   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel2-1   gloucestershiren03londuoft_699   40   0.46632728   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
