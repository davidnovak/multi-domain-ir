###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 6, overall act: 4.215, act diff: 2.215, ratio: 0.526
#   pulse 2: activated: 1016014, non-zero: 1016014, borderline: 1016008, overall act: 126890.304, act diff: 126886.088, ratio: 1.000
#   pulse 3: activated: 1016014, non-zero: 1016014, borderline: 1016008, overall act: 126890.304, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1016014
#   final overall activation: 126890.3
#   number of spread. activ. pulses: 3
#   running time: 126312

###################################
# top k results in TREC format: 

2   rel2-6   ireland   1   0.536683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   england   2   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   dublin   3   0.38038954   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1845   4   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1641   5   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   clifton house   6   0.256897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   cangort   7   0.2505826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   cangort park   8   0.24706864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   sopwell hall   9   0.24581638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   kilcomin   10   0.24260072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   ballincor   11   0.24028353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   corolanty   12   0.23835292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   loughkeen   13   0.23736937   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   ivy hall   14   0.23648544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   shinrone   15   0.2361593   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   strafford   16   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   modreeny   17   0.23193496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   cloughjordan   18   0.22886814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   fort nisbett   19   0.22739066   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   borrisokane   20   0.22210974   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-6   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   ireland   3   0.536683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   parliamentaryhis00obriuoft_215   4   0.47758645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_431   5   0.47398064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   calendarstatepa00levagoog_361   6   0.47340676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirishper01maddgoog_67   7   0.47104666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_315   8   0.4708332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyofmodernb00conauoft_188   9   0.4698304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   harleianmiscell00oldygoog_332   10   0.46526134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   constitutionalhi03hall_415   11   0.4649451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   completehistoryo0306kenn_434   12   0.46446007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   selectspeecheswi00cannuoft_358   13   0.4641117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirishper01maddgoog_229   14   0.46150184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   calendarstatepa12offigoog_21   15   0.46046337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   studentshistoryo03garduoft_42   16   0.4598504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   lifeofdanielocon00cusa_839   17   0.45620295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   englishlawirisht00gibbrich_20   18   0.4559494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   essayonelementso00burn_72   19   0.45461884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   politicalstudies00broduoft_351   20   0.45359546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   england   21   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   memoirsandcorre02castgoog_67   22   0.4522274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   ahistorymodernb03conagoog_187   23   0.45012453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   livingagevolume01littgoog_588   24   0.4495837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_249   25   0.44902313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   publications13irisuoft_16   26   0.44808164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   dublinreview14londuoft_194   27   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   irelandsaintpatr00morriala_201   28   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   contemporaryrev10unkngoog_480   29   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   pt3historyofirel00wriguoft_306   30   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirelanda00smilgoog_120   31   0.4473424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   memoirsirgeorge00creigoog_79   32   0.4473424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   politicalstudies00broduoft_382   33   0.4457008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   hallamsworks04halliala_612   34   0.4451411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   worksrighthonor37burkgoog_439   35   0.4451411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirelanda00smilgoog_312   36   0.4451411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   parliamentaryhis00obriuoft_21   37   0.4451411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   standardlibrary09unkngoog_84   38   0.44514108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   fairfaxcorrespon01johniala_870   39   0.44514108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   methodistmagazin1834meth_610   40   0.44514108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
