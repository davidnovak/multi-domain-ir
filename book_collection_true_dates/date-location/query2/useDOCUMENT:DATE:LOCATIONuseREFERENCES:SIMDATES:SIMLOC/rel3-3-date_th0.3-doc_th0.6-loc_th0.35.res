###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1052129, non-zero: 1052129, borderline: 1052121, overall act: 147134.302, act diff: 147128.617, ratio: 1.000
#   pulse 3: activated: 1052167, non-zero: 1052167, borderline: 1052130, overall act: 147148.408, act diff: 14.105, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052167
#   final overall activation: 147148.4
#   number of spread. activ. pulses: 3
#   running time: 129291

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1641   2   0.99999946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   england   3   0.99908537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   dublin   4   0.84534335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   clifton house   5   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   cangort   6   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   cangort park   7   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   shinrone   8   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   cloughjordan   9   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   kilcomin   10   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   corolanty   11   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   sopwell hall   12   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   ballincor   13   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   loughkeen   14   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   ivy hall   15   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   modreeny   16   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   fort nisbett   17   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   borrisokane   18   0.46007994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   miltown park   19   0.45539412   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   brosna   20   0.4483522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   ireland   4   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   1641   5   0.99999946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   england   6   0.99908537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   dublin   7   0.84534335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   clifton house   8   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   cangort   9   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   cangort park   10   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   shinrone   11   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   cloughjordan   12   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   kilcomin   13   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   corolanty   14   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   sopwell hall   15   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   ballincor   16   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   loughkeen   17   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   ivy hall   18   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   modreeny   19   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   fort nisbett   20   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   21   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   22   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   23   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   24   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   25   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   26   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   27   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   28   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   29   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   30   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_249   31   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_382   32   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_229   33   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_172   34   0.60681516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_21   35   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_120   36   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   redeemerstearsw00urwigoog_21   37   0.60359144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   irelandsaintpatr00morriala_201   38   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   dublinreview14londuoft_194   39   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   pt3historyofirel00wriguoft_306   40   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
