###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1052089, non-zero: 1052089, borderline: 1052081, overall act: 147124.505, act diff: 147118.820, ratio: 1.000
#   pulse 3: activated: 1052111, non-zero: 1052111, borderline: 1052080, overall act: 147128.307, act diff: 3.802, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052111
#   final overall activation: 147128.3
#   number of spread. activ. pulses: 3
#   running time: 152892

###################################
# top k results in TREC format: 

2   rel3-3   1641   1   0.99999946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   ireland   2   0.9999975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   england   3   0.99908537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   dublin   4   0.84534335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1845   5   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1640   6   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1642   7   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   strafford   8   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   dublin castle   9   0.2254641   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   kingdom   10   0.19588166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1846   11   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   1844   12   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   munster   13   0.14621823   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   earth   14   0.12246969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   londonderry   15   0.11970486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   tyrconnel   16   0.117382236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   loughborough   17   0.100260586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   scotland   18   0.091073826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   lisle   19   0.08605033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   surrey   20   0.08371859   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   1641   4   0.99999946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   ireland   5   0.9999975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   england   6   0.99908537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   dublin   7   0.84534335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   8   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   9   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   10   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   11   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   12   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   13   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   14   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   15   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   16   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   17   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_249   18   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_382   19   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_229   20   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_172   21   0.60681516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_21   22   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_120   23   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   redeemerstearsw00urwigoog_21   24   0.60359144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   dublinreview14londuoft_194   25   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   irelandsaintpatr00morriala_201   26   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   pt3historyofirel00wriguoft_306   27   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_24   28   0.6024419   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_351   29   0.6016643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   strangerinirelan00carr_91   30   0.6007721   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   cu31924091770861_873   31   0.5973563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   worksrighthonor37burkgoog_439   32   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   fairfaxcorrespon01johniala_870   33   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_312   34   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_612   35   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   methodistmagazin1834meth_610   36   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   publications13irisuoft_16   37   0.5900143   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_385   38   0.5857836   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_373   39   0.58458275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   fairfaxcorrespon01johniala_705   40   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
