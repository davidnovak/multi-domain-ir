###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 9, overall act: 5.791, act diff: 3.791, ratio: 0.655
#   pulse 2: activated: 263998, non-zero: 263998, borderline: 263993, overall act: 38698.807, act diff: 38693.017, ratio: 1.000
#   pulse 3: activated: 274296, non-zero: 274296, borderline: 274287, overall act: 39505.154, act diff: 806.347, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 274296
#   final overall activation: 39505.2
#   number of spread. activ. pulses: 3
#   running time: 55897

###################################
# top k results in TREC format: 

2   rel2-3   ireland   1   0.8449849   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1641   2   0.67290056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   dublin   3   0.33507118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   england   4   0.33507118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1845   5   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1640   6   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1642   7   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1846   8   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1844   9   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1643   10   0.12462876   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   1639   11   0.12462876   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   essex   12   0.09821233   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   strafford   13   0.09821233   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   belgium   14   0.09219892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   rome   15   0.09219892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   germany   16   0.09219892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel2-3   netherlands   17   0.09219892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel2-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   ireland   3   0.8449849   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   1641   4   0.67290056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   englandsfightwit00wals_355   5   0.63972855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   hallamsworks04halliala_601   6   0.6246651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   hallamsworks04halliala_599   7   0.59106064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   hallamsworks04halliala_602   8   0.57808894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_291   9   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_411   10   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_130   11   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   fastiecclesiaeh02cottgoog_20   12   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   englishinireland03frou_118   13   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   compendiumofhist02lawl_76   14   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_345   15   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   calendarstatepa12offigoog_335   16   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_413   17   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   economichistoryo00obri_321   18   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   statechurcheskin00allerich_587   19   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   revolutionaryir00mahagoog_100   20   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   historymoderneu11russgoog_386   21   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   historyofdiocese02healiala_23   22   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   memoirsandcorre02castgoog_42   23   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   irishtangleandwa1920john_53   24   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   constitutionalhi03hall_414   25   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   historyofireland3_00will_307   26   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   proceedingsofro22roya_307   27   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   cu31924091770861_319   28   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   cu31924091770861_286   29   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   cu31924091786628_78   30   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   castlesofireland00adamiala_301   31   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   confederationofk00meeh_10   32   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   englishinireland03frou_309   33   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_421   34   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   cu31924091770861_891   35   0.5164428   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   politicalstudies00broduoft_370   36   0.50727487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   cu31924082457148_382   37   0.5065976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   irelandundercomm02dunluoft_382   38   0.5065976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   politicalstudies00broduoft_382   39   0.50290865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_249   40   0.49564424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
