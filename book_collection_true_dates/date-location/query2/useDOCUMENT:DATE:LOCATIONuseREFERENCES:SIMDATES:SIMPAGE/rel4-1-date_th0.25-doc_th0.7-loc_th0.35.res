###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1512025, non-zero: 1512025, borderline: 1512015, overall act: 213051.035, act diff: 213038.319, ratio: 1.000
#   pulse 3: activated: 1518335, non-zero: 1518335, borderline: 1518321, overall act: 214009.560, act diff: 958.525, ratio: 0.004

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1518335
#   final overall activation: 214009.6
#   number of spread. activ. pulses: 3
#   running time: 256968

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   0.9548845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1641   2   0.77572834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   dublin   3   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   england   4   0.63565564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   strafford   5   0.46278515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   london   6   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1643   7   0.30758688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1845   8   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1640   9   0.3024225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1642   10   0.3024225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1632   11   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1629   12   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1689   13   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1701   14   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1699   15   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1690   16   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1651   17   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1846   18   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1844   19   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel4-1   1639   20   0.15006922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   ireland   5   0.9548845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   1641   6   0.77572834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_601   7   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_96   8   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_602   9   0.73656374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_355   10   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   studentshistoryo03garduoft_42   11   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_599   12   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   harleianmiscell00oldygoog_332   13   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   dublin   14   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_24   15   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   thomasharrisonr00wessgoog_83   16   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_315   17   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   strangerinirelan00carr_91   18   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   redeemerstearsw00urwigoog_21   19   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyofengland08lodguoft_78   20   0.673832   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   bibliothecagrenv03grenrich_31   21   0.67260575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa00levagoog_361   22   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   constitutionalhi03hall_415   23   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   publications13irisuoft_16   24   0.67004275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirelanda00smilgoog_172   25   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   cu31924091770861_873   26   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   selectspeecheswi00cannuoft_358   27   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirishper01maddgoog_67   28   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyforreadyr03larnuoft_236   29   0.6661447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirishper01maddgoog_210   30   0.6648544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa12offigoog_373   31   0.664576   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_431   32   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   completehistoryo0306kenn_434   33   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   politicalstudies00broduoft_382   34   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_249   35   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   cu31924091770861_898   36   0.66313225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirelanda00smilgoog_120   37   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   politicalstudies00broduoft_385   38   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_432   39   0.659821   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirishper01maddgoog_229   40   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
