###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 614175, non-zero: 614175, borderline: 614165, overall act: 63405.407, act diff: 63398.240, ratio: 1.000
#   pulse 3: activated: 614175, non-zero: 614175, borderline: 614165, overall act: 63405.407, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614175
#   final overall activation: 63405.4
#   number of spread. activ. pulses: 3
#   running time: 77697

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1863   2   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1831   3   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   europe   4   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   russia   5   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1830   6   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1825   7   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   warsaw   8   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   saint petersburg   9   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   caucasus region   10   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   france   11   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   poland   4   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   5   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   6   0.59603727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   7   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   8   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1863   9   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1831   10   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   11   0.5821408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   12   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   13   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   14   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   15   0.5766296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   16   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   17   0.57169396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   18   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   19   0.56320757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   20   0.5631847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   21   0.56042904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   22   0.55935913   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   23   0.55700266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   24   0.55578136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   25   0.55509245   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_390   26   0.5466382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   27   0.5421291   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   28   0.53885806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   29   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_150   30   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_420   31   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_164   32   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   polishpeasantine01thomuoft_216   33   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   34   0.53849673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_398   35   0.53483915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_406   36   0.52428573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_127   37   0.52428573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   38   0.5230319   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   39   0.51934797   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924027992605_272   40   0.51738226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
