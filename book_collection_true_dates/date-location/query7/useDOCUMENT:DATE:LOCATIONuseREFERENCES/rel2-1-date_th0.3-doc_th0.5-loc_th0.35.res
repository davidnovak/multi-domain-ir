###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 569154, non-zero: 569154, borderline: 569147, overall act: 55800.532, act diff: 55794.954, ratio: 1.000
#   pulse 3: activated: 569175, non-zero: 569175, borderline: 569153, overall act: 55805.190, act diff: 4.658, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 569175
#   final overall activation: 55805.2
#   number of spread. activ. pulses: 3
#   running time: 100789

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.9986811   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   1863   2   0.9983717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   russia   3   0.991545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   1831   4   0.89031047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   europe   5   0.78339565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   warsaw   6   0.41589758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   saint petersburg   7   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   caucasus region   8   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   vienna   9   0.24539183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   france   10   0.2348728   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   rome   11   0.19141074   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   vistula   12   0.18339217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   1830   13   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   1825   14   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   1772   15   0.13733643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   great britain   16   0.13626097   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   alhambra   17   0.1057713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   prussia   18   0.10277367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   united states   19   0.097642064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-1   germany   20   0.07747127   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   poland   3   0.9986811   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   1863   4   0.9983717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   russia   5   0.991545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   1831   6   0.89031047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   europe   7   0.78339565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   8   0.5646878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   9   0.55283916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   10   0.54607207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   11   0.54430574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   12   0.53788686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   13   0.5363658   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   14   0.53355694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   15   0.5321019   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   16   0.53196466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   17   0.5274982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   18   0.52340466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   19   0.51755995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   20   0.51458126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   21   0.5134345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   outlineseuropea04beargoog_503   22   0.5088632   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_390   23   0.49981302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   24   0.49698666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   25   0.49499035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   26   0.49149355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_164   27   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_150   28   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   anthologyofmoder00selviala_126   29   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_420   30   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   polishpeasantine01thomuoft_216   31   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_398   32   0.4871994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_406   33   0.47593424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_127   34   0.47593424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   americanhistoric19151916jame_633   35   0.4754629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_41   36   0.47430265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   libraryofhistori12weit_41   37   0.47359833   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924027992605_272   38   0.4685737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924060429846_289   39   0.46650395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-1   germanemperorhis00bigeuoft_118   40   0.46641362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
