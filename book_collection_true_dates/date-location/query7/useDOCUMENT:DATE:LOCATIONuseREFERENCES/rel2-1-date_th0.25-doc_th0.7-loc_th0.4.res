###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 114985, non-zero: 114985, borderline: 114980, overall act: 13589.472, act diff: 13583.895, ratio: 1.000
#   pulse 3: activated: 114985, non-zero: 114985, borderline: 114980, overall act: 13589.472, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 114985
#   final overall activation: 13589.5
#   number of spread. activ. pulses: 3
#   running time: 30979

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   1863   2   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   1831   3   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   europe   4   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   russia   5   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   warsaw   6   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   caucasus region   7   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   saint petersburg   8   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   1830   9   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   1825   10   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
7   rel2-1   france   11   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_420   4   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_164   5   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   polishpeasantine01thomuoft_216   6   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_150   7   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_126   8   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   secretsocieties01heckgoog_192   9   0.46403852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_426   10   0.45812804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   1863   11   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   1831   12   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   someproblemspea00goog_191   13   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_388   14   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_428   15   0.44684103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_43   16   0.4458609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   foundingofgerman02sybeuoft_555   17   0.44113356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   polishexperienc02hallgoog_20   18   0.43818647   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_231   19   0.4368995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_398   20   0.43651208   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   21   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   sketchesinpoland00littrich_57   22   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_410   23   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   stormssunshineof01mackuoft_43   24   0.43438724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   advocateofpeace82amerrich_327   25   0.42767447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_193   26   0.42606664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaasitis00gurouoft_44   27   0.42475736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924028567273_442   28   0.42408082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   withworldspeople05ridp_189   29   0.4231688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   alhambrakremlin00prim_311   30   0.42253587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   sketchesinpoland00littrich_35   31   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_406   32   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_127   33   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   bookofmemoriesof00halluoft_375   34   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_390   35   0.41241583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_151   36   0.40926063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_442   37   0.40398747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_245   38   0.40398747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_408   39   0.40040228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_424   40   0.40001714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
