###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 10, overall act: 4.478, act diff: 2.478, ratio: 0.553
#   pulse 2: activated: 164533, non-zero: 164533, borderline: 164526, overall act: 14586.921, act diff: 14582.443, ratio: 1.000
#   pulse 3: activated: 164533, non-zero: 164533, borderline: 164526, overall act: 14586.921, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164533
#   final overall activation: 14586.9
#   number of spread. activ. pulses: 3
#   running time: 30250

###################################
# top k results in TREC format: 

7   rel2-3   poland   1   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1831   2   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1830   3   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1863   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1825   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   europe   6   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   russia   7   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   caucasus region   8   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   saint petersburg   9   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   warsaw   10   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaasitis00gurouoft_44   3   0.3771705   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   poland   4   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   peacehandbooks08grea_81   5   0.34449425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1863   6   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1830   7   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1831   8   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1825   9   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   polishpeasantine01thomuoft_216   10   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   englishwomaninr00unkngoog_305   11   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_420   12   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_150   13   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_164   14   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   anthologyofmoder00selviala_126   15   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russianpolitica02kovagoog_288   16   0.33488518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   americanhistoric19151916jame_633   17   0.33223477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924088053800_313   18   0.3275239   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   secretsocieties01heckgoog_192   19   0.31967625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_426   20   0.31572562   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   polishexperienc02hallgoog_20   21   0.3123545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   someproblemspea00goog_191   22   0.310129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_388   23   0.310129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_428   24   0.30822575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_43   25   0.3075772   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   lectureonsocialp00tochrich_22   26   0.30513784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   foundingofgerman02sybeuoft_555   27   0.30445483   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   essaysonliturgio00neal_234   28   0.30166653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_231   29   0.30166653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_398   30   0.30141175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   sketchesinpoland00littrich_57   31   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   leavesfromdiaryo00grevuoft_137   32   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_410   33   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_33   34   0.29806015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   advocateofpeace82amerrich_327   35   0.29561743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_358   36   0.29542425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   kosciuszkobiogra00gard_193   37   0.29456675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   withworldspeople05ridp_189   38   0.2926757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   alhambrakremlin00prim_311   39   0.2922631   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   historytenyears06blangoog_465   40   0.29007652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.35
