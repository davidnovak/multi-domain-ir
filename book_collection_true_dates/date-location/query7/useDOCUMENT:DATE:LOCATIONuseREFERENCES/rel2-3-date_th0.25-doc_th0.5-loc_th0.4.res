###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 10, overall act: 4.478, act diff: 2.478, ratio: 0.553
#   pulse 2: activated: 129734, non-zero: 129734, borderline: 129728, overall act: 12001.921, act diff: 11997.443, ratio: 1.000
#   pulse 3: activated: 129734, non-zero: 129734, borderline: 129728, overall act: 12001.921, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 129734
#   final overall activation: 12001.9
#   number of spread. activ. pulses: 3
#   running time: 28018

###################################
# top k results in TREC format: 

7   rel2-3   poland   1   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   1831   2   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   1830   3   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   1863   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   1825   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   europe   6   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   russia   7   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   caucasus region   8   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   saint petersburg   9   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-3   warsaw   10   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   poland   3   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   1830   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   1831   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   1863   6   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   1825   7   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_358   8   0.29542425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historicmorgancl00eame_250   9   0.2686337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   americanstatepap06unit_14   10   0.26821375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_468   11   0.2655389   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   vanburenmartin00sheprich_317   12   0.2648886   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historyofcuyahog00john_646   13   0.25795445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   honoursregistero00univuoft_158   14   0.25795445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historylasalleco00bald_94   15   0.25571498   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   teachingofmodern00handuoft_37   16   0.25515556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historylasalleco00bald_397   17   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   canadianmagazine37torouoft_184   18   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   woolgrowingtari05wriguoft_70   19   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   congressionalse175offigoog_304   20   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historyoffayette00barr_276   21   0.24660507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   remakingofmodern06marruoft_185   22   0.24530816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_85   23   0.23559643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_834   24   0.23484221   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   ahistorymodernb03conagoog_409   25   0.23398842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   tribuneofnovasc26gran_116   26   0.23349519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   worksofhuberthow19bancrich_676   27   0.23349519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   documentsassemb56assegoog_537   28   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   worthiesworkersb00fiel_97   29   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   ourhomerailways00unkngoog_37   30   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   americanstatesmen18morsrich_315   31   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historylasalleco00bald_294   32   0.23251869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   cu31924014512614_19   33   0.23164365   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   woolgrowingtari05wriguoft_73   34   0.23071484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   memorialsofcambr02coopiala_386   35   0.22923031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_368   36   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   chroniclesofgree00browrich_155   37   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   listofresidents2195116_646   38   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   historytenyears06blangoog_531   39   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-3   calmetsdictionar00calm_948   40   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
