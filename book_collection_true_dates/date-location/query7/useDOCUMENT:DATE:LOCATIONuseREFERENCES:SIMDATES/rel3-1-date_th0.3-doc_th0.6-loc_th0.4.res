###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164540, non-zero: 164540, borderline: 164532, overall act: 21208.878, act diff: 21201.711, ratio: 1.000
#   pulse 3: activated: 164540, non-zero: 164540, borderline: 164532, overall act: 23732.258, act diff: 2523.380, ratio: 0.106
#   pulse 4: activated: 184634, non-zero: 184634, borderline: 184624, overall act: 26641.917, act diff: 2909.659, ratio: 0.109
#   pulse 5: activated: 184634, non-zero: 184634, borderline: 184624, overall act: 29674.669, act diff: 3032.752, ratio: 0.102
#   pulse 6: activated: 204286, non-zero: 204286, borderline: 204265, overall act: 32845.892, act diff: 3171.223, ratio: 0.097
#   pulse 7: activated: 656458, non-zero: 656458, borderline: 656429, overall act: 108201.692, act diff: 75355.800, ratio: 0.696
#   pulse 8: activated: 749825, non-zero: 749825, borderline: 749308, overall act: 138813.433, act diff: 30611.741, ratio: 0.221
#   pulse 9: activated: 5684847, non-zero: 5684847, borderline: 5683741, overall act: 2223659.705, act diff: 2084846.272, ratio: 0.938
#   pulse 10: activated: 6660690, non-zero: 6660690, borderline: 5862657, overall act: 3071178.273, act diff: 847518.569, ratio: 0.276

###################################
# spreading activation process summary: 
#   final number of activated nodes: 6660690
#   final overall activation: 3071178.3
#   number of spread. activ. pulses: 10
#   running time: 1281708

###################################
# top k results in TREC format: 

7   rel3-1   montreal   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   kittery   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   bethel   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   holy land   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   izmit   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   arnold   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   ellesmere   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   southern europe   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   cyrenaica   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   loretto   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   hainan   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   senlis   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   carlsbad   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   fiji islands   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   livorno   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   seneca   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   livonia   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   iznik   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   tonquin   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
7   rel3-1   nederland   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   montreal   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   kittery   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   bethel   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   holy land   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   izmit   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   arnold   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   ellesmere   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   southern europe   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   cyrenaica   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   loretto   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   hainan   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   senlis   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   carlsbad   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   fiji islands   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   livorno   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   seneca   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   livonia   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   iznik   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   tonquin   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   cat   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   can   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   cal   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   bry   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   bri   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   bot   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   ber   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   bay   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   ayr   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   art   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   ark   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   ala   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   aix   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   abt   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   mifflin   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   nottingham   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   exeter hall   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   zeebrugge   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   serbia   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   dumfries   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel3-1   nederland   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
