###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 164540, non-zero: 164540, borderline: 164533, overall act: 14589.132, act diff: 14583.730, ratio: 1.000
#   pulse 3: activated: 164548, non-zero: 164548, borderline: 164540, overall act: 16754.094, act diff: 2164.962, ratio: 0.129
#   pulse 4: activated: 164548, non-zero: 164548, borderline: 164540, overall act: 17963.880, act diff: 1209.786, ratio: 0.067
#   pulse 5: activated: 204284, non-zero: 204284, borderline: 204274, overall act: 21871.059, act diff: 3907.180, ratio: 0.179
#   pulse 6: activated: 204284, non-zero: 204284, borderline: 204273, overall act: 23328.667, act diff: 1457.608, ratio: 0.062

###################################
# spreading activation process summary: 
#   final number of activated nodes: 204284
#   final overall activation: 23328.7
#   number of spread. activ. pulses: 6
#   running time: 60960

###################################
# top k results in TREC format: 

7   rel2-3   1830   1   0.75010353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1831   2   0.7501035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   poland   3   0.606939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1863   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1825   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1832   6   0.31103024   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1829   7   0.31103024   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   europe   8   0.27741334   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   russia   9   0.24229038   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1864   10   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1862   11   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1824   12   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1826   13   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   saint petersburg   14   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   warsaw   15   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   caucasus region   16   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1833   17   0.13421866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   1828   18   0.13421866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   finland   19   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-3   siberia   20   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   1830   3   0.75010353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   1831   4   0.7501035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   poland   5   0.606939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaasitis00gurouoft_44   6   0.5489593   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   historicmorgancl00eame_250   7   0.52340513   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924031684685_794   8   0.5071223   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   hazardsregister_11phil_125   9   0.4891824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   anthologyofmoder00selviala_126   10   0.48740724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   englishwomaninr00unkngoog_305   11   0.48740724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_834   12   0.47783205   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   memorialsofcambr02coopiala_386   13   0.47704765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   americanstatepap06unit_14   14   0.4757006   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   historyoffayette00barr_276   15   0.4737279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   historytenyears06blangoog_465   16   0.46414274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924031684685_796   17   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   austriaviennapr00kohlgoog_511   18   0.4614857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_43   19   0.45650566   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   contributionsol01lowegoog_315   20   0.45574504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   lectureonsocialp00tochrich_22   21   0.45401916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   foundingofgerman02sybeuoft_555   22   0.45332238   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_785   23   0.45223546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_85   24   0.45196947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   historylasalleco00bald_94   25   0.45086136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_231   26   0.45047548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_358   27   0.4458026   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   peacehandbooks08grea_81   28   0.4446382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   historylasalleco00bald_443   29   0.44319975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   woolgrowingtari05wriguoft_70   30   0.44196448   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   remakingofmodern06marruoft_185   31   0.4392175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   hazardsregister_11phil_124   32   0.43710488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   hazardsregistero14phil_402   33   0.4347707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   bookofmemoriesof00halluoft_375   34   0.43471354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924073899001_127   35   0.43471354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   russianpolitica02kovagoog_288   36   0.43442667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaasitis00gurouoft_43   37   0.43413693   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   italyitalianisland03spal_345   38   0.43313882   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   kosciuszkobiogra00gard_151   39   0.43198234   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-3   speechesdukewel02hazlgoog_270   40   0.4313349   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
