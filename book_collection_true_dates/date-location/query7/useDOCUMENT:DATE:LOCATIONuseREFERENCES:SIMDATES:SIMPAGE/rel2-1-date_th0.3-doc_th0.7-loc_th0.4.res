###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 114991, non-zero: 114991, borderline: 114986, overall act: 13592.183, act diff: 13584.757, ratio: 0.999
#   pulse 3: activated: 141770, non-zero: 141770, borderline: 141764, overall act: 16677.645, act diff: 3085.462, ratio: 0.185
#   pulse 4: activated: 141770, non-zero: 141770, borderline: 141764, overall act: 17696.160, act diff: 1018.515, ratio: 0.058
#   pulse 5: activated: 141770, non-zero: 141770, borderline: 141764, overall act: 18147.564, act diff: 451.404, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 141770
#   final overall activation: 18147.6
#   number of spread. activ. pulses: 5
#   running time: 39087

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1831   2   0.6138376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1863   3   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1830   4   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   europe   5   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   russia   6   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   warsaw   7   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1832   8   0.28931874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   caucasus region   9   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   saint petersburg   10   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1862   11   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1864   12   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1829   13   0.21793541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   1825   14   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-1   france   15   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   1831   4   0.6138376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaasitis00gurouoft_44   5   0.56281877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_126   6   0.54209924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924031684685_794   7   0.527213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_43   8   0.49963737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   foundingofgerman02sybeuoft_555   9   0.49520865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   polishpeasantine01thomuoft_216   10   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_420   11   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_164   12   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_150   13   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_231   14   0.49123964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   englishwomaninr00unkngoog_305   15   0.48598373   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   peacehandbooks08grea_81   16   0.48464677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   americanhistoric19151916jame_633   17   0.4732734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   stormssunshineof01mackuoft_43   18   0.47196594   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   russianpolitica02kovagoog_288   19   0.47032177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_127   20   0.46912938   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   bookofmemoriesof00halluoft_375   21   0.46912938   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   polishexperienc02hallgoog_20   22   0.46575403   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_151   23   0.4652754   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   secretsocieties01heckgoog_192   24   0.46403852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924088053800_314   25   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924031684685_796   26   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924088053800_312   27   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_245   28   0.4603108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_426   29   0.45812804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   1863   30   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   operationsofsurg1902jaco_633   31   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   conquestsofcross02hodd_183   32   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   recoverypoland00krycgoog_229   33   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_121   34   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   advocateofpeace82amerrich_327   35   0.45556256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   someproblemspea00goog_191   36   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_388   37   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_428   38   0.44684103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   1830   39   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-1   cu31924073899001_33   40   0.4426476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
