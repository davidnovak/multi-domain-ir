###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 129742, non-zero: 129742, borderline: 129736, overall act: 12004.143, act diff: 11998.741, ratio: 1.000
#   pulse 3: activated: 129742, non-zero: 129742, borderline: 129736, overall act: 14168.725, act diff: 2164.582, ratio: 0.153
#   pulse 4: activated: 129742, non-zero: 129742, borderline: 129736, overall act: 14960.583, act diff: 791.858, ratio: 0.053
#   pulse 5: activated: 129742, non-zero: 129742, borderline: 129736, overall act: 15237.700, act diff: 277.117, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 129742
#   final overall activation: 15237.7
#   number of spread. activ. pulses: 5
#   running time: 32064

###################################
# top k results in TREC format: 

7   rel2-3   1830   1   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1831   2   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   poland   3   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1863   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1825   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1832   6   0.27006692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1829   7   0.27006692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   europe   8   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1824   9   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1862   10   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1864   11   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1826   12   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   caucasus region   13   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   russia   14   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   saint petersburg   15   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   warsaw   16   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1830   3   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1831   4   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924031684685_794   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924031684685_796   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   americanstatepap06unit_14   7   0.41813976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historicmorgancl00eame_250   8   0.3938935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_358   9   0.38722083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   remakingofmodern06marruoft_185   10   0.38481882   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   vanburenmartin00sheprich_317   11   0.37228313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_834   12   0.3693847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   documentsassemb56assegoog_537   13   0.36623633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historylasalleco00bald_294   14   0.36594114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924014512614_19   15   0.3646427   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofcuyahog00john_646   16   0.3643785   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   honoursregistero00univuoft_158   17   0.3643785   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historylasalleco00bald_94   18   0.36330682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   poland   19   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   completeworksna23hawtgoog_15   20   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924012131599_142   21   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   listofresidents2195116_646   22   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyhawtreyf02hawtgoog_537   23   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   chroniclesofgree00browrich_155   24   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historytenyears06blangoog_531   25   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   calmetsdictionar00calm_101   26   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   eclecticmagazin59unkngoog_361   27   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   halfcenturyofbos00damr_449   28   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofcolesco00perr_320   29   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_368   30   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   miscellaneouses01alisgoog_260   31   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   annualregister108unkngoog_455   32   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   calmetsdictionar00calm_948   33   0.36103582   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_468   34   0.35834366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   hazardsregister_11phil_125   35   0.3545347   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   hazardsregistero14phil_402   36   0.34899557   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyoffayette00barr_276   37   0.34754166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   canadianmagazine37torouoft_184   38   0.34732908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   woolgrowingtari05wriguoft_70   39   0.34732908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   congressionalse175offigoog_304   40   0.34732908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
