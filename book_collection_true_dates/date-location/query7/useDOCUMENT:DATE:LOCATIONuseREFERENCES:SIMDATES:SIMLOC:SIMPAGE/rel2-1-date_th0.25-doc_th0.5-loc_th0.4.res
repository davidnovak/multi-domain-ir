###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 115012, non-zero: 115012, borderline: 115007, overall act: 13598.337, act diff: 13590.910, ratio: 0.999
#   pulse 3: activated: 141798, non-zero: 141798, borderline: 141791, overall act: 16684.769, act diff: 3086.432, ratio: 0.185
#   pulse 4: activated: 593396, non-zero: 593396, borderline: 593386, overall act: 67805.454, act diff: 51120.685, ratio: 0.754
#   pulse 5: activated: 612812, non-zero: 612812, borderline: 612747, overall act: 72730.235, act diff: 4924.781, ratio: 0.068
#   pulse 6: activated: 2119699, non-zero: 2119699, borderline: 2119503, overall act: 434434.688, act diff: 361704.453, ratio: 0.833
#   pulse 7: activated: 3567439, non-zero: 3567439, borderline: 3523501, overall act: 1038072.900, act diff: 603638.212, ratio: 0.581
#   pulse 8: activated: 8783224, non-zero: 8783224, borderline: 8424296, overall act: 5456882.130, act diff: 4418809.232, ratio: 0.810
#   pulse 9: activated: 10626661, non-zero: 10626661, borderline: 5232802, overall act: 7497215.502, act diff: 2040333.373, ratio: 0.272
#   pulse 10: activated: 11327579, non-zero: 11327579, borderline: 3693109, overall act: 8603122.103, act diff: 1105906.601, ratio: 0.129

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327579
#   final overall activation: 8603122.1
#   number of spread. activ. pulses: 10
#   running time: 3155374

###################################
# top k results in TREC format: 

7   rel2-1   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   calatayud   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   sandpoint   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   mchenry county   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   embarcadero   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   point barrow   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   holy land   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   pangbourne   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   new lenox   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   calatayud   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   sandpoint   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   mchenry county   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   embarcadero   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   point barrow   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   holy land   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   pangbourne   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bustransporindex02newyuoft_65   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   north egremont   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   poynton   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   kittery   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   newport railway station   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   thurcaston   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   lincoln square   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   morro bay   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   brocket hall   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   merrymeeting bay   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   edmund street   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   new castle county   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bidford   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   lidiana   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bethel   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   betham   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   warburg   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   hawthornden   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   north kensington   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   shillong   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   new lenox   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
