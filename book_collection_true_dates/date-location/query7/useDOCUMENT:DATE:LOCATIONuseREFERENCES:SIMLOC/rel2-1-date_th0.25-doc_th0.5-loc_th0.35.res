###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 569202, non-zero: 569202, borderline: 569195, overall act: 55810.530, act diff: 55804.953, ratio: 1.000
#   pulse 3: activated: 569222, non-zero: 569222, borderline: 569200, overall act: 55815.181, act diff: 4.651, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 569222
#   final overall activation: 55815.2
#   number of spread. activ. pulses: 3
#   running time: 156625

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.9986811   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1863   2   0.9983717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   russia   3   0.991545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1831   4   0.89031047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   europe   5   0.78339565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   warsaw   6   0.41589758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   italy   7   0.35656202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   saint petersburg   8   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   caucasus region   9   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   libiya   10   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   blue tower   11   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   as sifarah al qatariyah   12   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   damascus governorate   13   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   arnous   14   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   jaddat as salihiyah   15   0.29657817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   netherlands   16   0.296515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   tshili   17   0.29582384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   al hijaz   18   0.29556167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   cuba   19   0.2954348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   al hind   20   0.29480168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   poland   3   0.9986811   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   1863   4   0.9983717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russia   5   0.991545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   1831   6   0.89031047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   europe   7   0.78339565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   8   0.5646878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   9   0.55283916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   10   0.54607207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   11   0.54430574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   12   0.53788686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   13   0.5363658   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   14   0.53355694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   15   0.5321019   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   16   0.53196466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   17   0.5274982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   18   0.52340466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   19   0.51755995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   20   0.51458126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   21   0.5134345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   outlineseuropea04beargoog_503   22   0.5088632   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_390   23   0.49981302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   24   0.49698666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   25   0.49499035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   26   0.49149355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_164   27   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_150   28   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   anthologyofmoder00selviala_126   29   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_420   30   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   polishpeasantine01thomuoft_216   31   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_398   32   0.4871994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_406   33   0.47593424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_127   34   0.47593424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   americanhistoric19151916jame_633   35   0.4754629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_41   36   0.47430265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   libraryofhistori12weit_41   37   0.47359833   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924027992605_272   38   0.4685737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924060429846_289   39   0.46650395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   germanemperorhis00bigeuoft_118   40   0.46641362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
