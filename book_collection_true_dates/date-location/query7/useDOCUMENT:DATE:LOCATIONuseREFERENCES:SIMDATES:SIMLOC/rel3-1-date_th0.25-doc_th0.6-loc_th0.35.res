###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 614229, non-zero: 614229, borderline: 614219, overall act: 63417.099, act diff: 63409.932, ratio: 1.000
#   pulse 3: activated: 706999, non-zero: 706999, borderline: 706985, overall act: 75848.821, act diff: 12431.722, ratio: 0.164
#   pulse 4: activated: 723966, non-zero: 723966, borderline: 723944, overall act: 95755.774, act diff: 19906.953, ratio: 0.208
#   pulse 5: activated: 729585, non-zero: 729585, borderline: 729485, overall act: 131656.331, act diff: 35900.557, ratio: 0.273
#   pulse 6: activated: 3053712, non-zero: 3053712, borderline: 3052953, overall act: 764321.158, act diff: 632664.828, ratio: 0.828
#   pulse 7: activated: 6279984, non-zero: 6279984, borderline: 6210346, overall act: 2632428.683, act diff: 1868107.522, ratio: 0.710
#   pulse 8: activated: 9015917, non-zero: 9015917, borderline: 7844704, overall act: 5705003.841, act diff: 3072575.159, ratio: 0.539
#   pulse 9: activated: 9586784, non-zero: 9586784, borderline: 5076203, overall act: 6453447.887, act diff: 748444.046, ratio: 0.116
#   pulse 10: activated: 9698409, non-zero: 9698409, borderline: 4313007, overall act: 6678374.996, act diff: 224927.109, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9698409
#   final overall activation: 6678375.0
#   number of spread. activ. pulses: 10
#   running time: 2042064

###################################
# top k results in TREC format: 

7   rel3-1   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   point barrow   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   bettws   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   holy land   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   pangbourne   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   porthleven   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   rataplan   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   warboys   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
7   rel3-1   new lenox   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   point barrow   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   bettws   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   holy land   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   pangbourne   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   porthleven   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   rataplan   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   warboys   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   thurcaston   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   lincoln square   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   morro bay   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   city of boston   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   brocket hall   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   merrymeeting bay   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   edmund street   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   new castle county   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   bidford   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   lidiana   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   bethel   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   betham   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   warburg   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   hawthornden   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   north kensington   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   shillong   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   calatayud   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   sandpoint   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   mchenry county   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   embarcadero   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel3-1   new lenox   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
