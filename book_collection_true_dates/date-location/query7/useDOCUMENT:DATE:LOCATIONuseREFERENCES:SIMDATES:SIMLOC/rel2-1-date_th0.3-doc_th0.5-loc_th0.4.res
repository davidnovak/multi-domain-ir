###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 115009, non-zero: 115009, borderline: 115004, overall act: 13596.509, act diff: 13590.931, ratio: 1.000
#   pulse 3: activated: 141788, non-zero: 141788, borderline: 141782, overall act: 16681.971, act diff: 3085.462, ratio: 0.185
#   pulse 4: activated: 141788, non-zero: 141788, borderline: 141781, overall act: 17700.839, act diff: 1018.868, ratio: 0.058
#   pulse 5: activated: 141788, non-zero: 141788, borderline: 141780, overall act: 20726.526, act diff: 3025.688, ratio: 0.146
#   pulse 6: activated: 162842, non-zero: 162842, borderline: 162805, overall act: 25025.491, act diff: 4298.964, ratio: 0.172
#   pulse 7: activated: 1156821, non-zero: 1156821, borderline: 1156727, overall act: 196930.936, act diff: 171905.446, ratio: 0.873
#   pulse 8: activated: 2245236, non-zero: 2245236, borderline: 2237760, overall act: 593444.439, act diff: 396513.503, ratio: 0.668
#   pulse 9: activated: 8029969, non-zero: 8029969, borderline: 7913418, overall act: 4512577.403, act diff: 3919132.962, ratio: 0.868
#   pulse 10: activated: 9145492, non-zero: 9145492, borderline: 4949240, overall act: 5830978.086, act diff: 1318400.684, ratio: 0.226

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9145492
#   final overall activation: 5830978.1
#   number of spread. activ. pulses: 10
#   running time: 1597952

###################################
# top k results in TREC format: 

7   rel2-1   victoria bridge   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   south oxford   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   nederland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   bapaume   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   montreux   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   ecorse   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   montreal   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   terra firma   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   betsey   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   new castle county   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   bethel   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   warburg   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   hawthornden   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   shillong   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   calatayud   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   mchenry county   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   embarcadero   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   point barrow   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   holy land   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
7   rel2-1   milton of balgonie   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   victoria bridge   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   south oxford   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   nederland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   bapaume   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   montreux   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   ecorse   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   montreal   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   terra firma   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   betsey   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   new castle county   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   bethel   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   warburg   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   hawthornden   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   shillong   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   calatayud   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   mchenry county   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   embarcadero   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   point barrow   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   holy land   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   dnipropetrovsk   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   nashua river   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   tonquin   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   izmit   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   felixstowe   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   strawberry point   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   jabal at tur   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   camp hancock   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   kentville   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   arnold   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   longfield   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   euston   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   lake minnetonka   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   yadkin   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   haight   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   ellesmere   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   southern europe   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   cyrenaica   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   point pelee   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   kittery   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel2-1   milton of balgonie   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
