###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 115009, non-zero: 115009, borderline: 115004, overall act: 13596.509, act diff: 13590.931, ratio: 1.000
#   pulse 3: activated: 141788, non-zero: 141788, borderline: 141782, overall act: 16681.971, act diff: 3085.462, ratio: 0.185
#   pulse 4: activated: 141788, non-zero: 141788, borderline: 141781, overall act: 17700.839, act diff: 1018.868, ratio: 0.058
#   pulse 5: activated: 162806, non-zero: 162806, borderline: 162797, overall act: 22705.521, act diff: 5004.683, ratio: 0.220
#   pulse 6: activated: 183843, non-zero: 183843, borderline: 183805, overall act: 27217.824, act diff: 4512.303, ratio: 0.166
#   pulse 7: activated: 1174508, non-zero: 1174508, borderline: 1174396, overall act: 199112.347, act diff: 171894.523, ratio: 0.863
#   pulse 8: activated: 2783419, non-zero: 2783419, borderline: 2775794, overall act: 753295.843, act diff: 554183.496, ratio: 0.736
#   pulse 9: activated: 8056468, non-zero: 8056468, borderline: 7861742, overall act: 4537624.113, act diff: 3784328.269, ratio: 0.834
#   pulse 10: activated: 9232766, non-zero: 9232766, borderline: 5004028, overall act: 5961348.949, act diff: 1423724.837, ratio: 0.239

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9232766
#   final overall activation: 5961348.9
#   number of spread. activ. pulses: 10
#   running time: 1644918

###################################
# top k results in TREC format: 

7   rel2-1   victoria bridge   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   south oxford   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   nederland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   bapaume   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   montreux   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   ecorse   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   montreal   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   terra firma   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   betsey   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   new castle county   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   bethel   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   warburg   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   hawthornden   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   shillong   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   calatayud   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   mchenry county   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   embarcadero   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   point barrow   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   holy land   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel2-1   milton of balgonie   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   victoria bridge   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   south oxford   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   nederland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bapaume   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   montreux   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   ecorse   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   montreal   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   terra firma   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   betsey   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   new castle county   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   bethel   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   warburg   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   hawthornden   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   shillong   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   calatayud   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   mchenry county   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   embarcadero   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   point barrow   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   holy land   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   nashua river   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   tonquin   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   izmit   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   felixstowe   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   strawberry point   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   jabal at tur   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   camp hancock   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   kentville   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   arnold   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   tribunealmanacpo1889newy_52   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   longfield   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   euston   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   lake minnetonka   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   yadkin   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   haight   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   ellesmere   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   southern europe   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   cyrenaica   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   point pelee   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   kittery   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel2-1   milton of balgonie   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
