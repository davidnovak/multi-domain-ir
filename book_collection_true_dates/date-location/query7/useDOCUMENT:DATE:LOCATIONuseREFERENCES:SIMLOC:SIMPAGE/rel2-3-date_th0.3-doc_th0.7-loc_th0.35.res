###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 164555, non-zero: 164555, borderline: 164548, overall act: 14591.506, act diff: 14586.104, ratio: 1.000
#   pulse 3: activated: 164555, non-zero: 164555, borderline: 164548, overall act: 14591.506, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164555
#   final overall activation: 14591.5
#   number of spread. activ. pulses: 3
#   running time: 45001

###################################
# top k results in TREC format: 

7   rel2-3   poland   1   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1863   2   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1830   3   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1831   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   1825   5   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   europe   6   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   libiya   7   0.17795949   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   blue tower   8   0.17794982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   as sifarah al qatariyah   9   0.17764854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   damascus governorate   10   0.17760518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   arnous   11   0.177107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   jaddat as salihiyah   12   0.17710339   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   netherlands   13   0.17706412   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   tshili   14   0.1766345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   italy   15   0.1766156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   al hijaz   16   0.17647155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   cuba   17   0.1763927   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   al hind   18   0.17599933   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   orient palace   19   0.17557716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-3   damascus   20   0.17557713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924031684685_794   3   0.5008992   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924031684685_796   4   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaasitis00gurouoft_44   5   0.3771705   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   poland   6   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   peacehandbooks08grea_81   7   0.34449425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1863   8   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1830   9   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1831   10   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   1825   11   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   polishpeasantine01thomuoft_216   12   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   englishwomaninr00unkngoog_305   13   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_420   14   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_150   15   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_164   16   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   anthologyofmoder00selviala_126   17   0.33814445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russianpolitica02kovagoog_288   18   0.33488518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   americanhistoric19151916jame_633   19   0.33223477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924088053800_313   20   0.3275239   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   secretsocieties01heckgoog_192   21   0.31967625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_426   22   0.31572562   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   polishexperienc02hallgoog_20   23   0.3123545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   someproblemspea00goog_191   24   0.310129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_388   25   0.310129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_428   26   0.30822575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_43   27   0.3075772   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   lectureonsocialp00tochrich_22   28   0.30513784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   foundingofgerman02sybeuoft_555   29   0.30445483   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   essaysonliturgio00neal_234   30   0.30166653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_231   31   0.30166653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_398   32   0.30141175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   sketchesinpoland00littrich_57   33   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   leavesfromdiaryo00grevuoft_137   34   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_410   35   0.30136725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cu31924073899001_33   36   0.29806015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   advocateofpeace82amerrich_327   37   0.29561743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_358   38   0.29542425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   kosciuszkobiogra00gard_193   39   0.29456675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-3   withworldspeople05ridp_189   40   0.2926757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
