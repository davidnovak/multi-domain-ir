###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924088053800_313, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 11, overall act: 3.854, act diff: 2.854, ratio: 0.741
#   pulse 2: activated: 79732, non-zero: 79732, borderline: 79729, overall act: 6178.444, act diff: 6174.591, ratio: 0.999
#   pulse 3: activated: 79732, non-zero: 79732, borderline: 79729, overall act: 6178.444, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 79732
#   final overall activation: 6178.4
#   number of spread. activ. pulses: 3
#   running time: 32107

###################################
# top k results in TREC format: 

7   rel1-1   poland   1   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   1863   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   1831   3   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   russia   4   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   warsaw   5   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   europe   6   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   caucasus region   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   saint petersburg   8   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel1-1   france   9   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel1-1   cu31924088053800_313   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   cu31924088053800_314   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   cu31924088053800_312   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   poland   4   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   1863   5   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   1831   6   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   russia   7   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   hereditarygenius1869galt_317   8   0.20549993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   adictionaryasan01chrigoog_672   9   0.19493648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   polishexperienc02hallgoog_20   10   0.19483899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   ulondonlibrarycat00univrich_658   11   0.19483899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   warsaw   12   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   europe   13   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   smithsonianmisce461905smit_190   14   0.19140452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   biographicalreg00gradgoog_40   15   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   materialypoisto00ligoog_106   16   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   historyofsaintlov2scha_1061   17   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   conversationswit01seniuoft_271   18   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   journalofroyalin3186870roya_391   19   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   downersofamerica00byudown_60   20   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   historyanddiges13moorgoog_840   21   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   cu31924017141361_565   22   0.18601009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   catalogofborrowd00museiala_23   23   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   indianaindianans00dunn_75   24   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   cu31924092508690_268   25   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   honoursregistero00univuoft_158   26   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   advocateofpeace82amerrich_293   27   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   materialypoisto00ligoog_242   28   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   anindextonorfol00ryegoog_80   29   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   publications27oxfo_376   30   0.16749324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   smithsonianmisce461905smit_188   31   0.16331558   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   biographicalreg00gradgoog_437   32   0.16174571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   proceedingsofses00jack_167   33   0.15983713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   ibis43brit_170   34   0.15983713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_297   35   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   socialstateofgre00berm_258   36   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_295   37   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   recessstudies00grangoog_375   38   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   pennycyclopaedi02goog_199   39   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel1-1   historyofcityofn00park_613   40   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
