###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 614223, non-zero: 614223, borderline: 614213, overall act: 63417.025, act diff: 63408.010, ratio: 1.000
#   pulse 3: activated: 614301, non-zero: 614301, borderline: 614238, overall act: 63456.156, act diff: 39.131, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614301
#   final overall activation: 63456.2
#   number of spread. activ. pulses: 3
#   running time: 203234

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   1863   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   russia   3   0.9999968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   1831   4   0.9993778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   europe   5   0.9947201   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   1830   6   0.9013743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   warsaw   7   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   prussia   8   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   france   9   0.6406877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   1825   10   0.59416175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   austria   11   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   rome   12   0.5655755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vienna   13   0.5513926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   siberia   14   0.49865395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   caucasus region   15   0.4460924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   saint petersburg   16   0.4411424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   italy   17   0.41747585   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   england   18   0.3746869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   germany   19   0.3551839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vistula   20   0.30991042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   poland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1863   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_313   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russia   6   0.9999968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1831   7   0.9993778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   europe   8   0.9947201   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1830   9   0.9013743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   warsaw   10   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_409   11   0.7196519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   prussia   12   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_427   13   0.6988682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_407   14   0.6668041   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_312   15   0.6557341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   france   16   0.6406877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_794   17   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_82   18   0.61318374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_425   19   0.60940266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   20   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_502   21   0.60426176   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   22   0.59603727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1825   23   0.59416175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   24   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_389   25   0.5896844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   26   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   austria   27   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   28   0.5821408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   29   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_796   30   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   31   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   32   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   33   0.5766296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924027992605_271   34   0.5758547   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_314   35   0.5750714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   36   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   37   0.57169396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_405   38   0.5691476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   39   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   rome   40   0.5655755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
