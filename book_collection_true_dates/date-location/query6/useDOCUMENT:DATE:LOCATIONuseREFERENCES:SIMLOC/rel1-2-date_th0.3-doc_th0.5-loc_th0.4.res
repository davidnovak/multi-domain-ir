###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924032471116_269, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 10, overall act: 2.785, act diff: 1.785, ratio: 0.641
#   pulse 2: activated: 11, non-zero: 11, borderline: 10, overall act: 2.785, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11
#   final overall activation: 2.8
#   number of spread. activ. pulses: 2
#   running time: 25217

###################################
# top k results in TREC format: 

6   rel1-2   england   1   0.29225492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   north   2   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   1861   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   1901   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   1900   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   1268   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   great britain   7   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   new york   8   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   south   9   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
6   rel1-2   boston   10   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 6   rel1-2   cu31924032471116_269   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   england   2   0.29225492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   north   3   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   1861   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   1900   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   1901   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   1268   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   new york   8   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   great britain   9   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   south   10   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
# 6   rel1-2   boston   11   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.4
