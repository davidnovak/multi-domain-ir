###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   apoliticalhisto03reidgoog_64, 1
#   cu31924032471116_269, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 19, non-zero: 19, borderline: 17, overall act: 6.456, act diff: 4.456, ratio: 0.690
#   pulse 2: activated: 829922, non-zero: 829922, borderline: 829919, overall act: 112266.668, act diff: 112260.212, ratio: 1.000
#   pulse 3: activated: 829928, non-zero: 829928, borderline: 829923, overall act: 112268.193, act diff: 1.524, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 829928
#   final overall activation: 112268.2
#   number of spread. activ. pulses: 3
#   running time: 163001

###################################
# top k results in TREC format: 

6   rel2-1   england   1   0.69871813   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   united states   2   0.28088853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   south   3   0.24746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   new york   4   0.24746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   north   5   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   france   6   0.19488531   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   europe   7   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   new orleans   8   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   1861   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   1901   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   1900   11   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   1268   12   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   london   13   0.14978474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   great britain   14   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   boston   15   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   charleston   16   0.10183607   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
6   rel2-1   palmerston   17   0.09493332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 6   rel2-1   cu31924032471116_269   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   apoliticalhisto03reidgoog_64   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   england   3   0.69871813   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   cu31924032471116_270   4   0.5737689   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   apoliticalhisto03reidgoog_65   5   0.53463304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   cu31924032471116_268   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   apoliticalhisto03reidgoog_63   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   united states   8   0.28088853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   cu31924032471116_271   9   0.27926475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   piercepenniless00nashgoog_93   10   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   workscomprisingh02cowpuoft_385   11   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   piercepenniless00nashgoog_46   12   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   piercepenniless00nashgoog_90   13   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   lyricallifepoems00mass_223   14   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   ageneralabridgm30vinegoog_96   15   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   economicreview05unkngoog_70   16   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   memoirnathhawth00hawtrich_308   17   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   economicreview05unkngoog_64   18   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   principlesofecon01marsuoft_54   19   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   principlesofecon01marsuoft_88   20   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   marriage00welliala_104   21   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   saintjohnsfire00sude_140   22   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   cu31924084839194_405   23   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   ageneralabridgm30vinegoog_61   24   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   marriage00welliala_154   25   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   lyricallifepoems00mass_244   26   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   lyricallifepoems00mass_243   27   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   lyricallifepoems00mass_257   28   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   naturalhistoryof00smitrich_88   29   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   ahistorycritici09saingoog_432   30   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   northmenincumbe00ferggoog_22   31   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   arbordayitshisto00scharich_120   32   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   sirtom00oliprich_25   33   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   sirtom00oliprich_24   34   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   piercepenniless00nashgoog_19   35   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   farmersplanterse00johnrich_243   36   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   philosophicalcri02brom_277   37   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   farmersplanterse00johnrich_270   38   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   faithofourfather00gibbrich_33   39   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 6   rel2-1   historyantiquit02ling_57   40   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
