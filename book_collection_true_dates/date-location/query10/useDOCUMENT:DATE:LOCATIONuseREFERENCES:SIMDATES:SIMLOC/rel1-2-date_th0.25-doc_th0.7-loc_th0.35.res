###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   reportofbritisha88brit_635, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 21, overall act: 3.938, act diff: 2.938, ratio: 0.746
#   pulse 2: activated: 22, non-zero: 22, borderline: 21, overall act: 3.938, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 22
#   final overall activation: 3.9
#   number of spread. activ. pulses: 2
#   running time: 18206

###################################
# top k results in TREC format: 

10   rel1-2   california   1   0.2819792   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   colorado   2   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   dakota   3   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   idaho   4   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   nevada   5   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1887   6   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1863   7   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1884   8   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1859   9   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1857   10   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1858   11   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1860   12   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1852   13   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1885   14   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   1880   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   oregon   16   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   montana   17   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   mono county   18   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   bodie   19   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
10   rel1-2   united states   20   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 10   rel1-2   reportofbritisha88brit_635   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   california   2   0.2819792   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   colorado   3   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   dakota   4   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   idaho   5   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   nevada   6   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1885   7   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1852   8   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1860   9   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1858   10   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1857   11   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1859   12   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1884   13   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1863   14   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1887   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   1880   16   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   oregon   17   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   montana   18   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   mono county   19   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   bodie   20   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   united states   21   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel1-2   arizona   22   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
