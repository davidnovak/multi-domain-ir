###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   waterqualityreso00sacr_128, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 68, non-zero: 68, borderline: 67, overall act: 7.495, act diff: 6.495, ratio: 0.867
#   pulse 2: activated: 68, non-zero: 68, borderline: 67, overall act: 7.495, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 68
#   final overall activation: 7.5
#   number of spread. activ. pulses: 2
#   running time: 39698

###################################
# top k results in TREC format: 

10   rel1-5   los vaqueros   1   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   california   2   0.21753265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   miwok   3   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   sierra nevada   4   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   brentwood   5   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   pleasanton   6   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   san francisco bay area   7   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   san jose   8   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   oakland   9   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   livermore   10   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   stockton   11   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   san joaquin valley   12   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   san joaquin river   13   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   contra costa county   14   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1898   15   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1974   16   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1852   17   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1851   18   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1850   19   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
10   rel1-5   1859   20   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 10   rel1-5   waterqualityreso00sacr_128   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   los vaqueros   2   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   california   3   0.21753265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   miwok   4   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   sierra nevada   5   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   oakland   6   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   pleasanton   7   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   san francisco bay area   8   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   san jose   9   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   livermore   10   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   stockton   11   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   san joaquin valley   12   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   san joaquin river   13   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   brentwood   14   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   contra costa county   15   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1898   16   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1974   17   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1852   18   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1851   19   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1850   20   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1858   21   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1866   22   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1864   23   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1861   24   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1868   25   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1880   26   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1900   27   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1867   28   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1869   29   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1860   30   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1862   31   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1863   32   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1865   33   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1856   34   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1857   35   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1859   36   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1892   37   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1893   38   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1894   39   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel1-5   1895   40   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
