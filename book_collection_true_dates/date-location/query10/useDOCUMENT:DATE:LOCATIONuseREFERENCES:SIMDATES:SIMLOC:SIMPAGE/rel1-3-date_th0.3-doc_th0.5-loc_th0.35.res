###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   ingersollscentur00inge_101, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 10, non-zero: 10, borderline: 9, overall act: 3.319, act diff: 2.319, ratio: 0.699
#   pulse 2: activated: 10, non-zero: 10, borderline: 9, overall act: 3.319, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10
#   final overall activation: 3.3
#   number of spread. activ. pulses: 2
#   running time: 5464

###################################
# top k results in TREC format: 

10   rel1-3   california   1   0.33573484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   san francisco   2   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   1848   3   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   1849   4   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   fort   5   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   monterey   6   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-3   sacramento   7   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 10   rel1-3   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   ingersollscentur00inge_100   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   ingersollscentur00inge_102   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   california   4   0.33573484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   san francisco   5   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   1848   6   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   1849   7   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   fort   8   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   monterey   9   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-3   sacramento   10   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
