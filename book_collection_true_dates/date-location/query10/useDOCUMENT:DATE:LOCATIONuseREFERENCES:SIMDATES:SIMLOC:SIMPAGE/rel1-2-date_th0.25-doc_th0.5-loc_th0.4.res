###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   reportofbritisha88brit_635, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 23, overall act: 4.862, act diff: 3.862, ratio: 0.794
#   pulse 2: activated: 24, non-zero: 24, borderline: 23, overall act: 4.862, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 24
#   final overall activation: 4.9
#   number of spread. activ. pulses: 2
#   running time: 20481

###################################
# top k results in TREC format: 

10   rel1-2   california   1   0.2819792   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   colorado   2   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   dakota   3   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   idaho   4   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   nevada   5   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1887   6   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1863   7   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1884   8   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1859   9   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1857   10   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1858   11   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1860   12   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1852   13   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1885   14   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   1880   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   oregon   16   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   montana   17   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   mono county   18   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   bodie   19   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
10   rel1-2   united states   20   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 10   rel1-2   reportofbritisha88brit_635   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   reportofbritisha88brit_634   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   reportofbritisha88brit_636   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   california   4   0.2819792   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   colorado   5   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   dakota   6   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   idaho   7   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   nevada   8   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1852   9   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1860   10   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1858   11   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1857   12   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1859   13   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1884   14   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1863   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1885   16   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1887   17   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   1880   18   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   mono county   19   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   montana   20   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   oregon   21   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   bodie   22   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   united states   23   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel1-2   arizona   24   0.11165442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
