###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   reportofbritisha88brit_635, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 43, non-zero: 43, borderline: 41, overall act: 7.774, act diff: 5.774, ratio: 0.743
#   pulse 2: activated: 223284, non-zero: 223284, borderline: 223280, overall act: 29832.424, act diff: 29824.650, ratio: 1.000
#   pulse 3: activated: 223284, non-zero: 223284, borderline: 223280, overall act: 29832.424, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 223284
#   final overall activation: 29832.4
#   number of spread. activ. pulses: 3
#   running time: 64990

###################################
# top k results in TREC format: 

10   rel2-6   california   1   0.5908367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1860   2   0.2537134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   dakota   3   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   colorado   4   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   idaho   5   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   nevada   6   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   san francisco   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1847   8   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1848   9   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1946   10   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1849   11   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1850   12   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1854   13   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1844   14   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1858   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1887   16   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1880   17   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1859   18   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1863   19   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-6   1857   20   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 10   rel2-6   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   reportofbritisha88brit_635   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   california   3   0.5908367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   history00ofcaliforeaverich_123   4   0.39884531   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   americanhistory00jameiala_382   5   0.39884531   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   departmentofinte091996unit_831   6   0.39884531   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   letterfromsecret00unitrich_137   7   0.37326783   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   chineseimmigrat00coolgoog_517   8   0.3587548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   unitedstatescon654offigoog_239   9   0.35503525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   hindsprecedentso00hind_203   10   0.3473609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   investmentspecul00conwrich_21   11   0.34419778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   letterfromsecret00unitrich_193   12   0.34233654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   californiaitspro00cali_136   13   0.34187624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   fouryearsinbrit01mayngoog_482   14   0.3411766   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   letterfromsecret00unitrich_149   15   0.32222173   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   trainingtrotting01marv_102   16   0.31935617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   pastpresentofcit00coll_107   17   0.31924313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   worksbancro25bancuoft_150   18   0.3171823   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   sanfranciscohist01youn_138   19   0.3171823   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   cu31924009150057_119   20   0.31299764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   letterfromsecret00unitrich_93   21   0.31153283   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   coastreview551891899sanf_93   22   0.31139702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   contestforcalif02kenngoog_105   23   0.30338624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   newenglandinlife00bridiala_230   24   0.30338624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   goldhuntersofcal01fari_125   25   0.30338624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   contestforcalif02kenngoog_212   26   0.30338624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   venezuelaacomme00commgoog_341   27   0.30338624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   contestforcalif02kenngoog_94   28   0.2956227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   contestforcalif02kenngoog_100   29   0.2956227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   cyclopediaoffarm00bail_191   30   0.29462492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   history00ofcaliforeaverich_140   31   0.29331177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   buildingindustri11111cont_479   32   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   forestryinmining00browrich_1   33   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   gt4waterpowerfromge190calirich_43   34   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   herberthooverman00lckell_55   35   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   lampshield1963stan_49   36   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   mindsandmanners00horngoog_239   37   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   typologyscriptu02fairgoog_607   38   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   travelsathome00twaigoog_136   39   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-6   buildingindustri11111cont_461   40   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
