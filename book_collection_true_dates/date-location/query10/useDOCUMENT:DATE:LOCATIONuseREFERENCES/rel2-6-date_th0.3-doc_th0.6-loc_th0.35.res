###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   reportofbritisha88brit_635, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 43, non-zero: 43, borderline: 41, overall act: 7.774, act diff: 5.774, ratio: 0.743
#   pulse 2: activated: 207458, non-zero: 207458, borderline: 207455, overall act: 28767.672, act diff: 28759.899, ratio: 1.000
#   pulse 3: activated: 207458, non-zero: 207458, borderline: 207455, overall act: 28767.672, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207458
#   final overall activation: 28767.7
#   number of spread. activ. pulses: 3
#   running time: 62572

###################################
# top k results in TREC format: 

10   rel2-6   california   1   0.5908367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1860   2   0.2537134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   dakota   3   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   colorado   4   0.2205592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   idaho   5   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   nevada   6   0.17586164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   san francisco   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1847   8   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1848   9   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1946   10   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1849   11   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1850   12   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1854   13   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1844   14   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1858   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1887   16   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1880   17   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1859   18   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1863   19   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-6   1857   20   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel2-6   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   reportofbritisha88brit_635   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   california   3   0.5908367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   buildingindustri11111cont_479   4   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   forestryinmining00browrich_1   5   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   gt4waterpowerfromge190calirich_43   6   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   herberthooverman00lckell_55   7   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   lampshield1963stan_49   8   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   mindsandmanners00horngoog_239   9   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   typologyscriptu02fairgoog_607   10   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   travelsathome00twaigoog_136   11   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   buildingindustri11111cont_461   12   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_565   13   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_563   14   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_559   15   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_557   16   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_555   17   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   flavouringmateri00clarrich_186   18   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_553   19   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_551   20   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_561   21   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   buildingindustri11111cont_415   22   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   pacificcoastvaca00morrrich_172   23   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs2931_934   24   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   commercialeduca00unkngoog_2   25   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs2931_961   26   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   bulletinofmuseum124harv_324   27   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   miltonmarksoral01markrich_528   28   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   ineztaleofalamo00evaniala_4   29   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   birdlore24nati_162   30   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   minutesofgeneral1915pres_549   31   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs3383_921   32   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   miltonmarksoral01markrich_440   33   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs3383_919   34   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs3383_917   35   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   miltonmarksoral01markrich_454   36   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   govuscourtsca9briefs3383_909   37   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   birdlore24nati_103   38   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   miltonmarksoral01markrich_466   39   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-6   flavouringmateri00clarrich_177   40   0.2871142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
