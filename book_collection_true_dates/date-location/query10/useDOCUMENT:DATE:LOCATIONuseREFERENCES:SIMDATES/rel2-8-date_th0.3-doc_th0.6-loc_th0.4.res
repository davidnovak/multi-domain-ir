###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   ingersollscentur00inge_101, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 26, non-zero: 26, borderline: 24, overall act: 6.178, act diff: 4.178, ratio: 0.676
#   pulse 2: activated: 335438, non-zero: 335438, borderline: 335432, overall act: 45416.857, act diff: 45410.680, ratio: 1.000
#   pulse 3: activated: 335438, non-zero: 335438, borderline: 335432, overall act: 48141.581, act diff: 2724.724, ratio: 0.057
#   pulse 4: activated: 397811, non-zero: 397811, borderline: 397803, overall act: 56623.508, act diff: 8481.926, ratio: 0.150
#   pulse 5: activated: 397811, non-zero: 397811, borderline: 397802, overall act: 59821.871, act diff: 3198.364, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 397811
#   final overall activation: 59821.9
#   number of spread. activ. pulses: 5
#   running time: 86596

###################################
# top k results in TREC format: 

10   rel2-8   1848   1   0.75819767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1849   2   0.75819767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   california   3   0.74130195   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   san francisco   4   0.6014204   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1850   5   0.55831623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1847   6   0.4240238   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   sacramento   7   0.23575458   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1851   8   0.18144342   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1846   9   0.18144342   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1860   10   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1946   11   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1854   12   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   1844   13   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   monterey   14   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   fort   15   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   calaveras county   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   new jersey   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   colombo   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   golden west   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
10   rel2-8   new york   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 10   rel2-8   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   policepeaceoffic19461946sanf_79   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   1848   3   0.75819767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   1849   4   0.75819767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   california   5   0.74130195   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   pacificcoasthighways00john_249   6   0.677216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   sanfranciscohist01youn_332   7   0.6404397   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   plain00srockiesbibwagnrich_226   8   0.62719434   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   tennesseespartne00hartrich_12   9   0.61017287   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   san francisco   10   0.6014204   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   pictorialhistory00hals_669   11   0.59647757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   histamerpeople00bearrich_317   12   0.59399307   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   cu31924026424451_290   13   0.5911271   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   journalofhouseof184546indi_439   14   0.589991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storycalifornia06nortgoog_396   15   0.58892417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   makingofourcount00burn_412   16   0.58772147   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storyofcaliforni02nort_261   17   0.58738667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storycalifornia05nortgoog_262   18   0.58738667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   instructionsforc00kentrich_12   19   0.58738667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   beginningsofsanf02eldr_201   20   0.5859722   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storyofcaliforni02nort_249   21   0.5808299   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   historyofsanfran00will_85   22   0.5802304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   historyoforegong04lymauoft_123   23   0.5793995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   californiafruits04wick_53   24   0.5780064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storycalifornia06nortgoog_250   25   0.5737953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   asirememberthemg00good_211   26   0.5733033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   oldnavaldayssket00meis_241   27   0.573255   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   establishmentofs00good_364   28   0.5731584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   establishmentofs00good_260   29   0.5731222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   lettersjohnmurr01forbrich_148   30   0.57312214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   storyofcaliforni02nort_289   31   0.57312214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   popularhistoryof02howirich_403   32   0.5693853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   buildingnationev00coff_369   33   0.5685816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   americanjournalo24ameruoft_281   34   0.5683296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   sanfranciscodire1867lang_760   35   0.5671169   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   cu31924018821656_239   36   0.56698203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   sanfranciscobusi51922sanf_457   37   0.5667602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   southofmarketjou119251926sout_126   38   0.566752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   historyofuniteds00gord_350   39   0.56660324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 10   rel2-8   schoolhistoryofu01hart_299   40   0.565264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
