###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   ingersollscentur00inge_101, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 26, non-zero: 26, borderline: 24, overall act: 6.178, act diff: 4.178, ratio: 0.676
#   pulse 2: activated: 335438, non-zero: 335438, borderline: 335432, overall act: 45416.857, act diff: 45410.680, ratio: 1.000
#   pulse 3: activated: 397811, non-zero: 397811, borderline: 397803, overall act: 54302.661, act diff: 8885.804, ratio: 0.164
#   pulse 4: activated: 397811, non-zero: 397811, borderline: 397803, overall act: 58646.666, act diff: 4344.005, ratio: 0.074
#   pulse 5: activated: 397811, non-zero: 397811, borderline: 397803, overall act: 61308.867, act diff: 2662.201, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 397811
#   final overall activation: 61308.9
#   number of spread. activ. pulses: 5
#   running time: 94904

###################################
# top k results in TREC format: 

10   rel2-8   1848   1   0.6912149   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1849   2   0.6912149   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   california   3   0.6281759   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   san francisco   4   0.4465667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1850   5   0.43607727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1847   6   0.43607724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   sacramento   7   0.23575458   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1851   8   0.20198224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1846   9   0.20198224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1860   10   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1946   11   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1854   12   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   1844   13   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   monterey   14   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   fort   15   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   calaveras county   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   new jersey   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   colombo   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   golden west   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
10   rel2-8   new york   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 10   rel2-8   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   policepeaceoffic19461946sanf_79   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   pacificcoasthighways00john_249   3   0.69247276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   1848   4   0.6912149   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   1849   5   0.6912149   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   sanfranciscohist01youn_332   6   0.65219486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   plain00srockiesbibwagnrich_226   7   0.6386509   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   california   8   0.6281759   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   tennesseespartne00hartrich_12   9   0.6217533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   journalofhouseof184546indi_439   10   0.6208604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   histamerpeople00bearrich_317   11   0.61021525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   pictorialhistory00hals_669   12   0.60592085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   cu31924026424451_290   13   0.60313386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storycalifornia06nortgoog_396   14   0.60208124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   makingofourcount00burn_412   15   0.597318   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storycalifornia05nortgoog_262   16   0.596989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storyofcaliforni02nort_261   17   0.596989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   instructionsforc00kentrich_12   18   0.5969889   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   beginningsofsanf02eldr_201   19   0.595599   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   historyofsanfran00will_85   20   0.594587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   californiafruits04wick_53   21   0.59425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storyofcaliforni02nort_249   22   0.59306175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   historyoforegong04lymauoft_123   23   0.5919565   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   oldnavaldayssket00meis_241   24   0.58835214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   buildingnationev00coff_369   25   0.58771086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storycalifornia06nortgoog_250   26   0.58718103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   southofmarketjou119251926sout_126   27   0.58488905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   asirememberthemg00good_211   28   0.5831472   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   establishmentofs00good_364   29   0.5830048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   storyofcaliforni02nort_289   30   0.5829692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   establishmentofs00good_260   31   0.5829692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   lettersjohnmurr01forbrich_148   32   0.5829691   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   schoolhistoryofu01hart_299   33   0.58233327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   sanfranciscodire1867lang_760   34   0.58210737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   popularhistoryof02howirich_403   35   0.57929546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   cu31924018821656_239   36   0.5780492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   americanjournalo24ameruoft_281   37   0.57795024   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   sanfranciscobusi51922sanf_457   38   0.57671446   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   historyofuniteds00gord_350   39   0.57656014   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 10   rel2-8   historyofsanfran00will_100   40   0.57467204   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
