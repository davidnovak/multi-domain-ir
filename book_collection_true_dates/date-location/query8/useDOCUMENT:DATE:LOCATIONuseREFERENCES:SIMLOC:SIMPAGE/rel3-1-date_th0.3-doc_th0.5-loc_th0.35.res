###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 270, non-zero: 270, borderline: 267, overall act: 26.812, act diff: 23.812, ratio: 0.888
#   pulse 2: activated: 1044481, non-zero: 1044481, borderline: 1044472, overall act: 136528.537, act diff: 136501.726, ratio: 1.000
#   pulse 3: activated: 1044520, non-zero: 1044520, borderline: 1044506, overall act: 136536.615, act diff: 8.077, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1044520
#   final overall activation: 136536.6
#   number of spread. activ. pulses: 3
#   running time: 230662

###################################
# top k results in TREC format: 

8   rel3-1   england   1   0.8943419   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   spain   2   0.67154235   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   holland   3   0.650901   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   1651   4   0.6400826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   europe   5   0.60270184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   france   6   0.5311612   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   portugal   7   0.34670088   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   1653   8   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   1654   9   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   calais   10   0.29025304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   great britain   11   0.28192666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   scotland   12   0.2602472   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   new world   13   0.2506827   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   newton blossomville   14   0.24149744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   filgrave   15   0.23852724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   hope college   16   0.23586139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   western theological seminary   17   0.23505872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   turvey   18   0.23300326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   sherington   19   0.23258282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel3-1   mediterranean   20   0.23253962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 8   rel3-1   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_237   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   england   4   0.8943419   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_236   5   0.7155972   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   spain   6   0.67154235   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   cu31924014592566_592   7   0.65983224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   holland   8   0.650901   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   1651   9   0.6400826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_238   10   0.62162423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_421   11   0.60866755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   europe   12   0.60270184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   cu31924014592566_594   13   0.6018288   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_235   14   0.59314907   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   france   15   0.5311612   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   encyclopdiabri07chisrich_523   16   0.48534462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   governmentalhist03sher_153   17   0.4795619   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_423   18   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   manualofinternat02ferg_476   19   0.4616664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   briefhistoryofna00fish_433   20   0.45688915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   encyclopaediabri12kell_89   21   0.45307535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   historyofcolonya00char_224   22   0.44939148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   historyforreadyr03larnuoft_770   23   0.4444738   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_390   24   0.44273657   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   governanceempir00silbgoog_125   25   0.44271168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   evolutionofstate00robeuoft_438   26   0.4401159   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   hallamsworks04halliala_45   27   0.43851304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_391   28   0.43565872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   irelandundercomm02dunluoft_123   29   0.43269202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   dial31unkngoog_221   30   0.42893922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_416   31   0.42856273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   scottishreview31paisuoft_310   32   0.427577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   historyofuniteds91banc_174   33   0.42672068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   americannationhi05hartuoft_33   34   0.42568347   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   lettersfromround00bann_45   35   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   cu31924032733879_89   36   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   worksthomascarl10traigoog_22   37   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   generalhistoryof04hawk_354   38   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   amongmybooks00loweiala_265   39   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel3-1   s3britishcritic01londuoft_50   40   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
