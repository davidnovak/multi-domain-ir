###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 32, non-zero: 32, borderline: 30, overall act: 6.871, act diff: 4.871, ratio: 0.709
#   pulse 2: activated: 5080, non-zero: 5080, borderline: 5077, overall act: 490.769, act diff: 483.897, ratio: 0.986
#   pulse 3: activated: 9522, non-zero: 9522, borderline: 9518, overall act: 891.002, act diff: 400.234, ratio: 0.449
#   pulse 4: activated: 13553, non-zero: 13553, borderline: 13548, overall act: 1507.956, act diff: 616.954, ratio: 0.409
#   pulse 5: activated: 17655, non-zero: 17655, borderline: 17649, overall act: 2284.254, act diff: 776.297, ratio: 0.340
#   pulse 6: activated: 17655, non-zero: 17655, borderline: 17649, overall act: 2633.154, act diff: 348.900, ratio: 0.133
#   pulse 7: activated: 17655, non-zero: 17655, borderline: 17649, overall act: 2859.303, act diff: 226.150, ratio: 0.079

###################################
# spreading activation process summary: 
#   final number of activated nodes: 17655
#   final overall activation: 2859.3
#   number of spread. activ. pulses: 7
#   running time: 34712

###################################
# top k results in TREC format: 

8   rel2-3   1653   1   0.6767918   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1652   2   0.63755083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1651   3   0.61646956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1654   4   0.5087901   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   england   5   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1650   6   0.2843657   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   spain   7   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1655   8   0.2490456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   europe   9   0.2440059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   holland   10   0.20519012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1627   11   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1595   12   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1625   13   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   1596   14   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   weymouth   15   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   calais   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   plymouth   17   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   portsmouth   18   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   boulogne   19   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-3   queensbury   20   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 8   rel2-3   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   1653   3   0.6767918   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   1652   4   0.63755083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   1651   5   0.61646956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   1654   6   0.5087901   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   registerofkentuc11kent_332   7   0.48650646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   memorialsofcambr02coopiala_466   8   0.47348702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   historyofguilfor00smitiala_31   9   0.46742752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   ageofmilton05mastuoft_109   10   0.4627328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   listofmanuscript00univ_538   11   0.43101972   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   cyclopdiaofuse02tomlrich_455   12   0.42485845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   registerofkentuc11kent_334   13   0.42186075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   cu31924032733879_104   14   0.421309   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   registerofkentuc11kent_328   15   0.41784975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   registerofkentuc11kent_320   16   0.41653264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   cu31924091770861_35   17   0.41541108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   historyofgardeni00ceci_354   18   0.41541106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   memorialsofcambr02coopiala_366   19   0.41049966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   philipthomashowa00palmuoft_137   20   0.40765697   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   ageofmilton05mastuoft_605   21   0.4052829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   archaeologiaaeli01sociuoft_44   22   0.4052829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   southafricanjou00sciegoog_383   23   0.4052829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   courtleetrecords04mancuoft_345   24   0.40445033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   briefhistoryofna00fish_433   25   0.4044503   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   revisedcharteror00taco_345   26   0.39621082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   causeofirelandpl00orei_407   27   0.39457703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   historyofcountyp00orme_254   28   0.39457703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   originsofbritish00beeruoft_434   29   0.39457703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   historyofolderno00palm_48   30   0.39014193   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   registerofkentuc11kent_324   31   0.38956714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   lifeofyoungsirhe00hosmiala_467   32   0.38500747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   earlyhistoryofen00leonrich_294   33   0.38500747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   historyoffir00thwi_88   34   0.38025674   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   listofmanuscript00univ_364   35   0.3789528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   thefirstdutchwar03navyuoft_326   36   0.37772602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   catalogueoralph00librgoog_208   37   0.37772602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   cu31924082457148_217   38   0.37772602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   miscellanyofscot19none_163   39   0.37772602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-3   generalhistoryof04hawk_98   40   0.37772602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
