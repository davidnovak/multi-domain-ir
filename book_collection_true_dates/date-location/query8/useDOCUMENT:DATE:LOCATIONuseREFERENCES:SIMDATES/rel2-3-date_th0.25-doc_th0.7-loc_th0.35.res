###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 32, non-zero: 32, borderline: 30, overall act: 6.871, act diff: 4.871, ratio: 0.709
#   pulse 2: activated: 5080, non-zero: 5080, borderline: 5077, overall act: 490.769, act diff: 483.897, ratio: 0.986
#   pulse 3: activated: 9522, non-zero: 9522, borderline: 9518, overall act: 891.002, act diff: 400.234, ratio: 0.449
#   pulse 4: activated: 13553, non-zero: 13553, borderline: 13548, overall act: 1507.956, act diff: 616.954, ratio: 0.409
#   pulse 5: activated: 22337, non-zero: 22337, borderline: 22330, overall act: 2649.648, act diff: 1141.692, ratio: 0.431
#   pulse 6: activated: 22337, non-zero: 22337, borderline: 22330, overall act: 3090.784, act diff: 441.136, ratio: 0.143
#   pulse 7: activated: 22337, non-zero: 22337, borderline: 22330, overall act: 3441.609, act diff: 350.825, ratio: 0.102
#   pulse 8: activated: 22337, non-zero: 22337, borderline: 22330, overall act: 3568.071, act diff: 126.462, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 22337
#   final overall activation: 3568.1
#   number of spread. activ. pulses: 8
#   running time: 36174

###################################
# top k results in TREC format: 

8   rel2-3   1651   1   0.71133053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1652   2   0.68978685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1653   3   0.6827375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1654   4   0.5308793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1650   5   0.3413902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   england   6   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   spain   7   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1655   8   0.2490456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   europe   9   0.2440059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   holland   10   0.20519012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1649   11   0.15818857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1627   12   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1595   13   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1625   14   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   1596   15   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   calais   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   portsmouth   17   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   boulogne   18   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   weymouth   19   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   portland   20   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 8   rel2-3   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1651   3   0.71133053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1652   4   0.68978685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1653   5   0.6827375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   memorialsofcambr02coopiala_466   6   0.5372192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1654   7   0.5308793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_332   8   0.5275535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   memorialsofcambr02coopiala_366   9   0.5159003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   ageofmilton05mastuoft_109   10   0.5024902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofguilfor00smitiala_31   11   0.5018738   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cyclopdiaofuse02tomlrich_455   12   0.49280366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   listofmanuscript00univ_538   13   0.4726145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_328   14   0.46568263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_320   15   0.45125747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_334   16   0.44949007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   selectessaysinan01asso_483   17   0.44940624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_415   18   0.449117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   dictionaryofmada01fitzuoft_80   19   0.44501203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerswadham01collgoog_149   20   0.44501203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_389   21   0.44390082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofolderno00palm_48   22   0.44287348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   briefhistoryofna00fish_433   23   0.442805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   courtleetrecords04mancuoft_345   24   0.44280496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   philipthomashowa00palmuoft_137   25   0.4381545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924032733879_104   26   0.43787602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   causeofirelandpl00orei_407   27   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofcountyp00orme_254   28   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_434   29   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   wadhamcollege00wellrich_87   30   0.43604442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924027990971_54   31   0.43604442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofgardeni00ceci_354   32   0.43236056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924091770861_35   33   0.4323605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   colonizationofno00boltrich_174   34   0.43036625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   archaeologiaaeli01sociuoft_44   35   0.42858633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   ageofmilton05mastuoft_605   36   0.42858633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   southafricanjou00sciegoog_383   37   0.42858633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   revisedcharteror00taco_345   38   0.4271401   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   dictionaryofmada01fitzuoft_89   39   0.42398664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   memoirofrichardb00bark_33   40   0.42398664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.35
