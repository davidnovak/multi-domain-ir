###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 258, non-zero: 258, borderline: 256, overall act: 22.578, act diff: 20.578, ratio: 0.911
#   pulse 2: activated: 980282, non-zero: 980282, borderline: 980278, overall act: 126683.342, act diff: 126660.764, ratio: 1.000
#   pulse 3: activated: 980315, non-zero: 980315, borderline: 980307, overall act: 126689.938, act diff: 6.596, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 980315
#   final overall activation: 126689.9
#   number of spread. activ. pulses: 3
#   running time: 198271

###################################
# top k results in TREC format: 

8   rel2-2   england   1   0.839055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   spain   2   0.66794777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   france   3   0.5061821   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   holland   4   0.4430103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   europe   5   0.40659708   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1651   6   0.40552422   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   portugal   7   0.34307736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   great britain   8   0.28192666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   new world   9   0.24682447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1652   10   0.21102692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1664   11   0.19095457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1660   12   0.19095457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1659   13   0.18280767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1598   14   0.18280767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   west indies   15   0.18216977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   calais   16   0.16596822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   jamaica   17   0.1658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1625   18   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1627   19   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
8   rel2-2   1595   20   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 8   rel2-2   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bookofhistoryhis11brycuoft_237   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   england   3   0.839055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   spain   4   0.66794777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924014592566_592   5   0.65983224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924014592566_594   6   0.6018288   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bookofhistoryhis11brycuoft_238   7   0.5860744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bookofhistoryhis11brycuoft_236   8   0.55945563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   france   9   0.5061821   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bookofhistoryhis11brycuoft_235   10   0.48061925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   holland   11   0.4430103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924014592566_591   12   0.4121168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   europe   13   0.40659708   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   1651   14   0.40552422   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924014592566_595   15   0.39016286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   ourmotherland00burr_12   16   0.36164108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   ourmotherland00burr_17   17   0.35163638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bibleinspainorjo01borruoft_167   18   0.34598017   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   portugal   19   0.34307736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   principlesofpoli03mill_141   20   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   progressofrussia00urqurich_119   21   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924013685866_157   22   0.33468983   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   historyofuniteds00form_36   23   0.33238575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   calendarmanuscr09ofgoog_243   24   0.33217588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   englandsfightwit00wals_203   25   0.33207282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   treasonandplots01humegoog_18   26   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   lawofnationscons12twis_851   27   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   principlespolit24millgoog_142   28   0.3307302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   elementseconomi01maclgoog_137   29   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   theoryofbalanceo02suviuoft_108   30   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   politicaleconomy04walk_96   31   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   studiesinstruct03carpgoog_218   32   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bibleinspainorjo01borruoft_291   33   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   historyofmodernc00duco_157   34   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bibleinspainorjo01borruoft_312   35   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   bibleinspainorjo01borruoft_377   36   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   cu31924092705114_424   37   0.32909417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   memoirsofjohnquis04adam_88   38   0.32672134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   selectionofcases01willuoft_364   39   0.32546344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel2-2   churchofengland03spenuoft_426   40   0.32526976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
