###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 258, non-zero: 258, borderline: 256, overall act: 22.578, act diff: 20.578, ratio: 0.911
#   pulse 2: activated: 980282, non-zero: 980282, borderline: 980278, overall act: 126683.342, act diff: 126660.764, ratio: 1.000
#   pulse 3: activated: 980302, non-zero: 980302, borderline: 980296, overall act: 126687.321, act diff: 3.979, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 980302
#   final overall activation: 126687.3
#   number of spread. activ. pulses: 3
#   running time: 205645

###################################
# top k results in TREC format: 

8   rel2-2   england   1   0.74898714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   spain   2   0.6040911   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   europe   3   0.31317624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   holland   4   0.30943748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   france   5   0.30217057   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   portugal   6   0.28192666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   great britain   7   0.28192666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1652   8   0.21102692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1664   9   0.19095457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1660   10   0.19095457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   west indies   11   0.18216977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   new world   12   0.18216977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   calais   13   0.16596822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   jamaica   14   0.1658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1595   15   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1651   16   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1625   17   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1627   18   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1596   19   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
8   rel2-2   1894   20   0.12887609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 8   rel2-2   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bookofhistoryhis11brycuoft_237   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   england   3   0.74898714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924014592566_592   4   0.65983224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   spain   5   0.6040911   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924014592566_594   6   0.6018288   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bookofhistoryhis11brycuoft_238   7   0.5860744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bookofhistoryhis11brycuoft_236   8   0.55945563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924014592566_591   9   0.4121168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924014592566_595   10   0.39016286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   ourmotherland00burr_12   11   0.36164108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   ourmotherland00burr_17   12   0.35163638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_167   13   0.34598017   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   principlesofpoli03mill_141   14   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   progressofrussia00urqurich_119   15   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924013685866_157   16   0.33468983   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   historyofuniteds00form_36   17   0.33238575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   calendarmanuscr09ofgoog_243   18   0.33217588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   englandsfightwit00wals_203   19   0.33207282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   treasonandplots01humegoog_18   20   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   lawofnationscons12twis_851   21   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   principlespolit24millgoog_142   22   0.3307302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_291   23   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   theoryofbalanceo02suviuoft_108   24   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   studiesinstruct03carpgoog_218   25   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   elementseconomi01maclgoog_137   26   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   politicaleconomy04walk_96   27   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   historyofmodernc00duco_157   28   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_312   29   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_377   30   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924092705114_424   31   0.32909417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   memoirsofjohnquis04adam_88   32   0.32672134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   selectionofcases01willuoft_364   33   0.32546344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   churchofengland03spenuoft_426   34   0.32526976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924024798245_33   35   0.32422626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   lifeofnapoleonii01roseuoft_564   36   0.32422626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   historyenglandf16gardgoog_258   37   0.32341406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   merchantadventu01linggoog_147   38   0.32330698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   hectorgermanieo00smitgoog_28   39   0.32330698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924067113880_168   40   0.32207662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
