###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 255, non-zero: 255, borderline: 253, overall act: 22.460, act diff: 20.460, ratio: 0.911
#   pulse 2: activated: 99729, non-zero: 99729, borderline: 99724, overall act: 9452.407, act diff: 9429.948, ratio: 0.998
#   pulse 3: activated: 99729, non-zero: 99729, borderline: 99724, overall act: 9452.407, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 99729
#   final overall activation: 9452.4
#   number of spread. activ. pulses: 3
#   running time: 68625

###################################
# top k results in TREC format: 

8   rel2-1   holland   1   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   1653   2   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   1654   3   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   england   4   0.27598518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   1651   5   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   europe   6   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   calais   7   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   spain   8   0.15861107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   portland   9   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   mediterranean   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   plymouth   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   portsmouth   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   boulogne   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   weymouth   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   denmark   15   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   queensbury   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   baltic   17   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   atlantic   18   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   north america   19   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-1   thames   20   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 8   rel2-1   bookofhistoryhis11brycuoft_237   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   bookofhistoryhis11brycuoft_236   3   0.51999086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   bookofhistoryhis11brycuoft_238   4   0.5049904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_421   5   0.50118303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   holland   6   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_423   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   livesofwarriorsw01cust_59   8   0.37468755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   landmarkhistoryo00ulmas_22   9   0.33073714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   burnetshistoryof01burnuoft_429   10   0.32674363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   treasuresofartin02waaguoft_327   11   0.32551044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   worksfrancisbaco03bacoiala_205   12   0.3076144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   pioneermothersof01gree_195   13   0.30685633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   1653   14   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   1654   15   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   250thanniversary00newyrich_117   16   0.2991831   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   cu31924028066797_380   17   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   englishheraldicb00daverich_402   18   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   thomasharrisonr00wessgoog_20   19   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   courtofkingsbench08greaiala_290   20   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   aschoolhistoryu01macegoog_109   21   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_430   22   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   kinginexilewande00scotuoft_420   23   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   lifeofyoungsirhe00hosmiala_433   24   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   augustanages00eltouoft_242   25   0.29197112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   lettersspeecheso02cromuoft_338   26   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   evolutionofstate00robeuoft_331   27   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   cu31924067742332_16   28   0.29155213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   historyofstateof00broduoft_613   29   0.29082623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   historyforreadyr03larnuoft_770   30   0.28895515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   registerofvisito00camduoft_113   31   0.28755295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   england   32   0.27598518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   cu31924028485070_293   33   0.27375957   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   historyofnewyork00pren_93   34   0.27134305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   burnetshistoryof00burnuoft_175   35   0.26919344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   kinginexilewande00scotuoft_444   36   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   carlylecompletew08carl_313   37   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   lettersfromround00bann_79   38   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   worksthomascarl10traigoog_114   39   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-1   cu31924027975733_471   40   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
