###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 129736, non-zero: 129736, borderline: 129730, overall act: 12002.845, act diff: 11997.443, ratio: 1.000
#   pulse 3: activated: 129736, non-zero: 129736, borderline: 129730, overall act: 12002.845, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 129736
#   final overall activation: 12002.8
#   number of spread. activ. pulses: 3
#   running time: 35813

###################################
# top k results in TREC format: 

7   rel2-3   1830   1   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1863   2   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1831   3   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-3   1825   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924031684685_794   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924031684685_796   4   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   poland   5   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1830   6   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1831   7   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1863   8   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   1825   9   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_358   10   0.29542425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historicmorgancl00eame_250   11   0.2686337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   americanstatepap06unit_14   12   0.26821375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_468   13   0.2655389   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   vanburenmartin00sheprich_317   14   0.2648886   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofcuyahog00john_646   15   0.25795445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   honoursregistero00univuoft_158   16   0.25795445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historylasalleco00bald_94   17   0.25571498   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   teachingofmodern00handuoft_37   18   0.25515556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historylasalleco00bald_397   19   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   canadianmagazine37torouoft_184   20   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   woolgrowingtari05wriguoft_70   21   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   congressionalse175offigoog_304   22   0.2509268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyoffayette00barr_276   23   0.24660507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   remakingofmodern06marruoft_185   24   0.24530816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_85   25   0.23559643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cataloguescient00unkngoog_834   26   0.23484221   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   ahistorymodernb03conagoog_409   27   0.23398842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   tribuneofnovasc26gran_116   28   0.23349519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   worksofhuberthow19bancrich_676   29   0.23349519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   documentsassemb56assegoog_537   30   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   worthiesworkersb00fiel_97   31   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   ourhomerailways00unkngoog_37   32   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   americanstatesmen18morsrich_315   33   0.23271771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historylasalleco00bald_294   34   0.23251869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   cu31924014512614_19   35   0.23164365   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   woolgrowingtari05wriguoft_73   36   0.23071484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   memorialsofcambr02coopiala_386   37   0.22923031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   historyofmodernb00conauoft_368   38   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   chroniclesofgree00browrich_155   39   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-3   listofresidents2195116_646   40   0.22921596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
