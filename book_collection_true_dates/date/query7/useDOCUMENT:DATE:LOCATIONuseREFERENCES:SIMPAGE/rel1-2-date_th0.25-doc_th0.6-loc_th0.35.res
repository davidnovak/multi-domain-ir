###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 12, overall act: 3.739, act diff: 2.739, ratio: 0.733
#   pulse 2: activated: 36007, non-zero: 36007, borderline: 36005, overall act: 2590.297, act diff: 2586.557, ratio: 0.999
#   pulse 3: activated: 36007, non-zero: 36007, borderline: 36005, overall act: 2590.297, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 36007
#   final overall activation: 2590.3
#   number of spread. activ. pulses: 3
#   running time: 15766

###################################
# top k results in TREC format: 

7   rel1-2   1830   1   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel1-2   1863   2   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel1-2   1831   3   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel1-2   1825   4   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel1-2   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   cu31924031684685_794   2   0.5008992   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   cu31924031684685_796   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   poland   4   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   europe   5   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   curiositiesfori00unkngoog_100   6   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   onrightwrong00lill_339   7   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   hilltop03sout_846   8   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   hilltop03sout_848   9   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   naturalistscabin02smituoft_237   10   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   songshistoryand00libbgoog_215   11   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_674   12   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_673   13   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_675   14   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   secretmemoirsco01orlgoog_336   15   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   reubenmedlicotto03sava_93   16   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   bluecraneshoreso00swifrich_43   17   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   britishforeignm34londuoft_95   18   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   britishforeignm34londuoft_94   19   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   worksofhonorde32balzuoft_196   20   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   biennialreportofsec1918mont_13   21   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   americanvocalist00mans_67   22   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_668   23   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   worksofhonorde32balzuoft_209   24   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   cu31924024898896_116   25   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   politicalscienc01walsgoog_340   26   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   treatiseonpoliti00destuoft_268   27   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   annualreportofst1893ohio_210   28   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   correspondencer01bourgoog_142   29   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   henryfourth00abboiala_176   30   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   vanityofhumangra00russ_47   31   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   russianpolitica02kovagoog_75   32   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   elliottspoems01elliiala_101   33   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   hilltop03sout_948   34   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   myexileinsiberi00gertgoog_253   35   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   infootprintsofhe00formiala_130   36   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   hilltop03sout_920   37   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   hilltop03sout_916   38   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   whoareslavsacon00radogoog_193   39   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel1-2   americanjournalo19chicuoft_646   40   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
