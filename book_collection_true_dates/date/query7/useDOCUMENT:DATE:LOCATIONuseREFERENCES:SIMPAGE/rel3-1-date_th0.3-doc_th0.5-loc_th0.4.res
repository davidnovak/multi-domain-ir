###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 164536, non-zero: 164536, borderline: 164528, overall act: 21208.967, act diff: 21199.951, ratio: 1.000
#   pulse 3: activated: 164552, non-zero: 164552, borderline: 164533, overall act: 21216.682, act diff: 7.716, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164552
#   final overall activation: 21216.7
#   number of spread. activ. pulses: 3
#   running time: 61455

###################################
# top k results in TREC format: 

7   rel3-1   1863   1   0.9854722   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
7   rel3-1   1831   2   0.8445021   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
7   rel3-1   1825   3   0.5709993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
7   rel3-1   1830   4   0.482382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   poland   4   0.9950559   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   1863   5   0.9854722   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   1831   6   0.8445021   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russia   7   0.7252485   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924088053800_312   8   0.58559424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   1825   9   0.5709993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   10   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_164   11   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_150   12   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   13   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   14   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   15   0.53849673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_419   16   0.5383418   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924031684685_794   17   0.527213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_165   18   0.52168906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_149   19   0.52168906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_43   20   0.5216104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   21   0.51312554   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_163   22   0.50951713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   23   0.5075743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   24   0.5030429   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   someproblemspea00goog_191   25   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_388   26   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_428   27   0.49696192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   28   0.49603966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   29   0.49158984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   30   0.49135894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   31   0.48760217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_398   32   0.4872372   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   33   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   sketchesinpoland00littrich_57   34   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_410   35   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   1830   36   0.482382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   37   0.47936845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   38   0.47890648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_193   39   0.47738993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   40   0.47715482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
