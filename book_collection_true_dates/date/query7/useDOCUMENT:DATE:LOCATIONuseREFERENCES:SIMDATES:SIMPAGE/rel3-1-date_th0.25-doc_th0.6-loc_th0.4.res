###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 164542, non-zero: 164542, borderline: 164534, overall act: 21210.660, act diff: 21201.644, ratio: 1.000
#   pulse 3: activated: 263732, non-zero: 263732, borderline: 263721, overall act: 33658.494, act diff: 12447.835, ratio: 0.370
#   pulse 4: activated: 281990, non-zero: 281990, borderline: 281977, overall act: 38837.027, act diff: 5178.533, ratio: 0.133
#   pulse 5: activated: 281992, non-zero: 281992, borderline: 281978, overall act: 44190.412, act diff: 5353.385, ratio: 0.121
#   pulse 6: activated: 282060, non-zero: 282060, borderline: 282022, overall act: 46657.432, act diff: 2467.020, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 282060
#   final overall activation: 46657.4
#   number of spread. activ. pulses: 6
#   running time: 131391

###################################
# top k results in TREC format: 

7   rel3-1   1831   1   0.99979764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1863   2   0.9993199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1830   3   0.9983085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1832   4   0.7272108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1825   5   0.5116361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1829   6   0.45821068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1862   7   0.41913325   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1833   8   0.38876504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1864   9   0.36818755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1824   10   0.2603948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1826   11   0.2603948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1861   12   0.23352426   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1828   13   0.18687606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1905   14   0.18289007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1915   15   0.1789162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1865   16   0.17575455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1842   17   0.115387596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1819   18   0.09530731   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1838   19   0.060242254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1834   20   0.060242254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   poland   4   0.9999967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1831   5   0.99979764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1863   6   0.9993199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1830   7   0.9983085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russia   8   0.94927263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   9   0.8603009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   europe   10   0.7755873   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_82   11   0.7653859   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1832   12   0.7272108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   13   0.72024107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   lectureonsocialp00tochrich_21   14   0.7185999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   15   0.6892226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   englishwomaninr00unkngoog_305   16   0.6876672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   17   0.67824346   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   18   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   19   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_164   20   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_150   21   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   22   0.6752228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   23   0.6737328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   24   0.6724221   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   25   0.6696698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_43   26   0.6626672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   27   0.657513   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   historicmorgancl00eame_250   28   0.6564837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   29   0.648652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_419   30   0.6480374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   31   0.6466843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_127   32   0.6466843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_151   33   0.6426174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   34   0.6424725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   stormssunshineof01mackuoft_43   35   0.6401767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_165   36   0.63897604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_149   37   0.63897604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   lectureonsocialp00tochrich_22   38   0.63796467   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   historytenyears06blangoog_465   39   0.6377053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_245   40   0.63735235   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
