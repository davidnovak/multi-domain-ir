###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164534, non-zero: 164534, borderline: 164526, overall act: 21207.185, act diff: 21200.018, ratio: 1.000
#   pulse 3: activated: 164537, non-zero: 164537, borderline: 164520, overall act: 21208.896, act diff: 1.711, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164537
#   final overall activation: 21208.9
#   number of spread. activ. pulses: 3
#   running time: 41683

###################################
# top k results in TREC format: 

7   rel3-1   1863   1   0.9854722   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1831   2   0.8445021   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1830   3   0.482382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1825   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   poland   4   0.9942749   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   1863   5   0.9854722   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   1831   6   0.8445021   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   russia   7   0.54928106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   8   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   9   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_164   10   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_150   11   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   12   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   13   0.53849673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   14   0.51312554   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   15   0.5075743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   16   0.5030429   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   someproblemspea00goog_191   17   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_388   18   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_428   19   0.49696192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   20   0.49603966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   21   0.49158984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   22   0.49135894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   23   0.48760217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_398   24   0.4872372   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   sketchesinpoland00littrich_57   25   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   26   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_410   27   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   1830   28   0.482382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   29   0.47936845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   30   0.47890648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_193   31   0.47738993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   32   0.47715482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   withworldspeople05ridp_189   33   0.47465578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   alhambrakremlin00prim_311   34   0.47405842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   stormssunshineof01mackuoft_43   35   0.46942425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924028567273_442   36   0.4685645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   cu31924073899001_127   37   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   sketchesinpoland00littrich_35   38   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_406   39   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   40   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.4
