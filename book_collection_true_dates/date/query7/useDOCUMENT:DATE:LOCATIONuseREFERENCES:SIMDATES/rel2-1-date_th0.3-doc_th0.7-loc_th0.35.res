###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 569157, non-zero: 569157, borderline: 569150, overall act: 55801.415, act diff: 55795.837, ratio: 1.000
#   pulse 3: activated: 593341, non-zero: 593341, borderline: 593333, overall act: 58881.339, act diff: 3079.924, ratio: 0.052
#   pulse 4: activated: 593341, non-zero: 593341, borderline: 593333, overall act: 59897.618, act diff: 1016.280, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 593341
#   final overall activation: 59897.6
#   number of spread. activ. pulses: 4
#   running time: 79354

###################################
# top k results in TREC format: 

7   rel2-1   1831   1   0.5956452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1863   2   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1830   3   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1832   4   0.28931874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1862   5   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1864   6   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1829   7   0.19044791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   1825   8   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   1831   4   0.5956452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   5   0.5852942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   6   0.57981354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   7   0.5764377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   8   0.5646878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   9   0.562637   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   americanhistoric19151916jame_633   10   0.5552618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaasitis00gurouoft_44   11   0.5503273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   12   0.54607207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russianpolitica02kovagoog_288   13   0.54453236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   14   0.54430574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   15   0.54224366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   anthologyofmoder00selviala_126   16   0.54209924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   peacehandbooks08grea_81   17   0.5381644   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   18   0.5363658   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   19   0.53355694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   20   0.5321019   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   historytenyears06blangoog_465   21   0.53151995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   22   0.52919054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924073899001_127   23   0.5277455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   24   0.5274982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   25   0.52340466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   26   0.51755995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   27   0.51458126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   spiritrussiastu00masagoog_252   28   0.514145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   englandduringthi00mart_819   29   0.51297843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   outlineseuropea04beargoog_503   30   0.5088632   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   expansionofruss00skri_211   31   0.50883085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_41   32   0.50748104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_303   33   0.50532037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   lectureonsocialp00tochrich_22   34   0.505139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_390   35   0.49981302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   foundingofgerman02sybeuoft_555   36   0.49520865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   37   0.49499035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_37   38   0.49340117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   greatrussiaherac00sarouoft_160   39   0.4927133   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_241   40   0.49171382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
