###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164540, non-zero: 164540, borderline: 164532, overall act: 21208.878, act diff: 21201.711, ratio: 1.000
#   pulse 3: activated: 263730, non-zero: 263730, borderline: 263719, overall act: 33656.713, act diff: 12447.835, ratio: 0.370
#   pulse 4: activated: 281988, non-zero: 281988, borderline: 281975, overall act: 38834.714, act diff: 5178.001, ratio: 0.133
#   pulse 5: activated: 281988, non-zero: 281988, borderline: 281974, overall act: 44187.512, act diff: 5352.798, ratio: 0.121
#   pulse 6: activated: 282043, non-zero: 282043, borderline: 282006, overall act: 46642.359, act diff: 2454.848, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 282043
#   final overall activation: 46642.4
#   number of spread. activ. pulses: 6
#   running time: 129009

###################################
# top k results in TREC format: 

7   rel3-1   1831   1   0.99979764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1863   2   0.9993199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1830   3   0.9983085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1832   4   0.7272108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1825   5   0.5116361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1829   6   0.45821068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1862   7   0.41913325   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1833   8   0.38876504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1864   9   0.36818755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1824   10   0.2603948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1826   11   0.2603948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1861   12   0.23352426   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1828   13   0.18687606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1905   14   0.18289007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1915   15   0.1789162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1865   16   0.17575455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1842   17   0.115387596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1819   18   0.09530731   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1838   19   0.060242254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
7   rel3-1   1834   20   0.060242254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   poland   4   0.99999404   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1831   5   0.99979764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1863   6   0.9993199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1830   7   0.9983085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russia   8   0.94927263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   europe   9   0.7755873   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   10   0.7577261   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   1832   11   0.7272108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   12   0.72024107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   13   0.6892226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   englishwomaninr00unkngoog_305   14   0.6876672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   15   0.67824346   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   16   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   17   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_164   18   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_150   19   0.6766468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   20   0.6752228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   21   0.6737328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   22   0.6724221   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   23   0.6696698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   24   0.657513   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   historicmorgancl00eame_250   25   0.6564837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   26   0.648652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   27   0.6466843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_127   28   0.6466843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_151   29   0.6426174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   30   0.6424725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   stormssunshineof01mackuoft_43   31   0.6401767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   lectureonsocialp00tochrich_22   32   0.63796467   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   historytenyears06blangoog_465   33   0.6377053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_245   34   0.63735235   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   cu31924073899001_33   35   0.6364296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   36   0.6360483   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   someproblemspea00goog_191   37   0.63360316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_388   38   0.63360316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   austriaviennapr00kohlgoog_511   39   0.6335351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel3-1   conquestsofcross02hodd_183   40   0.63336635   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
