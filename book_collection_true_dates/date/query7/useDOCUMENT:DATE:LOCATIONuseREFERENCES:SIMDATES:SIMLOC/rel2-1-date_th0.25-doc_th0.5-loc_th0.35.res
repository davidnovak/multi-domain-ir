###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 569205, non-zero: 569205, borderline: 569198, overall act: 55811.414, act diff: 55805.836, ratio: 1.000
#   pulse 3: activated: 593409, non-zero: 593409, borderline: 593386, overall act: 58895.883, act diff: 3084.470, ratio: 0.052
#   pulse 4: activated: 758456, non-zero: 758456, borderline: 758425, overall act: 136016.797, act diff: 77120.913, ratio: 0.567
#   pulse 5: activated: 918396, non-zero: 918396, borderline: 914362, overall act: 181113.360, act diff: 45096.563, ratio: 0.249
#   pulse 6: activated: 7434763, non-zero: 7434763, borderline: 7427235, overall act: 3859046.303, act diff: 3677932.943, ratio: 0.953
#   pulse 7: activated: 8312250, non-zero: 8312250, borderline: 4930966, overall act: 4649894.565, act diff: 790848.263, ratio: 0.170
#   pulse 8: activated: 9642620, non-zero: 9642620, borderline: 5299359, overall act: 6552912.005, act diff: 1903017.441, ratio: 0.290
#   pulse 9: activated: 9707982, non-zero: 9707982, borderline: 2874417, overall act: 6698456.832, act diff: 145544.827, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9707982
#   final overall activation: 6698456.8
#   number of spread. activ. pulses: 9
#   running time: 2088495

###################################
# top k results in TREC format: 

7   rel2-1   709   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   774   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   750   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   725   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1010   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1011   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1000   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   958   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   775   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1022   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1009   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1001   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1002   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1003   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1004   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1005   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1006   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1007   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   1008   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   625   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   new lenox   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   victoria bridge   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   haverstraw bay   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   milton of balgonie   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   terra firma   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   betsey   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   south oxford   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   nederland   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   bapaume   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   bettws   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   holy land   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   pangbourne   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   porthleven   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   rataplan   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   warboys   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   montreux   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   ecorse   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   montreat   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   montreal   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   morro bay   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   city of boston   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   brocket hall   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   merrymeeting bay   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   edmund street   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   new castle county   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   bidford   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   lidiana   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   bethel   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   betham   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   warburg   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   santa luzia   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   hawthornden   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   north kensington   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   shillong   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   calatayud   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   sandpoint   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   mchenry county   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   embarcadero   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   point barrow   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   betten   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
