###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 10, overall act: 2.815, act diff: 1.815, ratio: 0.645
#   pulse 2: activated: 36027, non-zero: 36027, borderline: 36025, overall act: 2593.057, act diff: 2590.242, ratio: 0.999
#   pulse 3: activated: 36027, non-zero: 36027, borderline: 36025, overall act: 2593.057, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 36027
#   final overall activation: 2593.1
#   number of spread. activ. pulses: 3
#   running time: 15963

###################################
# top k results in TREC format: 

7   rel1-2   1830   1   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel1-2   1863   2   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel1-2   1831   3   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
7   rel1-2   1825   4   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel1-2   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   poland   2   0.36212304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   europe   3   0.21094358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   curiositiesfori00unkngoog_100   4   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   onrightwrong00lill_339   5   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   hilltop03sout_846   6   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   hilltop03sout_848   7   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   naturalistscabin02smituoft_237   8   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   songshistoryand00libbgoog_215   9   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_674   10   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_673   11   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_675   12   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   secretmemoirsco01orlgoog_336   13   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   reubenmedlicotto03sava_93   14   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   bluecraneshoreso00swifrich_43   15   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   britishforeignm34londuoft_95   16   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   britishforeignm34londuoft_94   17   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   worksofhonorde32balzuoft_196   18   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   biennialreportofsec1918mont_13   19   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   americanvocalist00mans_67   20   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   principlespracti18861fagg_668   21   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   worksofhonorde32balzuoft_209   22   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   cu31924024898896_116   23   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   politicalscienc01walsgoog_340   24   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   treatiseonpoliti00destuoft_268   25   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   annualreportofst1893ohio_210   26   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   correspondencer01bourgoog_142   27   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   henryfourth00abboiala_176   28   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   vanityofhumangra00russ_47   29   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   russianpolitica02kovagoog_75   30   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   elliottspoems01elliiala_101   31   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   hilltop03sout_948   32   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   myexileinsiberi00gertgoog_253   33   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   infootprintsofhe00formiala_130   34   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   hilltop03sout_920   35   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   hilltop03sout_916   36   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   whoareslavsacon00radogoog_193   37   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   americanjournalo19chicuoft_646   38   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   retrospectionorr00piozuoft_222   39   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel1-2   reubenmedlicotto03sava_45   40   0.17910853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
