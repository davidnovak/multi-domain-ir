###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 5.578, act diff: 3.578, ratio: 0.641
#   pulse 2: activated: 115009, non-zero: 115009, borderline: 115004, overall act: 13596.509, act diff: 13590.931, ratio: 1.000
#   pulse 3: activated: 141788, non-zero: 141788, borderline: 141782, overall act: 16681.971, act diff: 3085.462, ratio: 0.185
#   pulse 4: activated: 141788, non-zero: 141788, borderline: 141782, overall act: 17700.486, act diff: 1018.515, ratio: 0.058
#   pulse 5: activated: 141788, non-zero: 141788, borderline: 141782, overall act: 18151.890, act diff: 451.404, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 141788
#   final overall activation: 18151.9
#   number of spread. activ. pulses: 5
#   running time: 42698

###################################
# top k results in TREC format: 

7   rel2-1   1831   1   0.6138376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1863   2   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1830   3   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1832   4   0.28931874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1862   5   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1864   6   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1829   7   0.21793541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
7   rel2-1   1825   8   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   1831   4   0.6138376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaasitis00gurouoft_44   5   0.56281877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_126   6   0.54209924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_43   7   0.49963737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   foundingofgerman02sybeuoft_555   8   0.49520865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   polishpeasantine01thomuoft_216   9   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_420   10   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_164   11   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_150   12   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_231   13   0.49123964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   englishwomaninr00unkngoog_305   14   0.48598373   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   peacehandbooks08grea_81   15   0.48464677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   americanhistoric19151916jame_633   16   0.4732734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   stormssunshineof01mackuoft_43   17   0.47196594   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   russianpolitica02kovagoog_288   18   0.47032177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_127   19   0.46912938   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   bookofmemoriesof00halluoft_375   20   0.46912938   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   polishexperienc02hallgoog_20   21   0.46575403   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_151   22   0.4652754   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   secretsocieties01heckgoog_192   23   0.46403852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_245   24   0.4603108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_426   25   0.45812804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   1863   26   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   conquestsofcross02hodd_183   27   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   recoverypoland00krycgoog_229   28   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   operationsofsurg1902jaco_633   29   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_121   30   0.4565704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   advocateofpeace82amerrich_327   31   0.45556256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   someproblemspea00goog_191   32   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_388   33   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_428   34   0.44684103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   1830   35   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_33   36   0.4426476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   historytenyears06blangoog_465   37   0.43767935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_398   38   0.43651208   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   lectureonsocialp00tochrich_22   39   0.43649217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 7   rel2-1   sketchesinpoland00littrich_57   40   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
