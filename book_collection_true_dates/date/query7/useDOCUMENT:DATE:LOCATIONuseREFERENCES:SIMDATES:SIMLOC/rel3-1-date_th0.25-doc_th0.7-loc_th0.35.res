###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 614229, non-zero: 614229, borderline: 614219, overall act: 63417.099, act diff: 63409.932, ratio: 1.000
#   pulse 3: activated: 706998, non-zero: 706998, borderline: 706985, overall act: 75848.253, act diff: 12431.154, ratio: 0.164
#   pulse 4: activated: 723949, non-zero: 723949, borderline: 723935, overall act: 81015.420, act diff: 5167.167, ratio: 0.064
#   pulse 5: activated: 723949, non-zero: 723949, borderline: 723935, overall act: 84309.491, act diff: 3294.071, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 723949
#   final overall activation: 84309.5
#   number of spread. activ. pulses: 5
#   running time: 139837

###################################
# top k results in TREC format: 

7   rel3-1   1831   1   0.83928984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1863   2   0.7726496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1830   3   0.72108155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1832   4   0.38597444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1862   5   0.355197   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1864   6   0.355197   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1829   7   0.34259927   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1825   8   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1833   9   0.18780367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1861   10   0.17575455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1865   11   0.17575455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1824   12   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1826   13   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1828   14   0.14558423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1831   4   0.83928984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1863   5   0.7726496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1830   6   0.72108155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   7   0.66644925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   8   0.6636629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   9   0.66141206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   10   0.65919673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   11   0.65451604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   12   0.6541654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   13   0.65254396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   14   0.64775455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   15   0.6438969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   16   0.63849205   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   17   0.6370009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   18   0.63399875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   19   0.6327096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   20   0.63028944   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   21   0.62791175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   22   0.6266794   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   23   0.6229431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   24   0.6227771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   25   0.61930335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   poland   26   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   lectureonsocialp00tochrich_22   27   0.61716586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americancatholic30philuoft_567   28   0.6154161   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   libraryofhistori12weit_41   29   0.6149752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   30   0.61477816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   31   0.6146509   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   32   0.61433697   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   33   0.6118029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cataloguescient00unkngoog_358   34   0.6099386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   35   0.60693294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_127   36   0.60198444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   greatrussiaherac00sarouoft_160   37   0.60002214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_390   38   0.59920985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   historicmorgancl00eame_250   39   0.59892446   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   40   0.5950868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.35
