###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164561, non-zero: 164561, borderline: 164553, overall act: 21215.031, act diff: 21207.864, ratio: 1.000
#   pulse 3: activated: 263754, non-zero: 263754, borderline: 263734, overall act: 33664.255, act diff: 12449.224, ratio: 0.370
#   pulse 4: activated: 391505, non-zero: 391505, borderline: 391472, overall act: 60582.674, act diff: 26918.419, ratio: 0.444
#   pulse 5: activated: 951567, non-zero: 951567, borderline: 950861, overall act: 154650.577, act diff: 94067.903, ratio: 0.608
#   pulse 6: activated: 6342768, non-zero: 6342768, borderline: 6337986, overall act: 2620103.468, act diff: 2465452.890, ratio: 0.941
#   pulse 7: activated: 8102535, non-zero: 8102535, borderline: 6280078, overall act: 4488877.428, act diff: 1868773.960, ratio: 0.416
#   pulse 8: activated: 9575883, non-zero: 9575883, borderline: 5426684, overall act: 6446762.402, act diff: 1957884.975, ratio: 0.304
#   pulse 9: activated: 9688036, non-zero: 9688036, borderline: 2997267, overall act: 6655415.904, act diff: 208653.502, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9688036
#   final overall activation: 6655415.9
#   number of spread. activ. pulses: 9
#   running time: 2001373

###################################
# top k results in TREC format: 

7   rel3-1   709   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   774   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   750   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   725   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1010   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1011   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1000   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   958   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   775   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1022   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1009   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1001   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1002   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1003   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1004   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1005   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1006   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1007   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   1008   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
7   rel3-1   625   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   point barrow   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   bettws   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   holy land   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   pangbourne   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   porthleven   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   rataplan   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   warboys   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   thurcaston   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   lincoln square   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   morro bay   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   city of boston   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   brocket hall   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   merrymeeting bay   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   edmund street   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   new castle county   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   bidford   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   lidiana   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   bethel   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   betham   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   warburg   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   hawthornden   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   north kensington   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   shillong   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   calatayud   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   sandpoint   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   mchenry county   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   embarcadero   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel3-1   new lenox   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
