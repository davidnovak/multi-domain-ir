###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 614229, non-zero: 614229, borderline: 614219, overall act: 63418.718, act diff: 63409.703, ratio: 1.000
#   pulse 3: activated: 614237, non-zero: 614237, borderline: 614224, overall act: 65937.903, act diff: 2519.185, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614237
#   final overall activation: 65937.9
#   number of spread. activ. pulses: 3
#   running time: 106174

###################################
# top k results in TREC format: 

7   rel3-1   1863   1   0.7509751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1831   2   0.7430492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1830   3   0.6042413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1825   4   0.59416175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1832   5   0.33068183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1862   6   0.2849298   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1864   7   0.2849298   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1829   8   0.27806276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1824   9   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   1826   10   0.16928883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   poland   4   0.76191187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1863   5   0.7509751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russia   6   0.74357826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1831   7   0.7430492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924088053800_312   8   0.6557341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924031684685_794   9   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   10   0.61458045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   11   0.6136545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   12   0.61208916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   13   0.6105634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   14   0.60939145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   15   0.6083302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   16   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   17   0.6048824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1830   18   0.6042413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1825   19   0.59416175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   20   0.5931126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   21   0.59130925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   22   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   23   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   24   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924031684685_796   25   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   26   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   27   0.5774338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   28   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924088053800_314   29   0.5750714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   30   0.57375264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   31   0.57361543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   32   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   lectureonsocialp00tochrich_22   33   0.5709702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   34   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   35   0.5684508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   36   0.56320757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   37   0.56042904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924073899001_127   38   0.5599677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   39   0.55509245   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   greatrussiaherac00sarouoft_160   40   0.5499332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
