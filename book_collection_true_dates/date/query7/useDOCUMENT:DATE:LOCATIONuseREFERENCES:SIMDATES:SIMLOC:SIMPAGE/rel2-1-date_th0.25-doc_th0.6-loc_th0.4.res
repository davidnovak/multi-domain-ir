###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 115012, non-zero: 115012, borderline: 115007, overall act: 13598.337, act diff: 13590.910, ratio: 0.999
#   pulse 3: activated: 141791, non-zero: 141791, borderline: 141785, overall act: 16683.798, act diff: 3085.462, ratio: 0.185
#   pulse 4: activated: 141791, non-zero: 141791, borderline: 141785, overall act: 17702.313, act diff: 1018.515, ratio: 0.058
#   pulse 5: activated: 162809, non-zero: 162809, borderline: 162802, overall act: 20135.002, act diff: 2432.689, ratio: 0.121
#   pulse 6: activated: 162809, non-zero: 162809, borderline: 162802, overall act: 20859.965, act diff: 724.963, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 162809
#   final overall activation: 20860.0
#   number of spread. activ. pulses: 6
#   running time: 51952

###################################
# top k results in TREC format: 

7   rel2-1   1831   1   0.69611096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1830   2   0.48243958   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1863   3   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1832   4   0.33464992   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1862   5   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1864   6   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1829   7   0.21793541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1825   8   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   1833   9   0.14365868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   1831   3   0.69611096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   poland   4   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaasitis00gurouoft_44   5   0.58408636   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_126   6   0.57660156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_43   7   0.53637725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   foundingofgerman02sybeuoft_555   8   0.53217226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_231   9   0.5284022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924031684685_794   10   0.527213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_127   11   0.50737345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   bookofmemoriesof00halluoft_375   12   0.50737345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_151   13   0.50370336   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_245   14   0.49897355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   stormssunshineof01mackuoft_43   15   0.49818268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   recoverypoland00krycgoog_229   16   0.4954085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   conquestsofcross02hodd_183   17   0.4954085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   operationsofsurg1902jaco_633   18   0.4954085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_121   19   0.4954085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_164   20   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   polishpeasantine01thomuoft_216   21   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_420   22   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_150   23   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   englishwomaninr00unkngoog_305   24   0.48598373   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   polishexperienc02hallgoog_20   25   0.4851906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   peacehandbooks08grea_81   26   0.48464677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   1830   27   0.48243958   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   advocateofpeace82amerrich_327   28   0.47523716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   americanhistoric19151916jame_633   29   0.4732734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   historytenyears06blangoog_439   30   0.47108644   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russianpolitica02kovagoog_288   31   0.47032177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   historyofourownt03mccaiala_238   32   0.46424267   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   revelationssibe02lachgoog_20   33   0.46424267   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   secretsocieties01heckgoog_192   34   0.46403852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   historytenyears06blangoog_465   35   0.46294263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_314   36   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924031684685_796   37   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_312   38   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   englandduringthi00mart_819   39   0.46164268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   conversationswit01seniuoft_271   40   0.4597456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
