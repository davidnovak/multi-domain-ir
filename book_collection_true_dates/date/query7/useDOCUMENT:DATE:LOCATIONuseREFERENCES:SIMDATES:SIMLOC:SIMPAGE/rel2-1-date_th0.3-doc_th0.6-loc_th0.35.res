###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 569205, non-zero: 569205, borderline: 569198, overall act: 55813.088, act diff: 55805.662, ratio: 1.000
#   pulse 3: activated: 593396, non-zero: 593396, borderline: 593387, overall act: 58894.143, act diff: 3081.055, ratio: 0.052
#   pulse 4: activated: 593396, non-zero: 593396, borderline: 593387, overall act: 69086.969, act diff: 10192.826, ratio: 0.148
#   pulse 5: activated: 593413, non-zero: 593413, borderline: 593398, overall act: 69543.085, act diff: 456.115, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 593413
#   final overall activation: 69543.1
#   number of spread. activ. pulses: 5
#   running time: 136689

###################################
# top k results in TREC format: 

7   rel2-1   1831   1   0.9031246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1863   2   0.7415996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1830   3   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1832   4   0.28931874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1862   5   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1864   6   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1829   7   0.21793541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1825   8   0.17624399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
7   rel2-1   1772   9   0.15302037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   poland   3   0.93853533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russia   4   0.9369229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   1831   5   0.9031246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   1863   6   0.7415996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   europe   7   0.70370543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924031684685_794   8   0.64228266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   9   0.6221896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   10   0.6170612   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   11   0.6169271   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   12   0.6124486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924031684685_796   13   0.6064928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924088053800_314   14   0.601972   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   americanhistoric19151916jame_633   15   0.6008896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   16   0.5978802   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russianpolitica02kovagoog_288   17   0.58888644   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   18   0.5870129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   peacehandbooks08grea_81   19   0.58344626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   20   0.5834111   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_409   21   0.5801643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russiaasitis00gurouoft_44   22   0.57976913   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   23   0.57934356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   24   0.5774417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   historytenyears06blangoog_465   25   0.5745585   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   26   0.57236725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924088053800_312   27   0.5715141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_42   28   0.5686744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   29   0.5672249   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   30   0.5645108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   31   0.56433594   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   32   0.56365144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   spiritrussiastu00masagoog_252   33   0.5601908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   anthologyofmoder00selviala_126   34   0.5596266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   lectureonsocialp00tochrich_22   35   0.55929357   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   cu31924073899001_127   36   0.556925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_303   37   0.55556375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   38   0.5535169   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_41   39   0.5508869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   40   0.55032605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
