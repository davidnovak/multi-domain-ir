###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 614223, non-zero: 614223, borderline: 614213, overall act: 63417.025, act diff: 63408.010, ratio: 1.000
#   pulse 3: activated: 614223, non-zero: 614223, borderline: 614213, overall act: 63417.025, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614223
#   final overall activation: 63417.0
#   number of spread. activ. pulses: 3
#   running time: 117926

###################################
# top k results in TREC format: 

7   rel3-1   1863   1   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1831   2   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1830   3   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   1825   4   0.34186885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_312   4   0.6557341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   poland   5   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_794   6   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   7   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   8   0.59603727   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   9   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   10   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1863   11   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1831   12   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   13   0.5821408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   14   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_796   15   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   16   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   17   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   18   0.5766296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_314   19   0.5750714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   20   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   21   0.57169396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   22   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   23   0.56320757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   24   0.5631847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   25   0.56042904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   26   0.55935913   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   27   0.55700266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   28   0.55578136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   29   0.55509245   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_390   30   0.5466382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   31   0.5421291   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   32   0.53885806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_164   33   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_420   34   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_150   35   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   36   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   polishpeasantine01thomuoft_216   37   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   38   0.53849673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_398   39   0.53483915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_406   40   0.52428573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
