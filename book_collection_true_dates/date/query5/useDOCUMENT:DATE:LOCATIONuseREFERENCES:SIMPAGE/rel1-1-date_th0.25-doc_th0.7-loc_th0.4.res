###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   manualforuseofge197778mass_52, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 58, non-zero: 58, borderline: 57, overall act: 10.245, act diff: 9.245, ratio: 0.902
#   pulse 2: activated: 58, non-zero: 58, borderline: 57, overall act: 10.245, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 58
#   final overall activation: 10.2
#   number of spread. activ. pulses: 2
#   running time: 56150

###################################
# top k results in TREC format: 

5   rel1-1   1919   1   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1917   2   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1912   3   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1913   4   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1870   5   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1869   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
5   rel1-1   1909   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 5   rel1-1   manualforuseofge197778mass_52   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   manualforuseofge197778mass_53   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   manualforuseofge197778mass_51   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   united states   4   0.23246084   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   texas   5   0.20010501   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   california   6   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   north carolina   7   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   georgia   8   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   oregon   9   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   arizona   10   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   illinois   11   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   idaho   12   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   wyoming   13   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   colorado   14   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   north dakota   15   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   michigan   16   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   wisconsin   17   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   montana   18   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   minnesota   19   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   indiana   20   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   south dakota   21   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   ohio   22   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   oklahoma   23   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   alabama   24   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   louisiana   25   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   new york   26   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   south carolina   27   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   washington   28   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   tennessee   29   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   nevada   30   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   maine   31   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   west virginia   32   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   kentucky   33   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   maryland   34   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   iowa   35   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   virginia   36   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   new hampshire   37   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   massachusetts   38   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   delaware   39   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 5   rel1-1   missouri   40   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
