###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1052085, non-zero: 1052085, borderline: 1052077, overall act: 147123.701, act diff: 147118.015, ratio: 1.000
#   pulse 3: activated: 1052085, non-zero: 1052085, borderline: 1052077, overall act: 147123.701, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052085
#   final overall activation: 147123.7
#   number of spread. activ. pulses: 3
#   running time: 113750

###################################
# top k results in TREC format: 

2   rel3-3   1641   1   0.5120209   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
2   rel3-3   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   ireland   4   0.80031395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   5   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   6   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   7   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   8   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   9   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   10   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   11   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   12   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   13   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   14   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_249   15   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_382   16   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_229   17   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_172   18   0.60681516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_21   19   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_120   20   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   redeemerstearsw00urwigoog_21   21   0.60359144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   dublinreview14londuoft_194   22   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   irelandsaintpatr00morriala_201   23   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   pt3historyofirel00wriguoft_306   24   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_24   25   0.6024419   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_351   26   0.6016643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   strangerinirelan00carr_91   27   0.6007721   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   cu31924091770861_873   28   0.5973563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   worksrighthonor37burkgoog_439   29   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_612   30   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   fairfaxcorrespon01johniala_870   31   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_312   32   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   methodistmagazin1834meth_610   33   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   publications13irisuoft_16   34   0.5900143   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_385   35   0.5857836   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   calendarstatepa12offigoog_373   36   0.58458275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   fairfaxcorrespon01johniala_705   37   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_380   38   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   collinsspeerageof07coll_161   39   0.5843117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel3-3   historyforreadyr03larnuoft_235   40   0.58422774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
