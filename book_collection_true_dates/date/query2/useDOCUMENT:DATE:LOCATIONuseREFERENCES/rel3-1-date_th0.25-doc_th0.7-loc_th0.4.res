###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 6.989, act diff: 3.989, ratio: 0.571
#   pulse 2: activated: 227904, non-zero: 227904, borderline: 227899, overall act: 44878.133, act diff: 44871.143, ratio: 1.000
#   pulse 3: activated: 227904, non-zero: 227904, borderline: 227899, overall act: 44878.133, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 227904
#   final overall activation: 44878.1
#   number of spread. activ. pulses: 3
#   running time: 50100

###################################
# top k results in TREC format: 

2   rel3-1   1641   1   0.39411858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1689   2   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1701   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1699   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1690   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1651   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-1   1643   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel3-1   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   ireland   4   0.8114913   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_291   5   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_411   6   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_130   7   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   fastiecclesiaeh02cottgoog_20   8   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   compendiumofhist02lawl_76   9   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_345   10   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   calendarstatepa12offigoog_335   11   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_413   12   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   cu31924091786628_78   13   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   castlesofireland00adamiala_301   14   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   confederationofk00meeh_10   15   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   englishinireland03frou_309   16   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   englishinireland03frou_118   17   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   economichistoryo00obri_321   18   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   statechurcheskin00allerich_587   19   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   revolutionaryir00mahagoog_100   20   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   historymoderneu11russgoog_386   21   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   memoirsandcorre02castgoog_42   22   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   historyofdiocese02healiala_23   23   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   irishtangleandwa1920john_53   24   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   constitutionalhi03hall_414   25   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   historyofireland3_00will_307   26   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   proceedingsofro22roya_307   27   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   cu31924091770861_319   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   cu31924091770861_286   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_421   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   politicalstudies00broduoft_370   31   0.52573967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   politicalstudies00broduoft_382   32   0.52029926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_249   33   0.5112249   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   historyirelanda00smilgoog_120   34   0.50910294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   cu31924091770861_873   35   0.50910294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   historicalreview02plow_29   36   0.50664926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_315   37   0.50646454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   irelandsaintpatr00morriala_17   38   0.50529885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_432   39   0.50529885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-1   constitutionalhi03hall_415   40   0.4980216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
