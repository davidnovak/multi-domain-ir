###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 19, overall act: 9.019, act diff: 5.019, ratio: 0.556
#   pulse 2: activated: 1512020, non-zero: 1512020, borderline: 1512010, overall act: 213047.049, act diff: 213038.030, ratio: 1.000
#   pulse 3: activated: 1513151, non-zero: 1513151, borderline: 1512202, overall act: 213311.854, act diff: 264.805, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1513151
#   final overall activation: 213311.9
#   number of spread. activ. pulses: 3
#   running time: 416119

###################################
# top k results in TREC format: 

2   rel4-1   1845   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1689   3   0.99832183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1688   4   0.99764967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1885   5   0.9973401   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1798   6   0.99527514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1649   7   0.98832387   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1691   8   0.9798899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1881   9   0.96882975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1884   10   0.96334773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1829   11   0.9489586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1903   12   0.9380105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1870   13   0.9227757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1896   14   0.9192652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1861   15   0.91493434   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1905   16   0.9144826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1893   17   0.903488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1877   18   0.9032939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1897   19   0.9005922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
2   rel4-1   1804   20   0.8981672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel4-1   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   england   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   cu31924029563875_157   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_600   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   scotland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   london   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   dublin   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   france   12   0.999914   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   great britain   13   0.9998397   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   belfast   14   0.9994268   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1689   15   0.99832183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   strafford   16   0.9980472   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   limerick   17   0.9977579   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1688   18   0.99764967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1885   19   0.9973401   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1798   20   0.99527514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   rome   21   0.991176   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   wales   22   0.99065953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1649   23   0.98832387   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   america   24   0.98792493   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   europe   25   0.9824612   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1691   26   0.9798899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   paris   27   0.97389144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   munster   28   0.9732155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1881   29   0.96882975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   spain   30   0.9687169   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1884   31   0.96334773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   drogheda   32   0.95673156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   leinster   33   0.95090824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1829   34   0.9489586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1903   35   0.9380105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   fairfax   36   0.9309624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   armagh   37   0.9232219   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1870   38   0.9227757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   1896   39   0.9192652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel4-1   kingdom   40   0.91690093   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
