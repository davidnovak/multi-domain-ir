###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1015986, non-zero: 1015986, borderline: 1015979, overall act: 141067.944, act diff: 141062.259, ratio: 1.000
#   pulse 3: activated: 1016169, non-zero: 1016169, borderline: 1015942, overall act: 141098.184, act diff: 30.240, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1016169
#   final overall activation: 141098.2
#   number of spread. activ. pulses: 3
#   running time: 208542

###################################
# top k results in TREC format: 

2   rel3-3   1845   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1640   3   0.53376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1688   4   0.42638123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1649   5   0.33102444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1667   6   0.3046173   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1630   7   0.2832046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1670   8   0.19011897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1819   9   0.17529237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1695   10   0.17529237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1782   11   0.17340614   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1647   12   0.1705421   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1181   13   0.16728532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1041   14   0.15976365   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1725   15   0.15968692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1652   16   0.15927456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1801   17   0.15927456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1798   18   0.15927456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1542   19   0.15927456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-3   1664   20   0.15860924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-3   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   dublin   8   0.99678564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   scotland   9   0.87746125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   london   10   0.8687772   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   france   11   0.71973217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   spain   12   0.65633446   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_315   13   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   calendarstatepa00levagoog_361   14   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_415   15   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_67   16   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_431   17   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_358   18   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   completehistoryo0306kenn_434   19   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_249   20   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_382   21   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_229   22   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   calendarstatepa12offigoog_21   23   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_120   24   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   studentshistoryo03garduoft_42   25   0.60477144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   harleianmiscell00oldygoog_332   26   0.6039614   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   dublinreview14londuoft_194   27   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   irelandsaintpatr00morriala_201   28   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   pt3historyofirel00wriguoft_306   29   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_351   30   0.6016643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   strafford   31   0.5977206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_612   32   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   fairfaxcorrespon01johniala_870   33   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_312   34   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   worksrighthonor37burkgoog_439   35   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   methodistmagazin1834meth_610   36   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   publications13irisuoft_16   37   0.5900143   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   thomasharrisonr00wessgoog_83   38   0.5846675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   calendarstatepa12offigoog_373   39   0.58458275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-3   fairfaxcorrespon01johniala_705   40   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
