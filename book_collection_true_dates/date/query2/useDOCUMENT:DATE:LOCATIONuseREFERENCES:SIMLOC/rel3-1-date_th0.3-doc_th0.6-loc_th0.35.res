###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 6.989, act diff: 3.989, ratio: 0.571
#   pulse 2: activated: 819387, non-zero: 819387, borderline: 819381, overall act: 104643.273, act diff: 104636.284, ratio: 1.000
#   pulse 3: activated: 819414, non-zero: 819414, borderline: 819400, overall act: 104656.431, act diff: 13.158, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819414
#   final overall activation: 104656.4
#   number of spread. activ. pulses: 3
#   running time: 124303

###################################
# top k results in TREC format: 

2   rel3-1   1641   1   0.39411858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1689   2   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1701   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1699   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1690   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1651   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1643   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ireland   4   0.9859868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   clifton house   5   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cangort   6   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cangort park   7   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   shinrone   8   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ballincor   9   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   corolanty   10   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ivy hall   11   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cloughjordan   12   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   kilcomin   13   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   sopwell hall   14   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   loughkeen   15   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   modreeny   16   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   fort nisbett   17   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   sharavogue   18   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   miltown park   19   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   brosna   20   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   carrig   21   0.5709184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_432   22   0.5692939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   historyofireland02daltuoft_409   23   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   olivercromwell00rossgoog_134   24   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091770861_319   25   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_413   26   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_411   27   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   statechurcheskin00allerich_587   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   memoirsandcorre02castgoog_42   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   proceedingsofro22roya_307   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   compendiumofhist02lawl_76   31   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_345   32   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_291   33   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_130   34   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   irishtangleandwa1920john_53   35   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091770861_286   36   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_421   37   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   castlesofireland00adamiala_301   38   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091786628_78   39   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   revolutionaryir00mahagoog_100   40   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
