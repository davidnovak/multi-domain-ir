###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 7, non-zero: 7, borderline: 5, overall act: 3.553, act diff: 1.553, ratio: 0.437
#   pulse 2: activated: 221880, non-zero: 221880, borderline: 221877, overall act: 37808.296, act diff: 37804.743, ratio: 1.000
#   pulse 3: activated: 221880, non-zero: 221880, borderline: 221877, overall act: 37808.296, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221880
#   final overall activation: 37808.3
#   number of spread. activ. pulses: 3
#   running time: 38414

###################################
# top k results in TREC format: 

2   rel2-2   1641   1   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel2-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   ireland   3   0.69080037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   creelofirishstor00barliala_138   4   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_8   5   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_82   6   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   britishmammalsat00johnrich_278   7   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   vikingsbalticat06dasegoog_209   8   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_4   9   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_143   10   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   lettersandjourna02bailuoft_176   11   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_155   12   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_23   13   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_11   14   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_17   15   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_13   16   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_44   17   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_43   18   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   naturalistscabin02smituoft_223   19   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_194   20   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   vikingsbalticat06dasegoog_211   21   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_92   22   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_210   23   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_206   24   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_274   25   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_272   26   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_271   27   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_270   28   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   lettersconcerni00clargoog_278   29   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_278   30   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_277   31   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_262   32   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   sexagenarianorre01belo_202   33   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_268   34   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_267   35   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_264   36   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_252   37   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_256   38   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_254   39   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_253   40   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
