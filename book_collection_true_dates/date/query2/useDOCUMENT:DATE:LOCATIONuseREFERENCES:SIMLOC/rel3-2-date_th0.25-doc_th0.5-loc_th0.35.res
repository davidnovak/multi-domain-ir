###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 7.352, act diff: 4.352, ratio: 0.592
#   pulse 2: activated: 879945, non-zero: 879945, borderline: 879937, overall act: 112390.253, act diff: 112382.902, ratio: 1.000
#   pulse 3: activated: 880235, non-zero: 880235, borderline: 879990, overall act: 112439.881, act diff: 49.628, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 880235
#   final overall activation: 112439.9
#   number of spread. activ. pulses: 3
#   running time: 269744

###################################
# top k results in TREC format: 

2   rel3-2   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1845   2   0.87752324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1688   3   0.7058885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1798   4   0.522807   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1690   5   0.51359   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1640   6   0.4075367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1870   7   0.40407145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1689   8   0.39537784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1654   9   0.32773793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1664   10   0.31199926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1643   11   0.30463284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1649   12   0.29764962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1642   13   0.2967272   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1637   14   0.28620204   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1652   15   0.28508785   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1648   16   0.2489335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1701   17   0.23176718   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1691   18   0.21908283   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1819   19   0.18137266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-2   1695   20   0.18137266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   england   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   cu31924029563875_157   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   dublin   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   london   8   0.9999939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   scotland   9   0.92067075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   1845   10   0.87752324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   munster   11   0.8289094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   strafford   12   0.81286514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   fairfax   13   0.8081174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   limerick   14   0.76274955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   1688   15   0.7058885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   derry   16   0.6593188   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_24   17   0.6349712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_432   18   0.6323343   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   strangerinirelan00carr_91   19   0.63231665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   cu31924091770861_873   20   0.62753767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   historyofireland02daltuoft_409   21   0.619015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   olivercromwell00rossgoog_134   22   0.619015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   politicalstudies00broduoft_385   23   0.6183103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   bibliothecagrenv03grenrich_31   24   0.61736053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   irelandonehundre00walsrich_38   25   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   landwarinireland00godk_205   26   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   compendiumofhist02lawl_189   27   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   commercialrestra00hely_255   28   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   viceroyspostbag02macdgoog_266   29   0.61533123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   historyengland00macagoog_517   30   0.6124737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   cu31924027975733_414   31   0.6124737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   cangort park   32   0.6109669   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   socialstateofgre00berm_196   33   0.6066881   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   confederationofk00meeh_10   34   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_413   35   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   memoirsandcorre02castgoog_42   36   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_411   37   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_130   38   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   proceedingsofro22roya_307   39   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-2   compendiumofhist02lawl_76   40   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
