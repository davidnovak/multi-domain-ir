###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 19, overall act: 7.668, act diff: 4.668, ratio: 0.609
#   pulse 2: activated: 1052138, non-zero: 1052138, borderline: 1052130, overall act: 144375.436, act diff: 144367.767, ratio: 1.000
#   pulse 3: activated: 1052138, non-zero: 1052138, borderline: 1052130, overall act: 144375.436, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052138
#   final overall activation: 144375.4
#   number of spread. activ. pulses: 3
#   running time: 135776

###################################
# top k results in TREC format: 

2   rel3-4   1641   1   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1689   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1701   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1699   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1690   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1651   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
2   rel3-4   1643   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel3-4   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   cu31924029563875_157   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   ireland   4   0.70711035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   studentshistoryo03garduoft_42   5   0.6013532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   harleianmiscell00oldygoog_332   6   0.5960795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_24   7   0.5889065   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   thomasharrisonr00wessgoog_83   8   0.5870921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   redeemerstearsw00urwigoog_21   9   0.58627963   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   strangerinirelan00carr_91   10   0.5800713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_315   11   0.5771714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   calendarstatepa00levagoog_361   12   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyirelanda00smilgoog_172   13   0.5741105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_431   14   0.57091826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyirishper01maddgoog_67   15   0.5707552   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   constitutionalhi03hall_415   16   0.57052505   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   selectspeecheswi00cannuoft_358   17   0.5678152   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   irelandsfightfor1919cree_101   18   0.5672616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   completehistoryo0306kenn_434   19   0.56657606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyengland00macagoog_517   20   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   cu31924027975733_414   21   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   cu31924091770861_873   22   0.5660654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_385   23   0.5639206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyirishper01maddgoog_229   24   0.5624587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   calendarstatepa12offigoog_21   25   0.5618158   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   viceroyspostbag02macdgoog_266   26   0.5610784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   irelandonehundre00walsrich_38   27   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   landwarinireland00godk_205   28   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   compendiumofhist02lawl_189   29   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   commercialrestra00hely_255   30   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_249   31   0.55935603   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyofbritish00colluoft_221   32   0.55848306   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_382   33   0.5582852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyirelanda00smilgoog_120   34   0.5574941   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   selectionofcases01willuoft_544   35   0.5572608   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   historyforreadyr03larnuoft_236   36   0.55689776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   cu31924077097792_337   37   0.5566082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_351   38   0.55613655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   irelandsaintpatr00morriala_201   39   0.55534315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel3-4   pt3historyofirel00wriguoft_306   40   0.55534315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
