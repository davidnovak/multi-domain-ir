###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 6, non-zero: 6, borderline: 5, overall act: 2.346, act diff: 1.346, ratio: 0.574
#   pulse 2: activated: 45472, non-zero: 45472, borderline: 45469, overall act: 3473.722, act diff: 3471.377, ratio: 0.999
#   pulse 3: activated: 45472, non-zero: 45472, borderline: 45469, overall act: 3473.722, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 45472
#   final overall activation: 3473.7
#   number of spread. activ. pulses: 3
#   running time: 19213

###################################
# top k results in TREC format: 

2   rel1-4   1845   1   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
2   rel1-4   1641   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
2   rel1-4   1846   3   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
2   rel1-4   1844   4   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
2   rel1-4   1640   5   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
2   rel1-4   1642   6   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel1-4   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   1641   3   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   england   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   ireland   5   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   dublin   6   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   blacksguidetoire00adam_492   7   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924031452000_435   8   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924031452000_431   9   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   principlesscien07jevogoog_417   10   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   journal110senagoog_179   11   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   principlespracti18861fagg_631   12   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   historyoforegonc00greeuoft_435   13   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   nilesnationalreg71nile_305   14   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   armylistjanpart221946grea_439   15   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924031452000_439   16   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   prideauxsformspr01prid_267   17   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cavourmakingofmo00orsi_132   18   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   annualrepor06amer_700   19   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cavourmakingofmo00orsi_143   20   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924018826077_8   21   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   prideauxsformspr01prid_283   22   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   bebelsreminiscen00bebe_129   23   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   sunnymemoriesoff01stow_188   24   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   ancientirishpari00cowa_101   25   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   geschichtedersch02tickuoft_544   26   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   winthropsjournal04wint_45   27   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   medievalenglishn00poweuoft_561   28   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   peacemanualorwar00beck_123   29   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   historyofpeterpa00linz_40   30   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924002897142_115   31   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   memoirsofmaximil663sull_128   32   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   worksoliverwend28holmgoog_135   33   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   proceedingsacad85unkngoog_126   34   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   cu31924019287378_76   35   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   manualgeology00lyelrich_168   36   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   manualgeology00lyelrich_167   37   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   manualgeology00lyelrich_165   38   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   onstagestudiesof01cookuoft_270   39   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-4   lifegeorgesteph05smilgoog_376   40   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
