###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1052129, non-zero: 1052129, borderline: 1052121, overall act: 147134.302, act diff: 147128.617, ratio: 1.000
#   pulse 3: activated: 1059800, non-zero: 1059800, borderline: 1059516, overall act: 147985.843, act diff: 851.540, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059800
#   final overall activation: 147985.8
#   number of spread. activ. pulses: 3
#   running time: 238230

###################################
# top k results in TREC format: 

2   rel3-3   1845   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1640   3   0.6917996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1688   4   0.61729634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1649   5   0.5594116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1642   6   0.43395188   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1689   7   0.38510615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1846   8   0.35338616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1667   9   0.3046173   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1630   10   0.2832046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1830   11   0.27363715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1844   12   0.27199826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1652   13   0.2657883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1643   14   0.26209974   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1687   15   0.25722107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1821   16   0.24783595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1639   17   0.23188552   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1663   18   0.2209002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1885   19   0.21459906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1670   20   0.19011897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   london   9   0.9784362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   scotland   10   0.9643316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   clifton house   11   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort   12   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort park   13   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   shinrone   14   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cloughjordan   15   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   kilcomin   16   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   strafford   17   0.780546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   corolanty   18   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   sopwell hall   19   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   france   20   0.76255596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   limerick   21   0.7164269   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   spain   22   0.7159989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ballincor   23   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   loughkeen   24   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1640   25   0.6917996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ivy hall   26   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   modreeny   27   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   fort nisbett   28   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   29   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   30   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   31   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   32   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   33   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1688   34   0.61729634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   35   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   36   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   37   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   38   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   39   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cu31924091770861_898   40   0.61069614   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
