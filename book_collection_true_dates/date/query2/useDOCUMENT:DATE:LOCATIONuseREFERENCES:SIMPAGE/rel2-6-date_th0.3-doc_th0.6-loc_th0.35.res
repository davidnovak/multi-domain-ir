###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 10, overall act: 6.064, act diff: 4.064, ratio: 0.670
#   pulse 2: activated: 1052086, non-zero: 1052086, borderline: 1052079, overall act: 133003.729, act diff: 132997.665, ratio: 1.000
#   pulse 3: activated: 1052091, non-zero: 1052091, borderline: 1052081, overall act: 133006.341, act diff: 2.612, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052091
#   final overall activation: 133006.3
#   number of spread. activ. pulses: 3
#   running time: 143868

###################################
# top k results in TREC format: 

2   rel2-6   1845   1   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-6   1641   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-6   1632   3   0.2107382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-6   1629   4   0.2107382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel2-6   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   ireland   3   0.8426498   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   hallamsworks04halliala_601   4   0.6794118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_96   5   0.6781855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   dublin   6   0.6343429   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   england   7   0.62116814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   hallamsworks04halliala_599   8   0.61644304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   hallamsworks04halliala_602   9   0.5617279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   strafford   10   0.5275149   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_95   11   0.5163028   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   studentshistoryo03garduoft_42   12   0.4950537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   harleianmiscell00oldygoog_332   13   0.49479952   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   parliamentaryhis00obriuoft_215   14   0.47758645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   thomasharrisonr00wessgoog_83   15   0.47464165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_431   16   0.47398064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   calendarstatepa00levagoog_361   17   0.47340676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   historyirishper01maddgoog_67   18   0.47104666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_315   19   0.4708332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   historyofmodernb00conauoft_188   20   0.4698304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   redeemerstearsw00urwigoog_21   21   0.4680659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   constitutionalhi03hall_415   22   0.4649451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   completehistoryo0306kenn_434   23   0.46446007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   selectspeecheswi00cannuoft_358   24   0.4641117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_98   25   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   historyirelanda00smilgoog_172   26   0.46159548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   historyirishper01maddgoog_229   27   0.46150184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   encyclopaediaofl07polluoft_81   28   0.46143323   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   calendarstatepa12offigoog_21   29   0.46046337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   encyclopaediaofl07polluoft_77   30   0.45859015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   irelandsfightfor1919cree_101   31   0.45836282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   lifeofdanielocon00cusa_839   32   0.45620295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   englishlawirisht00gibbrich_20   33   0.4559494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_24   34   0.45535484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   essayonelementso00burn_72   35   0.45461884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   politicalstudies00broduoft_351   36   0.45359546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   memoirsandcorre02castgoog_67   37   0.4522274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   ahistorymodernb03conagoog_187   38   0.45012453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   livingagevolume01littgoog_588   39   0.4495837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-6   cu31924077097792_337   40   0.44917008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
