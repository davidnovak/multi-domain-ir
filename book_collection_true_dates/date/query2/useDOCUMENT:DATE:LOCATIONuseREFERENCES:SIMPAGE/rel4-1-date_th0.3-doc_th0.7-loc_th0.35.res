###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1512021, non-zero: 1512021, borderline: 1512011, overall act: 213050.127, act diff: 213037.411, ratio: 1.000
#   pulse 3: activated: 1512024, non-zero: 1512024, borderline: 1512012, overall act: 213051.909, act diff: 1.782, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512024
#   final overall activation: 213051.9
#   number of spread. activ. pulses: 3
#   running time: 192557

###################################
# top k results in TREC format: 

2   rel4-1   1641   1   0.62436765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1632   3   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1629   4   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1701   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1699   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1690   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1689   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1651   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1643   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   ireland   5   0.9548845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_601   6   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_96   7   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_355   8   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   studentshistoryo03garduoft_42   9   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_599   10   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   harleianmiscell00oldygoog_332   11   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   dublin   12   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_24   13   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   thomasharrisonr00wessgoog_83   14   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_315   15   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   strangerinirelan00carr_91   16   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   redeemerstearsw00urwigoog_21   17   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyofengland08lodguoft_78   18   0.673832   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   bibliothecagrenv03grenrich_31   19   0.67260575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa00levagoog_361   20   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   constitutionalhi03hall_415   21   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   publications13irisuoft_16   22   0.67004275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirelanda00smilgoog_172   23   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   cu31924091770861_873   24   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   selectspeecheswi00cannuoft_358   25   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirishper01maddgoog_67   26   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyforreadyr03larnuoft_236   27   0.6661447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa12offigoog_373   28   0.664576   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_431   29   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   completehistoryo0306kenn_434   30   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   politicalstudies00broduoft_382   31   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_249   32   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirelanda00smilgoog_120   33   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   politicalstudies00broduoft_385   34   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_432   35   0.659821   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyirishper01maddgoog_229   36   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_602   37   0.6593794   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa12offigoog_21   38   0.6592293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   landwarinireland00godk_205   39   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   compendiumofhist02lawl_189   40   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
