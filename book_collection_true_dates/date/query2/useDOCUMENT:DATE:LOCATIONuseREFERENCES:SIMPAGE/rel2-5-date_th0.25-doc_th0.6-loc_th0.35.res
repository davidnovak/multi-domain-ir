###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.736, act diff: 5.736, ratio: 0.741
#   pulse 2: activated: 879905, non-zero: 879905, borderline: 879898, overall act: 96842.262, act diff: 96834.526, ratio: 1.000
#   pulse 3: activated: 879909, non-zero: 879909, borderline: 879900, overall act: 96843.841, act diff: 1.579, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 879909
#   final overall activation: 96843.8
#   number of spread. activ. pulses: 3
#   running time: 156369

###################################
# top k results in TREC format: 

2   rel2-5   1641   1   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1689   3   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1701   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1699   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1690   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1651   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel2-5   1643   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel2-5   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   ireland   3   0.732791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_601   4   0.62992036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_599   5   0.60976464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924029563875_158   6   0.5628241   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   dublin   7   0.5586054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924029563875_156   8   0.5116861   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   bibliothecagrenv03grenrich_31   9   0.50440717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_24   10   0.5004303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   strangerinirelan00carr_91   11   0.4914904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   calendarstatepa12offigoog_525   12   0.48964527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_432   13   0.48509726   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyengland00macagoog_517   14   0.48264053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924027975733_414   15   0.4826405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   politicalstudies00broduoft_385   16   0.47820508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924091770861_873   17   0.47759178   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   viceroyspostbag02macdgoog_266   18   0.4760442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   compendiumofhist02lawl_189   19   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   irelandonehundre00walsrich_38   20   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   landwarinireland00godk_205   21   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   commercialrestra00hely_255   22   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyofireland02daltuoft_409   23   0.47090447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   olivercromwell00rossgoog_134   24   0.47090447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyforreadyr03larnuoft_236   25   0.46602526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   irelandundercomm02dunluoft_279   26   0.4648844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyofenniski02trim_72   27   0.4648844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   socialstateofgre00berm_196   28   0.45929736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   redeemerstearsw00urwigoog_21   29   0.45559338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyofclareda00whit_272   30   0.4551854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   whathavegreeksd00mahagoog_196   31   0.45385545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   irelandpopebrief00magurich_47   32   0.45385545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   cu31924029565607_447   33   0.45168686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   calendarstatepa12offigoog_541   34   0.450603   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_602   35   0.45031837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historyofengland08lodguoft_78   36   0.45012972   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   1641   37   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_392   38   0.44600916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   lifeofolivercrom02head_290   39   0.44313112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel2-5   historycountydu01dalgoog_819   40   0.4403133   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
