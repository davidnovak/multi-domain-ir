###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 9, overall act: 5.791, act diff: 3.791, ratio: 0.655
#   pulse 2: activated: 264008, non-zero: 264008, borderline: 264003, overall act: 38701.928, act diff: 38696.137, ratio: 1.000
#   pulse 3: activated: 264034, non-zero: 264034, borderline: 263998, overall act: 38717.477, act diff: 15.549, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264034
#   final overall activation: 38717.5
#   number of spread. activ. pulses: 3
#   running time: 59561

###################################
# top k results in TREC format: 

2   rel2-3   1641   1   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel2-3   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel2-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   ireland   3   0.99999994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   1641   4   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   constitutionalhi03hall_415   5   0.6576398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   englandsfightwit00wals_355   6   0.63972855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_601   7   0.6246651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_599   8   0.59106064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   fastiecclesiaeh02cottgoog_19   9   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_414   10   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   calendarstatepa12offigoog_336   11   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   economichistoryo00obri_320   12   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   fastiecclesiaeh02cottgoog_21   13   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   englishinireland03frou_308   14   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_131   15   0.5201666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_291   16   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_130   17   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   calendarstatepa12offigoog_335   18   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   economichistoryo00obri_321   19   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   historymoderneu11russgoog_386   20   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   historyofdiocese02healiala_23   21   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   statechurcheskin00allerich_587   22   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   irishtangleandwa1920john_53   23   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   constitutionalhi03hall_414   24   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   memoirsandcorre02castgoog_42   25   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   historyofireland3_00will_307   26   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   proceedingsofro22roya_307   27   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   cu31924091770861_319   28   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   cu31924091770861_286   29   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   cu31924091786628_78   30   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   castlesofireland00adamiala_301   31   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   confederationofk00meeh_10   32   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   englishinireland03frou_309   33   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   fastiecclesiaeh02cottgoog_20   34   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   englishinireland03frou_118   35   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_345   36   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_413   37   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   compendiumofhist02lawl_76   38   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_411   39   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_421   40   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
