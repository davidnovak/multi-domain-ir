###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 9, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 221882, non-zero: 221882, borderline: 221879, overall act: 37810.003, act diff: 37804.602, ratio: 1.000
#   pulse 3: activated: 221882, non-zero: 221882, borderline: 221879, overall act: 37810.003, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221882
#   final overall activation: 37810.0
#   number of spread. activ. pulses: 3
#   running time: 41964

###################################
# top k results in TREC format: 

2   rel2-2   1641   1   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel2-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   ireland   3   0.69080037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   englandsfightwit00wals_355   4   0.65287596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   viceroysofirelan00omahuoft_96   5   0.6117925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   englandsfightwit00wals_353   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   viceroysofirelan00omahuoft_98   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   creelofirishstor00barliala_138   8   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_8   9   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_82   10   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   britishmammalsat00johnrich_278   11   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   vikingsbalticat06dasegoog_209   12   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_4   13   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_143   14   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   lettersandjourna02bailuoft_176   15   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_155   16   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_23   17   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_11   18   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_17   19   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_13   20   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_44   21   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_43   22   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   naturalistscabin02smituoft_223   23   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   lifeandlettersg02towngoog_194   24   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   vikingsbalticat06dasegoog_211   25   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   speeches00philiala_92   26   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_210   27   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_206   28   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_274   29   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_272   30   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_271   31   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_270   32   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   lettersconcerni00clargoog_278   33   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_278   34   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_277   35   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_262   36   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   sexagenarianorre01belo_202   37   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_268   38   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_267   39   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel2-2   doingmybitforire00skiniala_264   40   0.3322899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
