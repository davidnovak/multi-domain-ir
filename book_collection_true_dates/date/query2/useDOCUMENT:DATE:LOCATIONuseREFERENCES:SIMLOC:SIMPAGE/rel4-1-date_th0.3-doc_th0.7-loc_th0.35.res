###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1512071, non-zero: 1512071, borderline: 1512061, overall act: 213063.165, act diff: 213050.449, ratio: 1.000
#   pulse 3: activated: 1512116, non-zero: 1512116, borderline: 1512090, overall act: 213088.106, act diff: 24.940, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512116
#   final overall activation: 213088.1
#   number of spread. activ. pulses: 3
#   running time: 228967

###################################
# top k results in TREC format: 

2   rel4-1   1641   1   0.62436765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1632   3   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1629   4   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1701   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1699   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1690   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1689   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1651   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel4-1   1643   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   ireland   5   0.99963075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   clifton house   6   0.9817638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   cangort   7   0.9751949   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   cangort park   8   0.96469945   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   kilcomin   9   0.94732016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   sopwell hall   10   0.9452628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   ballincor   11   0.9303618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   corolanty   12   0.92966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   cloughjordan   13   0.9198651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   shinrone   14   0.90191466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   modreeny   15   0.887132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   fort nisbett   16   0.8837817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   loughkeen   17   0.8606351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   ivy hall   18   0.8591056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   sharavogue   19   0.78621817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_601   20   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_96   21   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   miltown park   22   0.71058005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   borrisokane   23   0.707991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   brosna   24   0.70731235   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   carrig   25   0.70708025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_355   26   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   studentshistoryo03garduoft_42   27   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_599   28   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   harleianmiscell00oldygoog_332   29   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   dublin   30   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_24   31   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   thomasharrisonr00wessgoog_83   32   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_315   33   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   strangerinirelan00carr_91   34   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   redeemerstearsw00urwigoog_21   35   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   historyofengland08lodguoft_78   36   0.673832   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   bibliothecagrenv03grenrich_31   37   0.67260575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   calendarstatepa00levagoog_361   38   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   constitutionalhi03hall_415   39   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel4-1   publications13irisuoft_16   40   0.67004275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
