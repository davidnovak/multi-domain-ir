###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1052126, non-zero: 1052126, borderline: 1052118, overall act: 147135.786, act diff: 147127.328, ratio: 1.000
#   pulse 3: activated: 1052522, non-zero: 1052522, borderline: 1052235, overall act: 147310.745, act diff: 174.959, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052522
#   final overall activation: 147310.7
#   number of spread. activ. pulses: 3
#   running time: 231993

###################################
# top k results in TREC format: 

2   rel3-3   1845   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1688   3   0.61729634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1649   4   0.5594116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1640   5   0.53376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1689   6   0.38510615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1667   7   0.3046173   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1630   8   0.2832046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1830   9   0.27363715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1652   10   0.2657883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1687   11   0.25722107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1821   12   0.24783595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1632   13   0.22315224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1629   14   0.22315224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1663   15   0.2209002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1885   16   0.21459906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1846   17   0.21328233   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1642   18   0.2057647   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1670   19   0.19011897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   1819   20   0.17529237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   london   9   0.9784362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   scotland   10   0.9717034   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   strafford   11   0.8892657   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   clifton house   12   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort   13   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort park   14   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   shinrone   15   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cloughjordan   16   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   kilcomin   17   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   corolanty   18   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   sopwell hall   19   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   20   0.7650973   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   france   21   0.76255596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   22   0.7561911   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_414   23   0.74639887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   historyforreadyr03larnuoft_235   24   0.74204755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_612   25   0.7416121   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   historyforreadyr03larnuoft_236   26   0.73663855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_601   27   0.728054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_432   28   0.7246256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_96   29   0.71948135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   limerick   30   0.7164269   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   spain   31   0.7159989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_613   32   0.7140476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ballincor   33   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   loughkeen   34   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ivy hall   35   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_381   36   0.69039124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   englishlawirisht00gibbrich_20   37   0.6858451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   modreeny   38   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_605   39   0.68142223   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_355   40   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
