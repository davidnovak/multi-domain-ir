###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1512071, non-zero: 1512071, borderline: 1512061, overall act: 213063.165, act diff: 213050.449, ratio: 1.000
#   pulse 3: activated: 1512289, non-zero: 1512289, borderline: 1512128, overall act: 213173.457, act diff: 110.292, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512289
#   final overall activation: 213173.5
#   number of spread. activ. pulses: 3
#   running time: 322004

###################################
# top k results in TREC format: 

2   rel4-1   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1643   2   0.32139573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1845   3   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1632   4   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1629   5   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1537   6   0.18979684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1654   7   0.18940406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1640   8   0.18895423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1782   9   0.18766755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1664   10   0.18745312   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1689   11   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1701   12   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1699   13   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1690   14   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1651   15   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1653   16   0.15851516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1688   17   0.15599239   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1637   18   0.1558705   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1630   19   0.1558705   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel4-1   1667   20   0.15508726   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   dublin   8   0.9999994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   london   9   0.99889284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   clifton house   10   0.9817638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   cangort   11   0.9751949   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   cangort park   12   0.96469945   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   kilcomin   13   0.94732016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   sopwell hall   14   0.9452628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   ballincor   15   0.9303618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   corolanty   16   0.92966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   cloughjordan   17   0.9198651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   shinrone   18   0.90191466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   modreeny   19   0.887132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   fort nisbett   20   0.8837817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   loughkeen   21   0.8606351   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   ivy hall   22   0.8591056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   strafford   23   0.82931924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_431   24   0.8112878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   constitutionalhi03hall_415   25   0.81100243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   causeofirelandpl00orei_432   26   0.809218   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   historyforreadyr03larnuoft_236   27   0.8086368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   historyforreadyr03larnuoft_235   28   0.7964654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   constitutionalhi03hall_414   29   0.7961591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   sharavogue   30   0.78621817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   revolutionaryir00mahagoog_99   31   0.75741476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_613   32   0.7562776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   hallamsworks04halliala_601   33   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   viceroysofirelan00omahuoft_96   34   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   publications13irisuoft_17   35   0.73535556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   politicalstudies00broduoft_381   36   0.730032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   calendarstatepa12offigoog_636   37   0.72835165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   scotland   38   0.71763396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   miltown park   39   0.71058005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel4-1   borrisokane   40   0.707991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
