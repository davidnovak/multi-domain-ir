###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 19, overall act: 7.668, act diff: 4.668, ratio: 0.609
#   pulse 2: activated: 1512024, non-zero: 1512024, borderline: 1512015, overall act: 203903.693, act diff: 203896.025, ratio: 1.000
#   pulse 3: activated: 1512419, non-zero: 1512419, borderline: 1512189, overall act: 203971.827, act diff: 68.134, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512419
#   final overall activation: 203971.8
#   number of spread. activ. pulses: 3
#   running time: 367019

###################################
# top k results in TREC format: 

2   rel3-4   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1845   2   0.99809504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1689   3   0.86904615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1691   4   0.7872588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1688   5   0.75252444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1885   6   0.7103773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1804   7   0.58380115   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1898   8   0.5607454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1897   9   0.5405758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1873   10   0.52112216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1642   11   0.48936862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1903   12   0.48805496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1870   13   0.4811802   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1844   14   0.4543712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1877   15   0.44066635   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1893   16   0.4317892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1829   17   0.40868673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1890   18   0.40761375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1798   19   0.40622163   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1664   20   0.4047451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   london   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   ireland   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1845   9   0.99809504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   scotland   10   0.9640205   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1689   11   0.86904615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1691   12   0.7872588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   limerick   13   0.76061654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1688   14   0.75252444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1885   15   0.7103773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   belfast   16   0.66713935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   londonderry   17   0.6664363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   great britain   18   0.6598652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   strafford   19   0.6557433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   studentshistoryo03garduoft_42   20   0.6013532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   harleianmiscell00oldygoog_332   21   0.5960795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_31   22   0.5918422   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyofengland08lodguoft_78   23   0.5896279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_24   24   0.5889065   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   thomasharrisonr00wessgoog_83   25   0.5870921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   redeemerstearsw00urwigoog_21   26   0.58627963   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_236   27   0.5849868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1804   28   0.58380115   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   strangerinirelan00carr_91   29   0.5800713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   publications13irisuoft_16   30   0.57954997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_315   31   0.5771714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   calendarstatepa00levagoog_361   32   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyirelanda00smilgoog_172   33   0.5741105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_431   34   0.57091826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyirishper01maddgoog_67   35   0.5707552   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_415   36   0.57052505   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   calendarstatepa12offigoog_373   37   0.56995064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   selectspeecheswi00cannuoft_358   38   0.5678152   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   irelandsfightfor1919cree_101   39   0.5672616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   completehistoryo0306kenn_434   40   0.56657606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
