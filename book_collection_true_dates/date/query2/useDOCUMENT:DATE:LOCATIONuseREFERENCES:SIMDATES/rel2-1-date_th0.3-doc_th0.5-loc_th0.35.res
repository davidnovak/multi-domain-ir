###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 16, overall act: 5.222, act diff: 3.222, ratio: 0.617
#   pulse 2: activated: 819363, non-zero: 819363, borderline: 819358, overall act: 96379.136, act diff: 96373.914, ratio: 1.000
#   pulse 3: activated: 819363, non-zero: 819363, borderline: 819355, overall act: 96380.071, act diff: 0.935, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819363
#   final overall activation: 96380.1
#   number of spread. activ. pulses: 3
#   running time: 126735

###################################
# top k results in TREC format: 

2   rel2-1   1641   1   0.82806534   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1640   2   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1642   3   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1701   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1699   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1690   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1689   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1651   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-1   1643   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel2-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   englandsfightwit00wals_354   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   ireland   3   0.895175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   1641   4   0.82806534   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   london   5   0.6199228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_432   6   0.5202243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   historyofireland02daltuoft_409   7   0.5051707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   olivercromwell00rossgoog_134   8   0.5051707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   bibliothecagrenv03grenrich_31   9   0.4830366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   cu31924091770861_286   10   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   revolutionaryir00mahagoog_100   11   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   castlesofireland00adamiala_301   12   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   historyofdiocese02healiala_23   13   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   confederationofk00meeh_10   14   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   cu31924091770861_319   15   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   irishtangleandwa1920john_53   16   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   cu31924091786628_78   17   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   englishinireland03frou_309   18   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   englishinireland03frou_118   19   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   economichistoryo00obri_321   20   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   fastiecclesiaeh02cottgoog_20   21   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   historyofireland3_00will_307   22   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_413   23   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   historymoderneu11russgoog_386   24   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_411   25   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   constitutionalhi03hall_414   26   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_130   27   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   statechurcheskin00allerich_587   28   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   memoirsandcorre02castgoog_42   29   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   proceedingsofro22roya_307   30   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   compendiumofhist02lawl_76   31   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_345   32   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   causeofirelandpl00orei_291   33   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   treatiseofexcheq02howa_421   34   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   calendarstatepa12offigoog_335   35   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   historyofclareda00whit_272   36   0.4753641   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   calendarstatepa12offigoog_475   37   0.4700415   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   politicalstudies00broduoft_370   38   0.46916416   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   gloucestershiren03londuoft_699   39   0.46632728   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-1   politicalstudies00broduoft_382   40   0.46444523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
