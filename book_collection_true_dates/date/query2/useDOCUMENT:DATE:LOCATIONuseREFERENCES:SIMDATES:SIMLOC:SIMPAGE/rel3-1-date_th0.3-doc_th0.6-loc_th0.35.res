###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 9.762, act diff: 6.762, ratio: 0.693
#   pulse 2: activated: 819391, non-zero: 819391, borderline: 819385, overall act: 104646.213, act diff: 104636.451, ratio: 1.000
#   pulse 3: activated: 819424, non-zero: 819424, borderline: 819408, overall act: 104661.070, act diff: 14.857, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 819424
#   final overall activation: 104661.1
#   number of spread. activ. pulses: 3
#   running time: 146144

###################################
# top k results in TREC format: 

2   rel3-1   1641   1   0.39411858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1632   2   0.1975077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1629   3   0.1975077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1640   4   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1642   5   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1689   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1699   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1701   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1690   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1651   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-1   1643   11   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ireland   4   0.99450743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   clifton house   5   0.9165857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cangort   6   0.91597384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cangort park   7   0.88515484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   shinrone   8   0.8793991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ballincor   9   0.8386512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   corolanty   10   0.8373628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   ivy hall   11   0.8319303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cloughjordan   12   0.8212805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   kilcomin   13   0.7858642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   sopwell hall   14   0.77894896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   loughkeen   15   0.77257013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   modreeny   16   0.68817604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   englandsfightwit00wals_355   17   0.68008155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   fort nisbett   18   0.6792251   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   viceroysofirelan00omahuoft_96   19   0.63442165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   sharavogue   20   0.58369094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   miltown park   21   0.5764139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   brosna   22   0.57356834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   carrig   23   0.5709184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_432   24   0.5692939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924029563875_158   25   0.5628241   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   historyofireland02daltuoft_409   26   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   olivercromwell00rossgoog_134   27   0.5540048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   proceedingsofro22roya_307   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   compendiumofhist02lawl_76   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_345   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   causeofirelandpl00orei_291   31   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_421   32   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   irishtangleandwa1920john_53   33   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091770861_319   34   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   revolutionaryir00mahagoog_100   35   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091770861_286   36   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_411   37   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   cu31924091786628_78   38   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   treatiseofexcheq02howa_130   39   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-1   englishinireland03frou_309   40   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
