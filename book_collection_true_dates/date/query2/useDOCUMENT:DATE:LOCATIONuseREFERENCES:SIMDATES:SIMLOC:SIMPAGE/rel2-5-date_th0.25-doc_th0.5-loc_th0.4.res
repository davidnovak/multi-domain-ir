###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.736, act diff: 5.736, ratio: 0.741
#   pulse 2: activated: 264026, non-zero: 264026, borderline: 264021, overall act: 30612.006, act diff: 30604.271, ratio: 1.000
#   pulse 3: activated: 264030, non-zero: 264030, borderline: 264023, overall act: 30613.485, act diff: 1.478, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264030
#   final overall activation: 30613.5
#   number of spread. activ. pulses: 3
#   running time: 74571

###################################
# top k results in TREC format: 

2   rel2-5   1641   1   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1640   3   0.22030298   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1642   4   0.22030298   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1701   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1699   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1690   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1689   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1651   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1643   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1846   11   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-5   1844   12   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-5   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   ireland   3   0.71973675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_601   4   0.5904117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_599   5   0.5631442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   dublin   6   0.5485247   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924029563875_156   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924029563875_158   8   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   1641   9   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_291   10   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_411   11   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_130   12   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   fastiecclesiaeh02cottgoog_20   13   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   englishinireland03frou_118   14   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   compendiumofhist02lawl_76   15   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_345   16   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   calendarstatepa12offigoog_335   17   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   economichistoryo00obri_321   18   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_413   19   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   revolutionaryir00mahagoog_100   20   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   historymoderneu11russgoog_386   21   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   statechurcheskin00allerich_587   22   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   memoirsandcorre02castgoog_42   23   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   historyofireland3_00will_307   24   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   historyofdiocese02healiala_23   25   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   proceedingsofro22roya_307   26   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   irishtangleandwa1920john_53   27   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   constitutionalhi03hall_414   28   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091770861_319   29   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091770861_286   30   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091786628_78   31   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   castlesofireland00adamiala_301   32   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   confederationofk00meeh_10   33   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   englishinireland03frou_309   34   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_421   35   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_602   36   0.43443072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_370   37   0.42702368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_382   38   0.4233431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_249   39   0.41723633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-5   historyirelanda00smilgoog_120   40   0.41581392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
