###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1052144, non-zero: 1052144, borderline: 1052135, overall act: 153564.419, act diff: 153551.703, ratio: 1.000
#   pulse 3: activated: 1059676, non-zero: 1059676, borderline: 1059546, overall act: 154599.790, act diff: 1035.372, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059676
#   final overall activation: 154599.8
#   number of spread. activ. pulses: 3
#   running time: 246069

###################################
# top k results in TREC format: 

2   rel4-1   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1640   2   0.46481532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1643   3   0.44500557   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1845   4   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1642   5   0.3024225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1632   6   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1629   7   0.23030055   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1782   8   0.18766755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1701   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1699   10   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1689   11   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1690   12   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1651   13   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1653   14   0.15851516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1688   15   0.15599239   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1667   16   0.15508726   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1846   17   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1844   18   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   1639   19   0.15006922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   dublin   8   0.9999947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   9   0.81100243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   10   0.8018684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_414   11   0.7961591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_236   12   0.79371476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_235   13   0.79206616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_432   14   0.7757778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   strafford   15   0.7584124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_613   16   0.7562776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_601   17   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_96   18   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_602   19   0.73656374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_381   20   0.730032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   revolutionaryir00mahagoog_99   21   0.7286991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_636   22   0.72835165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   scotland   23   0.7137856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_68   24   0.707184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_25   25   0.7043432   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_435   26   0.7036122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_357   27   0.70238817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   london   28   0.69957477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   cu31924091770861_287   29   0.6990484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_430   30   0.6961846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_355   31   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_23   32   0.6917561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   33   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyengland00macagoog_518   34   0.68964297   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_237   35   0.68822455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_599   36   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_292   37   0.686227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   38   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   39   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   publications13irisuoft_17   40   0.68028486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
