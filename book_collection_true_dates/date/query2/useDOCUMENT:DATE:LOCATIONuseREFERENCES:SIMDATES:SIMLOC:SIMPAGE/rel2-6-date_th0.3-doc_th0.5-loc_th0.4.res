###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 10, overall act: 6.064, act diff: 4.064, ratio: 0.670
#   pulse 2: activated: 1016015, non-zero: 1016015, borderline: 1016009, overall act: 126891.961, act diff: 126885.898, ratio: 1.000
#   pulse 3: activated: 1016020, non-zero: 1016020, borderline: 1016011, overall act: 126894.435, act diff: 2.473, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1016020
#   final overall activation: 126894.4
#   number of spread. activ. pulses: 3
#   running time: 138026

###################################
# top k results in TREC format: 

2   rel2-6   1845   1   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1641   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1632   3   0.1981199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1629   4   0.1981199   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1844   5   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1846   6   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1640   7   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-6   1642   8   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-6   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   ireland   3   0.8314368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   hallamsworks04halliala_601   4   0.6457772   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   viceroysofirelan00omahuoft_96   5   0.6364413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   dublin   6   0.62151855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   england   7   0.6128826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   hallamsworks04halliala_599   8   0.57267815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   hallamsworks04halliala_602   9   0.5501084   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   strafford   10   0.5120625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   viceroysofirelan00omahuoft_95   11   0.5008301   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   parliamentaryhis00obriuoft_215   12   0.47758645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_431   13   0.47398064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   calendarstatepa00levagoog_361   14   0.47340676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirishper01maddgoog_67   15   0.47104666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_315   16   0.4708332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyofmodernb00conauoft_188   17   0.4698304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   harleianmiscell00oldygoog_332   18   0.46526134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   constitutionalhi03hall_415   19   0.4649451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   completehistoryo0306kenn_434   20   0.46446007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   selectspeecheswi00cannuoft_358   21   0.4641117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   viceroysofirelan00omahuoft_98   22   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirishper01maddgoog_229   23   0.46150184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   calendarstatepa12offigoog_21   24   0.46046337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   studentshistoryo03garduoft_42   25   0.4598504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   lifeofdanielocon00cusa_839   26   0.45620295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   englishlawirisht00gibbrich_20   27   0.4559494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   essayonelementso00burn_72   28   0.45461884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   politicalstudies00broduoft_351   29   0.45359546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   memoirsandcorre02castgoog_67   30   0.4522274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   ahistorymodernb03conagoog_187   31   0.45012453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   livingagevolume01littgoog_588   32   0.4495837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   causeofirelandpl00orei_249   33   0.44902313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   publications13irisuoft_16   34   0.44808164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   dublinreview14londuoft_194   35   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   irelandsaintpatr00morriala_201   36   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   contemporaryrev10unkngoog_480   37   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   pt3historyofirel00wriguoft_306   38   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   historyirelanda00smilgoog_120   39   0.4473424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-6   memoirsirgeorge00creigoog_79   40   0.4473424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
