###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 11, non-zero: 11, borderline: 9, overall act: 5.791, act diff: 3.791, ratio: 0.655
#   pulse 2: activated: 264012, non-zero: 264012, borderline: 264007, overall act: 38702.732, act diff: 38696.941, ratio: 1.000
#   pulse 3: activated: 264018, non-zero: 264018, borderline: 264011, overall act: 38704.249, act diff: 1.517, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264018
#   final overall activation: 38704.2
#   number of spread. activ. pulses: 3
#   running time: 63389

###################################
# top k results in TREC format: 

2   rel2-3   1641   1   0.5120209   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-3   1845   2   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-3   1640   3   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-3   1642   4   0.2505602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-3   1846   5   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel2-3   1844   6   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel2-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   ireland   3   0.8449849   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   englandsfightwit00wals_355   4   0.63972855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_601   5   0.6246651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   hallamsworks04halliala_599   6   0.59106064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_291   7   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_411   8   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_130   9   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   fastiecclesiaeh02cottgoog_20   10   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   compendiumofhist02lawl_76   11   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   englishinireland03frou_309   12   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   englishinireland03frou_118   13   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_345   14   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   calendarstatepa12offigoog_335   15   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_413   16   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   economichistoryo00obri_321   17   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   statechurcheskin00allerich_587   18   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   memoirsandcorre02castgoog_42   19   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   revolutionaryir00mahagoog_100   20   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   historymoderneu11russgoog_386   21   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   historyofdiocese02healiala_23   22   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   irishtangleandwa1920john_53   23   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   constitutionalhi03hall_414   24   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   historyofireland3_00will_307   25   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   proceedingsofro22roya_307   26   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   cu31924091770861_319   27   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   cu31924091770861_286   28   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   cu31924091786628_78   29   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   castlesofireland00adamiala_301   30   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   confederationofk00meeh_10   31   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   treatiseofexcheq02howa_421   32   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   1641   33   0.5120209   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   politicalstudies00broduoft_370   34   0.50727487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   politicalstudies00broduoft_382   35   0.50290865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_249   36   0.49564424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   historyirelanda00smilgoog_120   37   0.49394864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   cu31924091770861_873   38   0.49394864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   causeofirelandpl00orei_315   39   0.49184215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel2-3   irelandsaintpatr00morriala_17   40   0.49091214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
