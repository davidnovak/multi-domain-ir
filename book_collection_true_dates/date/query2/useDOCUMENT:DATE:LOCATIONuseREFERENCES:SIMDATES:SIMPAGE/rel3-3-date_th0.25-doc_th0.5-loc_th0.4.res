###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1015991, non-zero: 1015991, borderline: 1015984, overall act: 141071.109, act diff: 141062.651, ratio: 1.000
#   pulse 3: activated: 1023772, non-zero: 1023772, borderline: 1023538, overall act: 142001.255, act diff: 930.146, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1023772
#   final overall activation: 142001.3
#   number of spread. activ. pulses: 3
#   running time: 220334

###################################
# top k results in TREC format: 

2   rel3-3   1845   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1640   3   0.6917996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1688   4   0.42638123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1642   5   0.3397891   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1649   6   0.33102444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1667   7   0.3046173   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1630   8   0.2832046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1846   9   0.23842366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1632   10   0.2119116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1629   11   0.2119116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1670   12   0.19011897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1819   13   0.17529237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1695   14   0.17529237   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1782   15   0.17340614   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1647   16   0.1705421   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1181   17   0.16728532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1041   18   0.15976365   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1725   19   0.15968692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-3   1652   20   0.15927456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-3   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   dublin   8   0.99841094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   scotland   9   0.9005551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   london   10   0.8687772   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   strafford   11   0.77877206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_415   12   0.7650973   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_431   13   0.7561911   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_414   14   0.74639887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_612   15   0.7416121   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_432   16   0.7246256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   france   17   0.71973217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_613   18   0.7140476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_601   19   0.69860685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_602   20   0.697475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   1640   21   0.6917996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_381   22   0.69039124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   historyforreadyr03larnuoft_236   23   0.6862901   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englishlawirisht00gibbrich_20   24   0.6858451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   viceroysofirelan00omahuoft_96   25   0.682079   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_355   26   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englishlawirisht00gibbrich_19   27   0.6736256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englishhistoryfo00higgrich_364   28   0.669815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   englishhistoryfo00higgrich_365   29   0.6676571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   cu31924091770861_892   30   0.66576254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_430   31   0.662846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_357   32   0.65807384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   spain   33   0.65633446   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_66   34   0.6430885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_68   35   0.6416897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   calendarstatepa00levagoog_360   36   0.64062184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   revolutionaryir00mahagoog_99   37   0.6371462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_352   38   0.6333924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   cu31924091786628_79   39   0.63332486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-3   irelandsaintpatr00morriala_200   40   0.63147676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
