###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 10.124, act diff: 7.124, ratio: 0.704
#   pulse 2: activated: 879910, non-zero: 879910, borderline: 879902, overall act: 112384.017, act diff: 112373.893, ratio: 1.000
#   pulse 3: activated: 888709, non-zero: 888709, borderline: 888459, overall act: 113506.300, act diff: 1122.282, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 888709
#   final overall activation: 113506.3
#   number of spread. activ. pulses: 3
#   running time: 231906

###################################
# top k results in TREC format: 

2   rel3-2   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1845   2   0.87752324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1688   3   0.7616524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1640   4   0.632059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1642   5   0.54981124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1798   6   0.522807   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1690   7   0.51359   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1643   8   0.43482366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1870   9   0.40407145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1689   10   0.39537784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1654   11   0.32773793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1664   12   0.31199926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1649   13   0.29764962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1637   14   0.28620204   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1652   15   0.28508785   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1844   16   0.28385898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1639   17   0.2585572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1648   18   0.2489335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1846   19   0.23426333   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-2   1701   20   0.23176718   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   england   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   cu31924029563875_157   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   dublin   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   london   8   0.9999967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   scotland   9   0.9513325   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   1845   10   0.87752324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   strafford   11   0.87671596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   munster   12   0.8289094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   fairfax   13   0.8081174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_432   14   0.76673603   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   limerick   15   0.76274955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   1688   16   0.7616524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   constitutionalhi03hall_414   17   0.75478363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   historyforreadyr03larnuoft_236   18   0.7403792   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   constitutionalhi03hall_415   19   0.7388591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_431   20   0.72212327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   compendiumofhist02lawl_75   21   0.72132546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   historyforreadyr03larnuoft_235   22   0.71752936   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   historyirishper01maddgoog_228   23   0.71049577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   bibliothecagrenv03grenrich_89   24   0.7052175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   calendarstatepa12offigoog_372   25   0.6982883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   cu31924091770861_892   26   0.69259685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_601   27   0.68934315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   bibliothecagrenv03grenrich_90   28   0.67472464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   englandsfightwit00wals_355   29   0.6719688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   revolutionaryir00mahagoog_145   30   0.6709003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   politicalstudies00broduoft_381   31   0.6655252   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   derry   32   0.6593188   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_599   33   0.65829563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   landwarinireland00godk_232   34   0.6519951   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   historicalreview02plow_30   35   0.64999264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   irelandbookoflig00reiduoft_150   36   0.64921826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   revolutionaryir00mahagoog_99   37   0.6482439   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   calendarstatepa12offigoog_636   38   0.6471653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_602   39   0.64495826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-2   revisedreportsb12courgoog_142   40   0.64344734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
