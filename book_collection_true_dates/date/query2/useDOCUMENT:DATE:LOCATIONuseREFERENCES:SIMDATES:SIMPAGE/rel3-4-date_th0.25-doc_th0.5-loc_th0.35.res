###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 28, non-zero: 28, borderline: 25, overall act: 10.441, act diff: 7.441, ratio: 0.713
#   pulse 2: activated: 1512025, non-zero: 1512025, borderline: 1512016, overall act: 203906.054, act diff: 203895.613, ratio: 1.000
#   pulse 3: activated: 1512496, non-zero: 1512496, borderline: 1512261, overall act: 204070.892, act diff: 164.838, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512496
#   final overall activation: 204070.9
#   number of spread. activ. pulses: 3
#   running time: 367153

###################################
# top k results in TREC format: 

2   rel3-4   1641   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1845   2   0.99809504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1689   3   0.86904615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1688   4   0.8032465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1691   5   0.7872588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1885   6   0.7103773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1804   7   0.58380115   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1898   8   0.5607454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1897   9   0.5405758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1873   10   0.52112216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1642   11   0.48936862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1903   12   0.48805496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1870   13   0.4811802   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1844   14   0.4543712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1877   15   0.44066635   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1893   16   0.4317892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1829   17   0.40868673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1890   18   0.40761375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1798   19   0.40622163   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-4   1664   20   0.4047451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   london   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   ireland   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1845   9   0.99809504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   scotland   10   0.978497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1689   11   0.86904615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   strafford   12   0.81961566   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1688   13   0.8032465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_465   14   0.7930075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1691   15   0.7872588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   cu31924024892881_569   16   0.7765714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   limerick   17   0.76061654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_463   18   0.75049627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_236   19   0.734777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_431   20   0.7291617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_432   21   0.72239256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_601   22   0.7221464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_415   23   0.7205244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_96   24   0.71851975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_235   25   0.7133091   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   1885   26   0.7103773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_90   27   0.70811105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_414   28   0.6976935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   cambridgemodhist10actouoft_892   29   0.6931685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_464   30   0.6912114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_461   31   0.68957025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_99   32   0.68869746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_89   33   0.6871948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_100   34   0.6861739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_462   35   0.68252575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   cambridgemodhist10actouoft_891   36   0.67975795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   publications13irisuoft_17   37   0.67660004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   strangerinirelan00carr_59   38   0.6742322   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_613   39   0.673916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_466   40   0.66874444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
