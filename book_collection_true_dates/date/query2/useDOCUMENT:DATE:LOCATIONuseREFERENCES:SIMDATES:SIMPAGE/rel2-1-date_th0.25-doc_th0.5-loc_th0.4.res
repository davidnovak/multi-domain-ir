###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 20, overall act: 7.070, act diff: 5.070, ratio: 0.717
#   pulse 2: activated: 227907, non-zero: 227907, borderline: 227903, overall act: 36595.517, act diff: 36588.447, ratio: 1.000
#   pulse 3: activated: 227911, non-zero: 227911, borderline: 227906, overall act: 36596.307, act diff: 0.789, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 227911
#   final overall activation: 36596.3
#   number of spread. activ. pulses: 3
#   running time: 55061

###################################
# top k results in TREC format: 

2   rel2-1   1641   1   0.39411858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1640   2   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1642   3   0.19454753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1701   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1699   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1690   6   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1689   7   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1651   8   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-1   1643   9   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   englandsfightwit00wals_354   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   ireland   3   0.77917707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   englandsfightwit00wals_355   4   0.644176   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   causeofirelandpl00orei_291   5   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   treatiseofexcheq02howa_411   6   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   treatiseofexcheq02howa_130   7   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   fastiecclesiaeh02cottgoog_20   8   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   compendiumofhist02lawl_76   9   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   causeofirelandpl00orei_345   10   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   calendarstatepa12offigoog_335   11   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   treatiseofexcheq02howa_413   12   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924091786628_78   13   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   castlesofireland00adamiala_301   14   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   confederationofk00meeh_10   15   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   englishinireland03frou_309   16   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   englishinireland03frou_118   17   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   economichistoryo00obri_321   18   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   statechurcheskin00allerich_587   19   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   revolutionaryir00mahagoog_100   20   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   historymoderneu11russgoog_386   21   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   memoirsandcorre02castgoog_42   22   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   historyofdiocese02healiala_23   23   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   irishtangleandwa1920john_53   24   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   constitutionalhi03hall_414   25   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   historyofireland3_00will_307   26   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   proceedingsofro22roya_307   27   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924091770861_319   28   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924091770861_286   29   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   treatiseofexcheq02howa_421   30   0.4807475   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   politicalstudies00broduoft_370   31   0.46916416   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   politicalstudies00broduoft_382   32   0.46444523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924029563875_158   33   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924029563875_156   34   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   englandsfightwit00wals_353   35   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   causeofirelandpl00orei_249   36   0.45659715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   historyirelanda00smilgoog_120   37   0.45476583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   cu31924091770861_873   38   0.45476583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   causeofirelandpl00orei_315   39   0.45249104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-1   irelandsaintpatr00morriala_17   40   0.4514868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
