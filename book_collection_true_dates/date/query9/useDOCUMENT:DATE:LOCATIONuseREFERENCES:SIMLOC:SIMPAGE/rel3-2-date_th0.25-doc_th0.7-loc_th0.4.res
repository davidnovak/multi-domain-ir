###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 76, non-zero: 76, borderline: 73, overall act: 16.052, act diff: 13.052, ratio: 0.813
#   pulse 2: activated: 124115, non-zero: 124115, borderline: 124109, overall act: 15435.265, act diff: 15419.214, ratio: 0.999
#   pulse 3: activated: 124132, non-zero: 124132, borderline: 124125, overall act: 15438.507, act diff: 3.241, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 124132
#   final overall activation: 15438.5
#   number of spread. activ. pulses: 3
#   running time: 71895

###################################
# top k results in TREC format: 

9   rel3-2   1863   1   0.27309436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1862   2   0.27309436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1852   3   0.27309436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1855   4   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1851   5   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1859   6   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1857   7   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1866   8   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1860   9   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1869   10   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1861   11   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1864   12   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1865   13   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1856   14   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1858   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1850   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1853   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1854   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1848   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
9   rel3-2   1849   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 9   rel3-2   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   encyclopediabrit03newyrich_155   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924082142666_16   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   austria   4   0.7283202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   encyclopediabrit03newyrich_154   5   0.7047888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   prussia   6   0.66289884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924082142666_15   7   0.65201825   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   encyclopediabrit03newyrich_153   8   0.6417255   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924082142666_17   9   0.63447076   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   encyclopediabrit03newyrich_156   10   0.5887098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   saxony   11   0.51025385   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924014592566_748   12   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924014592566_750   13   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   lifetimesofstein02seeluoft_259   14   0.44077384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   holstein   15   0.43724164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   ourchancellorske00buscuoft_403   16   0.43216068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   foundationsofger00barkuoft_97   17   0.43139943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   journalpolitica02socigoog_11   18   0.42867306   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924088053800_143   19   0.42857257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   foundationsofger00barkuoft_248   20   0.42854428   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   menwhohavemaden00stragoog_155   21   0.42679802   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   threegermanysgl00faygoog_392   22   0.42219427   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   outlineseuropea04beargoog_378   23   0.42109182   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   threegermanysgl00faygoog_397   24   0.42103443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   foundationsofger00barkuoft_95   25   0.42039192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   europe1789192000turnrich_277   26   0.418939   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   marvelousstoryof00nort_227   27   0.41795135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   shorthistoryofeu00terr_454   28   0.4173598   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   threegermanysgl00faygoog_358   29   0.41639733   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   foundationsofger00barkuoft_355   30   0.4163128   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   foundationsofger00barkuoft_359   31   0.4161592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924014592566_291   32   0.41568854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   travelsinnorthg00dwiggoog_356   33   0.4120073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924014592566_362   34   0.4112486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   ourchancellorske00buscuoft_354   35   0.41015857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924082142666_334   36   0.40823767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   politicalliferi05stapgoog_26   37   0.40781617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   europe1789192000turnrich_275   38   0.4074076   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   commentariesupon01philuoft_251   39   0.40706715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 9   rel3-2   cu31924082142666_25   40   0.40328336   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
