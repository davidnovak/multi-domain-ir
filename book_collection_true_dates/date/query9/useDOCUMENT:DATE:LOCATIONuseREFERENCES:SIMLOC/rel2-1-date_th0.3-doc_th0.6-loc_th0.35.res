###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 67, non-zero: 67, borderline: 65, overall act: 10.719, act diff: 8.719, ratio: 0.813
#   pulse 2: activated: 114014, non-zero: 114014, borderline: 114009, overall act: 10327.466, act diff: 10316.747, ratio: 0.999
#   pulse 3: activated: 114014, non-zero: 114014, borderline: 114009, overall act: 10327.466, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 114014
#   final overall activation: 10327.5
#   number of spread. activ. pulses: 3
#   running time: 60541

###################################
# top k results in TREC format: 

9   rel2-1   1855   1   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1853   2   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1851   3   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1859   4   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1857   5   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1866   6   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1864   7   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1862   8   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1860   9   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1869   10   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1861   11   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1863   12   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1865   13   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1856   14   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1858   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1850   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1852   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1854   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1848   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
9   rel2-1   1849   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   austria   3   0.44222224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   elbe   4   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   prussia   5   0.3878032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   marvelousstoryof00nort_227   6   0.31701896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924014592566_362   7   0.3108315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   bohemia   8   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   saxony   9   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924082142666_237   10   0.30218443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_364   11   0.29823935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_352   12   0.29680276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_365   13   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_32   14   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   serbiaeurope00mark_142   15   0.29496095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_96   16   0.29373276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofeuropef00holt_116   17   0.29215991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924082142666_119   18   0.29208982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofcontemp00seiguoft_295   19   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   historyofcontemp00seigrich_299   20   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   europe1789192000turnrich_282   21   0.2903725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   shorthistoryofeu00terr_465   22   0.289929   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   universalantho25garn_415   23   0.289677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   papersrelatingt19statgoog_84   24   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   papersrelatingt04unkngoog_90   25   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   reckoningdiscuss00beckrich_146   26   0.28733686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924031684685_715   27   0.28570086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_357   28   0.2851643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   silesia   29   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_37   30   0.28468382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   shorthistoryofeu00terr_459   31   0.28458318   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_301   32   0.28441188   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   outlineseuropea04beargoog_378   33   0.28393888   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   foundationsofger00barkuoft_248   34   0.28354967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   storygreatwar04ruhlgoog_345   35   0.28330514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   briefhistoryofna00fish_538   36   0.28262043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   cu31924082142666_37   37   0.28232694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_311   38   0.28220978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_359   39   0.28220978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 9   rel2-1   polishexperienc02hallgoog_307   40   0.28220978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
