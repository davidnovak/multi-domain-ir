###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 55, non-zero: 55, borderline: 53, overall act: 9.019, act diff: 7.019, ratio: 0.778
#   pulse 2: activated: 75192, non-zero: 75192, borderline: 75189, overall act: 5894.884, act diff: 5885.865, ratio: 0.998
#   pulse 3: activated: 75192, non-zero: 75192, borderline: 75189, overall act: 5894.884, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75192
#   final overall activation: 5894.9
#   number of spread. activ. pulses: 3
#   running time: 34543

###################################
# top k results in TREC format: 

9   rel2-3   1855   1   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1853   2   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1851   3   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1859   4   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1857   5   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1866   6   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1864   7   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1862   8   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1860   9   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1869   10   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1861   11   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1863   12   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1865   13   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1856   14   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1858   15   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1850   16   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1852   17   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1854   18   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1848   19   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
9   rel2-3   1849   20   0.10363388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 9   rel2-3   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   austria   3   0.4236283   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   prussia   4   0.35679588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   saxony   5   0.32253665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   hanover   6   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   elbe   7   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924060379777_226   8   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_244   9   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924060379777_250   10   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cavourmakingofmo00orsi_167   11   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924031236031_350   12   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924031236031_358   13   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_232   14   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_226   15   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_256   16   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   secretmemoirsco01orlgoog_446   17   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   newscuttings00gran_30   18   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924027975733_88   19   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   journalseriesage65royauoft_442   20   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   storiesseatoldb00unkngoog_94   21   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924060379777_343   22   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   historyofpopesth00rankuoft_513   23   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924031236031_360   24   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924031236031_366   25   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   catholicsocialis00nittiala_260   26   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924082163829_160   27   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_82   28   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   schoolgardenbein00schw_90   29   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   memoirscountess02genlgoog_44   30   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   generalarmoryofe00burk_1337   31   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924027758022_392   32   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924082163829_142   33   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   cu31924031296340_439   34   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   autobiographycor01deweuoft_203   35   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   electricfurnace01baurgoog_20   36   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   firsteleventheig1871mass_323   37   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   royalfavourites01stongoog_11   38   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   firsteleventheig1871mass_353   39   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 9   rel2-3   familysaveallsup00bouv_586   40   0.20870228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
