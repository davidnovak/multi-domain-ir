###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 25, non-zero: 25, borderline: 24, overall act: 4.826, act diff: 3.826, ratio: 0.793
#   pulse 2: activated: 207441, non-zero: 207441, borderline: 207439, overall act: 18178.564, act diff: 18173.738, ratio: 1.000
#   pulse 3: activated: 207441, non-zero: 207441, borderline: 207439, overall act: 18178.564, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207441
#   final overall activation: 18178.6
#   number of spread. activ. pulses: 3
#   running time: 51243

###################################
# top k results in TREC format: 

10   rel1-4   1848   1   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1854   2   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1860   3   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1946   4   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1850   5   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1847   6   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1849   7   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
10   rel1-4   1844   8   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel1-4   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   policepeaceoffic19461946sanf_80   2   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   policepeaceoffic19461946sanf_78   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   california   4   0.370601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_479   5   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   forestryinmining00browrich_1   6   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   gt4waterpowerfromge190calirich_43   7   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   herberthooverman00lckell_55   8   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   lampshield1963stan_49   9   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   mindsandmanners00horngoog_239   10   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   typologyscriptu02fairgoog_607   11   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   travelsathome00twaigoog_136   12   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_461   13   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_565   14   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_563   15   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_559   16   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_557   17   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_555   18   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   flavouringmateri00clarrich_186   19   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_553   20   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_551   21   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_561   22   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_415   23   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   pacificcoastvaca00morrrich_172   24   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_934   25   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   commercialeduca00unkngoog_2   26   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_961   27   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   bulletinofmuseum124harv_324   28   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_528   29   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   ineztaleofalamo00evaniala_4   30   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   birdlore24nati_162   31   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_549   32   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_921   33   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_440   34   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_919   35   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_917   36   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_454   37   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_909   38   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   birdlore24nati_103   39   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_466   40   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
