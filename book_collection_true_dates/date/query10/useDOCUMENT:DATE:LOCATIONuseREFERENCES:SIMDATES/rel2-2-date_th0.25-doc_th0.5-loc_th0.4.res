###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   sanfranciscohist01youn_192, 1
#   ingersollscentur00inge_101, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 4.911, act diff: 2.911, ratio: 0.593
#   pulse 2: activated: 281221, non-zero: 281221, borderline: 281216, overall act: 37260.518, act diff: 37255.607, ratio: 1.000
#   pulse 3: activated: 281221, non-zero: 281221, borderline: 281216, overall act: 40310.950, act diff: 3050.432, ratio: 0.076
#   pulse 4: activated: 344136, non-zero: 344136, borderline: 344093, overall act: 46892.038, act diff: 6581.088, ratio: 0.140
#   pulse 5: activated: 397858, non-zero: 397858, borderline: 397793, overall act: 81427.616, act diff: 34535.578, ratio: 0.424
#   pulse 6: activated: 1080029, non-zero: 1080029, borderline: 1077390, overall act: 232042.103, act diff: 150614.486, ratio: 0.649
#   pulse 7: activated: 7513909, non-zero: 7513909, borderline: 7499982, overall act: 3879309.664, act diff: 3647267.563, ratio: 0.940
#   pulse 8: activated: 8486130, non-zero: 8486130, borderline: 5077304, overall act: 4955265.297, act diff: 1075955.633, ratio: 0.217
#   pulse 9: activated: 9556350, non-zero: 9556350, borderline: 4819848, overall act: 6412001.424, act diff: 1456736.129, ratio: 0.227
#   pulse 10: activated: 9622097, non-zero: 9622097, borderline: 2986682, overall act: 6470678.628, act diff: 58677.204, ratio: 0.009

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9622097
#   final overall activation: 6470678.6
#   number of spread. activ. pulses: 10
#   running time: 2041293

###################################
# top k results in TREC format: 

10   rel2-2   709   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   774   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   750   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   725   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1010   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1011   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1000   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   958   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   775   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1022   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1009   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1001   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1002   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1003   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1004   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1005   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1006   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1007   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   1008   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
10   rel2-2   625   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 10   rel2-2   milton of balgonie   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   bapaume   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   victoria bridge   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   haverstraw bay   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   montreal   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   terra firma   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   betsey   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   south oxford   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   nederland   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   sandpoint   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   mchenry county   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   embarcadero   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   point barrow   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   bettws   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   holy land   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   pangbourne   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   montreux   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   ecorse   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   montreat   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   ellesmere   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   southern europe   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   cyrenaica   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   ecouen   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   point pelee   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   north egremont   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   poynton   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   kittery   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   lincoln square   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   morro bay   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   merrymeeting bay   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   new castle county   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   bidford   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   lidiana   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   bethel   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   betham   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   warburg   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   hawthornden   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   shillong   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   calatayud   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 10   rel2-2   new lenox   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
