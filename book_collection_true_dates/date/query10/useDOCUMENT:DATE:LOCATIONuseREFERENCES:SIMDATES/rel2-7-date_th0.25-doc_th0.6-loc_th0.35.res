###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   reportofbritisha88brit_635, 1
#   waterqualityreso00sacr_128, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 79, non-zero: 79, borderline: 77, overall act: 11.384, act diff: 9.384, ratio: 0.824
#   pulse 2: activated: 207494, non-zero: 207494, borderline: 207491, overall act: 23022.709, act diff: 23011.325, ratio: 1.000
#   pulse 3: activated: 207494, non-zero: 207494, borderline: 207491, overall act: 23022.709, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207494
#   final overall activation: 23022.7
#   number of spread. activ. pulses: 3
#   running time: 76523

###################################
# top k results in TREC format: 

10   rel2-7   1859   1   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1857   2   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1863   3   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1860   4   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1884   5   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1885   6   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1887   7   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1880   8   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1858   9   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1852   10   0.20313847   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1866   11   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1862   12   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1868   13   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1867   14   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1869   15   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1861   16   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1864   17   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1865   18   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1856   19   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-7   1851   20   0.08588561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel2-7   reportofbritisha88brit_635   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   waterqualityreso00sacr_128   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   california   3   0.47064278   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   los vaqueros   4   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   buildingindustri11111cont_479   5   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   forestryinmining00browrich_1   6   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   gt4waterpowerfromge190calirich_43   7   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   herberthooverman00lckell_55   8   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   lampshield1963stan_49   9   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   mindsandmanners00horngoog_239   10   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   typologyscriptu02fairgoog_607   11   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   travelsathome00twaigoog_136   12   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   buildingindustri11111cont_461   13   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_565   14   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_563   15   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_559   16   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_557   17   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_555   18   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   flavouringmateri00clarrich_186   19   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_553   20   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_551   21   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_561   22   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   buildingindustri11111cont_415   23   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   pacificcoastvaca00morrrich_172   24   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs2931_934   25   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   commercialeduca00unkngoog_2   26   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs2931_961   27   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   bulletinofmuseum124harv_324   28   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   miltonmarksoral01markrich_528   29   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   ineztaleofalamo00evaniala_4   30   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   birdlore24nati_162   31   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   minutesofgeneral1915pres_549   32   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs3383_921   33   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   miltonmarksoral01markrich_440   34   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs3383_919   35   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs3383_917   36   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   miltonmarksoral01markrich_454   37   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   govuscourtsca9briefs3383_909   38   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   birdlore24nati_103   39   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-7   miltonmarksoral01markrich_466   40   0.23107177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
