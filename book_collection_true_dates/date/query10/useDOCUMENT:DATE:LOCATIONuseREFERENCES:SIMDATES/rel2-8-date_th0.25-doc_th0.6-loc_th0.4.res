###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   ingersollscentur00inge_101, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 26, non-zero: 26, borderline: 24, overall act: 6.178, act diff: 4.178, ratio: 0.676
#   pulse 2: activated: 335438, non-zero: 335438, borderline: 335432, overall act: 45416.857, act diff: 45410.680, ratio: 1.000
#   pulse 3: activated: 397811, non-zero: 397811, borderline: 397803, overall act: 54302.661, act diff: 8885.804, ratio: 0.164
#   pulse 4: activated: 397811, non-zero: 397811, borderline: 397802, overall act: 58647.227, act diff: 4344.566, ratio: 0.074
#   pulse 5: activated: 397819, non-zero: 397819, borderline: 397808, overall act: 72482.941, act diff: 13835.714, ratio: 0.191
#   pulse 6: activated: 423648, non-zero: 423648, borderline: 423454, overall act: 89282.212, act diff: 16799.271, ratio: 0.188
#   pulse 7: activated: 2749318, non-zero: 2749318, borderline: 2748370, overall act: 701902.081, act diff: 612619.869, ratio: 0.873
#   pulse 8: activated: 6155271, non-zero: 6155271, borderline: 6107507, overall act: 2630379.178, act diff: 1928477.097, ratio: 0.733
#   pulse 9: activated: 8911875, non-zero: 8911875, borderline: 7761008, overall act: 5628774.577, act diff: 2998395.400, ratio: 0.533
#   pulse 10: activated: 9495570, non-zero: 9495570, borderline: 5079159, overall act: 6329072.324, act diff: 700297.748, ratio: 0.111

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9495570
#   final overall activation: 6329072.3
#   number of spread. activ. pulses: 10
#   running time: 1606278

###################################
# top k results in TREC format: 

10   rel2-8   1000   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1008   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1010   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1011   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1003   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1004   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1005   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1006   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1007   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1016   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1017   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1018   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1019   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1020   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1021   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1022   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1009   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1001   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   1002   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-8   958   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 10   rel2-8   haverstraw bay   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   nederland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   bapaume   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   victoria bridge   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   montreat   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   montreal   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   terra firma   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   betsey   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   south oxford   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   warburg   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   hawthornden   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   shillong   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   calatayud   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   mchenry county   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   embarcadero   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   point barrow   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   holy land   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   montreux   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   ecorse   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   kentville   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   arnold   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   port kent   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   tribunealmanacpo1889newy_52   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   longfield   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   euston   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   lake minnetonka   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   yadkin   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   green cove springs   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   haight   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   ellesmere   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   southern europe   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   cyrenaica   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   point pelee   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   kittery   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   lincoln square   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   morro bay   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   new castle county   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   bethel   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   betham   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-8   milton of balgonie   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.4
