###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 22, overall act: 3.902, act diff: 2.902, ratio: 0.744
#   pulse 2: activated: 207439, non-zero: 207439, borderline: 207437, overall act: 18177.640, act diff: 18173.738, ratio: 1.000
#   pulse 3: activated: 207439, non-zero: 207439, borderline: 207437, overall act: 18177.640, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207439
#   final overall activation: 18177.6
#   number of spread. activ. pulses: 3
#   running time: 47896

###################################
# top k results in TREC format: 

10   rel1-4   1848   1   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1854   2   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1860   3   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1946   4   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1850   5   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1847   6   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1849   7   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
10   rel1-4   1844   8   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 10   rel1-4   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   california   2   0.370601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_479   3   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   forestryinmining00browrich_1   4   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   gt4waterpowerfromge190calirich_43   5   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   herberthooverman00lckell_55   6   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   lampshield1963stan_49   7   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   mindsandmanners00horngoog_239   8   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   typologyscriptu02fairgoog_607   9   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   travelsathome00twaigoog_136   10   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_461   11   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_565   12   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_563   13   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_559   14   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_557   15   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_555   16   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   flavouringmateri00clarrich_186   17   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_553   18   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_551   19   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_561   20   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_415   21   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   pacificcoastvaca00morrrich_172   22   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_934   23   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   commercialeduca00unkngoog_2   24   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_961   25   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   bulletinofmuseum124harv_324   26   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_528   27   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   ineztaleofalamo00evaniala_4   28   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   birdlore24nati_162   29   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_549   30   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_921   31   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_440   32   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_919   33   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_917   34   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_454   35   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_909   36   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   birdlore24nati_103   37   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_466   38   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   flavouringmateri00clarrich_177   39   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_567   40   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
