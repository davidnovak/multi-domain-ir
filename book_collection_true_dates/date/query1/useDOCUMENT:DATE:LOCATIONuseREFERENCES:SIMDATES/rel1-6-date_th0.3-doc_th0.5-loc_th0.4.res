###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   newannualarmylis1874hart_347, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 51, overall act: 6.453, act diff: 5.453, ratio: 0.845
#   pulse 2: activated: 52, non-zero: 52, borderline: 51, overall act: 6.453, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 52
#   final overall activation: 6.5
#   number of spread. activ. pulses: 2
#   running time: 18292

###################################
# top k results in TREC format: 

1   rel1-6   1859   1   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1857   2   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1861   3   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1824   4   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1838   5   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1849   6   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1847   7   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1845   8   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1846   9   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1848   10   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1844   11   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1839   12   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1860   13   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1863   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1858   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1854   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
1   rel1-6   1855   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 1   rel1-6   newannualarmylis1874hart_347   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   sebastopol   2   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   canton   3   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   china   4   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   crimea   5   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   heights   6   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   delhi   7   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   india   8   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   lucknow   9   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   punjaub   10   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1859   11   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1854   12   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1863   13   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1839   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1844   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1838   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1860   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1824   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1861   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1845   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1846   21   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1847   22   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1848   23   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1858   24   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1849   25   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1857   26   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   1855   27   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   murree   28   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   new zealand   29   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   nankin   30   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   pekin   31   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   gulf of finland   32   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   bomarsund   33   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   rat island   34   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   chusan   35   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   malabar   36   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   north   37   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   parana   38   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   matale   39   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
# 1   rel1-6   canada   40   0.08707067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.4
