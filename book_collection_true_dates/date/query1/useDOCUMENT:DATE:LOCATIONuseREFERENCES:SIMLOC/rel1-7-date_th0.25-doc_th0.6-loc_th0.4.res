###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   newannualarmylis1874hart_481, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 26, overall act: 4.294, act diff: 3.294, ratio: 0.767
#   pulse 2: activated: 27, non-zero: 27, borderline: 26, overall act: 4.294, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 27
#   final overall activation: 4.3
#   number of spread. activ. pulses: 2
#   running time: 5351

###################################
# top k results in TREC format: 

1   rel1-7   1858   1   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1865   2   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1863   3   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1861   4   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1860   5   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1862   6   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1764   7   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1857   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1859   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
1   rel1-7   1851   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 1   rel1-7   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   delhi   2   0.2888946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   agra   3   0.2507578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   lucknow   4   0.22617008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   terai   5   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   chingli   6   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   meerut   7   0.15582155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1859   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1858   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1857   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1865   11   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1863   12   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1861   13   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1860   14   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1862   15   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1764   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   1851   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   bareilly   18   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   rooya   19   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   garrison   20   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   sikkim   21   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   gwalior   22   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   hissar   23   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   narmada   24   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   kirwee   25   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   arrah   26   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel1-7   cawnpore   27   0.09879659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
