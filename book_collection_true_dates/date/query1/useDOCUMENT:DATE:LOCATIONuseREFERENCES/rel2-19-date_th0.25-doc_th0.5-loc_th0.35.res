###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_466, 1
#   newannualarmylis1874hart_464, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 54, non-zero: 54, borderline: 52, overall act: 9.813, act diff: 7.813, ratio: 0.796
#   pulse 2: activated: 14467, non-zero: 14467, borderline: 14462, overall act: 1402.853, act diff: 1393.040, ratio: 0.993
#   pulse 3: activated: 14467, non-zero: 14467, borderline: 14462, overall act: 1402.853, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 14467
#   final overall activation: 1402.9
#   number of spread. activ. pulses: 3
#   running time: 17562

###################################
# top k results in TREC format: 

1   rel2-19   1849   1   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1846   2   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1855   3   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1857   4   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1860   5   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1858   6   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1859   7   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1848   8   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1843   9   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1866   10   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1865   11   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1854   12   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1852   13   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1853   14   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1845   15   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1847   16   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1841   17   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1840   18   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1842   19   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-19   1838   20   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel2-19   newannualarmylis1874hart_464   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   gwalior   3   0.47376928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhi   4   0.42501354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   lucknow   5   0.3599618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_256   6   0.33788657   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_257   7   0.3234301   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_498   8   0.31869012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   encyclopaediabri14chisrich_499   9   0.30450222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   cu31924023968237_406   10   0.30399755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_143   11   0.29810953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   cu31924023968237_403   12   0.29553708   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_453   13   0.29476425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_174   14   0.29308417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_173   15   0.2894521   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_258   16   0.28233922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_409   17   0.28199717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   historyofourownt03mccaiala_20   18   0.2814687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_89   19   0.27923292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   historyofindianm01forr_199   20   0.27903697   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_119   21   0.2776482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   ourgreatvassalem00bell_89   22   0.27617308   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_110   23   0.27617308   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   cu31924023252962_417   24   0.27614358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   mutinyofbengalar00mallrich_194   25   0.27570617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_280   26   0.27320623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   aroundworld00hend_265   27   0.2704733   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_279   28   0.26897678   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_126   29   0.26703578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_255   30   0.2668343   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_202   31   0.2666226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   cu31924023968237_411   32   0.26511735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   hartsannualarmy17hartgoog_211   33   0.26342422   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_390   34   0.2632257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   punjaub   35   0.26285502   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1874hart_461   36   0.26256973   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   redyearstoryofin00traciala_293   37   0.26219735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   newannualarmylis1898lond_635   38   0.26117417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   delhisiegeassau00youngoog_161   39   0.26112935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-19   historyofourownt03mccaiala_19   40   0.258396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
