###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_466, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 52, non-zero: 52, borderline: 50, overall act: 9.282, act diff: 7.282, ratio: 0.785
#   pulse 2: activated: 10559, non-zero: 10559, borderline: 10554, overall act: 1065.606, act diff: 1056.324, ratio: 0.991
#   pulse 3: activated: 10559, non-zero: 10559, borderline: 10554, overall act: 1065.606, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10559
#   final overall activation: 1065.6
#   number of spread. activ. pulses: 3
#   running time: 15879

###################################
# top k results in TREC format: 

1   rel2-9   1854   1   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1858   2   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1865   3   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1860   4   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1866   5   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1857   6   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1859   7   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1855   8   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1868   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1862   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1863   11   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1848   12   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1852   13   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1853   14   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1845   15   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1846   16   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1849   17   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
1   rel2-9   1843   18   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel2-9   newannualarmylis1874hart_466   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   lucknow   3   0.45171002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   gwalior   4   0.40666384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   agra   5   0.37584528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   hartsannualarmy17hartgoog_256   6   0.31037366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_341   7   0.28351903   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_179   8   0.28230762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   encyclopaediabri14chisrich_499   9   0.2820211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   hartsannualarmy17hartgoog_181   10   0.278024   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   winterindia01scid_204   11   0.2751685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   historyofsepoywa00kaye_473   12   0.2751685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   cu31924023223872_152   13   0.27432287   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_385   14   0.27376547   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   cu31924023252962_432   15   0.27230886   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   mutinyofbengalar00mallrich_195   16   0.27048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   fortyoneyearsini00robe_191   17   0.26828435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   fromindependence00brew_149   18   0.26636508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   historyofchristi02houg_487   19   0.26636508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   hartsannualarmy17hartgoog_257   20   0.26454523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   cu31924023968237_278   21   0.26435682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   newannualarmylis1874hart_174   22   0.26429746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   newannualarmylis1874hart_467   23   0.26362628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   reminiscencesofb00edwauoft_76   24   0.2613007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   storiesaboutfamo00orperich_85   25   0.26089898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   illustrationsofh00thoruoft_480   26   0.26089898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   storiesaboutfam00orpegoog_86   27   0.26089898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   historyofourownt03mccaiala_20   28   0.25879648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_152   29   0.25602758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_221   30   0.25570202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   recollectionsofm1900fayr_200   31   0.25529107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   cu31924031774155_92   32   0.25529107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   tigershootingind00fiferich_276   33   0.25529107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   hartsannualarmy17hartgoog_258   34   0.25508702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   indikacountrypeo00hursuoft_731   35   0.25408167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_246   36   0.25348932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   delhisiegeassau00youngoog_383   37   0.2530147   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   newannualarmylis1874hart_476   38   0.25099063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   generalsirrichar00thor_130   39   0.2506202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
# 1   rel2-9   undersunimpressi00landrich_282   40   0.2506202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.35
