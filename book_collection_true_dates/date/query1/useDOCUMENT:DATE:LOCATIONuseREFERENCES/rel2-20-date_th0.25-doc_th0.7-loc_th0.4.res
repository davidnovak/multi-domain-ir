###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_466, 1
#   newannualarmylis1874hart_347, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 66, non-zero: 66, borderline: 64, overall act: 10.952, act diff: 8.952, ratio: 0.817
#   pulse 2: activated: 66, non-zero: 66, borderline: 64, overall act: 10.952, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 66
#   final overall activation: 11.0
#   number of spread. activ. pulses: 2
#   running time: 19806

###################################
# top k results in TREC format: 

1   rel2-20   1854   1   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1859   2   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1860   3   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1848   4   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1845   5   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1846   6   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1849   7   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1857   8   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1858   9   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1855   10   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1865   11   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1843   12   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1866   13   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1852   14   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1853   15   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1838   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1847   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1844   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1824   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
1   rel2-20   1839   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 1   rel2-20   newannualarmylis1874hart_347   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   delhi   3   0.36994043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   gwalior   4   0.350148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   lucknow   5   0.33964604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   punjaub   6   0.24117683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   sebastopol   7   0.24026829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   canton   8   0.22189257   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1854   9   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1845   10   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1846   11   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1858   12   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1848   13   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1849   14   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1857   15   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1859   16   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1860   17   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1855   18   0.20842858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   camp   19   0.19252771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   cawnpore   20   0.19252771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   china   21   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   crimea   22   0.17283106   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   agra   23   0.16901405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   heights   24   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   india   25   0.13747801   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   sind   26   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   qombovan   27   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   city   28   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   mastha   29   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   pegu   30   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   kakrala   31   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   tharyarwady   32   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1866   33   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1865   34   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1843   35   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1852   36   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1853   37   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1838   38   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1839   39   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
# 1   rel2-20   1861   40   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.4
