###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_347, 1
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 64, non-zero: 64, borderline: 62, overall act: 10.492, act diff: 8.492, ratio: 0.809
#   pulse 2: activated: 5136, non-zero: 5136, borderline: 5133, overall act: 593.196, act diff: 582.704, ratio: 0.982
#   pulse 3: activated: 5136, non-zero: 5136, borderline: 5133, overall act: 593.196, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5136
#   final overall activation: 593.2
#   number of spread. activ. pulses: 3
#   running time: 19199

###################################
# top k results in TREC format: 

1   rel2-27   1847   1   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1846   2   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1859   3   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1858   4   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1848   5   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1845   6   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1855   7   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1857   8   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1849   9   0.22363596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1851   10   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1856   11   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1852   12   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1853   13   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1863   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1861   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1838   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1854   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1844   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1839   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-27   1824   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 1   rel2-27   newannualarmylis1874hart_228   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   newannualarmylis1874hart_347   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   lucknow   3   0.49201566   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   sebastopol   4   0.3365651   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   apersonaljourna00andegoog_121   5   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   systemsoflandte00cobd_226   6   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reportsofmission1896pres_306   7   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   apersonaljourna00andegoog_110   8   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   tennysonhisarta01broogoog_251   9   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   journalofroyalin7188183roya_413   10   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   phoeberowe01thob_185   11   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   narrativemutini00hutcgoog_6   12   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   fortyoneyearsini00robe_316   13   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   lifeofclaudmarti00hill_182   14   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_279   15   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   libraryofworldsbe22warn_51   16   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_267   17   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   memoirofsirwilli00veitrich_344   18   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   newannualarmylis1898lond_946   19   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_211   20   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_234   21   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_231   22   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   reminiscencesofb00edwauoft_230   23   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   passionsanimals00thomgoog_139   24   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   historyofindianm01forr_16   25   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   historyofindianm01forr_14   26   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   historyofindianm01forr_29   27   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   memorialsoflife02edwa_52   28   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   recordsofindianm06indi_263   29   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   macmillansmagazi43macmuoft_261   30   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   memorialsoflife02edwa_49   31   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   livesofindianoff02kayeiala_346   32   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   livesofindianoff02kayeiala_348   33   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   cu31924022927283_266   34   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   cu31924013563865_47   35   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   lifeofisabellath00thobrich_290   36   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   lifeofisabellath00thobrich_286   37   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   troubadourselect00gibbuoft_36   38   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   journaloftravels00wina_252   39   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-27   journaloftravels00wina_251   40   0.24116232   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
