###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_466, 1
#   newannualarmylis1874hart_481, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 47, non-zero: 47, borderline: 45, overall act: 8.757, act diff: 6.757, ratio: 0.772
#   pulse 2: activated: 17705, non-zero: 17705, borderline: 17699, overall act: 2007.506, act diff: 1998.749, ratio: 0.996
#   pulse 3: activated: 17705, non-zero: 17705, borderline: 17699, overall act: 2007.506, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 17705
#   final overall activation: 2007.5
#   number of spread. activ. pulses: 3
#   running time: 12449

###################################
# top k results in TREC format: 

1   rel2-21   1857   1   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1860   2   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1865   3   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1858   4   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1859   5   0.21083225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1851   6   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1861   7   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1862   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1863   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1764   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1846   11   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1866   12   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1855   13   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1853   14   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1852   15   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1854   16   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1845   17   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1848   18   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1849   19   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-21   1843   20   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-21   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   delhi   3   0.49853885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   lucknow   4   0.41818276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_256   5   0.41544905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_257   6   0.4148784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_258   7   0.40498686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   agra   8   0.4027046   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_280   9   0.39633295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   mutinyofbengalar00mallrich_194   10   0.36882898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_176   11   0.36628297   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_203   12   0.3657717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_409   13   0.363063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   gwalior   14   0.36048275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   encyclopaediabri14chisrich_499   15   0.35900465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_479   16   0.35809168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_568   17   0.3505094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   highroadofempire00murrrich_653   18   0.34335458   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_212   19   0.34005398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_202   20   0.33795133   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1898lond_635   21   0.3375872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   delhisiegeassau00youngoog_119   22   0.33732063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1898lond_633   23   0.33677936   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   historyofourownt03mccaiala_20   24   0.33641964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   blackwoodsmagazi134edinuoft_786   25   0.3356454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_174   26   0.33461693   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   hartsannualarmy17hartgoog_126   27   0.33351684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_461   28   0.3329718   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   cu31924023968237_406   29   0.33117852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   fortyoneyearsini00robe_201   30   0.32991454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   historyofsepoywa00kaye_624   31   0.3283099   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   akbargreatmogul100smituoft_54   32   0.32758677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   fortyoneyearsini00robe_20   33   0.32675073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   fortyoneyearsini00robe_191   34   0.3244023   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_472   35   0.3229378   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   delhisiegeassau00youngoog_313   36   0.32255512   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   newannualarmylis1874hart_483   37   0.32219926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   cu31924023968237_403   38   0.32072255   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   cu31924023561933_82   39   0.31839186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-21   narrativeofvisit00fort_266   40   0.31839186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
