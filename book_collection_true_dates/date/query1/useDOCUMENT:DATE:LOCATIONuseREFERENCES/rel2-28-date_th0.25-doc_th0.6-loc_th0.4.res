###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_481, 1
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 45, non-zero: 45, borderline: 43, overall act: 8.330, act diff: 6.330, ratio: 0.760
#   pulse 2: activated: 5117, non-zero: 5117, borderline: 5114, overall act: 668.007, act diff: 659.677, ratio: 0.988
#   pulse 3: activated: 5117, non-zero: 5117, borderline: 5114, overall act: 668.007, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5117
#   final overall activation: 668.0
#   number of spread. activ. pulses: 3
#   running time: 9596

###################################
# top k results in TREC format: 

1   rel2-28   1858   1   0.22602305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1859   2   0.22602305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1857   3   0.22602305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1851   4   0.22602305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1848   5   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1846   6   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1856   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1855   8   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1852   9   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1853   10   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1845   11   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1847   12   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1849   13   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1860   14   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1862   15   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1861   16   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1863   17   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1865   18   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-28   1764   19   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 1   rel2-28   newannualarmylis1874hart_481   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   newannualarmylis1874hart_228   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   lucknow   3   0.55840445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   delhi   4   0.2888946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   apersonaljourna00andegoog_121   5   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   systemsoflandte00cobd_226   6   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reportsofmission1896pres_306   7   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   apersonaljourna00andegoog_110   8   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   tennysonhisarta01broogoog_251   9   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   journalofroyalin7188183roya_413   10   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   phoeberowe01thob_185   11   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   narrativemutini00hutcgoog_6   12   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   fortyoneyearsini00robe_316   13   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   lifeofclaudmarti00hill_182   14   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_279   15   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   libraryofworldsbe22warn_51   16   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_267   17   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   memoirofsirwilli00veitrich_344   18   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   newannualarmylis1898lond_946   19   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_211   20   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_234   21   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_231   22   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   reminiscencesofb00edwauoft_230   23   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   passionsanimals00thomgoog_139   24   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   historyofindianm01forr_16   25   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   historyofindianm01forr_14   26   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   historyofindianm01forr_29   27   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   memorialsoflife02edwa_52   28   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   recordsofindianm06indi_263   29   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   macmillansmagazi43macmuoft_261   30   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   memorialsoflife02edwa_49   31   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   livesofindianoff02kayeiala_346   32   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   livesofindianoff02kayeiala_348   33   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   cu31924022927283_266   34   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   cu31924013563865_47   35   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   lifeofisabellath00thobrich_290   36   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   lifeofisabellath00thobrich_286   37   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   troubadourselect00gibbuoft_36   38   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   journaloftravels00wina_252   39   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-28   journaloftravels00wina_251   40   0.27216655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
