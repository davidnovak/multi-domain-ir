###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_466, 1
#   newannualarmylis1874hart_464, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 54, non-zero: 54, borderline: 52, overall act: 9.813, act diff: 7.813, ratio: 0.796
#   pulse 2: activated: 10459, non-zero: 10459, borderline: 10455, overall act: 976.015, act diff: 966.203, ratio: 0.990
#   pulse 3: activated: 10459, non-zero: 10459, borderline: 10455, overall act: 976.015, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10459
#   final overall activation: 976.0
#   number of spread. activ. pulses: 3
#   running time: 17034

###################################
# top k results in TREC format: 

1   rel2-19   1849   1   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1846   2   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1855   3   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1857   4   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1860   5   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1858   6   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1859   7   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1848   8   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1843   9   0.2034923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1866   10   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1865   11   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1854   12   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1852   13   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1853   14   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1845   15   0.10597362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1847   16   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1841   17   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1840   18   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1842   19   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
1   rel2-19   1838   20   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 1   rel2-19   newannualarmylis1874hart_464   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   newannualarmylis1874hart_466   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   gwalior   3   0.47376928   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhi   4   0.42501354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   lucknow   5   0.3599618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_89   6   0.27923292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_119   7   0.2776482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_110   8   0.27617308   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   ourgreatvassalem00bell_89   9   0.27617308   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   cu31924023252962_417   10   0.27614358   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   mutinyofbengalar00mallrich_194   11   0.27570617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   hartsannualarmy17hartgoog_255   12   0.2668343   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_498   13   0.26380613   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   punjaub   14   0.26285502   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_161   15   0.26112935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   indikacountrypeo00hursuoft_731   16   0.2544915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   cu31924023252467_70   17   0.25442806   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   annexationpunja00bellgoog_90   18   0.24987713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   greatparliamenta00belliala_116   19   0.24840778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_376   20   0.24840778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   cu31924003933409_102   21   0.24835783   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   highroadofempire00murrrich_293   22   0.24270073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_313   23   0.24217102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   delhisiegeassau00youngoog_137   24   0.23928002   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   studiointernatio49lond_55   25   0.2359273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   reminiscencesofb00edwauoft_276   26   0.23306005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   memoirservicesb01buckgoog_93   27   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   parvatiromanceof00chauiala_124   28   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   generalsirrichar00thor_67   29   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   generalsirrichar00thor_62   30   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   generalsirrichar00thor_77   31   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   ourgreatvassalem00bell_23   32   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   generalsirrichar00thor_73   33   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   mydiaryinindiain02russuoft_62   34   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   cloudsonhorizon01custgoog_82   35   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   generalsirrichar00thor_58   36   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   journalofbombayn11189798bomb_198   37   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   illustrationsofh00thoruoft_439   38   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   mysteriousindiai00chau_156   39   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 1   rel2-19   mysteriousindiai00chau_160   40   0.23255101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
