###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_463, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 62, non-zero: 62, borderline: 60, overall act: 10.155, act diff: 8.155, ratio: 0.803
#   pulse 2: activated: 9505, non-zero: 9505, borderline: 9501, overall act: 980.629, act diff: 970.474, ratio: 0.990
#   pulse 3: activated: 9505, non-zero: 9505, borderline: 9501, overall act: 980.629, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9505
#   final overall activation: 980.6
#   number of spread. activ. pulses: 3
#   running time: 15749

###################################
# top k results in TREC format: 

1   rel2-8   1854   1   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1859   2   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1857   3   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1865   4   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1868   5   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1858   6   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1855   7   0.20473441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1862   8   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1863   9   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1860   10   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1866   11   0.107255004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1842   12   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1856   13   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1848   14   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1853   15   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1845   16   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1846   17   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1849   18   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1843   19   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
1   rel2-8   1844   20   0.099667996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-8   newannualarmylis1874hart_463   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   lucknow   3   0.49371156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   agra   4   0.35982493   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   winterindia01scid_204   5   0.2833444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   historyofsepoywa00kaye_473   6   0.2833444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   delhisiegeassau00youngoog_385   7   0.2819044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   shahabad   8   0.28158778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   fromindependence00brew_149   9   0.27806604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   historyofchristi02houg_487   10   0.27806604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   cu31924023252962_432   11   0.2754597   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   hartsannualarmy17hartgoog_258   12   0.26698548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   cu31924031774155_92   13   0.26293686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   recollectionsofm1900fayr_200   14   0.26293686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   tigershootingind00fiferich_276   15   0.26293686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   delhisiegeassau00youngoog_221   16   0.26124534   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   delhisiegeassau00youngoog_152   17   0.2599037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   delhisiegeassau00youngoog_246   18   0.2554953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   hartsannualarmy17hartgoog_280   19   0.2528368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   newannualarmylis1874hart_203   20   0.24610315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   delhisiegeassau00youngoog_400   21   0.24588783   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   gwalior   22   0.24422872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   hartsannualarmy17hartgoog_568   23   0.24301101   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   fortyoneyearsini00robe_316   24   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_231   25   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_230   26   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   tennysonhisarta01broogoog_251   27   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_279   28   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_267   29   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   memoirofsirwilli00veitrich_344   30   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_211   31   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reminiscencesofb00edwauoft_234   32   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   lifeofisabellath00thobrich_313   33   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   reginaldheberbis00bric_147   34   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   passionsanimals00thomgoog_139   35   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   macmillansmagazi43macmuoft_261   36   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   cu31924022927283_266   37   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   cu31924013563865_47   38   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   lifeofisabellath00thobrich_290   39   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
# 1   rel2-8   lifeofisabellath00thobrich_286   40   0.24196078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.6-loc_th0.35
