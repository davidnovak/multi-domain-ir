###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 16, non-zero: 16, borderline: 15, overall act: 3.376, act diff: 2.376, ratio: 0.704
#   pulse 2: activated: 16, non-zero: 16, borderline: 15, overall act: 3.376, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 16
#   final overall activation: 3.4
#   number of spread. activ. pulses: 2
#   running time: 7532

###################################
# top k results in TREC format: 

8   rel1-2   1651   1   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-2   1653   2   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-2   1654   3   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 8   rel1-2   pictorialhistor01macfgoog_422   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   1653   2   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   1651   3   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   1654   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   holland   5   0.20519012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   queensbury   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   portland   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   europe   8   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   plymouth   9   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   weymouth   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   denmark   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   mediterranean   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   calais   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   baltic   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   portsmouth   15   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-2   boulogne   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.5-loc_th0.35
