###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 270, non-zero: 270, borderline: 267, overall act: 26.812, act diff: 23.812, ratio: 0.888
#   pulse 2: activated: 1044446, non-zero: 1044446, borderline: 1044437, overall act: 136521.288, act diff: 136494.476, ratio: 1.000
#   pulse 3: activated: 1044450, non-zero: 1044450, borderline: 1044440, overall act: 136522.585, act diff: 1.297, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1044450
#   final overall activation: 136522.6
#   number of spread. activ. pulses: 3
#   running time: 170685

###################################
# top k results in TREC format: 

8   rel3-1   1651   1   0.6400826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1653   2   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1654   3   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1652   4   0.21102692   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1595   5   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1625   6   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1627   7   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1596   8   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1350   9   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1348   10   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1363   11   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1361   12   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1359   13   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1357   14   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1355   15   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1353   16   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1373   17   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1372   18   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1374   19   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
8   rel3-1   1354   20   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 8   rel3-1   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_237   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_236   4   0.7155972   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   cu31924014592566_592   5   0.65983224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   england   6   0.64488494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   1651   7   0.6400826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_238   8   0.62162423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_421   9   0.60866755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   cu31924014592566_594   10   0.6018288   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_235   11   0.59314907   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   holland   12   0.5655399   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   encyclopdiabri07chisrich_523   13   0.48534462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   governmentalhist03sher_153   14   0.4795619   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_423   15   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   manualofinternat02ferg_476   16   0.4616664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   briefhistoryofna00fish_433   17   0.45688915   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   encyclopaediabri12kell_89   18   0.45307535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   historyofcolonya00char_224   19   0.44939148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   historyforreadyr03larnuoft_770   20   0.4444738   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_390   21   0.44273657   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   governanceempir00silbgoog_125   22   0.44271168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   evolutionofstate00robeuoft_438   23   0.4401159   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   hallamsworks04halliala_45   24   0.43851304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_391   25   0.43565872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   irelandundercomm02dunluoft_123   26   0.43269202   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   dial31unkngoog_221   27   0.42893922   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_416   28   0.42856273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   scottishreview31paisuoft_310   29   0.427577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   historyofuniteds91banc_174   30   0.42672068   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   americannationhi05hartuoft_33   31   0.42568347   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   historyofunionja00cumbrich_122   32   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   thefirstdutchwar03navyuoft_217   33   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   atextbookoncivi01martgoog_148   34   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   kinginexilewande00scotuoft_372   35   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   puritancromwell00firtuoft_558   36   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   cu31924032733879_89   37   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   generalhistoryof04hawk_354   38   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   worksthomascarl10traigoog_22   39   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 8   rel3-1   miltonjoh00pattuoft_146   40   0.42540735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
