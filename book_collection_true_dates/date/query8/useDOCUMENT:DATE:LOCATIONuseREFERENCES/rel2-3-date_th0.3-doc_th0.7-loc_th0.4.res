###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 32, non-zero: 32, borderline: 30, overall act: 6.871, act diff: 4.871, ratio: 0.709
#   pulse 2: activated: 5079, non-zero: 5079, borderline: 5076, overall act: 490.402, act diff: 483.531, ratio: 0.986
#   pulse 3: activated: 5079, non-zero: 5079, borderline: 5076, overall act: 490.402, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5079
#   final overall activation: 490.4
#   number of spread. activ. pulses: 3
#   running time: 25294

###################################
# top k results in TREC format: 

8   rel2-3   1651   1   0.38038954   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1653   2   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1654   3   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1652   4   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1627   5   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1625   6   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1595   7   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
8   rel2-3   1596   8   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 8   rel2-3   pictorialhistor01macfgoog_422   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   cu31924014592566_593   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   1651   3   0.38038954   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   england   4   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   spain   5   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   1653   6   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   1654   7   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   europe   8   0.2440059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   holland   9   0.20519012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   historyofholland00edmu_316   10   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   shakespearesengl00win_147   11   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   calendarofclaren03bodluoft_270   12   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   selectessaysinan01asso_438   13   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   swedishtheatreof00naes_335   14   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   generalabridgmen08vine_76   15   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   doctrineofevolu00cram_87   16   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   govuscourtsca9briefs0028_696   17   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   clarkepaperssel00firtgoog_272   18   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   libraryofamerica14_2spar_155   19   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   summaryhistorica01dodsrich_285   20   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   melodiesmadrigal01stod_148   21   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   melodiesmadrigal01stod_149   22   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   minutesofcourtof01newn_15   23   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   memoriesofmonths01maxwrich_211   24   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   introductiontol12hallgoog_884   25   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   puritancromwell00firtuoft_470   26   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   medicaldirectory16medi_553   27   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   cu31924086564469_87   28   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   calendarstatepa12offigoog_545   29   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   pascalpasc00sainrich_426   30   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   parliamentandco00parrgoog_30   31   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   cu31924022841294_123   32   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   cu31924079619411_469   33   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   jesuitrelations89jesugoog_377   34   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   publications19yorkuoft_154   35   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   internationalcl13unkngoog_109   36   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   rembrandthislife00mich_521   37   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   richardsteele00stee_302   38   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   historyofnewlond00caul_95   39   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 8   rel2-3   theophilusanglic00worduoft_243   40   0.1879341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
