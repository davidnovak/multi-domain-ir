###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 251, non-zero: 251, borderline: 249, overall act: 20.611, act diff: 18.611, ratio: 0.903
#   pulse 2: activated: 99728, non-zero: 99728, borderline: 99723, overall act: 9450.602, act diff: 9429.991, ratio: 0.998
#   pulse 3: activated: 99728, non-zero: 99728, borderline: 99723, overall act: 9450.602, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 99728
#   final overall activation: 9450.6
#   number of spread. activ. pulses: 3
#   running time: 133689

###################################
# top k results in TREC format: 

8   rel2-1   1653   1   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1654   2   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1651   3   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1350   4   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1348   5   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1363   6   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1361   7   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1359   8   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1357   9   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1355   10   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1353   11   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1373   12   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1372   13   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1374   14   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1354   15   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1356   16   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1358   17   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1360   18   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1362   19   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
8   rel2-1   1347   20   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 8   rel2-1   bookofhistoryhis11brycuoft_237   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   holland   3   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   livesofwarriorsw01cust_59   4   0.37468755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   landmarkhistoryo00ulmas_22   5   0.33073714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   burnetshistoryof01burnuoft_429   6   0.32674363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   treasuresofartin02waaguoft_327   7   0.32551044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   worksfrancisbaco03bacoiala_205   8   0.3076144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   pioneermothersof01gree_195   9   0.30685633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   1653   10   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   1654   11   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   250thanniversary00newyrich_117   12   0.2991831   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   cu31924028066797_380   13   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   englishheraldicb00daverich_402   14   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   courtofkingsbench08greaiala_290   15   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   aschoolhistoryu01macegoog_109   16   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   thomasharrisonr00wessgoog_20   17   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_430   18   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_420   19   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   lifeofyoungsirhe00hosmiala_433   20   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   augustanages00eltouoft_242   21   0.29197112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   lettersspeecheso02cromuoft_338   22   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   evolutionofstate00robeuoft_331   23   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   cu31924067742332_16   24   0.29155213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   historyofstateof00broduoft_613   25   0.29082623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   historyforreadyr03larnuoft_770   26   0.28895515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   registerofvisito00camduoft_113   27   0.28755295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   england   28   0.27598518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   cu31924028485070_293   29   0.27375957   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   historyofnewyork00pren_93   30   0.27134305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   burnetshistoryof00burnuoft_175   31   0.26919344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_444   32   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   carlylecompletew08carl_313   33   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   lettersfromround00bann_79   34   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   cu31924027975733_471   35   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   fournewyorkboys00davigoog_87   36   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   littlejenningsfi01sergiala_24   37   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   memorialswillar00fiskgoog_127   38   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   worksthomascarl10traigoog_114   39   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
# 8   rel2-1   fullersthoughts00fullrich_232   40   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.5-loc_th0.35
