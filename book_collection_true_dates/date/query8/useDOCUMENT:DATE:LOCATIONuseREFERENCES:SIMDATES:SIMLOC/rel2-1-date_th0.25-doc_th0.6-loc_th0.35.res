###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 251, non-zero: 251, borderline: 249, overall act: 20.611, act diff: 18.611, ratio: 0.903
#   pulse 2: activated: 99744, non-zero: 99744, borderline: 99739, overall act: 9454.281, act diff: 9433.669, ratio: 0.998
#   pulse 3: activated: 99744, non-zero: 99744, borderline: 99739, overall act: 9766.540, act diff: 312.260, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 99744
#   final overall activation: 9766.5
#   number of spread. activ. pulses: 3
#   running time: 69094

###################################
# top k results in TREC format: 

8   rel2-1   1653   1   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1654   2   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1652   3   0.2738929   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1651   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1655   5   0.21397822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1350   6   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1348   7   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1363   8   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1361   9   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1359   10   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1357   11   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1355   12   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1353   13   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1373   14   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1372   15   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1374   16   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1354   17   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1356   18   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1358   19   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   1360   20   0.06364474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 8   rel2-1   bookofhistoryhis11brycuoft_237   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   1653   3   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   1654   4   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   holland   5   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   livesofwarriorsw01cust_59   6   0.42950392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   landmarkhistoryo00ulmas_22   7   0.3876902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   burnetshistoryof01burnuoft_429   8   0.38387918   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pioneermothersof01gree_195   9   0.36487168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   treasuresofartin02waaguoft_327   10   0.3619045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   registerofvisito00camduoft_113   11   0.36136174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   250thanniversary00newyrich_117   12   0.35752493   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   thomasharrisonr00wessgoog_20   13   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_420   14   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   courtofkingsbench08greaiala_290   15   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   aschoolhistoryu01macegoog_109   16   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_430   17   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lifeofyoungsirhe00hosmiala_433   18   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lettersspeecheso02cromuoft_338   19   0.35021484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   evolutionofstate00robeuoft_331   20   0.35021484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   historyofstateof00broduoft_613   21   0.34951544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   historyforreadyr03larnuoft_770   22   0.34772095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924067742332_16   23   0.34024742   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   worksfrancisbaco03bacoiala_205   24   0.33692515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   influenceseapow13mahagoog_70   25   0.33378616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   memorialswillar00fiskgoog_127   26   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   worksthomascarl10traigoog_114   27   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   fournewyorkboys00davigoog_87   28   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   fullersthoughts00fullrich_232   29   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_444   30   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lettersfromround00bann_79   31   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924027975733_471   32   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   littlejenningsfi01sergiala_24   33   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   carlylecompletew08carl_313   34   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924028485070_288   35   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   manualofinternat02ferg_476   36   0.32365453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924028066797_380   37   0.32350183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   englishheraldicb00daverich_402   38   0.32350183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924067742332_57   39   0.32344297   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924028589939_215   40   0.31789288   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
