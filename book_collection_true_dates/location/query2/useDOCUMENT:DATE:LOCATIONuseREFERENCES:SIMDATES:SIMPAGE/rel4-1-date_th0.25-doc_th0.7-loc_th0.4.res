###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1052104, non-zero: 1052104, borderline: 1052095, overall act: 153553.152, act diff: 153540.436, ratio: 1.000
#   pulse 3: activated: 1059508, non-zero: 1059508, borderline: 1059495, overall act: 154515.412, act diff: 962.260, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059508
#   final overall activation: 154515.4
#   number of spread. activ. pulses: 3
#   running time: 174697

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   0.9548845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   dublin   2   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   england   3   0.63565564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   strafford   4   0.46278515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   london   5   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   essex   6   0.11795033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   town   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   europe   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   shoreditch   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   peacock   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   limerick   11   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
2   rel4-1   city   12   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   ireland   5   0.9548845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   1641   6   0.77572834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_601   7   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_96   8   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_602   9   0.73656374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_355   10   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   11   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_599   12   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   13   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   dublin   14   0.6834969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   15   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   thomasharrisonr00wessgoog_83   16   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_315   17   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   strangerinirelan00carr_91   18   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   redeemerstearsw00urwigoog_21   19   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa00levagoog_361   20   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   21   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_172   22   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924091770861_873   23   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_358   24   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_67   25   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   26   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_434   27   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_382   28   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_249   29   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924091770861_898   30   0.66313225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_120   31   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_385   32   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_229   33   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_21   34   0.6592293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandonehundre00walsrich_38   35   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   compendiumofhist02lawl_189   36   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   landwarinireland00godk_205   37   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   commercialrestra00hely_255   38   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroyspostbag02macdgoog_266   39   0.65841407   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
# 2   rel4-1   historyengland00macagoog_517   40   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.4
