###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924029563875_157, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 19, non-zero: 19, borderline: 18, overall act: 4.538, act diff: 3.538, ratio: 0.780
#   pulse 2: activated: 627180, non-zero: 627180, borderline: 627178, overall act: 59857.384, act diff: 59852.846, ratio: 1.000
#   pulse 3: activated: 627180, non-zero: 627180, borderline: 627178, overall act: 59857.384, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 627180
#   final overall activation: 59857.4
#   number of spread. activ. pulses: 3
#   running time: 94226

###################################
# top k results in TREC format: 

2   rel1-2   london   1   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   ireland   2   0.27465868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   dublin   3   0.17114265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   shoreditch   4   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   europe   5   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   peacock   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   city   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   town   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
2   rel1-2   limerick   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel1-2   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   cu31924029563875_158   2   0.5628241   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   cu31924029563875_156   3   0.5116861   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   london   4   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   ireland   5   0.27465868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_86   6   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   prabacteriology00stitrich_6   7   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   masterofmarton01tabo_65   8   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   forum04unkngoog_180   9   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   ethicsofdemocrac00postrich_67   10   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   forum04unkngoog_190   11   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   methodisthymnboo00stev_462   12   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_99   13   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   fourlettersonpro00burkrich_111   14   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   lifeandlettersg02towngoog_168   15   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_381   16   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   herberthooverman00lckell_89   17   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   herberthooverman00lckell_84   18   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_397   19   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_395   20   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_41   21   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   lifeandlettersg02towngoog_140   22   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   piercepenniless00nashgoog_29   23   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   workscomprisingh02cowpuoft_378   24   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   ageneralabridgm30vinegoog_86   25   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   universalanthol11brangoog_66   26   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   journalseriesage65royauoft_360   27   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_27   28   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   marriage00welliala_157   29   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   marriage00welliala_151   30   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   textbookonroadsp01spal_421   31   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   saintjohnsfire00sude_109   32   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   marriage00welliala_143   33   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   cu31924020159368_219   34   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   georgeselwyn00selw_326   35   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_79   36   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_74   37   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   economicreview05unkngoog_73   38   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   practicalobserva00wild_504   39   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel1-2   practicalobserva00wild_502   40   0.17823784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
