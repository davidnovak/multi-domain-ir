###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1052104, non-zero: 1052104, borderline: 1052095, overall act: 153553.152, act diff: 153540.436, ratio: 1.000
#   pulse 3: activated: 1059632, non-zero: 1059632, borderline: 1059503, overall act: 154586.404, act diff: 1033.252, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059632
#   final overall activation: 154586.4
#   number of spread. activ. pulses: 3
#   running time: 240853

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   england   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   dublin   3   0.9999947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   strafford   4   0.7584124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   scotland   5   0.7137856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   london   6   0.69957477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   limerick   7   0.5414639   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   munster   8   0.41922724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   fairfax   9   0.41709816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   spain   10   0.40462184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   kingdom   11   0.36557087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   derry   12   0.34777528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   londonderry   13   0.33823743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   dublin castle   14   0.3311887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   drogheda   15   0.28266177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   france   16   0.2577474   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   clarendon   17   0.22448464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   connaught   18   0.21786673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   america   19   0.20900765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
2   rel4-1   city   20   0.18378584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   dublin   8   0.9999947   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   9   0.81100243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   10   0.8018684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_414   11   0.7961591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_236   12   0.79371476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_235   13   0.79206616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_432   14   0.7757778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   strafford   15   0.7584124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_613   16   0.7562776   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_601   17   0.7512844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_96   18   0.743369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_602   19   0.73656374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_381   20   0.730032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   revolutionaryir00mahagoog_99   21   0.7286991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_636   22   0.72835165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   scotland   23   0.7137856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_68   24   0.707184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_25   25   0.7043432   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_435   26   0.7036122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_357   27   0.70238817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   london   28   0.69957477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   cu31924091770861_287   29   0.6990484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_430   30   0.6961846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_355   31   0.69500595   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_23   32   0.6917561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   33   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   historyengland00macagoog_518   34   0.68964297   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   historyforreadyr03larnuoft_237   35   0.68822455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_599   36   0.6863355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_292   37   0.686227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   38   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   39   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel4-1   publications13irisuoft_17   40   0.68028486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.4
