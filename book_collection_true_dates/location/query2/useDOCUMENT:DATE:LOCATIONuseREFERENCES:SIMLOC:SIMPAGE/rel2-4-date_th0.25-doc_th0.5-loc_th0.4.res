###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.356, act diff: 5.356, ratio: 0.728
#   pulse 2: activated: 221896, non-zero: 221896, borderline: 221893, overall act: 30778.838, act diff: 30771.482, ratio: 1.000
#   pulse 3: activated: 221898, non-zero: 221898, borderline: 221894, overall act: 30779.887, act diff: 1.049, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221898
#   final overall activation: 30779.9
#   number of spread. activ. pulses: 3
#   running time: 56912

###################################
# top k results in TREC format: 

2   rel2-4   ireland   1   0.67013997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   dublin   2   0.410892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   london   3   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   strafford   4   0.33804712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   england   5   0.33804712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   clifton house   6   0.2670752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   cangort   7   0.26053482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   cangort park   8   0.25689435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   sopwell hall   9   0.25559682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   kilcomin   10   0.25226468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   ballincor   11   0.2498633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   corolanty   12   0.2478623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   loughkeen   13   0.24684286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   ivy hall   14   0.24592662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   shinrone   15   0.24558857   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   modreeny   16   0.24120934   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   cloughjordan   17   0.2380296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   fort nisbett   18   0.23649755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   borrisokane   19   0.23102096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-4   town   20   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   ireland   3   0.67013997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_96   4   0.5858838   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   cu31924029563875_156   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   cu31924029563875_158   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_98   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_95   8   0.42250812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   dublin   9   0.410892   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   london   10   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   strafford   11   0.33804712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   england   12   0.33804712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   creelofirishstor00barliala_138   13   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_8   14   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_82   15   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   britishmammalsat00johnrich_278   16   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_209   17   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_4   18   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_143   19   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   lettersandjourna02bailuoft_176   20   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_155   21   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_23   22   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_11   23   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_17   24   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_13   25   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_44   26   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_43   27   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   naturalistscabin02smituoft_223   28   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_194   29   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_211   30   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   speeches00philiala_92   31   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_210   32   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_206   33   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_274   34   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_272   35   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_271   36   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_270   37   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   lettersconcerni00clargoog_278   38   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_278   39   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_277   40   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
