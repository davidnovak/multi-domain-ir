###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 28, non-zero: 28, borderline: 25, overall act: 10.441, act diff: 7.441, ratio: 0.713
#   pulse 2: activated: 1512071, non-zero: 1512071, borderline: 1512062, overall act: 203917.375, act diff: 203906.934, ratio: 1.000
#   pulse 3: activated: 1512079, non-zero: 1512079, borderline: 1512066, overall act: 203921.035, act diff: 3.660, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512079
#   final overall activation: 203921.0
#   number of spread. activ. pulses: 3
#   running time: 182499

###################################
# top k results in TREC format: 

2   rel3-4   ireland   1   0.94478816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   dublin   2   0.76815075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   england   3   0.7295845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   strafford   4   0.5433503   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   london   5   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   clifton house   6   0.3330296   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   cangort   7   0.3251004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   cangort park   8   0.32067955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   sopwell hall   9   0.31910267   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   kilcomin   10   0.31505018   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   ballincor   11   0.31212696   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   corolanty   12   0.30968955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   loughkeen   13   0.30844718   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   ivy hall   14   0.30733025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   shinrone   15   0.30691805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   modreeny   16   0.30157477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   cloughjordan   17   0.29769063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   fort nisbett   18   0.2958179   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   borrisokane   19   0.28911668   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-4   haddington road   20   0.2512177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-4   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   cu31924029563875_157   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   ireland   4   0.94478816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   dublin   5   0.76815075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   england   6   0.7295845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_601   7   0.7221464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_96   8   0.71851975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_599   9   0.65944695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   1641   10   0.6543156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_602   11   0.6146017   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   studentshistoryo03garduoft_42   12   0.6013532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   cu31924029563875_158   13   0.5988058   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   harleianmiscell00oldygoog_332   14   0.5960795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_31   15   0.5918422   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyofengland08lodguoft_78   16   0.5896279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_24   17   0.5889065   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   thomasharrisonr00wessgoog_83   18   0.5870921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   redeemerstearsw00urwigoog_21   19   0.58627963   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_236   20   0.5849868   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   strangerinirelan00carr_91   21   0.5800713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   publications13irisuoft_16   22   0.57954997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_315   23   0.5771714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   calendarstatepa00levagoog_361   24   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyirelanda00smilgoog_172   25   0.5741105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_431   26   0.57091826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyirishper01maddgoog_67   27   0.5707552   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_415   28   0.57052505   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   calendarstatepa12offigoog_373   29   0.56995064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   selectspeecheswi00cannuoft_358   30   0.5678152   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   irelandsfightfor1919cree_101   31   0.5672616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   completehistoryo0306kenn_434   32   0.56657606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyengland00macagoog_517   33   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   cu31924027975733_414   34   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   cu31924091770861_873   35   0.5660654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_95   36   0.5646014   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   politicalstudies00broduoft_385   37   0.5639206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   historyirishper01maddgoog_229   38   0.5624587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   calendarstatepa12offigoog_21   39   0.5618158   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-4   viceroyspostbag02macdgoog_266   40   0.5610784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
