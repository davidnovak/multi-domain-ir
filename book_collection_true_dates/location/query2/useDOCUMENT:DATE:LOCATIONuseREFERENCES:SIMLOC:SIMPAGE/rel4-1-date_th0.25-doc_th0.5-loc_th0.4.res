###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 31, non-zero: 31, borderline: 27, overall act: 12.716, act diff: 8.716, ratio: 0.685
#   pulse 2: activated: 1052140, non-zero: 1052140, borderline: 1052131, overall act: 153563.511, act diff: 153550.795, ratio: 1.000
#   pulse 3: activated: 1053195, non-zero: 1053195, borderline: 1052515, overall act: 154016.304, act diff: 452.794, ratio: 0.003

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1053195
#   final overall activation: 154016.3
#   number of spread. activ. pulses: 3
#   running time: 440807

###################################
# top k results in TREC format: 

2   rel4-1   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   ireland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   dublin   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   london   4   0.9999999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   scotland   5   0.9999814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   strafford   6   0.9982138   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   france   7   0.9963465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   great britain   8   0.98920906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   belfast   9   0.9844535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   limerick   10   0.98288625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   munster   11   0.9594444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   rome   12   0.9533008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   europe   13   0.952301   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   america   14   0.9298006   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   spain   15   0.92873996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   kingdom   16   0.8946444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   fairfax   17   0.8628088   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   drogheda   18   0.8619522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   derry   19   0.8495571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel4-1   wexford   20   0.8334687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   england   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   cu31924029563875_157   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   dublin   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   london   10   0.9999999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   scotland   11   0.9999814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   strafford   12   0.9982138   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   france   13   0.9963465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   great britain   14   0.98920906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   belfast   15   0.9844535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   limerick   16   0.98288625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1798   17   0.9676984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1688   18   0.9651433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   munster   19   0.9594444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   rome   20   0.9533008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   europe   21   0.952301   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1885   22   0.94153476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1649   23   0.93323725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   america   24   0.9298006   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   spain   25   0.92873996   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   kingdom   26   0.8946444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   fairfax   27   0.8628088   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   drogheda   28   0.8619522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   derry   29   0.8495571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   1689   30   0.83746135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   landwarinireland00godk_232   31   0.83595175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   wexford   32   0.8334687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   leinster   33   0.82779104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   connaught   34   0.82002664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   strangerinirelan00carr_59   35   0.8173279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   36   0.81100243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   37   0.8018684   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   cu31924024892881_569   38   0.8007774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   armagh   39   0.8007702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_414   40   0.7961591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
