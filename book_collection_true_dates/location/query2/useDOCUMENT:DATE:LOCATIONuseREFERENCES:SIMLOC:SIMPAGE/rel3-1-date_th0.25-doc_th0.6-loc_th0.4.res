###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 9.762, act diff: 6.762, ratio: 0.693
#   pulse 2: activated: 227922, non-zero: 227922, borderline: 227917, overall act: 44885.665, act diff: 44875.903, ratio: 1.000
#   pulse 3: activated: 227928, non-zero: 227928, borderline: 227921, overall act: 44887.466, act diff: 1.801, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 227928
#   final overall activation: 44887.5
#   number of spread. activ. pulses: 3
#   running time: 65162

###################################
# top k results in TREC format: 

2   rel3-1   ireland   1   0.92194253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   dublin   2   0.41866523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   clifton house   3   0.37767184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   cangort   4   0.3688891   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   cangort park   5   0.36398572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   sopwell hall   6   0.36223555   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   london   7   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   kilcomin   8   0.35773504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   ballincor   9   0.3544863   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   corolanty   10   0.3517759   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   loughkeen   11   0.35039383   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   ivy hall   12   0.34915102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   shinrone   13   0.34869233   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   strafford   14   0.3463362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   england   15   0.3463362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   modreeny   16   0.34274244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   cloughjordan   17   0.33841333   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   fort nisbett   18   0.33632487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   borrisokane   19   0.32884535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-1   town   20   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel3-1   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   ireland   4   0.92194253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   englandsfightwit00wals_355   5   0.68008155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   viceroysofirelan00omahuoft_96   6   0.63442165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_291   7   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_411   8   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_130   9   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   fastiecclesiaeh02cottgoog_20   10   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   compendiumofhist02lawl_76   11   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_345   12   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   calendarstatepa12offigoog_335   13   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_413   14   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   cu31924091786628_78   15   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   castlesofireland00adamiala_301   16   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   confederationofk00meeh_10   17   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   englishinireland03frou_309   18   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   englishinireland03frou_118   19   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   revolutionaryir00mahagoog_100   20   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   historymoderneu11russgoog_386   21   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   economichistoryo00obri_321   22   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   historyofdiocese02healiala_23   23   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   irishtangleandwa1920john_53   24   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   constitutionalhi03hall_414   25   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   statechurcheskin00allerich_587   26   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   memoirsandcorre02castgoog_42   27   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   historyofireland3_00will_307   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   proceedingsofro22roya_307   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   cu31924091770861_319   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   cu31924091770861_286   31   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_421   32   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   politicalstudies00broduoft_370   33   0.52573967   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   politicalstudies00broduoft_382   34   0.52029926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_249   35   0.5112249   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   historyirelanda00smilgoog_120   36   0.50910294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   cu31924091770861_873   37   0.50910294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   historicalreview02plow_29   38   0.50664926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_315   39   0.50646454   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-1   irelandsaintpatr00morriala_17   40   0.50529885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
