###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 24, non-zero: 24, borderline: 22, overall act: 7.736, act diff: 5.736, ratio: 0.741
#   pulse 2: activated: 879945, non-zero: 879945, borderline: 879938, overall act: 96850.157, act diff: 96842.422, ratio: 1.000
#   pulse 3: activated: 879972, non-zero: 879972, borderline: 879959, overall act: 96856.108, act diff: 5.951, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 879972
#   final overall activation: 96856.1
#   number of spread. activ. pulses: 3
#   running time: 150060

###################################
# top k results in TREC format: 

2   rel2-5   ireland   1   0.8522374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   london   2   0.7160261   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   dublin   3   0.6967783   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   england   4   0.44239074   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   scotland   5   0.23886822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   clifton house   6   0.23395795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   cangort   7   0.2281628   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   cangort park   8   0.22493929   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   sopwell hall   9   0.22379076   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   kilcomin   10   0.22084215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   ballincor   11   0.2187179   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   corolanty   12   0.21694839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   loughkeen   13   0.21604702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   ivy hall   14   0.21523702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   shinrone   15   0.21493816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   modreeny   16   0.21106811   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   cloughjordan   17   0.20825936   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   fort nisbett   18   0.20690647   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   strafford   19   0.20501322   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel2-5   borrisokane   20   0.20207222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel2-5   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   ireland   3   0.8522374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   1641   4   0.7550289   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   london   5   0.7160261   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   dublin   6   0.6967783   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_601   7   0.62992036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_599   8   0.60976464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924029563875_158   9   0.5628241   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   bibliothecagrenv03grenrich_30   10   0.5407031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924029563875_156   11   0.5116861   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_23   12   0.5111344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   bibliothecagrenv03grenrich_31   13   0.50440717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_24   14   0.5004303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   strangerinirelan00carr_91   15   0.4914904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   calendarstatepa12offigoog_525   16   0.48964527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_432   17   0.48509726   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   historyengland00macagoog_517   18   0.48264053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924027975733_414   19   0.4826405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   politicalstudies00broduoft_385   20   0.47820508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924091770861_873   21   0.47759178   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   viceroyspostbag02macdgoog_266   22   0.4760442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   landwarinireland00godk_205   23   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   compendiumofhist02lawl_189   24   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   irelandonehundre00walsrich_38   25   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   commercialrestra00hely_255   26   0.47515425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   historyofireland02daltuoft_409   27   0.47090447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   olivercromwell00rossgoog_134   28   0.47090447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   causeofirelandpl00orei_25   29   0.46702775   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   historyforreadyr03larnuoft_236   30   0.46602526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   irelandundercomm02dunluoft_279   31   0.4648844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   historyofenniski02trim_72   32   0.4648844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   socialstateofgre00berm_196   33   0.45929736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   redeemerstearsw00urwigoog_21   34   0.45559338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   historyofclareda00whit_272   35   0.4551854   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   whathavegreeksd00mahagoog_196   36   0.45385545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   irelandpopebrief00magurich_47   37   0.45385545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   cu31924029565607_447   38   0.45168686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   calendarstatepa12offigoog_541   39   0.450603   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel2-5   hallamsworks04halliala_602   40   0.45031837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
