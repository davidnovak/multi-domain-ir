###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 10, overall act: 6.064, act diff: 4.064, ratio: 0.670
#   pulse 2: activated: 1052126, non-zero: 1052126, borderline: 1052119, overall act: 133011.999, act diff: 133005.936, ratio: 1.000
#   pulse 3: activated: 1052126, non-zero: 1052126, borderline: 1052119, overall act: 133011.999, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052126
#   final overall activation: 133012.0
#   number of spread. activ. pulses: 3
#   running time: 136720

###################################
# top k results in TREC format: 

2   rel2-6   ireland   1   0.536683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   england   2   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   dublin   3   0.38038954   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   clifton house   4   0.256897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   cangort   5   0.2505826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   cangort park   6   0.24706864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   sopwell hall   7   0.24581638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   kilcomin   8   0.24260072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   ballincor   9   0.24028353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   corolanty   10   0.23835292   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   loughkeen   11   0.23736937   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   ivy hall   12   0.23648544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   shinrone   13   0.2361593   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   strafford   14   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   modreeny   15   0.23193496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   cloughjordan   16   0.22886814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   fort nisbett   17   0.22739066   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   borrisokane   18   0.22210974   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   newton blossomville   19   0.20824702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
2   rel2-6   filgrave   20   0.20565914   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel2-6   hallamsworks04halliala_600   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   hallamsworks04halliala_601   3   0.6794118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_96   4   0.6781855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   hallamsworks04halliala_599   5   0.61644304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   ireland   6   0.536683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   studentshistoryo03garduoft_42   7   0.4950537   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   harleianmiscell00oldygoog_332   8   0.49479952   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   parliamentaryhis00obriuoft_215   9   0.47758645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   thomasharrisonr00wessgoog_83   10   0.47464165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_431   11   0.47398064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   calendarstatepa00levagoog_361   12   0.47340676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   historyirishper01maddgoog_67   13   0.47104666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_315   14   0.4708332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   historyofmodernb00conauoft_188   15   0.4698304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   redeemerstearsw00urwigoog_21   16   0.4680659   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   constitutionalhi03hall_415   17   0.4649451   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   completehistoryo0306kenn_434   18   0.46446007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   selectspeecheswi00cannuoft_358   19   0.4641117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   viceroysofirelan00omahuoft_98   20   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   historyirelanda00smilgoog_172   21   0.46159548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   historyirishper01maddgoog_229   22   0.46150184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   encyclopaediaofl07polluoft_81   23   0.46143323   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   calendarstatepa12offigoog_21   24   0.46046337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   encyclopaediaofl07polluoft_77   25   0.45859015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   irelandsfightfor1919cree_101   26   0.45836282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   lifeofdanielocon00cusa_839   27   0.45620295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   englishlawirisht00gibbrich_20   28   0.4559494   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_24   29   0.45535484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   essayonelementso00burn_72   30   0.45461884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   politicalstudies00broduoft_351   31   0.45359546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   england   32   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   memoirsandcorre02castgoog_67   33   0.4522274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   ahistorymodernb03conagoog_187   34   0.45012453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   livingagevolume01littgoog_588   35   0.4495837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   cu31924077097792_337   36   0.44917008   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   causeofirelandpl00orei_249   37   0.44902313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   publications13irisuoft_16   38   0.44808164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   strangerinirelan00carr_91   39   0.44786102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 2   rel2-6   irelandsaintpatr00morriala_201   40   0.44736496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
