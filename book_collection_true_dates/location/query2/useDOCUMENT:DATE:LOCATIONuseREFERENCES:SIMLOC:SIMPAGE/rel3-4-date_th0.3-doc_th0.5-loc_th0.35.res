###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 28, non-zero: 28, borderline: 25, overall act: 10.441, act diff: 7.441, ratio: 0.713
#   pulse 2: activated: 1512071, non-zero: 1512071, borderline: 1512062, overall act: 203917.375, act diff: 203906.934, ratio: 1.000
#   pulse 3: activated: 1512546, non-zero: 1512546, borderline: 1512311, overall act: 204082.290, act diff: 164.915, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1512546
#   final overall activation: 204082.3
#   number of spread. activ. pulses: 3
#   running time: 347256

###################################
# top k results in TREC format: 

2   rel3-4   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   london   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   ireland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   dublin   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   scotland   5   0.978497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   strafford   6   0.81961566   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   limerick   7   0.76061654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   belfast   8   0.66713935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   londonderry   9   0.6664363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   great britain   10   0.6598652   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   edinburgh   11   0.5621699   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   paris   12   0.5490591   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   wales   13   0.5249738   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   spain   14   0.5116382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   drogheda   15   0.49092072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   kingdom   16   0.43832   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   munster   17   0.4128228   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   clarendon   18   0.41236183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   france   19   0.40730014   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
2   rel3-4   united kingdom   20   0.40020505   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   london   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   ireland   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1845   9   0.99809504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   scotland   10   0.978497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1689   11   0.86904615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   strafford   12   0.81961566   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1688   13   0.8032465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_465   14   0.7930075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1691   15   0.7872588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   cu31924024892881_569   16   0.7765714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   limerick   17   0.76061654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_463   18   0.75049627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_236   19   0.734777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_431   20   0.7291617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   causeofirelandpl00orei_432   21   0.72239256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_601   22   0.7221464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_415   23   0.7205244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   viceroysofirelan00omahuoft_96   24   0.71851975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   historyforreadyr03larnuoft_235   25   0.7133091   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   1885   26   0.7103773   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_90   27   0.70811105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   constitutionalhi03hall_414   28   0.6976935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   cambridgemodhist10actouoft_892   29   0.6931685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_464   30   0.6912114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_461   31   0.68957025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_99   32   0.68869746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   bibliothecagrenv03grenrich_89   33   0.6871948   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_100   34   0.6861739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_462   35   0.68252575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   cambridgemodhist10actouoft_891   36   0.67975795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   publications13irisuoft_17   37   0.67660004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   strangerinirelan00carr_59   38   0.6742322   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   hallamsworks04halliala_613   39   0.673916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 2   rel3-4   revolutionaryir00mahagoog_466   40   0.66874444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
