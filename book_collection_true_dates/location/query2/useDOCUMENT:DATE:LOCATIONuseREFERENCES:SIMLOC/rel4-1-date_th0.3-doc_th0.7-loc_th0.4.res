###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 19, overall act: 9.019, act diff: 5.019, ratio: 0.556
#   pulse 2: activated: 1052138, non-zero: 1052138, borderline: 1052129, overall act: 153560.378, act diff: 153551.359, ratio: 1.000
#   pulse 3: activated: 1052142, non-zero: 1052142, borderline: 1052132, overall act: 153562.534, act diff: 2.157, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052142
#   final overall activation: 153562.5
#   number of spread. activ. pulses: 3
#   running time: 125011

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   0.91867346   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   cangort   2   0.54949653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   cangort park   3   0.54281414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   sopwell hall   4   0.53592527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   kilcomin   5   0.5332182   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   ballincor   6   0.5307397   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   corolanty   7   0.5276561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   loughkeen   8   0.5237755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   shinrone   9   0.5235583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   ivy hall   10   0.5219562   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   dublin   11   0.51782155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   cloughjordan   12   0.5063596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   fort nisbett   13   0.50191313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   england   14   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   clifton house   15   0.40657678   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   modreeny   16   0.36955452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   london   17   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   borrisokane   18   0.35477576   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   haddington road   19   0.2512177   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   lansdowne hotel   20   0.25120786   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   ireland   5   0.91867346   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   6   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   7   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   8   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   thomasharrisonr00wessgoog_83   9   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_315   10   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   strangerinirelan00carr_91   11   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   redeemerstearsw00urwigoog_21   12   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa00levagoog_361   13   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   14   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_172   15   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924091770861_873   16   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_358   17   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_67   18   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   19   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_434   20   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_382   21   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_249   22   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_120   23   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_385   24   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_229   25   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_21   26   0.6592293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandonehundre00walsrich_38   27   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   compendiumofhist02lawl_189   28   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   landwarinireland00godk_205   29   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   commercialrestra00hely_255   30   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroyspostbag02macdgoog_266   31   0.65841407   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyengland00macagoog_517   32   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924027975733_414   33   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandsaintpatr00morriala_201   34   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   dublinreview14londuoft_194   35   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   pt3historyofirel00wriguoft_306   36   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_351   37   0.65477914   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyofbritish00colluoft_221   38   0.6499617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandsfightfor1919cree_101   39   0.649956   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   worksrighthonor37burkgoog_439   40   0.64962125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
