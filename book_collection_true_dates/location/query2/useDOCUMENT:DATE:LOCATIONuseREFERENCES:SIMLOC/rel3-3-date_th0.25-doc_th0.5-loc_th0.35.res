###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1052125, non-zero: 1052125, borderline: 1052117, overall act: 147133.498, act diff: 147127.813, ratio: 1.000
#   pulse 3: activated: 1052400, non-zero: 1052400, borderline: 1052118, overall act: 147186.793, act diff: 53.295, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052400
#   final overall activation: 147186.8
#   number of spread. activ. pulses: 3
#   running time: 245926

###################################
# top k results in TREC format: 

2   rel3-3   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   ireland   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   dublin   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   london   4   0.9784362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   scotland   5   0.9643316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   clifton house   6   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   cangort   7   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   cangort park   8   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   shinrone   9   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   cloughjordan   10   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   kilcomin   11   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   strafford   12   0.780546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   corolanty   13   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   sopwell hall   14   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   france   15   0.76255596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   limerick   16   0.7164269   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   spain   17   0.7159989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   ballincor   18   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   loughkeen   19   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
2   rel3-3   ivy hall   20   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1845   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1641   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   dublin   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   london   9   0.9784362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   scotland   10   0.9643316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   clifton house   11   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort   12   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cangort park   13   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   shinrone   14   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   cloughjordan   15   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   kilcomin   16   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   strafford   17   0.780546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   corolanty   18   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   sopwell hall   19   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   france   20   0.76255596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   limerick   21   0.7164269   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   spain   22   0.7159989   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ballincor   23   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   loughkeen   24   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   ivy hall   25   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   modreeny   26   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   fort nisbett   27   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   28   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   29   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   30   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   31   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   32   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   1688   33   0.61729634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   34   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   35   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   36   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   37   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   38   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_249   39   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_382   40   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.35
