###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   englandsfightwit00wals_354, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 7, non-zero: 7, borderline: 5, overall act: 3.942, act diff: 1.942, ratio: 0.493
#   pulse 2: activated: 264007, non-zero: 264007, borderline: 264002, overall act: 38700.268, act diff: 38696.326, ratio: 1.000
#   pulse 3: activated: 264008, non-zero: 264008, borderline: 263975, overall act: 38701.275, act diff: 1.006, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264008
#   final overall activation: 38701.3
#   number of spread. activ. pulses: 3
#   running time: 46422

###################################
# top k results in TREC format: 

2   rel2-3   ireland   1   0.99999976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   england   2   0.32194734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   clifton house   3   0.30134338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   cangort   4   0.2940641   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   cangort park   5   0.29000908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   sopwell hall   6   0.28856325   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   kilcomin   7   0.28484893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   ballincor   8   0.2821709   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   corolanty   9   0.27993873   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   loughkeen   10   0.2788012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   ivy hall   11   0.27777874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   shinrone   12   0.27740142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   modreeny   13   0.27251217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   cloughjordan   14   0.26896018   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   fort nisbett   15   0.26724818   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   borrisokane   16   0.2611253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   dublin   17   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
2   rel2-3   berkeley   18   0.07620445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-3   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   ireland   3   0.99999976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   1641   4   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_291   5   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_411   6   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_130   7   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   fastiecclesiaeh02cottgoog_20   8   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   compendiumofhist02lawl_76   9   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_345   10   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   calendarstatepa12offigoog_335   11   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   castlesofireland00adamiala_301   12   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   confederationofk00meeh_10   13   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   englishinireland03frou_309   14   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   englishinireland03frou_118   15   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_413   16   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   economichistoryo00obri_321   17   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   statechurcheskin00allerich_587   18   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   memoirsandcorre02castgoog_42   19   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   revolutionaryir00mahagoog_100   20   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   historymoderneu11russgoog_386   21   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   historyofdiocese02healiala_23   22   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   historyofireland3_00will_307   23   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   irishtangleandwa1920john_53   24   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   constitutionalhi03hall_414   25   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   proceedingsofro22roya_307   26   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   cu31924091770861_319   27   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   cu31924091770861_286   28   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   cu31924091786628_78   29   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   treatiseofexcheq02howa_421   30   0.5179872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   politicalstudies00broduoft_370   31   0.50727487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   politicalstudies00broduoft_382   32   0.50290865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_249   33   0.49564424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   historyirelanda00smilgoog_120   34   0.49394864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   cu31924091770861_873   35   0.49394864   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_315   36   0.49184215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   irelandsaintpatr00morriala_17   37   0.49091214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   causeofirelandpl00orei_432   38   0.49091214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   constitutionalhi03hall_415   39   0.48511335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel2-3   irelandundercomm02dunluoft_177   40   0.48511335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.5-loc_th0.4
