###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 19, overall act: 9.019, act diff: 5.019, ratio: 0.556
#   pulse 2: activated: 1052138, non-zero: 1052138, borderline: 1052129, overall act: 153560.378, act diff: 153551.359, ratio: 1.000
#   pulse 3: activated: 1052222, non-zero: 1052222, borderline: 1052098, overall act: 153577.091, act diff: 16.713, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052222
#   final overall activation: 153577.1
#   number of spread. activ. pulses: 3
#   running time: 175739

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   england   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   dublin   3   0.99998856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   london   4   0.69957477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   scotland   5   0.64845073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   cangort   6   0.54949653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   cangort park   7   0.54281414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   strafford   8   0.54267496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   limerick   9   0.5414639   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   sopwell hall   10   0.53592527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   kilcomin   11   0.5332182   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   ballincor   12   0.5307397   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   corolanty   13   0.5276561   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   loughkeen   14   0.5237755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   shinrone   15   0.5235583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   ivy hall   16   0.5219562   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   cloughjordan   17   0.5063596   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   fort nisbett   18   0.50191313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   munster   19   0.41922724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
2   rel4-1   fairfax   20   0.41709816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   england   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   1641   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   dublin   8   0.99998856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   london   9   0.69957477   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   10   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   11   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   12   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   thomasharrisonr00wessgoog_83   13   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_315   14   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   strangerinirelan00carr_91   15   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   redeemerstearsw00urwigoog_21   16   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   calendarstatepa00levagoog_361   17   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   18   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_172   19   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   cu31924091770861_873   20   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_358   21   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_67   22   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   23   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_434   24   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_382   25   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_249   26   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_120   27   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_385   28   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_229   29   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_21   30   0.6592293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   compendiumofhist02lawl_189   31   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   irelandonehundre00walsrich_38   32   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   landwarinireland00godk_205   33   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   commercialrestra00hely_255   34   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   viceroyspostbag02macdgoog_266   35   0.65841407   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   historyengland00macagoog_517   36   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   cu31924027975733_414   37   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   dublinreview14londuoft_194   38   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   irelandsaintpatr00morriala_201   39   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel4-1   pt3historyofirel00wriguoft_306   40   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.4
