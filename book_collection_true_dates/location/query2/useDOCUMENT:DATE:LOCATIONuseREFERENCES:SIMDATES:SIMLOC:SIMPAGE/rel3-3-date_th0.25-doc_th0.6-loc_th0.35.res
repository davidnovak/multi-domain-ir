###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1052130, non-zero: 1052130, borderline: 1052122, overall act: 147136.591, act diff: 147128.132, ratio: 1.000
#   pulse 3: activated: 1059583, non-zero: 1059583, borderline: 1059540, overall act: 147962.358, act diff: 825.767, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059583
#   final overall activation: 147962.4
#   number of spread. activ. pulses: 3
#   running time: 186577

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.99999994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   england   2   0.99944955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   dublin   3   0.92334384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   clifton house   4   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   cangort   5   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   cangort park   6   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   shinrone   7   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   cloughjordan   8   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   kilcomin   9   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   corolanty   10   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   sopwell hall   11   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   ballincor   12   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   loughkeen   13   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   ivy hall   14   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   modreeny   15   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   fort nisbett   16   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   strafford   17   0.5442673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   borrisokane   18   0.46007994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   miltown park   19   0.45539412   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
2   rel3-3   brosna   20   0.4483522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   ireland   4   0.99999994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   1641   5   0.9999997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   england   6   0.99944955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   dublin   7   0.92334384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   clifton house   8   0.84254056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   cangort   9   0.8421414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   cangort park   10   0.83992887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   shinrone   11   0.8311356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   cloughjordan   12   0.81634295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   kilcomin   13   0.780551   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   corolanty   14   0.7776313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   sopwell hall   15   0.7735711   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_414   16   0.74639887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_601   17   0.728054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_432   18   0.7246256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_96   19   0.71948135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_602   20   0.7049586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   ballincor   21   0.7015282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   loughkeen   22   0.69390583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   ivy hall   23   0.6914815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   modreeny   24   0.6823186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_355   25   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   fort nisbett   26   0.67336094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_68   27   0.6682488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_430   28   0.662846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_599   29   0.6604816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_357   30   0.65807384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_435   31   0.65066427   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_25   32   0.6488376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_66   33   0.6430885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_360   34   0.64062184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_352   35   0.6333924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   36   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   irelandsaintpatr00morriala_200   37   0.63147676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_23   38   0.6288822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   39   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   40   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
