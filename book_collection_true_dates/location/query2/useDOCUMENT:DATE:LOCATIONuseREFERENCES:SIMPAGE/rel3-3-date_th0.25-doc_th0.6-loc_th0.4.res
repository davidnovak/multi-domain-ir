###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1015987, non-zero: 1015987, borderline: 1015980, overall act: 141070.305, act diff: 141061.847, ratio: 1.000
#   pulse 3: activated: 1016014, non-zero: 1016014, borderline: 1015985, overall act: 141084.023, act diff: 13.718, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1016014
#   final overall activation: 141084.0
#   number of spread. activ. pulses: 3
#   running time: 138223

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.99999577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   england   2   0.9988005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   dublin   3   0.7097276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   strafford   4   0.5307653   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   scotland   5   0.19915377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   rome   6   0.16713683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   munster   7   0.1438645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   tyrconnel   8   0.117382236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   londonderry   9   0.114431486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   dublin castle   10   0.11212991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   kingdom   11   0.11171436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   borlase   12   0.11007648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   essex   13   0.10974911   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   netherlands   14   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   belgium   15   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   germany   16   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   surrey   17   0.08371859   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   enniskillen   18   0.072389014   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   city   19   0.07066138   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
2   rel3-3   kinsale   20   0.060879152   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   ireland   4   0.99999577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   1641   5   0.9999889   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   england   6   0.9988005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_414   7   0.74639887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_432   8   0.7246256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   dublin   9   0.7097276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_601   10   0.69860685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   viceroysofirelan00omahuoft_96   11   0.682079   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_355   12   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_430   13   0.662846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_357   14   0.65807384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_66   15   0.6430885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_68   16   0.6416897   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   calendarstatepa00levagoog_360   17   0.64062184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_352   18   0.6333924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   irelandsaintpatr00morriala_200   19   0.63147676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_602   20   0.62738174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_315   21   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_350   22   0.6226687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_416   23   0.62259763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_599   24   0.6205621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   calendarstatepa00levagoog_361   25   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_415   26   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_67   27   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_431   28   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_358   29   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   completehistoryo0306kenn_434   30   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_383   31   0.6109615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_359   32   0.60880375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_249   33   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_382   34   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_229   35   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   calendarstatepa12offigoog_21   36   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_119   37   0.6064698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_120   38   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   dublinreview14londuoft_195   39   0.6054997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 2   rel3-3   studentshistoryo03garduoft_42   40   0.60477144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
