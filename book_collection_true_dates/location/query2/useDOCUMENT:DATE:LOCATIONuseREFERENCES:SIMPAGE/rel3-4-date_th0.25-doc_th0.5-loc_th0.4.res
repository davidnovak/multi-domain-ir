###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 28, non-zero: 28, borderline: 25, overall act: 10.441, act diff: 7.441, ratio: 0.713
#   pulse 2: activated: 1052100, non-zero: 1052100, borderline: 1052092, overall act: 144367.558, act diff: 144357.117, ratio: 1.000
#   pulse 3: activated: 1052269, non-zero: 1052269, borderline: 1052117, overall act: 144450.303, act diff: 82.745, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052269
#   final overall activation: 144450.3
#   number of spread. activ. pulses: 3
#   running time: 200159

###################################
# top k results in TREC format: 

2   rel3-4   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   england   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   dublin   3   0.9999998   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   scotland   4   0.8458639   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   london   5   0.8137906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   strafford   6   0.7260148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   limerick   7   0.61039984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   drogheda   8   0.38290414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   munster   9   0.36135396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   fairfax   10   0.35737962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   spain   11   0.34832144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   kingdom   12   0.31576222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   derry   13   0.29884988   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   londonderry   14   0.28931987   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   dublin castle   15   0.28768042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   europe   16   0.24412437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   bristol   17   0.2129747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   wexford   18   0.20624396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   belfast   19   0.20187038   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
2   rel3-4   tyrconnel   20   0.19647072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-4   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   cu31924029563875_157   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   dublin   7   0.9999998   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   1845   8   0.9934246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   scotland   9   0.8458639   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   london   10   0.8137906   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   strafford   11   0.7260148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_601   12   0.7221464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   constitutionalhi03hall_415   13   0.7205244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   viceroysofirelan00omahuoft_96   14   0.71851975   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   historyforreadyr03larnuoft_236   15   0.7150031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   historyforreadyr03larnuoft_235   16   0.70634127   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   constitutionalhi03hall_414   17   0.6976935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_432   18   0.6764553   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_613   19   0.673916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   revisedreportsb12courgoog_142   20   0.66280735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_599   21   0.65944695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   revolutionaryir00mahagoog_99   22   0.6534515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   revisedreportsb12courgoog_144   23   0.65281224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_25   24   0.65230495   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_381   25   0.6481298   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   historyirishper01maddgoog_68   26   0.6447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   calendarstatepa12offigoog_636   27   0.64241105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   completehistoryo0306kenn_435   28   0.64188874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   englishlawirisht00gibbrich_19   29   0.6400371   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   landwarinireland00godk_231   30   0.64001685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   historyengland00macagoog_518   31   0.63086945   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   encyclopaediaofl07polluoft_78   32   0.6295675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   cu31924091770861_287   33   0.6291842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   historyforreadyr03larnuoft_237   34   0.62857103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   selectspeecheswi00cannuoft_357   35   0.6257613   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_430   36   0.625643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_23   37   0.62492704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   strangerinirelan00carr_90   38   0.6167902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_602   39   0.6146017   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
# 2   rel3-4   encyclopaediaofl07polluoft_76   40   0.61246574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.4
