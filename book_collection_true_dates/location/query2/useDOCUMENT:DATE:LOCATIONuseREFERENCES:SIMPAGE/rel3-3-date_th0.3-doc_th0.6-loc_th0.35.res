###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 12, overall act: 8.458, act diff: 5.458, ratio: 0.645
#   pulse 2: activated: 1052086, non-zero: 1052086, borderline: 1052078, overall act: 147125.989, act diff: 147117.531, ratio: 1.000
#   pulse 3: activated: 1052121, non-zero: 1052121, borderline: 1052086, overall act: 147143.103, act diff: 17.114, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052121
#   final overall activation: 147143.1
#   number of spread. activ. pulses: 3
#   running time: 145684

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   england   2   0.99944955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   dublin   3   0.92334384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   strafford   4   0.5442673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   dublin castle   5   0.2254641   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   scotland   6   0.20597184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   kingdom   7   0.19588166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   rome   8   0.16713683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   munster   9   0.14621823   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   earth   10   0.12246969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   londonderry   11   0.11970486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   tyrconnel   12   0.117382236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   borlase   13   0.117094524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   essex   14   0.11433557   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   loughborough   15   0.100260586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   belgium   16   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   netherlands   17   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   germany   18   0.09762845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   lisle   19   0.08605033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
2   rel3-3   surrey   20   0.08371859   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   ireland   4   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   1641   5   0.99999946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   england   6   0.99944955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   dublin   7   0.92334384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_414   8   0.74639887   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_601   9   0.728054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_432   10   0.7246256   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   viceroysofirelan00omahuoft_96   11   0.71948135   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   englandsfightwit00wals_355   12   0.6776363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_68   13   0.6682488   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_430   14   0.662846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_599   15   0.6604816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_357   16   0.65807384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_435   17   0.65066427   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_25   18   0.6488376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_66   19   0.6430885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_360   20   0.64062184   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   hallamsworks04halliala_602   21   0.63622767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_352   22   0.6333924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   studentshistoryo03garduoft_42   23   0.6329048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   irelandsaintpatr00morriala_200   24   0.63147676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_23   25   0.6288822   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   harleianmiscell00oldygoog_332   26   0.62778425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_315   27   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   strangerinirelan00carr_90   28   0.6230453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_350   29   0.6226687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_416   30   0.62259763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   calendarstatepa00levagoog_361   31   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_83   32   0.61850405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   thomasharrisonr00wessgoog_84   33   0.61828655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   constitutionalhi03hall_415   34   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirishper01maddgoog_67   35   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   historyirelanda00smilgoog_121   36   0.6140367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   causeofirelandpl00orei_431   37   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   selectspeecheswi00cannuoft_358   38   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   completehistoryo0306kenn_434   39   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 2   rel3-3   politicalstudies00broduoft_383   40   0.6109615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
