###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 9.762, act diff: 6.762, ratio: 0.693
#   pulse 2: activated: 227908, non-zero: 227908, borderline: 227903, overall act: 44880.734, act diff: 44870.972, ratio: 1.000
#   pulse 3: activated: 227939, non-zero: 227939, borderline: 227897, overall act: 44900.023, act diff: 19.289, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 227939
#   final overall activation: 44900.0
#   number of spread. activ. pulses: 3
#   running time: 65393

###################################
# top k results in TREC format: 

2   rel3-1   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   england   2   0.6523903   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   dublin   3   0.49642172   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   london   4   0.46493047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   strafford   5   0.3463362   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   city   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   town   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   limerick   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   europe   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   shoreditch   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   peacock   11   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   tyrconnel   12   0.09879347   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   germany   13   0.09797848   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   rome   14   0.09797848   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   belgium   15   0.09797848   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   netherlands   16   0.09797848   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   dublin castle   17   0.09112507   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   st thomas   18   0.07953368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   france   19   0.07953368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-1   berkeley   20   0.07896695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-1   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   1641   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   englandsfightwit00wals_355   6   0.68008155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   constitutionalhi03hall_415   7   0.67299247   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   england   8   0.6523903   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   viceroysofirelan00omahuoft_96   9   0.63442165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_431   10   0.6333031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   historicalreview02plow_30   11   0.59899795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   economichistoryo00obri_320   12   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   fastiecclesiaeh02cottgoog_21   13   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   fastiecclesiaeh02cottgoog_19   14   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   calendarstatepa12offigoog_336   15   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_414   16   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   englishinireland03frou_308   17   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_131   18   0.5884338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   historyirelanda00smilgoog_119   19   0.5785611   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   constitutionalhi03hall_413   20   0.5711393   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   politicalstudies00broduoft_369   21   0.5666405   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_292   22   0.56579196   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   cu31924091770861_320   23   0.56078666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_413   24   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   compendiumofhist02lawl_76   25   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_411   26   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_345   27   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   proceedingsofro22roya_307   28   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   cu31924091770861_319   29   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   fastiecclesiaeh02cottgoog_20   30   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   cu31924091770861_286   31   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   treatiseofexcheq02howa_130   32   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   cu31924091786628_78   33   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   castlesofireland00adamiala_301   34   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   confederationofk00meeh_10   35   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   englishinireland03frou_309   36   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   calendarstatepa12offigoog_335   37   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   englishinireland03frou_118   38   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   causeofirelandpl00orei_291   39   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-1   economichistoryo00obri_321   40   0.53904253   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
