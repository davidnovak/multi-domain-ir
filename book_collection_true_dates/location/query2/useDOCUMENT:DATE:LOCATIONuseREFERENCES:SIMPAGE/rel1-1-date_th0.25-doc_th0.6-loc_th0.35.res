###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   englandsfightwit00wals_354, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 5, non-zero: 5, borderline: 4, overall act: 2.631, act diff: 1.631, ratio: 0.620
#   pulse 2: activated: 221864, non-zero: 221864, borderline: 221862, overall act: 25528.708, act diff: 25526.077, ratio: 1.000
#   pulse 3: activated: 221864, non-zero: 221864, borderline: 221862, overall act: 25528.708, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221864
#   final overall activation: 25528.7
#   number of spread. activ. pulses: 3
#   running time: 30964

###################################
# top k results in TREC format: 

2   rel1-1   ireland   1   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 2   rel1-1   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   englandsfightwit00wals_355   2   0.59637725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   englandsfightwit00wals_353   3   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   ireland   4   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   1641   5   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   creelofirishstor00barliala_138   6   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_8   7   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_82   8   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   britishmammalsat00johnrich_278   9   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   vikingsbalticat06dasegoog_209   10   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_4   11   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   lifeandlettersg02towngoog_143   12   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   lettersandjourna02bailuoft_176   13   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   lifeandlettersg02towngoog_155   14   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_23   15   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_11   16   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_17   17   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_13   18   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_44   19   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_43   20   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   naturalistscabin02smituoft_223   21   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   lifeandlettersg02towngoog_194   22   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   vikingsbalticat06dasegoog_211   23   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   speeches00philiala_92   24   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_210   25   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_206   26   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_274   27   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_272   28   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_271   29   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_270   30   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   lettersconcerni00clargoog_278   31   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_278   32   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_277   33   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_262   34   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   sexagenarianorre01belo_202   35   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_268   36   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_267   37   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_264   38   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_252   39   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 2   rel1-1   doingmybitforire00skiniala_256   40   0.22703262   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
