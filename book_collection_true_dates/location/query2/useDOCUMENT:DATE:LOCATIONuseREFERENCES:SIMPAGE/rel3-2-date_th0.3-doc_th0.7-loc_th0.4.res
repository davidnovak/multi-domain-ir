###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 27, non-zero: 27, borderline: 24, overall act: 10.124, act diff: 7.124, ratio: 0.704
#   pulse 2: activated: 264009, non-zero: 264009, borderline: 264003, overall act: 46262.484, act diff: 46252.359, ratio: 1.000
#   pulse 3: activated: 264009, non-zero: 264009, borderline: 264003, overall act: 46262.484, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264009
#   final overall activation: 46262.5
#   number of spread. activ. pulses: 3
#   running time: 66250

###################################
# top k results in TREC format: 

2   rel3-2   ireland   1   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   dublin   2   0.39932328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   london   3   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   england   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   shoreditch   5   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   town   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   europe   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   city   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   limerick   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-2   peacock   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel3-2   englandsfightwit00wals_354   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924029563875_157   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   ireland   4   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   englandsfightwit00wals_355   5   0.6719688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_601   6   0.6548349   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   1641   7   0.62436765   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   hallamsworks04halliala_599   8   0.61609036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_291   9   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_411   10   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_130   11   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   fastiecclesiaeh02cottgoog_20   12   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   compendiumofhist02lawl_76   13   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   englishinireland03frou_309   14   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   englishinireland03frou_118   15   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_345   16   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   calendarstatepa12offigoog_335   17   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_413   18   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   economichistoryo00obri_321   19   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   revolutionaryir00mahagoog_100   20   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historymoderneu11russgoog_386   21   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   statechurcheskin00allerich_587   22   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyofdiocese02healiala_23   23   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   memoirsandcorre02castgoog_42   24   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   irishtangleandwa1920john_53   25   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   constitutionalhi03hall_414   26   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyofireland3_00will_307   27   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   proceedingsofro22roya_307   28   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_319   29   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_286   30   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091786628_78   31   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   castlesofireland00adamiala_301   32   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   confederationofk00meeh_10   33   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   treatiseofexcheq02howa_421   34   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   politicalstudies00broduoft_370   35   0.59268457   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   politicalstudies00broduoft_382   36   0.588027   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_249   37   0.5802522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   historyirelanda00smilgoog_120   38   0.57843274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   cu31924091770861_873   39   0.57843274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-2   causeofirelandpl00orei_315   40   0.5761701   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
