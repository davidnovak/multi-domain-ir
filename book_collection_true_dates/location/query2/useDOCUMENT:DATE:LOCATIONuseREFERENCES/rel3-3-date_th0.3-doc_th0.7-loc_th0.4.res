###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 9, non-zero: 9, borderline: 6, overall act: 5.685, act diff: 2.685, ratio: 0.472
#   pulse 2: activated: 1015986, non-zero: 1015986, borderline: 1015979, overall act: 141067.944, act diff: 141062.259, ratio: 1.000
#   pulse 3: activated: 1015986, non-zero: 1015986, borderline: 1015979, overall act: 141067.944, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1015986
#   final overall activation: 141067.9
#   number of spread. activ. pulses: 3
#   running time: 106384

###################################
# top k results in TREC format: 

2   rel3-3   ireland   1   0.80031395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-3   england   2   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-3   dublin   3   0.38038954   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
2   rel3-3   strafford   4   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel3-3   viceroysofirelan00omahuoft_97   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   ireland   4   0.80031395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_315   5   0.6241031   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   calendarstatepa00levagoog_361   6   0.62015265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   constitutionalhi03hall_415   7   0.6172136   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_67   8   0.61489016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_431   9   0.61383706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   selectspeecheswi00cannuoft_358   10   0.6137229   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   completehistoryo0306kenn_434   11   0.61180645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   causeofirelandpl00orei_249   12   0.60823846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_382   13   0.60815203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   historyirishper01maddgoog_229   14   0.6072254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   calendarstatepa12offigoog_21   15   0.6067627   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_120   16   0.60632265   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   studentshistoryo03garduoft_42   17   0.60477144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   harleianmiscell00oldygoog_332   18   0.6039614   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   irelandsaintpatr00morriala_201   19   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   dublinreview14londuoft_194   20   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   pt3historyofirel00wriguoft_306   21   0.60325634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_351   22   0.6016643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   historyirelanda00smilgoog_312   23   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   hallamsworks04halliala_612   24   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   worksrighthonor37burkgoog_439   25   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   fairfaxcorrespon01johniala_870   26   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   methodistmagazin1834meth_610   27   0.5956615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   publications13irisuoft_16   28   0.5900143   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   thomasharrisonr00wessgoog_83   29   0.5846675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   calendarstatepa12offigoog_373   30   0.58458275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   fairfaxcorrespon01johniala_705   31   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   politicalstudies00broduoft_380   32   0.5843118   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   collinsspeerageof07coll_161   33   0.5843117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   historyforreadyr03larnuoft_235   34   0.58422774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   projectofcommonw00curt_470   35   0.57756805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   memoirsandcorre02castgoog_67   36   0.57617295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   confederationofk00meeh_10   37   0.57580715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   cu31924091770861_319   38   0.57580715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   fastiecclesiaeh02cottgoog_20   39   0.57580715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel3-3   statechurcheskin00allerich_587   40   0.57580715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
