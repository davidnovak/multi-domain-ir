###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 20, non-zero: 20, borderline: 18, overall act: 5.887, act diff: 3.887, ratio: 0.660
#   pulse 2: activated: 264006, non-zero: 264006, borderline: 264001, overall act: 30606.456, act diff: 30600.569, ratio: 1.000
#   pulse 3: activated: 264006, non-zero: 264006, borderline: 264001, overall act: 30606.456, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 264006
#   final overall activation: 30606.5
#   number of spread. activ. pulses: 3
#   running time: 63423

###################################
# top k results in TREC format: 

2   rel2-5   ireland   1   0.48682883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   dublin   2   0.39932328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   london   3   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   england   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   shoreditch   5   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   town   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   europe   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   city   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   limerick   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel2-5   peacock   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel2-5   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   ireland   3   0.48682883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   1641   4   0.44794905   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_291   5   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_411   6   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_130   7   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   fastiecclesiaeh02cottgoog_20   8   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   compendiumofhist02lawl_76   9   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_345   10   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   calendarstatepa12offigoog_335   11   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   castlesofireland00adamiala_301   12   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   confederationofk00meeh_10   13   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   englishinireland03frou_309   14   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   englishinireland03frou_118   15   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_413   16   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   economichistoryo00obri_321   17   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   statechurcheskin00allerich_587   18   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   memoirsandcorre02castgoog_42   19   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   revolutionaryir00mahagoog_100   20   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   historymoderneu11russgoog_386   21   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   historyofireland3_00will_307   22   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   historyofdiocese02healiala_23   23   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   proceedingsofro22roya_307   24   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   irishtangleandwa1920john_53   25   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   constitutionalhi03hall_414   26   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091770861_319   27   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091770861_286   28   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091786628_78   29   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   treatiseofexcheq02howa_421   30   0.4360872   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_370   31   0.42702368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   politicalstudies00broduoft_382   32   0.4233431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_249   33   0.41723633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   historyirelanda00smilgoog_120   34   0.41581392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   cu31924091770861_873   35   0.41581392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_315   36   0.4140484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   irelandsaintpatr00morriala_17   37   0.41326943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   causeofirelandpl00orei_432   38   0.41326943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   constitutionalhi03hall_415   39   0.40842012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel2-5   irelandundercomm02dunluoft_177   40   0.40842012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
