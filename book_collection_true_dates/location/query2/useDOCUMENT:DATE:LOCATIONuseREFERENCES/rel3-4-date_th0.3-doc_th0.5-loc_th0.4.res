###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 22, non-zero: 22, borderline: 19, overall act: 7.668, act diff: 4.668, ratio: 0.609
#   pulse 2: activated: 1052098, non-zero: 1052098, borderline: 1052090, overall act: 144365.142, act diff: 144357.474, ratio: 1.000
#   pulse 3: activated: 1052209, non-zero: 1052209, borderline: 1052061, overall act: 144384.250, act diff: 19.107, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1052209
#   final overall activation: 144384.2
#   number of spread. activ. pulses: 3
#   running time: 182066

###################################
# top k results in TREC format: 

2   rel3-4   ireland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   england   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   dublin   3   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   scotland   4   0.76292497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   london   5   0.7382748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   limerick   6   0.61039984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   strafford   7   0.50045407   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   drogheda   8   0.38290414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   munster   9   0.36135396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   fairfax   10   0.35737962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   spain   11   0.34832144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   kingdom   12   0.31576222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   derry   13   0.29884988   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   londonderry   14   0.28931987   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   dublin castle   15   0.28768042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   europe   16   0.24412437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   bristol   17   0.2129747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   wexford   18   0.20624396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   belfast   19   0.20187038   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
2   rel3-4   tyrconnel   20   0.19647072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 2   rel3-4   england   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   1641   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   hallamsworks04halliala_600   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   viceroysofirelan00omahuoft_97   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   ireland   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   cu31924029563875_157   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   dublin   7   0.99999964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   1845   8   0.9934246   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   scotland   9   0.76292497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   london   10   0.7382748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   limerick   11   0.61039984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   studentshistoryo03garduoft_42   12   0.6013532   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   harleianmiscell00oldygoog_332   13   0.5960795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_24   14   0.5889065   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   thomasharrisonr00wessgoog_83   15   0.5870921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   redeemerstearsw00urwigoog_21   16   0.58627963   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   strangerinirelan00carr_91   17   0.5800713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_315   18   0.5771714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   calendarstatepa00levagoog_361   19   0.5751702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   historyirelanda00smilgoog_172   20   0.5741105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_431   21   0.57091826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   historyirishper01maddgoog_67   22   0.5707552   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   constitutionalhi03hall_415   23   0.57052505   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   selectspeecheswi00cannuoft_358   24   0.5678152   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   irelandsfightfor1919cree_101   25   0.5672616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   completehistoryo0306kenn_434   26   0.56657606   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   historyengland00macagoog_517   27   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   cu31924027975733_414   28   0.5662921   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   cu31924091770861_873   29   0.5660654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_385   30   0.5639206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   historyirishper01maddgoog_229   31   0.5624587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   calendarstatepa12offigoog_21   32   0.5618158   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   viceroyspostbag02macdgoog_266   33   0.5610784   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   landwarinireland00godk_205   34   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   irelandonehundre00walsrich_38   35   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   compendiumofhist02lawl_189   36   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   commercialrestra00hely_255   37   0.5605369   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   causeofirelandpl00orei_249   38   0.55935603   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   historyofbritish00colluoft_221   39   0.55848306   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
# 2   rel3-4   politicalstudies00broduoft_382   40   0.5582852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.5-loc_th0.4
