###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 20, non-zero: 20, borderline: 18, overall act: 5.508, act diff: 3.508, ratio: 0.637
#   pulse 2: activated: 221879, non-zero: 221879, borderline: 221876, overall act: 30773.561, act diff: 30768.053, ratio: 1.000
#   pulse 3: activated: 221879, non-zero: 221879, borderline: 221876, overall act: 30773.561, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 221879
#   final overall activation: 30773.6
#   number of spread. activ. pulses: 3
#   running time: 57863

###################################
# top k results in TREC format: 

2   rel2-4   ireland   1   0.55900216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   london   2   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   dublin   3   0.3125406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   strafford   4   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   england   5   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   city   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   europe   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   shoreditch   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   town   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   limerick   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
2   rel2-4   peacock   11   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 2   rel2-4   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   viceroysofirelan00omahuoft_97   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   ireland   3   0.55900216   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   london   4   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   dublin   5   0.3125406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   creelofirishstor00barliala_138   6   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_8   7   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_82   8   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   britishmammalsat00johnrich_278   9   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_209   10   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_4   11   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_143   12   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   lettersandjourna02bailuoft_176   13   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_155   14   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_23   15   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_11   16   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_17   17   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_13   18   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_44   19   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_43   20   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   naturalistscabin02smituoft_223   21   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   lifeandlettersg02towngoog_194   22   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   vikingsbalticat06dasegoog_211   23   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   speeches00philiala_92   24   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_210   25   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_206   26   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_274   27   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_272   28   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_271   29   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_270   30   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   lettersconcerni00clargoog_278   31   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_278   32   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_277   33   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_262   34   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   sexagenarianorre01belo_202   35   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_268   36   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_267   37   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_264   38   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_252   39   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
# 2   rel2-4   doingmybitforire00skiniala_256   40   0.27244326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.4
