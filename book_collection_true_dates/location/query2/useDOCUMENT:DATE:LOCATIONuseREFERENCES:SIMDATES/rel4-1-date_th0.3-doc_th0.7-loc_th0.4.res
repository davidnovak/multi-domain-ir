###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (4): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   viceroysofirelan00omahuoft_97, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 4, non-zero: 4, borderline: 4, overall act: 4.000, act diff: 4.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 19, overall act: 9.019, act diff: 5.019, ratio: 0.556
#   pulse 2: activated: 1052102, non-zero: 1052102, borderline: 1052093, overall act: 153550.019, act diff: 153541.000, ratio: 1.000
#   pulse 3: activated: 1059503, non-zero: 1059503, borderline: 1059492, overall act: 154510.540, act diff: 960.521, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1059503
#   final overall activation: 154510.5
#   number of spread. activ. pulses: 3
#   running time: 129598

###################################
# top k results in TREC format: 

2   rel4-1   ireland   1   0.8812601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   dublin   2   0.51782155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   england   3   0.45307326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   london   4   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   strafford   5   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   town   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   europe   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   shoreditch   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   peacock   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   limerick   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
2   rel4-1   city   11   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 2   rel4-1   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroysofirelan00omahuoft_97   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   englandsfightwit00wals_354   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   ireland   5   0.8812601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   1641   6   0.77572834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   studentshistoryo03garduoft_42   7   0.6908953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   harleianmiscell00oldygoog_332   8   0.68371445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_24   9   0.6814029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   thomasharrisonr00wessgoog_83   10   0.6802687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_315   11   0.6771716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   strangerinirelan00carr_91   12   0.67658985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   redeemerstearsw00urwigoog_21   13   0.6741094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa00levagoog_361   14   0.6720273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   constitutionalhi03hall_415   15   0.6704381   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_172   16   0.6688751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924091770861_873   17   0.66856164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   selectspeecheswi00cannuoft_358   18   0.6665563   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_67   19   0.6664035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_431   20   0.66452545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   completehistoryo0306kenn_434   21   0.66424525   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_382   22   0.6640541   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   causeofirelandpl00orei_249   23   0.6634409   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924091770861_898   24   0.66313225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirelanda00smilgoog_120   25   0.6615774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_385   26   0.6614478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyirishper01maddgoog_229   27   0.6595417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   calendarstatepa12offigoog_21   28   0.6592293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   landwarinireland00godk_205   29   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandonehundre00walsrich_38   30   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   compendiumofhist02lawl_189   31   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   commercialrestra00hely_255   32   0.6588791   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   viceroyspostbag02macdgoog_266   33   0.65841407   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyengland00macagoog_517   34   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   cu31924027975733_414   35   0.6582361   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   dublinreview14londuoft_194   36   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   irelandsaintpatr00morriala_201   37   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   pt3historyofirel00wriguoft_306   38   0.6580043   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   politicalstudies00broduoft_351   39   0.65477914   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 2   rel4-1   historyofbritish00colluoft_221   40   0.6499617   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
