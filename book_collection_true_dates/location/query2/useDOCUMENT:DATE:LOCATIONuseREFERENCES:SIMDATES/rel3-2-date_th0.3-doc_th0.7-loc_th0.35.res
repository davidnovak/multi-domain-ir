###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   englandsfightwit00wals_354, 1
#   cu31924029563875_157, 1
#   hallamsworks04halliala_600, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 21, non-zero: 21, borderline: 18, overall act: 7.352, act diff: 4.352, ratio: 0.592
#   pulse 2: activated: 879909, non-zero: 879909, borderline: 879901, overall act: 112381.582, act diff: 112374.230, ratio: 1.000
#   pulse 3: activated: 888292, non-zero: 888292, borderline: 888282, overall act: 113346.664, act diff: 965.082, ratio: 0.009

###################################
# spreading activation process summary: 
#   final number of activated nodes: 888292
#   final overall activation: 113346.7
#   number of spread. activ. pulses: 3
#   running time: 137304

###################################
# top k results in TREC format: 

2   rel3-2   ireland   1   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   dublin   2   0.39932328   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   london   3   0.36032423   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   england   4   0.24491866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   shoreditch   5   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   peacock   6   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   europe   7   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   town   8   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   limerick   9   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
2   rel3-2   city   10   0.108621895   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 2   rel3-2   cu31924029563875_157   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   hallamsworks04halliala_600   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   englandsfightwit00wals_354   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   1641   4   0.77572834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   ireland   5   0.7746675   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_24   6   0.6349712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_432   7   0.6323343   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   strangerinirelan00carr_91   8   0.63231665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   cu31924091770861_873   9   0.62753767   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   historyofireland02daltuoft_409   10   0.619015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   olivercromwell00rossgoog_134   11   0.619015   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   politicalstudies00broduoft_385   12   0.6183103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   bibliothecagrenv03grenrich_31   13   0.61736053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   compendiumofhist02lawl_189   14   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   irelandonehundre00walsrich_38   15   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   landwarinireland00godk_205   16   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   commercialrestra00hely_255   17   0.61627054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   viceroyspostbag02macdgoog_266   18   0.61533123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   historyengland00macagoog_517   19   0.6124737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   cu31924027975733_414   20   0.6124737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   socialstateofgre00berm_196   21   0.6066881   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   historyofdiocese02healiala_23   22   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   castlesofireland00adamiala_301   23   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   confederationofk00meeh_10   24   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   cu31924091770861_319   25   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   historymoderneu11russgoog_386   26   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   englishinireland03frou_309   27   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   constitutionalhi03hall_414   28   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   englishinireland03frou_118   29   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   economichistoryo00obri_321   30   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   fastiecclesiaeh02cottgoog_20   31   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_413   32   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   statechurcheskin00allerich_587   33   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_411   34   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   memoirsandcorre02castgoog_42   35   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   proceedingsofro22roya_307   36   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   compendiumofhist02lawl_76   37   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   treatiseofexcheq02howa_130   38   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_345   39   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
# 2   rel3-2   causeofirelandpl00orei_291   40   0.6040615   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.35
