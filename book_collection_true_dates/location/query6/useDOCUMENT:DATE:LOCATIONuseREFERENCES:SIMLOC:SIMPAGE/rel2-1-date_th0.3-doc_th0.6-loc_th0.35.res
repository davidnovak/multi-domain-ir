###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   apoliticalhisto03reidgoog_64, 1
#   cu31924032471116_269, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 19, non-zero: 19, borderline: 17, overall act: 6.456, act diff: 4.456, ratio: 0.690
#   pulse 2: activated: 829932, non-zero: 829932, borderline: 829929, overall act: 112269.036, act diff: 112262.580, ratio: 1.000
#   pulse 3: activated: 829932, non-zero: 829932, borderline: 829929, overall act: 112269.036, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 829932
#   final overall activation: 112269.0
#   number of spread. activ. pulses: 3
#   running time: 148565

###################################
# top k results in TREC format: 

6   rel2-1   england   1   0.54243803   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   newton blossomville   2   0.24775805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   filgrave   3   0.24471736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   turvey   4   0.23906165   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   sherington   5   0.23863113   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   easton maudit   6   0.23679769   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   tyringham   7   0.2362757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   north   8   0.23413575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   chicheley   9   0.23379883   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   hardmead   10   0.2320831   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   bozeat   11   0.23052697   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   harrold   12   0.22795807   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   united states   13   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   europe   14   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   new orleans   15   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   great britain   16   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   boston   17   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   south   18   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
6   rel2-1   new york   19   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 6   rel2-1   cu31924032471116_269   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   apoliticalhisto03reidgoog_64   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   cu31924032471116_270   3   0.5737689   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   england   4   0.54243803   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   apoliticalhisto03reidgoog_65   5   0.53463304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   cu31924032471116_268   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   apoliticalhisto03reidgoog_63   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   piercepenniless00nashgoog_93   8   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   workscomprisingh02cowpuoft_385   9   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   piercepenniless00nashgoog_46   10   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   piercepenniless00nashgoog_90   11   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   lyricallifepoems00mass_223   12   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   ageneralabridgm30vinegoog_96   13   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   economicreview05unkngoog_70   14   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   memoirnathhawth00hawtrich_308   15   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   economicreview05unkngoog_64   16   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   principlesofecon01marsuoft_54   17   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   principlesofecon01marsuoft_88   18   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   marriage00welliala_104   19   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   saintjohnsfire00sude_140   20   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   cu31924084839194_405   21   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   ageneralabridgm30vinegoog_61   22   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   marriage00welliala_154   23   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   lyricallifepoems00mass_244   24   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   lyricallifepoems00mass_243   25   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   lyricallifepoems00mass_257   26   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   naturalhistoryof00smitrich_88   27   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   ahistorycritici09saingoog_432   28   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   northmenincumbe00ferggoog_22   29   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   arbordayitshisto00scharich_120   30   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   sirtom00oliprich_25   31   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   sirtom00oliprich_24   32   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   piercepenniless00nashgoog_19   33   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   farmersplanterse00johnrich_243   34   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   philosophicalcri02brom_277   35   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   farmersplanterse00johnrich_270   36   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   faithofourfather00gibbrich_33   37   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   historyantiquit02ling_57   38   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   principlesofecon01marsuoft_29   39   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
# 6   rel2-1   churchinscotland00luck_400   40   0.26475877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.6-loc_th0.35
