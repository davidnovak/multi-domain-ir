###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 569154, non-zero: 569154, borderline: 569147, overall act: 55802.206, act diff: 55794.780, ratio: 1.000
#   pulse 3: activated: 569185, non-zero: 569185, borderline: 569159, overall act: 55816.798, act diff: 14.592, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 569185
#   final overall activation: 55816.8
#   number of spread. activ. pulses: 3
#   running time: 185310

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.9988896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   russia   2   0.9976267   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   europe   3   0.8627592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   rome   4   0.47888094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   warsaw   5   0.41589758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   france   6   0.31439313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   saint petersburg   7   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   caucasus region   8   0.308114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   siberia   9   0.25307074   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   vienna   10   0.24539183   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   istanbul   11   0.22116522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   tiber   12   0.22116522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   bosporus   13   0.22116522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   vistula   14   0.18339217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   moscow   15   0.16476277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   great britain   16   0.13626097   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   alhambra   17   0.1057713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   prussia   18   0.10277367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   united states   19   0.097642064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
7   rel2-1   asia   20   0.085860476   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   poland   3   0.9988896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   1863   4   0.9983717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russia   5   0.9976267   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   1831   6   0.89031047   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   europe   7   0.8627592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_409   8   0.69826746   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_427   9   0.675061   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924031684685_794   10   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_425   11   0.5803344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924031684685_796   12   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924088053800_314   13   0.5750714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   14   0.5646878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   15   0.55283916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   someproblemspea00goog_190   16   0.54861355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924088053800_312   17   0.5471878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   18   0.54607207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   19   0.54430574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_310   20   0.54123604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   21   0.53788686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_191   22   0.53769445   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   23   0.5363658   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   24   0.53355694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   25   0.5321019   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   26   0.53196466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   27   0.5274982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   28   0.52340466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_42   29   0.52138835   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   30   0.51755995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   31   0.51458126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   32   0.5134345   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_423   33   0.5088833   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   outlineseuropea04beargoog_503   34   0.5088632   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_246   35   0.5078455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_390   36   0.49981302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   37   0.49698666   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   38   0.49499035   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   39   0.49149355   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 7   rel2-1   cu31924073899001_150   40   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
