###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 13, non-zero: 13, borderline: 11, overall act: 4.585, act diff: 2.585, ratio: 0.564
#   pulse 2: activated: 79732, non-zero: 79732, borderline: 79728, overall act: 9200.776, act diff: 9196.191, ratio: 1.000
#   pulse 3: activated: 79732, non-zero: 79732, borderline: 79728, overall act: 9200.776, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 79732
#   final overall activation: 9200.8
#   number of spread. activ. pulses: 3
#   running time: 25165

###################################
# top k results in TREC format: 

7   rel2-2   poland   1   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   russia   2   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   warsaw   3   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   europe   4   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   caucasus region   5   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   saint petersburg   6   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
7   rel2-2   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel2-2   cu31924088053800_313   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   1863   3   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   1831   4   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   poland   5   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   hereditarygenius1869galt_317   6   0.30223835   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   adictionaryasan01chrigoog_672   7   0.28720024   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   polishexperienc02hallgoog_20   8   0.28706107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   ulondonlibrarycat00univrich_658   9   0.28706107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   smithsonianmisce461905smit_190   10   0.28215465   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   materialypoisto00ligoog_106   11   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   downersofamerica00byudown_60   12   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   biographicalreg00gradgoog_40   13   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   journalofroyalin3186870roya_391   14   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   historyofsaintlov2scha_1061   15   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   conversationswit01seniuoft_271   16   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   historyanddiges13moorgoog_840   17   0.28062716   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   cu31924017141361_565   18   0.27443185   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   indianaindianans00dunn_75   19   0.25245732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   catalogofborrowd00museiala_23   20   0.25245732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   cu31924092508690_268   21   0.25245732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   advocateofpeace82amerrich_293   22   0.24905932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   materialypoisto00ligoog_242   23   0.24905932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   honoursregistero00univuoft_158   24   0.24905932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   anindextonorfol00ryegoog_80   25   0.24905932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   publications27oxfo_376   26   0.24777849   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   smithsonianmisce461905smit_188   27   0.24173571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   russia   28   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   biographicalreg00gradgoog_437   29   0.23946227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   proceedingsofses00jack_167   30   0.23669638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   ibis43brit_170   31   0.23669638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   briefhistoryofun00steeuoft_297   32   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   socialstateofgre00berm_258   33   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   briefhistoryofun00steeuoft_295   34   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   recessstudies00grangoog_375   35   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   pennycyclopaedi02goog_199   36   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   historyofcityofn00park_613   37   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   briefhistoryofun00steeuoft_275   38   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   briefhistoryofun00steeuoft_277   39   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel2-2   briefhistoryofun00steeuoft_293   40   0.22462544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.7-loc_th0.35
