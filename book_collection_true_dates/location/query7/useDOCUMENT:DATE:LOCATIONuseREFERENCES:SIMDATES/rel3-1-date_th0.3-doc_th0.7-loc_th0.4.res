###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164540, non-zero: 164540, borderline: 164532, overall act: 21208.878, act diff: 21201.711, ratio: 1.000
#   pulse 3: activated: 164540, non-zero: 164540, borderline: 164532, overall act: 23732.258, act diff: 2523.380, ratio: 0.106
#   pulse 4: activated: 184634, non-zero: 184634, borderline: 184625, overall act: 26641.604, act diff: 2909.346, ratio: 0.109
#   pulse 5: activated: 184634, non-zero: 184634, borderline: 184625, overall act: 27441.170, act diff: 799.566, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 184634
#   final overall activation: 27441.2
#   number of spread. activ. pulses: 5
#   running time: 45578

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   caucasus region   5   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   saint petersburg   6   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   1831   4   0.821096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   1830   5   0.64299804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   6   0.64239734   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   poland   7   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   8   0.6146509   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   1863   9   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   10   0.5770717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   11   0.5731333   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   12   0.56960064   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   13   0.5553744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   englishwomaninr00unkngoog_305   14   0.5512248   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_127   15   0.5498679   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   16   0.5498679   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   hazardsregister_11phil_125   17   0.5496203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_151   18   0.5464191   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   historicmorgancl00eame_250   19   0.5462077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   20   0.5443962   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   21   0.5424094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_245   22   0.5419724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_164   23   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   24   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   25   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_150   26   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   conquestsofcross02hodd_183   27   0.53861916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   recoverypoland00krycgoog_229   28   0.53861916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   operationsofsurg1902jaco_633   29   0.53861916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_121   30   0.53861916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   31   0.53786737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   historytenyears06blangoog_465   32   0.53323334   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   austriaviennapr00kohlgoog_511   33   0.5296762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   stormssunshineof01mackuoft_43   34   0.52785254   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   35   0.5216291   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   americanstatepap06unit_14   36   0.5207363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_33   37   0.5207018   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   conversationswit01seniuoft_271   38   0.51896554   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cataloguescient00unkngoog_358   39   0.51598775   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   historytenyears06blangoog_439   40   0.51570535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.7-loc_th0.4
