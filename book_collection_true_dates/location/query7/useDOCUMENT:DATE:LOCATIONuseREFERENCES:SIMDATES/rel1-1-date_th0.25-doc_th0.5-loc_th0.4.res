###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924088053800_313, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 10, non-zero: 10, borderline: 9, overall act: 2.929, act diff: 1.929, ratio: 0.659
#   pulse 2: activated: 79734, non-zero: 79734, borderline: 79731, overall act: 6178.126, act diff: 6175.197, ratio: 1.000
#   pulse 3: activated: 79734, non-zero: 79734, borderline: 79731, overall act: 6178.126, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 79734
#   final overall activation: 6178.1
#   number of spread. activ. pulses: 3
#   running time: 35238

###################################
# top k results in TREC format: 

7   rel1-1   poland   1   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   russia   2   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   warsaw   3   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   europe   4   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   caucasus region   5   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   saint petersburg   6   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
7   rel1-1   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 7   rel1-1   cu31924088053800_313   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   poland   2   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   1863   3   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   1831   4   0.30540052   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   russia   5   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   hereditarygenius1869galt_317   6   0.20549993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   adictionaryasan01chrigoog_672   7   0.19493648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   polishexperienc02hallgoog_20   8   0.19483899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   ulondonlibrarycat00univrich_658   9   0.19483899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   warsaw   10   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   europe   11   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   smithsonianmisce461905smit_190   12   0.19140452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   journalofroyalin3186870roya_391   13   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   materialypoisto00ligoog_106   14   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   downersofamerica00byudown_60   15   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   biographicalreg00gradgoog_40   16   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   historyofsaintlov2scha_1061   17   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   conversationswit01seniuoft_271   18   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   historyanddiges13moorgoog_840   19   0.19033648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   cu31924017141361_565   20   0.18601009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   indianaindianans00dunn_75   21   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   catalogofborrowd00museiala_23   22   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   cu31924092508690_268   23   0.17073293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   advocateofpeace82amerrich_293   24   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   materialypoisto00ligoog_242   25   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   honoursregistero00univuoft_158   26   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   anindextonorfol00ryegoog_80   27   0.16837965   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   publications27oxfo_376   28   0.16749324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   smithsonianmisce461905smit_188   29   0.16331558   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   biographicalreg00gradgoog_437   30   0.16174571   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   proceedingsofses00jack_167   31   0.15983713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   ibis43brit_170   32   0.15983713   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_297   33   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   socialstateofgre00berm_258   34   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_295   35   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   recessstudies00grangoog_375   36   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   pennycyclopaedi02goog_199   37   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   historyofcityofn00park_613   38   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_275   39   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
# 7   rel1-1   briefhistoryofun00steeuoft_277   40   0.15152436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.4
