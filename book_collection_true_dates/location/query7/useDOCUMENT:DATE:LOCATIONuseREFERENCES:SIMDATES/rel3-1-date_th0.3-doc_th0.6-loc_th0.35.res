###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 614181, non-zero: 614181, borderline: 614171, overall act: 63407.100, act diff: 63399.934, ratio: 1.000
#   pulse 3: activated: 614182, non-zero: 614182, borderline: 614171, overall act: 65923.839, act diff: 2516.739, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614182
#   final overall activation: 65923.8
#   number of spread. activ. pulses: 3
#   running time: 121025

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.7233732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   russia   2   0.54774433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   europe   3   0.48211133   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   saint petersburg   5   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   caucasus region   6   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
7   rel3-1   great britain   8   0.07411904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1863   4   0.7509751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1831   5   0.7430492   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   poland   6   0.7233732   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   7   0.61458045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   8   0.6136545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   9   0.61208916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   10   0.6105634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   11   0.60939145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   12   0.6083302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   13   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   14   0.6048824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   1830   15   0.6042413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   16   0.5931126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   17   0.59130925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   18   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   19   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   20   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   21   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   22   0.5774338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   23   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   24   0.57375264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   25   0.57361543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   26   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   lectureonsocialp00tochrich_22   27   0.5709702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   28   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   29   0.5684508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   30   0.56320757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   31   0.56042904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   cu31924073899001_127   32   0.5599677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   33   0.55509245   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   greatrussiaherac00sarouoft_160   34   0.5499332   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   russia   35   0.54774433   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   spiritrussiastu00masagoog_252   36   0.5470201   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_390   37   0.5466382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   englandduringthi00mart_819   38   0.54590875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   39   0.5421291   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
# 7   rel3-1   speechesdukewel02hazlgoog_492   40   0.54210037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.3-doc_th0.6-loc_th0.35
