###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164555, non-zero: 164555, borderline: 164547, overall act: 21213.338, act diff: 21206.171, ratio: 1.000
#   pulse 3: activated: 164555, non-zero: 164555, borderline: 164547, overall act: 21213.338, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164555
#   final overall activation: 21213.3
#   number of spread. activ. pulses: 3
#   running time: 48694

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   libiya   5   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   blue tower   6   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   as sifarah al qatariyah   7   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   damascus governorate   8   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   arnous   9   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   jaddat as salihiyah   10   0.29657817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   netherlands   11   0.296515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   tshili   12   0.29582384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   italy   13   0.29579344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   al hijaz   14   0.29556167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   cuba   15   0.2954348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   al hind   16   0.29480168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   orient palace   17   0.294122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   damascus   18   0.29412198   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   banama   19   0.29387063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
7   rel3-1   bab al barid   20   0.28923368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   poland   4   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   1863   5   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   1831   6   0.5860787   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   7   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   8   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_164   9   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_150   10   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   11   0.5387129   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   12   0.53849673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   13   0.51312554   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   14   0.5075743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   15   0.5030429   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   someproblemspea00goog_191   16   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_388   17   0.49966413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_428   18   0.49696192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   19   0.49603966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   20   0.49158984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   21   0.49135894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   22   0.48760217   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_398   23   0.4872372   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   sketchesinpoland00littrich_57   24   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   leavesfromdiaryo00grevuoft_137   25   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_410   26   0.48717335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   27   0.47936845   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   28   0.47890648   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   kosciuszkobiogra00gard_193   29   0.47738993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   30   0.47715482   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   withworldspeople05ridp_189   31   0.47465578   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   alhambrakremlin00prim_311   32   0.47405842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   stormssunshineof01mackuoft_43   33   0.46942425   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924028567273_442   34   0.4685645   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   sketchesinpoland00littrich_35   35   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_127   36   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_406   37   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   38   0.46539044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_390   39   0.46450147   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel3-1   conversationswit01seniuoft_271   40   0.46446007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.4
