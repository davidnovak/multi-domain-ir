###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 614181, non-zero: 614181, borderline: 614171, overall act: 63408.719, act diff: 63399.704, ratio: 1.000
#   pulse 3: activated: 706950, non-zero: 706950, borderline: 706937, overall act: 75839.873, act diff: 12431.154, ratio: 0.164
#   pulse 4: activated: 723901, non-zero: 723901, borderline: 723887, overall act: 81007.040, act diff: 5167.167, ratio: 0.064
#   pulse 5: activated: 723901, non-zero: 723901, borderline: 723887, overall act: 84301.111, act diff: 3294.071, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 723901
#   final overall activation: 84301.1
#   number of spread. activ. pulses: 5
#   running time: 135729

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   saint petersburg   5   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   caucasus region   6   0.2519036   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
7   rel3-1   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1831   4   0.83928984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1863   5   0.7726496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   1830   6   0.72108155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   7   0.66644925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   8   0.6636629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   9   0.66141206   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   10   0.65919673   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924088053800_312   11   0.6557341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   12   0.65451604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   13   0.6541654   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   14   0.65254396   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   15   0.64775455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   16   0.6438969   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   17   0.63849205   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   18   0.6370009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   19   0.63399875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   20   0.6327096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   21   0.63028944   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   22   0.62791175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   23   0.6266794   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   24   0.6229431   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   25   0.6227771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   26   0.61930335   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   poland   27   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   lectureonsocialp00tochrich_22   28   0.61716586   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924031684685_794   29   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   americancatholic30philuoft_567   30   0.6154161   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   libraryofhistori12weit_41   31   0.6149752   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   32   0.61477816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   33   0.6146509   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   34   0.61433697   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   35   0.6118029   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cataloguescient00unkngoog_358   36   0.6099386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   37   0.60693294   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   cu31924073899001_127   38   0.60198444   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   greatrussiaherac00sarouoft_160   39   0.60002214   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_390   40   0.59920985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
