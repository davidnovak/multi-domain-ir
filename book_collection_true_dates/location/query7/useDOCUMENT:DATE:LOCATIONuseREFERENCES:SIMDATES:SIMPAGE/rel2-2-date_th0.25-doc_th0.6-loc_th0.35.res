###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 13, overall act: 5.509, act diff: 3.509, ratio: 0.637
#   pulse 2: activated: 79737, non-zero: 79737, borderline: 79733, overall act: 9202.583, act diff: 9197.074, ratio: 0.999
#   pulse 3: activated: 106825, non-zero: 106825, borderline: 106820, overall act: 12289.183, act diff: 3086.600, ratio: 0.251
#   pulse 4: activated: 106825, non-zero: 106825, borderline: 106820, overall act: 13308.279, act diff: 1019.095, ratio: 0.077
#   pulse 5: activated: 127992, non-zero: 127992, borderline: 127986, overall act: 15741.514, act diff: 2433.235, ratio: 0.155
#   pulse 6: activated: 127992, non-zero: 127992, borderline: 127986, overall act: 16466.950, act diff: 725.436, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 127992
#   final overall activation: 16466.9
#   number of spread. activ. pulses: 6
#   running time: 36909

###################################
# top k results in TREC format: 

7   rel2-2   poland   1   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   russia   2   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   europe   3   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   warsaw   4   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   caucasus region   5   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   saint petersburg   6   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
7   rel2-2   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 7   rel2-2   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   1831   3   0.69611096   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   1830   4   0.48243958   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cu31924088053800_314   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cu31924088053800_312   6   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   1863   7   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   hazardsregister_11phil_125   8   0.4566187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   americanstatepap06unit_14   9   0.4284842   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historicmorgancl00eame_250   10   0.4215516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   honoursregistero00univuoft_158   11   0.40648517   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   hazardsregister_11phil_124   12   0.40531522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historylasalleco00bald_294   13   0.39607862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   remakingofmodern06marruoft_185   14   0.3945544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cataloguescient00unkngoog_834   15   0.392672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   illustratedflor02brit_81   16   0.38270146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   angloamericanli00chasgoog_196   17   0.38270146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   gentlemansmagaz164unkngoog_431   18   0.37937853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   contributionsol01lowegoog_315   19   0.37752885   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cataloguescient00unkngoog_358   20   0.37693313   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cataloguescient00unkngoog_566   21   0.37687695   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   a612145401warduoft_483   22   0.37589943   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   ahistoryfourgeo00unkngoog_345   23   0.37582424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   antislaveryrepor005soci_454   24   0.37582424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   ananalyticaldig01courgoog_100   25   0.37582424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   congressionalse175offigoog_237   26   0.37582424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   hazardsregistero14phil_402   27   0.37529394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   americanstatepap06unit_15   28   0.37474275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   hereditarygenius1869galt_317   29   0.37443846   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   cu31924021526037_116   30   0.3719916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   calmetsdictionar00calm_101   31   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historyhawtreyf02hawtgoog_537   32   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historyofmodernb00conauoft_368   33   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historytenyears06blangoog_531   34   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   chroniclesofgree00browrich_155   35   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   halfcenturyofbos00damr_449   36   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   listofresidents2195116_646   37   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   historyofcolesco00perr_320   38   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   eclecticmagazin59unkngoog_361   39   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 7   rel2-2   miscellaneouses01alisgoog_260   40   0.3703025   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
