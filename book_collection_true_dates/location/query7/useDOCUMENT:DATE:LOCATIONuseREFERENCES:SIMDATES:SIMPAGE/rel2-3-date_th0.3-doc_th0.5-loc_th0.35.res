###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 5.402, act diff: 3.402, ratio: 0.630
#   pulse 2: activated: 164540, non-zero: 164540, borderline: 164533, overall act: 14589.132, act diff: 14583.730, ratio: 1.000
#   pulse 3: activated: 164548, non-zero: 164548, borderline: 164540, overall act: 16754.094, act diff: 2164.962, ratio: 0.129
#   pulse 4: activated: 164548, non-zero: 164548, borderline: 164540, overall act: 17963.880, act diff: 1209.786, ratio: 0.067
#   pulse 5: activated: 164548, non-zero: 164548, borderline: 164540, overall act: 18240.900, act diff: 277.020, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 164548
#   final overall activation: 18240.9
#   number of spread. activ. pulses: 5
#   running time: 51961

###################################
# top k results in TREC format: 

7   rel2-3   poland   1   0.42193693   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   europe   2   0.27741334   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   russia   3   0.24229038   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   caucasus region   4   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   saint petersburg   5   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   warsaw   6   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   france   7   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   moscow   8   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   siberia   9   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   finland   10   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   asia   11   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   black sea   12   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel2-3   england   13   0.07060125   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel2-3   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   medievalandmode02robigoog_789   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   1830   3   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   1831   4   0.56021273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaasitis00gurouoft_44   5   0.50802946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924031684685_794   6   0.50704926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924031684685_796   7   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   englishwomaninr00unkngoog_305   8   0.45227116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   anthologyofmoder00selviala_126   9   0.45227116   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   poland   10   0.42193693   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   peacehandbooks08grea_81   11   0.42147464   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_43   12   0.420132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   historytenyears06blangoog_465   13   0.41856372   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   americanstatepap06unit_14   14   0.41813976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   lectureonsocialp00tochrich_22   15   0.41754994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   foundingofgerman02sybeuoft_555   16   0.41682655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   austriaviennapr00kohlgoog_511   17   0.41577554   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   russiaitspeople00gurouoft_231   18   0.4138712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   russianpolitica02kovagoog_288   19   0.4110284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   americanhistoric19151916jame_633   20   0.40536156   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924088053800_313   21   0.40300387   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924073899001_127   22   0.39752305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   bookofmemoriesof00halluoft_375   23   0.39752305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   kosciuszkobiogra00gard_151   24   0.3946927   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   historicmorgancl00eame_250   25   0.3938935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   selectionsfromsp02russuoft_245   26   0.39105466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   operationsofsurg1902jaco_633   27   0.38831964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   recoverypoland00krycgoog_229   28   0.38831964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924028567273_440   29   0.38831964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   conquestsofcross02hodd_183   30   0.38831964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   anthologyofmoder00selviala_121   31   0.38831964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   americanhistoric19151916jame_114   32   0.38817418   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cataloguescient00unkngoog_358   33   0.38722083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   cu31924073899001_33   34   0.3860899   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   remakingofmodern06marruoft_185   35   0.38481882   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   northamreview36miscrich_139   36   0.38454348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   historyforreadyr03larnuoft_331   37   0.382759   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   historyofnapoleo00abbo_107   38   0.3817515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   stormssunshineof01mackuoft_43   39   0.3796529   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel2-3   historytenyears06blangoog_489   40   0.3751778   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
