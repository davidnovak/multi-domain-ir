###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 15, non-zero: 15, borderline: 13, overall act: 5.509, act diff: 3.509, ratio: 0.637
#   pulse 2: activated: 79737, non-zero: 79737, borderline: 79733, overall act: 9202.583, act diff: 9197.074, ratio: 0.999
#   pulse 3: activated: 106825, non-zero: 106825, borderline: 106820, overall act: 12289.183, act diff: 3086.600, ratio: 0.251
#   pulse 4: activated: 106825, non-zero: 106825, borderline: 106820, overall act: 13308.279, act diff: 1019.095, ratio: 0.077
#   pulse 5: activated: 106825, non-zero: 106825, borderline: 106820, overall act: 13759.926, act diff: 451.647, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 106825
#   final overall activation: 13759.9
#   number of spread. activ. pulses: 5
#   running time: 33165

###################################
# top k results in TREC format: 

7   rel2-2   poland   1   0.3305187   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   russia   2   0.23988354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   europe   3   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   warsaw   4   0.19148763   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   caucasus region   5   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   saint petersburg   6   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
7   rel2-2   france   7   0.121718764   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel2-2   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   1831   3   0.6138376   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cu31924088053800_314   4   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cu31924088053800_312   5   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   1863   6   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   1830   7   0.4429751   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   americanstatepap06unit_14   8   0.39494053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   honoursregistero00univuoft_158   9   0.37746844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   remakingofmodern06marruoft_185   10   0.3630442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   historylasalleco00bald_294   11   0.35792324   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   hereditarygenius1869galt_317   12   0.3445851   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cataloguescient00unkngoog_834   13   0.34158304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   historyofmodernb00conauoft_368   14   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   annualregister108unkngoog_455   15   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   completeworksna23hawtgoog_15   16   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   listofresidents2195116_646   17   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   historyhawtreyf02hawtgoog_537   18   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   chroniclesofgree00browrich_155   19   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cu31924012131599_142   20   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   historyofcolesco00perr_320   21   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   miscellaneouses01alisgoog_260   22   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   calmetsdictionar00calm_101   23   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   halfcenturyofbos00damr_449   24   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   historytenyears06blangoog_531   25   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   eclecticmagazin59unkngoog_361   26   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   calmetsdictionar00calm_948   27   0.34034646   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   documentsassemb56assegoog_537   28   0.33989626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   hazardsregistero14phil_402   29   0.33957112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   antislaveryrepor005soci_454   30   0.3380771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   ahistoryfourgeo00unkngoog_345   31   0.3380771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   ananalyticaldig01courgoog_100   32   0.3380771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   congressionalse175offigoog_237   33   0.3380771   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   hazardsregister_11phil_125   34   0.33415124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   cu31924011505942_205   35   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   documentsassemb56assegoog_383   36   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   amemorialjoseph00conggoog_459   37   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   johngreenleafwhi00carp_113   38   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   nationalhymnbook00thom_164   39   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
# 7   rel2-2   worthiesworkersb00fiel_98   40   0.33350134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.4
