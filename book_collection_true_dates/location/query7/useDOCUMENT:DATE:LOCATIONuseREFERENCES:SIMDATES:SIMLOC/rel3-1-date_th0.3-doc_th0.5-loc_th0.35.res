###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 614229, non-zero: 614229, borderline: 614219, overall act: 63417.099, act diff: 63409.932, ratio: 1.000
#   pulse 3: activated: 614285, non-zero: 614285, borderline: 614226, overall act: 65945.792, act diff: 2528.693, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614285
#   final overall activation: 65945.8
#   number of spread. activ. pulses: 3
#   running time: 175434

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   russia   2   0.99998766   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   europe   3   0.99131083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   warsaw   4   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   prussia   5   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   france   6   0.5871248   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   austria   7   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vienna   8   0.5513926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   caucasus region   9   0.4460924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   saint petersburg   10   0.4411424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   italy   11   0.41747585   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   germany   12   0.3551839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vistula   13   0.30991042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   rome   14   0.3034028   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   england   15   0.29842708   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   libiya   16   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   blue tower   17   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   as sifarah al qatariyah   18   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   damascus governorate   19   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   arnous   20   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   poland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1863   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_313   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russia   6   0.99998766   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1831   7   0.9996485   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   europe   8   0.99131083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1830   9   0.949146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   warsaw   10   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   prussia   11   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   12   0.61458045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   13   0.6136545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   14   0.61208916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   15   0.6105634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   16   0.60939145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   17   0.6083302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   18   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   19   0.6048824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   20   0.5931126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   21   0.59130925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   22   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   23   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   france   24   0.5871248   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   austria   25   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   26   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   27   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   28   0.5774338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   secretsocieties01heckgoog_192   29   0.5767574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   conquestsofcross02hodd_183   30   0.57375264   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   anthologyofmoder00selviala_126   31   0.57361543   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   someproblemspea00goog_191   32   0.5724703   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   lectureonsocialp00tochrich_22   33   0.5709702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_311   34   0.5686564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   alhambrakremlin00prim_303   35   0.5684508   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_426   36   0.56320757   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   withworldspeople05ridp_189   37   0.56042904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924073899001_127   38   0.5599677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_503   39   0.55509245   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   vienna   40   0.5513926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
