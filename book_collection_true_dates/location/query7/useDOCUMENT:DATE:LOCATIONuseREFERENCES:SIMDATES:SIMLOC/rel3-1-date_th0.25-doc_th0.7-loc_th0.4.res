###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 11, overall act: 7.167, act diff: 4.167, ratio: 0.581
#   pulse 2: activated: 164561, non-zero: 164561, borderline: 164553, overall act: 21215.031, act diff: 21207.864, ratio: 1.000
#   pulse 3: activated: 263751, non-zero: 263751, borderline: 263740, overall act: 33662.866, act diff: 12447.835, ratio: 0.370
#   pulse 4: activated: 282009, non-zero: 282009, borderline: 281997, overall act: 38840.576, act diff: 5177.710, ratio: 0.133
#   pulse 5: activated: 282009, non-zero: 282009, borderline: 281997, overall act: 42141.688, act diff: 3301.112, ratio: 0.078
#   pulse 6: activated: 282009, non-zero: 282009, borderline: 281997, overall act: 43098.468, act diff: 956.780, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 282009
#   final overall activation: 43098.5
#   number of spread. activ. pulses: 6
#   running time: 75613

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   libiya   5   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   blue tower   6   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   as sifarah al qatariyah   7   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   damascus governorate   8   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   arnous   9   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   jaddat as salihiyah   10   0.29657817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   netherlands   11   0.296515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   tshili   12   0.29582384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   italy   13   0.29579344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   al hijaz   14   0.29556167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   cuba   15   0.2954348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   al hind   16   0.29480168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   orient palace   17   0.294122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   damascus   18   0.29412198   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   banama   19   0.29387063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
7   rel3-1   bab al barid   20   0.28923368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924088053800_313   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   1831   4   0.8411814   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   1863   5   0.7726496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   1830   6   0.7384909   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaasitis00gurouoft_44   7   0.66489875   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   anthologyofmoder00selviala_126   8   0.62242013   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   poland   9   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   peacehandbooks08grea_81   10   0.61463404   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   historicmorgancl00eame_250   11   0.61097133   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   russianpolitica02kovagoog_288   12   0.60295594   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   polishpeasantine01thomuoft_216   13   0.601584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_420   14   0.601584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_164   15   0.601584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_150   16   0.601584   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   polishexperienc02hallgoog_20   17   0.59836817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   americanhistoric19151916jame_633   18   0.5927387   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cataloguescient00unkngoog_358   19   0.5904097   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   hazardsregister_11phil_125   20   0.5858823   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_43   21   0.5854041   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   englishwomaninr00unkngoog_305   22   0.5848759   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   foundingofgerman02sybeuoft_555   23   0.5815226   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   secretsocieties01heckgoog_192   24   0.5785289   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   russiaitspeople00gurouoft_231   25   0.5780408   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_426   26   0.57351315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   someproblemspea00goog_191   27   0.5663575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_388   28   0.5663575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   conversationswit01seniuoft_271   29   0.5660689   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cataloguescient00unkngoog_566   30   0.56474346   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   selectionsfromsp02russuoft_428   31   0.5639108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_33   32   0.56318516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   memorialsofcambr02coopiala_386   33   0.56181157   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   historytenyears06blangoog_465   34   0.5607667   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cataloguescient00unkngoog_834   35   0.5601953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   advocateofpeace82amerrich_327   36   0.55909103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   bookofmemoriesof00halluoft_375   37   0.5585862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   cu31924073899001_127   38   0.5585862   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   austriaviennapr00kohlgoog_511   39   0.5573588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
# 7   rel3-1   americanstatepap06unit_14   40   0.5562882   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.25-doc_th0.7-loc_th0.4
