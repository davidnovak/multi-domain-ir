###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 115009, non-zero: 115009, borderline: 115004, overall act: 13597.453, act diff: 13590.027, ratio: 0.999
#   pulse 3: activated: 115009, non-zero: 115009, borderline: 115004, overall act: 13597.453, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 115009
#   final overall activation: 13597.5
#   number of spread. activ. pulses: 3
#   running time: 44565

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   libiya   5   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   blue tower   6   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   as sifarah al qatariyah   7   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   damascus governorate   8   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   arnous   9   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   jaddat as salihiyah   10   0.29657817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   netherlands   11   0.296515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   tshili   12   0.29582384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   italy   13   0.29579344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   al hijaz   14   0.29556167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   cuba   15   0.2954348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   al hind   16   0.29480168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   orient palace   17   0.294122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   damascus   18   0.29412198   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   banama   19   0.29387063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
7   rel2-1   bab al barid   20   0.28923368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924031684685_794   4   0.527213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_420   5   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_164   6   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   polishpeasantine01thomuoft_216   7   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_150   8   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   anthologyofmoder00selviala_126   9   0.4913384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   secretsocieties01heckgoog_192   10   0.46403852   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_314   11   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924031684685_796   12   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924088053800_312   13   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_426   14   0.45812804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   1863   15   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   1831   16   0.4570441   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   someproblemspea00goog_191   17   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_388   18   0.44971356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_428   19   0.44684103   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_43   20   0.4458609   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   foundingofgerman02sybeuoft_555   21   0.44113356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   polishexperienc02hallgoog_20   22   0.43818647   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaitspeople00gurouoft_231   23   0.4368995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_398   24   0.43651208   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   leavesfromdiaryo00grevuoft_137   25   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   sketchesinpoland00littrich_57   26   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_410   27   0.43644437   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   stormssunshineof01mackuoft_43   28   0.43438724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   advocateofpeace82amerrich_327   29   0.42767447   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_193   30   0.42606664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   russiaasitis00gurouoft_44   31   0.42475736   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924028567273_442   32   0.42408082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   withworldspeople05ridp_189   33   0.4231688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   alhambrakremlin00prim_311   34   0.42253587   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_406   35   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   sketchesinpoland00littrich_35   36   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   cu31924073899001_127   37   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   bookofmemoriesof00halluoft_375   38   0.41335672   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   selectionsfromsp02russuoft_390   39   0.41241583   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
# 7   rel2-1   kosciuszkobiogra00gard_151   40   0.40926063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.4
