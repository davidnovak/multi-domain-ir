###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 17, non-zero: 17, borderline: 15, overall act: 7.426, act diff: 5.426, ratio: 0.731
#   pulse 2: activated: 569205, non-zero: 569205, borderline: 569198, overall act: 55813.088, act diff: 55805.662, ratio: 1.000
#   pulse 3: activated: 593389, non-zero: 593389, borderline: 593381, overall act: 58893.012, act diff: 3079.924, ratio: 0.052
#   pulse 4: activated: 593389, non-zero: 593389, borderline: 593381, overall act: 59909.291, act diff: 1016.280, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 593389
#   final overall activation: 59909.3
#   number of spread. activ. pulses: 4
#   running time: 92871

###################################
# top k results in TREC format: 

7   rel2-1   poland   1   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   europe   2   0.38680688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   russia   3   0.36250737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   warsaw   4   0.317622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   libiya   5   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   blue tower   6   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   as sifarah al qatariyah   7   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   damascus governorate   8   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   arnous   9   0.29658398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   jaddat as salihiyah   10   0.29657817   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   netherlands   11   0.296515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   tshili   12   0.29582384   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   italy   13   0.29579344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   al hijaz   14   0.29556167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   cuba   15   0.2954348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   al hind   16   0.29480168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   orient palace   17   0.294122   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   damascus   18   0.29412198   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   banama   19   0.29387063   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
7   rel2-1   bab al barid   20   0.28923368   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 7   rel2-1   cu31924031684685_795   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924088053800_313   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   poland   3   0.6186022   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924031684685_794   4   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   1831   5   0.5956452   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_43   6   0.5852942   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaitspeople00gurouoft_231   7   0.57981354   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924031684685_796   8   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   advocateofpeace82amerrich_327   9   0.5764377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924088053800_314   10   0.5750714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_408   11   0.5646878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_245   12   0.562637   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   americanhistoric19151916jame_633   13   0.5552618   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russiaasitis00gurouoft_44   14   0.5503273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924088053800_312   15   0.5471878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_410   16   0.54607207   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   russianpolitica02kovagoog_288   17   0.54453236   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_442   18   0.54430574   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   conquestsofcross02hodd_183   19   0.54224366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   anthologyofmoder00selviala_126   20   0.54209924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   peacehandbooks08grea_81   21   0.5381644   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_424   22   0.5363658   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_428   23   0.53355694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   secretsocieties01heckgoog_192   24   0.5321019   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   historytenyears06blangoog_465   25   0.53151995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   conversationswit01seniuoft_271   26   0.52919054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   cu31924073899001_127   27   0.5277455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   someproblemspea00goog_191   28   0.5274982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_311   29   0.52340466   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_426   30   0.51755995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   withworldspeople05ridp_189   31   0.51458126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   spiritrussiastu00masagoog_252   32   0.514145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   englandduringthi00mart_819   33   0.51297843   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   outlineseuropea04beargoog_503   34   0.5088632   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   expansionofruss00skri_211   35   0.50883085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   eclecticmagazin18unkngoog_41   36   0.50748104   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   alhambrakremlin00prim_303   37   0.50532037   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   lectureonsocialp00tochrich_22   38   0.505139   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   selectionsfromsp02russuoft_390   39   0.49981302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
# 7   rel2-1   foundingofgerman02sybeuoft_555   40   0.49520865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.7-loc_th0.35
