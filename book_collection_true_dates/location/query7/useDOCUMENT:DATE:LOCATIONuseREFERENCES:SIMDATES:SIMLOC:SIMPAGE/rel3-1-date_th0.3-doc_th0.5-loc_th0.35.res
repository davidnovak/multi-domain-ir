###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   cu31924088053800_313, 1
#   cu31924031684685_795, 1
#   medievalandmode02robigoog_789, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 15, overall act: 9.015, act diff: 6.015, ratio: 0.667
#   pulse 2: activated: 614229, non-zero: 614229, borderline: 614219, overall act: 63418.718, act diff: 63409.703, ratio: 1.000
#   pulse 3: activated: 614306, non-zero: 614306, borderline: 614243, overall act: 65973.511, act diff: 2554.793, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 614306
#   final overall activation: 65973.5
#   number of spread. activ. pulses: 3
#   running time: 172451

###################################
# top k results in TREC format: 

7   rel3-1   poland   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   russia   2   0.9999968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   europe   3   0.9947201   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   warsaw   4   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   prussia   5   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   france   6   0.6406877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   austria   7   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   rome   8   0.5655755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vienna   9   0.5513926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   siberia   10   0.49865395   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   caucasus region   11   0.4460924   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   saint petersburg   12   0.4411424   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   italy   13   0.41747585   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   england   14   0.3746869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   germany   15   0.3551839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   vistula   16   0.30991042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   libiya   17   0.2979548   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   blue tower   18   0.2979392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   as sifarah al qatariyah   19   0.29745486   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
7   rel3-1   damascus governorate   20   0.29738516   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 7   rel3-1   medievalandmode02robigoog_789   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_795   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   poland   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1863   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_313   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russia   6   0.9999968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1831   7   0.9996485   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   europe   8   0.9947201   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1830   9   0.949146   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   warsaw   10   0.83863866   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_409   11   0.7196519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   prussia   12   0.70610946   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_427   13   0.6988682   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   outlineseuropea04beargoog_502   14   0.6720542   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_407   15   0.6668041   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924088053800_312   16   0.6557341   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   france   17   0.6406877   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_403   18   0.6267688   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_794   19   0.61713266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_43   20   0.61458045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   americanhistoric19151916jame_633   21   0.6136545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_82   22   0.61318374   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   advocateofpeace82amerrich_327   23   0.61208916   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russianpolitica02kovagoog_288   24   0.6105634   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_425   25   0.60940266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaitspeople00gurouoft_231   26   0.60939145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   russiaasitis00gurouoft_44   27   0.6083302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_408   28   0.60703087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   peacehandbooks08grea_81   29   0.6048824   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   1825   30   0.59416175   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_245   31   0.5931126   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   historytenyears06blangoog_465   32   0.59130925   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_410   33   0.58975124   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_389   34   0.5896844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_442   35   0.58810955   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   austria   36   0.5829411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_424   37   0.58072567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   cu31924031684685_796   38   0.57882524   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   selectionsfromsp02russuoft_428   39   0.5781117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 7   rel3-1   conversationswit01seniuoft_271   40   0.5774338   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
