###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 23, non-zero: 23, borderline: 22, overall act: 3.902, act diff: 2.902, ratio: 0.744
#   pulse 2: activated: 207451, non-zero: 207451, borderline: 207449, overall act: 18179.360, act diff: 18175.459, ratio: 1.000
#   pulse 3: activated: 207451, non-zero: 207451, borderline: 207449, overall act: 18179.360, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207451
#   final overall activation: 18179.4
#   number of spread. activ. pulses: 3
#   running time: 46014

###################################
# top k results in TREC format: 

10   rel1-4   california   1   0.370601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   madera county   2   0.17036527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   coarsegold   3   0.1672478   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   deadwood gulch   4   0.16541281   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   san francisco   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   vichy spring   6   0.14830981   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   fresno flats   7   0.14787753   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   striped rock creek   8   0.1461735   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   ahwahnee   9   0.14195527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   san joaquin experimental range   10   0.13173304   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   bass lake station   11   0.1306379   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   china wells   12   0.12778509   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   bates station   13   0.12773523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   raynor creek   14   0.11557978   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   sierra county   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   calaveras county   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   sacramento   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   new york   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   carson hill   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
10   rel1-4   golden west   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel1-4   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   california   2   0.370601   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_479   3   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   forestryinmining00browrich_1   4   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   gt4waterpowerfromge190calirich_43   5   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   herberthooverman00lckell_55   6   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   lampshield1963stan_49   7   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   mindsandmanners00horngoog_239   8   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   typologyscriptu02fairgoog_607   9   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   travelsathome00twaigoog_136   10   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_461   11   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_565   12   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_563   13   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_559   14   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_557   15   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_555   16   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   flavouringmateri00clarrich_186   17   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_553   18   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_551   19   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_561   20   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   buildingindustri11111cont_415   21   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   pacificcoastvaca00morrrich_172   22   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_934   23   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   commercialeduca00unkngoog_2   24   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs2931_961   25   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   bulletinofmuseum124harv_324   26   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_528   27   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   ineztaleofalamo00evaniala_4   28   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   birdlore24nati_162   29   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_549   30   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_921   31   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_440   32   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_919   33   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_917   34   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_454   35   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   govuscourtsca9briefs3383_909   36   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   birdlore24nati_103   37   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   miltonmarksoral01markrich_466   38   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   flavouringmateri00clarrich_177   39   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel1-4   minutesofgeneral1915pres_567   40   0.18320839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.25-doc_th0.6-loc_th0.35
