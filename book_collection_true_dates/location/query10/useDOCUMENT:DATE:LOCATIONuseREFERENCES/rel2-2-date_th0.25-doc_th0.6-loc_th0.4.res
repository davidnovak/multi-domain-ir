###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   sanfranciscohist01youn_192, 1
#   ingersollscentur00inge_101, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 4.911, act diff: 2.911, ratio: 0.593
#   pulse 2: activated: 281219, non-zero: 281219, borderline: 281214, overall act: 37259.850, act diff: 37254.939, ratio: 1.000
#   pulse 3: activated: 281219, non-zero: 281219, borderline: 281214, overall act: 37259.850, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 281219
#   final overall activation: 37259.9
#   number of spread. activ. pulses: 3
#   running time: 59705

###################################
# top k results in TREC format: 

10   rel2-2   california   1   0.60449284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   san francisco   2   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   yerba buena   3   0.28235662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   united states   4   0.19556819   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   sacramento   5   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   monterey   6   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   fort   7   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   sutter   8   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   los angeles   9   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
10   rel2-2   philadelphia   10   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 10   rel2-2   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   sanfranciscohist01youn_192   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   california   3   0.60449284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   tennesseespartne00hartrich_12   4   0.4917898   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   beginningsofsanf02eldr_206   5   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   establishmentofs00good_111   6   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   reportsofcasesar01newyiala_213   7   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   appletonscyclop01wils_536   8   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   schoolarithmeti00ellwgoog_219   9   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   californiachrono00monn_51   10   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   popularhistoryof00ridp_510   11   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   pioneerregisterl11oakhrich_14   12   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   lifeofdavidbelas02wintuoft_288   13   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   greathogg00thacuoft_28   14   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   worksralphwaldo14emergoog_343   15   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   cu31924013844885_111   16   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   californiaitshi00mcgrgoog_262   17   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   oldaceotherpoems01broo_83   18   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   cu31924030196160_16   19   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   ademnellaindianl00hurs_150   20   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   indianfinancecu00doragoog_85   21   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   ohiossilvertongu00bigg_210   22   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   athomeabroadfirs00tayliala_676   23   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   diggersinearth00tapprich_48   24   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   improvingschools00broo_223   25   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   worksofhuberthow02banc_182   26   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   pacifichistoryst01harr_193   27   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   diaryafortynine01canfgoog_13   28   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   navalhygienehuma00wilsrich_170   29   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   worksofhuberthow19bancrich_702   30   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   premisesfreetra01dixwgoog_218   31   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   chautauquanorga01circgoog_42   32   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   sketchesbordera01hubbgoog_11   33   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   federalstatecons01thoriala_439   34   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   proceedingscolle15wyom_10   35   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   beginningsofsanf02eldr_329   36   0.45374665   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   storycalifornia06nortgoog_250   37   0.44037774   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   sanfranciscohist01youn_22   38   0.4372259   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   ahistorycollege01willgoog_265   39   0.4334316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 10   rel2-2   establishmentofs00good_366   40   0.4334316   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
