###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   ingersollscentur00inge_101, 1
#   waterqualityreso00sacr_128, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 75, non-zero: 75, borderline: 73, overall act: 9.853, act diff: 7.853, ratio: 0.797
#   pulse 2: activated: 207490, non-zero: 207490, borderline: 207487, overall act: 25180.726, act diff: 25170.874, ratio: 1.000
#   pulse 3: activated: 207490, non-zero: 207490, borderline: 207487, overall act: 25180.726, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207490
#   final overall activation: 25180.7
#   number of spread. activ. pulses: 3
#   running time: 68324

###################################
# top k results in TREC format: 

10   rel2-9   california   1   0.51561075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   san francisco   2   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   los vaqueros   3   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   sierra nevada   4   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   miwok   5   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   sacramento   6   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   monterey   7   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   fort   8   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   brentwood   9   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   san jose   10   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   livermore   11   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   pleasanton   12   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   contra costa county   13   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   san francisco bay area   14   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   san joaquin river   15   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   stockton   16   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   oakland   17   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
10   rel2-9   san joaquin valley   18   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel2-9   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   waterqualityreso00sacr_128   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   california   3   0.51561075   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   san francisco   4   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   los vaqueros   5   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   buildingindustri11111cont_479   6   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   forestryinmining00browrich_1   7   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   gt4waterpowerfromge190calirich_43   8   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   herberthooverman00lckell_55   9   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   lampshield1963stan_49   10   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   mindsandmanners00horngoog_239   11   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   typologyscriptu02fairgoog_607   12   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   travelsathome00twaigoog_136   13   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   buildingindustri11111cont_461   14   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_565   15   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_563   16   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_559   17   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_557   18   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_555   19   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   flavouringmateri00clarrich_186   20   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_553   21   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_551   22   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_561   23   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   buildingindustri11111cont_415   24   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   pacificcoastvaca00morrrich_172   25   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs2931_934   26   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   commercialeduca00unkngoog_2   27   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs2931_961   28   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   bulletinofmuseum124harv_324   29   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   miltonmarksoral01markrich_528   30   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   ineztaleofalamo00evaniala_4   31   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   birdlore24nati_162   32   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   minutesofgeneral1915pres_549   33   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs3383_921   34   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   miltonmarksoral01markrich_440   35   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs3383_919   36   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs3383_917   37   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   miltonmarksoral01markrich_454   38   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   govuscourtsca9briefs3383_909   39   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 10   rel2-9   birdlore24nati_103   40   0.25224167   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
