###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   sanfranciscohist01youn_192, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 29, non-zero: 29, borderline: 27, overall act: 6.415, act diff: 4.415, ratio: 0.688
#   pulse 2: activated: 281234, non-zero: 281234, borderline: 281229, overall act: 37731.536, act diff: 37725.121, ratio: 1.000
#   pulse 3: activated: 344139, non-zero: 344139, borderline: 344132, overall act: 47123.244, act diff: 9391.707, ratio: 0.199
#   pulse 4: activated: 344139, non-zero: 344139, borderline: 344132, overall act: 51496.344, act diff: 4373.100, ratio: 0.085
#   pulse 5: activated: 344139, non-zero: 344139, borderline: 344132, overall act: 54023.337, act diff: 2526.993, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 344139
#   final overall activation: 54023.3
#   number of spread. activ. pulses: 5
#   running time: 86772

###################################
# top k results in TREC format: 

10   rel2-3   california   1   0.6291687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   yerba buena   2   0.28235662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   united states   3   0.19556819   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   san francisco   4   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   sutter   5   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   los angeles   6   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   philadelphia   7   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   sacramento county   8   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   east   9   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   golden west   10   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   sierra county   11   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   sacramento   12   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   calaveras county   13   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   new york   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   carson hill   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   mokelumne river   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   kansas   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   colombo   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-3   new jersey   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 10   rel2-3   sanfranciscohist01youn_192   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   policepeaceoffic19461946sanf_79   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   1848   3   0.72320855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   1849   4   0.72320855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   journalofhouseof184546indi_439   5   0.6429011   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   tennesseespartne00hartrich_12   6   0.6360162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   california   7   0.6291687   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   pacificcoasthighways00john_249   8   0.6206712   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   californiafruits04wick_53   9   0.60915923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   historyofsanfran00will_85   10   0.6073211   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   storycalifornia06nortgoog_250   11   0.6035432   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   plain00srockiesbibwagnrich_226   12   0.583834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   cataloguescient00unkngoog_872   13   0.5835861   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   truehistoryofmis01dixo_417   14   0.5834044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   codesstatutesofs01cali_254   15   0.58166337   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   cataloguescient00unkngoog_890   16   0.58056647   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   reportsofcasesar01newyiala_213   17   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   appletonscyclop01wils_536   18   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   popularhistoryof00ridp_510   19   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   pioneerregisterl11oakhrich_14   20   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   californiaitshi00mcgrgoog_262   21   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   cu31924013844885_111   22   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   beginningsofsanf02eldr_206   23   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   establishmentofs00good_111   24   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   oldaceotherpoems01broo_83   25   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   pacifichistoryst01harr_193   26   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   diaryafortynine01canfgoog_13   27   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   navalhygienehuma00wilsrich_170   28   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   worksofhuberthow19bancrich_702   29   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   cu31924030196160_16   30   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   premisesfreetra01dixwgoog_218   31   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   ademnellaindianl00hurs_150   32   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   ohiossilvertongu00bigg_210   33   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   chautauquanorga01circgoog_42   34   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   sketchesbordera01hubbgoog_11   35   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   proceedingscolle15wyom_10   36   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   federalstatecons01thoriala_439   37   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   schoolarithmeti00ellwgoog_219   38   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   lifeofdavidbelas02wintuoft_288   39   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-3   greathogg00thacuoft_28   40   0.578893   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
