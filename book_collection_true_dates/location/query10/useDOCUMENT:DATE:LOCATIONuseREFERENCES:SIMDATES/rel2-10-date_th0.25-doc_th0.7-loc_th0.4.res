###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   policepeaceoffic19461946sanf_79, 1
#   waterqualityreso00sacr_128, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 87, non-zero: 87, borderline: 85, overall act: 11.345, act diff: 9.345, ratio: 0.824
#   pulse 2: activated: 207502, non-zero: 207502, borderline: 207499, overall act: 26552.550, act diff: 26541.205, ratio: 1.000
#   pulse 3: activated: 207502, non-zero: 207502, borderline: 207499, overall act: 26552.550, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 207502
#   final overall activation: 26552.6
#   number of spread. activ. pulses: 3
#   running time: 78978

###################################
# top k results in TREC format: 

10   rel2-10   california   1   0.5442568   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   los vaqueros   2   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   sierra nevada   3   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   miwok   4   0.17341912   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   san francisco   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   oakland   6   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   stockton   7   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   san francisco bay area   8   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   san joaquin river   9   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   contra costa county   10   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   brentwood   11   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   pleasanton   12   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   san jose   13   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   livermore   14   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   san joaquin valley   15   0.110084414   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   mokelumne river   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   sacramento county   17   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   new york   18   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   sacramento   19   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
10   rel2-10   calaveras county   20   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 10   rel2-10   policepeaceoffic19461946sanf_79   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   waterqualityreso00sacr_128   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   california   3   0.5442568   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   los vaqueros   4   0.2781926   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   buildingindustri11111cont_479   5   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   forestryinmining00browrich_1   6   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   gt4waterpowerfromge190calirich_43   7   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   herberthooverman00lckell_55   8   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   lampshield1963stan_49   9   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   mindsandmanners00horngoog_239   10   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   typologyscriptu02fairgoog_607   11   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   travelsathome00twaigoog_136   12   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   buildingindustri11111cont_461   13   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_565   14   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_563   15   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_559   16   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_557   17   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_555   18   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   flavouringmateri00clarrich_186   19   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_553   20   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_551   21   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_561   22   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   buildingindustri11111cont_415   23   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   pacificcoastvaca00morrrich_172   24   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs2931_934   25   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   commercialeduca00unkngoog_2   26   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs2931_961   27   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   bulletinofmuseum124harv_324   28   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   miltonmarksoral01markrich_528   29   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   ineztaleofalamo00evaniala_4   30   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   birdlore24nati_162   31   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   minutesofgeneral1915pres_549   32   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs3383_921   33   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   miltonmarksoral01markrich_440   34   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs3383_919   35   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs3383_917   36   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   miltonmarksoral01markrich_454   37   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   govuscourtsca9briefs3383_909   38   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   birdlore24nati_103   39   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
# 10   rel2-10   miltonmarksoral01markrich_466   40   0.2656042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.7-loc_th0.4
