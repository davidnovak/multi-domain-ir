###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   sanfranciscohist01youn_192, 1
#   ingersollscentur00inge_101, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 14, non-zero: 14, borderline: 12, overall act: 4.911, act diff: 2.911, ratio: 0.593
#   pulse 2: activated: 281233, non-zero: 281233, borderline: 281228, overall act: 37263.292, act diff: 37258.381, ratio: 1.000
#   pulse 3: activated: 281233, non-zero: 281233, borderline: 281228, overall act: 40313.724, act diff: 3050.432, ratio: 0.076
#   pulse 4: activated: 281233, non-zero: 281233, borderline: 281228, overall act: 41363.974, act diff: 1050.250, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 281233
#   final overall activation: 41364.0
#   number of spread. activ. pulses: 4
#   running time: 60700

###################################
# top k results in TREC format: 

10   rel2-2   california   1   0.60449284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   san francisco   2   0.30383277   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   yerba buena   3   0.28235662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   madera county   4   0.27348033   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   coarsegold   5   0.26863077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   deadwood gulch   6   0.26577234   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   vichy spring   7   0.23899484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   fresno flats   8   0.238315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   striped rock creek   9   0.23563367   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   ahwahnee   10   0.22898662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   san joaquin experimental range   11   0.2128243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   bass lake station   12   0.21108836   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   china wells   13   0.20656244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   bates station   14   0.2064833   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   united states   15   0.19556819   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   raynor creek   16   0.1871385   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   monterey   17   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   sacramento   18   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   fort   19   0.13430275   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
10   rel2-2   sutter   20   0.124353   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 10   rel2-2   ingersollscentur00inge_101   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   sanfranciscohist01youn_192   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   california   3   0.60449284   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   1848   4   0.5921386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   1849   5   0.5921386   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   tennesseespartne00hartrich_12   6   0.58171105   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   storycalifornia06nortgoog_250   7   0.5439141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   beginningsofsanf02eldr_206   8   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   establishmentofs00good_111   9   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   reportsofcasesar01newyiala_213   10   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   appletonscyclop01wils_536   11   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   schoolarithmeti00ellwgoog_219   12   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   californiachrono00monn_51   13   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   popularhistoryof00ridp_510   14   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   pioneerregisterl11oakhrich_14   15   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   lifeofdavidbelas02wintuoft_288   16   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   greathogg00thacuoft_28   17   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   worksralphwaldo14emergoog_343   18   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   cu31924013844885_111   19   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   californiaitshi00mcgrgoog_262   20   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   indianfinancecu00doragoog_85   21   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   oldaceotherpoems01broo_83   22   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   athomeabroadfirs00tayliala_676   23   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   diggersinearth00tapprich_48   24   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   improvingschools00broo_223   25   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   worksofhuberthow02banc_182   26   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   cu31924030196160_16   27   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   ademnellaindianl00hurs_150   28   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   pacifichistoryst01harr_193   29   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   diaryafortynine01canfgoog_13   30   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   navalhygienehuma00wilsrich_170   31   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   worksofhuberthow19bancrich_702   32   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   ohiossilvertongu00bigg_210   33   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   premisesfreetra01dixwgoog_218   34   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   chautauquanorga01circgoog_42   35   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   sketchesbordera01hubbgoog_11   36   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   federalstatecons01thoriala_439   37   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   proceedingscolle15wyom_10   38   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   beginningsofsanf02eldr_329   39   0.5297212   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
# 10   rel2-2   plain00srockiesbibwagnrich_226   40   0.52537185   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.35
