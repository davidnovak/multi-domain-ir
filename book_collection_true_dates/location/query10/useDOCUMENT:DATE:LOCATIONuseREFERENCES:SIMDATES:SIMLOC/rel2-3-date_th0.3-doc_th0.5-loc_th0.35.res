###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   sanfranciscohist01youn_192, 1
#   policepeaceoffic19461946sanf_79, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 29, non-zero: 29, borderline: 27, overall act: 6.415, act diff: 4.415, ratio: 0.688
#   pulse 2: activated: 281246, non-zero: 281246, borderline: 281241, overall act: 37734.419, act diff: 37728.004, ratio: 1.000
#   pulse 3: activated: 344151, non-zero: 344151, borderline: 344144, overall act: 47126.126, act diff: 9391.707, ratio: 0.199
#   pulse 4: activated: 344158, non-zero: 344158, borderline: 344112, overall act: 51502.319, act diff: 4376.193, ratio: 0.085
#   pulse 5: activated: 390997, non-zero: 390997, borderline: 390848, overall act: 88740.976, act diff: 37238.657, ratio: 0.420
#   pulse 6: activated: 2328797, non-zero: 2328797, borderline: 2324521, overall act: 560503.423, act diff: 471762.447, ratio: 0.842
#   pulse 7: activated: 7968436, non-zero: 7968436, borderline: 7883373, overall act: 4359041.357, act diff: 3798537.933, ratio: 0.871
#   pulse 8: activated: 9122414, non-zero: 9122414, borderline: 5117307, overall act: 5790884.862, act diff: 1431843.506, ratio: 0.247
#   pulse 9: activated: 9654574, non-zero: 9654574, borderline: 3824585, overall act: 6582327.328, act diff: 791442.466, ratio: 0.120
#   pulse 10: activated: 9712497, non-zero: 9712497, borderline: 2851824, overall act: 6714070.732, act diff: 131743.404, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 9712497
#   final overall activation: 6714070.7
#   number of spread. activ. pulses: 10
#   running time: 1944467

###################################
# top k results in TREC format: 

10   rel2-3   new lenox   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   haverstraw bay   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   milton of balgonie   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   victoria bridge   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   terra firma   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   betsey   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   south oxford   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   nederland   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   bapaume   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   bettws   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   holy land   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   pangbourne   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   porthleven   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   rataplan   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   warboys   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   montreux   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   ecorse   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   montreat   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   montreal   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
10   rel2-3   betten   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 10   rel2-3   new lenox   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   victoria bridge   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   haverstraw bay   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   milton of balgonie   4   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   terra firma   5   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   betsey   6   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   south oxford   7   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   nederland   8   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   bapaume   9   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   bettws   10   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   holy land   11   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   pangbourne   12   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   porthleven   13   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   rataplan   14   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   warboys   15   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   montreux   16   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   ecorse   17   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   montreat   18   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   montreal   19   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   city of boston   20   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   brocket hall   21   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   merrymeeting bay   22   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   edmund street   23   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   new castle county   24   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   bidford   25   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   lidiana   26   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   bethel   27   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   betham   28   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   warburg   29   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   santa luzia   30   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   betica   31   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   hawthornden   32   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   north kensington   33   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   shillong   34   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   calatayud   35   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   sandpoint   36   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   mchenry county   37   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   embarcadero   38   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   point barrow   39   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 10   rel2-3   betten   40   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
