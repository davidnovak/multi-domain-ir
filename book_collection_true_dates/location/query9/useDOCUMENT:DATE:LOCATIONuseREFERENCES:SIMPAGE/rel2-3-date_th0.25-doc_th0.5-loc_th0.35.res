###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 59, non-zero: 59, borderline: 57, overall act: 10.868, act diff: 8.868, ratio: 0.816
#   pulse 2: activated: 110057, non-zero: 110057, borderline: 110053, overall act: 9409.615, act diff: 9398.747, ratio: 0.999
#   pulse 3: activated: 110074, non-zero: 110074, borderline: 110068, overall act: 9412.794, act diff: 3.179, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 110074
#   final overall activation: 9412.8
#   number of spread. activ. pulses: 3
#   running time: 49788

###################################
# top k results in TREC format: 

9   rel2-3   austria   1   0.60744953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   prussia   2   0.54016054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   saxony   3   0.37626266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   hanover   4   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   elbe   5   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   bavaria   6   0.26939356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   holstein   7   0.24832352   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   berlin   8   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   moravia   9   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   bad langensalza   10   0.2079528   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   vienna   11   0.20058009   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   denmark   12   0.19701664   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   jicin   13   0.18809588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   hesse   14   0.17704222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   bohemia   15   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   gottingen   16   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   trutnowo   17   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   jutland   18   0.105902016   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   frankfort   19   0.09673286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
9   rel2-3   mikulov   20   0.09673286   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 9   rel2-3   cu31924014592566_749   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   austria   3   0.60744953   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_15   4   0.59817225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_17   5   0.5614111   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   prussia   6   0.54016054   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_14   7   0.5091592   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924014592566_748   8   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924014592566_750   9   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_18   10   0.3793577   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   saxony   11   0.37626266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   hanover   12   0.31738377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   lifetimesofstein02seeluoft_364   13   0.28154993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_352   14   0.2800378   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_365   15   0.27931443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   foundingofgerman02sybeuoft_32   16   0.27931443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   serbiaeurope00mark_142   17   0.2782878   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   foundingofgerman02sybeuoft_96   18   0.27778804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   historyofeuropef00holt_116   19   0.27628985   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   historyofcontemp00seiguoft_295   20   0.2755602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   historyofcontemp00seigrich_299   21   0.2755602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924082142666_119   22   0.2754155   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   europe1789192000turnrich_282   23   0.27382314   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   universalantho25garn_415   24   0.27326855   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   elbe   25   0.27288526   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   reckoningdiscuss00beckrich_146   26   0.27104625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   lifetimesofstein02seeluoft_37   27   0.26992804   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   bavaria   28   0.26939356   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   cu31924031684685_715   29   0.26931933   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_301   30   0.26891625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_357   31   0.2687072   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   shorthistoryofeu00terr_459   32   0.267999   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   outlineseuropea04beargoog_378   33   0.2674702   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   storygreatwar04ruhlgoog_345   34   0.26721844   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   foundingofgerman02sybeuoft_537   35   0.26686436   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   foundationsofger00barkuoft_248   36   0.2668078   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   briefhistoryofna00fish_538   37   0.26668754   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_312   38   0.26666123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_311   39   0.26617867   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
# 9   rel2-3   ourchancellorske00buscuoft_359   40   0.26617867   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.25-doc_th0.5-loc_th0.35
