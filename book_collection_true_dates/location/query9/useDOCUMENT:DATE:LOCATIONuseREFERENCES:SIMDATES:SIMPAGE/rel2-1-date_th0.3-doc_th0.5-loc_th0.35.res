###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 71, non-zero: 71, borderline: 69, overall act: 12.567, act diff: 10.567, ratio: 0.841
#   pulse 2: activated: 113976, non-zero: 113976, borderline: 113971, overall act: 10322.703, act diff: 10310.136, ratio: 0.999
#   pulse 3: activated: 114016, non-zero: 114016, borderline: 114008, overall act: 10328.695, act diff: 5.992, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 114016
#   final overall activation: 10328.7
#   number of spread. activ. pulses: 3
#   running time: 100949

###################################
# top k results in TREC format: 

9   rel2-1   austria   1   0.65173393   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   elbe   2   0.54005045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   prussia   3   0.53043073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   saxony   4   0.4019838   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   italy   5   0.3522522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   jicin   6   0.33317629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   germany   7   0.32892758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   bohemia   8   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   silesia   9   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   hungary   10   0.28489158   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   vienna   11   0.2700141   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   holstein   12   0.2611638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   dresden   13   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   prague   14   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   berlin   15   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   moravia   16   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   pest   17   0.17195964   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   denmark   18   0.17186162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   trutnowo   19   0.16543837   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
9   rel2-1   peschiera   20   0.16483803   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   austria   3   0.65173393   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_154   4   0.63275546   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924014592566_750   5   0.55956495   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   elbe   6   0.54005045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_156   7   0.5313244   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   prussia   8   0.53043073   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   encyclopediabrit03newyrich_153   9   0.51588535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924014592566_748   10   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924014592566_751   11   0.40786707   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   saxony   12   0.4019838   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   italy   13   0.3522522   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   jicin   14   0.33317629   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   germany   15   0.32892758   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   marvelousstoryof00nort_227   16   0.31701896   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924014592566_362   17   0.3108315   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   bohemia   18   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924082142666_237   19   0.30218443   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_364   20   0.29823935   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_352   21   0.29680276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_365   22   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_32   23   0.29657462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   serbiaeurope00mark_142   24   0.29496095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   foundingofgerman02sybeuoft_96   25   0.29373276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   historyofeuropef00holt_116   26   0.29215991   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924082142666_119   27   0.29208982   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   historyofcontemp00seiguoft_295   28   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   historyofcontemp00seigrich_299   29   0.29168087   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   europe1789192000turnrich_282   30   0.2903725   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   shorthistoryofeu00terr_465   31   0.289929   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   universalantho25garn_415   32   0.289677   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   papersrelatingt19statgoog_84   33   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   papersrelatingt04unkngoog_90   34   0.28862834   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   reckoningdiscuss00beckrich_146   35   0.28733686   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   cu31924031684685_715   36   0.28570086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   ourchancellorske00buscuoft_357   37   0.2851643   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   silesia   38   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   hungary   39   0.28489158   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
# 9   rel2-1   lifetimesofstein02seeluoft_37   40   0.28468382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.35
