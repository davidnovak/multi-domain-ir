###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   encyclopediabrit03newyrich_155, 1
#   cu31924082142666_16, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 34, non-zero: 34, borderline: 32, overall act: 7.105, act diff: 5.105, ratio: 0.718
#   pulse 2: activated: 110060, non-zero: 110060, borderline: 110056, overall act: 10173.473, act diff: 10166.369, ratio: 0.999
#   pulse 3: activated: 110060, non-zero: 110060, borderline: 110056, overall act: 10173.473, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 110060
#   final overall activation: 10173.5
#   number of spread. activ. pulses: 3
#   running time: 41092

###################################
# top k results in TREC format: 

9   rel2-5   austria   1   0.45714977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   prussia   2   0.38772225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   saxony   3   0.3441148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   holstein   4   0.31223398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   hanover   5   0.23115462   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   acropolis hill   6   0.22465234   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   art gallery   7   0.22386755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   acropolis museum   8   0.22263062   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   propylaia   9   0.22229607   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   acropolis   10   0.22222655   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   acropolis of athens   11   0.22221826   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   parthenon   12   0.22215997   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   areopagus   13   0.22204874   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   erechtheion   14   0.22193715   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   pnyx   15   0.22191302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   dhafni   16   0.21570744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   peronia   17   0.2089604   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   trakhones   18   0.2011074   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   berlin   19   0.2010266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
9   rel2-5   moravia   20   0.2010266   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 9   rel2-5   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   cu31924082142666_16   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   austria   3   0.45714977   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   prussia   4   0.38772225   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   saxony   5   0.3441148   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   holstein   6   0.31223398   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   lifetimesofstein02seeluoft_364   7   0.30337334   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_352   8   0.30178598   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_365   9   0.3011069   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   foundingofgerman02sybeuoft_32   10   0.3011069   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   serbiaeurope00mark_142   11   0.29991737   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   foundingofgerman02sybeuoft_96   12   0.29926082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   historyofeuropef00holt_116   13   0.29766142   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   historyofcontemp00seiguoft_295   14   0.2969326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   historyofcontemp00seigrich_299   15   0.2969326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   cu31924082142666_119   16   0.29687622   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   europe1789192000turnrich_282   17   0.29516795   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   universalantho25garn_415   18   0.2945559   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   reckoningdiscuss00beckrich_146   19   0.2921813   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   lifetimesofstein02seeluoft_37   20   0.29072815   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   cu31924031684685_715   21   0.2903676   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_301   22   0.28978553   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_357   23   0.28973222   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   shorthistoryofeu00terr_459   24   0.289004   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   outlineseuropea04beargoog_378   25   0.28842348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   storygreatwar04ruhlgoog_345   26   0.28808984   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   foundationsofger00barkuoft_248   27   0.28776932   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   foundingofgerman02sybeuoft_537   28   0.28754726   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   briefhistoryofna00fish_538   29   0.2875003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_312   30   0.28738642   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_311   31   0.28697816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   ourchancellorske00buscuoft_359   32   0.28697816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   polishexperienc02hallgoog_307   33   0.28697816   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   historyofeuropef00holt_102   34   0.28669372   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   cu31924082142666_37   35   0.28663626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   cu31924014592566_362   36   0.28571576   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   edinburghreview128londuoft_249   37   0.28542027   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   marvelousstoryof00nort_227   38   0.2853971   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   foundingofgerman02sybeuoft_122   39   0.28444573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 9   rel2-5   menwhohavemaden00stragoog_155   40   0.28444573   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
