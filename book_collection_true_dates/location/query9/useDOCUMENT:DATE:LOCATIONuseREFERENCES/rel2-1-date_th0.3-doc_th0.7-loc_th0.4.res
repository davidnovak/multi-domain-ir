###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   cu31924014592566_749, 1
#   encyclopediabrit03newyrich_155, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 67, non-zero: 67, borderline: 65, overall act: 10.719, act diff: 8.719, ratio: 0.813
#   pulse 2: activated: 75190, non-zero: 75190, borderline: 75187, overall act: 6150.044, act diff: 6139.325, ratio: 0.998
#   pulse 3: activated: 75190, non-zero: 75190, borderline: 75187, overall act: 6150.044, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 75190
#   final overall activation: 6150.0
#   number of spread. activ. pulses: 3
#   running time: 44203

###################################
# top k results in TREC format: 

9   rel2-1   austria   1   0.44222224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   elbe   2   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   prussia   3   0.3878032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   bohemia   4   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   saxony   5   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   silesia   6   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   jicin   7   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   dresden   8   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   italy   9   0.22582704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   moravia   10   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   berlin   11   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   prague   12   0.177523   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   bavaria   13   0.14684908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   koniggratz   14   0.13562085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   hungary   15   0.13562085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   holstein   16   0.13562085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   germany   17   0.13562085   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   bad langensalza   18   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   hanover   19   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
9   rel2-1   breslau   20   0.093056194   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4

###################################
# top nodes: 

# 9   rel2-1   encyclopediabrit03newyrich_155   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924014592566_749   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   austria   3   0.44222224   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   elbe   4   0.3939273   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   prussia   5   0.3878032   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   bohemia   6   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   saxony   7   0.30960995   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   silesia   8   0.2851132   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   jicin   9   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   dresden   10   0.25945282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   italy   11   0.22582704   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_226   12   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_244   13   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_250   14   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cavourmakingofmo00orsi_167   15   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_350   16   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_358   17   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_232   18   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_226   19   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_256   20   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   secretmemoirsco01orlgoog_446   21   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   newscuttings00gran_30   22   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924027975733_88   23   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   journalseriesage65royauoft_442   24   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   storiesseatoldb00unkngoog_94   25   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924060379777_343   26   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   historyofpopesth00rankuoft_513   27   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_360   28   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031236031_366   29   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   catholicsocialis00nittiala_260   30   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924082163829_160   31   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   schoolgardenbein00schw_82   32   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   schoolgardenbein00schw_90   33   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   memoirscountess02genlgoog_44   34   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   generalarmoryofe00burk_1337   35   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924027758022_392   36   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924082163829_142   37   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   cu31924031296340_439   38   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   autobiographycor01deweuoft_203   39   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
# 9   rel2-1   electricfurnace01baurgoog_20   40   0.21757683   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.3-doc_th0.7-loc_th0.4
