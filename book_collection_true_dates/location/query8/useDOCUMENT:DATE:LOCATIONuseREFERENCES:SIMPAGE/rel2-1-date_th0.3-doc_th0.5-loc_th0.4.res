###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMPAGE
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 255, non-zero: 255, borderline: 253, overall act: 22.460, act diff: 20.460, ratio: 0.911
#   pulse 2: activated: 99729, non-zero: 99729, borderline: 99724, overall act: 9452.407, act diff: 9429.948, ratio: 0.998
#   pulse 3: activated: 99752, non-zero: 99752, borderline: 99744, overall act: 9456.055, act diff: 3.648, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 99752
#   final overall activation: 9456.1
#   number of spread. activ. pulses: 3
#   running time: 80803

###################################
# top k results in TREC format: 

8   rel2-1   holland   1   0.61765903   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   england   2   0.5742749   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   europe   3   0.40552986   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   france   4   0.3082363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   spain   5   0.24732181   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   scotland   6   0.23280856   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   calais   7   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   london   8   0.13391213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   worcester   9   0.13391213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   mediterranean   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   plymouth   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   denmark   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   portsmouth   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   portland   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   boulogne   15   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   weymouth   16   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   queensbury   17   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   baltic   18   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   new england   19   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
8   rel2-1   dunkirk   20   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4

###################################
# top nodes: 

# 8   rel2-1   bookofhistoryhis11brycuoft_237   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   holland   3   0.61765903   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   england   4   0.5742749   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   bookofhistoryhis11brycuoft_236   5   0.51999086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   bookofhistoryhis11brycuoft_238   6   0.5049904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_421   7   0.50118303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   1651   8   0.46994168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_423   9   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   europe   10   0.40552986   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   livesofwarriorsw01cust_59   11   0.37468755   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   landmarkhistoryo00ulmas_22   12   0.33073714   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   bookofhistoryhis11brycuoft_235   13   0.32805392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   burnetshistoryof01burnuoft_429   14   0.32674363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   treasuresofartin02waaguoft_327   15   0.32551044   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   france   16   0.3082363   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   worksfrancisbaco03bacoiala_205   17   0.3076144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   pioneermothersof01gree_195   18   0.30685633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   1653   19   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   1654   20   0.3038274   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   250thanniversary00newyrich_117   21   0.2991831   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   cu31924028066797_380   22   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   englishheraldicb00daverich_402   23   0.2980491   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   thomasharrisonr00wessgoog_20   24   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   courtofkingsbench08greaiala_290   25   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   aschoolhistoryu01macegoog_109   26   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   kinginexilewande00scotuoft_420   27   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   pictorialhistor01macfgoog_430   28   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   lifeofyoungsirhe00hosmiala_433   29   0.29548743   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   augustanages00eltouoft_242   30   0.29197112   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   lettersspeecheso02cromuoft_338   31   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   evolutionofstate00robeuoft_331   32   0.2915556   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   cu31924067742332_16   33   0.29155213   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   historyofstateof00broduoft_613   34   0.29082623   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   historyforreadyr03larnuoft_770   35   0.28895515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   registerofvisito00camduoft_113   36   0.28755295   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   cu31924028485070_293   37   0.27375957   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   historyofnewyork00pren_93   38   0.27134305   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   burnetshistoryof00burnuoft_175   39   0.26919344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
# 8   rel2-1   kinginexilewande00scotuoft_444   40   0.2663114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMPAGE-date_th0.3-doc_th0.5-loc_th0.4
