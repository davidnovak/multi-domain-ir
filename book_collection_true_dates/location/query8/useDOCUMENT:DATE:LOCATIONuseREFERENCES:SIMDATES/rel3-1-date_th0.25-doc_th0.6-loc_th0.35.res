###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (3): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 3, non-zero: 3, borderline: 3, overall act: 3.000, act diff: 3.000, ratio: 1.000
#   pulse 1: activated: 264, non-zero: 264, borderline: 261, overall act: 24.039, act diff: 21.039, ratio: 0.875
#   pulse 2: activated: 1044447, non-zero: 1044447, borderline: 1044438, overall act: 136519.797, act diff: 136495.758, ratio: 1.000
#   pulse 3: activated: 1047119, non-zero: 1047119, borderline: 1047109, overall act: 137432.192, act diff: 912.395, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 1047119
#   final overall activation: 137432.2
#   number of spread. activ. pulses: 3
#   running time: 188752

###################################
# top k results in TREC format: 

8   rel3-1   england   1   0.528162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   holland   2   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   spain   3   0.40804455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   europe   4   0.33633456   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   calais   5   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   weymouth   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   plymouth   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   queensbury   8   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   portland   9   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   boulogne   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   denmark   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   portsmouth   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   mediterranean   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   baltic   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   penn   15   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   west indies   16   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   athens   17   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   portugal   18   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   great britain   19   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
8   rel3-1   cadiz   20   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 8   rel3-1   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   bookofhistoryhis11brycuoft_237   3   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   1653   4   0.6548261   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   1651   5   0.5737859   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   1652   6   0.55236435   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   americannationhi05hartuoft_33   7   0.54694164   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   navalbattlesanci00shiprich_129   8   0.5424786   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   yearbookofhollan18holl_105   9   0.5345853   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   england   10   0.528162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   workssamueljohn18murpgoog_62   11   0.52476186   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   encyclopdiabri07chisrich_523   12   0.52307993   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   englandadestroy00crongoog_11   13   0.52210575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   questioninbaptis00whit_131   14   0.5167567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   lawofnationscons12twis_379   15   0.5167567   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   manualofinternat02ferg_476   16   0.51156706   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   briefhistoryofna00fish_433   17   0.50708336   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   historyofevelynf00eveluoft_53   18   0.50219876   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   principlesofpoli02nichuoft_276   19   0.5000143   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   completehistoric00clar_231   20   0.499881   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   historyforreadyr03larnuoft_770   21   0.49541798   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   historyofyachtin00clar_36   22   0.49351123   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   memorialsofcambr02coopiala_466   23   0.48998114   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   hallamsworks04halliala_45   24   0.48981082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_391   25   0.4871243   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   1654   26   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   irelandundercomm02dunluoft_123   27   0.48433098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   holland   28   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   greatgreaterbrit00barkuoft_37   29   0.48313558   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   governmentalhist03sher_153   30   0.4795619   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   originsofbritish00beeruoft_445   31   0.47846884   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   newillustratedhi02browuoft_327   32   0.47829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   moneybankingdisc05howauoft_77   33   0.47829   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   schoolchildandt00adamgoog_241   34   0.47705504   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   livesofeminentbr00inrosc_59   35   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   developmentfree03dunigoog_52   36   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   kinginexilewande00scotuoft_376   37   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   sirhenryvanejrg00kinggoog_121   38   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   completeworksofr02burnuoft_186   39   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel3-1   carlylecompletew08carl_210   40   0.47516048   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.6-loc_th0.35
