###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   pictorialhistor01macfgoog_422, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 255, non-zero: 255, borderline: 253, overall act: 22.460, act diff: 20.460, ratio: 0.911
#   pulse 2: activated: 99730, non-zero: 99730, borderline: 99725, overall act: 9452.969, act diff: 9430.509, ratio: 0.998
#   pulse 3: activated: 99730, non-zero: 99730, borderline: 99725, overall act: 9765.228, act diff: 312.260, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 99730
#   final overall activation: 9765.2
#   number of spread. activ. pulses: 3
#   running time: 72871

###################################
# top k results in TREC format: 

8   rel2-1   holland   1   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   england   2   0.27598518   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   europe   3   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   calais   4   0.22816166   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   spain   5   0.15861107   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   portland   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   weymouth   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   denmark   8   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   queensbury   9   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   mediterranean   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   plymouth   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   portsmouth   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   boulogne   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   baltic   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   atlantic   15   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   north america   16   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   thames   17   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   dunkirk   18   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   france   19   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
8   rel2-1   new york   20   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 8   rel2-1   bookofhistoryhis11brycuoft_237   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   bookofhistoryhis11brycuoft_236   3   0.51999086   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   bookofhistoryhis11brycuoft_238   4   0.5049904   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_421   5   0.50118303   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   1653   6   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   1654   7   0.4861968   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   holland   8   0.48390293   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_423   9   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   livesofwarriorsw01cust_59   10   0.42950392   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   landmarkhistoryo00ulmas_22   11   0.3876902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   burnetshistoryof01burnuoft_429   12   0.38387918   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pioneermothersof01gree_195   13   0.36487168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   treasuresofartin02waaguoft_327   14   0.3619045   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   registerofvisito00camduoft_113   15   0.36136174   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   250thanniversary00newyrich_117   16   0.35752493   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   pictorialhistor01macfgoog_430   17   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   thomasharrisonr00wessgoog_20   18   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_420   19   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   courtofkingsbench08greaiala_290   20   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   aschoolhistoryu01macegoog_109   21   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lifeofyoungsirhe00hosmiala_433   22   0.35398394   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lettersspeecheso02cromuoft_338   23   0.35021484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   evolutionofstate00robeuoft_331   24   0.35021484   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   historyofstateof00broduoft_613   25   0.34951544   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   historyforreadyr03larnuoft_770   26   0.34772095   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924067742332_16   27   0.34024742   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   worksfrancisbaco03bacoiala_205   28   0.33692515   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   influenceseapow13mahagoog_70   29   0.33378616   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   memorialswillar00fiskgoog_127   30   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   fournewyorkboys00davigoog_87   31   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   fullersthoughts00fullrich_232   32   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   kinginexilewande00scotuoft_444   33   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   lettersfromround00bann_79   34   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924027975733_471   35   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   littlejenningsfi01sergiala_24   36   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   worksthomascarl10traigoog_114   37   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   carlylecompletew08carl_313   38   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   cu31924028485070_288   39   0.32597005   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
# 8   rel2-1   manualofinternat02ferg_476   40   0.32365453   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.6-loc_th0.35
