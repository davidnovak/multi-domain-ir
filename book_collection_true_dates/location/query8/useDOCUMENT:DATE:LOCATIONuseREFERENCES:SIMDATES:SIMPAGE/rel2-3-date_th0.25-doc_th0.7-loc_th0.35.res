###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMPAGE

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMPAGE
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   pictorialhistor01macfgoog_422, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 36, non-zero: 36, borderline: 34, overall act: 8.720, act diff: 6.720, ratio: 0.771
#   pulse 2: activated: 5084, non-zero: 5084, borderline: 5081, overall act: 492.617, act diff: 483.897, ratio: 0.982
#   pulse 3: activated: 9526, non-zero: 9526, borderline: 9522, overall act: 892.851, act diff: 400.234, ratio: 0.448
#   pulse 4: activated: 13557, non-zero: 13557, borderline: 13552, overall act: 1509.805, act diff: 616.954, ratio: 0.409
#   pulse 5: activated: 22341, non-zero: 22341, borderline: 22334, overall act: 2651.497, act diff: 1141.692, ratio: 0.431
#   pulse 6: activated: 22341, non-zero: 22341, borderline: 22334, overall act: 3092.633, act diff: 441.136, ratio: 0.143
#   pulse 7: activated: 22341, non-zero: 22341, borderline: 22334, overall act: 3443.458, act diff: 350.825, ratio: 0.102
#   pulse 8: activated: 22341, non-zero: 22341, borderline: 22334, overall act: 3569.919, act diff: 126.462, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 22341
#   final overall activation: 3569.9
#   number of spread. activ. pulses: 8
#   running time: 34111

###################################
# top k results in TREC format: 

8   rel2-3   england   1   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   spain   2   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   europe   3   0.2440059   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   holland   4   0.20519012   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   calais   5   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   weymouth   6   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   queensbury   7   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   portland   8   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   portsmouth   9   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   denmark   10   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   mediterranean   11   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   plymouth   12   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   boulogne   13   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   baltic   14   0.13057499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   greece   15   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   great britain   16   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   penn   17   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   west indies   18   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   cadiz   19   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
8   rel2-3   athens   20   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 8   rel2-3   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   pictorialhistor01macfgoog_422   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1651   3   0.71133053   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1652   4   0.68978685   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1653   5   0.6827375   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   memorialsofcambr02coopiala_466   6   0.5372192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   1654   7   0.5308793   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_332   8   0.5275535   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   memorialsofcambr02coopiala_366   9   0.5159003   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   ageofmilton05mastuoft_109   10   0.5024902   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofguilfor00smitiala_31   11   0.5018738   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cyclopdiaofuse02tomlrich_455   12   0.49280366   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   listofmanuscript00univ_538   13   0.4726145   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_328   14   0.46568263   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924014592566_594   15   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   pictorialhistor01macfgoog_421   16   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924014592566_592   17   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   pictorialhistor01macfgoog_423   18   0.46211717   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_320   19   0.45125747   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerofkentuc11kent_334   20   0.44949007   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   selectessaysinan01asso_483   21   0.44940624   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_415   22   0.449117   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   dictionaryofmada01fitzuoft_80   23   0.44501203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   registerswadham01collgoog_149   24   0.44501203   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_389   25   0.44390082   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofolderno00palm_48   26   0.44287348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   briefhistoryofna00fish_433   27   0.442805   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   courtleetrecords04mancuoft_345   28   0.44280496   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   philipthomashowa00palmuoft_137   29   0.4381545   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924032733879_104   30   0.43787602   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofcountyp00orme_254   31   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   causeofirelandpl00orei_407   32   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   originsofbritish00beeruoft_434   33   0.4365192   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   wadhamcollege00wellrich_87   34   0.43604442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924027990971_54   35   0.43604442   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   historyofgardeni00ceci_354   36   0.43236056   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   cu31924091770861_35   37   0.4323605   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   colonizationofno00boltrich_174   38   0.43036625   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   ageofmilton05mastuoft_605   39   0.42858633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
# 8   rel2-3   archaeologiaaeli01sociuoft_44   40   0.42858633   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMPAGE-date_th0.25-doc_th0.7-loc_th0.35
