###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 17, overall act: 3.513, act diff: 2.513, ratio: 0.715
#   pulse 2: activated: 18, non-zero: 18, borderline: 17, overall act: 3.513, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 18
#   final overall activation: 3.5
#   number of spread. activ. pulses: 2
#   running time: 20814

###################################
# top k results in TREC format: 

8   rel1-3   england   1   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   spain   2   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   athens   3   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   greece   4   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   europe   5   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   cadiz   6   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   new world   7   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   great britain   8   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   west indies   9   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   portugal   10   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
8   rel1-3   penn   11   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 8   rel1-3   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   england   2   0.2952077   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   spain   3   0.266694   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1625   4   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1627   5   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1651   6   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1595   7   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1596   8   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   1652   9   0.14938858   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   new world   10   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   greece   11   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   athens   12   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   europe   13   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   cadiz   14   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   great britain   15   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   west indies   16   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   portugal   17   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
# 8   rel1-3   penn   18   0.11716388   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.5-loc_th0.35
