###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   bookofhistoryhis11brycuoft_237, 1
#   cu31924014592566_593, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 254, non-zero: 254, borderline: 252, overall act: 20.729, act diff: 18.729, ratio: 0.904
#   pulse 2: activated: 980302, non-zero: 980302, borderline: 980298, overall act: 126685.846, act diff: 126665.117, ratio: 1.000
#   pulse 3: activated: 980302, non-zero: 980302, borderline: 980298, overall act: 126685.846, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 980302
#   final overall activation: 126685.8
#   number of spread. activ. pulses: 3
#   running time: 169542

###################################
# top k results in TREC format: 

8   rel2-2   england   1   0.528162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   spain   2   0.40804455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   holland   3   0.30943748   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   newton blossomville   4   0.24149744   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   filgrave   5   0.23852724   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   turvey   6   0.23300326   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   sherington   7   0.23258282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   easton maudit   8   0.23079227   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   tyringham   9   0.23028252   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   chicheley   10   0.22786377   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   hardmead   11   0.22618839   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   bozeat   12   0.2246689   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   harrold   13   0.22216067   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   europe   14   0.21521094   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   baltimore harbour   15   0.18864542   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   loughine   16   0.18687168   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   spanish island   17   0.18501276   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   donegall   18   0.18461871   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   killeena   19   0.18331215   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
8   rel2-2   kilmoon   20   0.18007468   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 8   rel2-2   cu31924014592566_593   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bookofhistoryhis11brycuoft_237   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   england   3   0.528162   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   spain   4   0.40804455   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   ourmotherland00burr_12   5   0.36164108   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   ourmotherland00burr_17   6   0.35163638   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_167   7   0.34598017   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   principlesofpoli03mill_141   8   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   progressofrussia00urqurich_119   9   0.3361777   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924013685866_157   10   0.33468983   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   historyofuniteds00form_36   11   0.33238575   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   calendarmanuscr09ofgoog_243   12   0.33217588   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   englandsfightwit00wals_203   13   0.33207282   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   treasonandplots01humegoog_18   14   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   lawofnationscons12twis_851   15   0.3311083   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   principlespolit24millgoog_142   16   0.3307302   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   politicaleconomy04walk_96   17   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   theoryofbalanceo02suviuoft_108   18   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   studiesinstruct03carpgoog_218   19   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_291   20   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   elementseconomi01maclgoog_137   21   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   historyofmodernc00duco_157   22   0.33004564   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_312   23   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorjo01borruoft_377   24   0.3299869   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924092705114_424   25   0.32909417   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   memoirsofjohnquis04adam_88   26   0.32672134   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   selectionofcases01willuoft_364   27   0.32546344   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   churchofengland03spenuoft_426   28   0.32526976   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924024798245_33   29   0.32422626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   lifeofnapoleonii01roseuoft_564   30   0.32422626   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   historyenglandf16gardgoog_258   31   0.32341406   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   merchantadventu01linggoog_147   32   0.32330698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   hectorgermanieo00smitgoog_28   33   0.32330698   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924067113880_168   34   0.32207662   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   lifetimeswriting00collrich_36   35   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   revisedreportsb26courgoog_433   36   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   cu31924091749212_449   37   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   shorthistoryofpa00skotuoft_196   38   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   jennygeddesorpre00bree_343   39   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
# 8   rel2-2   bibleinspainorj02unkngoog_179   40   0.32175538   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMLOC-date_th0.3-doc_th0.6-loc_th0.4
