###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_463, 1
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 53, non-zero: 53, borderline: 51, overall act: 9.442, act diff: 7.442, ratio: 0.788
#   pulse 2: activated: 5125, non-zero: 5125, borderline: 5122, overall act: 699.622, act diff: 690.180, ratio: 0.987
#   pulse 3: activated: 5125, non-zero: 5125, borderline: 5122, overall act: 699.622, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5125
#   final overall activation: 699.6
#   number of spread. activ. pulses: 3
#   running time: 12126

###################################
# top k results in TREC format: 

1   rel2-18   lucknow   1   0.58485514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   chanda   2   0.23942329   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   shahabad   3   0.19846521   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   cabool   4   0.18963519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   arrah   5   0.16514042   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   agra   6   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   sobraon   7   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   cawnpore   8   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   panjnad river   9   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   punjaub   10   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   benares   11   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   danapur   12   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   mosque   13   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   sebastopol   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   windham   15   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   redan   16   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   bareilly   17   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   aliwal   18   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   gya   19   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-18   mathura   20   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-18   newannualarmylis1874hart_463   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   newannualarmylis1874hart_228   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   lucknow   3   0.58485514   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   apersonaljourna00andegoog_121   4   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   systemsoflandte00cobd_226   5   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reportsofmission1896pres_306   6   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   apersonaljourna00andegoog_110   7   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   tennysonhisarta01broogoog_251   8   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   journalofroyalin7188183roya_413   9   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   phoeberowe01thob_185   10   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   narrativemutini00hutcgoog_6   11   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   fortyoneyearsini00robe_316   12   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   lifeofclaudmarti00hill_182   13   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_279   14   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   libraryofworldsbe22warn_51   15   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_267   16   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   memoirofsirwilli00veitrich_344   17   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   newannualarmylis1898lond_946   18   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_211   19   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_234   20   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_231   21   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   reminiscencesofb00edwauoft_230   22   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   passionsanimals00thomgoog_139   23   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   historyofindianm01forr_16   24   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   historyofindianm01forr_14   25   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   historyofindianm01forr_29   26   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   memorialsoflife02edwa_52   27   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   recordsofindianm06indi_263   28   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   macmillansmagazi43macmuoft_261   29   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   memorialsoflife02edwa_49   30   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   livesofindianoff02kayeiala_346   31   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   livesofindianoff02kayeiala_348   32   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   cu31924022927283_266   33   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   cu31924013563865_47   34   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   lifeofisabellath00thobrich_290   35   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   lifeofisabellath00thobrich_286   36   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   troubadourselect00gibbuoft_36   37   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   journaloftravels00wina_252   38   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   journaloftravels00wina_251   39   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-18   lifeofclaudmarti00hill_170   40   0.28436762   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
