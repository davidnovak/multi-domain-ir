###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_410, 1
#   newannualarmylis1874hart_228, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 50, non-zero: 50, borderline: 48, overall act: 8.818, act diff: 6.818, ratio: 0.773
#   pulse 2: activated: 5122, non-zero: 5122, borderline: 5119, overall act: 700.556, act diff: 691.738, ratio: 0.987
#   pulse 3: activated: 5122, non-zero: 5122, borderline: 5119, overall act: 700.556, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5122
#   final overall activation: 700.6
#   number of spread. activ. pulses: 3
#   running time: 15195

###################################
# top k results in TREC format: 

1   rel2-13   lucknow   1   0.58620894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   shahabad   2   0.29001382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   arrah   3   0.25745413   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   chanda   4   0.23942329   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   agra   5   0.22086102   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   china   6   0.1910497   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   gwalior   7   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   bengal   8   0.15209739   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   windham   9   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   mosque   10   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   benares   11   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   sebastopol   12   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   danapur   13   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   redan   14   0.10476908   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   rowa   15   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   pekin   16   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   sevastopol   17   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   calcutta   18   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   india   19   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
1   rel2-13   algiers   20   0.09641279   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35

###################################
# top nodes: 

# 1   rel2-13   newannualarmylis1874hart_228   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   newannualarmylis1874hart_410   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   lucknow   3   0.58620894   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   shahabad   4   0.29001382   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   apersonaljourna00andegoog_121   5   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   systemsoflandte00cobd_226   6   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reportsofmission1896pres_306   7   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   apersonaljourna00andegoog_110   8   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   tennysonhisarta01broogoog_251   9   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   journalofroyalin7188183roya_413   10   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   phoeberowe01thob_185   11   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   narrativemutini00hutcgoog_6   12   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   fortyoneyearsini00robe_316   13   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   lifeofclaudmarti00hill_182   14   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_279   15   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   libraryofworldsbe22warn_51   16   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_267   17   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   memoirofsirwilli00veitrich_344   18   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   newannualarmylis1898lond_946   19   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_211   20   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_234   21   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_231   22   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   reminiscencesofb00edwauoft_230   23   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   passionsanimals00thomgoog_139   24   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   historyofindianm01forr_16   25   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   historyofindianm01forr_14   26   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   historyofindianm01forr_29   27   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   memorialsoflife02edwa_52   28   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   recordsofindianm06indi_263   29   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   macmillansmagazi43macmuoft_261   30   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   memorialsoflife02edwa_49   31   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   livesofindianoff02kayeiala_346   32   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   livesofindianoff02kayeiala_348   33   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   cu31924022927283_266   34   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   cu31924013563865_47   35   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   lifeofisabellath00thobrich_290   36   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   lifeofisabellath00thobrich_286   37   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   troubadourselect00gibbuoft_36   38   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   journaloftravels00wina_252   39   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
# 1   rel2-13   journaloftravels00wina_251   40   0.28498966   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.35
