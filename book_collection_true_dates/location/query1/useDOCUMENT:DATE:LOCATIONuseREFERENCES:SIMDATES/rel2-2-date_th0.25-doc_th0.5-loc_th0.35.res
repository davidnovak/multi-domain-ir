###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.5
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.5
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (2): 
#   newannualarmylis1874hart_480, 1
#   newannualarmylis1874hart_463, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 2, non-zero: 2, borderline: 2, overall act: 2.000, act diff: 2.000, ratio: 1.000
#   pulse 1: activated: 60, non-zero: 60, borderline: 58, overall act: 10.136, act diff: 8.136, ratio: 0.803
#   pulse 2: activated: 5132, non-zero: 5132, borderline: 5129, overall act: 667.222, act diff: 657.086, ratio: 0.985
#   pulse 3: activated: 5132, non-zero: 5132, borderline: 5129, overall act: 667.222, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 5132
#   final overall activation: 667.2
#   number of spread. activ. pulses: 3
#   running time: 17759

###################################
# top k results in TREC format: 

1   rel2-2   lucknow   1   0.5561621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   cawnpore   2   0.32326865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   delhi   3   0.2991923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   agra   4   0.23899533   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   bareilly   5   0.23656994   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   china   6   0.20950438   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   cabool   7   0.18963519   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   jhansi   8   0.18539499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   india   9   0.18539499   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   panjnad river   10   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   sobraon   11   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   punjaub   12   0.15096098   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   rooya   13   0.14414719   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   baree   14   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   gwalior   15   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   mathura   16   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   gya   17   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   camp   18   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   abwal   19   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
1   rel2-2   jhelum   20   0.09568572   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35

###################################
# top nodes: 

# 1   rel2-2   newannualarmylis1874hart_480   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   newannualarmylis1874hart_463   2   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   lucknow   3   0.5561621   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   cawnpore   4   0.32326865   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   delhi   5   0.2991923   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   apersonaljourna00andegoog_121   6   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   systemsoflandte00cobd_226   7   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reportsofmission1896pres_306   8   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   apersonaljourna00andegoog_110   9   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   tennysonhisarta01broogoog_251   10   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   journalofroyalin7188183roya_413   11   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   phoeberowe01thob_185   12   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   narrativemutini00hutcgoog_6   13   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   fortyoneyearsini00robe_316   14   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   lifeofclaudmarti00hill_182   15   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_279   16   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   libraryofworldsbe22warn_51   17   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_267   18   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   memoirofsirwilli00veitrich_344   19   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   newannualarmylis1898lond_946   20   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_211   21   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_234   22   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_231   23   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   reminiscencesofb00edwauoft_230   24   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   passionsanimals00thomgoog_139   25   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   historyofindianm01forr_16   26   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   historyofindianm01forr_14   27   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   historyofindianm01forr_29   28   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   memorialsoflife02edwa_52   29   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   recordsofindianm06indi_263   30   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   macmillansmagazi43macmuoft_261   31   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   memorialsoflife02edwa_49   32   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   livesofindianoff02kayeiala_346   33   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   livesofindianoff02kayeiala_348   34   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   cu31924022927283_266   35   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   cu31924013563865_47   36   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   lifeofisabellath00thobrich_290   37   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   lifeofisabellath00thobrich_286   38   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   troubadourselect00gibbuoft_36   39   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
# 1   rel2-2   journaloftravels00wina_252   40   0.27112812   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES-date_th0.25-doc_th0.5-loc_th0.35
