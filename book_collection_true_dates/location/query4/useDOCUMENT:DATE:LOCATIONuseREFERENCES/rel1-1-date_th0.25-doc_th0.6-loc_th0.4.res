###################################
# parameters: 
#   date_th: 0.25
#   doc_th: 0.6
#   loc_th: 0.4
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.25
#   sa.spread_threshold.DOCUMENT = 0.6
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   empirecommercein00wooluoft_192, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 18, non-zero: 18, borderline: 17, overall act: 3.555, act diff: 2.555, ratio: 0.719
#   pulse 2: activated: 18, non-zero: 18, borderline: 17, overall act: 3.555, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 18
#   final overall activation: 3.6
#   number of spread. activ. pulses: 2
#   running time: 20006

###################################
# top k results in TREC format: 

4   rel1-1   djibouti   1   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   europe   2   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   ethiopia   3   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   adowa   4   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   italy   5   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   federal democratic republic of ethiopia   6   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   state of eritrea   7   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   wich'ale   8   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   france   9   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   somaliland   10   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
4   rel1-1   ras asir   11   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4

###################################
# top nodes: 

# 4   rel1-1   empirecommercein00wooluoft_192   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   ethiopia   2   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   djibouti   3   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   italy   4   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   europe   5   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   adowa   6   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   federal democratic republic of ethiopia   7   0.18779144   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1892   8   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1889   9   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1890   10   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1895   11   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1896   12   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   1891   13   0.13857411   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   wich'ale   14   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   state of eritrea   15   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   france   16   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   somaliland   17   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
# 4   rel1-1   ras asir   18   0.11933487   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.25-doc_th0.6-loc_th0.4
