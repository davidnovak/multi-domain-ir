###################################
# parameters: 
#   date_th: 0.3
#   doc_th: 0.7
#   loc_th: 0.35
#   nodestouse: DOCUMENT:DATE:LOCATION
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.act_diff_ratio_limit_diff = 0.01
#   sa.function = ciir.multi.sa.function.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 10
#   sa.nodes_to_print = DATE:LOCATION
#   sa.nodes_to_use = DOCUMENT:DATE:LOCATION
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.7
#   sa.spread_threshold.LOCATION = 0.35
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim
#   sa.weight_property.SIMPAGE = pagesim

###################################
# initial activations (1): 
#   manualforuseofge197778mass_52, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 56, non-zero: 56, borderline: 55, overall act: 9.320, act diff: 8.320, ratio: 0.893
#   pulse 2: activated: 56, non-zero: 56, borderline: 55, overall act: 9.320, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 56
#   final overall activation: 9.3
#   number of spread. activ. pulses: 2
#   running time: 55545

###################################
# top k results in TREC format: 

5   rel1-1   united states   1   0.23246084   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   texas   2   0.20010501   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   california   3   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   north carolina   4   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   georgia   5   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   illinois   6   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   arizona   7   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   oregon   8   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   minnesota   9   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   indiana   10   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   south dakota   11   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   wyoming   12   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   colorado   13   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   north dakota   14   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   michigan   15   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   wisconsin   16   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   idaho   17   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   montana   18   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   ohio   19   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
5   rel1-1   oklahoma   20   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35

###################################
# top nodes: 

# 5   rel1-1   manualforuseofge197778mass_52   1   1.0   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   united states   2   0.23246084   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   texas   3   0.20010501   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   california   4   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   north carolina   5   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   georgia   6   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   oregon   7   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   arizona   8   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   illinois   9   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   indiana   10   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   south dakota   11   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   idaho   12   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   wyoming   13   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   colorado   14   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   north dakota   15   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   michigan   16   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   wisconsin   17   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   montana   18   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   minnesota   19   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   ohio   20   0.17773348   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   mississippi   21   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   oklahoma   22   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   alabama   23   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   louisiana   24   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   new york   25   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   south carolina   26   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   washington   27   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   tennessee   28   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   nevada   29   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   maine   30   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   west virginia   31   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   kentucky   32   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   maryland   33   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   iowa   34   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   virginia   35   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   new hampshire   36   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   massachusetts   37   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   delaware   38   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   missouri   39   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
# 5   rel1-1   nebraska   40   0.14749527   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES:SIMDATES:SIMLOC-date_th0.3-doc_th0.7-loc_th0.35
