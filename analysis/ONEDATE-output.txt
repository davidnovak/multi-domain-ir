SA configuration: 
	sa.act_diff_ratio_limit = 0.05
	sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
	sa.id_property_name.DATE = dateID
	sa.id_property_name.DOCUMENT = docID
	sa.id_property_name.LOCATION = locID
	sa.max_pulses = 10
	sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
	sa.progress_prefix = ONEDATE_
	sa.relationships_to_use = SIMDATES
	sa.weight_property.REFERENCES = relevance
	sa.weight_property.SIMDATES = sim
	sa.weight_property.SIMDOC = sim
	sa.weight_property.SIMLOC = sim

initial document activations (1): 
	1848, 1, DATE

#   pulse 0: activated nodes: 1, borderline nodes: 1, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 13, borderline nodes: 12, overall activation: 0.788, activation diff: 0.788, ratio: 1.000
#   pulse 2: activated nodes: 25, borderline nodes: 12, overall activation: 0.622, activation diff: 0.295, ratio: 0.475
#   pulse 3: activated nodes: 37, borderline nodes: 12, overall activation: 0.491, activation diff: 0.160, ratio: 0.325
#   pulse 4: activated nodes: 49, borderline nodes: 12, overall activation: 0.388, activation diff: 0.114, ratio: 0.293
#   pulse 5: activated nodes: 61, borderline nodes: 12, overall activation: 0.306, activation diff: 0.086, ratio: 0.280
#   pulse 6: activated nodes: 71, borderline nodes: 12, overall activation: 0.241, activation diff: 0.066, ratio: 0.275
#   pulse 7: activated nodes: 73, borderline nodes: 12, overall activation: 0.190, activation diff: 0.052, ratio: 0.273
#   pulse 8: activated nodes: 73, borderline nodes: 12, overall activation: 0.149, activation diff: 0.041, ratio: 0.272
#   pulse 9: activated nodes: 73, borderline nodes: 12, overall activation: 0.118, activation diff: 0.032, ratio: 0.272
#   pulse 10: activated nodes: 73, borderline nodes: 12, overall activation: 0.092, activation diff: 0.025, ratio: 0.273

final number of activated nodes: 73
final overall activation: 0.1
number of spread. activ. pulses: 10
running time: 323

top nodes: 
1848, 0.011868201
1847, 0.01109561
1849, 0.01109561
1850, 0.00929972
1846, 0.009299719
1845, 0.0071361167
1851, 0.007136116
1844, 0.005088739
1852, 0.0050887386
1853, 0.0033971712
1843, 0.003397171
1842, 0.002112137
1854, 0.002112137
1855, 0.0010177146
1841, 0.0010177145
1856, 5.467534E-4
1840, 5.4675335E-4
1839, 2.8450164E-4
1857, 2.8450164E-4
1838, 1.434208E-4
1858, 1.4342077E-4
1837, 6.993723E-5
1859, 6.993723E-5
1836, 3.2718348E-5
1860, 3.2718348E-5
1835, 1.1241766E-5
1861, 1.1241766E-5
1834, 4.7567646E-6
1862, 4.7567646E-6
1833, 1.964726E-6
1863, 1.964726E-6
1832, 7.9068286E-7
1864, 7.9068275E-7
1831, 3.091121E-7
1865, 3.0911204E-7
1830, 1.1649661E-7
1866, 1.1649661E-7
1829, 2.8511607E-8
1867, 2.8511606E-8
1828, 1.0028896E-8
1868, 1.0028894E-8
1869, 3.43432E-9
1827, 3.4343197E-9
1826, 1.1402894E-9
1870, 1.1402892E-9
1825, 3.6497083E-10
1871, 3.6497078E-10
1872, 1.1142583E-10
1824, 1.1142583E-10
1873, 1.6185813E-11

top documents: 
_________________________________________________________________


