SA configuration: 
	sa.act_diff_ratio_limit = 0.05
	sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
	sa.id_property_name.DATE = dateID
	sa.id_property_name.DOCUMENT = docID
	sa.id_property_name.LOCATION = locID
	sa.max_pulses = 10
	sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 2.0)
	sa.progress_prefix = ONEDATE_
	sa.relationships_to_use = SIMLOC
	sa.weight_property.REFERENCES = relevance
	sa.weight_property.SIMDATES = sim
	sa.weight_property.SIMDOC = sim
	sa.weight_property.SIMLOC = sim

initial document activations (1): 
	prague, 1, LOCATION

#   pulse 0: activated nodes: 1, borderline nodes: 1, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 11, borderline nodes: 10, overall activation: 2.802, activation diff: 2.802, ratio: 1.000
#   pulse 2: activated nodes: 31, borderline nodes: 20, overall activation: 6.069, activation diff: 3.266, ratio: 0.538
#   pulse 3: activated nodes: 62, borderline nodes: 31, overall activation: 10.407, activation diff: 4.338, ratio: 0.417
#   pulse 4: activated nodes: 112, borderline nodes: 50, overall activation: 15.863, activation diff: 5.457, ratio: 0.344
#   pulse 5: activated nodes: 157, borderline nodes: 45, overall activation: 22.585, activation diff: 6.722, ratio: 0.298
#   pulse 6: activated nodes: 219, borderline nodes: 62, overall activation: 29.911, activation diff: 7.325, ratio: 0.245
#   pulse 7: activated nodes: 219, borderline nodes: 62, overall activation: 36.879, activation diff: 6.968, ratio: 0.189
#   pulse 8: activated nodes: 219, borderline nodes: 62, overall activation: 42.615, activation diff: 5.736, ratio: 0.135
#   pulse 9: activated nodes: 219, borderline nodes: 62, overall activation: 47.191, activation diff: 4.575, ratio: 0.097
#   pulse 10: activated nodes: 219, borderline nodes: 62, overall activation: 50.876, activation diff: 3.686, ratio: 0.072

final number of activated nodes: 219
final overall activation: 50.9
number of spread. activ. pulses: 10
running time: 586

top nodes: 
prague, 0.99869424
bohemia, 0.99848974
metr, 0.9984057
central europe, 0.9977077
grunt, 0.99770725
theresienstadt, 0.9974077
mir, 0.99667794
dresden, 0.99568856
rodin, 0.9946598
marienbad, 0.9795501
babylonia, 0.96474415
saxony, 0.7934466
dd, 0.79150295
rocknitz, 0.7907768
mars, 0.7907518
grimma, 0.79072106
leipzig, 0.79014194
moravia, 0.7897943
nuremberg, 0.78476393
middle west, 0.78456664
olmntz, 0.7826873
eichstiitt, 0.7818465
erlangen, 0.7817147
eichstatt, 0.7813873
bavaria, 0.77999413
460, 0.770864
troppau, 0.7691144
androa, 0.7680551
hofburg, 0.7467956
vienna, 0.74666613
harland, 0.70205814
merseburg, 0.5894592
halle, 0.5889432
fronte, 0.58688885
burschen, 0.5853787
weimar, 0.57224995
kneipe, 0.5702821
augsburg, 0.569249
potsdam, 0.5685514
holocaust, 0.56740284
amfortas, 0.5673642
berlin, 0.56728715
woltmann, 0.5670438
beriin, 0.5664635
desiderius, 0.56505394
parzival, 0.557101
munich, 0.55578256
alei, 0.5551307
schweinfurt, 0.51072174
sadowa, 0.5067544

top documents: 
_________________________________________________________________


