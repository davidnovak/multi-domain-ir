SA configuration: 
	sa.act_diff_ratio_limit = 0.05
	sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
	sa.id_property_name.DATE = dateID
	sa.id_property_name.DOCUMENT = docID
	sa.id_property_name.LOCATION = locID
	sa.max_pulses = 10
	sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
	sa.progress_prefix = 
	sa.relationships_to_use = SIMDATES:REFERENCES
	sa.weight_property.REFERENCES = relevance
	sa.weight_property.SIMDATES = sim
	sa.weight_property.SIMDOC = sim
	sa.weight_property.SIMLOC = sim

initial document activations (2): 
	doc1_10, 1, DOCUMENT
	doc1_20, 1, DOCUMENT

#   pulse 0: activated nodes: 2, borderline nodes: 2, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 5, borderline nodes: 3, overall activation: 1.843, activation diff: 1.843, ratio: 1.000
#   pulse 2: activated nodes: 11, borderline nodes: 6, overall activation: 1.946, activation diff: 0.510, ratio: 0.262
#   pulse 3: activated nodes: 17, borderline nodes: 6, overall activation: 2.016, activation diff: 0.270, ratio: 0.134
#   pulse 4: activated nodes: 23, borderline nodes: 6, overall activation: 2.050, activation diff: 0.169, ratio: 0.082
#   pulse 5: activated nodes: 29, borderline nodes: 6, overall activation: 2.060, activation diff: 0.108, ratio: 0.052
#   pulse 6: activated nodes: 35, borderline nodes: 6, overall activation: 2.054, activation diff: 0.072, ratio: 0.035

final number of activated nodes: 35
final overall activation: 2.1
number of spread. activ. pulses: 6
running time: 85

top nodes: 
1848, 0.39496115
doc1_20, 0.31044552
doc1_10, 0.31044552
1900, 0.2114707
1800, 0.2114707
1847, 0.12135058
1849, 0.12135058
1801, 0.06662818
1901, 0.06662818
1799, 0.06662818
1899, 0.06662818
1846, 0.022825094
1850, 0.022825094
1802, 0.012856342
1902, 0.012856342
1798, 0.012856342
1898, 0.012856342
1845, 0.0020485814
1851, 0.0020485814
1803, 0.0011861443
1903, 0.0011861443
1797, 0.0011861443
1897, 0.0011861443
1844, 5.57245E-5
1852, 5.57245E-5
1796, 3.313987E-5
1904, 3.313987E-5
1804, 3.313987E-5
1896, 3.313987E-5
1843, 0.0
1905, 0.0
1805, 0.0
1795, 0.0
1895, 0.0
1853, 0.0

top documents: 
doc1_20, 0.31044552
doc1_10, 0.31044552
_________________________________________________________________


