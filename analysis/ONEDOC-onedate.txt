SA configuration: 
	sa.act_diff_ratio_limit = 0.05
	sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
	sa.id_property_name.DATE = dateID
	sa.id_property_name.DOCUMENT = docID
	sa.id_property_name.LOCATION = locID
	sa.max_pulses = 10
	sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
	sa.progress_prefix = 
	sa.relationships_to_use = SIMDATES:REFERENCES
	sa.weight_property.REFERENCES = relevance
	sa.weight_property.SIMDATES = sim
	sa.weight_property.SIMDOC = sim
	sa.weight_property.SIMLOC = sim

initial document activations (1): 
	doc1_10, 1, DOCUMENT

#   pulse 0: activated nodes: 1, borderline nodes: 1, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 2, borderline nodes: 1, overall activation: 0.731, activation diff: 0.731, ratio: 1.000
#   pulse 2: activated nodes: 4, borderline nodes: 2, overall activation: 0.615, activation diff: 0.269, ratio: 0.437
#   pulse 3: activated nodes: 6, borderline nodes: 2, overall activation: 0.534, activation diff: 0.169, ratio: 0.316
#   pulse 4: activated nodes: 8, borderline nodes: 2, overall activation: 0.467, activation diff: 0.105, ratio: 0.225
#   pulse 5: activated nodes: 10, borderline nodes: 2, overall activation: 0.408, activation diff: 0.071, ratio: 0.175
#   pulse 6: activated nodes: 12, borderline nodes: 2, overall activation: 0.356, activation diff: 0.059, ratio: 0.166
#   pulse 7: activated nodes: 12, borderline nodes: 2, overall activation: 0.309, activation diff: 0.049, ratio: 0.158
#   pulse 8: activated nodes: 12, borderline nodes: 2, overall activation: 0.268, activation diff: 0.042, ratio: 0.157
#   pulse 9: activated nodes: 12, borderline nodes: 2, overall activation: 0.231, activation diff: 0.037, ratio: 0.158
#   pulse 10: activated nodes: 12, borderline nodes: 2, overall activation: 0.200, activation diff: 0.032, ratio: 0.159

final number of activated nodes: 12
final overall activation: 0.2
number of spread. activ. pulses: 10
running time: 97

top nodes: 
1848, 0.06664556
doc1_10, 0.04775882
1847, 0.031316243
1849, 0.031316243
1846, 0.009505058
1850, 0.009505058
1845, 0.0016324266
1851, 0.0016324266
1852, 1.1436727E-4
1844, 1.1436727E-4
1853, 0.0
1843, 0.0

top documents: 
doc1_10, 0.04775882
_________________________________________________________________


