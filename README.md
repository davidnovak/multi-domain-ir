# Multi-domain Information Retrieval using Spreading Activation #

### Internal Google document ###
[link](https://docs.google.com/document/d/1KTb-HdeK_27txn2dls41E8oBQnlZpNhsNka87f0kcCY/edit)

### What is this repository for? ###

* Java sources that use the Neo4j to implement the spreading activation for multi-domain information retrieval
* Example data to be inserted into Neo4j
* Description of data and processes

### Internal Testing Environment ###

* The common testing Neo4j graph database is store in  /mnt/nfs/work3/dnovak/neo4j/data

There are two ways to use the database

1. embedding it into a Java application being developed in this Maven repository
1. using a standalone server: 

The server is normally running on node "compute-1-9", accesible from sydney by
```
#!bash

qlogin -l hostname=compute-1-9
```

If the server is not running, it might be started using
```
#!bash

/home/dnovak/neo4j/bin/neo4j start
```


The GUI is accessible at [http://sydney.cs.umass.edu:7474/](http://sydney.cs.umass.edu:7474/)
and you can use commands like

```
#!bash

MATCH (d:DOCUMENT {docID:'generalhistoryfo00myerrich_787'}) RETURN d
```

![neo4j.png](https://bitbucket.org/repo/ggLzMp/images/1290445536-neo4j.png)


### Data ###
The data can be imported into the database from CVS format like this:

documents:
```
#!bash
docID:ID,name,yearPublished:int
book1_page21,"History of wars - page 21",1901
book2_page03,"Revolutionary year 1848 - page 03",1888
book3_page111,"The time of steel - page 211",1900
```

locations:
```
#!bash
locID:ID,loc_lat:float,loc_long:float
america,37.09024,-95.712891
belgium,50.503887,4.469936
```

documents-locations:
```
#!bash
locID:ID,relevance:float,docID:ID
america,1.0,frenchassemblyof00curt_108
belgium,1.0,frenchassemblyof00curt_242
```

documents-dates:
```
#!bash
dateID:ID,relevance:float,docID:ID
1782,1.0,frenchassemblyof00curt_146
1848,1.0,frenchassemblyof00curt_251
```

dates-dates:
```
#!bash
dateID:ID,sim:float,dateID:ID
1782,0.78,1781
1782,0.32,1780
```


### Building the project
The MESSIF project is needed to compile this project:
https://bitbucket.org/disalab/messif
Or you can use  lib/messif-2.3.5-DEVEL.jar and lib/messif-2.3.5-DEVEL.pom

To compile the project by Maven, copy the MESSIF .jar and .pom to:
~/.m2/repository/messif/messif/2.3.5-DEVEL/

And then run
```
#!bash

mvn clean install dependency:copy-dependencies

```
to copy all necessary jar files to target/dependency. Requires Java 8:
```
#!bash

export JAVA_HOME=/share/apps/java8
BIN=${JAVA_HOME}/bin
if ! echo ${PATH} | /bin/grep -q ${BIN} ; then export PATH=${BIN}:${PATH} fi
```


### Load the data
Example command to fill the data into a(n empty) Neo4j database:
```
#!bash

java -cp "target/*:target/dependency/*" ciir.multi.LoadData -b databases/22books/ -l 22books/locations.csv -o 22books/doc-loc.csv -a 22books/doc-date.csv -e 22books/date-date.csv -c 22books/loc-loc.csv
```

### Spreading activation
The following example command should work:
```
#!bash

java -cp "target/*:target/dependency/*" ciir.multi.RunSA -b databases/22books/ -c sa-config.properties -a 22books/top-10-relevant.csv -p lambda 0.0,0.25,0.5,0.75 -p decay 0.1,0.2,0.25,0.5 -p norm_threshold 0,0.1,0.2,0.3 -p max_pulses 5,10 -o -t 22books/output/ 1 Q1 REF-SIMDATE-SIMLOC 20 > 22books/sa-output-REF-SIMDATE-SIMLOC.txt
```


### Who do I talk to? ###

* DD&J