###################################
# parameters: 
#   date_th: 0.6
#   doc_th: 0.95
#   loc_th: 0.4
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.6
#   sa.spread_threshold.DOCUMENT = 0.95
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 2699, non-zero: 2699, borderline: 2692, overall act: 884.804, act diff: 880.569, ratio: 0.995
#   pulse 3: activated: 2699, non-zero: 2699, borderline: 2692, overall act: 884.804, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 2699
#   final overall activation: 884.8
#   number of spread. activ. pulses: 3
#   running time: 130

###################################
# top k results in TREC format: 

1   rel1-1   generalhistoryfo00myerrich_787   1   1.0   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_327   2   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_318   3   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   generalhistoryfo00myerrich_750   4   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   historyofuniteds07good_239   5   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   europesincenapol00leveuoft_99   6   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_326   7   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_325   8   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_323   9   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_115   10   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_110   11   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_294   12   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_293   13   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_185   14   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_176   15   0.8194999   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_23   16   0.7278944   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_61   17   0.7278944   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_64   18   0.7278944   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_504   19   0.7278944   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
1   rel1-1   ouroldworldbackg00bearrich_329   20   0.7278944   REFERENCES-date_th0.6-doc_th0.95-loc_th0.4
