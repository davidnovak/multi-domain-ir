###################################
# parameters: 
#   date_th: 0.5
#   doc_th: 0.8
#   loc_th: 0.4
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.5
#   sa.spread_threshold.DOCUMENT = 0.8
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   shorthistoryofmo00haslrich_98, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 11, overall act: 6.083, act diff: 5.083, ratio: 0.836
#   pulse 2: activated: 2696, non-zero: 2696, borderline: 2685, overall act: 924.454, act diff: 918.371, ratio: 0.993
#   pulse 3: activated: 2916, non-zero: 2916, borderline: 2883, overall act: 1042.156, act diff: 117.702, ratio: 0.113
#   pulse 4: activated: 5927, non-zero: 5927, borderline: 5805, overall act: 4248.621, act diff: 3206.465, ratio: 0.755
#   pulse 5: activated: 8424, non-zero: 8424, borderline: 5623, overall act: 6097.888, act diff: 1849.267, ratio: 0.303
#   pulse 6: activated: 10027, non-zero: 10027, borderline: 4892, overall act: 8398.850, act diff: 2300.962, ratio: 0.274
#   pulse 7: activated: 11015, non-zero: 11015, borderline: 3090, overall act: 9157.852, act diff: 759.002, ratio: 0.083
#   pulse 8: activated: 11071, non-zero: 11071, borderline: 2067, overall act: 9249.346, act diff: 91.494, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11071
#   final overall activation: 9249.3
#   number of spread. activ. pulses: 8
#   running time: 561

###################################
# top k results in TREC format: 

1   rel1-2   historyofuniteds07good_349   1   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_345   2   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_347   3   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_348   4   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bookman44unkngoog_930   5   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_139   6   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bookman44unkngoog_929   7   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_140   8   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_111   9   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_472   10   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_842   11   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_473   12   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_470   13   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_471   14   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_845   15   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_474   16   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_822   17   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_836   18   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_839   19   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_350   20   1.0   REFERENCES-date_th0.5-doc_th0.8-loc_th0.4
