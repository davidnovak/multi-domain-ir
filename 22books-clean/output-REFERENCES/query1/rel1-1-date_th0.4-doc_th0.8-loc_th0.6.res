###################################
# parameters: 
#   date_th: 0.4
#   doc_th: 0.8
#   loc_th: 0.6
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.4
#   sa.spread_threshold.DOCUMENT = 0.8
#   sa.spread_threshold.LOCATION = 0.6
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 106, non-zero: 106, borderline: 104, overall act: 26.484, act diff: 22.249, ratio: 0.840
#   pulse 3: activated: 106, non-zero: 106, borderline: 104, overall act: 26.484, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 106
#   final overall activation: 26.5
#   number of spread. activ. pulses: 3
#   running time: 46

###################################
# top k results in TREC format: 

1   rel1-1   generalhistoryfo00myerrich_787   1   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   essentialsinmod01howegoog_262   2   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   essentialsinmod01howegoog_264   3   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   essentialsinmod01howegoog_255   4   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   shorthistoryofmo00haslrich_191   5   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   encyclopediaame28unkngoog_553   6   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   encyclopediaame28unkngoog_152   7   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   neweasterneurop00butlgoog_79   8   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   cambridgemodern09protgoog_778   9   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   europesincenapol00leveuoft_359   10   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   europesincenapol00leveuoft_44   11   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   cambridgemodern09protgoog_416   12   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   cambridgemodern09protgoog_415   13   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   ouroldworldbackg00bearrich_399   14   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   encyclopediaame28unkngoog_273   15   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   englishcyclopae05kniggoog_134   16   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   encyclopediaame28unkngoog_276   17   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   essentialsinmod01howegoog_263   18   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   essentialsinmod01howegoog_261   19   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
1   rel1-1   cambridgemodern09protgoog_420   20   0.22703262   REFERENCES-date_th0.4-doc_th0.8-loc_th0.6
