###################################
# parameters: 
#   date_th: 0.6
#   doc_th: 0.8
#   loc_th: 0.4
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.6
#   sa.spread_threshold.DOCUMENT = 0.8
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 2699, non-zero: 2699, borderline: 2692, overall act: 884.804, act diff: 880.569, ratio: 0.995
#   pulse 3: activated: 2818, non-zero: 2818, borderline: 2797, overall act: 947.882, act diff: 63.078, ratio: 0.067
#   pulse 4: activated: 5284, non-zero: 5284, borderline: 5229, overall act: 3730.516, act diff: 2782.634, ratio: 0.746
#   pulse 5: activated: 7617, non-zero: 7617, borderline: 5418, overall act: 5428.761, act diff: 1698.244, ratio: 0.313
#   pulse 6: activated: 9741, non-zero: 9741, borderline: 5388, overall act: 8162.262, act diff: 2733.501, ratio: 0.335
#   pulse 7: activated: 10994, non-zero: 10994, borderline: 3343, overall act: 9122.598, act diff: 960.336, ratio: 0.105
#   pulse 8: activated: 11067, non-zero: 11067, borderline: 2103, overall act: 9237.707, act diff: 115.109, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11067
#   final overall activation: 9237.7
#   number of spread. activ. pulses: 8
#   running time: 523

###################################
# top k results in TREC format: 

1   rel1-1   historyofuniteds07good_349   1   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_345   2   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_347   3   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_348   4   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   bookman44unkngoog_930   5   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_139   6   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   bookman44unkngoog_929   7   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_140   8   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_111   9   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_842   10   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_473   11   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_470   12   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_471   13   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_845   14   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_474   15   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_822   16   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_836   17   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_839   18   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   generalhistoryfo00myerrich_787   19   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_350   20   1.0   REFERENCES-date_th0.6-doc_th0.8-loc_th0.4
