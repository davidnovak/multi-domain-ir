###################################
# parameters: 
#   date_th: 0.4
#   doc_th: 0.8
#   loc_th: 0.4
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.spread_threshold.DATE = 0.4
#   sa.spread_threshold.DOCUMENT = 0.8
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 2734, non-zero: 2734, borderline: 2726, overall act: 903.879, act diff: 899.644, ratio: 0.995
#   pulse 3: activated: 2885, non-zero: 2885, borderline: 2862, overall act: 985.433, act diff: 81.554, ratio: 0.083
#   pulse 4: activated: 5684, non-zero: 5684, borderline: 5590, overall act: 4050.060, act diff: 3064.627, ratio: 0.757
#   pulse 5: activated: 8170, non-zero: 8170, borderline: 5584, overall act: 5874.774, act diff: 1824.713, ratio: 0.311
#   pulse 6: activated: 9951, non-zero: 9951, borderline: 4874, overall act: 8332.766, act diff: 2457.992, ratio: 0.295
#   pulse 7: activated: 11023, non-zero: 11023, borderline: 2957, overall act: 9155.215, act diff: 822.449, ratio: 0.090
#   pulse 8: activated: 11083, non-zero: 11083, borderline: 1888, overall act: 9250.856, act diff: 95.641, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11083
#   final overall activation: 9250.9
#   number of spread. activ. pulses: 8
#   running time: 1160

###################################
# top k results in TREC format: 

1   rel1-1   historyofuniteds07good_349   1   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_345   2   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_347   3   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_348   4   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_139   5   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bookman44unkngoog_929   6   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_140   7   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_111   8   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bostoncollegebul0405bost_5   9   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_473   10   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_470   11   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_471   12   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_845   13   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_474   14   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_822   15   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_836   16   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_839   17   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   generalhistoryfo00myerrich_787   18   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   bookman44unkngoog_930   19   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
1   rel1-1   historyofuniteds07good_350   20   1.0   REFERENCES-date_th0.4-doc_th0.8-loc_th0.4
