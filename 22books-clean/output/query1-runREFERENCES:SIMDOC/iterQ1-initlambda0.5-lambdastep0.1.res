###################################
# parameters: 
#   initlambda: 0.5
#   lambdastep: 0.1
#   relationships: REFERENCES:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.function = ciir.multi.sa.GrowingLambdaOutDivSAFunction(norm_function, 0.5, 0.1)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.IdentityFunction()
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 1: activated nodes: 197, borderline nodes: 192, overall activation: 5.000, activation diff: 4.971, ratio: 0.994
#   pulse 2: activated nodes: 5013, borderline nodes: 4816, overall activation: 5.000, activation diff: 1.955, ratio: 0.391
#   pulse 3: activated nodes: 11735, borderline nodes: 6722, overall activation: 5.000, activation diff: 1.209, ratio: 0.242
#   pulse 4: activated nodes: 13019, borderline nodes: 1284, overall activation: 5.000, activation diff: 0.708, ratio: 0.142
#   pulse 5: activated nodes: 13070, borderline nodes: 51, overall activation: 5.000, activation diff: 0.320, ratio: 0.064
#   pulse 6: activated nodes: 13070, borderline nodes: 0, overall activation: 5.000, activation diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 13070
#   final overall activation: 5.0
#   number of spread. activ. pulses: 6
#   running time: 1807

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.16838397   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.16456546   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.15936786   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   4   0.15680347   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.15563688   REFERENCES:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.012274544   REFERENCES:SIMDOC
1   Q1   cambridgemodern09protgoog_401   7   0.0105060795   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_399   8   0.010491204   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_760   9   0.009421408   REFERENCES:SIMDOC
1   Q1   generalhistoryfo00myerrich_775   10   0.009271932   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_104   11   0.008457985   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   12   0.008135604   REFERENCES:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   13   0.008053511   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_240   14   0.007981453   REFERENCES:SIMDOC
1   Q1   encyclopediaame28unkngoog_326   15   0.007960012   REFERENCES:SIMDOC
1   Q1   europesincenapol00leveuoft_44   16   0.00751311   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_132   17   0.0073958514   REFERENCES:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   18   0.007376235   REFERENCES:SIMDOC
1   Q1   shorthistoryofmo00haslrich_42   19   0.007238563   REFERENCES:SIMDOC
1   Q1   lifeofstratfordc02laneuoft_201   20   0.007175764   REFERENCES:SIMDOC
