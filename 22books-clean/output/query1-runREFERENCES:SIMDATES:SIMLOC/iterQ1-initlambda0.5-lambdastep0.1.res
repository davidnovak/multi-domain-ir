###################################
# parameters: 
#   initlambda: 0.5
#   lambdastep: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.function = ciir.multi.sa.GrowingLambdaOutDivSAFunction(norm_function, 0.5, 0.1)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.IdentityFunction()
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 2: activated nodes: 4500, borderline nodes: 4453, overall activation: 5.000, activation diff: 1.965, ratio: 0.393
#   pulse 3: activated nodes: 8299, borderline nodes: 3799, overall activation: 5.000, activation diff: 1.284, ratio: 0.257
#   pulse 4: activated nodes: 11332, borderline nodes: 3033, overall activation: 5.000, activation diff: 0.722, ratio: 0.144
#   pulse 5: activated nodes: 11450, borderline nodes: 118, overall activation: 5.000, activation diff: 0.319, ratio: 0.064
#   pulse 6: activated nodes: 11458, borderline nodes: 8, overall activation: 5.000, activation diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 5.0
#   number of spread. activ. pulses: 6
#   running time: 1130

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.15827738   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.15805359   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   3   0.15667082   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.15613341   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.15553482   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   6   0.00503757   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.0047165677   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   8   0.004554607   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_188   9   0.0044814944   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_52   10   0.0042995717   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.00410024   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   12   0.004051574   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_104   13   0.004006365   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_105   14   0.0039043936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.0038662727   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_135   16   0.003678851   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_110   17   0.003675276   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_267   18   0.0036532797   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_132   19   0.0036330628   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_115   20   0.0035275884   REFERENCES:SIMDATES:SIMLOC
