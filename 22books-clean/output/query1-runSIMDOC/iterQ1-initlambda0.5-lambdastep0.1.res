###################################
# parameters: 
#   initlambda: 0.5
#   lambdastep: 0.1
#   relationships: SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.function = ciir.multi.sa.GrowingLambdaOutDivSAFunction(norm_function, 0.5, 0.1)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.IdentityFunction()
#   sa.progress_prefix = 
#   sa.relationships_to_use = SIMDOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 1: activated nodes: 155, borderline nodes: 150, overall activation: 5.000, activation diff: 4.840, ratio: 0.968
#   pulse 2: activated nodes: 1864, borderline nodes: 1709, overall activation: 5.000, activation diff: 1.925, ratio: 0.385
#   pulse 3: activated nodes: 5738, borderline nodes: 3874, overall activation: 5.000, activation diff: 1.099, ratio: 0.220
#   pulse 4: activated nodes: 8587, borderline nodes: 2849, overall activation: 5.000, activation diff: 0.649, ratio: 0.130
#   pulse 5: activated nodes: 8967, borderline nodes: 380, overall activation: 5.000, activation diff: 0.297, ratio: 0.059
#   pulse 6: activated nodes: 8978, borderline nodes: 11, overall activation: 5.000, activation diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8978
#   final overall activation: 5.0
#   number of spread. activ. pulses: 6
#   running time: 1336

###################################
# top k results in TREC format: 

1   Q1   shorthistoryofmo00haslrich_98   1   0.20393589   SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   0.18073641   SIMDOC
1   Q1   generalhistoryfo00myerrich_787   3   0.17984484   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_185   4   0.16047788   SIMDOC
1   Q1   cambridgemodern09protgoog_449   5   0.15855455   SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.06405234   SIMDOC
1   Q1   ouroldworldbackg00bearrich_399   7   0.062354002   SIMDOC
1   Q1   ouroldworldbackg00bearrich_400   8   0.056246307   SIMDOC
1   Q1   europesincenapol00leveuoft_53   9   0.055338383   SIMDOC
1   Q1   selwynhouseschoo1940selw_22   10   0.04658823   SIMDOC
1   Q1   lifeofstratfordc02laneuoft_193   11   0.04451014   SIMDOC
1   Q1   encyclopediaame28unkngoog_318   12   0.037529476   SIMDOC
1   Q1   essentialsinmod01howegoog_265   13   0.03746274   SIMDOC
1   Q1   essentialsinmod01howegoog_129   14   0.036577903   SIMDOC
1   Q1   generalhistoryfo00myerrich_760   15   0.03522872   SIMDOC
1   Q1   shorthistoryofmo00haslrich_90   16   0.03250508   SIMDOC
1   Q1   generalhistoryfo00myerrich_775   17   0.027989848   SIMDOC
1   Q1   cambridgemodern09protgoog_748   18   0.027127087   SIMDOC
1   Q1   cambridgemodern09protgoog_743   19   0.025039475   SIMDOC
1   Q1   europesincenapol00leveuoft_44   20   0.024992619   SIMDOC
