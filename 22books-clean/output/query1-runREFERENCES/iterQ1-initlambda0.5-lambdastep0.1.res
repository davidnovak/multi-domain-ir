###################################
# parameters: 
#   initlambda: 0.5
#   lambdastep: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.function = ciir.multi.sa.GrowingLambdaOutDivSAFunction(norm_function, 0.5, 0.1)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.IdentityFunction()
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 2: activated nodes: 4203, borderline nodes: 4156, overall activation: 5.000, activation diff: 1.951, ratio: 0.390
#   pulse 3: activated nodes: 7307, borderline nodes: 3104, overall activation: 5.000, activation diff: 1.273, ratio: 0.255
#   pulse 4: activated nodes: 10472, borderline nodes: 3165, overall activation: 5.000, activation diff: 0.715, ratio: 0.143
#   pulse 5: activated nodes: 11264, borderline nodes: 792, overall activation: 5.000, activation diff: 0.316, ratio: 0.063
#   pulse 6: activated nodes: 11330, borderline nodes: 66, overall activation: 5.000, activation diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 5.0
#   number of spread. activ. pulses: 6
#   running time: 1037

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.16666904   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   2   0.1604485   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   3   0.15740743   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.15636013   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.1559268   REFERENCES
1   Q1   cambridgemodern09protgoog_401   6   0.011270194   REFERENCES
1   Q1   encyclopediaame28unkngoog_188   7   0.0066750906   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   8   0.00559687   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.005442185   REFERENCES
1   Q1   encyclopediaame28unkngoog_135   10   0.0052195154   REFERENCES
1   Q1   europesincenapol00leveuoft_132   11   0.005175514   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   12   0.0051003788   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.004994794   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   14   0.0048786113   REFERENCES
1   Q1   europesincenapol00leveuoft_52   15   0.0047183875   REFERENCES
1   Q1   englishcyclopae05kniggoog_75   16   0.004625808   REFERENCES
1   Q1   shorthistoryofmo00haslrich_104   17   0.0044056275   REFERENCES
1   Q1   shorthistoryofmo00haslrich_105   18   0.004286656   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.0042547723   REFERENCES
1   Q1   shorthistoryofmo00haslrich_110   20   0.004067028   REFERENCES
