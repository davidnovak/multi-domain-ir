###################################
# parameters: 
#   initlambda: 0.5
#   lambdastep: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.01
#   sa.function = ciir.multi.sa.GrowingLambdaOutDivSAFunction(norm_function, 0.5, 0.1)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.IdentityFunction()
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 5, borderline nodes: 5, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 1: activated nodes: 47, borderline nodes: 42, overall activation: 5.000, activation diff: 5.000, ratio: 1.000
#   pulse 2: activated nodes: 4214, borderline nodes: 4167, overall activation: 5.000, activation diff: 1.951, ratio: 0.390
#   pulse 3: activated nodes: 7452, borderline nodes: 3238, overall activation: 5.000, activation diff: 1.272, ratio: 0.254
#   pulse 4: activated nodes: 10557, borderline nodes: 3105, overall activation: 5.000, activation diff: 0.715, ratio: 0.143
#   pulse 5: activated nodes: 11305, borderline nodes: 748, overall activation: 5.000, activation diff: 0.316, ratio: 0.063
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5.000, activation diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5.0
#   number of spread. activ. pulses: 6
#   running time: 927

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   0.16662747   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   2   0.16043283   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   3   0.15736246   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.15624262   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.15591112   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_401   6   0.011269842   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_188   7   0.0066671045   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   8   0.0055967006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.005425569   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_135   10   0.0051764045   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_132   11   0.0051754126   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   12   0.005058445   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.004996378   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   14   0.0048243566   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_52   15   0.004671987   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_75   16   0.0046095126   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_104   17   0.0043883263   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_105   18   0.0042915107   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.0042383475   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_110   20   0.004066791   REFERENCES:SIMDATES
