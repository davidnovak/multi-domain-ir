###################################
# parameters: 
#   date_th: 0.4
#   doc_th: 0.95
#   loc_th: 0.4
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.spread_threshold.DATE = 0.4
#   sa.spread_threshold.DOCUMENT = 0.95
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 2736, non-zero: 2736, borderline: 2728, overall act: 904.333, act diff: 900.098, ratio: 0.995
#   pulse 3: activated: 2736, non-zero: 2736, borderline: 2728, overall act: 904.333, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 2736
#   final overall activation: 904.3
#   number of spread. activ. pulses: 3
#   running time: 99

###################################
# top k results in TREC format: 

1   rel1-1   generalhistoryfo00myerrich_787   1   1.0   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_326   2   0.8823656   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_325   3   0.8823656   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_327   4   0.8823656   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   historyofuniteds07good_239   5   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   europesincenapol00leveuoft_99   6   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_318   7   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   generalhistoryfo00myerrich_750   8   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_323   9   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   englishcyclopae05kniggoog_147   10   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_115   11   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_110   12   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_294   13   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_293   14   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_185   15   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   essentialsinmod01howegoog_176   16   0.8194999   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_23   17   0.7278944   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_61   18   0.7278944   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   shorthistoryofmo00haslrich_64   19   0.7278944   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
1   rel1-1   encyclopediaame28unkngoog_504   20   0.7278944   REFERENCES:SIMDATES-date_th0.4-doc_th0.95-loc_th0.4
