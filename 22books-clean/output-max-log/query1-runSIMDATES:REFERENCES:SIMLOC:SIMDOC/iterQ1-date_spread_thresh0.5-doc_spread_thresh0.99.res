###################################
# parameters: 
#   date_spread_thresh: 0.5
#   doc_spread_thresh: 0.99
#   relationships: SIMDATES:REFERENCES:SIMLOC:SIMDOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSALogDivFunction(norm_function, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = SIMDATES:REFERENCES:SIMLOC:SIMDOC
#   sa.spread_threshold.DATE = 0.5
#   sa.spread_threshold.DOCUMENT = 0.99
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 5, non-zero: 5, borderline: 5, overall act: 5.000, act diff: 5.000, ratio: 1.000
#   pulse 1: activated: 47, non-zero: 47, borderline: 42, overall act: 27.173, act diff: 22.173, ratio: 0.816
#   pulse 2: activated: 4048, non-zero: 4048, borderline: 4009, overall act: 1283.635, act diff: 1256.462, ratio: 0.979
#   pulse 3: activated: 4102, non-zero: 4102, borderline: 4061, overall act: 1314.377, act diff: 30.741, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4102
#   final overall activation: 1314.4
#   number of spread. activ. pulses: 3
#   running time: 176

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   1.0   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   selwynhouseschoo1940selw_21   2   1.0   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_98   3   1.0   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   cambridgemodern09protgoog_449   4   1.0   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_787   5   1.0   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_96   6   0.82526237   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_154   7   0.806212   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   essentialsinmod01howegoog_306   8   0.79781914   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_136   9   0.7949611   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   ouroldworldbackg00bearrich_383   10   0.7915135   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_134   11   0.78531075   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   shorthistoryofmo00haslrich_242   12   0.7792089   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   englishcyclopae05kniggoog_147   13   0.77514845   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_774   14   0.7738985   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_325   15   0.7632632   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_711   16   0.76207644   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   generalhistoryfo00myerrich_782   17   0.76207644   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   politicalsketche00retsrich_153   18   0.7562619   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   europesincenapol00leveuoft_57   19   0.75493133   SIMDATES:REFERENCES:SIMLOC:SIMDOC
1   Q1   encyclopediaame28unkngoog_327   20   0.7513193   SIMDATES:REFERENCES:SIMLOC:SIMDOC
