###################################
# parameters: 
#   date_spread_thresh: 0.3
#   doc_spread_thresh: 0.9
#   relationships: SIMDATES:REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSALogDivFunction(norm_function, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = SIMDATES:REFERENCES:SIMLOC
#   sa.spread_threshold.DATE = 0.3
#   sa.spread_threshold.DOCUMENT = 0.9
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (5): 
#   generalhistoryfo00myerrich_787, 1
#   shorthistoryofmo00haslrich_98, 1
#   lifeofstratfordc02laneuoft_185, 1
#   selwynhouseschoo1940selw_21, 1
#   cambridgemodern09protgoog_449, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 5, non-zero: 5, borderline: 5, overall act: 5.000, act diff: 5.000, ratio: 1.000
#   pulse 1: activated: 47, non-zero: 47, borderline: 42, overall act: 27.173, act diff: 22.173, ratio: 0.816
#   pulse 2: activated: 4214, non-zero: 4214, borderline: 4167, overall act: 1342.628, act diff: 1315.455, ratio: 0.980
#   pulse 3: activated: 4223, non-zero: 4223, borderline: 4175, overall act: 1366.871, act diff: 24.243, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4223
#   final overall activation: 1366.9
#   number of spread. activ. pulses: 3
#   running time: 862

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   1.0   SIMDATES:REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   1.0   SIMDATES:REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   1.0   SIMDATES:REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   1.0   SIMDATES:REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   1.0   SIMDATES:REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   6   0.82981074   SIMDATES:REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.82371503   SIMDATES:REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.806212   SIMDATES:REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8041281   SIMDATES:REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   10   0.7982588   SIMDATES:REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_306   11   0.79781914   SIMDATES:REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   12   0.7949611   SIMDATES:REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.78957826   SIMDATES:REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.78531075   SIMDATES:REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_774   15   0.7827976   SIMDATES:REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_242   16   0.7792089   SIMDATES:REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.765024   SIMDATES:REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_57   18   0.7630204   SIMDATES:REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   19   0.76207644   SIMDATES:REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_782   20   0.76207644   SIMDATES:REFERENCES:SIMLOC
