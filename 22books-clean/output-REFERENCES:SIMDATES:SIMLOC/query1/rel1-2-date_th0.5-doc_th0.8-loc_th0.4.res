###################################
# parameters: 
#   date_th: 0.5
#   doc_th: 0.8
#   loc_th: 0.4
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.5
#   sa.spread_threshold.DOCUMENT = 0.8
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   shorthistoryofmo00haslrich_98, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 11, overall act: 6.083, act diff: 5.083, ratio: 0.836
#   pulse 2: activated: 2696, non-zero: 2696, borderline: 2685, overall act: 924.454, act diff: 918.371, ratio: 0.993
#   pulse 3: activated: 2916, non-zero: 2916, borderline: 2883, overall act: 1042.156, act diff: 117.702, ratio: 0.113
#   pulse 4: activated: 5941, non-zero: 5941, borderline: 5819, overall act: 4258.572, act diff: 3216.416, ratio: 0.755
#   pulse 5: activated: 8501, non-zero: 8501, borderline: 5686, overall act: 6198.080, act diff: 1939.508, ratio: 0.313
#   pulse 6: activated: 10124, non-zero: 10124, borderline: 4877, overall act: 8492.104, act diff: 2294.024, ratio: 0.270
#   pulse 7: activated: 11021, non-zero: 11021, borderline: 2961, overall act: 9204.333, act diff: 712.229, ratio: 0.077
#   pulse 8: activated: 11077, non-zero: 11077, borderline: 1991, overall act: 9310.987, act diff: 106.653, ratio: 0.011

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11077
#   final overall activation: 9311.0
#   number of spread. activ. pulses: 8
#   running time: 510

###################################
# top k results in TREC format: 

1   rel1-2   historyofuniteds07good_349   1   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_345   2   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_347   3   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_348   4   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bookman44unkngoog_930   5   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_139   6   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bookman44unkngoog_929   7   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_140   8   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   bostoncollegebul0405bost_111   9   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_472   10   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_842   11   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_473   12   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_470   13   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_471   14   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_845   15   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_474   16   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_822   17   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_836   18   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_839   19   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
1   rel1-2   historyofuniteds07good_350   20   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.8-loc_th0.4
