###################################
# parameters: 
#   date_th: 0.6
#   doc_th: 0.95
#   loc_th: 0.5
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.6
#   sa.spread_threshold.DOCUMENT = 0.95
#   sa.spread_threshold.LOCATION = 0.5
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   generalhistoryfo00myerrich_787, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 3.235, ratio: 0.764
#   pulse 2: activated: 8, non-zero: 8, borderline: 7, overall act: 4.235, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8
#   final overall activation: 4.2
#   number of spread. activ. pulses: 2
#   running time: 17

###################################
# top k results in TREC format: 

1   rel1-1   generalhistoryfo00myerrich_787   1   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.6-doc_th0.95-loc_th0.5
