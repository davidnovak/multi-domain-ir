###################################
# parameters: 
#   date_th: 0.4
#   doc_th: 0.95
#   loc_th: 0.6
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.4
#   sa.spread_threshold.DOCUMENT = 0.95
#   sa.spread_threshold.LOCATION = 0.6
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   shorthistoryofmo00haslrich_98, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 11, overall act: 6.083, act diff: 5.083, ratio: 0.836
#   pulse 2: activated: 112, non-zero: 112, borderline: 110, overall act: 28.787, act diff: 22.703, ratio: 0.789
#   pulse 3: activated: 112, non-zero: 112, borderline: 110, overall act: 28.787, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 112
#   final overall activation: 28.8
#   number of spread. activ. pulses: 3
#   running time: 21

###################################
# top k results in TREC format: 

1   rel1-2   shorthistoryofmo00haslrich_98   1   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   essentialsinmod01howegoog_262   2   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   essentialsinmod01howegoog_264   3   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   essentialsinmod01howegoog_255   4   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   shorthistoryofmo00haslrich_191   5   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   encyclopediaame28unkngoog_553   6   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   encyclopediaame28unkngoog_152   7   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   neweasterneurop00butlgoog_79   8   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   cambridgemodern09protgoog_778   9   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   europesincenapol00leveuoft_359   10   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   europesincenapol00leveuoft_44   11   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   cambridgemodern09protgoog_416   12   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   cambridgemodern09protgoog_415   13   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   ouroldworldbackg00bearrich_399   14   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   encyclopediaame28unkngoog_273   15   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   englishcyclopae05kniggoog_134   16   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   encyclopediaame28unkngoog_276   17   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   essentialsinmod01howegoog_263   18   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   essentialsinmod01howegoog_261   19   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
1   rel1-2   cambridgemodern09protgoog_420   20   0.22703262   REFERENCES:SIMDATES:SIMLOC-date_th0.4-doc_th0.95-loc_th0.6
