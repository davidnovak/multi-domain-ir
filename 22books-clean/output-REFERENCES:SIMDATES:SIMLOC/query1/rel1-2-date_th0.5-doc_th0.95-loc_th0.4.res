###################################
# parameters: 
#   date_th: 0.5
#   doc_th: 0.95
#   loc_th: 0.4
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.MaxSAFunction(norm_function)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.0, 1.0)
#   sa.progress_prefix = 
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.spread_threshold.DATE = 0.5
#   sa.spread_threshold.DOCUMENT = 0.95
#   sa.spread_threshold.LOCATION = 0.4
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMDOC = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations (1): 
#   shorthistoryofmo00haslrich_98, 1

###################################
# spreading activation process log: 
#   pulse 0: activated: 1, non-zero: 1, borderline: 1, overall act: 1.000, act diff: 1.000, ratio: 1.000
#   pulse 1: activated: 12, non-zero: 12, borderline: 11, overall act: 6.083, act diff: 5.083, ratio: 0.836
#   pulse 2: activated: 2696, non-zero: 2696, borderline: 2685, overall act: 924.454, act diff: 918.371, ratio: 0.993
#   pulse 3: activated: 2696, non-zero: 2696, borderline: 2685, overall act: 924.454, act diff: 0.000, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 2696
#   final overall activation: 924.5
#   number of spread. activ. pulses: 3
#   running time: 106

###################################
# top k results in TREC format: 

1   rel1-2   shorthistoryofmo00haslrich_98   1   1.0   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   shorthistoryofmo00haslrich_136   2   0.9242476   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   politicalsketche00retsrich_154   3   0.9242476   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_367   4   0.8823656   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   englishcyclopae05kniggoog_147   5   0.8823656   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_327   6   0.8823656   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   englishcyclopae05kniggoog_224   7   0.8823656   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   politicalsketche00retsrich_96   8   0.8823656   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   bookman44unkngoog_717   9   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_465   10   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   ouroldworldbackg00bearrich_383   11   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   europesincenapol00leveuoft_138   12   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_323   13   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   shorthistoryofmo00haslrich_134   14   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   encyclopediaame28unkngoog_188   15   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   englishcyclopae05kniggoog_225   16   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_447   17   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_306   18   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   essentialsinmod01howegoog_319   19   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
1   rel1-2   bookman44unkngoog_718   20   0.8194999   REFERENCES:SIMDATES:SIMLOC-date_th0.5-doc_th0.95-loc_th0.4
