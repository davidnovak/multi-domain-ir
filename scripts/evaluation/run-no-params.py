#!/share/apps/python/bin/python


import argparse
import glob
import shlex, subprocess
from subprocess import PIPE
import re
import sys
import os


default_collection = 'book_collection'
#default_sa_file = 'sa-config.properties'

p = argparse.ArgumentParser('runs the spreading activation for given SA config file without any parameters')
p.add_argument('-i', '--initial', help='file pattern to be used as initial files', required=True, nargs='*')
p.add_argument('-c', '--collection', help='collection, default: ' + default_collection, default=default_collection)
p.add_argument('-s', '--saconfig', help='SA config file', required=True)
args = p.parse_args()


def run_query(cmd):
    print >> sys.stderr, "running command: " + str(cmd)
    gproc = subprocess.Popen(shlex.split(cmd), stdin=PIPE, stdout=PIPE)
    outdata, errdata = gproc.communicate()
    if errdata:
        for line in errdata:
            sys.stderr.write(line)

    return outdata


def get_cmd(init_file_list, seed_number, outdir):
    """
    creates the java command line with %s places for the run_id
    :return:
    """
    initials_str=''
    for init_f in init_file_list:
        initials_str += ' -a ' + init_f

    cmd = 'java -Xmx20000m -cp "target/*:target/dependency/*" ciir.multi.RunSA'
    cmd += ' -b databases/' + args.collection
    cmd += ' -c ' + args.saconfig
    cmd += initials_str
    cmd += ' -o -t ' + outdir + ' ' + os.path.splitext(args.saconfig)[0] + ' 20'
    return cmd


def get_init_files():
    """
    returns a map of lists of initial files
    :return:
    """
    init_files = {}
    for init_f in args.initial:
        match = re.match('.*query[0-9]*-rel([0-9]*).*\.csv', init_f)
        if not match or not match.group(1):
            sys.stderr.write('error matching file init file ' + init_f + '\n')
            continue
        seed_number = int(match.group(1))
        if not init_files.has_key(seed_number):
            init_files[seed_number] = []
        init_files[seed_number].append(init_f)
    return init_files


def main():
    outdir=args.collection
    #mkdir -p $OUTDIR
    outfile = open(outdir + '/sa-output.log', 'a')

    init_files = get_init_files()

    for seed_number in init_files.keys():
        cmd = get_cmd(init_files[seed_number], seed_number, outdir)
        output = run_query(cmd)
        if output:
            for line in output:
                outfile.write(line)

    outfile.close()


main()
