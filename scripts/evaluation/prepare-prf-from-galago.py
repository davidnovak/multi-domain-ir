#!/usr/bin/python

import sys
import math

if len(sys.argv) < 2:
    print 'Usage: ' + sys.argv[0] + ' <file-with-galago-output> [<output-file>]'
    print '\t takes galago top-k results and transforms Galago scores to [0,1] scale'
    exit(1)


def main():
    # 1 Q0 youngmidshipman00morrgoog_88 3 -6.01842007 galago
    galago_output = sys.argv[1]
    reader = open(galago_output, 'r')

    docids = []
    distances = []
    linecount = 0
    for line in reader:
        splits = line.split(' ')
        docids.append(splits[2])
        distances.append(float(splits[4]))
        linecount += 1
    reader.close()
    print >> sys.stderr, "lines in the input: " + str(linecount) + "\n"

    largest_value = distances[0]
    smallest_value = distances[-1]
    normalizer = float(largest_value - smallest_value)

    # output
    output = open(sys.argv[2], 'w') if len(sys.argv) > 2 else sys.stdout
    for i in range(0, linecount):
        output.write(docids[i])
        output.write(',')
        distance = (distances[i] - smallest_value) / normalizer
        output.write(str(distance) + '\n')

    if output is not sys.stdout:
        output.close()


main()
