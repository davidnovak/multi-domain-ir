#!/share/apps/python/bin/python


import argparse
import glob
import shlex, subprocess
from subprocess import PIPE
import re
import sys


default_nodes = ['DOCUMENT:DATE:LOCATION']
#default_edges = ['REFERENCES:SIMDATES:SIMLOC:SIMDOC']
default_collection = 'book_collection'

p = argparse.ArgumentParser('runs the spreading activation for given combination of params and initial files')
p.add_argument('-i', '--initial', help='file pattern to be used as initial files', required=True, nargs='*')
p.add_argument('-c', '--collection', help='collection, default: ' + default_collection, default=default_collection)
p.add_argument('-n', '--nodes', help='different nodes to use, default (just one run): ' + str(default_nodes), default=default_nodes, nargs='*')
#p.add_argument('-e', '--edges', help='different types of edges to use, default: ' + str(default_edges), default=default_edges, nargs='*')
p.add_argument('-v', '--variable', help='if used, several values for each params are used', action='store_true')
p.add_argument('-o', '--output', help='location to write to, default is to use same name as collection')
args = p.parse_args()

def run_query(cmd):
    print >> sys.stderr, "running command: " + str(cmd)
    gproc = subprocess.Popen(shlex.split(cmd), stdin=PIPE, stdout=PIPE)
    outdata, errdata = gproc.communicate()
    if errdata:
        for line in errdata:
            sys.stderr.write(line)

    return outdata

# these are the "selected" parameters for individual "number of initial documents"
selected_parameters = {
    1 : {'doc_th': 0.5, 'date_th': 0.25, 'loc_th': 0.35},
    2 : {'doc_th': 0.6, 'date_th': 0.25, 'loc_th': 0.35},
    3 : {'doc_th': 0.7, 'date_th': 0.15, 'loc_th': 0.35}
}
selected_edges = ['REFERENCES:SIMDATES:SIMLOC:SIMPAGE']

# all combination of these variables is used when  -v  param is used
param_variables = {
    'doc_th': '0.5,0.6,0.7',
    'date_th': '0.25,0.3',
    'loc_th': '0.35,0.4'
}
edges_variables = ['REFERENCES', 'REFERENCES:SIMDATES', 'REFERENCES:SIMDATES:SIMLOC', 'REFERENCES:SIMLOC', 'REFERENCES:SIMPAGE',
                   'REFERENCES:SIMDATES:SIMPAGE', 'REFERENCES:SIMDATES:SIMLOC:SIMPAGE', 'REFERENCES:SIMLOC:SIMPAGE']


def get_param_str(number_activated):
    param_str = ''
    if args.variable:
        for param in param_variables.keys():
            param_str += ' -p ' + param + ' ' + param_variables[param]
    else:
        for param in selected_parameters[number_activated].keys():
            param_str += ' -p ' + param + ' ' + str(selected_parameters[number_activated][param])
    return param_str


def get_cmd(init_file_list, seed_number, outdir):
    """
    creates the java command line with %s places for the run_id
    :return:
    """
    initials_str=''
    for init_f in init_file_list:
        initials_str += ' -a ' + init_f

    cmd = 'java -Xmx20000m -cp "target/*:target/dependency/*" ciir.multi.RunSA'
    cmd += ' -b databases/' + args.collection
    cmd += ' -c sa-config.properties'
    cmd += initials_str
    cmd += get_param_str(seed_number)
    cmd += ' -p nodestouse %s -p relationships %s -o -t ' + outdir + ' nodes%s-edges%s 20'
    return cmd


def get_init_files():
    """
    returns a map of lists of initial files
    :return:
    """
    init_files = {}
    for init_f in args.initial:
        match = re.match('.*query[0-9]*-rel([0-9]*).*\.csv', init_f)
        if not match or not match.group(1):
            sys.stderr.write('error matching file init file ' + init_f + '\n')
            continue
        seed_number = int(match.group(1))
        if not init_files.has_key(seed_number):
            init_files[seed_number] = []
        init_files[seed_number].append(init_f)
    return init_files


def main():
    #if output in args:
    #    outdir=args.output
    #else:
    #    outdir=args.collection

    outdir=args.output

    #outdir=args.collection
    #mkdir -p $OUTDIR
    outfile = open(outdir + '/sa-output.log', 'a')

    init_files = get_init_files()

    for seed_number in init_files.keys():
        cmd_pattern = get_cmd(init_files[seed_number], seed_number, outdir)
        for nodes_to_use in args.nodes:
            for edges_to_use in (edges_variables if args.variable else selected_edges):
                cmd = cmd_pattern % (nodes_to_use, edges_to_use, nodes_to_use, edges_to_use)
                output = run_query(cmd)
                if output:
                    for line in output:
                        outfile.write(line)

    outfile.close()


main()
