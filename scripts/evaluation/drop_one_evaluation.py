import os
import argparse
from subprocess import call, Popen, PIPE
import re
import shlex

#open results:
#for each segment seperated by a blank line
#build a dummy doc and run it through galago's eval
#return the average of the evaluation scores of the individual segments
#for each query, for each relevant doc in the list, evaluate p@10 and ndcg@10 for that result list without doc, then take the average over all the result lists for that query

def main():

    p = argparse.ArgumentParser("Takes as input a document. Returns a list of documents order by temporal proximity to the input document")
    p.add_argument('results', help="input document or list of documents (if list must have an extension)")
    p.add_argument('judgments', help="input document or list of documents (if list must have an extension)")
    p.add_argument('drop', help="list of drop documents for each query (format: <qid>,<docid>)")
    args = p.parse_args()

    temp_path = 'temporary_judgments'
    if not os.path.exists(temp_path):
        os.mkdir(temp_path)

    drop_list = []
    for line in open(args.drop):
        drop_list.append(line.strip().split()[0])

    counter = 0
    for doc in drop_list:
        print(doc)
        output = ''
        c = 1
        for line in open(args.results):
            items = line.split()
            if not items[2] == doc:
                output += '{} {} {} {} {} {}\n'.format(items[0],items[1],items[2],c,items[4],items[5])
                c += 1
        writer = open(temp_path+'/'+str(counter)+'.res','w')
        writer.write(output)
        writer.close()
        counter += 1
    #begin evaluation
    results = evaluate_dir(temp_path,10,args.judgments)
    print('MNDCG10: %f MP10 %f' % (results[0],results[1]))
    
    find_unjudged(temp_path,10,args.judgments)

    #delete temporary files
    for i in range(counter):
        os.remove(temp_path+'/'+str(i)+'.res')
            
#given a directory of seperate result files, evaluates them all together
def evaluate_dir(directory: str, k: int, judgments: str) -> float:

    galago_bin = './lemur-galago/core/target/appassembler/bin/galago'

    """
    runs the evaluation on all .res files in the directory
    from each of the files
    :return: file name of the created statistic file or None if not created
    """
    #cmd = galago_bin + ' eval --metrics+map'
    cmd = galago_bin + ' eval'
    cmd += ' --metrics+ndcg' + str(k)
    cmd += ' --metrics+P' + str(k)
    #cmd += ' --metrics+R-prec'
    cmd += ' --judgments=' + judgments

    hasresfiles = False
    for f in os.listdir(directory):
        if f.endswith('.res'):
            if os.path.getsize(directory + '/' + f) > 0:
                cmd += ' --runs+%s' % (directory + '/' + f)
                hasresfiles = True
    if not hasresfiles:
        return None

    #print(cmd)

    gproc = Popen(shlex.split(cmd), stdout=PIPE)
    gout = gproc.communicate()[0].decode('UTF-8')
    lines = gout.strip().split("\n")

    for line in lines:
        print(line)

    map_score = 0.0
    p_ten_score = 0.0
    c = 0
    for line in lines:
        segments = line.strip().split()
        if len(segments) == 3 and not (segments[0] == 'run-id'):
            map_score += float(segments[1])
            p_ten_score += float(segments[2])
            c += 1

    return (map_score/c,p_ten_score/c)

def find_unjudged(directory: str, k: int, judgments: str):

    judged_docs = set()
    for line in open(judgments):
        judged_docs.add(line.split()[0] + ' ' + line.split()[2])

    output = set()
    for f in os.listdir(directory):
        if f.endswith('.res'):
            c = 0
            for line in open(directory + '/' + f):
                if not line.split()[0] + ' ' + line.split()[2] in judged_docs:
                    output.add(line.split()[2])
                c += 1
                if c == 10:
                    break

    for doc in output:
        print(doc)

main()
