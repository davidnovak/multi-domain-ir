#!/share/apps/python/bin/python


import argparse
import glob
import re
import os
import sys

default_k = '10'
default_judged = 'book_collection/binary.rel'

p = argparse.ArgumentParser('gets the documents to be judged for a given directory with results')
p.add_argument('-r', '--results', help='top dir with results', required=True)
p.add_argument('-k', '--top_k', help='size of the top-k list to consider, default: ' + default_k,
               default=default_k)
p.add_argument('-j', '--judged', help='file with judged documents, default: ' + default_judged, default=default_judged)
p.add_argument('-s', '--seeds', help='remove the seed documents - the top "k" results where "k" is taken from the file '
                                     + 'name, e.g. rel3-1 would remove top-3 results', action='store_true')
args = p.parse_args()

out_file_name = "all-top" + str(args.top_k) + ".docs"
to_judge_file_name = 'to-judge-top' + str(args.top_k) + '.docs'


# param "filename"
# takes a file with trec-formatted results and extract only:
#    query_id  doc_id
# 0 britishcostumedu1910ashd_454
def get_just_docs(trec_file, k):
    ret_val = []
    reader = open(trec_file, 'r')
    for line in reader:
        if len(ret_val) >= k:
            break
        matcher = re.match('(^[^#][^ ]*)  *[^ ]*  *([^ ]*).*', line)
        if not matcher:
            continue
        ret_val.append(matcher.group(1) + " " + matcher.group(2))
    reader.close()

    return ret_val


def get_seed_number(file_name):
    if not args.seeds:
        return 0
    match = re.match('.*rel([0-9]*).*\.res', file_name)
    if not match or not match.group(1):
        sys.stderr.write('error matching result file name' + file_name + '\n')
        return 0
    return int(match.group(1))


# print info about created file "filename"
def print_info_file(file_name, length):
    print "... created file " + file_name + "   with # of lines: " + str(length)


def create_file_with_all(out_file, set_with_lines):
    writer = open(out_file, 'w')
    for line in set_with_lines:
        writer.write(line + '\n')
    writer.close()
    print_info_file(out_file, len(set_with_lines))


def create_file_to_judge(out_file, set_with_lines, judged):
    writer = open(out_file, 'w')
    i = 0
    for line in set_with_lines:
        if line not in judged:
            writer.write(line + '\n')
            i += 1
    writer.close()
    print_info_file(out_file, i)


def main():
    judged = set(get_just_docs(args.judged, sys.maxint))

    global_all_docs = set()
    for directory in glob.iglob(args.results + "/*"):
        if not os.path.isdir(directory):
            continue

        all_docs = set()
        for res_file in glob.iglob(directory + '/*.res'):
            seed_number = get_seed_number(res_file)
            for line in get_just_docs(res_file, int(args.top_k) + int(seed_number)):
                all_docs.add(line)

        create_file_with_all(directory + '/' + out_file_name, all_docs)
        create_file_to_judge(directory + '/' + to_judge_file_name, all_docs, judged)

        for line in all_docs:
            global_all_docs.add(line)

    create_file_with_all(args.results + '/' + out_file_name, global_all_docs)
    create_file_to_judge(args.results + '/' + to_judge_file_name, global_all_docs, judged)


main()
