#!/share/apps/python/bin/python


import argparse
import numpy as np
import itertools
import re


p = argparse.ArgumentParser("takes a file with a list of relevant documents and generates files with all subsets of this list")
p.add_argument('-i', '--input', help="input file; expected format for each line: docId 1", required=True)
p.add_argument('-q', '--query', help="query number", required=True)
p.add_argument('-o', '--output', help="output directory")
p.add_argument('-e', '--entities', help="a file with the entities to be added to each initial file")
args = p.parse_args()


def generate_subsets(list, set_size):
    """
    generates all subsets of given size from given list (set) of strings
    :param list:
    :param set_size:
    :return: a list of lists
    """
    return itertools.combinations(list, set_size)


def sigmoid(value):
    steepness =  - 1.0
    threshold = 0.0
    return ((2.0 / (1.0 + np.exp(steepness * (float(value) - threshold)))) - 1.0)


def log_weights(occurences, all_occurences):
    return np.log(occurences + 1) / np.log(all_occurences + 1)


def create_comb_file(combination, queryid, counter):
    run_id = "rel" + str(len(combination)) + "-" + str(counter) + ("-ent" if args.entities else "")
    filename = (args.output + "/" if args.output else "") + \
               "query" + str(queryid) + "-" + run_id  + \
               ".csv"

    writer = open(filename, "w")
    for line in combination:
        writer.write(queryid + " " + run_id + " " + line)
    if args.entities:
        reader = open(args.entities, 'r')
        entities = []
        for line in reader:
            entities.append(line)
        weight = sigmoid(len(combination) * log_weights(1, len(entities)))
        #weight = log_weights(1, len(entities))
        for line in entities:
            writer.write(queryid + " " + run_id + " " + line.replace(' 1 ', ' ' + str(weight) +' '))

    writer.close()


def read_input():
    """
    reads all lines in the input file
    :return: list of lines (stripped)
    """
    reader = open(args.input, "r")
    ret_list = []
    for line in reader:
        ret_list.append(line)
    reader.close()
    return ret_list


def main():
    all_lines = read_input()
    for i in range(1, len(all_lines) + 1):
        subsets = generate_subsets(all_lines, i)
        counter = 1
        for subset in subsets:
            create_comb_file(subset, args.query, counter)
            counter += 1


main()