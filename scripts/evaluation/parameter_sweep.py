#!/share/apps/python/bin/python

import os
import argparse
import copy
import numpy as np
import re


#dates - r-precision (avg prec)
#locs - p@1-0 P@20
#rel from wiki
#expanded rel

#<q#><rel docs><params><rl date><rel locs>

def main():
    p = argparse.ArgumentParser('will run to find the optimal parameter values for a set of queries')
    p.add_argument('-r', '--results', help='dir which contains a results dir for each query or a list of results dir', required=True)
    p.add_argument('-s', '--seeds', help='maximum number of seed documents to consider', required=True)
    p.add_argument('-e', '--entity', help='entity type', required=True)
    p.add_argument('-rt', '--restrict', help='retrict edges to specified combinaton', default='all')
    p.add_argument('-o', '--output', help='file to write parameters to', default='optimal_parameters.csv')
    args = p.parse_args()

    seeds = int(args.seeds)
    entity_type = args.entity
    if args.restrict == 'entities':
        desired_edges = set(['REFERENCES:SIMDATES','REFERENCES:SIMDATES:SIMLOC','REFERENCES:SIMLOC','REFERENCES:SIMDATES:SIMLOC:SIMPAGE', 'REFERENCES:SIMLOC:SIMPAGE','REFERENCES:SIMDATES:SIMPAGE'])
    else:
        desired_edges = set([args.restrict])
    rel_file = 'book_collection/entities-binary.rel'

    #get counts of relevant results for each query
    relevant_counts = dict(location=dict(),date=dict())
    reader = open(rel_file)
    for line in reader:
        qid = line.split(' ')[0]
        if line.strip().split(' ')[3] == '1':
            if line.strip().split(' ')[2].isdigit():
                if qid in relevant_counts['date']:
                    relevant_counts['date'][qid] += 1
                else:
                    relevant_counts['date'][qid] = 1
            else:
                if qid in relevant_counts['location']:
                    relevant_counts['location'][qid] += 1
                else:
                    relevant_counts['location'][qid] = 1
    reader.close()

    #open top directory and count the number of directories
    query_scores = dict()
    query_run_data = dict()

    #determine if this is a directory or a list and process accordingly.
    if os.path.isdir(args.results):
        for this_item in os.listdir(args.results):
            if os.path.isdir(os.path.abspath(args.results) +'/' + this_item):
                q = re.findall(r'\d+', this_item)[-1]
                data = process_stats(os.path.abspath(os.path.abspath(args.results) +'/' + this_item + '/statistics.csv'),q,desired_edges)
                query_scores[q] = data[0]
                query_run_data[q] = data[1] 
                #query_scores[this_item[-1]] = process_stats(os.path.abspath(os.path.abspath(args.results) +'/' + this_item + '/statistics.csv'))
    else:
        reader = open(args.results,'r')
        for line in reader:
            if os.path.isdir(line.strip()):
                q = re.findall(r'\d+', line.strip())[-1]
                data = process_stats(line.strip() + '/statistics.csv',q,desired_edges)
                query_scores[q] = data[0]
                query_run_data[q] = data[1]       
                #query_scores[line.strip()[-1]] = data[0]
                #query_run_data[line.strip()[-1]] = data[1]
                #query_scores[line.strip()[-1]] = process_stats(line.strip() + '/statistics.csv')
                

    output = []
    output_avg = []
    avg_performance = dict()
    for seeds in range(1,seeds + 1):
        avg_performance[seeds] = [0.0,0.0,0.0,0.0,0.0]
        #print '# seeds: ' + str(seeds)
        #for each q_i in Q
        #be sure the test set only uses queries which used the required number of seeds
        #as input
        test_set = set()
        for qid in query_scores:
            if seeds in query_scores[qid]:
                test_set.add(qid)
                #print qid
        for qid in test_set:
            #pick params using Q - Q_i
            train_set = test_set.copy()
            train_set.remove(qid)
            if not len(train_set) == 0:
                #print 'train set contains: '
                #for q in train_set:
                    #print q

                #which means find the param settings with the best average preformance over Q - Q_i
                opt_param = dict(code=set(),score=0)
                for param_code in query_scores[qid][seeds]:
                    score_sum = 0
                    #calculate p@10 for each parameter setting
                    for train_qid in train_set:
                        score_sum += query_scores[train_qid][seeds][param_code]['p_ten']
                    avg_score = score_sum / len(train_set)
                    if opt_param['score'] < avg_score:
                        opt_param['code'] = [param_code]
                        opt_param['score'] = avg_score
                    elif opt_param['score'] == avg_score:
                        opt_param['code'].append(param_code)
                
                #select the optimal parameter based on highest thresholds
                best_param = dict(code='',th_sum=0)
                for param_code in opt_param['code']:
                    thresholds = param_code.split('-')
                    th_sum = float(thresholds[1]) + float(thresholds[2]) + float(thresholds[3])
                    if th_sum > best_param['th_sum']:
                        best_param['th_sum'] = th_sum
                        best_param['code'] = param_code

                #best_params = dict(doc_th=dict(code='',value=0),date_th=dict(code='',value=0),loc_th=dict(code='',value=0))
                #for param_code in opt_param['code']:
                    #thresholds = param_code.split('-')
                    #print(param_code)
                    #if float(thresholds[1]) > best_params['doc_th']['value']:
                        #best_params['doc_th']['code'] = param_code
                        #best_params['doc_th']['value'] = float(thresholds[1])
                    #if float(thresholds[2]) > best_params['doc_th']['value']:
                        #best_params['date_th']['code'] = param_code
                        #best_params['date_th']['value'] = float(thresholds[2])
                    #if float(thresholds[3]) > best_params['doc_th']['value']:
                        #best_params['loc_th']['code'] = param_code
                        #best_params['loc_th']['value'] = float(thresholds[3])

                # if there is an optimal parameter score which maximizes doc_th and
                # date_th chose it a the best option
                # If there isn't, then choose one of the other two parameters
                #if best_params['doc_th']['code'] == best_params['date_th']['code']:
                        #max_threshold_opt_param = best_params['doc_th']['code']
                #else:
                        #best_param['code'] = best_params['date_th']['code']

                #fix r-precision for any instances in which the rel count > 10
                if relevant_counts[entity_type][qid] > 10:
                    r_prec = '-'
                else:
                    r_prec = str(query_scores[qid][seeds][best_param['code']]['r_prec'])

                ap = query_scores[qid][seeds][best_param['code']]['ap']
                p_five = query_scores[qid][seeds][best_param['code']]['p_five']
                p_ten = query_scores[qid][seeds][best_param['code']]['p_ten']
                act_nodes = query_run_data[qid][seeds][best_param['code']][0]
                run_time = query_run_data[qid][seeds][best_param['code']][1]
                output.append('%d,%s,%s,%d,%f,%f,%f,%f,%s,%d,%f,%f' % (seeds,qid,best_param['code'].replace('-',','),len(train_set),opt_param['score'],ap,p_five,p_ten,r_prec,relevant_counts[entity_type][qid],act_nodes,run_time))
                avg_performance[seeds][0] += query_scores[qid][seeds][best_param['code']]['p_ten']
                avg_performance[seeds][1] += query_scores[qid][seeds][best_param['code']]['ap']
                #avg_performance[seeds][2] += query_scores[qid][seeds][best_param['code']]['r_prec']
                avg_performance[seeds][3] += query_run_data[qid][seeds][best_param['code']][0]
                avg_performance[seeds][4] += query_run_data[qid][seeds][best_param['code']][1]

        output_avg.append('%d,%f,%f,%f,%f' % (seeds,avg_performance[seeds][0]/len(test_set),avg_performance[seeds][1]/len(test_set),avg_performance[seeds][3]/len(test_set),avg_performance[seeds][4]/len(test_set)))
        #output_avg.append('%d,%f,%f,%f,%f,%f' % (seeds,avg_performance[seeds][0]/len(test_set),avg_performance[seeds][1]/len(test_set),avg_performance[seeds][2]/len(test_set),avg_performance[seeds][3]/len(test_set),avg_performance[seeds][4]/len(test_set)))

    writer = open(args.output,'w')
    writer.write('seeds,qid,relationships,doc_th,date_th,loc_th,training count,train p@10,test ap,test p@5,test p@10,test r-prec,# relevant entities,final number of activated nodes,running time\n')
    for line in output:
        writer.write(line + '\n')
    writer.write('\nseeds,mean test p@10,mean test ap,mean final count activated nodes, mean running time\n')
    #writer.write('\nseeds,mean test p@10,mean test ap,mean test r-prec,mean final count activated nodes, mean running time\n')
    for line in output_avg:
        writer.write(line + '\n')   
    writer.close()
        

#a stats file contains the scores for all possible parameter settings for
#a single query
def process_stats(stats_file,q,desired_edges):
    
    #structure:
    #dict(<#seeds><dict(<code><[score1,score2,...]>)>)
    #where a score is "realtionships-doc_th-date_th-loc_th"
    raw_query_x_scores = dict()
    raw_query_x_run_data = dict()

    reader = open(stats_file, 'r')
    for line in reader:
        columns = line.strip().split(',')
        if len(columns) > 1 and columns[0] == 'DOCUMENT:DATE:LOCATION':
            relationships = columns[1]
            doc_th = columns[2]
            date_th = columns[3]
            loc_th = columns[4]
            doc_combo = clolumns[5]
            seeds = int(columns[6])
            act_nodes = float(columns[8])
            time = float(columns[10])
            ap = float(columns[12])
            p_five = float(columns[13])
            p_ten = float(columns[14])
            r_prec = float(columns[15])
            if relationships in desired_edges or 'all' in desired_edges:
                if not seeds in raw_query_x_scores:
                    raw_query_x_scores[seeds] = dict()
                    raw_query_x_run_data[seeds] = dict()
                param_code = '%s-%s-%s-%s' % (relationships,doc_th,date_th,loc_th)
                if not param_code in raw_query_x_scores[seeds]:
                    raw_query_x_scores[seeds][param_code] = dict(ap=[],p_five=[],p_ten=[],r_prec=[])
                    raw_query_x_run_data[seeds][param_code] = [[],[]]
                raw_query_x_scores[seeds][param_code]['ap'].append(ap)
                raw_query_x_scores[seeds][param_code]['p_five'].append(p_five)
                raw_query_x_scores[seeds][param_code]['p_ten'].append(p_ten)
                raw_query_x_scores[seeds][param_code]['r_prec'].append(r_prec)
                raw_query_x_run_data[seeds][param_code][0].append([act_nodes])
                raw_query_x_run_data[seeds][param_code][1].append([time])
    reader.close()

    #structure:
    #dict(<#seeds><dict(<code><avg_score>)>)
    #where a score is "realtionships-doc_th-date_th-loc_th"
    final_query_x_scores = dict()
    final_query_x_run_data = dict()
    for seeds in raw_query_x_scores:
        #print seeds
        final_query_x_scores[seeds] = dict()
        final_query_x_run_data[seeds] = dict()
        for param_code in raw_query_x_scores[seeds]:
            a = np.array(raw_query_x_scores[seeds][param_code]['ap'])
            b = np.array(raw_query_x_scores[seeds][param_code]['p_five'])
            c = np.array(raw_query_x_scores[seeds][param_code]['p_ten'])
            d = np.array(raw_query_x_scores[seeds][param_code]['r_prec'])
            final_query_x_scores[seeds][param_code] = dict(ap=np.mean(a),p_five=np.mean(b),p_ten=np.mean(c),r_prec=np.mean(d))
            a = np.array(raw_query_x_run_data[seeds][param_code][0])
            b = np.array(raw_query_x_run_data[seeds][param_code][1])
            final_query_x_run_data[seeds][param_code] = [np.mean(a),np.mean(b)]

    return [final_query_x_scores,final_query_x_run_data]

main()
