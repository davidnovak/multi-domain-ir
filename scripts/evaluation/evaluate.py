#!/share/apps/python/bin/python

import sys, os
import re
import shlex
from subprocess import call, Popen, PIPE
import argparse


default_ks = ['10']
default_judged = 'book_collection/binary.rel'
#default_baseline = 'book_collection/sample_queries-ql.res'
stat_file = 'statistics.csv'


p = argparse.ArgumentParser('will create file <dir_with_res_files>/' + stat_file)
p.add_argument('-r', '--results', help='top dir with results', required=True)
p.add_argument('-j', '--judgements', help='file with judged documents, default: ' + default_judged, default=default_judged)
p.add_argument('-k', '--top_ks', help='the list of top-k values to be used, default: ' + str(default_ks), default=default_ks, nargs='*')
p.add_argument('-s', '--seeds', help='remove the seed documents - the top "k" results where "k" is taken from the file '
                                     + 'name, e.g. rel3-1 would remove top-3 results', action='store_true')
#p.add_argument('-b', '--baseline', help='baseline file, default: ' + default_baseline, default=default_baseline)
args = p.parse_args()


#galago_bin='/home/wem/lemur-galago/core/target/appassembler/bin/galago'
galago_bin = './lemur-galago/core/target/appassembler/bin/galago'


parameters = ('nodestouse', 'relationships', 'doc_th', 'date_th', 'loc_th')
measures = ('number of spread. activ. pulses', 'final number of activated nodes', 'final overall activation', 'running time')
specials = ('iter', 'seeds')

params = 'FAKE'

def find_stat(line, statistics, stat_values):
    for stat in statistics:
        match = re.search('(?<=^#\s\s\s'+stat+': ).*', line)
        if match is not None:
            stat_values[stat] = match.group(0)


def find_specials(line, spec_values):
    m = re.search('^[0-9]+\s*(rel([0-9]*)[^\s]*).*', line)
    if m:
        spec_values['iter'] = m.group(1)
        spec_values['seeds'] = m.group(2)


def open_stat_file(directory):
    """
    creates and opens an output file to print statistics to
    :param directory: directory to crate the file in
    :return: a pair  (output_file_name, opened_file_descriptor)
    """
    filename = directory + '/' + stat_file
    print >> sys.stderr, 'creating file ' + filename
    return filename, open(filename, 'w')


def parse_stats(file):
    """
    parse the SA output file for selected statistics
    :param file: SA output file to open
    :return: the pair, where 1st component is dictionary with parameters+values and the 2nd is dictionary with statistics (measures)
    :rtype: tuple
    """
    param_values = {}
    measure_values = {}
    spec_values = {}

    infile = open(file, 'r')
    for line in infile:
        find_stat(line, parameters, param_values)
        find_stat(line, measures, measure_values)
        find_specials(line, spec_values)

    return param_values, measure_values, spec_values


def print_stats(directory, lines):
    """
    prints the output statistics for given director
    :param lines:
    :return: the name of the created output file
    """
    # open the output file
    stat_file_name, stat_output = open_stat_file(directory)

    # first, print statistics as CSV header
    for param in parameters + specials + measures:
        stat_output.write(param + ',')
    for line in lines:
        filename = re.search('^[^\s]*', line).group(0)
        if os.path.isfile(filename):
            (param_values, measure_values, spec_values) = parse_stats(filename)
            for param in parameters:
                stat_output.write((param_values[param] if param in param_values else '') + ',')
            for param in specials:
                stat_output.write((spec_values[param] if param in spec_values else '') + ',')
            for measure in measures:
                stat_output.write((measure_values[measure] if measure in measure_values else '') + ',')

        stat_output.write(re.sub('\s+', ',', line) + '\n')

    stat_output.close()
    return stat_file_name


def get_seed_number(file_name):
    if not args.seeds:
        return 0
    match = re.match('.*rel([0-9]*).*\.res', file_name)
    if not match or not match.group(1):
        sys.stderr.write('error matching result file name' + file_name + '\n')
        return 0
    return int(match.group(1))


r = re.compile('(^[^\s]*\s+[^\s]*\s+)(([^\s]+ )+[^\s]+)(\s+[^\s]*\s+[^\s]*\s+[^\s]*)$')


def clean_file(resourse_file,rel,qid):
    number = get_seed_number(resourse_file)
    cleaned_file = resourse_file + '.clean'
    reader = open(resourse_file, 'r')
    writer = open(cleaned_file, 'w')

    #track where we are in the top 10 results
    counter = -1 #-1 indicates we haven't seen the result header line
    template = ''

    for line in reader:
        # print all the comments back
        if re.match('^#..*', line):
            if line.strip() == '# top k results in TREC format:' or  line.strip() == '# top k results in TREC format: ':
                counter = 0 #mark that we've seen the result header line
                print 'counter set!'
            writer.write(line)
            continue
        # comment out the first 'k' results because they are the seeds
        if number > 0:
            writer.write('# ' + line)
            number -= 1
            template = line
            if len(line.strip()) == 0 and counter >= 0:
                counter += 1
        #if the line is not a comment or a top 'k' result
        else:
            print line.strip()
            #generate dummy documents based on template line
            if counter >= 1 and counter < 11 and len(line.strip()) == 0:
                print 'found blank line!'
                if template == '':
                    print 'template empty!'
                    while counter < 11:
                        to_write = '%s   %s   DUMMY_DOCUMENT_%d   %d   %f   %s\n' % (qid,rel,counter,counter,0.1/counter,params)
                        writer.write(to_write)
                        counter += 1
                        print 'counter at ' + str(counter) + '!'
                else:
                    while counter < 11:
                        prefix = re.findall('\d+\s+rel\d+-\d+\s+',template)[0]
                        prefix += 'DUMMY_DOCUMENT'
                        to_write = re.sub(' \d+ ',' %d ' % (counter),template)
                        to_write = re.sub('\d+\s+rel\d+-\d+\s+(\w+\s?)+','',to_write)
                        to_write = prefix + to_write
                        writer.write(to_write)
                        counter += 1
                        print 'counter at ' + str(counter) + '!'
            if len(line.strip()) == 0 and counter == 0:
                counter = 1
                print 'counter at 1!'

            # if the output node ID is composed of more than one words, put underscores there
            # 8   rel1-1   north america   10   0.10058327   nodesDOCUMENT:DATE:LOCATION-edgesREFERENCES-date_th0.2-doc_th0.7-loc_th0.4
            m = r.match(line)
            if m:
                writer.write(m.group(1))
                writer.write(m.group(2).replace(" ", "_"))
                writer.write(m.group(4))
                writer.write('\n')
            else:
                writer.write(line)
            #assume any line that is not empty or a comment can serve as a template
            if not len(line.strip()) == 0:
                template = line
                #counter = int(re.findall('\s+\d+\s+',template)[0].strip()) + 1
                counter += 1
                print 'counter at ' + str(counter) + '!'
    writer.close()
    return cleaned_file


def evaluate_dir(directory):
    """
    runs the evaluation on all .res files in the directory
    from each of the files, the RF seeds are first removed
    :return: file name of the created statistic file or None if not created
    """
    cmd = galago_bin + ' eval --metrics+map'
    for k in args.top_ks:
        cmd += ' --metrics+P' + k
    cmd += ' --metrics+R-prec'
    cmd += ' --judgments=' + args.judgements
    #cmd += ' --baseline=' + args.baseline
    #cmd += ' --runs+' + args.baseline + ' '

    hasresfiles = False
    for f in os.listdir(directory):
        if f.endswith('.res'):
            rel =  f.split('.')[0]
            qid = re.findall(r'\d+', directory)[-1]
            cmd += ' --runs+%s' % clean_file(directory + '/' + f,rel,qid)
            hasresfiles = True
    if not hasresfiles:
        return None

    print >> sys.stderr, 'running Galago eval command'
    print >> sys.stderr, cmd

    gproc = Popen(shlex.split(cmd), stdout=PIPE)
    gout = gproc.communicate()[0].decode('UTF-8')
    lines = gout.strip().split("\n")
    return print_stats(directory, lines)


def call_main(results, judgements=args.judgements, top_ks=args.top_ks, seeds=args.seeds):
    args.results = results
    args.judgements = judgements
    args.top_ks = top_ks
    args.seeds = seeds
    main()


def main():
    if evaluate_dir(args.results) is not None:
        return
    # else run the evaluation on all subdirectories
    statfiles = []
    for file in os.listdir(args.results):
        if os.path.isdir('%s/%s' % (args.results, file)):
            out_file = evaluate_dir('%s/%s' % (args.results, file))
            if out_file is not None:
                statfiles.append(out_file)

    # ...and make an overall statistic file
    _, stat_output = open_stat_file(args.results)
    for file in statfiles:
        with open(file,'r') as fi: stat_output.write(fi.read())

    stat_output.close()


main()
