import sys
import itertools


#for the <query id>, generate the appropriate command for a relevant document set of size <doc_count>
#./scripts/evaluation/run-sa.py -i book_collection/initial/query1/query1-rel1-1.csv -v > logs/query1.log 2>&1 &
def main():

    query_id = sys.argv[1]
    doc_count = int(sys.argv[2])

    addresses = range(0,doc_count)

    output = './scripts/evaluation/run-sa.py -i'
    for i in range(1,doc_count+1):
        counter = 1
        for c in itertools.combinations(addresses,i):
            output += ' book_collection/initial/query' + query_id + '/query' + query_id
            output += '-rel' + str(i) + '-' + str(counter) + '.csv'
            counter += 1
        
    output += ' -v > logs/query'
    output += query_id
    output += '.log 2>&1 &'
    print(output)

main()
