import sys, sqlite3
import math

#takes in a list of locations and a location - coordinate dictionary
#produces a new location - coordinate dictionary
def main():

    file_list = sys.argv[1]
    location_db = sys.argv[2]
    if len(sys.argv) > 3:
        suffix = sys.argv[3]
    else:
        suffix = ''

    print('CONNECTING TO DATABASE')
    conn = sqlite3.connect(location_db)
    #conn = sqlite3.connect(location_db,':memory:')
    print('CONNECTED')
    cursor = conn.execute('''select name from sqlite_master where type = 'table'; ''') 
    tables = cursor.fetchall()
    for t in tables:
        print(t)

    #locations is a dict storing <location, list> pairs, which in turn store <lat, lng> pairs
    locations = dict()
    doc_loc = dict()
    output = []

    print('PROCESSING LOCATIONS')
    if len(file_list.split('.')) > 1 and file_list.split('.')[1] == 'csv':
        process(conn,file_list,locations,doc_loc)

    else:
        reader = open(file_list, 'r')
        for locations_file in reader:
            #print('READING FILE: ' + locations_file)
            locations = process(conn,'/mnt/nfs/work3/wem/geo-temporal_retrieval/Proteus/homer/entity-records-wrong/location/'+locations_file.strip(),locations,doc_loc)
        reader.close()

    print('BUILDING OUTPUT')
    for location in locations:
        output.append(location+'\t' +str(locations[location][0]) + '\t' + str(locations[location][1]))

    writer = open('locations-'+suffix,'w')
    for line in output:
        writer.write(line + '\n')
    writer.close()

    writer = open('doc-loc-'+suffix,'w')
    for document in doc_loc:
        total = 0
        for location in doc_loc[document]:
            total += doc_loc[document][location]
        for location in doc_loc[document]:
            val = math.log(doc_loc[document][location]+1)/math.log(total+1)
            writer.write(str(location) + '\t' + str(val) + '\t' + document + '\n')
    writer.close()

def process(conn,locations_file,locations,doc_loc):

    reader = open(locations_file, 'r')

    for line in reader:
        location = line.split(',')[0]
        document = line.split(',')[1].strip()
        cursor = conn.execute('''SELECT * FROM main.primary_names WHERE name = '%s' LIMIT 1''' % (location))
        geocode_results = cursor.fetchall()
        if not len(geocode_results) == 0:
            geocode_result = geocode_results[0]
            locations[location] = [geocode_result[1],geocode_result[2]]
            if not document in doc_loc:
                doc_loc[document] = dict()
            if location in doc_loc[document]:
                doc_loc[document][location] = doc_loc[document][location] + 1
            else:
                doc_loc[document][location] = 1
        else:
            cursor = conn.execute('''SELECT * FROM main.alternate_names WHERE name = '%s' LIMIT 1''' % (location))
            geocode_results = cursor.fetchall()
            if not len(geocode_results) == 0:
                geocode_result = geocode_results[0]
                location = geocode_result[3]
                locations[location] = [geocode_result[1],geocode_result[2]]
                if not document in doc_loc:
                    doc_loc[document] = dict()
                if location in doc_loc[document]:
                    doc_loc[document][location] = doc_loc[document][location] + 1
                else:
                    doc_loc[document][location] = 1
    reader.close()
    
    return locations

main()
