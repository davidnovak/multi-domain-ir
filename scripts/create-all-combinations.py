import sys
import os
import argparse

#given a results file and a qrel file, generate all combinations

def main():

    p = argparse.ArgumentParser('will run to find the optimal parameter values for a set of queries')
    p.add_argument('-f', '--results', help='file which contains results', required=True)
    p.add_argument('-q', '--qrel', help='query relevance file', required=True)
    p.add_argument('-o', '--output', help='root output directory', required=True)
    p.add_argument('-i', '--ids', help='the range of ids to use. Seperate ids using commas, or use - to specify a range', required=True)
    args = p.parse_args()

    results_file = args.results
    qrel_file = args.qrel
    output_root = args.output
    id_range = args.ids #ids numbers to regenerate

    #results_file = sys.argv[1]
    #qrel_file = sys.argv[2]
    #output_root = sys.argv[3]
    #id_range = sys.argv[4] #ids numbers to regenerate

    reader = open(qrel_file, 'r')
    relevant_docs = dict() # query-id - [doc_1,doc_2,...]
    unjudged = dict() # query-id - [doc_1,doc_2,...]
    for line in reader:
        query_id = line.split(',')[0]
        doc_id = line.split(',')[2]
        #record the unjudged documents for each query
        if len(line.strip().split(',')[4]) == 0:
            if not query_id in unjudged:
                unjudged[query_id] = []
            unjudged[query_id].append(doc_id)
        #record the relevant documents for each query
        elif line.strip().split(',')[4] == '1':
            if not query_id in relevant_docs:
                relevant_docs[query_id] = []
            #print('ADDED TO RELEVANT LIST: %s %s' % (query_id,doc_id))
            relevant_docs[query_id].append(doc_id)
    reader.close()

    reader = open(results_file, 'r')
    top_ten_relevant = dict() # query-id - [doc_1,doc_2,...]
    for line in reader:
        query_id = line.split(' ')[0]
        doc_id = line.split(' ')[2]
        rank = int(line.split(' ')[3])
        if query_id in relevant_docs and doc_id in relevant_docs[query_id]:
            if not query_id in top_ten_relevant:
                top_ten_relevant[query_id] = []
            #print('ADDED TO TOP TEN RELEVANT LIST: %s %s' % (query_id,doc_id))
            if rank <= 10:
                top_ten_relevant[query_id].append(doc_id)
        #print the unjudged documents that occur in the top ten results
        if query_id in unjudged and doc_id in unjudged[query_id]:
            print(query_id + ' ' + doc_id)
    reader.close()

    #generate the requested set of ids
    requested_ids = set()
    sections = id_range.split(',')
    for section in sections:
        if '-' in section:
            lower = int(section.split('-')[0])
            upper = int(section.split('-')[1])
            r = range(lower,upper+1)
            for i in r:
                requested_ids.add(str(i))
        else:
            requested_ids.add(section)

    #write out the top ten relevant files
    script_output = []
    for query_id in top_ten_relevant:
        if query_id in requested_ids:
            os.system("rm %squery%s/*"% (output_root,query_id))
            writer = open('%squery%s/query%s-rel-top10.txt' % (output_root,query_id,query_id),'w')
            for doc_id in top_ten_relevant[query_id]:
                writer.write(doc_id + ' 1\n')
            writer.close()
            script_output.append('./scripts/evaluation/all-combinations.py -i book_collection/initial/query%s/query%s-rel-top10.txt -q %s -o book_collection/initial/query%s/\n' % (query_id,query_id,query_id,query_id))
        
    
    writer = open('run-all-combinations.sh','w')
    for line in script_output:
        writer.write(line)
    writer.close()

main()
