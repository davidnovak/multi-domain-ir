import sys

def main():

    books_file = sys.argv[1]
    locations_file = sys.argv[2]
    

    reader = open(locations_file, 'r')
    locations = dict()
    for line in reader:
        book = line.split('/')[9]
        locations[book] = line
    reader.close()

    reader = open(books_file, 'r')
    output = []
    for line in reader:
        output.append(locations[line.strip()])
    reader.close()
    
    writer = open('output','w')
    for line in output:
        writer.write(line)
    writer.close()

main()
