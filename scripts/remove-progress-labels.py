#!/usr/bin/python

import dis
import sys
import math
import shlex
from subprocess import call, Popen, PIPE
import py2neo
from py2neo import Graph
from py2neo.cypher import RecordList
from py2neo.cypher import Record
import argparse
import numpy as np
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description="will remove all node labels and properties with given prefix")
parser.add_argument("-c", "--connection", help="use a different than default http://localhost:7474/db/data")
parser.add_argument("prefix", action='store', help="prefix of labels and properties of nodes to be removed")
args = parser.parse_args()

graph = Graph(args.connection) if args.connection else Graph()


def execute_cypher_py2neo(cmd):
    """
    Runs given Cypher command on the Neo4j connection
    :rtype: RecordList
    :param cmd:
    :return: list of records that are output of the
    """
    records = graph.cypher.execute(cmd)
    #for record in records:
    #    print(record)
    return records


def remove_labels(cqlpattern, prefix):
    label = prefix
    result = execute_cypher_py2neo(cqlpattern % (label, label))
    pulse = 0
    while result.one > 0:
        label = prefix + '_pulse_' + str(pulse)
        result = execute_cypher_py2neo(cqlpattern % (label, label))
        pulse += 1


cql_remove_label = 'MATCH (d:%s) REMOVE d:%s RETURN count(d)'
cql_remove_props = 'MATCH (d) WHERE d.%s >= 0 REMOVE d.%s RETURN count(d)'
def main():
    remove_labels(cql_remove_label, args.prefix + 'ACTIVATED')
    remove_labels(cql_remove_props, args.prefix + 'activation')


main()