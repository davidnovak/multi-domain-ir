#!/usr/bin/python


import sys
import math
import argparse
import re


parser = argparse.ArgumentParser('reads in a MESSIF k-NN output file and transforms the results to loc-loc.csv or doc-doc.csv')
parser.add_argument('messif_output')
parser.add_argument('-o', '--output', metavar="out_file", help='if not used, print to stdout')
parser.add_argument('-d', '--documents', action='store_true', help='if used, the input is considered doc-doc k-NN; otherwise it is location output')
parser.add_argument('-l', '--limit', help='limit the actual number of neighbors [default: all in the file]', default=1000000)
parser.add_argument('-m', '--minimum', help='minimum number of neighbors to print even if over limit [default: no min limit]', default=0)
parser.add_argument('-r', '--radius', help='limit the query-object distance [default: no limit]', default=1000000)
args = parser.parse_args()

LIMIT_NEIGHBORS = int(args.limit)
LIMIT_RADIUS = float(args.radius)
results_file = args.messif_output


def transform_loc_dist(score):
    return math.exp(-(math.fabs( score / float(50000) )))


def transform_doc_dist(score):
    return 1 - score


def main():
    reader = open(results_file, 'r')
    #re.compile('[0-9]
    objects = []
    distances = dict()
    overallcount = 0
    for line in reader:
        firsttext = line.split(' ')[0]
        if not firsttext == 'ApproxRangeQueryOperation' and not firsttext == 'ApproxKNNQueryOperation':
            segments = line.split(', ')
            counter = 0
            before_a_comma = None
            for segment in segments:
                if before_a_comma:
                    segment = before_a_comma + ', ' + segment
                id_dist = segment.split(':')
                if len(id_dist) != 2:
                    before_a_comma = segment
                    #print >> sys.stderr, segment
                    continue
                if counter == 0:
                    start_name = id_dist[0]
                    counter += 1
                    continue
                score = float(id_dist[1].replace(' ','').replace(',',''))
                if score > LIMIT_RADIUS and counter >= int(args.minimum):
                    break

                objects.append([start_name,score,id_dist[0]])
                counter += 1
                if counter >= LIMIT_NEIGHBORS:
                    break
                overallcount += 1
                before_a_comma = None
    reader.close()
    print >> sys.stderr, "relevant pairs in the input: " + str(overallcount)

    for object in objects:
        la = object[0]
        lb = object[2]
        score = object[1]
        if not (((lb+'-'+la) in distances) or ((la+'-'+lb) in distances)):
            if not la == lb:
                distances[la+'-'+lb] = transform_doc_dist(score) if args.documents else transform_loc_dist(score)

    writer = open(args.output, 'w') if args.output else sys.stdout
    if args.documents:
        writer.write('docID:ID\tsim:float\tdocID:ID\n')
    else:
        writer.write('locID:ID\tsim:float\tlocID:ID\n')

    for e in distances:
        if not (distances[e] == 0):
            writer.write(e.split('-')[0] + '\t' + str(distances[e]) + '\t' + e.split('-')[1] + '\n')
        #writer.write(e.split('-')[0] + ',' + str(distances[e]) + ',' + e.split('-')[1] + '\n')

    if args.output:
        writer.close()


main()
