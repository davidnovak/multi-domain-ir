#!/usr/bin/python


import numpy as np
import argparse

parser = argparse.ArgumentParser('generate random projection pivots for sparse vectors')
parser.add_argument('termid_number', help='dimensions taken from interval [0, termid_number)')
parser.add_argument('dimensionality', help='number of random projections to generate')
parser.add_argument('pivot_number', help='number of pivots to generate')
args = parser.parse_args()

termid_number = int(args.termid_number)
value = np.sqrt(float(1) / float(args.dimensionality))
for i in range(0, int(args.pivot_number)):
    pivotstr = ''
    for d in range(0, int(args.dimensionality)):
        pivotstr += str(np.random.choice(termid_number)) + " " + ('%.6f' % value) + " "
    print pivotstr