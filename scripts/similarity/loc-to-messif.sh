#!/bin/bash

echo "THIS FILE IS NOT NECESSARY - THE KEY CAN BE STORED IN THE FIRST COLUMN OF THE FILE"

FILE=locations.csv
if [ $# -gt 1 ]; then
	FILE=$1
fi
OUT=${FILE%%.csv}.data

echo "transforming file $FILE into $OUT"

tail -n +2 $FILE | sed $'s/^\([^,]*\),/#objectKey messif.objects.keys.AbstractObjectKey \\1\\\n/' > $OUT

