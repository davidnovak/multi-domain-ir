#!/share/apps/python/bin/python

import sys
import math


if len(sys.argv) < 2:
    print 'Usage: ' + sys.argv[0] + ' <file-with-dates> [<output-file:date-date.tsv>]'
    print '\ttakes a CSV file with dates in the first column and generates date-date.tsv'
    exit(1)


def main():

    results_file = sys.argv[1]

    reader = open(results_file, 'r')
    dates = []
    distances = dict()
    for line in reader:
        date = line.strip().split('\t')[0]
        if date.isdigit():
            dates.append(date)
    reader.close()

    dates=set(dates)
    for da in dates:
        for db in dates:
            if not (((db+'-'+da) in distances) or ((da+'-'+db) in distances)):
                if not da == db:
                    #distances[da+'-'+db] = math.exp(-(math.fabs( int(da) - int(db) )))
                    distances[da+'-'+db] = 1.0 if math.fabs( int(da) - int(db) ) == 1 else 0.0
		    

    outfilename = 'date-date.tsv'
    if len(sys.argv) > 2:
        outfilename = sys.argv[2]
    writer = open(outfilename,'w')
    writer.write('dateID:ID\tsim:float\tdateID:ID\n')
    for e in distances:
        if not (distances[e] < 0.001):
            writer.write(e.split('-')[0] + '\t' + str(distances[e]) + '\t' + e.split('-')[1] + '\n')
    writer.close()

main()
