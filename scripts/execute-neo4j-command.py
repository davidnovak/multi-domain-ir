#!/share/apps/python/bin/python

import sys
from py2neo import Graph
from py2neo.cypher import RecordList
from py2neo.cypher import Record
import argparse
import py2neo


parser = argparse.ArgumentParser(description="runs given command on running Neo4j database")
parser.add_argument("-d", "--database", help="use a different than default http://localhost:7474/db/data")
parser.add_argument("-c", "--command", help="command to be executed, possibly with %s string", required=True)
parser.add_argument("-f", "--file", help="if the command contains %s, lines for this file are used: cmd % line")
args = parser.parse_args()

graph = Graph(args.database) if args.database else Graph()


def execute_cypher_py2neo(cmd):
    """
    Runs given Cypher command on the Neo4j connection
    :param cmd:
    :return: RecordList
    """
    sys.stderr.write("executing: " + cmd + "\n")
    try:
        records = graph.cypher.execute(cmd)
        for outline in records:
            sys.stderr.write('\t' + str(outline) + '\n')
    except py2neo.cypher.error.statement.EntityNotFound as e:
        sys.stderr.write(str(e))


def main():
    if args.file:
        sys.stderr.write("reading file: " + args.file + "\n")
        reader = open(args.file, "r")
        for line in reader:
            execute_cypher_py2neo(args.command % line.strip())
    else:
        execute_cypher_py2neo(args.command)

main()