#!/usr/bin/python

import sys, os
import re
import shlex
from subprocess import call, Popen, PIPE

galago_bin='/home/wem/lemur-galago/core/target/appassembler/bin/galago'

def main():
    cmd = galago_bin + ' doc '
    cmd += sys.argv[1]
    cmd += ' '
    cmd += sys.argv[2]

    gproc = Popen(shlex.split(cmd), stdout=PIPE)
    gout = gproc.communicate()[0].decode('UTF-8')
    lines = gout.strip().split("\n")
    for line in lines:
        output = line.encode('iso8859_15', errors='replace').decode('iso8859_15').replace('<br>','\n')
        output = output.replace('<PERSON>','').replace('</PERSON>','')
        output = output.replace('<ORGANIZATION>','').replace('</ORGANIZATION>','')
        output = output.replace('<LOCATION>','').replace('</LOCATION>','')
        output = output.replace('<DATE>','').replace('</DATE>','')
        print(output)
        #print(line.replace('<br>','\n'))

main()
