import sys

def main():

    results_file = sys.argv[1]

    reader = open(results_file, 'r')
    results = dict() 
    count = 0
    track = False
    for line in reader:
        if track == True:
            name = line.split(',')[0]
            if name in results:
                results[name] += 1
            else:
                results[name] = 1
            count += 1
            if count == 50:
                track = False
        if line == 'top documents: \n':
            count = 0
            track = True
    reader.close()
    
    writer = open('combined_results.csv','w')
    for name in results:
        writer.write(name + ',' + str(results[name]) + '\n')
    writer.close()

main()
