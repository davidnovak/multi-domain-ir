import sys, sqlite3
import math

#given a list of relevant entities and a query id, creates a properly formatted qrel file
#where all locations have been converted to the primary name in the database or removed
def main():

    entity_list = sys.argv[1]
    location_db = sys.argv[2]
    qid = sys.argv[3]

    print('CONNECTING TO DATABASE')
    conn = sqlite3.connect(location_db)
    #conn = sqlite3.connect(location_db,':memory:')
    print('CONNECTED')
    cursor = conn.execute('''select name from sqlite_master where type = 'table'; ''') 
    tables = cursor.fetchall()
    for t in tables:
        print(t)

    #locations is a dict storing <location, list> pairs, which in turn store <lat, lng> pairs
    locations = set()
    dates = set()

    #seperate dates and locations
    print('PROCESSING ENTITIES')
    reader = open(entity_list, 'r')
    for line in reader:
        #entity = line.strip().split(' ')[2].replace('_',' ')
        entity = line.strip()
        if entity.isdigit():
            dates.add(entity)
        else:
            locations.add(entity.lower())
    reader.close()

    print('PROCESSING ENTITIES')
    locations = process(conn,locations)

    writer = open('query%s-entities.qrel'%(qid),'w')
    for date in dates:
        writer.write('%s Q0 %s 1\n'%(qid,date))
    for location in locations:
        writer.write('%s Q0 %s 1\n'%(qid,location))
    writer.close()

def process(conn,locations):
    final_locations = set()

    print('MISSING ENTITIES')

    for location in locations:
        cursor = conn.execute('''SELECT * FROM main.primary_names WHERE name = '%s' LIMIT 1''' % (location))
        results = cursor.fetchall()
        if not len(results) == 0:
            final_locations.add(location.replace(' ','_'))
        else:
            cursor = conn.execute('''SELECT * FROM main.alternate_names WHERE name = '%s' LIMIT 1''' % (location))
            results = cursor.fetchall()
            if not len(results) == 0:
                result = results[0]
                primary_name = result[3]
                final_locations.add(primary_name.replace(' ','_'))
            else:
                print(location)

    print('')
    return final_locations

main()
