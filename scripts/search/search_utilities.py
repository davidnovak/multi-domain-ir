import math
import re
import sqlite3
from geopy.distance import great_circle
from py2neo import Graph
from decimal import *

#ensure that all documents to be found have dates
#also ensure they actually exist in the graph
def verify_set(limit: str, entity: str) -> set:
    target_documents = set()
    this_graph = Graph('http://compute-1-9:7474/db/data/')
    entities = set()
    if entity == 'all':
        entities.add('location')
        entities.add('date')
    else:
        entities.add(entity)

    for line in open(limit,'r'):
        doc = line.strip().split()[2]
        #print(doc)
        doc_id = ''
        for record in this_graph.cypher.execute("MATCH(d:DOCUMENT{docID:'"+ doc +"'})  RETURN ID(d) as id"):
            doc_id = record.id
        if not doc_id == '':
            cmds = []
            if 'date' in entities:
                cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(y)
WHERE y:DATE
return y.dateID as dateID;''' % (doc_id)
                cmds.append(cmd)
            if 'location' in entities:
                cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(l)
WHERE l:LOCATION
return l.locID as locID;''' % (doc_id)
                cmds.append(cmd)
            #make sure all target_documents are reachable
            for cmd in cmds:
                if len(this_graph.cypher.execute(cmd)) > 0:
                    target_documents.add(doc)
    return target_documents

#ensure that all documents to be found actually exist in the graph
def verify_input_list(target_documents: [], limit: str, entity: str) -> set:
    this_graph = Graph('http://compute-1-9:7474/db/data/')

    for line in open(limit,'r'):
        doc = line.strip().split()[0]
        #print(doc)
        doc_id = ''
        for record in this_graph.cypher.execute("MATCH(d:DOCUMENT{docID:'"+ doc +"'})  RETURN ID(d) as id"):
            doc_id = record.id
        if not doc_id == '':
            if entity == 'date':
                cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(y)
WHERE y:DATE
return y.dateID as dateID;''' % (doc_id)
            elif entity == 'location':
                cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(l)
WHERE l:LOCATION
return l.locID as locID;''' % (doc_id)
            #make sure all target_documents are reachable
            #if len(this_graph.cypher.execute(cmd)) > 0:
                #target_documents.append(doc)
            target_documents.append(doc)
    return target_documents

def in_graph(doc: str) -> bool:
    doc_id = ''
    this_graph = Graph('http://compute-1-9:7474/db/data/')
    for record in this_graph.cypher.execute("MATCH(d:DOCUMENT{docID:'"+ doc +"'})  RETURN ID(d) as id"):
        doc_id = record.id
    return len(doc_id) > 0

#ensure that all documents to be found have dates
#also ensure they actually exist in the graph
def get_ranking(limit: str) -> dict:
    this_graph = Graph('http://compute-1-9:7474/db/data/')
    ranking_dict = dict()
    results = []
    for line in open(limit,'r'):
        doc = line.strip().split()[2]
        score = float(line.strip().split()[4])
        ranking_dict[doc] = score
        results.append((score,doc))
    return {'results': results, 'ranking_dict': ranking_dict}

def date_distance(a: str, b: str) -> int:
    return abs(int(a) - int(b))

def loc_distance(a: str, b: str) -> float:
    this_graph = Graph('http://compute-1-9:7474/db/data/')
    #add y_j to date_queue
    cmd = '''MATCH(d:LOCATION{locID:"%s"}) RETURN d.loc_long as long, d.loc_lat as lat''' % (a)
    loc_a_coord = 0
    for record in this_graph.cypher.execute(cmd):
        loc_a_coord = (record.lat,record.long)
    cmd = '''MATCH(d:LOCATION{locID:"%s"}) RETURN d.loc_long as long, d.loc_lat as lat''' % (b)
    loc_b_coord = 0
    for record in this_graph.cypher.execute(cmd):
        loc_b_coord = (record.lat,record.long)
    return great_circle(loc_a_coord,loc_b_coord).kilometers

def coord_distance(a_lat: float, a_long: float, b: str) -> float:
    this_graph = Graph('http://compute-1-9:7474/db/data/')
    loc_a_coord = (a_lat,a_long)
    cmd = '''MATCH(d:LOCATION{locID:"%s"}) RETURN d.loc_long as long, d.loc_lat as lat''' % (b)
    loc_b_coord = 0
    for record in this_graph.cypher.execute(cmd):
        loc_b_coord = (record.lat,record.long)
    return great_circle(loc_a_coord,loc_b_coord).kilometers

def similarity(distance: float, weight: float = 1) -> float:
    return math.exp(weight*(0-distance))

def parseLocations(conn: sqlite3.Connection, text: str) -> set():

    locations = set()

    candidates = set()
    segments = text.split()
    for i in range(len(segments)):
        candidates.add(segments[i])
        if not i == len(segments)-1:
            candidates.add(segments[i] +' ' + segments[i+1])

    for candidate in candidates:
        cursor = conn.execute('''SELECT * FROM main.primary_names WHERE name = '%s' LIMIT 1''' % (candidate))
        geocode_results = cursor.fetchall()
        if not len(geocode_results) == 0:
            locations.add(candidate)
        else:
            cursor = conn.execute('''SELECT * FROM main.alternate_names WHERE name = '%s' LIMIT 1''' % (candidate))
            geocode_results = cursor.fetchall()
            if not len(geocode_results) == 0:
                geocode_result = geocode_results[0]
                location = geocode_result[3]
                locations.add(location)

    return locations

def parseDates(text: str) -> set():
    p = re.compile('\d\d\d\d')
    return set(p.findall(text))

#break tie scores in the results by using the original ranking
#if results is empty, returns the documents from the original ranking
#in their original order
def rerank(results: [], original_ranking: dict, limit: int, weight: Decimal, drop_doc: str) -> []:

    new_results = []
    seen_docs = set()

    for result in results:
        seen_docs.add(result[1])
        if result[1] == 'newannualarmylis1874hart_228':
            print('FOUND {} with PS score {:f}'.format('newannualarmylis1874hart_228',result[0]))
        if result[1] in original_ranking['ranking_dict']:
            org_score = pow(2,Decimal(original_ranking['ranking_dict'][result[1]]))
            new_results.append((Decimal(result[0])*Decimal(1-weight)+org_score*weight,result[1]))
        else:
            new_results.append((Decimal(result[0])*Decimal(1-weight)+Decimal(0)*weight,result[1]))

    for result in original_ranking['results']:
        #if result[1] == 'newannualarmylis1874hart_228':
            #print('FOUND {} with QL score {:f}'.format('newannualarmylis1874hart_228',result[0]))
            #if result[1] in seen_docs:
                #print('NOT ADDED, ALREADY SEEN')
            #if result[1] == drop_doc:
                #print('NOT ADDED, IS ON DROP LIST')
            #print('              actual score {:f}'.format(result[0]))
            #print('              weighted score {:f}'.format(result[0]))
        if not (result[1] in seen_docs or result[1] == drop_doc):
            org_score = pow(2,Decimal(result[0]))
            new_results.append((0+org_score*weight,result[1]))

    new_results = sorted(new_results,key=lambda result: result[0], reverse=True)

    return new_results[0:limit]