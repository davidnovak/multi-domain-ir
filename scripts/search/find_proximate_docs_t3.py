import argparse
#import statistics
from py2neo import Graph
import queue
from geopy.distance import great_circle
import search_utilities
import entity
from decimal import *

def main():

    p = argparse.ArgumentParser("Takes as input a document. Returns a list of documents order by proximity to the input document")
    p.add_argument('document', help="input document or list of documents (if list must have an extension)")
    p.add_argument('-l', '--limit', help='limit results to documents from this list')
    p.add_argument('-r', '--requested', type=int, help='number of results requested',default=10)
    p.add_argument('-q', '--qid', help='query id',default=0)
    p.add_argument('-e', '--entity', help='entity type',default='date')
    p.add_argument('-s', '--similarity', help='minimum similarity',default=0)
    p.add_argument('-d', '--direct', help='direct links only',action="store_true")
    p.add_argument('-i', '--input', help='number of input documents to use',default=1)
    p.add_argument('-w', '--weight', type=Decimal, help='original query weight',default=Decimal(0))
    p.add_argument('-c', '--combine', help='use combine approach',action="store_true")
    args = p.parse_args()

    documents = []
    #check if input is a list:
    if len(args.document.split('.')) > 1:
        #for line in open(args.document,'r'):
            #documents.append(line.strip().split()[0])
        search_utilities.verify_input_list(documents, args.document, args.entity)
    else:
        documents.append(args.document)

    if args.limit:
        target_documents = search_utilities.verify_set(args.limit, args.entity)
        original_ranking = search_utilities.get_ranking(args.limit)
    else:
        target_documents = set()

    direct_only = args.direct

    #if we are going to combine scores then
    #we should pull a large number of results and then return
    #the originally requested amount after all processing is done
    if args.combine:
        requested = 1000
        target_documents = set()
    else:
        requested = args.requested

    result_lists = []
    if args.input == 1:
        for document in documents:
            result_lists.append(find_documents(document,requested,target_documents,original_ranking,args.entity,direct_only))
    else:
        result_lists.append(find_documents(documents,requested,target_documents,original_ranking,args.entity,direct_only))

    #rerank, if needed
    if not args.weight == 0:
        for i in range(len(result_lists)):
            result_lists[i] = search_utilities.rerank(result_lists[i], original_ranking, args.requested, args.weight,documents[i])

    d = 1
    for result_list in result_lists:
        c = 1
        for result in result_list:
            if args.qid is None:
                print(str(d) + ' id is None')
            if result[0] is None:
                print(str(d) + ' result[0] is None')
            if result[1] is None:
                print(str(d) + ' result[1] is None')
            print(str(args.qid) + ' Q0 ' + result[1] + ' ' + str(c) + ' ' + str(result[0]) + ' ' + 'proximity')
            c += 1
        print()
        d += 1

def find_documents_entities(start_queue: queue.PriorityQueue, limit: int, target_documents: set, original_ranking: set, entity_type: str, direct_only: int) -> [str]:
    results = []
    examined_docs = set()
    examined_entities = set()
    entity_queue = queue.PriorityQueue()
    dist = 1

    #print(doc)

    this_graph = Graph('http://compute-1-9:7474/db/data/')

    while not (entity_queue.empty() and start_queue.empty()) and len(results) < limit:
        if not start_queue.empty():
            pair = (1,start_queue.get()[1])
        else:
            pair = entity_queue.get()
        #print(pair[1])
        examined_entities.add(pair[1].id)
        #for each d_j with edge to y_i:

        #date_id = ''
        #for record in this_graph.cypher.execute("MATCH(d:DATE{dateID:'"+ pair[1] +"'})  RETURN ID(d) as id"):
            #date_id = record.id
        #cmd = '''START n=node(%s)
#MATCH n-[:REFERENCES]-(d)
#WHERE d:DOCUMENT
#return d.docID as docID;''' % (date_id)

        for record in pair[1].getDocuments():
            #add d_j to results
            if not record.docID in examined_docs:
                examined_docs.add(record.docID)
                if len(target_documents) == 0 or record.docID in target_documents:
                    results.append((0-pair[0],record.docID))
                if len(results) >= limit:
                    break
        #for each y_j with edge to y_i:
        #cmd = '''START n=node(%s)
#MATCH n-[:SIMDATES]-(y)
#WHERE y:DATE
#return y.dateID as dateID;''' % (date_id)

        for record in pair[1].getNeighbors():
            #if y_j not in examined_dates:
            if not record.id in examined_entities:
                #add y_j to date_queue
                if pair[1].type == 'date':
                    sim = search_utilities.similarity(search_utilities.date_distance(pair[1].getValue(),record.dateID))
                elif pair[1].type == 'location':
                    sim = search_utilities.similarity(search_utilities.loc_distance(pair[1].getValue(),record.locID))
                e = entity.GraphEntity(record.id,pair[1].type,this_graph)
                i = pair[0]*sim
                #print(i)
                #if direct_only is set to true, then we will only search for
                #documents directly linked through a shared entity
                #will never add anything to the entity_queue
                if not direct_only:
                    entity_queue.put((i,e))

    results = secondary_sort(results, original_ranking, limit)

    return results

def find_documents(doc: str, limit: int, target_documents: set, original_ranking: set, entity_type: str, direct_only: int) -> [str]:
    results = []
    examined_docs = set()
    examined_entities = set()
    entity_queue = queue.PriorityQueue()

    #print(doc)

    this_graph = Graph('http://compute-1-9:7474/db/data/')
    doc_id = ''
    for record in this_graph.cypher.execute("MATCH(d:DOCUMENT{docID:'"+ doc +"'})  RETURN ID(d) as id"):
        doc_id = record.id

    start_entities = []
    date_entities = []
    location_entities = []
    #print('RUNNING COMMAND:')
    #print('DOCUMENT NAME: %s' % doc)
    #print('DOCUMENT ID: %s' % doc_id)

    if entity_type == 'date' or entity_type == 'all':
        cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(y)
WHERE y:DATE
return id(y) as id;''' % (doc_id)
        #print(cmd)
        #print('using doc {}'.format(doc))
        for record in this_graph.cypher.execute(cmd):
            #similarity between entity and doc = 1
            #since the priority queue takes the element
            #with the lowest score first, we need to use
            #negative similarities
            #start_entities.append((0-1,entity.GraphEntity(record.id,'date',this_graph)))
            #print(record)
            #print(record.id)
            date_entities.append(entity.GraphEntity(record.id,'date',this_graph))

    if entity_type == 'location' or entity_type == 'all':
        cmd = '''START n=node(%s)
MATCH n-[:REFERENCES]->(l)
WHERE l:LOCATION
return id(l) as id;''' % (doc_id)
        #print(cmd)
        for record in this_graph.cypher.execute(cmd):
            #similarity between entity and doc = 1
            #since the priority queue takes the element
            #with the lowest score first, we need to use
            #negative similarities
            #start_entities.append((0-1,entity.GraphEntity(record.id,'location',this_graph)))
            location_entities.append(entity.GraphEntity(record.id,'location',this_graph))

    #randomly load the queue
    #start_entities.shuffle()
    #print()
    #sort by similarity to the center point
    #dates = []

    if entity_type == 'date' or entity_type == 'all':
        this_sum = 0
        count = 0
        for e in date_entities:
            #print(e.id)
            date = float(e.getValue())
            this_sum += date
            count += 1
            #dates.append(date)
        #mean_date = statistics.mean(dates)
        if count == 0:
            mean_date = 0
        else:
            mean_date = this_sum/count

    if entity_type == 'location' or entity_type == 'all':
        #latitudes = []
        #longitudes = []
        lat_sum = 0
        long_sum = 0
        count = 0
        for e in location_entities:
            cmd = '''MATCH(d:LOCATION{locID:"%s"}) RETURN d.loc_long as long, d.loc_lat as lat''' % (e.getValue())
            loc_a_coord = 0
            for record in this_graph.cypher.execute(cmd):
                #latitudes.append(record.lat)
                #longitudes.append(record.long)
                lat_sum += record.lat
                long_sum += record.long
                count += 1
        #mean_lat = statistics.mean(latitudes)
        #mean_long = statistics.mean(longitudes)
        if count == 0:
            mean_lat = 0
            mean_long = 0
        else:
            mean_lat = lat_sum/count
            mean_long = long_sum/count

    start_queue = queue.PriorityQueue()
    for e in date_entities:
        sim = search_utilities.similarity(search_utilities.date_distance(mean_date,e.getValue()))
        start_queue.put((sim,e))

    for e in location_entities:
        sim = search_utilities.similarity(search_utilities.coord_distance(mean_lat,mean_long,e.getValue()))
        start_queue.put((sim,e))

    while not (entity_queue.empty() and start_queue.empty()) and len(results) < limit:
        if not start_queue.empty():
            pair = (1,start_queue.get()[1])
        else:
            pair = entity_queue.get()
        #print(pair[1])
        examined_entities.add(pair[1].id)
        #for each d_j with edge to y_i:

        #date_id = ''
        #for record in this_graph.cypher.execute("MATCH(d:DATE{dateID:'"+ pair[1] +"'})  RETURN ID(d) as id"):
            #date_id = record.id
        #cmd = '''START n=node(%s)
#MATCH n-[:REFERENCES]-(d)
#WHERE d:DOCUMENT
#return d.docID as docID;''' % (date_id)

        for record in pair[1].getDocuments():
            #add d_j to results
            if not record.docID in examined_docs:
                examined_docs.add(record.docID)
                if len(target_documents) == 0 or record.docID in target_documents:
                    results.append((pair[0],record.docID))
                if len(results) >= limit:
                    break
        #for each y_j with edge to y_i:
        #cmd = '''START n=node(%s)
#MATCH n-[:SIMDATES]-(y)
#WHERE y:DATE
#return y.dateID as dateID;''' % (date_id)

        for record in pair[1].getNeighbors():
            #if y_j not in examined_dates:
            if not record.id in examined_entities:
                #add y_j to date_queue
                if pair[1].type == 'date':
                    sim = search_utilities.similarity(search_utilities.date_distance(pair[1].getValue(),record.dateID))
                elif pair[1].type == 'location':
                    sim = search_utilities.similarity(search_utilities.loc_distance(pair[1].getValue(),record.locID))
                e = entity.GraphEntity(record.id,pair[1].type,this_graph)
                i = pair[0]*sim
                #print(i)
                #if direct_only is set to true, then we will only search for
                #documents directly linked through a shared entity
                #will never add anything to the entity_queue
                if not direct_only:
                    entity_queue.put((i,e))

    if not len(target_documents) == 0:
        results = secondary_sort(results, original_ranking, limit, doc)

    return results

#break tie scores in the results by using the original ranking
#if results is empty, returns the documents from the original ranking
#in their original order
def secondary_sort(results: [], original_ranking: dict, limit: int, ignore: str = '') -> []:

    #bin the results by distance score
    score_set = dict()
    scores = []
    docs = set()
    #results = <score, doc> pairs
    #print('DISTANCE RESULTS:')
    for result in results:
        #print('{0} {1}'.format(result[0], result[1]))
        if not result[0] in score_set:
            score_set[result[0]] = []
            scores.append(result[0])
            docs.add(result[1])
        score_set[result[0]].append(result[1])
    #print('')

    if len(results) < limit:
        score_set['0'] = []
        scores.append('0')
        missing = limit - len(results)
        for line in original_ranking['results']:
            doc = line[1]
            if missing > 0 and (not doc in docs) and not doc == ignore:
                score_set['0'].append(doc)
                missing = missing - 1

    results = []
    #print('SORTING')
    #sort equidistant documents by original score
    for score in scores:
        pq = queue.PriorityQueue()
        for doc in score_set[score]:
            pq.put((original_ranking['ranking_dict'][doc],doc))
        while not pq.empty():
            result = pq.get()
            #print('{0} {1}'.format(score, result[1]))
            results.append((score,result[1]))
    #print('')

    
    #if len(results) == 0:
    #    scores = []
    #    for i in range(limit):
    #        results.append(original_ranking['results'][i])


    return results

main()
