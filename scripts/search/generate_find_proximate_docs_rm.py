import os
import argparse
import shutil

def main():
    p = argparse.ArgumentParser("Creates a script to run the proximity search on rm3 results")
    p.add_argument('type', help="type of entity to use")
    p.add_argument('-p', '--prf', help='details evaluation results for each run', action='store_true', default=False)
    args = p.parse_args()

    entity = args.type
    prf = args.prf

    qids = [1,2,3,4,5,6,7,8,9,10]
    
    temp_path = 'tmp'
    if not os.path.exists(temp_path):
        os.mkdir(temp_path)
    if not os.path.exists(temp_path+'/initial'):
        os.mkdir(temp_path+'/initial')
    if not os.path.exists(temp_path+'/results'):
        os.mkdir(temp_path+'/results')
    if prf:
        if not os.path.exists('proximate_docs/rm3_prf_t3_' + entity):
            os.mkdir('proximate_docs/rm3_prf_t3_' + entity)
        rf = 'prf'
    else:
        if not os.path.exists('proximate_docs/rm3_rf_t3_' + entity):
            os.mkdir('proximate_docs/rm3_rf_t3_' + entity)
        rf = 'rf'

    cmd = ''

    for qid in qids:
        #load up the results from the query
        #create a results file for each segment
        #create an initial file using the relevant docs in that segment

        if os.path.exists('proximate_docs/rm3_{0}_t3_{1}/query{2:d}'.format(rf,entity,qid)):
            shutil.rmtree('proximate_docs/rm3_{0}_t3_{1}/query{2:d}'.format(rf,entity,qid))
        os.mkdir('proximate_docs/rm3_{0}_t3_{1}/query{2:d}'.format(rf,entity,qid))
        if os.path.exists('tmp/initial/query{:d}'.format(qid)):
            shutil.rmtree('tmp/initial/query{:d}'.format(qid))
        os.mkdir('tmp/initial/query{:d}'.format(qid))
        if os.path.exists('tmp/results/query{:d}'.format(qid)):
            shutil.rmtree('tmp/results/query{:d}'.format(qid))
        os.mkdir('tmp/results/query{:d}'.format(qid))

        relevant_docs = set()
        #first determine what files are relevant
        for line in open('judgments/doc-binary.rel'):
            segments = line.strip().split()
            #print(segments[0] + ' ' + str(qid) + ' ' + segments[3])
            if int(segments[0]) == qid and segments[3] == '1':
                relevant_docs.add(segments[2])
                #print('{0:d} {1}'.format(qid,segments[2]))

        #generate the temporary files
        
        output = ''
        initial = ''
        run_counter = 0
        initial_counter = 0
        k = 0
        for line in open('rm_results-full/query{:d}.txt'.format(qid)):
            if len(line.strip()) == 0:
                if len(output.strip()) > 0:
                    run_counter += 1
                    writer = open(temp_path+'/initial/query'+str(qid)+'/run'+str(run_counter)+'.txt','w')
                    writer.write(initial)
                    writer.close()
                    #writer = open(temp_path+'/results/query'+str(qid)+'/run'+str(run_counter),'w')
                    #writer.write(output)
                    #writer.close()

                    #if not os.path.exists('rm3-proximity-search-all/query{0:d}/input{1:d}'.format(qid,run_counter)):
                    #    os.mkdir('rm3-proximity-search-all/query{0:d}/input{1:d}'.format(qid,run_counter))

                    output = ''
                    initial = ''

                    #print(cmd)
                    #print(initial_counter)
                    cmd = generate(cmd,qid,run_counter,range(1,initial_counter+1),entity,rf)
                    #print(cmd)
                    initial_counter = 0
                    k = 0

            else:
                output += line
                k += 1
                if line.split()[2] in relevant_docs and k < 11 and not prf:
                    initial += '{} 1\n'.format(line.split()[2])
                    initial_counter +=1
                if k < 11 and prf:
                    initial += '{} 1\n'.format(line.split()[2])
                    initial_counter +=1

        for line in open('rm_results-dropped/query{:d}.txt'.format(qid)):
            if len(line.strip()) == 0:
                if len(output.strip()) > 0:
                    run_counter += 1
                    #writer = open(temp_path+'/initial/query'+str(qid)+'/run'+str(run_counter)+'.txt','w')
                    #writer.write(initial)
                    #writer.close()
                    writer = open(temp_path+'/results/query'+str(qid)+'/run'+str(run_counter),'w')
                    writer.write(output)
                    writer.close()

                    #if not os.path.exists('rm3-proximity-search-all/query{0:d}/input{1:d}'.format(qid,run_counter)):
                    #    os.mkdir('rm3-proximity-search-all/query{0:d}/input{1:d}'.format(qid,run_counter))

                    output = ''
                    initial = ''

                    #print(cmd)
                    #print(initial_counter)
                    cmd = generate(cmd,qid,run_counter,range(1,initial_counter+1),entity,rf)
                    #print(cmd)
                    initial_counter = 0
                    k = 0

            else:
                output += line
                k += 1
                if line.split()[2] in relevant_docs and k < 11 and not prf:
                    initial += '{} 1\n'.format(line.split()[2])
                    initial_counter +=1
                if k < 11 and prf:
                    initial += '{} 1\n'.format(line.split()[2])
                    initial_counter +=1
                

    writer = open('find_proximate_docs_rm3.sh','w')
    writer.write(cmd)
    writer.close()

    #set permission
    os.system("chmod 755 find_proximate_docs_rm3.sh")

def generate(cmd,qid,rid,oids,entity,rf):
    #print('{},{},{}'.format(cmd,qid,rid))

    #for oid in oids:
    cmd += 'qsub -cwd -b y '
    cmd += '-e rm3_{0}_t3_{1}-query{2:d}-input{3:d}.err '.format(rf,entity,qid,rid)
    cmd += '-o proximate_docs/rm3_{0}_t3_{1}/query{2:d}/input{3:d} '.format(rf,entity,qid,rid)
    cmd += '/share/apps/python3/bin/python3 scripts/search/find_proximate_docs_t3.py '
    cmd += '/home/wem/multi-domain-ir/'
    cmd += 'tmp/initial/query{0:d}/run{1:d}.txt '.format(qid,rid)
    cmd += '-l /home/wem/multi-domain-ir/'
    cmd += 'tmp/results/query{0:d}/run{1:d} '.format(qid,rid)
    cmd += '-q {0:d} -e {1}\n'.format(qid,entity)

        #if not os.path.exists('rm3-proximity-search-all/query{0:d}/input{1:d}/run{2:d}'.format(qid,rid,oid)):
            #os.mkdir('rm3-proximity-search-all/query{0:d}/input{1:d}/run{2:d}'.format(qid,rid,oid))
    #print(cmd)        

    return cmd

main()
