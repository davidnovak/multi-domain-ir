#!/usr/bin/python
import sys, os
import re
import time
import sqlite3
from subprocess import call, Popen, PIPE

#args: <database> <metadata file list>
def main():
    load(sys.argv[1])

def load(metadata_file):

    files = []
    files.append(metadata_file)
    
    metadata_entries = []

    values = {"<identifier>":"","<title>":"","<date>":"","<TEI>":""}

    reader = open(metadata_file,'r')
    for line in reader:
        if line == '#METADATA\n':
            title = values["<title>"]
            if title == '':
                title = values["<TEI>"]
            entry = values["<identifier>"] + '\t' + title + '\t' + values["<date>"]
            if not entry == '\t\t':
                metadata_entries.append(entry)
            values = {"<identifier>":"","<title>":"","<date>":"","<TEI>":""}
        results = []
        results.append(re.search("identifier,(.*)", line))
        results.append(re.search("title,(.*)", line))
        results.append(re.search("^date,(.*)", line))
        results.append(re.search("TEI,(.*)", line))
        if results[0]:
            if values["<identifier>"] == "":
                values["<identifier>"] = results[0].group(1)
            else:
                values["<identifier>"] = values["<identifier>"] + " | " + results[0].group(1)
        if results[1]:
            if values["<title>"] == "":
                values["<title>"] = results[1].group(1)
            else:
                values["<title>"] = values["<title>"] + " | " + results[1].group(1)
        if results[2]:
            if values["<date>"] == "":
                values["<date>"] = results[2].group(1)
            else:
                values["<date>"] = values["<date>"] + " | " + results[2].group(1)
        if results[3]:
            if values["<TEI>"] == "":
                values["<TEI>"] = results[3].group(1)
            else:
                values["<TEI>"] = values["<TEI>"] + " | " + results[2].group(1)
    reader.close()

    writer = open('metadata_list.txt','w')
    for entry in metadata_entries:
        writer.write(entry + '\n')
    writer.close()
          
main()
