import sys

def main():

    results_file = sys.argv[1]
    dict_file = sys.argv[2]

    reader = open(dict_file, 'r')
    book_dict = dict()
    for line in reader:
        print(line)
        book = line.strip().split('\t')[0]
        title = line.strip().split('\t')[1]
        if len(line.strip().split('\t')) > 2:
            date = line.strip().split('\t')[2]
        else:
            date = ''
        book_dict[book] = [title,date]
    reader.close()

    reader = open(results_file, 'r')
    books = set()
    output = []   
    for line in reader:
        doc = line.strip().split(',')[1]
        segments = doc.split('_')
        book = segments[0]
        name = book + '-page' + segments[1]
        title = book_dict[book][0]
        date = book_dict[book][1]
        line = name + '\t"' + title + '  - page ' + segments[1] + '"\t' + date
        books.add(line)
    reader.close()
    
    writer = open('documents.tsv','w')
    writer.write('docID:ID\tname\tyearPublished:int\n')
    for book in books:
        writer.write(book + '\n')
    writer.close()

main()
