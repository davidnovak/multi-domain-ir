#!/share/apps/python/bin/python

import sys, os
import re
import shlex
from subprocess import call, Popen, PIPE
import argparse

default_output = 'page-page.tsv'

p = argparse.ArgumentParser('generate the page-page similarity edges')
p.add_argument('-b', '--books', help='a file with books and their number of pages', required=True)
p.add_argument('-o', '--output', help='output file, default' + default_output, default=default_output)
args = p.parse_args()

header = 'docID:ID\tpagesim:float\tdocID:ID'


def generate_one_book(bookid, pagecount, output):
    counter = 1
    while counter <= pagecount:
        output.write(bookid + "_" + str(counter - 1) + '\t1.0\t' + bookid + "_" + str(counter) + '\n')
        counter += 1


def main():
    reader = open(args.books, 'r')
    writer = open(args.output, 'w')
    writer.write(header + '\n')
    r = re.compile('^(.*)\s\s*(.*)$')
    for line in reader:
        m = r.match(line.strip())
        if not m or not m.group(1) or not m.group(2):
            sys.stderr.write('error parsing line: "' + line + '"')
            continue
        generate_one_book(m.group(1), int(m.group(2)), writer)
    writer.close()
    reader.close()


main()