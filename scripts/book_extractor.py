import sys

def main():

    results_file = sys.argv[1]

    reader = open(results_file, 'r')
    books = set()
    output = []   
    for line in reader:
        book = line.split(' ')[2].split('_')[0]
        books.add(book)
    reader.close()
    
    writer = open('output','w')
    for book in books:
        writer.write(book + '\n')
    writer.close()

main()
