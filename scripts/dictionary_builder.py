import sys

def main():

    results_file = sys.argv[1]

    reader = open(results_file, 'r')
    locations = dict()   
    for line in reader:
        name = line.split(',')[0]
        lat = line.split(',')[1]
        lng = line.split(',')[2]
        latlng = (lat + ',' + lng).strip()
        if not latlng in locations:
            #print(latlng)
            locations[latlng] = [name,set()]
        else:
            #print(latlng)
            #print(locations[latlng])
            if len(name) < len(locations[latlng][0]):
                locations[latlng][1].add(locations[latlng][0])
                locations[latlng][0] = name
            else:
                locations[latlng][1].add(name)
    reader.close()
    
    writer = open('dictionary_file.csv','w')
    for ll in locations:
        output = locations[ll][0] + ','
        for name in locations[ll][1]:
            output += name + ','
        writer.write(output + '\n')
    writer.close()

main()
