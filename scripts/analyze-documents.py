#!/usr/bin/python

import dis
import sys
import math
import shlex
from subprocess import call, Popen, PIPE
import py2neo
from py2neo import Graph
from py2neo.cypher import RecordList
from py2neo.cypher import Record
import argparse
import numpy as np
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser(description="will analyze neighbors of relevant and non-relevant documents")
parser.add_argument("-c", "--connection", help="use a different than default http://localhost:7474/db/data")
parser.add_argument("-j", "--judgements", help="file with judgements")
parser.add_argument("-r", "--results", help="file with result from spreading activation")
parser.add_argument("-i", "--initial", help="file with the initial documents")
parser.add_argument("-a", "--all", help="file with all documents")
parser.add_argument("-d", "--docID", help="a single doc ID to process")
parser.add_argument("-v", "--verbose", help="print all values", action='store_true')
parser.add_argument("-s", "--store", dest="queryid", metavar="queryid", help="sets the labels '<queryid>_INITIAL' nad '<queryid>_RELEVANT' to respective nodes in Neo4j'")
parser.add_argument("--distribution", dest="random_pairs", action='store', type=int, help="calculate distribution among relevant, nonrelevant, and initial-rel, initial-nonrel")
args = parser.parse_args()

graph = Graph(args.connection) if args.connection else Graph()


def execute_cypher_py2neo(cmd):
    """
    Runs given Cypher command on the Neo4j connection
    :param cmd:
    :return: RecordList
    """
    records = graph.cypher.execute(cmd)
    #for record in records:
    #    print(record)
    return records



cql_commands = [
    ("all edges", "MATCH (d:DOCUMENT {docID: '%s'})-[]-(o) RETURN count(o);"),
    ("locations", "MATCH (d:DOCUMENT {docID: '%s'})-[]-(o:LOCATION) RETURN count(o);"),
    ("dates", "MATCH (d:DOCUMENT {docID: '%s'})-[]-(o:DATE) RETURN count(o);")
]

def analyze_list(list_of_ids):
    """
    Analyses the properties of given list of document IDs. The results are printed to stdout.
    :param list_of_ids:
    :return:
    """
    for (label, command) in cql_commands:
        edges = []
        for docid in list_of_ids:
            edges.append(execute_cypher_py2neo(command % docid).one)
            #edges.append(int(execute_cypher_py2neo(command % docid).one[0]))
        print "\t" + label + ": ",
        if args.verbose:
            print np.sort(edges)
        print "\t\taverage: " + str(np.mean(edges))


cql_set_label = "MATCH (d:DOCUMENT {docID: '%s'}) SET d :%s RETURN d"
def set_label(ids, label):
    for docid in ids:
        execute_cypher_py2neo(cql_set_label % (docid, label))
        if args.verbose:
            print "setting label '" + label + "' to doc with ID: " + docid



# 1 0 graduateschoolof9192bran_51 0
def parse_judgements(reader):
    relevant, nonrelevant = [], []
    for line in reader:
        parts = line.strip().split(' ')
        if parts[3] == "0":
            nonrelevant.append(parts[2])
        else:
            relevant.append(parts[2])
    return relevant, nonrelevant


def parse_results(reader):
    ids = []
    for line in reader:
        line = line.strip()
        if line is '' or line[0] == '#':
            continue
        ids.append(line.split('   ')[2])
    return ids

# historyofuniteds07good_239,1
def parse_csv(reader):
    ids = []
    for line in reader:
        ids.append(line.strip().split(',')[0])
    return ids


if args.docID:
    print("\nanalyzing doc with ID: " + args.docID)
    analyze_list([args.docID])

all_docs = []
if args.all:
    print "\nreading & analyzing file with all documents " + args.all
    reader = open(args.all, "r")
    all_docs = parse_csv(reader)
    reader.close()
    # skip the first line and select just 1000 random ids
    ids = np.random.choice(all_docs[1:], 1000)
    analyze_list(ids)


relevant, nonrelevant = [], []
if args.judgements:
    print "\nreading & analyzing judgements file " + args.judgements
    reader = open(args.judgements, "r")
    relevant, nonrelevant = parse_judgements(reader)
    reader.close()
    print "  relevant documents (" + str(len(relevant)) + "): "
    analyze_list(relevant)
    print "  non-relevant documents (" + str(len(nonrelevant)) + "): "
    analyze_list(nonrelevant)

    # set labels in Neo4f
    if args.queryid:
        set_label(relevant, args.queryid + "_RELEVANT")
        set_label(nonrelevant, args.queryid + "_NONRELEVANT")

initial = []
if args.initial:
    print "\nreading & analyzing file with initial documents " + args.initial
    reader = open(args.initial, "r")
    initial = parse_csv(reader)
    reader.close()
    analyze_list(initial)

    # set labels in Neo4f
    if args.queryid:
        set_label(initial, args.queryid + "_INITIAL")


if args.results:
    print "\nreading & analyzing SA output file " + args.results
    reader = open(args.results, "r")
    ids = parse_results(reader)
    reader.close()
    analyze_list(ids)
    if relevant:
        ids_nonrelevant = np.setdiff1d(ids, relevant)
        print "  non-relevant in output file (" + str(len(ids_nonrelevant)) + "): "
        analyze_list(ids_nonrelevant)
    if initial:
        ids_noninitial = np.setdiff1d(ids, initial)
        print "  non-initial docs in output file (" + str(len(ids_noninitial)) + "): "
        analyze_list(ids_noninitial)



#########################################################################
###   Analysis of the number of shared dates and locations
#########################################################################

def get_random_pair(list1, list2):
    first, second = np.random.choice(list1), np.random.choice(list2)
    while first == second:
        second = np.random.choice(list2)
    return first, second


def get_random_excluded(list1, to_exclude_from):
    first = np.random.choice(list1)
    while first in to_exclude_from:
        first = np.random.choice(list1)
    return first


def draw_plot(distribution, title=None, avg=None):
    dist = [0] * (np.max(distribution) + 1)
    for value in set(distribution):
        # to_plot[value/2] = float(distribution.count(value)) / len(distribution)
        dist[value] = float(distribution.count(value)) / len(distribution)

    if len(dist) > 10:
        to_plot = [0] * len(dist)
        for i in range(0, len(to_plot)/2):
            to_plot[2*i] = (dist[2*i] + dist[2 * i + 1]) / 2
            to_plot[2*i+1] = to_plot[2 * i]
    else:
        to_plot = dist

    if avg:
        if title:
            title += ', avg: ' + "%.2f" % avg
        else:
            title = 'avg: ' + "%.2f" % avg

    plt.plot(to_plot, label=title)


def save_plot(title, filename):
    plt.legend()
    plt.title(title)
    #plt.show()
    plt.savefig(filename)
    plt.clf()


def print_distribution(distribution, title=None):
    print np.sort(distribution)
    avg = np.mean(distribution)
    print "\taverage: " + str(avg)
    draw_plot(distribution, title=title, avg=avg)


def node_node_distribution(cql_path, list1, list2, title=None):
    paths = []
    for i in range(0, args.random_pairs):
        paths.append(execute_cypher_py2neo(cql_path % get_random_pair(list1, list2)).one)
    print_distribution(paths, title=title)


def set_node_distribution(cql_path, set, list, title=None):
    paths = []
    for i in range(0, min(args.random_pairs, len(list))):
        paths.append(execute_cypher_py2neo(cql_path % get_random_excluded(list, set)).one)
    print_distribution(paths, title=title)


variants = [
    ('locations_and_dates', ''),
    ('locations', ':LOCATION'),
    ('dates', ':DATE')
]
if args.random_pairs:
    for variant_desc, variant_cql in variants:
        print
        cql_anypath = "MATCH (d:DOCUMENT {docID: '%s'})-[]-(a" + variant_cql +")-[]-(x:DOCUMENT {docID: '%s'}) RETURN count(DISTINCT a);"
        if relevant:
            print 'random pairs relevant-relevant'
            node_node_distribution(cql_anypath, relevant, relevant, title='relevant-relevant')

        if relevant and nonrelevant:
            print 'random pairs of relevant-nonrelevant'
            node_node_distribution(cql_anypath, relevant, nonrelevant, 'relevant-nonrelevant')

        if relevant and all_docs:
            print 'random pairs of relevant-any'
            node_node_distribution(cql_anypath, relevant, all_docs, 'relevant-any')

        save_plot('one document - one document, # of shared ' + variant_desc, '1doc-1doc-shared-'+variant_desc+'.png')

        cql_set_path = "MATCH (d:DOCUMENT:EYOR_INITIAL)-[]-(a" + variant_cql + ")-[]-(x:DOCUMENT {docID: '%s'}) RETURN count(DISTINCT a);"
        if relevant and initial:
            print 'random pairs initial set - relevant'
            set_node_distribution(cql_set_path, initial, relevant, title='initial set - relevant')

        if initial and nonrelevant:
            print 'random pairs of initial set - nonrelevant'
            set_node_distribution(cql_set_path, initial, nonrelevant, title='initial set - nonrelevant')

        if initial and all_docs:
            print 'random pairs of initial set - a random_doc'
            set_node_distribution(cql_set_path, initial, all_docs, title='initial set - any document')

        save_plot('six initial documents - one document, # of shared ' + variant_desc, '6doc-1doc-shared-'+variant_desc+'.png')

#neo4j_command_prefix="/Users/david/Applications/neo4j-community-2.3.0/bin/neo4j-shell -readonly -path "
#
#if len(sys.argv) < 2:
#    print 'Usage: ' + sys.argv[0] + ' <database-dir> <file-with-judgements> [<sa-output-file>]'
#    print '   will run commands: ' + neo4j_command_prefix + "<database-dir>"
#    exit(1)
#
#neo4j_command_prefix += sys.argv[1] + " -c "
#
#def execute_cypher_cmd(cmd):
#    """
#    Executes given Cypher command on Neo4j
#    :rtype: list
#    """
#    print >> sys.stderr, "running command: " + cmd
#    gproc = Popen(shlex.split(cmd), stdout=PIPE)
#    gout = gproc.communicate()[0].decode('UTF-8')
#    lines = gout.strip().split("\n")
#    return lines