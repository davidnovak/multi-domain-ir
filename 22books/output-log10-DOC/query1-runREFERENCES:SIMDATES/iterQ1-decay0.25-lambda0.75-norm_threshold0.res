###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 243.468, activation diff: 230.749, ratio: 0.948
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 601.082, activation diff: 357.614, ratio: 0.595
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1256.678, activation diff: 655.596, ratio: 0.522
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1971.681, activation diff: 715.003, ratio: 0.363
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2633.605, activation diff: 661.925, ratio: 0.251
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 3203.829, activation diff: 570.223, ratio: 0.178
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 3677.785, activation diff: 473.956, ratio: 0.129
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 4063.924, activation diff: 386.139, ratio: 0.095
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 4374.661, activation diff: 310.738, ratio: 0.071
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 4622.676, activation diff: 248.015, ratio: 0.054
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 4819.493, activation diff: 196.816, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4819.5
#   number of spread. activ. pulses: 12
#   running time: 513

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756117   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98399276   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9825518   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98156923   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97857517   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9703882   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7137985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7137503   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.71317005   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.713153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7129772   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.71291566   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.71284765   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.71185863   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7118505   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.711654   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.71158004   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.71156406   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.71150005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.7113505   REFERENCES:SIMDATES
