###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 890.941, activation diff: 871.076, ratio: 0.978
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1763.332, activation diff: 872.391, ratio: 0.495
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3424.119, activation diff: 1660.787, ratio: 0.485
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4629.213, activation diff: 1205.094, ratio: 0.260
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5381.804, activation diff: 752.591, ratio: 0.140
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5826.308, activation diff: 444.504, ratio: 0.076
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 6082.124, activation diff: 255.816, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6082.1
#   number of spread. activ. pulses: 8
#   running time: 444

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99541926   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99501544   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947777   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99356574   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9901426   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79306394   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.793033   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7929054   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7928353   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.79280096   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.7927593   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7927164   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.79248214   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.79245543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.79244244   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.7924342   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7923945   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7923626   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.7923051   REFERENCES:SIMDATES
