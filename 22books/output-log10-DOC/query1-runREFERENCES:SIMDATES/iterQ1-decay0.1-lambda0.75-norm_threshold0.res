###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 288.642, activation diff: 275.923, ratio: 0.956
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 748.586, activation diff: 459.944, ratio: 0.614
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1728.269, activation diff: 979.683, ratio: 0.567
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2819.178, activation diff: 1090.909, ratio: 0.387
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3833.418, activation diff: 1014.240, ratio: 0.265
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4707.711, activation diff: 874.293, ratio: 0.186
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 5434.130, activation diff: 726.419, ratio: 0.134
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 6025.221, activation diff: 591.091, ratio: 0.098
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 6500.124, activation diff: 474.903, ratio: 0.073
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 6878.546, activation diff: 378.422, ratio: 0.055
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 7178.394, activation diff: 299.848, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7178.4
#   number of spread. activ. pulses: 12
#   running time: 697

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756194   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98400515   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9825661   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.981603   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97861505   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9705395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.85661346   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.85652196   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.85588425   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8558422   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.8556428   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.8555837   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.8555094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.85445917   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.85444915   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.8542304   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.85419136   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.8540108   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8539349   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.85385025   REFERENCES:SIMDATES
