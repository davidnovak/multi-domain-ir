###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1318.821, activation diff: 1321.033, ratio: 1.002
#   pulse 3: activated nodes: 8648, borderline nodes: 3408, overall activation: 1446.063, activation diff: 766.607, ratio: 0.530
#   pulse 4: activated nodes: 10815, borderline nodes: 3099, overall activation: 3674.241, activation diff: 2268.467, ratio: 0.617
#   pulse 5: activated nodes: 11247, borderline nodes: 1431, overall activation: 4712.106, activation diff: 1038.253, ratio: 0.220
#   pulse 6: activated nodes: 11299, borderline nodes: 542, overall activation: 5119.046, activation diff: 406.940, ratio: 0.079
#   pulse 7: activated nodes: 11317, borderline nodes: 458, overall activation: 5266.694, activation diff: 147.648, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 5266.7
#   number of spread. activ. pulses: 7
#   running time: 430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99933827   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9991553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99906945   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9982053   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99654037   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7494053   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74938774   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7493608   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7493391   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7493321   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7492859   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7492768   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.74925566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7492547   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.74924403   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.74919426   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   18   0.7491861   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   19   0.749181   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   20   0.7491695   REFERENCES:SIMDATES
