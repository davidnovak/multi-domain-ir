###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 168.177, activation diff: 155.458, ratio: 0.924
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 373.744, activation diff: 205.566, ratio: 0.550
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 656.006, activation diff: 282.262, ratio: 0.430
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 939.173, activation diff: 283.167, ratio: 0.302
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1192.539, activation diff: 253.366, ratio: 0.212
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1407.594, activation diff: 215.055, ratio: 0.153
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1585.022, activation diff: 177.428, ratio: 0.112
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1728.923, activation diff: 143.900, ratio: 0.083
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1844.331, activation diff: 115.408, ratio: 0.063
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1936.169, activation diff: 91.838, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1936.2
#   number of spread. activ. pulses: 11
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98341185   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9786153   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9766717   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97534007   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97170967   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9607532   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4677034   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.46766582   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.46717697   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.46706015   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.46693096   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.46678716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4665159   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.46568787   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.46567944   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.46562266   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   17   0.4654278   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.46539018   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.4653718   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.46524966   REFERENCES:SIMDATES
