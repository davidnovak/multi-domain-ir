###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1743.598, activation diff: 1738.154, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2417.685, activation diff: 889.021, ratio: 0.368
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4785.418, activation diff: 2367.733, ratio: 0.495
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5829.631, activation diff: 1044.213, ratio: 0.179
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6213.712, activation diff: 384.081, ratio: 0.062
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 6345.626, activation diff: 131.915, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6345.6
#   number of spread. activ. pulses: 7
#   running time: 413

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9996607   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99961364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994817   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99866295   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974916   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7996784   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.7996624   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7996038   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.79958606   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.79957455   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.79956543   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7995641   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7995428   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7995399   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.7995354   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.79951525   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.79951394   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7994929   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.7994907   REFERENCES:SIMDATES
