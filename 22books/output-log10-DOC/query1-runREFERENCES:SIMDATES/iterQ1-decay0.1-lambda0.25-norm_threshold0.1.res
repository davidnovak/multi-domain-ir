###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1777.426, activation diff: 1775.797, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 2411.541, activation diff: 1129.455, ratio: 0.468
#   pulse 4: activated nodes: 10849, borderline nodes: 2436, overall activation: 5665.391, activation diff: 3278.236, ratio: 0.579
#   pulse 5: activated nodes: 11284, borderline nodes: 727, overall activation: 7221.836, activation diff: 1556.547, ratio: 0.216
#   pulse 6: activated nodes: 11335, borderline nodes: 83, overall activation: 7828.761, activation diff: 606.926, ratio: 0.078
#   pulse 7: activated nodes: 11341, borderline nodes: 18, overall activation: 8043.783, activation diff: 215.021, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 8043.8
#   number of spread. activ. pulses: 7
#   running time: 406

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995447   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99944973   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99932075   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984617   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997069   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8994984   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.899495   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8994374   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.899411   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.8994005   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.89937913   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8993483   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.8993412   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.8993365   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.8993314   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.8993192   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.89931273   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.8992893   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   20   0.8992833   REFERENCES:SIMDATES
