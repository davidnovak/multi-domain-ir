###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 130.525, activation diff: 118.483, ratio: 0.908
#   pulse 3: activated nodes: 8518, borderline nodes: 3427, overall activation: 305.246, activation diff: 174.720, ratio: 0.572
#   pulse 4: activated nodes: 8931, borderline nodes: 3613, overall activation: 572.907, activation diff: 267.661, ratio: 0.467
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 853.190, activation diff: 280.282, ratio: 0.329
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1109.503, activation diff: 256.313, ratio: 0.231
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1329.840, activation diff: 220.337, ratio: 0.166
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1513.161, activation diff: 183.321, ratio: 0.121
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1662.750, activation diff: 149.589, ratio: 0.090
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1783.288, activation diff: 120.538, ratio: 0.068
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1879.575, activation diff: 96.287, ratio: 0.051
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1956.004, activation diff: 76.430, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1956.0
#   number of spread. activ. pulses: 12
#   running time: 449

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869367   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9827068   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9807551   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9799038   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97692376   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9666035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.47511366   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.47503775   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47472557   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.4745304   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.47451204   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.47423303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.47418332   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.47350287   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.4734221   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.47337684   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   17   0.47324482   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.4732303   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.4732253   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.4730947   REFERENCES:SIMDATES
