###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 96.385, activation diff: 85.063, ratio: 0.883
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 238.712, activation diff: 142.346, ratio: 0.596
#   pulse 4: activated nodes: 8582, borderline nodes: 3940, overall activation: 486.225, activation diff: 247.513, ratio: 0.509
#   pulse 5: activated nodes: 8930, borderline nodes: 3613, overall activation: 760.291, activation diff: 274.066, ratio: 0.360
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1018.367, activation diff: 258.076, ratio: 0.253
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1243.909, activation diff: 225.543, ratio: 0.181
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1433.553, activation diff: 189.643, ratio: 0.132
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1589.458, activation diff: 155.905, ratio: 0.098
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1715.796, activation diff: 126.337, ratio: 0.074
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1817.169, activation diff: 101.373, ratio: 0.056
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1897.935, activation diff: 80.766, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1897.9
#   number of spread. activ. pulses: 12
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98607355   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9811009   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9783596   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9779544   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9751232   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96239924   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4742909   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.47414127   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47395107   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.47375512   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.47354633   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.47335395   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.47316065   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.47272396   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.47246897   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.47242972   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.47236213   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   18   0.47236013   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4723192   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.4721601   REFERENCES:SIMDATES
