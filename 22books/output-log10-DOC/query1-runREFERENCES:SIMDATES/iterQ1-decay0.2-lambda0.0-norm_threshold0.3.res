###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2069.909, activation diff: 2096.312, ratio: 1.013
#   pulse 3: activated nodes: 8569, borderline nodes: 3417, overall activation: 825.364, activation diff: 2894.891, ratio: 3.507
#   pulse 4: activated nodes: 10742, borderline nodes: 3063, overall activation: 4713.077, activation diff: 5460.915, ratio: 1.159
#   pulse 5: activated nodes: 11256, borderline nodes: 1369, overall activation: 1634.246, activation diff: 5650.275, ratio: 3.457
#   pulse 6: activated nodes: 11316, borderline nodes: 285, overall activation: 5223.945, activation diff: 5299.840, ratio: 1.015
#   pulse 7: activated nodes: 11323, borderline nodes: 213, overall activation: 5858.434, activation diff: 1114.759, ratio: 0.190
#   pulse 8: activated nodes: 11326, borderline nodes: 181, overall activation: 6086.082, activation diff: 284.207, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 6086.1
#   number of spread. activ. pulses: 8
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998575   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998986   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_503   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_488   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   14   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.8   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   20   0.8   REFERENCES:SIMDATES
