###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2592.559, activation diff: 2620.822, ratio: 1.011
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1289.694, activation diff: 3876.465, ratio: 3.006
#   pulse 4: activated nodes: 10844, borderline nodes: 2599, overall activation: 5993.474, activation diff: 7082.276, ratio: 1.182
#   pulse 5: activated nodes: 11277, borderline nodes: 887, overall activation: 3043.682, activation diff: 6743.361, ratio: 2.216
#   pulse 6: activated nodes: 11333, borderline nodes: 88, overall activation: 7096.789, activation diff: 5857.584, ratio: 0.825
#   pulse 7: activated nodes: 11339, borderline nodes: 24, overall activation: 7884.376, activation diff: 1086.892, ratio: 0.138
#   pulse 8: activated nodes: 11340, borderline nodes: 10, overall activation: 8028.176, activation diff: 196.880, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 8028.2
#   number of spread. activ. pulses: 8
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999877   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999084   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   7   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   8   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   10   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   11   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   12   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   13   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   15   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   16   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_517   17   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   18   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_514   20   0.9   REFERENCES:SIMDATES
