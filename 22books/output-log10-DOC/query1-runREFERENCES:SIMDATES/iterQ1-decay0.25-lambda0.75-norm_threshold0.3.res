###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 96.517, activation diff: 85.957, ratio: 0.891
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 272.629, activation diff: 176.156, ratio: 0.646
#   pulse 4: activated nodes: 9662, borderline nodes: 5522, overall activation: 716.517, activation diff: 443.888, ratio: 0.620
#   pulse 5: activated nodes: 10693, borderline nodes: 4223, overall activation: 1316.196, activation diff: 599.679, ratio: 0.456
#   pulse 6: activated nodes: 11050, borderline nodes: 2975, overall activation: 1947.797, activation diff: 631.602, ratio: 0.324
#   pulse 7: activated nodes: 11182, borderline nodes: 2105, overall activation: 2534.011, activation diff: 586.214, ratio: 0.231
#   pulse 8: activated nodes: 11225, borderline nodes: 1379, overall activation: 3045.518, activation diff: 511.507, ratio: 0.168
#   pulse 9: activated nodes: 11270, borderline nodes: 921, overall activation: 3476.922, activation diff: 431.404, ratio: 0.124
#   pulse 10: activated nodes: 11296, borderline nodes: 703, overall activation: 3833.399, activation diff: 356.477, ratio: 0.093
#   pulse 11: activated nodes: 11300, borderline nodes: 620, overall activation: 4124.030, activation diff: 290.631, ratio: 0.070
#   pulse 12: activated nodes: 11309, borderline nodes: 593, overall activation: 4358.961, activation diff: 234.931, ratio: 0.054
#   pulse 13: activated nodes: 11311, borderline nodes: 548, overall activation: 4547.711, activation diff: 188.749, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11311
#   final overall activation: 4547.7
#   number of spread. activ. pulses: 13
#   running time: 485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98866177   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98438567   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9818458   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.98155314   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9795716   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9679332   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7201481   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.71995974   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71976554   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7197587   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.7196099   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7191999   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7186588   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.7185936   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7184807   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.7184272   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   17   0.7181426   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.7180119   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7179738   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.7179679   REFERENCES:SIMDATES
