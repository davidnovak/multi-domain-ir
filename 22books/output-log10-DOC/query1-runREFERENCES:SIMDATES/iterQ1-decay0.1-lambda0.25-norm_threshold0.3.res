###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1373.422, activation diff: 1377.793, ratio: 1.003
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1554.087, activation diff: 1070.886, ratio: 0.689
#   pulse 4: activated nodes: 10568, borderline nodes: 3675, overall activation: 4998.374, activation diff: 3560.323, ratio: 0.712
#   pulse 5: activated nodes: 11226, borderline nodes: 1852, overall activation: 6734.673, activation diff: 1742.248, ratio: 0.259
#   pulse 6: activated nodes: 11308, borderline nodes: 267, overall activation: 7490.223, activation diff: 755.550, ratio: 0.101
#   pulse 7: activated nodes: 11334, borderline nodes: 103, overall activation: 7772.919, activation diff: 282.695, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7772.9
#   number of spread. activ. pulses: 7
#   running time: 413

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99906975   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99876416   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9987512   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978907   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9959613   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8990111   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.89900744   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   9   0.8990043   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.8989896   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   11   0.898964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.8989427   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.89890176   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.8988795   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   15   0.89886945   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   16   0.89886594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.8988552   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   18   0.89884585   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   19   0.8988427   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.8988247   REFERENCES:SIMDATES
