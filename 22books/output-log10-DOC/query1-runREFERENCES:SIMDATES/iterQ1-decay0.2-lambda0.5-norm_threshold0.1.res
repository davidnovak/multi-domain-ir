###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 773.859, activation diff: 755.720, ratio: 0.977
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1502.172, activation diff: 728.313, ratio: 0.485
#   pulse 4: activated nodes: 10831, borderline nodes: 3001, overall activation: 3143.427, activation diff: 1641.255, ratio: 0.522
#   pulse 5: activated nodes: 11256, borderline nodes: 1048, overall activation: 4381.668, activation diff: 1238.240, ratio: 0.283
#   pulse 6: activated nodes: 11319, borderline nodes: 327, overall activation: 5172.765, activation diff: 791.097, ratio: 0.153
#   pulse 7: activated nodes: 11333, borderline nodes: 109, overall activation: 5647.854, activation diff: 475.089, ratio: 0.084
#   pulse 8: activated nodes: 11336, borderline nodes: 61, overall activation: 5924.861, activation diff: 277.007, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 5924.9
#   number of spread. activ. pulses: 8
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951313   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9944961   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99434257   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99311805   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9889094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79277205   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7926873   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7926215   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.79255646   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.79244995   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.792386   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.79225117   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.79218155   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7920958   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.79205966   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.79204696   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7919661   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.79195285   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.79193604   REFERENCES:SIMDATES
