###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 883.820, activation diff: 886.032, ratio: 1.003
#   pulse 3: activated nodes: 8648, borderline nodes: 3408, overall activation: 739.122, activation diff: 281.761, ratio: 0.381
#   pulse 4: activated nodes: 8916, borderline nodes: 3611, overall activation: 1683.254, activation diff: 944.171, ratio: 0.561
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2032.900, activation diff: 349.646, ratio: 0.172
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2144.914, activation diff: 112.014, ratio: 0.052
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2178.825, activation diff: 33.911, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2178.8
#   number of spread. activ. pulses: 7
#   running time: 383

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993366   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9991553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99906266   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981992   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9965306   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49959952   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49959034   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4995601   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49953425   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.4995043   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   12   0.49945697   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49943596   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.4994226   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   15   0.4994083   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.49939287   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.49939114   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.49934885   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.49934825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.49934345   REFERENCES:SIMDATES
