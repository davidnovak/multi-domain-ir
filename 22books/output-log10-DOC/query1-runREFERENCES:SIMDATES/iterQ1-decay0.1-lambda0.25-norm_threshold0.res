###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1959.120, activation diff: 1953.676, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2895.060, activation diff: 1176.182, ratio: 0.406
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 6028.124, activation diff: 3133.064, ratio: 0.520
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 7454.452, activation diff: 1426.328, ratio: 0.191
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 7989.232, activation diff: 534.780, ratio: 0.067
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 8173.942, activation diff: 184.710, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 8173.9
#   number of spread. activ. pulses: 7
#   running time: 429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99966085   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99961364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999482   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.998663   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974916   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8996414   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.89962125   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8995596   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8995343   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.89953107   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.8995112   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.89951026   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.8994902   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.8994898   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.8994879   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.89947975   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.89947253   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   19   0.8994342   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.8994304   REFERENCES:SIMDATES
