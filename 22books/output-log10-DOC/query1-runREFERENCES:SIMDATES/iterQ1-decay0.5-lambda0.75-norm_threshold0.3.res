###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 69.035, activation diff: 58.474, ratio: 0.847
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 181.639, activation diff: 112.649, ratio: 0.620
#   pulse 4: activated nodes: 8267, borderline nodes: 4404, overall activation: 401.322, activation diff: 219.683, ratio: 0.547
#   pulse 5: activated nodes: 8778, borderline nodes: 3687, overall activation: 662.572, activation diff: 261.250, ratio: 0.394
#   pulse 6: activated nodes: 8918, borderline nodes: 3618, overall activation: 919.295, activation diff: 256.723, ratio: 0.279
#   pulse 7: activated nodes: 8929, borderline nodes: 3611, overall activation: 1149.029, activation diff: 229.734, ratio: 0.200
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1345.026, activation diff: 195.998, ratio: 0.146
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1507.763, activation diff: 162.736, ratio: 0.108
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1640.603, activation diff: 132.840, ratio: 0.081
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1747.803, activation diff: 107.200, ratio: 0.061
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1833.608, activation diff: 85.804, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1833.6
#   number of spread. activ. pulses: 12
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9848644   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97898334   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97547066   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.97482324   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97298276   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95686984   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4732549   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.47301346   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   9   0.4729584   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.47281784   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.47234535   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.47234482   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   13   0.47182631   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.47179666   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.47132483   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.47132218   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   17   0.4713178   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.4713053   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.47123808   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.47105923   REFERENCES:SIMDATES
