###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1635.836, activation diff: 1630.392, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2187.060, activation diff: 753.400, ratio: 0.344
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4195.814, activation diff: 2008.754, ratio: 0.479
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5063.870, activation diff: 868.056, ratio: 0.171
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 5377.709, activation diff: 313.839, ratio: 0.058
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5484.849, activation diff: 107.140, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5484.8
#   number of spread. activ. pulses: 7
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9996605   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99961364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99948144   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99866295   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974916   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7496971   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.749683   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.749626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74961174   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.7495965   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7495924   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.74959075   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7495669   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.74956375   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.7495613   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.74953693   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.7495233   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.74952304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.74952054   REFERENCES:SIMDATES
