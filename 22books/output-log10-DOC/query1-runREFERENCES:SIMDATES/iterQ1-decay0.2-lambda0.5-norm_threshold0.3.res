###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 534.531, activation diff: 521.384, ratio: 0.975
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 1008.631, activation diff: 478.846, ratio: 0.475
#   pulse 4: activated nodes: 10293, borderline nodes: 4814, overall activation: 2610.038, activation diff: 1601.407, ratio: 0.614
#   pulse 5: activated nodes: 11166, borderline nodes: 2499, overall activation: 3903.140, activation diff: 1293.102, ratio: 0.331
#   pulse 6: activated nodes: 11262, borderline nodes: 961, overall activation: 4779.159, activation diff: 876.019, ratio: 0.183
#   pulse 7: activated nodes: 11306, borderline nodes: 560, overall activation: 5322.009, activation diff: 542.850, ratio: 0.102
#   pulse 8: activated nodes: 11320, borderline nodes: 444, overall activation: 5645.661, activation diff: 323.652, ratio: 0.057
#   pulse 9: activated nodes: 11323, borderline nodes: 330, overall activation: 5834.896, activation diff: 189.235, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 5834.9
#   number of spread. activ. pulses: 9
#   running time: 517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99773425   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99701256   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9963851   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9962214   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99532205   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9915186   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79586244   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7958146   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7957899   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.7957431   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7957283   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.79558396   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.79549426   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7954325   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.795427   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.79534316   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   17   0.79533124   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.795294   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.7952858   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.7952652   REFERENCES:SIMDATES
