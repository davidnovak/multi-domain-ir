###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1940.921, activation diff: 1967.323, ratio: 1.014
#   pulse 3: activated nodes: 8569, borderline nodes: 3417, overall activation: 681.545, activation diff: 2622.086, ratio: 3.847
#   pulse 4: activated nodes: 10742, borderline nodes: 3100, overall activation: 4143.792, activation diff: 4770.602, ratio: 1.151
#   pulse 5: activated nodes: 11248, borderline nodes: 1414, overall activation: 1273.206, activation diff: 4950.604, ratio: 3.888
#   pulse 6: activated nodes: 11301, borderline nodes: 549, overall activation: 4499.693, activation diff: 4708.291, ratio: 1.046
#   pulse 7: activated nodes: 11316, borderline nodes: 479, overall activation: 5016.783, activation diff: 993.235, ratio: 0.198
#   pulse 8: activated nodes: 11316, borderline nodes: 478, overall activation: 5237.207, activation diff: 277.435, ratio: 0.053
#   pulse 9: activated nodes: 11317, borderline nodes: 472, overall activation: 5263.019, activation diff: 40.955, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 5263.0
#   number of spread. activ. pulses: 9
#   running time: 665

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999856   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989843   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   14   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   18   0.74999994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   19   0.74999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.74999994   REFERENCES:SIMDATES
