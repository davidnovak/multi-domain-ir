###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 730.285, activation diff: 714.569, ratio: 0.978
#   pulse 3: activated nodes: 8455, borderline nodes: 3470, overall activation: 1451.487, activation diff: 721.952, ratio: 0.497
#   pulse 4: activated nodes: 10601, borderline nodes: 3794, overall activation: 3599.615, activation diff: 2148.128, ratio: 0.597
#   pulse 5: activated nodes: 11223, borderline nodes: 1730, overall activation: 5291.874, activation diff: 1692.259, ratio: 0.320
#   pulse 6: activated nodes: 11305, borderline nodes: 389, overall activation: 6409.465, activation diff: 1117.592, ratio: 0.174
#   pulse 7: activated nodes: 11331, borderline nodes: 114, overall activation: 7092.052, activation diff: 682.586, ratio: 0.096
#   pulse 8: activated nodes: 11336, borderline nodes: 55, overall activation: 7494.562, activation diff: 402.510, ratio: 0.054
#   pulse 9: activated nodes: 11339, borderline nodes: 39, overall activation: 7728.130, activation diff: 233.569, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 7728.1
#   number of spread. activ. pulses: 9
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9973425   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9968413   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99681926   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99572533   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99260545   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8956984   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.8956158   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   9   0.89561236   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.8955787   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.89551735   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.89538395   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.8953718   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8952975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.8952545   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.89525235   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.8951963   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   18   0.8951813   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.89516395   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.8951279   REFERENCES:SIMDATES
