###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1222.129, activation diff: 1226.499, ratio: 1.004
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1283.029, activation diff: 852.285, ratio: 0.664
#   pulse 4: activated nodes: 10562, borderline nodes: 3763, overall activation: 3949.849, activation diff: 2752.047, ratio: 0.697
#   pulse 5: activated nodes: 11216, borderline nodes: 1960, overall activation: 5238.119, activation diff: 1290.819, ratio: 0.246
#   pulse 6: activated nodes: 11299, borderline nodes: 567, overall activation: 5782.980, activation diff: 544.861, ratio: 0.094
#   pulse 7: activated nodes: 11321, borderline nodes: 352, overall activation: 5986.294, activation diff: 203.313, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5986.3
#   number of spread. activ. pulses: 7
#   running time: 554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99906915   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99876285   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9987512   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978899   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99595827   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7991205   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.799111   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.79908985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.7990792   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.7990694   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.7990602   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.799017   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.798982   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7989793   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.79894954   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   17   0.79894143   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   18   0.79893625   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.7989302   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_34   20   0.7989036   REFERENCES:SIMDATES
