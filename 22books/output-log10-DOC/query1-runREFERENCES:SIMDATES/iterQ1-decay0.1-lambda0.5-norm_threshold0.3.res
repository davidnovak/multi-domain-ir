###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 599.308, activation diff: 586.162, ratio: 0.978
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 1174.385, activation diff: 580.415, ratio: 0.494
#   pulse 4: activated nodes: 10304, borderline nodes: 4752, overall activation: 3274.618, activation diff: 2100.234, ratio: 0.641
#   pulse 5: activated nodes: 11176, borderline nodes: 2371, overall activation: 4988.863, activation diff: 1714.245, ratio: 0.344
#   pulse 6: activated nodes: 11289, borderline nodes: 710, overall activation: 6159.301, activation diff: 1170.437, ratio: 0.190
#   pulse 7: activated nodes: 11319, borderline nodes: 284, overall activation: 6888.475, activation diff: 729.174, ratio: 0.106
#   pulse 8: activated nodes: 11327, borderline nodes: 141, overall activation: 7324.544, activation diff: 436.069, ratio: 0.060
#   pulse 9: activated nodes: 11336, borderline nodes: 73, overall activation: 7580.191, activation diff: 255.647, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 7580.2
#   number of spread. activ. pulses: 9
#   running time: 471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99773425   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9970139   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99638987   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9962218   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.995326   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99153125   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.89534754   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.89530283   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.89528275   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.895211   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.8951951   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.89510334   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.89494824   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.89487374   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.894858   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   16   0.8948442   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.8947954   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   18   0.8947557   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.8947229   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.89470816   REFERENCES:SIMDATES
