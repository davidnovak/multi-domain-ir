###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 258.526, activation diff: 245.807, ratio: 0.951
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 649.410, activation diff: 390.884, ratio: 0.602
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1405.582, activation diff: 756.172, ratio: 0.538
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2238.038, activation diff: 832.456, ratio: 0.372
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3010.955, activation diff: 772.917, ratio: 0.257
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3677.638, activation diff: 666.683, ratio: 0.181
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 4232.318, activation diff: 554.680, ratio: 0.131
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 4684.539, activation diff: 452.222, ratio: 0.097
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 5048.572, activation diff: 364.033, ratio: 0.072
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 5339.147, activation diff: 290.575, ratio: 0.054
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 5569.738, activation diff: 230.590, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5569.7
#   number of spread. activ. pulses: 12
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756146   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98399734   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.98255754   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98158187   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97859013   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9704453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7614037   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.76134187   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.76074255   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.76071477   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.76053804   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.76047003   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.76040155   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7593965   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.75939524   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.75918585   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.7591161   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.75904346   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7589754   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.75886333   REFERENCES:SIMDATES
