###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 611.654, activation diff: 595.938, ratio: 0.974
#   pulse 3: activated nodes: 8455, borderline nodes: 3470, overall activation: 1144.087, activation diff: 533.058, ratio: 0.466
#   pulse 4: activated nodes: 10590, borderline nodes: 3969, overall activation: 2540.571, activation diff: 1396.484, ratio: 0.550
#   pulse 5: activated nodes: 11209, borderline nodes: 1936, overall activation: 3619.678, activation diff: 1079.106, ratio: 0.298
#   pulse 6: activated nodes: 11263, borderline nodes: 696, overall activation: 4322.597, activation diff: 702.919, ratio: 0.163
#   pulse 7: activated nodes: 11284, borderline nodes: 541, overall activation: 4748.568, activation diff: 425.972, ratio: 0.090
#   pulse 8: activated nodes: 11289, borderline nodes: 508, overall activation: 4998.728, activation diff: 250.159, ratio: 0.050
#   pulse 9: activated nodes: 11291, borderline nodes: 494, overall activation: 5143.279, activation diff: 144.551, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11291
#   final overall activation: 5143.3
#   number of spread. activ. pulses: 9
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99734104   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9968412   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99681425   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99572134   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99259686   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7464128   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7463436   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.74633366   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7462938   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.74626327   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7461401   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.74606645   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.74605775   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   15   0.74603784   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.74602807   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.74595773   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7459469   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.74590015   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.7458675   REFERENCES:SIMDATES
