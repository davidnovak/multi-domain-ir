###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 491.391, activation diff: 473.252, ratio: 0.963
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 811.488, activation diff: 320.097, ratio: 0.394
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1338.668, activation diff: 527.180, ratio: 0.394
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1709.566, activation diff: 370.898, ratio: 0.217
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1936.146, activation diff: 226.580, ratio: 0.117
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2067.508, activation diff: 131.362, ratio: 0.064
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2141.673, activation diff: 74.165, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2141.7
#   number of spread. activ. pulses: 8
#   running time: 402

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99512625   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9944955   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99431986   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9930928   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98885673   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49546975   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.4954256   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49536324   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49530572   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.49521166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.49518284   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.49512374   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.494954   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   15   0.49492857   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.49489385   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.4948681   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.49485487   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.49483943   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.49482164   REFERENCES:SIMDATES
