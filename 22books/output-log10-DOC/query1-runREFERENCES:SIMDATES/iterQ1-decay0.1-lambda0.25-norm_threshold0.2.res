###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1579.821, activation diff: 1582.034, ratio: 1.001
#   pulse 3: activated nodes: 8648, borderline nodes: 3408, overall activation: 1937.177, activation diff: 1124.386, ratio: 0.580
#   pulse 4: activated nodes: 10816, borderline nodes: 2982, overall activation: 5309.739, activation diff: 3442.474, ratio: 0.648
#   pulse 5: activated nodes: 11258, borderline nodes: 1285, overall activation: 6980.591, activation diff: 1672.611, ratio: 0.240
#   pulse 6: activated nodes: 11327, borderline nodes: 147, overall activation: 7666.071, activation diff: 685.480, ratio: 0.089
#   pulse 7: activated nodes: 11335, borderline nodes: 52, overall activation: 7915.377, activation diff: 249.306, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 7915.4
#   number of spread. activ. pulses: 7
#   running time: 416

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.999339   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9991553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9990708   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99820584   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99654114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8992872   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8992667   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.89924335   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.89922595   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.89922035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.8991986   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8991462   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.8991418   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.8991134   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.89910525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   17   0.8990977   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_12   18   0.89909714   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   19   0.8990826   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   20   0.89908016   REFERENCES:SIMDATES
