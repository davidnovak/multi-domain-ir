###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 102.013, activation diff: 91.453, ratio: 0.896
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 291.735, activation diff: 189.766, ratio: 0.650
#   pulse 4: activated nodes: 9676, borderline nodes: 5518, overall activation: 794.587, activation diff: 502.852, ratio: 0.633
#   pulse 5: activated nodes: 10754, borderline nodes: 4014, overall activation: 1486.792, activation diff: 692.205, ratio: 0.466
#   pulse 6: activated nodes: 11094, borderline nodes: 2822, overall activation: 2221.390, activation diff: 734.598, ratio: 0.331
#   pulse 7: activated nodes: 11209, borderline nodes: 1919, overall activation: 2905.713, activation diff: 684.323, ratio: 0.236
#   pulse 8: activated nodes: 11236, borderline nodes: 1124, overall activation: 3504.125, activation diff: 598.411, ratio: 0.171
#   pulse 9: activated nodes: 11293, borderline nodes: 757, overall activation: 4009.430, activation diff: 505.305, ratio: 0.126
#   pulse 10: activated nodes: 11304, borderline nodes: 599, overall activation: 4427.222, activation diff: 417.792, ratio: 0.094
#   pulse 11: activated nodes: 11314, borderline nodes: 513, overall activation: 4768.286, activation diff: 341.064, ratio: 0.072
#   pulse 12: activated nodes: 11319, borderline nodes: 461, overall activation: 5044.174, activation diff: 275.888, ratio: 0.055
#   pulse 13: activated nodes: 11319, borderline nodes: 405, overall activation: 5265.876, activation diff: 221.702, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 5265.9
#   number of spread. activ. pulses: 13
#   running time: 549

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98866343   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9844061   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9818826   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.98161185   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9796018   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.968081   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7681908   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7679954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.76780385   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.76777506   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.7676238   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.76718944   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.76668125   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.7665497   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7665046   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.76644754   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   17   0.76601887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.7659684   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.7659554   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.76593924   REFERENCES:SIMDATES
