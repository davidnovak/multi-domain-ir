###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 187.551, activation diff: 175.508, ratio: 0.936
#   pulse 3: activated nodes: 8518, borderline nodes: 3427, overall activation: 477.536, activation diff: 289.985, ratio: 0.607
#   pulse 4: activated nodes: 10724, borderline nodes: 4269, overall activation: 1070.543, activation diff: 593.007, ratio: 0.554
#   pulse 5: activated nodes: 11180, borderline nodes: 2254, overall activation: 1753.283, activation diff: 682.740, ratio: 0.389
#   pulse 6: activated nodes: 11236, borderline nodes: 971, overall activation: 2406.630, activation diff: 653.347, ratio: 0.271
#   pulse 7: activated nodes: 11298, borderline nodes: 606, overall activation: 2980.693, activation diff: 574.063, ratio: 0.193
#   pulse 8: activated nodes: 11311, borderline nodes: 532, overall activation: 3463.958, activation diff: 483.265, ratio: 0.140
#   pulse 9: activated nodes: 11313, borderline nodes: 487, overall activation: 3861.338, activation diff: 397.380, ratio: 0.103
#   pulse 10: activated nodes: 11316, borderline nodes: 470, overall activation: 4183.474, activation diff: 322.136, ratio: 0.077
#   pulse 11: activated nodes: 11316, borderline nodes: 467, overall activation: 4442.191, activation diff: 258.717, ratio: 0.058
#   pulse 12: activated nodes: 11316, borderline nodes: 464, overall activation: 4648.626, activation diff: 206.435, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 4648.6
#   number of spread. activ. pulses: 12
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.986941   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98276913   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9808688   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98005545   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9770812   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9672679   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.71284115   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.71273816   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71220446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7120789   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7120459   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.71197903   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.71161723   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7108457   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.710783   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.71058214   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.7104732   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.7104108   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.71033865   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   20   0.7102543   REFERENCES:SIMDATES
