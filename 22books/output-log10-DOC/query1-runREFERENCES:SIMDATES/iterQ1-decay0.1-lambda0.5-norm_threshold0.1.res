###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 868.015, activation diff: 849.876, ratio: 0.979
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1756.027, activation diff: 888.012, ratio: 0.506
#   pulse 4: activated nodes: 10832, borderline nodes: 2929, overall activation: 3932.773, activation diff: 2176.746, ratio: 0.553
#   pulse 5: activated nodes: 11270, borderline nodes: 961, overall activation: 5587.166, activation diff: 1654.392, ratio: 0.296
#   pulse 6: activated nodes: 11330, borderline nodes: 150, overall activation: 6651.050, activation diff: 1063.884, ratio: 0.160
#   pulse 7: activated nodes: 11339, borderline nodes: 49, overall activation: 7289.356, activation diff: 638.306, ratio: 0.088
#   pulse 8: activated nodes: 11340, borderline nodes: 21, overall activation: 7661.095, activation diff: 371.740, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 7661.1
#   number of spread. activ. pulses: 8
#   running time: 418

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951324   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99449617   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99434626   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9931207   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9889133   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.891871   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.89177454   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8917105   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.8916446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.89150715   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.89143693   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.89128774   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.89127386   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.8911273   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.8910858   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.89108086   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   18   0.8909855   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8909806   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.8909806   REFERENCES:SIMDATES
