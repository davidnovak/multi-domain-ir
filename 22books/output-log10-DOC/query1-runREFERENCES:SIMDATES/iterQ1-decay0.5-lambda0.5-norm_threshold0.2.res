###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 413.936, activation diff: 398.220, ratio: 0.962
#   pulse 3: activated nodes: 8455, borderline nodes: 3470, overall activation: 681.994, activation diff: 268.475, ratio: 0.394
#   pulse 4: activated nodes: 8723, borderline nodes: 3669, overall activation: 1229.141, activation diff: 547.147, ratio: 0.445
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1624.982, activation diff: 395.841, ratio: 0.244
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1869.617, activation diff: 244.635, ratio: 0.131
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2012.392, activation diff: 142.774, ratio: 0.071
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2093.367, activation diff: 80.975, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2093.4
#   number of spread. activ. pulses: 8
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9957644   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9946903   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9937025   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9936917   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9925301   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98726714   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49520296   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.4951224   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49511132   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49506098   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.49490327   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.49481896   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.49468035   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.4946748   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.49461573   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.49459445   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.49453783   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.49452782   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.49452525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.4944764   REFERENCES:SIMDATES
