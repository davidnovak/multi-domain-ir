###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 145.038, activation diff: 133.715, ratio: 0.922
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 392.819, activation diff: 247.801, ratio: 0.631
#   pulse 4: activated nodes: 10232, borderline nodes: 4992, overall activation: 993.402, activation diff: 600.583, ratio: 0.605
#   pulse 5: activated nodes: 11022, borderline nodes: 3168, overall activation: 1744.168, activation diff: 750.766, ratio: 0.430
#   pulse 6: activated nodes: 11202, borderline nodes: 2036, overall activation: 2496.633, activation diff: 752.464, ratio: 0.301
#   pulse 7: activated nodes: 11239, borderline nodes: 1006, overall activation: 3176.062, activation diff: 679.429, ratio: 0.214
#   pulse 8: activated nodes: 11296, borderline nodes: 651, overall activation: 3757.861, activation diff: 581.800, ratio: 0.155
#   pulse 9: activated nodes: 11315, borderline nodes: 516, overall activation: 4241.728, activation diff: 483.866, ratio: 0.114
#   pulse 10: activated nodes: 11320, borderline nodes: 394, overall activation: 4637.306, activation diff: 395.579, ratio: 0.085
#   pulse 11: activated nodes: 11320, borderline nodes: 310, overall activation: 4957.094, activation diff: 319.788, ratio: 0.065
#   pulse 12: activated nodes: 11323, borderline nodes: 237, overall activation: 5213.613, activation diff: 256.519, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 5213.6
#   number of spread. activ. pulses: 12
#   running time: 512

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860829   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98122656   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9786567   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97821975   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9753683   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9635023   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7591385   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.75896865   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.75849867   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.75845474   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.7582899   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7580743   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.75746405   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.75707674   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.75694525   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.7568216   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.75644535   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.7563719   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.7563584   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.7562992   REFERENCES:SIMDATES
