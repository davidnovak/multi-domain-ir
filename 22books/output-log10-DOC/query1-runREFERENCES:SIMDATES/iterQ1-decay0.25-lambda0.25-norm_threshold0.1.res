###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1483.963, activation diff: 1482.334, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 1813.087, activation diff: 742.787, ratio: 0.410
#   pulse 4: activated nodes: 10845, borderline nodes: 2518, overall activation: 3926.666, activation diff: 2127.271, ratio: 0.542
#   pulse 5: activated nodes: 11278, borderline nodes: 821, overall activation: 4883.596, activation diff: 956.956, ratio: 0.196
#   pulse 6: activated nodes: 11314, borderline nodes: 483, overall activation: 5241.884, activation diff: 358.288, ratio: 0.068
#   pulse 7: activated nodes: 11319, borderline nodes: 439, overall activation: 5367.748, activation diff: 125.864, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 5367.7
#   number of spread. activ. pulses: 7
#   running time: 488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99954426   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99944973   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9993199   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984615   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9970689   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74957955   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.7495785   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7495234   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74950033   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.749495   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.74944645   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   13   0.7494379   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.74943703   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.74943227   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.749426   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.74942493   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.7494248   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7493878   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   20   0.74935836   REFERENCES:SIMDATES
