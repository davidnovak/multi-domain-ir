###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 994.858, activation diff: 993.229, ratio: 0.998
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 935.954, activation diff: 217.869, ratio: 0.233
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1780.867, activation diff: 844.914, ratio: 0.474
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2091.366, activation diff: 310.499, ratio: 0.148
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2190.164, activation diff: 98.797, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2190.2
#   number of spread. activ. pulses: 6
#   running time: 370

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988718   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99821067   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9978257   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99752843   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99674416   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9938449   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49887162   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49886745   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49877664   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.4987384   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   11   0.49861962   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.49861   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   13   0.49850655   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.49850598   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   15   0.49849147   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.49847385   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.49845678   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.498431   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4984108   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.49840975   REFERENCES:SIMDATES
