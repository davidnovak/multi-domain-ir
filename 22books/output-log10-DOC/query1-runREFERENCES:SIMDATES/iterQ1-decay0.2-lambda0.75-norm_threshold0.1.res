###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 198.956, activation diff: 186.913, ratio: 0.939
#   pulse 3: activated nodes: 8518, borderline nodes: 3427, overall activation: 514.104, activation diff: 315.148, ratio: 0.613
#   pulse 4: activated nodes: 10732, borderline nodes: 4231, overall activation: 1195.666, activation diff: 681.562, ratio: 0.570
#   pulse 5: activated nodes: 11193, borderline nodes: 2121, overall activation: 1990.073, activation diff: 794.407, ratio: 0.399
#   pulse 6: activated nodes: 11247, borderline nodes: 887, overall activation: 2753.684, activation diff: 763.611, ratio: 0.277
#   pulse 7: activated nodes: 11305, borderline nodes: 534, overall activation: 3425.667, activation diff: 671.982, ratio: 0.196
#   pulse 8: activated nodes: 11320, borderline nodes: 345, overall activation: 3992.109, activation diff: 566.442, ratio: 0.142
#   pulse 9: activated nodes: 11325, borderline nodes: 197, overall activation: 4458.473, activation diff: 466.364, ratio: 0.105
#   pulse 10: activated nodes: 11328, borderline nodes: 145, overall activation: 4836.908, activation diff: 378.435, ratio: 0.078
#   pulse 11: activated nodes: 11333, borderline nodes: 86, overall activation: 5141.007, activation diff: 304.099, ratio: 0.059
#   pulse 12: activated nodes: 11335, borderline nodes: 68, overall activation: 5383.702, activation diff: 242.696, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5383.7
#   number of spread. activ. pulses: 12
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98694146   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9827768   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9808818   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98007405   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97710115   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9673554   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7603873   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.76026493   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.75970614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.75958896   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7595507   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.75947624   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7590972   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.75833064   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.758276   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.7580663   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.7579449   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.75782067   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7577436   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.75769055   REFERENCES:SIMDATES
