###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 221.766, activation diff: 209.724, ratio: 0.946
#   pulse 3: activated nodes: 8518, borderline nodes: 3427, overall activation: 589.146, activation diff: 367.380, ratio: 0.624
#   pulse 4: activated nodes: 10746, borderline nodes: 4159, overall activation: 1470.866, activation diff: 881.720, ratio: 0.599
#   pulse 5: activated nodes: 11203, borderline nodes: 1969, overall activation: 2514.028, activation diff: 1043.162, ratio: 0.415
#   pulse 6: activated nodes: 11289, borderline nodes: 720, overall activation: 3519.449, activation diff: 1005.421, ratio: 0.286
#   pulse 7: activated nodes: 11319, borderline nodes: 297, overall activation: 4403.971, activation diff: 884.522, ratio: 0.201
#   pulse 8: activated nodes: 11330, borderline nodes: 116, overall activation: 5149.415, activation diff: 745.444, ratio: 0.145
#   pulse 9: activated nodes: 11334, borderline nodes: 64, overall activation: 5762.423, activation diff: 613.009, ratio: 0.106
#   pulse 10: activated nodes: 11338, borderline nodes: 47, overall activation: 6258.932, activation diff: 496.509, ratio: 0.079
#   pulse 11: activated nodes: 11340, borderline nodes: 26, overall activation: 6657.185, activation diff: 398.252, ratio: 0.060
#   pulse 12: activated nodes: 11340, borderline nodes: 21, overall activation: 6974.522, activation diff: 317.338, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 6974.5
#   number of spread. activ. pulses: 12
#   running time: 542

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869424   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9827895   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9809027   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.980105   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9771352   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96750295   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.85547924   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8553121   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.85471433   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8545873   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.8545577   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.8544766   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.8540584   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8532781   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.8532296   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.8530388   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.8528687   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.8526504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.85256696   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.85256344   REFERENCES:SIMDATES
