###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 999.478, activation diff: 979.613, ratio: 0.980
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2065.748, activation diff: 1066.270, ratio: 0.516
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4268.469, activation diff: 2202.721, ratio: 0.516
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5878.040, activation diff: 1609.571, ratio: 0.274
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6887.777, activation diff: 1009.737, ratio: 0.147
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7483.722, activation diff: 595.945, ratio: 0.080
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 7826.415, activation diff: 342.693, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7826.4
#   number of spread. activ. pulses: 8
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99542004   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99501544   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947801   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99356735   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9901445   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.89219964   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.89216673   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8920283   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.8919558   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.89190304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.891855   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.89180994   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.89160323   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.8915291   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.89151704   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.89150894   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.89145947   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.89143705   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.8913936   REFERENCES:SIMDATES
