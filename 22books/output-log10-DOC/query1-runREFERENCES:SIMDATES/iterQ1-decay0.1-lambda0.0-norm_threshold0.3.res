###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2327.887, activation diff: 2354.289, ratio: 1.011
#   pulse 3: activated nodes: 8569, borderline nodes: 3417, overall activation: 1134.962, activation diff: 3462.460, ratio: 3.051
#   pulse 4: activated nodes: 10743, borderline nodes: 2988, overall activation: 5916.882, activation diff: 6908.850, ratio: 1.168
#   pulse 5: activated nodes: 11263, borderline nodes: 1279, overall activation: 2535.755, activation diff: 7054.349, ratio: 2.782
#   pulse 6: activated nodes: 11330, borderline nodes: 124, overall activation: 6834.873, activation diff: 6394.529, ratio: 0.936
#   pulse 7: activated nodes: 11337, borderline nodes: 53, overall activation: 7697.151, activation diff: 1305.493, ratio: 0.170
#   pulse 8: activated nodes: 11339, borderline nodes: 22, overall activation: 7910.746, activation diff: 281.827, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 7910.7
#   number of spread. activ. pulses: 8
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999861   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998988   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   7   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   8   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_579   10   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   11   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   12   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   13   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_503   15   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   16   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   17   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   18   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_517   19   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   20   0.9   REFERENCES:SIMDATES
