###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 651.198, activation diff: 635.482, ratio: 0.976
#   pulse 3: activated nodes: 8455, borderline nodes: 3470, overall activation: 1244.247, activation diff: 593.716, ratio: 0.477
#   pulse 4: activated nodes: 10594, borderline nodes: 3900, overall activation: 2873.558, activation diff: 1629.311, ratio: 0.567
#   pulse 5: activated nodes: 11212, borderline nodes: 1873, overall activation: 4145.567, activation diff: 1272.009, ratio: 0.307
#   pulse 6: activated nodes: 11295, borderline nodes: 616, overall activation: 4979.067, activation diff: 833.500, ratio: 0.167
#   pulse 7: activated nodes: 11320, borderline nodes: 372, overall activation: 5486.285, activation diff: 507.218, ratio: 0.092
#   pulse 8: activated nodes: 11325, borderline nodes: 220, overall activation: 5785.027, activation diff: 298.742, ratio: 0.052
#   pulse 9: activated nodes: 11328, borderline nodes: 172, overall activation: 5958.037, activation diff: 173.009, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5958.0
#   number of spread. activ. pulses: 9
#   running time: 491

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9973416   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99684125   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99681616   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.995723   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9926007   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7961749   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7960999   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7960942   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7960551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7960149   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7958844   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.7958412   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7958069   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   15   0.7957764   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.795773   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.7957034   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.79568875   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.79564214   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   20   0.7956412   REFERENCES:SIMDATES
