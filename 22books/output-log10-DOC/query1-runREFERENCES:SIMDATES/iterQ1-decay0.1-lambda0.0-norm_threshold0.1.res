###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2832.601, activation diff: 2862.544, ratio: 1.011
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 1468.119, activation diff: 4245.152, ratio: 2.892
#   pulse 4: activated nodes: 10877, borderline nodes: 2225, overall activation: 6141.818, activation diff: 7158.056, ratio: 1.165
#   pulse 5: activated nodes: 11316, borderline nodes: 509, overall activation: 6771.519, activation diff: 3127.851, ratio: 0.462
#   pulse 6: activated nodes: 11338, borderline nodes: 60, overall activation: 7958.201, activation diff: 1525.090, ratio: 0.192
#   pulse 7: activated nodes: 11346, borderline nodes: 16, overall activation: 8131.561, activation diff: 220.496, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11346
#   final overall activation: 8131.6
#   number of spread. activ. pulses: 7
#   running time: 1476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998903   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991715   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990495   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   7   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   8   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   10   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   11   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   12   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   13   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   14   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   15   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_517   16   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   17   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   18   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_514   19   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_560   20   0.9   REFERENCES:SIMDATES
