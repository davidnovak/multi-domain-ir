###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1295.977, activation diff: 1322.379, ratio: 1.020
#   pulse 3: activated nodes: 8569, borderline nodes: 3417, overall activation: 94.360, activation diff: 1389.975, ratio: 14.731
#   pulse 4: activated nodes: 8837, borderline nodes: 3620, overall activation: 2063.115, activation diff: 2141.854, ratio: 1.038
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 129.906, activation diff: 2107.556, ratio: 16.224
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2111.810, activation diff: 2059.756, ratio: 0.975
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1944.841, activation diff: 244.857, ratio: 0.126
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2150.595, activation diff: 206.086, ratio: 0.096
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2150.589, activation diff: 0.353, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2150.6
#   number of spread. activ. pulses: 9
#   running time: 429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999834   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989367   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9988358   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777365   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49999923   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4999958   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49999034   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4999875   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49998346   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4999617   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49995133   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49995133   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.49994203   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.49993056   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.4999237   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.49991497   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.49991414   REFERENCES:SIMDATES
