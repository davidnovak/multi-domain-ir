###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1576.663, activation diff: 1606.605, ratio: 1.019
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 107.905, activation diff: 1656.270, ratio: 15.349
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 2169.606, activation diff: 2185.983, ratio: 1.008
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1676.478, activation diff: 617.946, ratio: 0.369
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2231.757, activation diff: 556.034, ratio: 0.249
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2231.842, activation diff: 0.695, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2231.8
#   number of spread. activ. pulses: 7
#   running time: 519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998605   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999122   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990459   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981761   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49999934   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49999654   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49999207   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49998978   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49998644   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49996865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49996015   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49996015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.4999525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.49994314   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.49993753   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.49993032   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.49992973   REFERENCES:SIMDATES
