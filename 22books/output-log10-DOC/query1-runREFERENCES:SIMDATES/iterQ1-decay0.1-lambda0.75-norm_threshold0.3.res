###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 113.006, activation diff: 102.446, ratio: 0.907
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 330.835, activation diff: 217.873, ratio: 0.659
#   pulse 4: activated nodes: 9685, borderline nodes: 5499, overall activation: 970.085, activation diff: 639.250, ratio: 0.659
#   pulse 5: activated nodes: 10845, borderline nodes: 3769, overall activation: 1873.145, activation diff: 903.060, ratio: 0.482
#   pulse 6: activated nodes: 11149, borderline nodes: 2601, overall activation: 2838.641, activation diff: 965.496, ratio: 0.340
#   pulse 7: activated nodes: 11221, borderline nodes: 1523, overall activation: 3739.247, activation diff: 900.606, ratio: 0.241
#   pulse 8: activated nodes: 11291, borderline nodes: 831, overall activation: 4525.671, activation diff: 786.424, ratio: 0.174
#   pulse 9: activated nodes: 11313, borderline nodes: 517, overall activation: 5188.885, activation diff: 663.214, ratio: 0.128
#   pulse 10: activated nodes: 11320, borderline nodes: 341, overall activation: 5736.929, activation diff: 548.045, ratio: 0.096
#   pulse 11: activated nodes: 11322, borderline nodes: 225, overall activation: 6183.620, activation diff: 446.690, ratio: 0.072
#   pulse 12: activated nodes: 11326, borderline nodes: 158, overall activation: 6544.244, activation diff: 360.625, ratio: 0.055
#   pulse 13: activated nodes: 11328, borderline nodes: 127, overall activation: 6833.456, activation diff: 289.212, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 6833.5
#   number of spread. activ. pulses: 13
#   running time: 515

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886664   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98444015   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.98194355   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9817114   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97965264   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96833646   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.86427593   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.86405325   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.86387026   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8637993   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.8636582   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.8631682   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8627051   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8625234   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.86249304   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.8624668   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.86193454   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.86191666   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.86189127   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.86180407   REFERENCES:SIMDATES
