###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2710.316, activation diff: 2741.753, ratio: 1.012
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1250.960, activation diff: 3790.314, ratio: 3.030
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 5138.773, activation diff: 5598.970, ratio: 1.090
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 6011.669, activation diff: 1540.598, ratio: 0.256
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6366.675, activation diff: 421.014, ratio: 0.066
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 6406.582, activation diff: 48.453, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6406.6
#   number of spread. activ. pulses: 7
#   running time: 480

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999901   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992496   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9991399   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835074   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_579   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   14   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_535   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_532   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMDATES
