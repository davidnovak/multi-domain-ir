###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 836.673, activation diff: 816.808, ratio: 0.976
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1616.490, activation diff: 779.817, ratio: 0.482
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3028.490, activation diff: 1412.000, ratio: 0.466
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4045.495, activation diff: 1017.006, ratio: 0.251
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4677.096, activation diff: 631.601, ratio: 0.135
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5049.184, activation diff: 372.088, ratio: 0.074
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 5263.090, activation diff: 213.905, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5263.1
#   number of spread. activ. pulses: 8
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954189   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9950154   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947761   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9935645   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99014103   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7434959   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7434665   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.74334425   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7432755   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7432494   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.7432109   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.74316895   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.7429189   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7429147   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.74290466   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.7428937   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7428579   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7428262   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.7427626   REFERENCES:SIMDATES
