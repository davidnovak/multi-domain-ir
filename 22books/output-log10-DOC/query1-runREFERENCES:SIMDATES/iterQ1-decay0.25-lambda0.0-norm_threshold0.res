###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2541.366, activation diff: 2572.803, ratio: 1.012
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1040.835, activation diff: 3425.554, ratio: 3.291
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4519.190, activation diff: 4900.526, ratio: 1.084
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5207.908, activation diff: 1258.735, ratio: 0.242
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 5501.556, activation diff: 348.315, ratio: 0.063
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5533.863, activation diff: 39.530, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5533.9
#   number of spread. activ. pulses: 7
#   running time: 648

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999005   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992484   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99913985   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835074   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
