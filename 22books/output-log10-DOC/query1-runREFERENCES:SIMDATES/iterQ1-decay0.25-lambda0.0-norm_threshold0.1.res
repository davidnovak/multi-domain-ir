###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2361.624, activation diff: 2391.567, ratio: 1.013
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 888.629, activation diff: 3205.824, ratio: 3.608
#   pulse 4: activated nodes: 10876, borderline nodes: 2299, overall activation: 4308.571, activation diff: 4949.399, ratio: 1.149
#   pulse 5: activated nodes: 11310, borderline nodes: 587, overall activation: 4434.146, activation diff: 2014.589, ratio: 0.454
#   pulse 6: activated nodes: 11318, borderline nodes: 468, overall activation: 5301.733, activation diff: 1101.300, ratio: 0.208
#   pulse 7: activated nodes: 11321, borderline nodes: 438, overall activation: 5418.345, activation diff: 145.897, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5418.3
#   number of spread. activ. pulses: 7
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998856   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999168   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990494   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
