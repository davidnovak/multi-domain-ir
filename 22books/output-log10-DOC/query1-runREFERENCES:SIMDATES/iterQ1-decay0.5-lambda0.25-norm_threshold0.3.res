###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 768.248, activation diff: 772.619, ratio: 1.006
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 586.130, activation diff: 312.760, ratio: 0.534
#   pulse 4: activated nodes: 8668, borderline nodes: 3756, overall activation: 1594.169, activation diff: 1008.196, ratio: 0.632
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1973.532, activation diff: 379.368, ratio: 0.192
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2097.218, activation diff: 123.687, ratio: 0.059
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2135.049, activation diff: 37.830, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2135.0
#   number of spread. activ. pulses: 7
#   running time: 385

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99906635   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99875164   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99875116   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978777   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9959216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49944547   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.4994267   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   9   0.49942437   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49939886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.4993563   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   12   0.4993038   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   13   0.4992574   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.49924207   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.49923643   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.49923414   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.49920973   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.49920475   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.49918628   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.4991771   REFERENCES:SIMDATES
