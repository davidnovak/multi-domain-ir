###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 502.142, activation diff: 488.996, ratio: 0.974
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 928.557, activation diff: 430.864, ratio: 0.464
#   pulse 4: activated nodes: 10280, borderline nodes: 4853, overall activation: 2307.511, activation diff: 1378.954, ratio: 0.598
#   pulse 5: activated nodes: 11147, borderline nodes: 2589, overall activation: 3408.730, activation diff: 1101.218, ratio: 0.323
#   pulse 6: activated nodes: 11234, borderline nodes: 1162, overall activation: 4148.563, activation diff: 739.834, ratio: 0.178
#   pulse 7: activated nodes: 11272, borderline nodes: 684, overall activation: 4605.161, activation diff: 456.598, ratio: 0.099
#   pulse 8: activated nodes: 11285, borderline nodes: 597, overall activation: 4876.546, activation diff: 271.385, ratio: 0.056
#   pulse 9: activated nodes: 11285, borderline nodes: 570, overall activation: 5034.847, activation diff: 158.301, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11285
#   final overall activation: 5034.8
#   number of spread. activ. pulses: 9
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977342   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9970118   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9963822   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9962213   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99531925   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9915093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74611974   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7460708   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.74604404   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.7460091   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.74599445   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.74582076   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7457628   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.7457109   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7457069   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.7456181   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.74557453   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   18   0.7455662   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.7455532   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.7455499   REFERENCES:SIMDATES
