###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1146.482, activation diff: 1150.853, ratio: 1.004
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1154.105, activation diff: 749.591, ratio: 0.649
#   pulse 4: activated nodes: 10559, borderline nodes: 3816, overall activation: 3462.263, activation diff: 2378.745, ratio: 0.687
#   pulse 5: activated nodes: 11208, borderline nodes: 2029, overall activation: 4546.835, activation diff: 1085.804, ratio: 0.239
#   pulse 6: activated nodes: 11260, borderline nodes: 660, overall activation: 4995.857, activation diff: 449.022, ratio: 0.090
#   pulse 7: activated nodes: 11290, borderline nodes: 552, overall activation: 5162.147, activation diff: 166.290, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 5162.1
#   number of spread. activ. pulses: 7
#   running time: 422

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99906886   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99876183   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9987512   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978892   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99595594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7491751   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.74916303   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.74914026   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.7491367   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.74911875   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7490982   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7490717   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7490386   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.7490338   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.749009   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   17   0.7489827   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   18   0.74897647   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.74896985   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   20   0.7489545   REFERENCES:SIMDATES
