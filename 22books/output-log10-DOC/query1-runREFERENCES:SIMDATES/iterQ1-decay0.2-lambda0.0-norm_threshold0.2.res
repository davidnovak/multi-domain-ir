###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2305.208, activation diff: 2333.470, ratio: 1.012
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 938.847, activation diff: 3238.880, ratio: 3.450
#   pulse 4: activated nodes: 10844, borderline nodes: 2631, overall activation: 4778.354, activation diff: 5600.355, ratio: 1.172
#   pulse 5: activated nodes: 11277, borderline nodes: 932, overall activation: 1981.137, activation diff: 5464.784, ratio: 2.758
#   pulse 6: activated nodes: 11325, borderline nodes: 173, overall activation: 5435.384, activation diff: 4936.697, ratio: 0.908
#   pulse 7: activated nodes: 11329, borderline nodes: 120, overall activation: 6051.457, activation diff: 904.066, ratio: 0.149
#   pulse 8: activated nodes: 11331, borderline nodes: 98, overall activation: 6189.361, activation diff: 177.171, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 6189.4
#   number of spread. activ. pulses: 8
#   running time: 431

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999874   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990827   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_532   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   20   0.8   REFERENCES:SIMDATES
