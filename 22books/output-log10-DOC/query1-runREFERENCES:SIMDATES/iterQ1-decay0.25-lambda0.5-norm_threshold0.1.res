###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 726.781, activation diff: 708.642, ratio: 0.975
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1379.266, activation diff: 652.485, ratio: 0.473
#   pulse 4: activated nodes: 10830, borderline nodes: 3079, overall activation: 2777.558, activation diff: 1398.291, ratio: 0.503
#   pulse 5: activated nodes: 11251, borderline nodes: 1106, overall activation: 3823.714, activation diff: 1046.157, ratio: 0.274
#   pulse 6: activated nodes: 11309, borderline nodes: 521, overall activation: 4487.827, activation diff: 664.112, ratio: 0.148
#   pulse 7: activated nodes: 11317, borderline nodes: 454, overall activation: 4885.126, activation diff: 397.299, ratio: 0.081
#   pulse 8: activated nodes: 11319, borderline nodes: 448, overall activation: 5116.488, activation diff: 231.362, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 5116.5
#   number of spread. activ. pulses: 8
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951307   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9944961   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99434024   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9931162   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98890626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74322236   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74314374   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7430774   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.743013   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7429208   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.74285984   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.74273205   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.7426324   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7425755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.7425438   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.74252856   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7424539   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7424401   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.7424136   REFERENCES:SIMDATES
