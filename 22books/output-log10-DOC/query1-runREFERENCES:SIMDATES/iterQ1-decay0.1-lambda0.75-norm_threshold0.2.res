###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 161.255, activation diff: 149.933, ratio: 0.930
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 448.049, activation diff: 286.813, ratio: 0.640
#   pulse 4: activated nodes: 10277, borderline nodes: 4989, overall activation: 1220.318, activation diff: 772.270, ratio: 0.633
#   pulse 5: activated nodes: 11083, borderline nodes: 2994, overall activation: 2202.550, activation diff: 982.231, ratio: 0.446
#   pulse 6: activated nodes: 11215, borderline nodes: 1667, overall activation: 3192.827, activation diff: 990.277, ratio: 0.310
#   pulse 7: activated nodes: 11291, borderline nodes: 759, overall activation: 4086.614, activation diff: 893.787, ratio: 0.219
#   pulse 8: activated nodes: 11316, borderline nodes: 430, overall activation: 4851.472, activation diff: 764.859, ratio: 0.158
#   pulse 9: activated nodes: 11321, borderline nodes: 226, overall activation: 5487.903, activation diff: 636.431, ratio: 0.116
#   pulse 10: activated nodes: 11328, borderline nodes: 130, overall activation: 6008.199, activation diff: 520.296, ratio: 0.087
#   pulse 11: activated nodes: 11332, borderline nodes: 80, overall activation: 6428.571, activation diff: 420.371, ratio: 0.065
#   pulse 12: activated nodes: 11336, borderline nodes: 61, overall activation: 6765.533, activation diff: 336.962, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 6765.5
#   number of spread. activ. pulses: 12
#   running time: 500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860848   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98124945   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.97871065   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97826797   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9754149   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9637289   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.854089   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8538703   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8533646   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8533354   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.8531568   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.8529152   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.8522445   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.85190487   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.85177463   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.8516752   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.8512269   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.8510538   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.851041   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.8509485   REFERENCES:SIMDATES
