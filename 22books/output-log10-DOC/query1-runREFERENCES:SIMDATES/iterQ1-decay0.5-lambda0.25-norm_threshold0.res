###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1097.029, activation diff: 1091.586, ratio: 0.995
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1131.707, activation diff: 172.010, ratio: 0.152
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1874.001, activation diff: 742.294, ratio: 0.396
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2145.968, activation diff: 271.967, ratio: 0.127
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2232.341, activation diff: 86.373, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2232.3
#   number of spread. activ. pulses: 6
#   running time: 364

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991119   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9986721   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9984786   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9981521   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99727386   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950042   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4991727   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.49914765   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49904597   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49900138   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.4989134   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   12   0.4989047   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49890238   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.49889803   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   15   0.4987962   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.498774   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.49876544   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   18   0.49875912   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4987364   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.49872065   REFERENCES:SIMDATES
