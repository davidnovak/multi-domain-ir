###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1696.617, activation diff: 1728.054, ratio: 1.019
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 139.048, activation diff: 1743.022, ratio: 12.535
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 2235.638, activation diff: 2162.782, ratio: 0.967
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2165.058, activation diff: 137.145, ratio: 0.063
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2268.910, activation diff: 104.007, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2268.9
#   number of spread. activ. pulses: 6
#   running time: 555

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999894   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999232   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9991393   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9983506   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4999994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49999687   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49999285   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49999076   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49998775   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49997163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49996397   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49996397   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.49995708   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.49994856   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.4999435   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.4999371   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.4999364   REFERENCES:SIMDATES
