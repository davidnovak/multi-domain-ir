###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2518.617, activation diff: 2548.559, ratio: 1.012
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 1073.876, activation diff: 3544.488, ratio: 3.301
#   pulse 4: activated nodes: 10876, borderline nodes: 2259, overall activation: 4900.580, activation diff: 5668.469, ratio: 1.157
#   pulse 5: activated nodes: 11311, borderline nodes: 548, overall activation: 5160.379, activation diff: 2383.953, ratio: 0.462
#   pulse 6: activated nodes: 11337, borderline nodes: 73, overall activation: 6148.142, activation diff: 1256.678, ratio: 0.204
#   pulse 7: activated nodes: 11339, borderline nodes: 38, overall activation: 6282.076, activation diff: 171.633, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 6282.1
#   number of spread. activ. pulses: 7
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998873   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999917   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990494   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_532   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   20   0.8   REFERENCES:SIMDATES
