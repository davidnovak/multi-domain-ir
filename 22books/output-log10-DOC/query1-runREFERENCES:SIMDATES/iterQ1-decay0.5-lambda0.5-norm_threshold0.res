###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 565.330, activation diff: 545.465, ratio: 0.965
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 933.895, activation diff: 368.565, ratio: 0.395
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1440.326, activation diff: 506.430, ratio: 0.352
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1787.653, activation diff: 347.327, ratio: 0.194
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1997.582, activation diff: 209.929, ratio: 0.105
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2118.537, activation diff: 120.954, ratio: 0.057
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2186.533, activation diff: 67.996, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2186.5
#   number of spread. activ. pulses: 8
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.995416   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9950152   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99476194   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99354863   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.990114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49565315   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49563655   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49554417   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49548554   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.4954764   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.49542043   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.49539006   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.49520582   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.49515986   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   16   0.49512938   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.49510604   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.4950847   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.49508387   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.49508178   REFERENCES:SIMDATES
