###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1405.821, activation diff: 1408.034, ratio: 1.002
#   pulse 3: activated nodes: 8648, borderline nodes: 3408, overall activation: 1604.866, activation diff: 880.971, ratio: 0.549
#   pulse 4: activated nodes: 10816, borderline nodes: 3054, overall activation: 4195.390, activation diff: 2640.173, ratio: 0.629
#   pulse 5: activated nodes: 11250, borderline nodes: 1369, overall activation: 5431.807, activation diff: 1237.218, ratio: 0.228
#   pulse 6: activated nodes: 11309, borderline nodes: 320, overall activation: 5927.754, activation diff: 495.947, ratio: 0.084
#   pulse 7: activated nodes: 11327, borderline nodes: 165, overall activation: 6108.200, activation diff: 180.446, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327
#   final overall activation: 6108.2
#   number of spread. activ. pulses: 7
#   running time: 620

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993385   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9991553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99907005   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99820554   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9965408   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79936606   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7993474   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7993216   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.7993012   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7992877   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.79926634   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.799235   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.79921746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7992092   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.7991996   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   17   0.79915553   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_60   18   0.7991513   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.7991424   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.79914224   REFERENCES:SIMDATES
