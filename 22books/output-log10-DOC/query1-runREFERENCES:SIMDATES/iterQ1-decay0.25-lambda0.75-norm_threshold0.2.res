###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 136.929, activation diff: 125.607, ratio: 0.917
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 365.901, activation diff: 228.991, ratio: 0.626
#   pulse 4: activated nodes: 10223, borderline nodes: 5023, overall activation: 891.504, activation diff: 525.604, ratio: 0.590
#   pulse 5: activated nodes: 10979, borderline nodes: 3290, overall activation: 1539.396, activation diff: 647.891, ratio: 0.421
#   pulse 6: activated nodes: 11181, borderline nodes: 2208, overall activation: 2184.348, activation diff: 644.952, ratio: 0.295
#   pulse 7: activated nodes: 11232, borderline nodes: 1222, overall activation: 2765.140, activation diff: 580.792, ratio: 0.210
#   pulse 8: activated nodes: 11281, borderline nodes: 774, overall activation: 3262.039, activation diff: 496.899, ratio: 0.152
#   pulse 9: activated nodes: 11295, borderline nodes: 632, overall activation: 3675.075, activation diff: 413.035, ratio: 0.112
#   pulse 10: activated nodes: 11305, borderline nodes: 592, overall activation: 4012.570, activation diff: 337.496, ratio: 0.084
#   pulse 11: activated nodes: 11305, borderline nodes: 550, overall activation: 4285.373, activation diff: 272.803, ratio: 0.064
#   pulse 12: activated nodes: 11306, borderline nodes: 538, overall activation: 4504.237, activation diff: 218.864, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11306
#   final overall activation: 4504.2
#   number of spread. activ. pulses: 12
#   running time: 538

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98608184   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98121274   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.97862387   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97819054   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9753409   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96337044   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.71166325   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.71151197   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71106845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7110005   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.7108599   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7106533   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7100754   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.70965004   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.709513   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.709398   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   17   0.70908594   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7090454   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.70904297   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.7089702   REFERENCES:SIMDATES
