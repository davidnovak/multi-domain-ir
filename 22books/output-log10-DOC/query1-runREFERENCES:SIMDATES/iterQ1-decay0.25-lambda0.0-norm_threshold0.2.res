###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2161.532, activation diff: 2189.795, ratio: 1.013
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 774.927, activation diff: 2931.583, ratio: 3.783
#   pulse 4: activated nodes: 10842, borderline nodes: 2668, overall activation: 4200.485, activation diff: 4889.519, ratio: 1.164
#   pulse 5: activated nodes: 11271, borderline nodes: 971, overall activation: 1545.651, activation diff: 4818.053, ratio: 3.117
#   pulse 6: activated nodes: 11313, borderline nodes: 500, overall activation: 4673.397, activation diff: 4434.385, ratio: 0.949
#   pulse 7: activated nodes: 11318, borderline nodes: 451, overall activation: 5203.146, activation diff: 803.229, ratio: 0.154
#   pulse 8: activated nodes: 11319, borderline nodes: 449, overall activation: 5331.412, activation diff: 164.649, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 5331.4
#   number of spread. activ. pulses: 8
#   running time: 741

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999873   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990815   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
