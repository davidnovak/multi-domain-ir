###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1581.784, activation diff: 1580.155, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 2007.357, activation diff: 866.497, ratio: 0.432
#   pulse 4: activated nodes: 10849, borderline nodes: 2480, overall activation: 4483.383, activation diff: 2493.087, ratio: 0.556
#   pulse 5: activated nodes: 11282, borderline nodes: 779, overall activation: 5628.806, activation diff: 1145.480, ratio: 0.204
#   pulse 6: activated nodes: 11331, borderline nodes: 129, overall activation: 6066.356, activation diff: 437.550, ratio: 0.072
#   pulse 7: activated nodes: 11337, borderline nodes: 56, overall activation: 6220.825, activation diff: 154.469, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 6220.8
#   number of spread. activ. pulses: 7
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99954444   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99944973   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99932027   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984616   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99706894   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79955244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.7995507   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7994947   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7994672   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.79946685   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.79941535   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.7994118   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7994057   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7994033   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.79939663   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.7993947   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.7993884   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7993485   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   20   0.79933673   REFERENCES:SIMDATES
