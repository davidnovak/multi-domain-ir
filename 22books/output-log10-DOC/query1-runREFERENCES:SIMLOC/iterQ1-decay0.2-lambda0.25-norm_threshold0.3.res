###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1227.473, activation diff: 1230.140, ratio: 1.002
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 1422.516, activation diff: 867.757, ratio: 0.610
#   pulse 4: activated nodes: 11251, borderline nodes: 4302, overall activation: 4945.321, activation diff: 3550.768, ratio: 0.718
#   pulse 5: activated nodes: 11402, borderline nodes: 734, overall activation: 6596.833, activation diff: 1651.793, ratio: 0.250
#   pulse 6: activated nodes: 11439, borderline nodes: 134, overall activation: 7211.332, activation diff: 614.499, ratio: 0.085
#   pulse 7: activated nodes: 11448, borderline nodes: 56, overall activation: 7420.767, activation diff: 209.435, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7420.8
#   number of spread. activ. pulses: 7
#   running time: 1197

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992805   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99913156   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99851894   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99793583   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961205   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.7992997   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.7991954   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7991908   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.79918087   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   11   0.7991693   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.799165   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.79916394   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.7991339   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7991022   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.7990869   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.7990849   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7990663   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.7990441   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.79903823   REFERENCES:SIMLOC
