###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2649.757, activation diff: 2676.356, ratio: 1.010
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2181.063, activation diff: 3407.953, ratio: 1.563
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 6629.639, activation diff: 4739.730, ratio: 0.715
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 7005.094, activation diff: 401.693, ratio: 0.057
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 7059.003, activation diff: 55.593, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7059.0
#   number of spread. activ. pulses: 6
#   running time: 1177

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999922   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992514   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9991399   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835074   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_26   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.75   REFERENCES:SIMLOC
