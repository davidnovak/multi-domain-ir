###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 96.624, activation diff: 86.064, ratio: 0.891
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 276.754, activation diff: 180.171, ratio: 0.651
#   pulse 4: activated nodes: 10714, borderline nodes: 6515, overall activation: 757.804, activation diff: 481.049, ratio: 0.635
#   pulse 5: activated nodes: 11163, borderline nodes: 4189, overall activation: 1454.546, activation diff: 696.742, ratio: 0.479
#   pulse 6: activated nodes: 11322, borderline nodes: 2198, overall activation: 2269.322, activation diff: 814.776, ratio: 0.359
#   pulse 7: activated nodes: 11399, borderline nodes: 943, overall activation: 3077.347, activation diff: 808.025, ratio: 0.263
#   pulse 8: activated nodes: 11416, borderline nodes: 486, overall activation: 3794.110, activation diff: 716.763, ratio: 0.189
#   pulse 9: activated nodes: 11425, borderline nodes: 263, overall activation: 4392.543, activation diff: 598.433, ratio: 0.136
#   pulse 10: activated nodes: 11429, borderline nodes: 185, overall activation: 4877.996, activation diff: 485.454, ratio: 0.100
#   pulse 11: activated nodes: 11434, borderline nodes: 155, overall activation: 5265.631, activation diff: 387.634, ratio: 0.074
#   pulse 12: activated nodes: 11435, borderline nodes: 137, overall activation: 5572.237, activation diff: 306.606, ratio: 0.055
#   pulse 13: activated nodes: 11437, borderline nodes: 128, overall activation: 5813.318, activation diff: 241.082, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 5813.3
#   number of spread. activ. pulses: 13
#   running time: 1489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886769   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9845252   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9821943   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9814236   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9795892   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9684572   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7201452   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7200066   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.71984756   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.71983254   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7197914   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7192903   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.71874005   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.71873283   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7187052   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7185303   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.71825504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.71819246   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.7181851   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7181311   REFERENCES:SIMLOC
