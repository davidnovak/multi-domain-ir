###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 161.445, activation diff: 150.123, ratio: 0.930
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 454.460, activation diff: 293.031, ratio: 0.645
#   pulse 4: activated nodes: 11080, borderline nodes: 5627, overall activation: 1286.082, activation diff: 831.621, ratio: 0.647
#   pulse 5: activated nodes: 11320, borderline nodes: 2204, overall activation: 2458.181, activation diff: 1172.099, ratio: 0.477
#   pulse 6: activated nodes: 11404, borderline nodes: 748, overall activation: 3704.759, activation diff: 1246.578, ratio: 0.336
#   pulse 7: activated nodes: 11433, borderline nodes: 270, overall activation: 4829.936, activation diff: 1125.177, ratio: 0.233
#   pulse 8: activated nodes: 11444, borderline nodes: 95, overall activation: 5770.846, activation diff: 940.910, ratio: 0.163
#   pulse 9: activated nodes: 11447, borderline nodes: 53, overall activation: 6531.581, activation diff: 760.736, ratio: 0.116
#   pulse 10: activated nodes: 11450, borderline nodes: 31, overall activation: 7136.225, activation diff: 604.644, ratio: 0.085
#   pulse 11: activated nodes: 11451, borderline nodes: 18, overall activation: 7612.060, activation diff: 475.835, ratio: 0.063
#   pulse 12: activated nodes: 11452, borderline nodes: 14, overall activation: 7984.210, activation diff: 372.150, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7984.2
#   number of spread. activ. pulses: 12
#   running time: 1412

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860988   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98138666   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.978621   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9785926   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97543347   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96417373   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8540642   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.8539145   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.85342777   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.85338485   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8533317   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8529876   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8523236   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8519578   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.8519292   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8517202   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.85140604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.8511454   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.8510926   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.851054   REFERENCES:SIMLOC
