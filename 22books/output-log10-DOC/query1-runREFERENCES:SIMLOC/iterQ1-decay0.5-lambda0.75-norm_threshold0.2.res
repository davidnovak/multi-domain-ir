###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 96.542, activation diff: 85.219, ratio: 0.883
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 245.370, activation diff: 148.844, ratio: 0.607
#   pulse 4: activated nodes: 10443, borderline nodes: 5446, overall activation: 539.937, activation diff: 294.567, ratio: 0.546
#   pulse 5: activated nodes: 10699, borderline nodes: 4521, overall activation: 905.941, activation diff: 366.005, ratio: 0.404
#   pulse 6: activated nodes: 10803, borderline nodes: 3957, overall activation: 1297.483, activation diff: 391.542, ratio: 0.302
#   pulse 7: activated nodes: 10861, borderline nodes: 3670, overall activation: 1676.761, activation diff: 379.278, ratio: 0.226
#   pulse 8: activated nodes: 10886, borderline nodes: 3589, overall activation: 2019.585, activation diff: 342.824, ratio: 0.170
#   pulse 9: activated nodes: 10887, borderline nodes: 3549, overall activation: 2312.213, activation diff: 292.628, ratio: 0.127
#   pulse 10: activated nodes: 10889, borderline nodes: 3531, overall activation: 2552.900, activation diff: 240.687, ratio: 0.094
#   pulse 11: activated nodes: 10894, borderline nodes: 3524, overall activation: 2746.882, activation diff: 193.982, ratio: 0.071
#   pulse 12: activated nodes: 10895, borderline nodes: 3519, overall activation: 2901.428, activation diff: 154.546, ratio: 0.053
#   pulse 13: activated nodes: 10895, borderline nodes: 3519, overall activation: 3023.657, activation diff: 122.230, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3023.7
#   number of spread. activ. pulses: 13
#   running time: 1506

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98956585   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98593676   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9837555   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.98366356   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.981002   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97161597   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.48073918   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.48064622   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.48053843   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.4804584   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.48029158   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.480276   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.48001462   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.4796821   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.47950342   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   16   0.4794715   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.47945988   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.47945902   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.479374   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.47937307   REFERENCES:SIMLOC
