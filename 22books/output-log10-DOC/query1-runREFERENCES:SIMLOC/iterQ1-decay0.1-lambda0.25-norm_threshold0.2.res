###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1594.432, activation diff: 1594.429, ratio: 1.000
#   pulse 3: activated nodes: 9448, borderline nodes: 3921, overall activation: 2180.840, activation diff: 1157.213, ratio: 0.531
#   pulse 4: activated nodes: 11311, borderline nodes: 3149, overall activation: 6534.522, activation diff: 4363.824, ratio: 0.668
#   pulse 5: activated nodes: 11424, borderline nodes: 368, overall activation: 8360.952, activation diff: 1826.506, ratio: 0.218
#   pulse 6: activated nodes: 11448, borderline nodes: 48, overall activation: 8989.482, activation diff: 628.530, ratio: 0.070
#   pulse 7: activated nodes: 11453, borderline nodes: 12, overall activation: 9194.489, activation diff: 205.006, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 9194.5
#   number of spread. activ. pulses: 7
#   running time: 1197

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9994973   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99936855   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.998982   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982424   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967107   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.8994126   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.89936936   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.8993311   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.89932925   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.89932287   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.89931154   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.89930874   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.899259   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.8992561   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.89923155   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.8992282   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.8992119   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.8991816   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.8991784   REFERENCES:SIMLOC
