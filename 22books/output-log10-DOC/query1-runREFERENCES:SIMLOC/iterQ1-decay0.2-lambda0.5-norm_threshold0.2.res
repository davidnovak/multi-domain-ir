###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 654.277, activation diff: 638.212, ratio: 0.975
#   pulse 3: activated nodes: 9228, borderline nodes: 4101, overall activation: 1310.757, activation diff: 656.793, ratio: 0.501
#   pulse 4: activated nodes: 11252, borderline nodes: 4296, overall activation: 3386.051, activation diff: 2075.294, ratio: 0.613
#   pulse 5: activated nodes: 11407, borderline nodes: 745, overall activation: 5095.329, activation diff: 1709.278, ratio: 0.335
#   pulse 6: activated nodes: 11438, borderline nodes: 161, overall activation: 6174.345, activation diff: 1079.016, ratio: 0.175
#   pulse 7: activated nodes: 11447, borderline nodes: 61, overall activation: 6797.520, activation diff: 623.175, ratio: 0.092
#   pulse 8: activated nodes: 11449, borderline nodes: 36, overall activation: 7146.494, activation diff: 348.974, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7146.5
#   number of spread. activ. pulses: 8
#   running time: 1211

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99578035   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9948365   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9940717   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.993513   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9925972   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9876782   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.7923523   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7923173   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7922907   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.79227865   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7921257   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7918563   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.7918397   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79173803   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7916962   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7916373   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7916026   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.79157233   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.7915532   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.79153985   REFERENCES:SIMLOC
