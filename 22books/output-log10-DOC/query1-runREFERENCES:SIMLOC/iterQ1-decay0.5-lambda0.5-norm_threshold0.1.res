###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 497.485, activation diff: 479.170, ratio: 0.963
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 916.837, activation diff: 419.352, ratio: 0.457
#   pulse 4: activated nodes: 10795, borderline nodes: 4013, overall activation: 1758.645, activation diff: 841.807, ratio: 0.479
#   pulse 5: activated nodes: 10886, borderline nodes: 3570, overall activation: 2446.239, activation diff: 687.594, ratio: 0.281
#   pulse 6: activated nodes: 10894, borderline nodes: 3520, overall activation: 2892.502, activation diff: 446.263, ratio: 0.154
#   pulse 7: activated nodes: 10897, borderline nodes: 3516, overall activation: 3153.880, activation diff: 261.378, ratio: 0.083
#   pulse 8: activated nodes: 10897, borderline nodes: 3514, overall activation: 3301.721, activation diff: 147.841, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 3301.7
#   number of spread. activ. pulses: 8
#   running time: 1232

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99592316   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99522173   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99458003   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99437594   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99311817   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9891299   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49546006   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49545988   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49541605   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49540955   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49529362   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.49527878   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49519247   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.49510098   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49506485   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49500313   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.4949818   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.4949789   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.4949658   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.49494994   REFERENCES:SIMLOC
