###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1462.984, activation diff: 1489.007, ratio: 1.018
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 429.067, activation diff: 1819.747, ratio: 4.241
#   pulse 4: activated nodes: 10798, borderline nodes: 3951, overall activation: 3074.201, activation diff: 2863.150, ratio: 0.931
#   pulse 5: activated nodes: 10891, borderline nodes: 3556, overall activation: 3167.195, activation diff: 505.200, ratio: 0.160
#   pulse 6: activated nodes: 10893, borderline nodes: 3522, overall activation: 3440.437, activation diff: 274.908, ratio: 0.080
#   pulse 7: activated nodes: 10896, borderline nodes: 3517, overall activation: 3457.468, activation diff: 17.198, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 3457.5
#   number of spread. activ. pulses: 7
#   running time: 1184

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999046   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99894935   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999985   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49999955   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4999991   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49999833   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.4999974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999735   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999717   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4999968   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999604   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999586   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999478   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49999356   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.4999932   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49999207   REFERENCES:SIMLOC
