###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1511.211, activation diff: 1507.385, ratio: 0.997
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2167.874, activation diff: 913.185, ratio: 0.421
#   pulse 4: activated nodes: 11360, borderline nodes: 2414, overall activation: 5121.741, activation diff: 2954.451, ratio: 0.577
#   pulse 5: activated nodes: 11434, borderline nodes: 229, overall activation: 6303.557, activation diff: 1181.816, ratio: 0.187
#   pulse 6: activated nodes: 11444, borderline nodes: 71, overall activation: 6697.440, activation diff: 393.882, ratio: 0.059
#   pulse 7: activated nodes: 11448, borderline nodes: 53, overall activation: 6824.151, activation diff: 126.711, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 6824.2
#   number of spread. activ. pulses: 7
#   running time: 1162

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99963975   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995184   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99934214   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848896   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9972005   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7496355   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.74960935   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.74959064   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7495844   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7495731   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.7495668   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7495655   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7495124   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.74950147   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7494982   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   17   0.74948394   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.7494738   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.749471   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.7494688   REFERENCES:SIMLOC
