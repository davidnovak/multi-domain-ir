###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 113.114, activation diff: 102.554, ratio: 0.907
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 334.585, activation diff: 221.512, ratio: 0.662
#   pulse 4: activated nodes: 10758, borderline nodes: 6515, overall activation: 1004.459, activation diff: 669.874, ratio: 0.667
#   pulse 5: activated nodes: 11200, borderline nodes: 3642, overall activation: 2023.408, activation diff: 1018.949, ratio: 0.504
#   pulse 6: activated nodes: 11345, borderline nodes: 1656, overall activation: 3222.859, activation diff: 1199.451, ratio: 0.372
#   pulse 7: activated nodes: 11409, borderline nodes: 615, overall activation: 4375.834, activation diff: 1152.975, ratio: 0.263
#   pulse 8: activated nodes: 11433, borderline nodes: 252, overall activation: 5371.925, activation diff: 996.091, ratio: 0.185
#   pulse 9: activated nodes: 11442, borderline nodes: 105, overall activation: 6191.196, activation diff: 819.271, ratio: 0.132
#   pulse 10: activated nodes: 11446, borderline nodes: 70, overall activation: 6849.248, activation diff: 658.052, ratio: 0.096
#   pulse 11: activated nodes: 11447, borderline nodes: 47, overall activation: 7370.967, activation diff: 521.718, ratio: 0.071
#   pulse 12: activated nodes: 11450, borderline nodes: 29, overall activation: 7781.343, activation diff: 410.376, ratio: 0.053
#   pulse 13: activated nodes: 11451, borderline nodes: 21, overall activation: 8102.479, activation diff: 321.136, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 8102.5
#   number of spread. activ. pulses: 13
#   running time: 1446

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886815   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98457366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.98227036   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9815894   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97966844   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96879786   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.86425537   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.8640952   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8639147   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8638561   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8638148   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8632326   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8627519   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8626593   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.8625428   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8625358   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8620754   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.8620347   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8620234   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.86182207   REFERENCES:SIMLOC
