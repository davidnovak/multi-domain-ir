###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 783.299, activation diff: 764.984, ratio: 0.977
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1624.032, activation diff: 840.732, ratio: 0.518
#   pulse 4: activated nodes: 11320, borderline nodes: 3003, overall activation: 3788.370, activation diff: 2164.339, ratio: 0.571
#   pulse 5: activated nodes: 11428, borderline nodes: 333, overall activation: 5426.094, activation diff: 1637.724, ratio: 0.302
#   pulse 6: activated nodes: 11447, borderline nodes: 63, overall activation: 6416.287, activation diff: 990.193, ratio: 0.154
#   pulse 7: activated nodes: 11451, borderline nodes: 31, overall activation: 6977.076, activation diff: 560.789, ratio: 0.080
#   pulse 8: activated nodes: 11453, borderline nodes: 17, overall activation: 7287.388, activation diff: 310.313, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 7287.4
#   number of spread. activ. pulses: 8
#   running time: 1237

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959232   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9952261   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9945886   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99437666   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931412   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9891578   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.792753   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7927519   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7926906   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.79267085   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.79253674   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7924595   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7923247   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.792226   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.79222286   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7921747   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.79215395   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.792051   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7920157   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.79201543   REFERENCES:SIMLOC
