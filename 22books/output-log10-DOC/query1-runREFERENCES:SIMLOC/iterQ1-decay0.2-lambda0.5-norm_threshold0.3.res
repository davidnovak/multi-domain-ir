###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 535.754, activation diff: 522.147, ratio: 0.975
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 1051.868, activation diff: 519.605, ratio: 0.494
#   pulse 4: activated nodes: 11160, borderline nodes: 5585, overall activation: 3003.643, activation diff: 1951.775, ratio: 0.650
#   pulse 5: activated nodes: 11376, borderline nodes: 1250, overall activation: 4755.486, activation diff: 1751.843, ratio: 0.368
#   pulse 6: activated nodes: 11425, borderline nodes: 357, overall activation: 5927.362, activation diff: 1171.875, ratio: 0.198
#   pulse 7: activated nodes: 11442, borderline nodes: 121, overall activation: 6621.312, activation diff: 693.950, ratio: 0.105
#   pulse 8: activated nodes: 11447, borderline nodes: 67, overall activation: 7015.218, activation diff: 393.906, ratio: 0.056
#   pulse 9: activated nodes: 11449, borderline nodes: 47, overall activation: 7235.195, activation diff: 219.977, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7235.2
#   number of spread. activ. pulses: 9
#   running time: 1299

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977495   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9971149   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.996613   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99608886   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9953399   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99170893   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.79591155   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.79586077   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7958393   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.79578876   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7957834   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7956215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.79554343   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7955406   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7954881   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.79544663   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.79541093   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   18   0.7954068   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.7953531   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.79534125   REFERENCES:SIMLOC
