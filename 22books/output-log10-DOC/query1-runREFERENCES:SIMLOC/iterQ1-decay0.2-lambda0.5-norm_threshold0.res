###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 920.124, activation diff: 900.260, ratio: 0.978
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1994.557, activation diff: 1074.432, ratio: 0.539
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4240.093, activation diff: 2245.537, ratio: 0.530
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5808.432, activation diff: 1568.339, ratio: 0.270
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6728.877, activation diff: 920.445, ratio: 0.137
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 7242.935, activation diff: 514.058, ratio: 0.071
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 7524.734, activation diff: 281.799, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7524.7
#   number of spread. activ. pulses: 8
#   running time: 1171

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99548435   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9949564   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99493754   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935844   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9903424   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.793079   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.79305613   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.79294753   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.7929386   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7928785   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.79284763   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7927896   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.7926047   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.79259324   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7925345   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7925035   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.79244214   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.79242575   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7923633   REFERENCES:SIMLOC
