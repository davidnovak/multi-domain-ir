###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 614.571, activation diff: 598.507, ratio: 0.974
#   pulse 3: activated nodes: 9228, borderline nodes: 4101, overall activation: 1212.188, activation diff: 597.910, ratio: 0.493
#   pulse 4: activated nodes: 11250, borderline nodes: 4356, overall activation: 3022.160, activation diff: 1809.971, ratio: 0.599
#   pulse 5: activated nodes: 11401, borderline nodes: 797, overall activation: 4531.699, activation diff: 1509.540, ratio: 0.333
#   pulse 6: activated nodes: 11428, borderline nodes: 220, overall activation: 5492.280, activation diff: 960.581, ratio: 0.175
#   pulse 7: activated nodes: 11438, borderline nodes: 119, overall activation: 6049.121, activation diff: 556.841, ratio: 0.092
#   pulse 8: activated nodes: 11441, borderline nodes: 89, overall activation: 6361.616, activation diff: 312.495, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 6361.6
#   number of spread. activ. pulses: 8
#   running time: 1245

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99578035   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99483573   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9940702   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99351287   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9925943   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98767394   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.74282616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7427959   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7427699   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.7427602   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74261546   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.74236435   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.7423192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7422488   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.74220175   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7421583   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.74211913   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   18   0.7420811   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7420695   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.74204767   REFERENCES:SIMLOC
