###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1331.193, activation diff: 1331.190, ratio: 1.000
#   pulse 3: activated nodes: 9448, borderline nodes: 3921, overall activation: 1688.498, activation diff: 841.572, ratio: 0.498
#   pulse 4: activated nodes: 11305, borderline nodes: 3287, overall activation: 4746.193, activation diff: 3064.506, ratio: 0.646
#   pulse 5: activated nodes: 11420, borderline nodes: 455, overall activation: 6079.146, activation diff: 1332.979, ratio: 0.219
#   pulse 6: activated nodes: 11437, borderline nodes: 127, overall activation: 6546.939, activation diff: 467.794, ratio: 0.071
#   pulse 7: activated nodes: 11441, borderline nodes: 74, overall activation: 6702.032, activation diff: 155.093, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 6702.0
#   number of spread. activ. pulses: 7
#   running time: 1214

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99949694   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993684   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.998982   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982421   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99671066   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.74950385   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7494698   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7494358   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7494319   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.7494278   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.749419   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.74937934   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.74936664   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7493503   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.74933857   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.74932206   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.74929976   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.7492979   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   20   0.7492919   REFERENCES:SIMLOC
