###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2344.320, activation diff: 2369.218, ratio: 1.011
#   pulse 3: activated nodes: 9348, borderline nodes: 3988, overall activation: 1297.575, activation diff: 3584.995, ratio: 2.763
#   pulse 4: activated nodes: 11299, borderline nodes: 3302, overall activation: 7850.596, activation diff: 7681.590, ratio: 0.978
#   pulse 5: activated nodes: 11425, borderline nodes: 381, overall activation: 8458.770, activation diff: 1765.642, ratio: 0.209
#   pulse 6: activated nodes: 11447, borderline nodes: 46, overall activation: 9184.808, activation diff: 755.648, ratio: 0.082
#   pulse 7: activated nodes: 11451, borderline nodes: 12, overall activation: 9232.972, activation diff: 52.277, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9233.0
#   number of spread. activ. pulses: 7
#   running time: 1203

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   14   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   16   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   18   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   19   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   20   0.9   REFERENCES:SIMLOC
