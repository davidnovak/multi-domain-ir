###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 864.065, activation diff: 844.201, ratio: 0.977
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1838.893, activation diff: 974.827, ratio: 0.530
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3820.404, activation diff: 1981.511, ratio: 0.519
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5220.339, activation diff: 1399.935, ratio: 0.268
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 6046.786, activation diff: 826.446, ratio: 0.137
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6510.095, activation diff: 463.309, ratio: 0.071
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 6764.820, activation diff: 254.725, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6764.8
#   number of spread. activ. pulses: 8
#   running time: 1228

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.995484   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9949559   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99493754   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935834   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99034184   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7435088   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7434883   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.74338615   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.74337673   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7433211   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.7432914   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7432375   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.7430576   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7430506   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.74297726   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.742962   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7429087   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7428924   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7428178   REFERENCES:SIMLOC
