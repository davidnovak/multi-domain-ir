###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 69.142, activation diff: 58.582, ratio: 0.847
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 185.568, activation diff: 116.466, ratio: 0.628
#   pulse 4: activated nodes: 9948, borderline nodes: 5983, overall activation: 439.386, activation diff: 253.818, ratio: 0.578
#   pulse 5: activated nodes: 10575, borderline nodes: 4915, overall activation: 769.457, activation diff: 330.071, ratio: 0.429
#   pulse 6: activated nodes: 10732, borderline nodes: 4448, overall activation: 1135.012, activation diff: 365.555, ratio: 0.322
#   pulse 7: activated nodes: 10804, borderline nodes: 3946, overall activation: 1500.955, activation diff: 365.943, ratio: 0.244
#   pulse 8: activated nodes: 10856, borderline nodes: 3698, overall activation: 1845.098, activation diff: 344.143, ratio: 0.187
#   pulse 9: activated nodes: 10878, borderline nodes: 3622, overall activation: 2151.403, activation diff: 306.305, ratio: 0.142
#   pulse 10: activated nodes: 10887, borderline nodes: 3575, overall activation: 2410.987, activation diff: 259.584, ratio: 0.108
#   pulse 11: activated nodes: 10887, borderline nodes: 3554, overall activation: 2624.000, activation diff: 213.013, ratio: 0.081
#   pulse 12: activated nodes: 10887, borderline nodes: 3541, overall activation: 2795.697, activation diff: 171.697, ratio: 0.061
#   pulse 13: activated nodes: 10890, borderline nodes: 3534, overall activation: 2932.695, activation diff: 136.998, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10890
#   final overall activation: 2932.7
#   number of spread. activ. pulses: 13
#   running time: 1344

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886637   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9843869   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9819752   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.98095965   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9793583   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96756405   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.4799784   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.4798504   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.47981042   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.47978058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.47959226   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.47940898   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.47901252   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47888768   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   15   0.47879007   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.47864226   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.47862476   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.4786149   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.47859868   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.47858673   REFERENCES:SIMLOC
