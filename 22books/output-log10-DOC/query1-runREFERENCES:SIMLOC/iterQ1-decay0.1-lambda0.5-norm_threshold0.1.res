###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 878.571, activation diff: 860.255, ratio: 0.979
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1878.367, activation diff: 999.796, ratio: 0.532
#   pulse 4: activated nodes: 11321, borderline nodes: 2925, overall activation: 4620.671, activation diff: 2742.304, ratio: 0.593
#   pulse 5: activated nodes: 11429, borderline nodes: 293, overall activation: 6643.613, activation diff: 2022.942, ratio: 0.304
#   pulse 6: activated nodes: 11449, borderline nodes: 45, overall activation: 7851.230, activation diff: 1207.617, ratio: 0.154
#   pulse 7: activated nodes: 11453, borderline nodes: 13, overall activation: 8529.614, activation diff: 678.384, ratio: 0.080
#   pulse 8: activated nodes: 11454, borderline nodes: 7, overall activation: 8902.456, activation diff: 372.842, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8902.5
#   number of spread. activ. pulses: 8
#   running time: 1237

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959232   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.995227   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9945899   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99437666   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931437   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98915976   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89185005   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89184976   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8917849   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.89175963   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8916091   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8915197   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8913698   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.8913058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.891261   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.89121544   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.8911896   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.89107215   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.891067   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.89106363   REFERENCES:SIMLOC
