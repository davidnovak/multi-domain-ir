###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2627.356, activation diff: 2653.380, ratio: 1.010
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 1618.511, activation diff: 4031.160, ratio: 2.491
#   pulse 4: activated nodes: 11328, borderline nodes: 2621, overall activation: 8118.930, activation diff: 7531.152, ratio: 0.928
#   pulse 5: activated nodes: 11435, borderline nodes: 245, overall activation: 8950.162, activation diff: 1235.902, ratio: 0.138
#   pulse 6: activated nodes: 11449, borderline nodes: 29, overall activation: 9265.380, activation diff: 326.787, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 9265.4
#   number of spread. activ. pulses: 6
#   running time: 1420

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999905   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   14   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   15   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   16   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   17   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   18   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   19   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_546   20   0.9   REFERENCES:SIMLOC
