###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1769.688, activation diff: 1796.288, ratio: 1.015
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 967.106, activation diff: 1913.954, ratio: 1.979
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 3357.815, activation diff: 2444.489, ratio: 0.728
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 3474.842, activation diff: 128.273, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3474.8
#   number of spread. activ. pulses: 5
#   running time: 1100

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99998987   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998677   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.999925   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989248   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9982559   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999982   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49999955   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49999905   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49999815   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49999747   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49999744   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999738   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49999657   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999642   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999583   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999523   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.49999443   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4999942   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49999264   REFERENCES:SIMLOC
