###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 102.121, activation diff: 91.561, ratio: 0.897
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 295.780, activation diff: 193.700, ratio: 0.655
#   pulse 4: activated nodes: 10735, borderline nodes: 6517, overall activation: 834.692, activation diff: 538.912, ratio: 0.646
#   pulse 5: activated nodes: 11184, borderline nodes: 4005, overall activation: 1630.067, activation diff: 795.375, ratio: 0.488
#   pulse 6: activated nodes: 11336, borderline nodes: 1984, overall activation: 2566.725, activation diff: 936.658, ratio: 0.365
#   pulse 7: activated nodes: 11404, borderline nodes: 819, overall activation: 3486.807, activation diff: 920.082, ratio: 0.264
#   pulse 8: activated nodes: 11420, borderline nodes: 385, overall activation: 4295.247, activation diff: 808.440, ratio: 0.188
#   pulse 9: activated nodes: 11429, borderline nodes: 186, overall activation: 4966.830, activation diff: 671.583, ratio: 0.135
#   pulse 10: activated nodes: 11440, borderline nodes: 122, overall activation: 5509.944, activation diff: 543.114, ratio: 0.099
#   pulse 11: activated nodes: 11444, borderline nodes: 99, overall activation: 5942.725, activation diff: 432.781, ratio: 0.073
#   pulse 12: activated nodes: 11445, borderline nodes: 82, overall activation: 6284.561, activation diff: 341.836, ratio: 0.054
#   pulse 13: activated nodes: 11448, borderline nodes: 63, overall activation: 6553.045, activation diff: 268.484, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 6553.0
#   number of spread. activ. pulses: 13
#   running time: 1455

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886786   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9845433   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9822228   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9814852   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9796187   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96858144   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.76818144   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.76803917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.76786363   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.76784956   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.767798   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7672704   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7667437   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.766713   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.76664966   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7665286   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7661936   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7661373   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7660958   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.7660641   REFERENCES:SIMLOC
