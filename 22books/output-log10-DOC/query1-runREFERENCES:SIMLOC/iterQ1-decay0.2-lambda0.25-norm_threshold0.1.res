###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1610.765, activation diff: 1606.939, ratio: 0.998
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2369.869, activation diff: 1029.561, ratio: 0.434
#   pulse 4: activated nodes: 11367, borderline nodes: 2382, overall activation: 5721.628, activation diff: 3352.457, ratio: 0.586
#   pulse 5: activated nodes: 11438, borderline nodes: 207, overall activation: 7049.392, activation diff: 1327.764, ratio: 0.188
#   pulse 6: activated nodes: 11451, borderline nodes: 33, overall activation: 7490.742, activation diff: 441.350, ratio: 0.059
#   pulse 7: activated nodes: 11453, borderline nodes: 14, overall activation: 7632.368, activation diff: 141.626, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 7632.4
#   number of spread. activ. pulses: 7
#   running time: 1237

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996398   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99951845   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99934214   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.998489   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9972005   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7996131   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.7995855   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.7995684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.79955876   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7995459   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.79953915   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.7995386   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.79948115   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.79947275   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   16   0.7994703   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.7994663   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.79945564   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.7994498   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.799436   REFERENCES:SIMLOC
