###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2190.717, activation diff: 2216.740, ratio: 1.012
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 1115.371, activation diff: 3157.065, ratio: 2.831
#   pulse 4: activated nodes: 11325, borderline nodes: 2728, overall activation: 5934.263, activation diff: 5546.553, ratio: 0.935
#   pulse 5: activated nodes: 11430, borderline nodes: 291, overall activation: 6472.864, activation diff: 933.524, ratio: 0.144
#   pulse 6: activated nodes: 11443, borderline nodes: 85, overall activation: 6759.558, activation diff: 295.333, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 6759.6
#   number of spread. activ. pulses: 6
#   running time: 1328

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999905   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   20   0.75   REFERENCES:SIMLOC
