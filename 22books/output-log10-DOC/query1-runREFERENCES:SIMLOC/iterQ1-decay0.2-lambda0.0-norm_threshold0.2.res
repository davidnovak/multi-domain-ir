###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2336.263, activation diff: 2362.287, ratio: 1.011
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 1276.408, activation diff: 3443.262, ratio: 2.698
#   pulse 4: activated nodes: 11327, borderline nodes: 2663, overall activation: 6646.816, activation diff: 6200.523, ratio: 0.933
#   pulse 5: activated nodes: 11433, borderline nodes: 279, overall activation: 7277.035, activation diff: 1039.215, ratio: 0.143
#   pulse 6: activated nodes: 11449, borderline nodes: 42, overall activation: 7579.396, activation diff: 312.312, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7579.4
#   number of spread. activ. pulses: 6
#   running time: 1249

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999905   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_517   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMLOC
