###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1954.741, activation diff: 1979.638, ratio: 1.013
#   pulse 3: activated nodes: 9348, borderline nodes: 3988, overall activation: 884.613, activation diff: 2800.722, ratio: 3.166
#   pulse 4: activated nodes: 11298, borderline nodes: 3442, overall activation: 5704.921, activation diff: 5597.760, ratio: 0.981
#   pulse 5: activated nodes: 11421, borderline nodes: 468, overall activation: 5997.070, activation diff: 1394.092, ratio: 0.232
#   pulse 6: activated nodes: 11434, borderline nodes: 134, overall activation: 6666.406, activation diff: 693.847, ratio: 0.104
#   pulse 7: activated nodes: 11441, borderline nodes: 89, overall activation: 6708.127, activation diff: 44.663, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 6708.1
#   number of spread. activ. pulses: 7
#   running time: 1190

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   20   0.75   REFERENCES:SIMLOC
