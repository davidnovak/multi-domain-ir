###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 131.158, activation diff: 119.115, ratio: 0.908
#   pulse 3: activated nodes: 9263, borderline nodes: 3992, overall activation: 319.093, activation diff: 187.935, ratio: 0.589
#   pulse 4: activated nodes: 10690, borderline nodes: 4580, overall activation: 656.001, activation diff: 336.907, ratio: 0.514
#   pulse 5: activated nodes: 10808, borderline nodes: 3862, overall activation: 1059.933, activation diff: 403.932, ratio: 0.381
#   pulse 6: activated nodes: 10881, borderline nodes: 3620, overall activation: 1472.630, activation diff: 412.697, ratio: 0.280
#   pulse 7: activated nodes: 10887, borderline nodes: 3544, overall activation: 1855.290, activation diff: 382.661, ratio: 0.206
#   pulse 8: activated nodes: 10893, borderline nodes: 3522, overall activation: 2185.942, activation diff: 330.652, ratio: 0.151
#   pulse 9: activated nodes: 10896, borderline nodes: 3517, overall activation: 2459.235, activation diff: 273.293, ratio: 0.111
#   pulse 10: activated nodes: 10896, borderline nodes: 3517, overall activation: 2679.756, activation diff: 220.520, ratio: 0.082
#   pulse 11: activated nodes: 10897, borderline nodes: 3516, overall activation: 2855.330, activation diff: 175.574, ratio: 0.061
#   pulse 12: activated nodes: 10897, borderline nodes: 3515, overall activation: 2993.971, activation diff: 138.641, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2994.0
#   number of spread. activ. pulses: 12
#   running time: 1297

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869466   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98282665   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9806578   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.980262   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97694945   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9671447   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.47513136   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.47506493   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4748203   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.47470978   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.47466615   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.47447163   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.47440246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47379714   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.47366717   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.47357804   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.47356564   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   18   0.47353333   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.4734161   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.47340962   REFERENCES:SIMLOC
