###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 137.106, activation diff: 125.784, ratio: 0.917
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 373.086, activation diff: 235.996, ratio: 0.633
#   pulse 4: activated nodes: 11067, borderline nodes: 5684, overall activation: 956.821, activation diff: 583.735, ratio: 0.610
#   pulse 5: activated nodes: 11284, borderline nodes: 2691, overall activation: 1752.693, activation diff: 795.872, ratio: 0.454
#   pulse 6: activated nodes: 11389, borderline nodes: 1078, overall activation: 2618.765, activation diff: 866.072, ratio: 0.331
#   pulse 7: activated nodes: 11418, borderline nodes: 456, overall activation: 3422.499, activation diff: 803.734, ratio: 0.235
#   pulse 8: activated nodes: 11424, borderline nodes: 220, overall activation: 4106.445, activation diff: 683.946, ratio: 0.167
#   pulse 9: activated nodes: 11432, borderline nodes: 154, overall activation: 4665.355, activation diff: 558.910, ratio: 0.120
#   pulse 10: activated nodes: 11436, borderline nodes: 125, overall activation: 5112.827, activation diff: 447.472, ratio: 0.088
#   pulse 11: activated nodes: 11437, borderline nodes: 113, overall activation: 5466.949, activation diff: 354.122, ratio: 0.065
#   pulse 12: activated nodes: 11438, borderline nodes: 101, overall activation: 5745.224, activation diff: 278.275, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5745.2
#   number of spread. activ. pulses: 12
#   running time: 1393

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860959   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9813539   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9785631   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.97850174   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.975361   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9638784   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7116541   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7115513   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7111578   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.71106887   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.71105903   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7107544   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7101983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.70979875   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7097292   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7094989   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7093667   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   18   0.70913726   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.7091347   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.70913047   REFERENCES:SIMLOC
