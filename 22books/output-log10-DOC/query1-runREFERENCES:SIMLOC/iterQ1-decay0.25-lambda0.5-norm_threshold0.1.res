###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 735.664, activation diff: 717.348, ratio: 0.975
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1500.084, activation diff: 764.421, ratio: 0.510
#   pulse 4: activated nodes: 11319, borderline nodes: 3083, overall activation: 3393.342, activation diff: 1893.258, ratio: 0.558
#   pulse 5: activated nodes: 11425, borderline nodes: 369, overall activation: 4843.793, activation diff: 1450.450, ratio: 0.299
#   pulse 6: activated nodes: 11439, borderline nodes: 101, overall activation: 5725.698, activation diff: 881.905, ratio: 0.154
#   pulse 7: activated nodes: 11442, borderline nodes: 63, overall activation: 6226.452, activation diff: 500.754, ratio: 0.080
#   pulse 8: activated nodes: 11445, borderline nodes: 56, overall activation: 6504.101, activation diff: 277.649, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 6504.1
#   number of spread. activ. pulses: 8
#   running time: 1274

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959232   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9952256   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99458766   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9943766   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931394   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98915625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7432045   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.74320304   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7431438   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.74312663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7430006   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7429294   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7428023   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7427037   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.7426852   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.74265516   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.74263287   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7425413   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7425077   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.74248874   REFERENCES:SIMLOC
