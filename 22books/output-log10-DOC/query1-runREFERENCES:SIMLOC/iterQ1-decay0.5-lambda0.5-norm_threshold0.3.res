###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 341.136, activation diff: 327.530, ratio: 0.960
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 604.424, activation diff: 265.499, ratio: 0.439
#   pulse 4: activated nodes: 10511, borderline nodes: 5669, overall activation: 1371.705, activation diff: 767.281, ratio: 0.559
#   pulse 5: activated nodes: 10796, borderline nodes: 4037, overall activation: 2078.181, activation diff: 706.476, ratio: 0.340
#   pulse 6: activated nodes: 10866, borderline nodes: 3663, overall activation: 2614.729, activation diff: 536.548, ratio: 0.205
#   pulse 7: activated nodes: 10887, borderline nodes: 3561, overall activation: 2960.533, activation diff: 345.804, ratio: 0.117
#   pulse 8: activated nodes: 10889, borderline nodes: 3536, overall activation: 3165.190, activation diff: 204.657, ratio: 0.065
#   pulse 9: activated nodes: 10894, borderline nodes: 3524, overall activation: 3282.976, activation diff: 117.785, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10894
#   final overall activation: 3283.0
#   number of spread. activ. pulses: 9
#   running time: 1229

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99774945   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9971088   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9966   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9960855   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9953091   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9916278   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.49742842   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.49740314   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.49739003   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.49736485   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49733102   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49718487   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.49717414   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.49710545   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.497096   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.49706694   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.4970657   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.49706304   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.4970433   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.49703735   REFERENCES:SIMLOC
