###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 416.045, activation diff: 399.981, ratio: 0.961
#   pulse 3: activated nodes: 9228, borderline nodes: 4101, overall activation: 747.962, activation diff: 332.116, ratio: 0.444
#   pulse 4: activated nodes: 10673, borderline nodes: 4795, overall activation: 1557.646, activation diff: 809.684, ratio: 0.520
#   pulse 5: activated nodes: 10855, borderline nodes: 3720, overall activation: 2267.549, activation diff: 709.903, ratio: 0.313
#   pulse 6: activated nodes: 10887, borderline nodes: 3564, overall activation: 2764.041, activation diff: 496.493, ratio: 0.180
#   pulse 7: activated nodes: 10890, borderline nodes: 3526, overall activation: 3065.501, activation diff: 301.459, ratio: 0.098
#   pulse 8: activated nodes: 10895, borderline nodes: 3518, overall activation: 3239.400, activation diff: 173.899, ratio: 0.054
#   pulse 9: activated nodes: 10895, borderline nodes: 3518, overall activation: 3337.739, activation diff: 98.338, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3337.7
#   number of spread. activ. pulses: 9
#   running time: 1287

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978901   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9974089   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969773   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99675   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99571794   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9927213   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.49759138   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.49758318   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49757886   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.4975726   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49751008   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.49745095   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49739194   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.49738133   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.4973495   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49729487   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.49727714   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.4972751   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.497274   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.4972455   REFERENCES:SIMLOC
