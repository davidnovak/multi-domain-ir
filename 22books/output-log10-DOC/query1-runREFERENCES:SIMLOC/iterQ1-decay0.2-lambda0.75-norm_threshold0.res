###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 265.836, activation diff: 253.117, ratio: 0.952
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 697.981, activation diff: 432.145, ratio: 0.619
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1639.872, activation diff: 941.891, ratio: 0.574
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2744.291, activation diff: 1104.419, ratio: 0.402
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3770.594, activation diff: 1026.302, ratio: 0.272
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 4637.138, activation diff: 866.544, ratio: 0.187
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 5340.072, activation diff: 702.934, ratio: 0.132
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 5899.162, activation diff: 559.090, ratio: 0.095
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 6338.965, activation diff: 439.802, ratio: 0.069
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 6682.600, activation diff: 343.635, ratio: 0.051
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 6949.920, activation diff: 267.320, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6949.9
#   number of spread. activ. pulses: 12
#   running time: 1292

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756826   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98408914   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9824879   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98184127   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97860765   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97075367   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.76140094   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7613891   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7608369   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7607785   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7606223   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.7606101   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7605075   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7596154   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.75949764   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7594461   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.7592068   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.75909483   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.75908446   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.75900024   REFERENCES:SIMLOC
