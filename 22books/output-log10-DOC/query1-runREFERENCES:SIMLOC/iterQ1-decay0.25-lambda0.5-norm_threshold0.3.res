###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 503.317, activation diff: 489.711, ratio: 0.973
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 973.548, activation diff: 473.509, ratio: 0.486
#   pulse 4: activated nodes: 11147, borderline nodes: 5618, overall activation: 2673.870, activation diff: 1700.322, ratio: 0.636
#   pulse 5: activated nodes: 11375, borderline nodes: 1341, overall activation: 4215.504, activation diff: 1541.634, ratio: 0.366
#   pulse 6: activated nodes: 11418, borderline nodes: 435, overall activation: 5259.085, activation diff: 1043.581, ratio: 0.198
#   pulse 7: activated nodes: 11430, borderline nodes: 173, overall activation: 5880.479, activation diff: 621.394, ratio: 0.106
#   pulse 8: activated nodes: 11435, borderline nodes: 126, overall activation: 6234.084, activation diff: 353.606, ratio: 0.057
#   pulse 9: activated nodes: 11437, borderline nodes: 110, overall activation: 6431.919, activation diff: 197.835, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 6431.9
#   number of spread. activ. pulses: 9
#   running time: 1228

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99774945   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9971143   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9966115   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9960886   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9953373   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9917027   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.74616337   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.74611723   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7460982   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.7460518   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7460458   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.74586785   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7458187   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.74580884   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7457694   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.7457049   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.74569345   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   18   0.74566334   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.74563676   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.7456056   REFERENCES:SIMLOC
