###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1809.873, activation diff: 1806.048, ratio: 0.998
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2787.340, activation diff: 1275.362, ratio: 0.458
#   pulse 4: activated nodes: 11367, borderline nodes: 2324, overall activation: 6963.153, activation diff: 4176.747, ratio: 0.600
#   pulse 5: activated nodes: 11439, borderline nodes: 189, overall activation: 8583.152, activation diff: 1620.013, ratio: 0.189
#   pulse 6: activated nodes: 11451, borderline nodes: 28, overall activation: 9116.149, activation diff: 532.996, ratio: 0.058
#   pulse 7: activated nodes: 11454, borderline nodes: 7, overall activation: 9285.199, activation diff: 169.051, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 9285.2
#   number of spread. activ. pulses: 7
#   running time: 1241

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99964   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995185   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99934214   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9972005   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8995689   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.8995384   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.8995251   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.89950794   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.8994918   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8994868   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.89948225   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.89944285   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.899419   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8994171   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.8994118   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.8994043   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.89940286   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.8993772   REFERENCES:SIMLOC
