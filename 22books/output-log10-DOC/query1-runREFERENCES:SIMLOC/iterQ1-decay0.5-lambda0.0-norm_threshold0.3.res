###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1305.441, activation diff: 1330.339, ratio: 1.019
#   pulse 3: activated nodes: 9348, borderline nodes: 3988, overall activation: 330.469, activation diff: 1614.812, ratio: 4.886
#   pulse 4: activated nodes: 10748, borderline nodes: 4400, overall activation: 2933.978, activation diff: 2866.435, ratio: 0.977
#   pulse 5: activated nodes: 10881, borderline nodes: 3626, overall activation: 2724.699, activation diff: 944.108, ratio: 0.346
#   pulse 6: activated nodes: 10890, borderline nodes: 3538, overall activation: 3394.322, activation diff: 679.033, ratio: 0.200
#   pulse 7: activated nodes: 10895, borderline nodes: 3525, overall activation: 3425.736, activation diff: 32.594, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3425.7
#   number of spread. activ. pulses: 7
#   running time: 1228

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998933   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883837   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999982   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4999995   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49999902   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49999815   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.4999971   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999708   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999687   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49999648   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999562   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999523   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999425   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49999288   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.4999925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49999118   REFERENCES:SIMLOC
