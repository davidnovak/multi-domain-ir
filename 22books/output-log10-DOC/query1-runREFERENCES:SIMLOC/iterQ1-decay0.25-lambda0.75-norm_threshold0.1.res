###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 188.440, activation diff: 176.397, ratio: 0.936
#   pulse 3: activated nodes: 9263, borderline nodes: 3992, overall activation: 493.110, activation diff: 304.670, ratio: 0.618
#   pulse 4: activated nodes: 11230, borderline nodes: 4405, overall activation: 1187.781, activation diff: 694.671, ratio: 0.585
#   pulse 5: activated nodes: 11387, borderline nodes: 1127, overall activation: 2074.666, activation diff: 886.885, ratio: 0.427
#   pulse 6: activated nodes: 11420, borderline nodes: 416, overall activation: 2965.652, activation diff: 890.986, ratio: 0.300
#   pulse 7: activated nodes: 11434, borderline nodes: 164, overall activation: 3749.343, activation diff: 783.691, ratio: 0.209
#   pulse 8: activated nodes: 11438, borderline nodes: 113, overall activation: 4398.187, activation diff: 648.844, ratio: 0.148
#   pulse 9: activated nodes: 11440, borderline nodes: 85, overall activation: 4920.515, activation diff: 522.328, ratio: 0.106
#   pulse 10: activated nodes: 11441, borderline nodes: 74, overall activation: 5334.752, activation diff: 414.237, ratio: 0.078
#   pulse 11: activated nodes: 11443, borderline nodes: 71, overall activation: 5660.416, activation diff: 325.665, ratio: 0.058
#   pulse 12: activated nodes: 11443, borderline nodes: 68, overall activation: 5915.044, activation diff: 254.628, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 5915.0
#   number of spread. activ. pulses: 12
#   running time: 1469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869507   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98288107   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98077697   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9803676   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9770991   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9676626   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.71283114   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.71277654   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.71227986   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.71214783   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.71214354   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7121296   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7117215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71104103   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.71091163   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7107841   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.7106695   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.71048963   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.71046257   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7104131   REFERENCES:SIMLOC
