###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1013.440, activation diff: 1009.614, ratio: 0.996
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 1236.413, activation diff: 406.004, ratio: 0.328
#   pulse 4: activated nodes: 10814, borderline nodes: 3761, overall activation: 2590.990, activation diff: 1354.628, ratio: 0.523
#   pulse 5: activated nodes: 10894, borderline nodes: 3530, overall activation: 3178.364, activation diff: 587.374, ratio: 0.185
#   pulse 6: activated nodes: 10897, borderline nodes: 3515, overall activation: 3383.457, activation diff: 205.093, ratio: 0.061
#   pulse 7: activated nodes: 10897, borderline nodes: 3514, overall activation: 3451.644, activation diff: 68.187, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 3451.6
#   number of spread. activ. pulses: 7
#   running time: 1180

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99963915   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995181   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9993421   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.998487   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9972002   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4997507   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.4997316   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49971092   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49970734   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.4997069   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.49970624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49969578   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.4996706   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.49966013   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49963358   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.4996233   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.4996109   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.4995959   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.4995921   REFERENCES:SIMLOC
