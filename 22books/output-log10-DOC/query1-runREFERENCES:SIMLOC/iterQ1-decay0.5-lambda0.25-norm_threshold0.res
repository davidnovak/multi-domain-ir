###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1138.382, activation diff: 1130.819, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1544.920, activation diff: 464.808, ratio: 0.301
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2785.475, activation diff: 1240.555, ratio: 0.445
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 3270.666, activation diff: 485.190, ratio: 0.148
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 3431.317, activation diff: 160.651, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3431.3
#   number of spread. activ. pulses: 6
#   running time: 1167

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991438   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99888986   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9986691   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9982261   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99735576   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9953981   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4992495   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.49920726   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49913973   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4991316   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.4991181   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.49910644   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49906534   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.499062   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.49903783   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   16   0.49895737   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.49893475   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.498904   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.49890366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.49888897   REFERENCES:SIMLOC
