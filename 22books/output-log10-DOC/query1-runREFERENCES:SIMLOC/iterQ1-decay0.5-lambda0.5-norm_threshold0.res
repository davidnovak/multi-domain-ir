###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 583.771, activation diff: 563.906, ratio: 0.966
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1107.116, activation diff: 523.346, ratio: 0.473
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1967.588, activation diff: 860.471, ratio: 0.437
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2611.747, activation diff: 644.160, ratio: 0.247
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 3005.356, activation diff: 393.609, ratio: 0.131
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 3230.116, activation diff: 224.760, ratio: 0.070
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 3355.310, activation diff: 125.194, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3355.3
#   number of spread. activ. pulses: 8
#   running time: 1280

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99599755   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9954815   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9949516   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9949373   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99356973   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99033   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49566102   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49564898   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4955827   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.4955727   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.49553695   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.4954905   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49548084   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.49533436   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49533254   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49525088   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.49523664   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.4951927   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.49518698   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.49515927   REFERENCES:SIMLOC
