###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 250.329, activation diff: 237.610, ratio: 0.949
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 647.733, activation diff: 397.404, ratio: 0.614
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1479.174, activation diff: 831.441, ratio: 0.562
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2456.625, activation diff: 977.451, ratio: 0.398
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 3372.602, activation diff: 915.977, ratio: 0.272
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 4150.186, activation diff: 777.584, ratio: 0.187
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 4783.191, activation diff: 633.005, ratio: 0.132
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 5287.927, activation diff: 504.736, ratio: 0.095
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 5685.753, activation diff: 397.826, ratio: 0.070
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 5997.103, activation diff: 311.350, ratio: 0.052
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 6239.656, activation diff: 242.553, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6239.7
#   number of spread. activ. pulses: 12
#   running time: 1356

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.987568   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9840848   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9824821   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.981833   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97859347   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97071123   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7137979   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7137929   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7132688   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.71322185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.71306074   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.7130606   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7129607   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71210307   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.7119932   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7119167   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.71168363   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7116337   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7116231   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.71152365   REFERENCES:SIMLOC
