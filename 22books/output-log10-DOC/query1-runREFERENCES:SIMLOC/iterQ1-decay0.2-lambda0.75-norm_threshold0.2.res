###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 145.219, activation diff: 133.897, ratio: 0.922
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 399.846, activation diff: 254.643, ratio: 0.637
#   pulse 4: activated nodes: 11072, borderline nodes: 5659, overall activation: 1059.547, activation diff: 659.701, ratio: 0.623
#   pulse 5: activated nodes: 11298, borderline nodes: 2472, overall activation: 1972.165, activation diff: 912.619, ratio: 0.463
#   pulse 6: activated nodes: 11397, borderline nodes: 958, overall activation: 2960.434, activation diff: 988.268, ratio: 0.334
#   pulse 7: activated nodes: 11420, borderline nodes: 379, overall activation: 3868.641, activation diff: 908.208, ratio: 0.235
#   pulse 8: activated nodes: 11436, borderline nodes: 167, overall activation: 4636.826, activation diff: 768.185, ratio: 0.166
#   pulse 9: activated nodes: 11443, borderline nodes: 97, overall activation: 5262.415, activation diff: 625.589, ratio: 0.119
#   pulse 10: activated nodes: 11447, borderline nodes: 65, overall activation: 5762.169, activation diff: 499.754, ratio: 0.087
#   pulse 11: activated nodes: 11449, borderline nodes: 50, overall activation: 6157.079, activation diff: 394.909, ratio: 0.064
#   pulse 12: activated nodes: 11449, borderline nodes: 38, overall activation: 6467.030, activation diff: 309.951, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6467.0
#   number of spread. activ. pulses: 12
#   running time: 1318

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98609704   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9813663   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9785848   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9785361   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9753879   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9639867   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7591237   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.75900906   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7585801   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.75851285   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.75848204   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7581647   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7575721   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.75717616   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7571445   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7569022   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.75671184   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7564712   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.756439   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.756415   REFERENCES:SIMLOC
