###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1808.980, activation diff: 1801.417, ratio: 0.996
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2978.714, activation diff: 1247.559, ratio: 0.419
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 6142.799, activation diff: 3164.085, ratio: 0.515
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 7310.200, activation diff: 1167.401, ratio: 0.160
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7686.139, activation diff: 375.939, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7686.1
#   number of spread. activ. pulses: 6
#   running time: 1160

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991438   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9988909   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9986696   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9982261   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99735856   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99539846   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79884493   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.7987491   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.7986944   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7986391   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7986383   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.79863364   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7985706   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.79854596   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7985403   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.79850084   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.798432   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.79837465   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7983489   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7983269   REFERENCES:SIMLOC
