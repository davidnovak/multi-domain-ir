###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 222.809, activation diff: 210.766, ratio: 0.946
#   pulse 3: activated nodes: 9263, borderline nodes: 3992, overall activation: 604.121, activation diff: 381.312, ratio: 0.631
#   pulse 4: activated nodes: 11251, borderline nodes: 4287, overall activation: 1602.620, activation diff: 998.500, ratio: 0.623
#   pulse 5: activated nodes: 11398, borderline nodes: 911, overall activation: 2888.568, activation diff: 1285.947, ratio: 0.445
#   pulse 6: activated nodes: 11436, borderline nodes: 253, overall activation: 4147.518, activation diff: 1258.950, ratio: 0.304
#   pulse 7: activated nodes: 11445, borderline nodes: 76, overall activation: 5232.861, activation diff: 1085.343, ratio: 0.207
#   pulse 8: activated nodes: 11450, borderline nodes: 33, overall activation: 6120.875, activation diff: 888.014, ratio: 0.145
#   pulse 9: activated nodes: 11452, borderline nodes: 17, overall activation: 6830.163, activation diff: 709.288, ratio: 0.104
#   pulse 10: activated nodes: 11453, borderline nodes: 10, overall activation: 7389.398, activation diff: 559.235, ratio: 0.076
#   pulse 11: activated nodes: 11453, borderline nodes: 9, overall activation: 7826.912, activation diff: 437.514, ratio: 0.056
#   pulse 12: activated nodes: 11454, borderline nodes: 7, overall activation: 8167.502, activation diff: 340.591, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8167.5
#   number of spread. activ. pulses: 12
#   running time: 1357

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869522   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9828993   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98081243   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9804028   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9771516   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96784866   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8554581   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.85536075   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.8547677   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.85464644   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.85462487   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8546163   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.85412645   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8533853   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.8533237   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8530779   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.85307413   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.8526614   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.85263956   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.85262704   REFERENCES:SIMLOC
