###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 172.793, activation diff: 160.074, ratio: 0.926
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 409.438, activation diff: 236.645, ratio: 0.578
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 797.020, activation diff: 387.582, ratio: 0.486
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1235.144, activation diff: 438.124, ratio: 0.355
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1658.941, activation diff: 423.797, ratio: 0.255
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 2031.957, activation diff: 373.017, ratio: 0.184
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 2342.623, activation diff: 310.666, ratio: 0.133
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2593.987, activation diff: 251.364, ratio: 0.097
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 2794.198, activation diff: 200.211, ratio: 0.072
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 2952.167, activation diff: 157.969, ratio: 0.054
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 3076.051, activation diff: 123.884, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3076.1
#   number of spread. activ. pulses: 12
#   running time: 1421

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756576   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9840517   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9824282   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98176605   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97848415   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9703747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.47579145   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.4757683   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.47545266   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.47543997   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.47532985   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4752439   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.47512394   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.4745416   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.4744991   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.47435603   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.4743477   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   18   0.47420484   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.4741612   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.4741292   REFERENCES:SIMLOC
