###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1151.546, activation diff: 1154.213, ratio: 1.002
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 1298.268, activation diff: 779.203, ratio: 0.600
#   pulse 4: activated nodes: 11250, borderline nodes: 4358, overall activation: 4393.136, activation diff: 3118.312, ratio: 0.710
#   pulse 5: activated nodes: 11401, borderline nodes: 793, overall activation: 5863.340, activation diff: 1470.354, ratio: 0.251
#   pulse 6: activated nodes: 11428, borderline nodes: 197, overall activation: 6414.703, activation diff: 551.363, ratio: 0.086
#   pulse 7: activated nodes: 11439, borderline nodes: 108, overall activation: 6603.406, activation diff: 188.702, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 6603.4
#   number of spread. activ. pulses: 7
#   running time: 1177

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99928033   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991315   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99851894   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979354   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961202   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.74934083   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.7492442   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74923944   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.749231   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7492143   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.74921083   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.7491975   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.7491689   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.749153   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.7491436   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.7491262   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7491056   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.7490939   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.74908954   REFERENCES:SIMLOC
