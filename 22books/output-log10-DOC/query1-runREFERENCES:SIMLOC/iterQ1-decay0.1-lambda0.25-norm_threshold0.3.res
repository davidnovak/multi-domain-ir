###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1379.328, activation diff: 1381.994, ratio: 1.002
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 1681.141, activation diff: 1054.263, ratio: 0.627
#   pulse 4: activated nodes: 11259, borderline nodes: 4214, overall activation: 6104.984, activation diff: 4460.681, ratio: 0.731
#   pulse 5: activated nodes: 11411, borderline nodes: 638, overall activation: 8124.644, activation diff: 2019.896, ratio: 0.249
#   pulse 6: activated nodes: 11441, borderline nodes: 79, overall activation: 8860.915, activation diff: 736.271, ratio: 0.083
#   pulse 7: activated nodes: 11451, borderline nodes: 26, overall activation: 9108.211, activation diff: 247.297, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9108.2
#   number of spread. activ. pulses: 7
#   running time: 1277

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99928075   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991318   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99851894   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979364   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961208   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.899218   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   8   0.8991127   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.89909804   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.899094   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   11   0.8990811   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.8990714   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8990666   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.89906114   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.89899826   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.8989961   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   17   0.8989848   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.89897317   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   19   0.89895064   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.89893746   REFERENCES:SIMLOC
