###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 199.896, activation diff: 187.854, ratio: 0.940
#   pulse 3: activated nodes: 9263, borderline nodes: 3992, overall activation: 529.608, activation diff: 329.711, ratio: 0.623
#   pulse 4: activated nodes: 11238, borderline nodes: 4361, overall activation: 1318.552, activation diff: 788.944, ratio: 0.598
#   pulse 5: activated nodes: 11389, borderline nodes: 1036, overall activation: 2332.449, activation diff: 1013.897, ratio: 0.435
#   pulse 6: activated nodes: 11426, borderline nodes: 352, overall activation: 3342.516, activation diff: 1010.067, ratio: 0.302
#   pulse 7: activated nodes: 11444, borderline nodes: 131, overall activation: 4224.912, activation diff: 882.396, ratio: 0.209
#   pulse 8: activated nodes: 11447, borderline nodes: 63, overall activation: 4952.680, activation diff: 727.769, ratio: 0.147
#   pulse 9: activated nodes: 11449, borderline nodes: 39, overall activation: 5537.139, activation diff: 584.458, ratio: 0.106
#   pulse 10: activated nodes: 11452, borderline nodes: 28, overall activation: 5999.917, activation diff: 462.779, ratio: 0.077
#   pulse 11: activated nodes: 11453, borderline nodes: 21, overall activation: 6363.299, activation diff: 363.382, ratio: 0.057
#   pulse 12: activated nodes: 11453, borderline nodes: 17, overall activation: 6647.109, activation diff: 283.811, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6647.1
#   number of spread. activ. pulses: 12
#   running time: 1429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98695123   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9828879   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9807906   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98038083   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9771185   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96773183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.76037323   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7603067   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.75977486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.75964886   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7596391   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.75962484   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7591889   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7584897   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.75838745   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.75821316   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.7581363   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.75787824   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7578487   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.75782454   REFERENCES:SIMLOC
