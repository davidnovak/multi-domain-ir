###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2084.601, activation diff: 2109.498, ratio: 1.012
#   pulse 3: activated nodes: 9348, borderline nodes: 3988, overall activation: 1016.204, activation diff: 3057.038, ratio: 3.008
#   pulse 4: activated nodes: 11298, borderline nodes: 3383, overall activation: 6403.736, activation diff: 6279.566, ratio: 0.981
#   pulse 5: activated nodes: 11422, borderline nodes: 428, overall activation: 6794.334, activation diff: 1520.192, ratio: 0.224
#   pulse 6: activated nodes: 11444, borderline nodes: 67, overall activation: 7488.468, activation diff: 720.847, ratio: 0.096
#   pulse 7: activated nodes: 11450, borderline nodes: 34, overall activation: 7532.904, activation diff: 47.664, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7532.9
#   number of spread. activ. pulses: 7
#   running time: 1190

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_441   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.8   REFERENCES:SIMLOC
