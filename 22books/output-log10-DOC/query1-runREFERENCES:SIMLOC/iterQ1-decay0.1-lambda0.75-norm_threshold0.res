###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 296.851, activation diff: 284.132, ratio: 0.957
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 800.773, activation diff: 503.922, ratio: 0.629
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1982.004, activation diff: 1181.231, ratio: 0.596
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3351.994, activation diff: 1369.990, ratio: 0.409
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4604.762, activation diff: 1252.768, ratio: 0.272
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 5651.663, activation diff: 1046.901, ratio: 0.185
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 6495.078, activation diff: 843.415, ratio: 0.130
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 7162.442, activation diff: 667.364, ratio: 0.093
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 7685.179, activation diff: 522.737, ratio: 0.068
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 8092.094, activation diff: 406.915, ratio: 0.050
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 8407.572, activation diff: 315.478, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8407.6
#   number of spread. activ. pulses: 12
#   running time: 1423

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756874   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9840964   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98249686   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9818554   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97863173   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9708231   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8566076   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.85657823   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.85597384   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8558944   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8557359   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.8557121   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8556037   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.854639   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.85450995   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.8544914   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.85425967   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.85402274   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.85401297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.8539465   REFERENCES:SIMLOC
