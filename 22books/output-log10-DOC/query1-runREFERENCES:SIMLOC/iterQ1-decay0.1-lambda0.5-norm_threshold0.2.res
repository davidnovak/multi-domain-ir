###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 733.687, activation diff: 717.623, ratio: 0.978
#   pulse 3: activated nodes: 9228, borderline nodes: 4101, overall activation: 1513.019, activation diff: 779.683, ratio: 0.515
#   pulse 4: activated nodes: 11257, borderline nodes: 4198, overall activation: 4162.036, activation diff: 2649.017, ratio: 0.636
#   pulse 5: activated nodes: 11407, borderline nodes: 635, overall activation: 6284.102, activation diff: 2122.066, ratio: 0.338
#   pulse 6: activated nodes: 11442, borderline nodes: 111, overall activation: 7601.274, activation diff: 1317.172, ratio: 0.173
#   pulse 7: activated nodes: 11450, borderline nodes: 33, overall activation: 8354.598, activation diff: 753.324, ratio: 0.090
#   pulse 8: activated nodes: 11452, borderline nodes: 16, overall activation: 8773.394, activation diff: 418.796, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 8773.4
#   number of spread. activ. pulses: 8
#   running time: 1199

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99578035   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9948379   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99407446   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9935131   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9926013   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98768365   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.8914057   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89135957   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.8913329   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.8913157   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.89114565   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.8908787   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.8908398   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89071554   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.89067745   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.8905953   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   17   0.8905759   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.8905707   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   19   0.8905464   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.8905442   REFERENCES:SIMLOC
