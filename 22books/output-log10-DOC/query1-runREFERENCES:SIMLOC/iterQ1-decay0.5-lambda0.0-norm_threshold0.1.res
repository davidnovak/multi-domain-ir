###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1614.572, activation diff: 1641.481, ratio: 1.017
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 590.188, activation diff: 1963.766, ratio: 3.327
#   pulse 4: activated nodes: 10853, borderline nodes: 3682, overall activation: 3218.824, activation diff: 2769.201, ratio: 0.860
#   pulse 5: activated nodes: 10896, borderline nodes: 3523, overall activation: 3388.177, activation diff: 258.203, ratio: 0.076
#   pulse 6: activated nodes: 10897, borderline nodes: 3516, overall activation: 3474.965, activation diff: 86.958, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 3475.0
#   number of spread. activ. pulses: 6
#   running time: 1137

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999136   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991727   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990493   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999985   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4999996   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4999992   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49999848   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.49999765   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999762   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999744   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4999971   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999642   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999627   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4999953   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49999416   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.49999386   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49999285   REFERENCES:SIMLOC
