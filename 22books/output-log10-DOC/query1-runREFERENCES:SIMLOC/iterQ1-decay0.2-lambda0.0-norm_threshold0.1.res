###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2578.361, activation diff: 2605.270, ratio: 1.010
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1659.443, activation diff: 3725.357, ratio: 2.245
#   pulse 4: activated nodes: 11372, borderline nodes: 2078, overall activation: 6961.433, activation diff: 5933.052, ratio: 0.852
#   pulse 5: activated nodes: 11441, borderline nodes: 123, overall activation: 7568.067, activation diff: 715.563, ratio: 0.095
#   pulse 6: activated nodes: 11452, borderline nodes: 21, overall activation: 7690.337, activation diff: 127.160, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7690.3
#   number of spread. activ. pulses: 6
#   running time: 1274

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999914   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991727   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990495   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   20   0.8   REFERENCES:SIMLOC
