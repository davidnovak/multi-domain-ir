###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1418.939, activation diff: 1418.937, ratio: 1.000
#   pulse 3: activated nodes: 9448, borderline nodes: 3921, overall activation: 1848.643, activation diff: 943.119, ratio: 0.510
#   pulse 4: activated nodes: 11311, borderline nodes: 3252, overall activation: 5324.260, activation diff: 3483.588, ratio: 0.654
#   pulse 5: activated nodes: 11422, borderline nodes: 420, overall activation: 6820.475, activation diff: 1496.273, ratio: 0.219
#   pulse 6: activated nodes: 11445, borderline nodes: 66, overall activation: 7342.837, activation diff: 522.361, ratio: 0.071
#   pulse 7: activated nodes: 11450, borderline nodes: 36, overall activation: 7515.473, activation diff: 172.636, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7515.5
#   number of spread. activ. pulses: 7
#   running time: 1206

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99949706   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993684   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.998982   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99824226   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967107   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.7994733   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7994362   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.79940015   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.79939544   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.79939514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.79938304   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.7993474   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   14   0.7993383   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.79931295   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.799312   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.79929197   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.79927135   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.7992595   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   20   0.799255   REFERENCES:SIMLOC
