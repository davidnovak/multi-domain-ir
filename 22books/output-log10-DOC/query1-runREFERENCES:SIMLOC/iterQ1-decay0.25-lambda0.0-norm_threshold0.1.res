###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2417.730, activation diff: 2444.639, ratio: 1.011
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1458.667, activation diff: 3416.160, ratio: 2.342
#   pulse 4: activated nodes: 11372, borderline nodes: 2111, overall activation: 6238.440, activation diff: 5335.097, ratio: 0.855
#   pulse 5: activated nodes: 11441, borderline nodes: 130, overall activation: 6768.509, activation diff: 633.347, ratio: 0.094
#   pulse 6: activated nodes: 11447, borderline nodes: 55, overall activation: 6882.035, activation diff: 117.442, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6882.0
#   number of spread. activ. pulses: 6
#   running time: 1295

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999914   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991727   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990495   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   18   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMLOC
