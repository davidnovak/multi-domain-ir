###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 892.461, activation diff: 892.458, ratio: 1.000
#   pulse 3: activated nodes: 9448, borderline nodes: 3921, overall activation: 956.628, activation diff: 397.812, ratio: 0.416
#   pulse 4: activated nodes: 10770, borderline nodes: 4238, overall activation: 2389.724, activation diff: 1433.396, ratio: 0.600
#   pulse 5: activated nodes: 10883, borderline nodes: 3614, overall activation: 3069.169, activation diff: 679.444, ratio: 0.221
#   pulse 6: activated nodes: 10890, borderline nodes: 3526, overall activation: 3325.634, activation diff: 256.465, ratio: 0.077
#   pulse 7: activated nodes: 10895, borderline nodes: 3518, overall activation: 3414.792, activation diff: 89.158, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3414.8
#   number of spread. activ. pulses: 7
#   running time: 1214

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99949604   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993678   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99898195   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.998238   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99670947   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.4996603   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.499641   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49961635   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.49960628   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.4995969   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49959683   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.49958318   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.49951005   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4995066   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.49950263   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.49949574   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.49949223   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.49948263   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.49947977   REFERENCES:SIMLOC
