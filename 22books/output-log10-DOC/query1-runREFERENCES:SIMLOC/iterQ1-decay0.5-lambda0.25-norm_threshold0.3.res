###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 771.911, activation diff: 774.578, ratio: 1.003
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 734.312, activation diff: 390.094, ratio: 0.531
#   pulse 4: activated nodes: 10663, borderline nodes: 4961, overall activation: 2190.345, activation diff: 1458.086, ratio: 0.666
#   pulse 5: activated nodes: 10861, borderline nodes: 3718, overall activation: 2939.349, activation diff: 749.004, ratio: 0.255
#   pulse 6: activated nodes: 10887, borderline nodes: 3562, overall activation: 3255.237, activation diff: 315.888, ratio: 0.097
#   pulse 7: activated nodes: 10891, borderline nodes: 3528, overall activation: 3370.820, activation diff: 115.583, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10891
#   final overall activation: 3370.8
#   number of spread. activ. pulses: 7
#   running time: 1182

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992786   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991301   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99851876   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99792695   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961114   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.49955064   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.49949062   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.49948335   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.4994734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.49945873   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.49945152   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.499426   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.49935484   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.49934667   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49934536   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.4993416   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.4993388   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.4993343   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.49933314   REFERENCES:SIMLOC
