###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1032.242, activation diff: 1012.378, ratio: 0.981
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2314.029, activation diff: 1281.786, ratio: 0.554
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5111.211, activation diff: 2797.182, ratio: 0.547
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 7021.743, activation diff: 1910.532, ratio: 0.272
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 8129.688, activation diff: 1107.946, ratio: 0.136
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8743.096, activation diff: 613.408, ratio: 0.070
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 9076.874, activation diff: 333.777, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 9076.9
#   number of spread. activ. pulses: 8
#   running time: 1242

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9954848   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9949571   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99493754   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935859   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9903431   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89222056   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89219177   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.89207095   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.89206314   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.8919938   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.8919606   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.89189446   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.89170057   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.8916785   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.89164746   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.8915808   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.8915105   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.8914944   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.8914547   REFERENCES:SIMLOC
