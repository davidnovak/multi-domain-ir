###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 600.626, activation diff: 587.020, ratio: 0.977
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 1212.503, activation diff: 615.794, ratio: 0.508
#   pulse 4: activated nodes: 11169, borderline nodes: 5520, overall activation: 3717.409, activation diff: 2504.906, ratio: 0.674
#   pulse 5: activated nodes: 11386, borderline nodes: 1080, overall activation: 5904.217, activation diff: 2186.808, ratio: 0.370
#   pulse 6: activated nodes: 11434, borderline nodes: 230, overall activation: 7334.974, activation diff: 1430.757, ratio: 0.195
#   pulse 7: activated nodes: 11444, borderline nodes: 69, overall activation: 8171.489, activation diff: 836.514, ratio: 0.102
#   pulse 8: activated nodes: 11450, borderline nodes: 31, overall activation: 8642.373, activation diff: 470.884, ratio: 0.054
#   pulse 9: activated nodes: 11451, borderline nodes: 19, overall activation: 8903.621, activation diff: 261.248, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 8903.6
#   number of spread. activ. pulses: 9
#   running time: 1303

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977495   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.997116   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99661547   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9960891   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9953437   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9917174   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.89540875   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.8953484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8953212   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.8952625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8952581   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.8951261   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8949976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.894992   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.89492774   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.89492524   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   17   0.8948937   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.8948458   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   19   0.89481735   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.89478755   REFERENCES:SIMLOC
