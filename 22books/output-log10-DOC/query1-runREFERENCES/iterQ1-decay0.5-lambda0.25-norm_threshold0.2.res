###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 883.247, activation diff: 886.567, ratio: 1.004
#   pulse 3: activated nodes: 8645, borderline nodes: 3406, overall activation: 705.578, activation diff: 266.777, ratio: 0.378
#   pulse 4: activated nodes: 8645, borderline nodes: 3406, overall activation: 1593.040, activation diff: 887.506, ratio: 0.557
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1931.723, activation diff: 338.683, ratio: 0.175
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2041.867, activation diff: 110.144, ratio: 0.054
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2075.550, activation diff: 33.683, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2075.5
#   number of spread. activ. pulses: 7
#   running time: 367

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992964   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99906254   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99891585   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99819875   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964218   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.4995601   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49955183   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.49953425   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.4995104   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.4994318   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.49942994   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.49939114   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.49939114   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.4993834   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.4993261   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.49932286   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.49931267   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49928144   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.49927917   REFERENCES
