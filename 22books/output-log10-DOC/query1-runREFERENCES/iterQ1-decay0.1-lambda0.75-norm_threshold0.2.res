###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 161.201, activation diff: 149.878, ratio: 0.930
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 446.517, activation diff: 285.337, ratio: 0.639
#   pulse 4: activated nodes: 10262, borderline nodes: 4982, overall activation: 1213.854, activation diff: 767.337, ratio: 0.632
#   pulse 5: activated nodes: 11081, borderline nodes: 2997, overall activation: 2190.890, activation diff: 977.036, ratio: 0.446
#   pulse 6: activated nodes: 11209, borderline nodes: 1665, overall activation: 3176.669, activation diff: 985.778, ratio: 0.310
#   pulse 7: activated nodes: 11281, borderline nodes: 755, overall activation: 4066.435, activation diff: 889.766, ratio: 0.219
#   pulse 8: activated nodes: 11309, borderline nodes: 431, overall activation: 4827.225, activation diff: 760.790, ratio: 0.158
#   pulse 9: activated nodes: 11315, borderline nodes: 224, overall activation: 5459.382, activation diff: 632.157, ratio: 0.116
#   pulse 10: activated nodes: 11323, borderline nodes: 131, overall activation: 5975.478, activation diff: 516.096, ratio: 0.086
#   pulse 11: activated nodes: 11327, borderline nodes: 76, overall activation: 6391.967, activation diff: 416.489, ratio: 0.065
#   pulse 12: activated nodes: 11332, borderline nodes: 58, overall activation: 6725.442, activation diff: 333.475, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6725.4
#   number of spread. activ. pulses: 12
#   running time: 491

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860848   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98122036   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9785548   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97826785   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97541463   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9636158   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.85403335   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8538015   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8533646   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8533323   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.8531568   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.85291046   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.85220534   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.85188484   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.85177463   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.851586   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.8511817   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.851051   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8509485   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.85093796   REFERENCES
