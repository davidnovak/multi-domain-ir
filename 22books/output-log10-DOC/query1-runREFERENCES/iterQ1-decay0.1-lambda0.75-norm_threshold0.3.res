###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 112.955, activation diff: 102.395, ratio: 0.907
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 329.742, activation diff: 216.833, ratio: 0.658
#   pulse 4: activated nodes: 9676, borderline nodes: 5492, overall activation: 964.782, activation diff: 635.040, ratio: 0.658
#   pulse 5: activated nodes: 10842, borderline nodes: 3773, overall activation: 1862.097, activation diff: 897.316, ratio: 0.482
#   pulse 6: activated nodes: 11141, borderline nodes: 2602, overall activation: 2822.747, activation diff: 960.650, ratio: 0.340
#   pulse 7: activated nodes: 11214, borderline nodes: 1531, overall activation: 3719.177, activation diff: 896.430, ratio: 0.241
#   pulse 8: activated nodes: 11280, borderline nodes: 826, overall activation: 4501.855, activation diff: 782.678, ratio: 0.174
#   pulse 9: activated nodes: 11307, borderline nodes: 516, overall activation: 5161.552, activation diff: 659.698, ratio: 0.128
#   pulse 10: activated nodes: 11313, borderline nodes: 341, overall activation: 5706.311, activation diff: 544.759, ratio: 0.095
#   pulse 11: activated nodes: 11315, borderline nodes: 220, overall activation: 6149.903, activation diff: 443.592, ratio: 0.072
#   pulse 12: activated nodes: 11321, borderline nodes: 160, overall activation: 6507.650, activation diff: 357.748, ratio: 0.055
#   pulse 13: activated nodes: 11324, borderline nodes: 126, overall activation: 6794.224, activation diff: 286.574, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 6794.2
#   number of spread. activ. pulses: 13
#   running time: 516

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886664   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9844115   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.98194337   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9815479   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97965217   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96821713   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8642254   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8639864   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8638668   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8637993   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.8636582   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.86316305   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8626881   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.86252326   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.8624313   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.8624149   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.8619325   REFERENCES
1   Q1   politicalsketche00retsrich_156   18   0.86189127   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.86187726   REFERENCES
1   Q1   politicalsketche00retsrich_153   20   0.8617711   REFERENCES
