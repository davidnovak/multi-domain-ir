###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 998.682, activation diff: 978.818, ratio: 0.980
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2052.803, activation diff: 1054.121, ratio: 0.514
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4234.000, activation diff: 2181.196, ratio: 0.515
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5831.151, activation diff: 1597.151, ratio: 0.274
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 6831.202, activation diff: 1000.051, ratio: 0.146
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 7419.694, activation diff: 588.492, ratio: 0.079
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 7757.001, activation diff: 337.307, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7757.0
#   number of spread. activ. pulses: 8
#   running time: 430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954045   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9949101   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947801   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99356735   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9900326   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89216113   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8920734   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8920283   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.8919558   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.8919029   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.891854   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.8917777   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.8915602   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.89152634   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   16   0.89150894   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.89143705   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.89143705   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.89141834   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.89139295   REFERENCES
