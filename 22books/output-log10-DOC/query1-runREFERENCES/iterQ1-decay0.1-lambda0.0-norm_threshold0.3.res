###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2327.692, activation diff: 2354.424, ratio: 1.011
#   pulse 3: activated nodes: 8566, borderline nodes: 3415, overall activation: 1128.369, activation diff: 3456.061, ratio: 3.063
#   pulse 4: activated nodes: 10694, borderline nodes: 2939, overall activation: 5835.058, activation diff: 6963.427, ratio: 1.193
#   pulse 5: activated nodes: 11254, borderline nodes: 1284, overall activation: 1900.006, activation diff: 7735.064, ratio: 4.071
#   pulse 6: activated nodes: 11324, borderline nodes: 118, overall activation: 5961.221, activation diff: 7861.227, ratio: 1.319
#   pulse 7: activated nodes: 11332, borderline nodes: 49, overall activation: 1924.227, activation diff: 7885.448, ratio: 4.098
#   pulse 8: activated nodes: 11335, borderline nodes: 18, overall activation: 5969.472, activation diff: 7893.700, ratio: 1.322
#   pulse 9: activated nodes: 11335, borderline nodes: 16, overall activation: 1926.781, activation diff: 7896.254, ratio: 4.098
#   pulse 10: activated nodes: 11336, borderline nodes: 16, overall activation: 5970.787, activation diff: 7897.568, ratio: 1.323
#   pulse 11: activated nodes: 11336, borderline nodes: 16, overall activation: 1927.345, activation diff: 7898.132, ratio: 4.098
#   pulse 12: activated nodes: 11336, borderline nodes: 14, overall activation: 5971.125, activation diff: 7898.470, ratio: 1.323
#   pulse 13: activated nodes: 11336, borderline nodes: 14, overall activation: 1927.511, activation diff: 7898.636, ratio: 4.098
#   pulse 14: activated nodes: 11336, borderline nodes: 14, overall activation: 5971.230, activation diff: 7898.740, ratio: 1.323
#   pulse 15: activated nodes: 11340, borderline nodes: 17, overall activation: 1927.567, activation diff: 7898.797, ratio: 4.098

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 1927.6
#   number of spread. activ. pulses: 15
#   running time: 561

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
