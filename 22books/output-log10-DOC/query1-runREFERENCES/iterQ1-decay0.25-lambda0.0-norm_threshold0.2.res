###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2160.947, activation diff: 2190.112, ratio: 1.013
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 756.596, activation diff: 2917.543, ratio: 3.856
#   pulse 4: activated nodes: 10803, borderline nodes: 2629, overall activation: 4134.580, activation diff: 4891.175, ratio: 1.183
#   pulse 5: activated nodes: 11265, borderline nodes: 973, overall activation: 1104.679, activation diff: 5239.259, ratio: 4.743
#   pulse 6: activated nodes: 11298, borderline nodes: 516, overall activation: 4179.180, activation diff: 5283.859, ratio: 1.264
#   pulse 7: activated nodes: 11305, borderline nodes: 470, overall activation: 1113.819, activation diff: 5292.999, ratio: 4.752
#   pulse 8: activated nodes: 11305, borderline nodes: 470, overall activation: 4181.389, activation diff: 5295.208, ratio: 1.266
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1114.382, activation diff: 5295.771, ratio: 4.752
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4181.632, activation diff: 5296.014, ratio: 1.266
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1114.446, activation diff: 5296.078, ratio: 4.752
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4181.683, activation diff: 5296.129, ratio: 1.267
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1114.457, activation diff: 5296.139, ratio: 4.752
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 4181.696, activation diff: 5296.152, ratio: 1.267
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1114.459, activation diff: 5296.155, ratio: 4.752

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 1114.5
#   number of spread. activ. pulses: 15
#   running time: 621

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
