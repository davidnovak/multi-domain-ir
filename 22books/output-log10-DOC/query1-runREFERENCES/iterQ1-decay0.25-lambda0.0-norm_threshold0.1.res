###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2360.289, activation diff: 2391.789, ratio: 1.013
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 851.166, activation diff: 3211.455, ratio: 3.773
#   pulse 4: activated nodes: 10835, borderline nodes: 2265, overall activation: 4169.031, activation diff: 5020.197, ratio: 1.204
#   pulse 5: activated nodes: 11302, borderline nodes: 591, overall activation: 1152.599, activation diff: 5321.631, ratio: 4.617
#   pulse 6: activated nodes: 11303, borderline nodes: 498, overall activation: 4203.790, activation diff: 5356.390, ratio: 1.274
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 1159.114, activation diff: 5362.904, ratio: 4.627
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4205.323, activation diff: 5364.437, ratio: 1.276
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1159.470, activation diff: 5364.793, ratio: 4.627
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4205.480, activation diff: 5364.949, ratio: 1.276
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1159.505, activation diff: 5364.985, ratio: 4.627
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4205.506, activation diff: 5365.011, ratio: 1.276
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1159.510, activation diff: 5365.016, ratio: 4.627
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 4205.512, activation diff: 5365.021, ratio: 1.276
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1159.511, activation diff: 5365.022, ratio: 4.627

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 1159.5
#   number of spread. activ. pulses: 15
#   running time: 540

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
