###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1579.233, activation diff: 1582.553, ratio: 1.002
#   pulse 3: activated nodes: 8645, borderline nodes: 3406, overall activation: 1915.206, activation diff: 1135.706, ratio: 0.593
#   pulse 4: activated nodes: 10773, borderline nodes: 2939, overall activation: 5260.860, activation diff: 3440.082, ratio: 0.654
#   pulse 5: activated nodes: 11252, borderline nodes: 1296, overall activation: 6940.213, activation diff: 1681.772, ratio: 0.242
#   pulse 6: activated nodes: 11321, borderline nodes: 142, overall activation: 7621.098, activation diff: 680.885, ratio: 0.089
#   pulse 7: activated nodes: 11331, borderline nodes: 48, overall activation: 7864.648, activation diff: 243.550, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 7864.6
#   number of spread. activ. pulses: 7
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99929893   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9990708   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9989159   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99820584   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99643314   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.89924335   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.89922595   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8992172   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8991986   REFERENCES
1   Q1   essentialsinmod01howegoog_293   11   0.8991691   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.8991456   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   13   0.8991413   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.8991134   REFERENCES
1   Q1   politicalsketche00retsrich_156   15   0.8990977   REFERENCES
1   Q1   essentialsinmod01howegoog_187   16   0.89907634   REFERENCES
1   Q1   europesincenapol00leveuoft_60   17   0.8990735   REFERENCES
1   Q1   europesincenapol00leveuoft_34   18   0.8990579   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.8990419   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.8990358   REFERENCES
