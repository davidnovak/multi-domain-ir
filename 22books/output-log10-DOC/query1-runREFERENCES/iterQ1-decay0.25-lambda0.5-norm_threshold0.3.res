###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 501.923, activation diff: 489.066, ratio: 0.974
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 917.942, activation diff: 420.901, ratio: 0.459
#   pulse 4: activated nodes: 10241, borderline nodes: 4817, overall activation: 2275.390, activation diff: 1357.448, ratio: 0.597
#   pulse 5: activated nodes: 11142, borderline nodes: 2595, overall activation: 3366.585, activation diff: 1091.194, ratio: 0.324
#   pulse 6: activated nodes: 11224, borderline nodes: 1172, overall activation: 4101.976, activation diff: 735.391, ratio: 0.179
#   pulse 7: activated nodes: 11252, borderline nodes: 681, overall activation: 4556.108, activation diff: 454.132, ratio: 0.100
#   pulse 8: activated nodes: 11266, borderline nodes: 597, overall activation: 4825.808, activation diff: 269.700, ratio: 0.056
#   pulse 9: activated nodes: 11266, borderline nodes: 570, overall activation: 4982.841, activation diff: 157.032, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11266
#   final overall activation: 4982.8
#   number of spread. activ. pulses: 9
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977342   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99698865   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9963821   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99604815   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99531925   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99141526   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.74608076   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7460708   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.74604404   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.745994   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.745947   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7457496   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.7457471   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.74571025   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7457069   REFERENCES
1   Q1   politicalsketche00retsrich_156   16   0.7455662   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.74555224   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7455448   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.7455448   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.7455359   REFERENCES
