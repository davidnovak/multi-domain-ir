###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 198.898, activation diff: 186.856, ratio: 0.939
#   pulse 3: activated nodes: 8515, borderline nodes: 3425, overall activation: 511.414, activation diff: 312.516, ratio: 0.611
#   pulse 4: activated nodes: 10711, borderline nodes: 4212, overall activation: 1184.439, activation diff: 673.025, ratio: 0.568
#   pulse 5: activated nodes: 11187, borderline nodes: 2118, overall activation: 1970.482, activation diff: 786.042, ratio: 0.399
#   pulse 6: activated nodes: 11241, borderline nodes: 884, overall activation: 2727.581, activation diff: 757.100, ratio: 0.278
#   pulse 7: activated nodes: 11299, borderline nodes: 538, overall activation: 3394.597, activation diff: 667.016, ratio: 0.196
#   pulse 8: activated nodes: 11314, borderline nodes: 347, overall activation: 3957.131, activation diff: 562.535, ratio: 0.142
#   pulse 9: activated nodes: 11319, borderline nodes: 194, overall activation: 4420.361, activation diff: 463.229, ratio: 0.105
#   pulse 10: activated nodes: 11325, borderline nodes: 151, overall activation: 4796.243, activation diff: 375.882, ratio: 0.078
#   pulse 11: activated nodes: 11329, borderline nodes: 90, overall activation: 5098.243, activation diff: 302.000, ratio: 0.059
#   pulse 12: activated nodes: 11331, borderline nodes: 71, overall activation: 5339.191, activation diff: 240.948, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5339.2
#   number of spread. activ. pulses: 12
#   running time: 498

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98694146   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9827535   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9807614   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.980074   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97710097   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9672624   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7603331   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7602072   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.75970614   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7595807   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.75954247   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.75947624   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.75906855   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7582827   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7582759   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.757937   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.75790924   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7577436   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.75774056   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.75768256   REFERENCES
