###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2591.947, activation diff: 2621.112, ratio: 1.011
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1280.815, activation diff: 3872.763, ratio: 3.024
#   pulse 4: activated nodes: 10805, borderline nodes: 2560, overall activation: 5884.123, activation diff: 7164.939, ratio: 1.218
#   pulse 5: activated nodes: 11268, borderline nodes: 895, overall activation: 1985.864, activation diff: 7869.987, ratio: 3.963
#   pulse 6: activated nodes: 11327, borderline nodes: 82, overall activation: 5988.519, activation diff: 7974.383, ratio: 1.332
#   pulse 7: activated nodes: 11334, borderline nodes: 20, overall activation: 2006.044, activation diff: 7994.563, ratio: 3.985
#   pulse 8: activated nodes: 11336, borderline nodes: 6, overall activation: 5995.220, activation diff: 8001.264, ratio: 1.335
#   pulse 9: activated nodes: 11341, borderline nodes: 9, overall activation: 2008.355, activation diff: 8003.575, ratio: 3.985
#   pulse 10: activated nodes: 11341, borderline nodes: 9, overall activation: 5996.299, activation diff: 8004.654, ratio: 1.335
#   pulse 11: activated nodes: 11341, borderline nodes: 6, overall activation: 2008.897, activation diff: 8005.196, ratio: 3.985
#   pulse 12: activated nodes: 11341, borderline nodes: 6, overall activation: 5996.578, activation diff: 8005.475, ratio: 1.335
#   pulse 13: activated nodes: 11341, borderline nodes: 6, overall activation: 2009.067, activation diff: 8005.645, ratio: 3.985
#   pulse 14: activated nodes: 11341, borderline nodes: 6, overall activation: 5996.666, activation diff: 8005.732, ratio: 1.335
#   pulse 15: activated nodes: 11341, borderline nodes: 6, overall activation: 2009.127, activation diff: 8005.793, ratio: 3.985

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 2009.1
#   number of spread. activ. pulses: 15
#   running time: 689

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
