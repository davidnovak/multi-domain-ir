###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1580.972, activation diff: 1580.523, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1978.792, activation diff: 870.996, ratio: 0.440
#   pulse 4: activated nodes: 10810, borderline nodes: 2443, overall activation: 4424.790, activation diff: 2467.979, ratio: 0.558
#   pulse 5: activated nodes: 11273, borderline nodes: 782, overall activation: 5570.244, activation diff: 1145.535, ratio: 0.206
#   pulse 6: activated nodes: 11325, borderline nodes: 125, overall activation: 6004.443, activation diff: 434.198, ratio: 0.072
#   pulse 7: activated nodes: 11331, borderline nodes: 61, overall activation: 6155.819, activation diff: 151.377, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 6155.8
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99951935   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99932027   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9992985   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984616   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9969791   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7995093   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7994947   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.799471   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7994671   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.79946685   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7994124   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7994033   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.7993946   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.7993648   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7993626   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.79934835   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   18   0.7993467   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7993214   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.7993214   REFERENCES
