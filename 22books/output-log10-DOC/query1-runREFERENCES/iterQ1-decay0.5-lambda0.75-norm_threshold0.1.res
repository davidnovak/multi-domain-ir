###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 130.468, activation diff: 118.425, ratio: 0.908
#   pulse 3: activated nodes: 8515, borderline nodes: 3425, overall activation: 301.236, activation diff: 170.768, ratio: 0.567
#   pulse 4: activated nodes: 8658, borderline nodes: 3408, overall activation: 556.087, activation diff: 254.851, ratio: 0.458
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 819.865, activation diff: 263.778, ratio: 0.322
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1060.812, activation diff: 240.947, ratio: 0.227
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1268.546, activation diff: 207.734, ratio: 0.164
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1442.084, activation diff: 173.537, ratio: 0.120
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1584.288, activation diff: 142.204, ratio: 0.090
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1699.342, activation diff: 115.054, ratio: 0.068
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1791.599, activation diff: 92.257, ratio: 0.051
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1865.090, activation diff: 73.491, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1865.1
#   number of spread. activ. pulses: 12
#   running time: 461

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869367   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98268145   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9806252   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9799029   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9769225   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9664835   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4750079   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.474782   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47472557   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.4745304   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.47446957   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.47421375   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4739172   REFERENCES
1   Q1   politicalsketche00retsrich_153   14   0.47350287   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.47337684   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.47336796   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.47324482   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.4729051   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.4728913   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.47287953   REFERENCES
