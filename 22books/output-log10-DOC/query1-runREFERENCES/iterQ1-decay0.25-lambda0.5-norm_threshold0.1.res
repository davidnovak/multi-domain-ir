###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 726.493, activation diff: 708.422, ratio: 0.975
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1367.124, activation diff: 640.630, ratio: 0.469
#   pulse 4: activated nodes: 10791, borderline nodes: 3040, overall activation: 2744.766, activation diff: 1377.642, ratio: 0.502
#   pulse 5: activated nodes: 11243, borderline nodes: 1112, overall activation: 3781.263, activation diff: 1036.498, ratio: 0.274
#   pulse 6: activated nodes: 11296, borderline nodes: 528, overall activation: 4440.120, activation diff: 658.857, ratio: 0.148
#   pulse 7: activated nodes: 11303, borderline nodes: 473, overall activation: 4834.021, activation diff: 393.901, ratio: 0.081
#   pulse 8: activated nodes: 11305, borderline nodes: 470, overall activation: 5063.063, activation diff: 229.042, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5063.1
#   number of spread. activ. pulses: 8
#   running time: 435

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951081   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9943402   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99433625   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9931162   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98877037   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.74318314   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7430774   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.7430742   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.743013   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7429202   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.74285924   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.74269223   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.74256134   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.74255234   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   16   0.7425438   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7424401   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.7424401   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.74241275   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.7424079   REFERENCES
