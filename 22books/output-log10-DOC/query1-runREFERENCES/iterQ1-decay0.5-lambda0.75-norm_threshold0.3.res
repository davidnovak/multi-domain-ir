###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 68.983, activation diff: 58.423, ratio: 0.847
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 180.319, activation diff: 111.382, ratio: 0.618
#   pulse 4: activated nodes: 8001, borderline nodes: 4195, overall activation: 393.366, activation diff: 213.046, ratio: 0.542
#   pulse 5: activated nodes: 8494, borderline nodes: 3489, overall activation: 640.559, activation diff: 247.193, ratio: 0.386
#   pulse 6: activated nodes: 8635, borderline nodes: 3411, overall activation: 880.471, activation diff: 239.913, ratio: 0.272
#   pulse 7: activated nodes: 8654, borderline nodes: 3403, overall activation: 1094.717, activation diff: 214.245, ratio: 0.196
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 1277.860, activation diff: 183.143, ratio: 0.143
#   pulse 9: activated nodes: 8660, borderline nodes: 3406, overall activation: 1430.431, activation diff: 152.572, ratio: 0.107
#   pulse 10: activated nodes: 8660, borderline nodes: 3406, overall activation: 1555.441, activation diff: 125.009, ratio: 0.080
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1656.701, activation diff: 101.260, ratio: 0.061
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1738.042, activation diff: 81.341, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1738.0
#   number of spread. activ. pulses: 12
#   running time: 438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9848644   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.978936   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9754689   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.97454387   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9729801   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95662373   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.47311705   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.47301346   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.47281784   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.47262996   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.47228283   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.4720525   REFERENCES
1   Q1   politicalsketche00retsrich_153   13   0.47182631   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.4717671   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   15   0.4713178   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.4713053   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.47124282   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.47093573   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4707803   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.47070804   REFERENCES
