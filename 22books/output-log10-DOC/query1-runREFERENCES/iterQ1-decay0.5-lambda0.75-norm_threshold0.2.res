###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 96.331, activation diff: 85.009, ratio: 0.882
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 236.047, activation diff: 139.736, ratio: 0.592
#   pulse 4: activated nodes: 8311, borderline nodes: 3741, overall activation: 473.342, activation diff: 237.295, ratio: 0.501
#   pulse 5: activated nodes: 8653, borderline nodes: 3404, overall activation: 731.543, activation diff: 258.202, ratio: 0.353
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 973.374, activation diff: 241.830, ratio: 0.248
#   pulse 7: activated nodes: 8660, borderline nodes: 3406, overall activation: 1184.957, activation diff: 211.584, ratio: 0.179
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1363.454, activation diff: 178.497, ratio: 0.131
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1510.772, activation diff: 147.318, ratio: 0.098
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1630.624, activation diff: 119.852, ratio: 0.074
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1727.160, activation diff: 96.536, ratio: 0.056
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1804.347, activation diff: 77.186, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1804.3
#   number of spread. activ. pulses: 12
#   running time: 433

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98607355   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98106694   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.97817755   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97795314   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9751213   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9622328   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.47416878   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.47395107   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.4738422   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.47375512   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.47349444   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.47313678   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4730693   REFERENCES
1   Q1   politicalsketche00retsrich_153   14   0.47272396   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.47242972   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.47240397   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.47236013   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.47199094   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.47189948   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.47187844   REFERENCES
