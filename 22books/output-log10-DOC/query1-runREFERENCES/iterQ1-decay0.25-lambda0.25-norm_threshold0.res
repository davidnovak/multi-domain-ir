###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1634.207, activation diff: 1630.001, ratio: 0.997
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2148.633, activation diff: 747.772, ratio: 0.348
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4107.114, activation diff: 1958.481, ratio: 0.477
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4964.179, activation diff: 857.065, ratio: 0.173
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 5272.572, activation diff: 308.393, ratio: 0.058
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 5376.943, activation diff: 104.371, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5376.9
#   number of spread. activ. pulses: 7
#   running time: 421

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9996454   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9995237   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99948144   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99866295   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974204   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7496545   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7496353   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.749626   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7496111   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7495965   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7495923   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7495664   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7495613   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.74955785   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.7495402   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.74952275   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.749501   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.74948806   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.74948806   REFERENCES
