###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1741.901, activation diff: 1737.695, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2378.875, activation diff: 885.877, ratio: 0.372
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4702.033, activation diff: 2323.158, ratio: 0.494
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5738.993, activation diff: 1036.960, ratio: 0.181
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 6117.800, activation diff: 378.807, ratio: 0.062
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 6246.293, activation diff: 128.494, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 6246.3
#   number of spread. activ. pulses: 7
#   running time: 419

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9996456   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9995237   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994817   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99866295   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974204   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7996321   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.79961103   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7996038   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.79958534   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.79957455   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7995654   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7995381   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7995367   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7995354   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.7995143   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.7994924   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.79947406   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.79946953   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.79946315   REFERENCES
