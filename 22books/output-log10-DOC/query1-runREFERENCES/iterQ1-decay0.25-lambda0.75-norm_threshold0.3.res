###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 96.465, activation diff: 85.905, ratio: 0.891
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 271.180, activation diff: 174.760, ratio: 0.644
#   pulse 4: activated nodes: 9656, borderline nodes: 5518, overall activation: 708.610, activation diff: 437.430, ratio: 0.617
#   pulse 5: activated nodes: 10684, borderline nodes: 4254, overall activation: 1297.646, activation diff: 589.036, ratio: 0.454
#   pulse 6: activated nodes: 11042, borderline nodes: 2999, overall activation: 1919.716, activation diff: 622.070, ratio: 0.324
#   pulse 7: activated nodes: 11175, borderline nodes: 2107, overall activation: 2498.603, activation diff: 578.887, ratio: 0.232
#   pulse 8: activated nodes: 11218, borderline nodes: 1403, overall activation: 3004.669, activation diff: 506.067, ratio: 0.168
#   pulse 9: activated nodes: 11262, borderline nodes: 927, overall activation: 3432.066, activation diff: 427.397, ratio: 0.125
#   pulse 10: activated nodes: 11283, borderline nodes: 700, overall activation: 3785.554, activation diff: 353.488, ratio: 0.093
#   pulse 11: activated nodes: 11287, borderline nodes: 614, overall activation: 4073.910, activation diff: 288.356, ratio: 0.071
#   pulse 12: activated nodes: 11295, borderline nodes: 586, overall activation: 4307.009, activation diff: 233.098, ratio: 0.054
#   pulse 13: activated nodes: 11298, borderline nodes: 538, overall activation: 4494.312, activation diff: 187.303, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11298
#   final overall activation: 4494.3
#   number of spread. activ. pulses: 13
#   running time: 511

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98866177   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9843555   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.98184556   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.98137844   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.979571   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9677978   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.720083   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7198719   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71976554   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7197415   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7196099   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.71918344   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7185921   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.7185631   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7184807   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7182723   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.7181426   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7179738   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.717958   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.71793324   REFERENCES
