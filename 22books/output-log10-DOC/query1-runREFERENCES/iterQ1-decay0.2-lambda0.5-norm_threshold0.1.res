###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 773.569, activation diff: 755.498, ratio: 0.977
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1491.283, activation diff: 717.714, ratio: 0.481
#   pulse 4: activated nodes: 10792, borderline nodes: 2962, overall activation: 3114.460, activation diff: 1623.178, ratio: 0.521
#   pulse 5: activated nodes: 11247, borderline nodes: 1060, overall activation: 4344.321, activation diff: 1229.860, ratio: 0.283
#   pulse 6: activated nodes: 11313, borderline nodes: 326, overall activation: 5129.999, activation diff: 785.678, ratio: 0.153
#   pulse 7: activated nodes: 11328, borderline nodes: 107, overall activation: 5601.037, activation diff: 471.038, ratio: 0.084
#   pulse 8: activated nodes: 11331, borderline nodes: 64, overall activation: 5874.932, activation diff: 273.896, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5874.9
#   number of spread. activ. pulses: 8
#   running time: 430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951087   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9943425   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99433625   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99311805   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9887736   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7927314   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7926215   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.79261255   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.79255646   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7924498   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.79238564   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7922088   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.79211366   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7920878   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   16   0.79205966   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.79195285   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.79195285   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.7919351   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.7919173   REFERENCES
