###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1483.165, activation diff: 1482.716, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1783.399, activation diff: 743.840, ratio: 0.417
#   pulse 4: activated nodes: 10806, borderline nodes: 2480, overall activation: 3865.636, activation diff: 2099.882, ratio: 0.543
#   pulse 5: activated nodes: 11269, borderline nodes: 821, overall activation: 4820.367, activation diff: 954.773, ratio: 0.198
#   pulse 6: activated nodes: 11299, borderline nodes: 512, overall activation: 5175.956, activation diff: 355.589, ratio: 0.069
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 5299.788, activation diff: 123.832, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5299.8
#   number of spread. activ. pulses: 7
#   running time: 414

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995191   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993199   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9992985   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984615   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9969791   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7495394   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7495234   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.749504   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7495003   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.749495   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.74944043   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.74943703   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.7494322   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.7493996   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.7493874   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.7493869   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.74936783   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7493543   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.7493543   REFERENCES
