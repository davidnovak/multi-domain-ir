###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1318.238, activation diff: 1321.558, ratio: 1.003
#   pulse 3: activated nodes: 8645, borderline nodes: 3406, overall activation: 1419.628, activation diff: 767.891, ratio: 0.541
#   pulse 4: activated nodes: 10772, borderline nodes: 3056, overall activation: 3623.638, activation diff: 2257.530, ratio: 0.623
#   pulse 5: activated nodes: 11241, borderline nodes: 1431, overall activation: 4662.519, activation diff: 1039.497, ratio: 0.223
#   pulse 6: activated nodes: 11286, borderline nodes: 551, overall activation: 5068.603, activation diff: 406.084, ratio: 0.080
#   pulse 7: activated nodes: 11303, borderline nodes: 477, overall activation: 5214.611, activation diff: 146.008, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 5214.6
#   number of spread. activ. pulses: 7
#   running time: 410

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992983   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99906945   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9989159   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9982052   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964323   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.7493608   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.7493464   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7493391   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.749332   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.74928445   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7492701   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7492547   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.74921656   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   15   0.74919385   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.7491912   REFERENCES
1   Q1   europesincenapol00leveuoft_16   17   0.7491694   REFERENCES
1   Q1   politicalsketche00retsrich_156   18   0.7491578   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7491573   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.7491573   REFERENCES
