###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 258.347, activation diff: 245.628, ratio: 0.951
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 645.714, activation diff: 387.367, ratio: 0.600
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1388.949, activation diff: 743.235, ratio: 0.535
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2207.424, activation diff: 818.475, ratio: 0.371
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2967.810, activation diff: 760.386, ratio: 0.256
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3623.871, activation diff: 656.060, ratio: 0.181
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4169.720, activation diff: 545.850, ratio: 0.131
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 4614.677, activation diff: 444.957, ratio: 0.096
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 4972.759, activation diff: 358.082, ratio: 0.072
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 5258.463, activation diff: 285.704, ratio: 0.054
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 5485.062, activation diff: 226.599, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5485.1
#   number of spread. activ. pulses: 12
#   running time: 493

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756146   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98397887   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9824641   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9815818   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9785899   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9703692   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.76135284   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7612632   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7607365   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.76071477   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.76053077   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.76047003   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.76037854   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.75939643   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.75935286   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.75915575   REFERENCES
1   Q1   essentialsinmod01howegoog_293   17   0.7590016   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7589754   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.7589748   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.7588556   REFERENCES
