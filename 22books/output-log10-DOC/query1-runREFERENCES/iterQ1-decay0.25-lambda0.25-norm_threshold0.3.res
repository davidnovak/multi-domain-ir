###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1146.065, activation diff: 1151.270, ratio: 1.005
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1133.785, activation diff: 745.621, ratio: 0.658
#   pulse 4: activated nodes: 10496, borderline nodes: 3753, overall activation: 3415.334, activation diff: 2366.657, ratio: 0.693
#   pulse 5: activated nodes: 11202, borderline nodes: 2024, overall activation: 4498.297, activation diff: 1084.400, ratio: 0.241
#   pulse 6: activated nodes: 11247, borderline nodes: 656, overall activation: 4946.280, activation diff: 447.982, ratio: 0.091
#   pulse 7: activated nodes: 11273, borderline nodes: 554, overall activation: 5110.843, activation diff: 164.564, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11273
#   final overall activation: 5110.8
#   number of spread. activ. pulses: 7
#   running time: 410

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99902475   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987618   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9984945   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99788886   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9958372   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.74916303   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.74914026   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.74911875   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.74911165   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   11   0.74906427   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7490386   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   13   0.7490272   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.7490247   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   15   0.7489693   REFERENCES
1   Q1   politicalsketche00retsrich_156   16   0.7489545   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.7489529   REFERENCES
1   Q1   essentialsinmod01howegoog_187   18   0.7489376   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7489297   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.7489297   REFERENCES
