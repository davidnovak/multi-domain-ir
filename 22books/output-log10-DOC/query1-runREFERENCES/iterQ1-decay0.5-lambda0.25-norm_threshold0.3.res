###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 767.831, activation diff: 773.036, ratio: 1.007
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 556.897, activation diff: 294.636, ratio: 0.529
#   pulse 4: activated nodes: 8397, borderline nodes: 3551, overall activation: 1502.063, activation diff: 945.305, ratio: 0.629
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 1868.572, activation diff: 366.519, ratio: 0.196
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1989.508, activation diff: 120.937, ratio: 0.061
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2026.821, activation diff: 37.313, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2026.8
#   number of spread. activ. pulses: 7
#   running time: 386

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999022   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987513   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9984943   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99787676   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9957999   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.4994267   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.49939886   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49939388   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.49933732   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49927348   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   12   0.49923414   REFERENCES
1   Q1   politicalsketche00retsrich_159   13   0.49923414   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.49923176   REFERENCES
1   Q1   politicalsketche00retsrich_153   15   0.49918628   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.4991723   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.49916753   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.49913707   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.4991156   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49911258   REFERENCES
