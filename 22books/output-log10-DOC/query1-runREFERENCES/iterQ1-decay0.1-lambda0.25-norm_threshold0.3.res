###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1373.005, activation diff: 1378.210, ratio: 1.004
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1539.472, activation diff: 1075.765, ratio: 0.699
#   pulse 4: activated nodes: 10505, borderline nodes: 3612, overall activation: 4957.327, activation diff: 3566.946, ratio: 0.720
#   pulse 5: activated nodes: 11219, borderline nodes: 1853, overall activation: 6699.572, activation diff: 1749.442, ratio: 0.261
#   pulse 6: activated nodes: 11302, borderline nodes: 262, overall activation: 7448.054, activation diff: 748.483, ratio: 0.100
#   pulse 7: activated nodes: 11330, borderline nodes: 102, overall activation: 7723.392, activation diff: 275.338, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 7723.4
#   number of spread. activ. pulses: 7
#   running time: 414

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99902564   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99876416   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99849457   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978906   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99584293   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.89900744   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.8989896   REFERENCES
1   Q1   essentialsinmod01howegoog_293   9   0.89895177   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8989427   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.8989358   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.89890105   REFERENCES
1   Q1   politicalsketche00retsrich_156   13   0.89886594   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.8988552   REFERENCES
1   Q1   essentialsinmod01howegoog_187   15   0.89883876   REFERENCES
1   Q1   europesincenapol00leveuoft_60   16   0.89883506   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   17   0.89883256   REFERENCES
1   Q1   europesincenapol00leveuoft_34   18   0.8988175   REFERENCES
1   Q1   essentialsinmod01howegoog_12   19   0.89880055   REFERENCES
1   Q1   shorthistoryofmo00haslrich_134   20   0.89878786   REFERENCES
