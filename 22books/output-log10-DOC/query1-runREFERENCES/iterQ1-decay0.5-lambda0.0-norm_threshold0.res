###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1694.275, activation diff: 1728.005, ratio: 1.020
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.163, activation diff: 1756.438, ratio: 28.255
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.927, activation diff: 2175.090, ratio: 1.029
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.278, activation diff: 2175.205, ratio: 34.927
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.974, activation diff: 2175.252, ratio: 1.029
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.282, activation diff: 2175.256, ratio: 34.926
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.975, activation diff: 2175.257, ratio: 1.029
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.282, activation diff: 2175.257, ratio: 34.926
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.975, activation diff: 2175.257, ratio: 1.029
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.282, activation diff: 2175.257, ratio: 34.926
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.975, activation diff: 2175.257, ratio: 1.029
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.282, activation diff: 2175.257, ratio: 34.926
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.975, activation diff: 2175.257, ratio: 1.029
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.282, activation diff: 2175.257, ratio: 34.926

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 62.3
#   number of spread. activ. pulses: 15
#   running time: 506

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
