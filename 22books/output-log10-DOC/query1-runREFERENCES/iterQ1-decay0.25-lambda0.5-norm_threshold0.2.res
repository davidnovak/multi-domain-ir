###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 611.419, activation diff: 595.891, ratio: 0.975
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 1132.505, activation diff: 521.747, ratio: 0.461
#   pulse 4: activated nodes: 10541, borderline nodes: 3924, overall activation: 2507.508, activation diff: 1375.003, ratio: 0.548
#   pulse 5: activated nodes: 11203, borderline nodes: 1944, overall activation: 3576.554, activation diff: 1069.047, ratio: 0.299
#   pulse 6: activated nodes: 11249, borderline nodes: 692, overall activation: 4274.553, activation diff: 697.999, ratio: 0.163
#   pulse 7: activated nodes: 11271, borderline nodes: 544, overall activation: 4697.558, activation diff: 423.005, ratio: 0.090
#   pulse 8: activated nodes: 11273, borderline nodes: 524, overall activation: 4945.649, activation diff: 248.091, ratio: 0.050
#   pulse 9: activated nodes: 11277, borderline nodes: 512, overall activation: 5088.701, activation diff: 143.052, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11277
#   final overall activation: 5088.7
#   number of spread. activ. pulses: 9
#   running time: 447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9973247   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99681425   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99672186   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99572134   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99251544   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.74638474   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.74633366   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.7462995   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.7462938   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7462629   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.74613965   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7460462   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.74602807   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.746009   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.74600303   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.74591684   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.74590015   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.74590015   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.7458668   REFERENCES
