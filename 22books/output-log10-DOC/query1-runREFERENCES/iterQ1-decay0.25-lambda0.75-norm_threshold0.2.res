###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 136.875, activation diff: 125.552, ratio: 0.917
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 363.569, activation diff: 226.716, ratio: 0.624
#   pulse 4: activated nodes: 10216, borderline nodes: 5024, overall activation: 880.553, activation diff: 516.983, ratio: 0.587
#   pulse 5: activated nodes: 10964, borderline nodes: 3320, overall activation: 1518.200, activation diff: 637.648, ratio: 0.420
#   pulse 6: activated nodes: 11175, borderline nodes: 2216, overall activation: 2154.980, activation diff: 636.780, ratio: 0.295
#   pulse 7: activated nodes: 11226, borderline nodes: 1241, overall activation: 2729.816, activation diff: 574.836, ratio: 0.211
#   pulse 8: activated nodes: 11271, borderline nodes: 774, overall activation: 3222.457, activation diff: 492.641, ratio: 0.153
#   pulse 9: activated nodes: 11285, borderline nodes: 627, overall activation: 3632.414, activation diff: 409.956, ratio: 0.113
#   pulse 10: activated nodes: 11295, borderline nodes: 586, overall activation: 3967.607, activation diff: 335.194, ratio: 0.084
#   pulse 11: activated nodes: 11295, borderline nodes: 542, overall activation: 4238.681, activation diff: 271.073, ratio: 0.064
#   pulse 12: activated nodes: 11295, borderline nodes: 530, overall activation: 4456.233, activation diff: 217.552, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11295
#   final overall activation: 4456.2
#   number of spread. activ. pulses: 12
#   running time: 493

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98608184   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9811827   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.97846204   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9781903   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97534037   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96324533   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7115908   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.711428   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71106845   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.71098256   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7108599   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.71063745   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7100418   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7095705   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.70951295   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7092218   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.70908594   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.7090056   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7089702   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.70895416   REFERENCES
