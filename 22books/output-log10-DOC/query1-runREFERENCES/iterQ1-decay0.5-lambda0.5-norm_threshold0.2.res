###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 413.701, activation diff: 398.173, ratio: 0.962
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 665.242, activation diff: 251.982, ratio: 0.379
#   pulse 4: activated nodes: 8452, borderline nodes: 3468, overall activation: 1171.305, activation diff: 506.063, ratio: 0.432
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 1543.296, activation diff: 371.990, ratio: 0.241
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1776.171, activation diff: 232.875, ratio: 0.131
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1913.474, activation diff: 137.303, ratio: 0.072
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1991.982, activation diff: 78.509, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1992.0
#   number of spread. activ. pulses: 8
#   running time: 400

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9957644   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9946574   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9937022   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9934528   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99252975   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98710346   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49514604   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.49511132   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.49506098   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.49502423   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.49480852   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.49478227   REFERENCES
1   Q1   politicalsketche00retsrich_153   13   0.4946748   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.4946413   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.49453783   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.4945378   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.49452525   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.4943703   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49433047   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.4943064   REFERENCES
