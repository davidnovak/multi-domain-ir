###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1095.737, activation diff: 1091.531, ratio: 0.996
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1093.292, activation diff: 153.120, ratio: 0.140
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1790.187, activation diff: 696.895, ratio: 0.389
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 2053.555, activation diff: 263.368, ratio: 0.128
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2138.636, activation diff: 85.080, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2138.6
#   number of spread. activ. pulses: 6
#   running time: 363

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991119   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99861175   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.998152   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9981189   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972737   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9947218   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49906808   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.49904597   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.49901408   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.49900138   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.49889812   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.49884212   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.49883276   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.498774   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.498774   REFERENCES
1   Q1   politicalsketche00retsrich_153   16   0.49875912   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.4987165   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.49864835   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.49863052   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.4986291   REFERENCES
