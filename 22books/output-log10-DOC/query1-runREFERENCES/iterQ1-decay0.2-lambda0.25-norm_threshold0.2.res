###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1405.236, activation diff: 1408.556, ratio: 1.002
#   pulse 3: activated nodes: 8645, borderline nodes: 3406, overall activation: 1579.913, activation diff: 885.588, ratio: 0.561
#   pulse 4: activated nodes: 10773, borderline nodes: 3011, overall activation: 4146.171, activation diff: 2632.755, ratio: 0.635
#   pulse 5: activated nodes: 11244, borderline nodes: 1370, overall activation: 5385.600, activation diff: 1240.609, ratio: 0.230
#   pulse 6: activated nodes: 11303, borderline nodes: 318, overall activation: 5879.651, activation diff: 494.052, ratio: 0.084
#   pulse 7: activated nodes: 11322, borderline nodes: 169, overall activation: 6057.163, activation diff: 177.512, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11322
#   final overall activation: 6057.2
#   number of spread. activ. pulses: 7
#   running time: 469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99956536   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992985   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99907005   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9989159   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99820554   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99643266   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.7993216   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.79930353   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7993012   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7992877   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.7992368   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.79923177   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7992092   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.79920554   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.7991432   REFERENCES
1   Q1   politicalsketche00retsrich_156   16   0.7991424   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.79914206   REFERENCES
1   Q1   essentialsinmod01howegoog_187   18   0.7991319   REFERENCES
1   Q1   europesincenapol00leveuoft_60   19   0.7991261   REFERENCES
1   Q1   europesincenapol00leveuoft_34   20   0.79912233   REFERENCES
