###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1940.731, activation diff: 1967.463, ratio: 1.014
#   pulse 3: activated nodes: 8566, borderline nodes: 3415, overall activation: 665.083, activation diff: 2605.814, ratio: 3.918
#   pulse 4: activated nodes: 10693, borderline nodes: 3051, overall activation: 4094.301, activation diff: 4759.384, ratio: 1.162
#   pulse 5: activated nodes: 11242, borderline nodes: 1411, overall activation: 1050.071, activation diff: 5144.372, ratio: 4.899
#   pulse 6: activated nodes: 11287, borderline nodes: 557, overall activation: 4151.701, activation diff: 5201.772, ratio: 1.253
#   pulse 7: activated nodes: 11303, borderline nodes: 491, overall activation: 1062.090, activation diff: 5213.791, ratio: 4.909
#   pulse 8: activated nodes: 11303, borderline nodes: 491, overall activation: 4154.714, activation diff: 5216.804, ratio: 1.256
#   pulse 9: activated nodes: 11304, borderline nodes: 490, overall activation: 1062.902, activation diff: 5217.616, ratio: 4.909
#   pulse 10: activated nodes: 11304, borderline nodes: 490, overall activation: 4155.033, activation diff: 5217.935, ratio: 1.256
#   pulse 11: activated nodes: 11304, borderline nodes: 490, overall activation: 1063.002, activation diff: 5218.035, ratio: 4.909
#   pulse 12: activated nodes: 11304, borderline nodes: 490, overall activation: 4155.110, activation diff: 5218.112, ratio: 1.256
#   pulse 13: activated nodes: 11304, borderline nodes: 490, overall activation: 1063.021, activation diff: 5218.132, ratio: 4.909
#   pulse 14: activated nodes: 11304, borderline nodes: 490, overall activation: 4155.136, activation diff: 5218.157, ratio: 1.256
#   pulse 15: activated nodes: 11304, borderline nodes: 490, overall activation: 1063.026, activation diff: 5218.162, ratio: 4.909

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 1063.0
#   number of spread. activ. pulses: 15
#   running time: 658

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
