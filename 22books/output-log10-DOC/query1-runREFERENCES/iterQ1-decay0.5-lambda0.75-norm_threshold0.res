###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 168.044, activation diff: 155.325, ratio: 0.924
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 368.388, activation diff: 200.344, ratio: 0.544
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 636.291, activation diff: 267.903, ratio: 0.421
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 903.066, activation diff: 266.774, ratio: 0.295
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1142.041, activation diff: 238.976, ratio: 0.209
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1345.652, activation diff: 203.611, ratio: 0.151
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1514.373, activation diff: 168.721, ratio: 0.111
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1651.801, activation diff: 137.428, ratio: 0.083
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1762.467, activation diff: 110.666, ratio: 0.063
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1850.863, activation diff: 88.396, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1850.9
#   number of spread. activ. pulses: 11
#   running time: 427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98341185   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9785892   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.97654134   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9753393   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9717086   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96063143   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4675826   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.46736816   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.46717697   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.46701562   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.46693096   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.46676657   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.46622333   REFERENCES
1   Q1   politicalsketche00retsrich_153   14   0.46568787   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.46562266   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.46561867   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.4654278   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.46511695   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.46505812   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.46500382   REFERENCES
