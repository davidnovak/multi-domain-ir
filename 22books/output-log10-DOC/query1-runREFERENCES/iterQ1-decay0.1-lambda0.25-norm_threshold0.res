###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1957.289, activation diff: 1953.083, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2855.084, activation diff: 1177.810, ratio: 0.413
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 5956.085, activation diff: 3101.002, ratio: 0.521
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 7384.823, activation diff: 1428.738, ratio: 0.193
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 7916.325, activation diff: 531.502, ratio: 0.067
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 8097.569, activation diff: 181.244, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 8097.6
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999778   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9996458   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9995237   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994819   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.998663   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99742043   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8995867   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8995623   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.8995596   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.89953345   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.89953107   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.8995112   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8994868   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.899481   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.89947975   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.89945877   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.8994581   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.8994297   REFERENCES
1   Q1   politicalsketche00retsrich_156   19   0.89942455   REFERENCES
1   Q1   essentialsinmod01howegoog_187   20   0.89941835   REFERENCES
