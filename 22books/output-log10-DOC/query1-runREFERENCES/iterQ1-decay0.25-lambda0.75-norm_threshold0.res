###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 243.297, activation diff: 230.578, ratio: 0.948
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 596.979, activation diff: 353.682, ratio: 0.592
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1237.977, activation diff: 640.998, ratio: 0.518
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1937.023, activation diff: 699.046, ratio: 0.361
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2584.731, activation diff: 647.708, ratio: 0.251
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 3143.062, activation diff: 558.331, ratio: 0.178
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3607.296, activation diff: 464.233, ratio: 0.129
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 3985.569, activation diff: 378.273, ratio: 0.095
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4289.984, activation diff: 304.415, ratio: 0.071
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 4532.936, activation diff: 242.953, ratio: 0.054
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4725.706, activation diff: 192.770, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4725.7
#   number of spread. activ. pulses: 12
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756117   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9839741   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.982458   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9815692   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97857505   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97031057   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7137436   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.71367264   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7131614   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.713153   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7129652   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.71291566   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.71282583   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.71185046   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7117957   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.71162516   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.71150005   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.7114992   REFERENCES
1   Q1   essentialsinmod01howegoog_293   19   0.7114388   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.71133816   REFERENCES
