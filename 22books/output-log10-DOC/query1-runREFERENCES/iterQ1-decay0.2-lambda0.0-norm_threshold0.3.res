###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2069.718, activation diff: 2096.450, ratio: 1.013
#   pulse 3: activated nodes: 8566, borderline nodes: 3415, overall activation: 812.188, activation diff: 2881.906, ratio: 3.548
#   pulse 4: activated nodes: 10693, borderline nodes: 3014, overall activation: 4656.152, activation diff: 5468.340, ratio: 1.174
#   pulse 5: activated nodes: 11250, borderline nodes: 1370, overall activation: 1309.781, activation diff: 5965.933, ratio: 4.555
#   pulse 6: activated nodes: 11310, borderline nodes: 281, overall activation: 4736.468, activation diff: 6046.249, ratio: 1.277
#   pulse 7: activated nodes: 11320, borderline nodes: 218, overall activation: 1324.842, activation diff: 6061.310, ratio: 4.575
#   pulse 8: activated nodes: 11323, borderline nodes: 185, overall activation: 4740.626, activation diff: 6065.468, ratio: 1.279
#   pulse 9: activated nodes: 11323, borderline nodes: 183, overall activation: 1325.926, activation diff: 6066.552, ratio: 4.575
#   pulse 10: activated nodes: 11323, borderline nodes: 181, overall activation: 4741.225, activation diff: 6067.151, ratio: 1.280
#   pulse 11: activated nodes: 11323, borderline nodes: 181, overall activation: 1326.075, activation diff: 6067.300, ratio: 4.575
#   pulse 12: activated nodes: 11323, borderline nodes: 181, overall activation: 4741.343, activation diff: 6067.418, ratio: 1.280
#   pulse 13: activated nodes: 11323, borderline nodes: 181, overall activation: 1326.105, activation diff: 6067.448, ratio: 4.575
#   pulse 14: activated nodes: 11323, borderline nodes: 181, overall activation: 4741.372, activation diff: 6067.477, ratio: 1.280
#   pulse 15: activated nodes: 11323, borderline nodes: 181, overall activation: 1326.112, activation diff: 6067.485, ratio: 4.575

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 1326.1
#   number of spread. activ. pulses: 15
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
