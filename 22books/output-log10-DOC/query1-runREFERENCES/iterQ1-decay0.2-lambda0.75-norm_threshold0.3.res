###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 101.962, activation diff: 91.402, ratio: 0.896
#   pulse 3: activated nodes: 7593, borderline nodes: 4595, overall activation: 290.369, activation diff: 188.453, ratio: 0.649
#   pulse 4: activated nodes: 9667, borderline nodes: 5511, overall activation: 787.306, activation diff: 496.937, ratio: 0.631
#   pulse 5: activated nodes: 10749, borderline nodes: 4055, overall activation: 1470.237, activation diff: 682.931, ratio: 0.465
#   pulse 6: activated nodes: 11088, borderline nodes: 2831, overall activation: 2196.863, activation diff: 726.627, ratio: 0.331
#   pulse 7: activated nodes: 11203, borderline nodes: 1930, overall activation: 2875.087, activation diff: 678.223, ratio: 0.236
#   pulse 8: activated nodes: 11230, borderline nodes: 1132, overall activation: 3468.945, activation diff: 593.858, ratio: 0.171
#   pulse 9: activated nodes: 11279, borderline nodes: 753, overall activation: 3970.755, activation diff: 501.810, ratio: 0.126
#   pulse 10: activated nodes: 11290, borderline nodes: 596, overall activation: 4385.755, activation diff: 415.000, ratio: 0.095
#   pulse 11: activated nodes: 11307, borderline nodes: 544, overall activation: 4724.487, activation diff: 338.732, ratio: 0.072
#   pulse 12: activated nodes: 11312, borderline nodes: 468, overall activation: 4998.453, activation diff: 273.966, ratio: 0.055
#   pulse 13: activated nodes: 11312, borderline nodes: 413, overall activation: 5218.542, activation diff: 220.089, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11312
#   final overall activation: 5218.5
#   number of spread. activ. pulses: 13
#   running time: 533

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98866343   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98437643   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9818823   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.98144144   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97960126   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96795166   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7681312   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.76791745   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.76779294   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.76777506   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7676238   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7671772   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7666361   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.76651746   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7665045   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7663212   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.76601887   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.7659631   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7659191   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.7658948   REFERENCES
