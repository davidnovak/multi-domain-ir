###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2304.614, activation diff: 2333.779, ratio: 1.013
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 923.666, activation diff: 3228.280, ratio: 3.495
#   pulse 4: activated nodes: 10805, borderline nodes: 2592, overall activation: 4701.328, activation diff: 5624.995, ratio: 1.196
#   pulse 5: activated nodes: 11268, borderline nodes: 933, overall activation: 1374.691, activation diff: 6076.019, ratio: 4.420
#   pulse 6: activated nodes: 11319, borderline nodes: 167, overall activation: 4764.863, activation diff: 6139.554, ratio: 1.289
#   pulse 7: activated nodes: 11325, borderline nodes: 123, overall activation: 1386.491, activation diff: 6151.354, ratio: 4.437
#   pulse 8: activated nodes: 11326, borderline nodes: 103, overall activation: 4768.126, activation diff: 6154.617, ratio: 1.291
#   pulse 9: activated nodes: 11326, borderline nodes: 103, overall activation: 1387.283, activation diff: 6155.409, ratio: 4.437
#   pulse 10: activated nodes: 11326, borderline nodes: 100, overall activation: 4768.538, activation diff: 6155.821, ratio: 1.291
#   pulse 11: activated nodes: 11326, borderline nodes: 100, overall activation: 1387.381, activation diff: 6155.919, ratio: 4.437
#   pulse 12: activated nodes: 11326, borderline nodes: 100, overall activation: 4768.608, activation diff: 6155.989, ratio: 1.291
#   pulse 13: activated nodes: 11326, borderline nodes: 100, overall activation: 1387.398, activation diff: 6156.006, ratio: 4.437
#   pulse 14: activated nodes: 11326, borderline nodes: 100, overall activation: 4768.623, activation diff: 6156.021, ratio: 1.291
#   pulse 15: activated nodes: 11326, borderline nodes: 100, overall activation: 1387.402, activation diff: 6156.025, ratio: 4.437

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 1387.4
#   number of spread. activ. pulses: 15
#   running time: 538

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
