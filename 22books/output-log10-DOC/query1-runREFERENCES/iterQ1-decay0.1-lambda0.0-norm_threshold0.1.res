###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2831.155, activation diff: 2862.654, ratio: 1.011
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1434.928, activation diff: 4266.083, ratio: 2.973
#   pulse 4: activated nodes: 10835, borderline nodes: 2194, overall activation: 5925.065, activation diff: 7359.993, ratio: 1.242
#   pulse 5: activated nodes: 11303, borderline nodes: 516, overall activation: 2065.571, activation diff: 7990.636, ratio: 3.868
#   pulse 6: activated nodes: 11332, borderline nodes: 59, overall activation: 6013.137, activation diff: 8078.708, ratio: 1.344
#   pulse 7: activated nodes: 11339, borderline nodes: 12, overall activation: 2081.767, activation diff: 8094.904, ratio: 3.888
#   pulse 8: activated nodes: 11341, borderline nodes: 7, overall activation: 6018.364, activation diff: 8100.131, ratio: 1.346
#   pulse 9: activated nodes: 11341, borderline nodes: 5, overall activation: 2083.684, activation diff: 8102.048, ratio: 3.888
#   pulse 10: activated nodes: 11341, borderline nodes: 1, overall activation: 6019.235, activation diff: 8102.919, ratio: 1.346
#   pulse 11: activated nodes: 11341, borderline nodes: 1, overall activation: 2084.158, activation diff: 8103.392, ratio: 3.888
#   pulse 12: activated nodes: 11341, borderline nodes: 1, overall activation: 6019.461, activation diff: 8103.619, ratio: 1.346
#   pulse 13: activated nodes: 11341, borderline nodes: 1, overall activation: 2084.313, activation diff: 8103.775, ratio: 3.888
#   pulse 14: activated nodes: 11341, borderline nodes: 1, overall activation: 6019.534, activation diff: 8103.847, ratio: 1.346
#   pulse 15: activated nodes: 11341, borderline nodes: 1, overall activation: 2084.371, activation diff: 8103.905, ratio: 3.888

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 2084.4
#   number of spread. activ. pulses: 15
#   running time: 1588

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
