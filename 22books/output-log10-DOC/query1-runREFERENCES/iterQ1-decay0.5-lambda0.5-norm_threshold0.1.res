###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 491.117, activation diff: 473.046, ratio: 0.963
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 793.765, activation diff: 302.648, ratio: 0.381
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1281.717, activation diff: 487.952, ratio: 0.381
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1630.554, activation diff: 348.837, ratio: 0.214
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1846.457, activation diff: 215.903, ratio: 0.117
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1972.913, activation diff: 126.456, ratio: 0.064
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2044.891, activation diff: 71.978, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2044.9
#   number of spread. activ. pulses: 8
#   running time: 387

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99510354   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9943356   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9943197   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99309254   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98871976   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49542695   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.49536324   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.495349   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.49530572   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.4952031   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.49509677   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4950778   REFERENCES
1   Q1   politicalsketche00retsrich_153   14   0.49492857   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.49489385   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.49489385   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.49483943   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.4947056   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49470043   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.49467173   REFERENCES
