###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 564.774, activation diff: 544.909, ratio: 0.965
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 914.602, activation diff: 349.829, ratio: 0.382
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1384.331, activation diff: 469.728, ratio: 0.339
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1711.432, activation diff: 327.101, ratio: 0.191
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1911.662, activation diff: 200.230, ratio: 0.105
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2028.184, activation diff: 116.522, ratio: 0.057
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2094.214, activation diff: 66.030, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2094.2
#   number of spread. activ. pulses: 8
#   running time: 367

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954004   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9949099   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947618   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9935485   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99000144   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4956184   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.49556857   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49554417   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.49548554   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.49546927   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.49540213   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.49529824   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49515986   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.49515986   REFERENCES
1   Q1   politicalsketche00retsrich_153   16   0.49512938   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.49508178   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.49501365   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49498314   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.49495304   REFERENCES
