###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 339.980, activation diff: 327.123, ratio: 0.962
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 544.282, activation diff: 207.557, ratio: 0.381
#   pulse 4: activated nodes: 8214, borderline nodes: 3919, overall activation: 1060.129, activation diff: 515.847, ratio: 0.487
#   pulse 5: activated nodes: 8656, borderline nodes: 3403, overall activation: 1452.799, activation diff: 392.670, ratio: 0.270
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1702.172, activation diff: 249.373, ratio: 0.147
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1850.428, activation diff: 148.255, ratio: 0.080
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1935.687, activation diff: 85.259, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1935.7
#   number of spread. activ. pulses: 8
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9954683   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9939828   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9928358   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99210215   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9918283   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98502797   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.49475923   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49473277   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.49472606   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.49454337   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49438447   REFERENCES
1   Q1   politicalsketche00retsrich_153   12   0.4943522   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.49422508   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   14   0.4941165   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.4940608   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.49406072   REFERENCES
1   Q1   essentialsinmod01howegoog_293   17   0.4939894   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   18   0.49396598   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.49384743   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49384522   REFERENCES
