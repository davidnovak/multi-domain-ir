###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 994.129, activation diff: 993.681, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 900.544, activation diff: 202.170, ratio: 0.224
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1693.999, activation diff: 793.455, ratio: 0.468
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1994.628, activation diff: 300.630, ratio: 0.151
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2091.872, activation diff: 97.244, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2091.9
#   number of spread. activ. pulses: 6
#   running time: 360

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988718   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99811035   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9975283   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9972208   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9967437   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9934882   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.49877664   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49875933   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.4987384   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.498662   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49853063   REFERENCES
1   Q1   politicalsketche00retsrich_153   12   0.49850655   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.49846902   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.498431   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.498431   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.49840975   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.4983523   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.49828342   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.4982815   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49827152   REFERENCES
