###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 650.963, activation diff: 635.435, ratio: 0.976
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 1233.906, activation diff: 583.649, ratio: 0.473
#   pulse 4: activated nodes: 10544, borderline nodes: 3854, overall activation: 2845.434, activation diff: 1611.528, ratio: 0.566
#   pulse 5: activated nodes: 11206, borderline nodes: 1883, overall activation: 4109.532, activation diff: 1264.098, ratio: 0.308
#   pulse 6: activated nodes: 11283, borderline nodes: 608, overall activation: 4938.232, activation diff: 828.700, ratio: 0.168
#   pulse 7: activated nodes: 11314, borderline nodes: 368, overall activation: 5441.742, activation diff: 503.510, ratio: 0.093
#   pulse 8: activated nodes: 11319, borderline nodes: 224, overall activation: 5737.471, activation diff: 295.729, ratio: 0.052
#   pulse 9: activated nodes: 11325, borderline nodes: 177, overall activation: 5908.107, activation diff: 170.636, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11325
#   final overall activation: 5908.1
#   number of spread. activ. pulses: 9
#   running time: 454

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99732524   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99681616   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99672186   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.995723   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99251926   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7961458   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7960942   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7960551   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.7960528   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7960148   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7958841   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7958007   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.79578996   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.795773   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.7957456   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.7956568   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.79564214   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.79564214   REFERENCES
1   Q1   politicalsketche00retsrich_156   20   0.7956412   REFERENCES
