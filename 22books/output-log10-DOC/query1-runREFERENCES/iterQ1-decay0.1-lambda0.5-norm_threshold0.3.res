###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 599.089, activation diff: 586.231, ratio: 0.979
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 1167.450, activation diff: 574.220, ratio: 0.492
#   pulse 4: activated nodes: 10259, borderline nodes: 4710, overall activation: 3255.613, activation diff: 2088.163, ratio: 0.641
#   pulse 5: activated nodes: 11168, borderline nodes: 2377, overall activation: 4964.540, activation diff: 1708.927, ratio: 0.344
#   pulse 6: activated nodes: 11283, borderline nodes: 707, overall activation: 6129.056, activation diff: 1164.516, ratio: 0.190
#   pulse 7: activated nodes: 11313, borderline nodes: 284, overall activation: 6851.867, activation diff: 722.811, ratio: 0.105
#   pulse 8: activated nodes: 11322, borderline nodes: 142, overall activation: 7282.054, activation diff: 430.186, ratio: 0.059
#   pulse 9: activated nodes: 11331, borderline nodes: 70, overall activation: 7532.897, activation diff: 250.843, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 7532.9
#   number of spread. activ. pulses: 9
#   running time: 453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99773425   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9969908   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99638987   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9960487   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9953259   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99143755   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89530337   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.89530283   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.89528275   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8951951   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.8951365   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.89506185   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8949466   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.89487374   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.894858   REFERENCES
1   Q1   politicalsketche00retsrich_156   16   0.8948442   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.894754   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.89470816   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.89470816   REFERENCES
1   Q1   essentialsinmod01howegoog_187   20   0.89469224   REFERENCES
