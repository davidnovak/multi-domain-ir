###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 144.983, activation diff: 133.661, ratio: 0.922
#   pulse 3: activated nodes: 8063, borderline nodes: 4126, overall activation: 390.710, activation diff: 245.747, ratio: 0.629
#   pulse 4: activated nodes: 10223, borderline nodes: 4991, overall activation: 983.555, activation diff: 592.846, ratio: 0.603
#   pulse 5: activated nodes: 11018, borderline nodes: 3186, overall activation: 1725.493, activation diff: 741.937, ratio: 0.430
#   pulse 6: activated nodes: 11196, borderline nodes: 2043, overall activation: 2470.899, activation diff: 745.407, ratio: 0.302
#   pulse 7: activated nodes: 11233, borderline nodes: 1011, overall activation: 3145.031, activation diff: 674.131, ratio: 0.214
#   pulse 8: activated nodes: 11286, borderline nodes: 648, overall activation: 3722.782, activation diff: 577.751, ratio: 0.155
#   pulse 9: activated nodes: 11308, borderline nodes: 519, overall activation: 4203.433, activation diff: 480.651, ratio: 0.114
#   pulse 10: activated nodes: 11313, borderline nodes: 425, overall activation: 4596.280, activation diff: 392.847, ratio: 0.085
#   pulse 11: activated nodes: 11314, borderline nodes: 333, overall activation: 4913.705, activation diff: 317.424, ratio: 0.065
#   pulse 12: activated nodes: 11318, borderline nodes: 265, overall activation: 5168.168, activation diff: 254.464, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 5168.2
#   number of spread. activ. pulses: 12
#   running time: 503

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860829   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9811969   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9784971   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97821957   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97536796   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96338177   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.75907266   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7588934   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.75849867   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7584439   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7582899   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7580627   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.75742865   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7570227   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.75694525   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7566774   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.75640374   REFERENCES
1   Q1   politicalsketche00retsrich_153   18   0.7563584   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.75629246   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.7562913   REFERENCES
