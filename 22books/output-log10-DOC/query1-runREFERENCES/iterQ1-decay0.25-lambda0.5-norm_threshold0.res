###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 835.967, activation diff: 816.102, ratio: 0.976
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1601.238, activation diff: 765.272, ratio: 0.478
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2976.962, activation diff: 1375.724, ratio: 0.462
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3971.218, activation diff: 994.256, ratio: 0.250
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4588.824, activation diff: 617.607, ratio: 0.135
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 4952.379, activation diff: 363.555, ratio: 0.073
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 5161.097, activation diff: 208.718, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5161.1
#   number of spread. activ. pulses: 8
#   running time: 439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954034   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9949101   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947761   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9935645   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9900291   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7434629   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74339443   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.74334425   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.7432755   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.7432488   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.7432097   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.74314195   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.74290466   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7429018   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7428507   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7428262   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.7428262   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.74282324   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.7427621   REFERENCES
