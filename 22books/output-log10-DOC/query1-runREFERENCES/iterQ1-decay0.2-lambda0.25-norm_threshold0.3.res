###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1221.712, activation diff: 1226.917, ratio: 1.004
#   pulse 3: activated nodes: 8397, borderline nodes: 3551, overall activation: 1264.596, activation diff: 851.251, ratio: 0.673
#   pulse 4: activated nodes: 10499, borderline nodes: 3700, overall activation: 3905.453, activation diff: 2746.325, ratio: 0.703
#   pulse 5: activated nodes: 11210, borderline nodes: 1957, overall activation: 5194.183, activation diff: 1291.665, ratio: 0.249
#   pulse 6: activated nodes: 11293, borderline nodes: 564, overall activation: 5736.150, activation diff: 541.967, ratio: 0.094
#   pulse 7: activated nodes: 11316, borderline nodes: 362, overall activation: 5935.721, activation diff: 199.571, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 5935.7
#   number of spread. activ. pulses: 7
#   running time: 485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932766   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9990251   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987628   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99849457   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99788964   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9958397   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.799111   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.79908985   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.79906017   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.79905325   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   11   0.7990134   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.79900575   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7989793   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   14   0.79896235   REFERENCES
1   Q1   politicalsketche00retsrich_156   15   0.7989302   REFERENCES
1   Q1   essentialsinmod01howegoog_187   16   0.79891497   REFERENCES
1   Q1   europesincenapol00leveuoft_60   17   0.7989079   REFERENCES
1   Q1   europesincenapol00leveuoft_34   18   0.7989036   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.79890287   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.79888976   REFERENCES
