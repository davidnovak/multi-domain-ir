###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1776.586, activation diff: 1776.138, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2385.168, activation diff: 1140.899, ratio: 0.478
#   pulse 4: activated nodes: 10810, borderline nodes: 2399, overall activation: 5610.389, activation diff: 3256.531, ratio: 0.580
#   pulse 5: activated nodes: 11275, borderline nodes: 733, overall activation: 7173.156, activation diff: 1562.914, ratio: 0.218
#   pulse 6: activated nodes: 11329, borderline nodes: 82, overall activation: 7775.627, activation diff: 602.471, ratio: 0.077
#   pulse 7: activated nodes: 11334, borderline nodes: 14, overall activation: 7985.942, activation diff: 210.315, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7985.9
#   number of spread. activ. pulses: 7
#   running time: 410

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99971795   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99951965   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99932075   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9992985   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984617   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99697924   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8994487   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.8994374   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.899411   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.89940476   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.8994005   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.89934784   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.89933926   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.8993314   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.8993192   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.89929056   REFERENCES
1   Q1   politicalsketche00retsrich_156   17   0.8992893   REFERENCES
1   Q1   essentialsinmod01howegoog_187   18   0.89927584   REFERENCES
1   Q1   europesincenapol00leveuoft_60   19   0.8992736   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.89926773   REFERENCES
