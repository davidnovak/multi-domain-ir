###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 730.050, activation diff: 714.522, ratio: 0.979
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 1443.725, activation diff: 714.469, ratio: 0.495
#   pulse 4: activated nodes: 10549, borderline nodes: 3746, overall activation: 3579.629, activation diff: 2135.904, ratio: 0.597
#   pulse 5: activated nodes: 11216, borderline nodes: 1735, overall activation: 5266.495, activation diff: 1686.866, ratio: 0.320
#   pulse 6: activated nodes: 11298, borderline nodes: 386, overall activation: 6378.042, activation diff: 1111.547, ratio: 0.174
#   pulse 7: activated nodes: 11326, borderline nodes: 113, overall activation: 7054.390, activation diff: 676.348, ratio: 0.096
#   pulse 8: activated nodes: 11332, borderline nodes: 53, overall activation: 7451.420, activation diff: 397.030, ratio: 0.053
#   pulse 9: activated nodes: 11334, borderline nodes: 36, overall activation: 7680.700, activation diff: 229.281, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7680.7
#   number of spread. activ. pulses: 9
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99788225   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99732625   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99681926   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.996722   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99572533   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99252415   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89566684   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.8956158   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.8955787   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.89555943   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.89551735   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.8953717   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.895349   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8952961   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.8952545   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.8952178   REFERENCES
1   Q1   politicalsketche00retsrich_156   17   0.8951813   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.89512813   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8951279   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.8951279   REFERENCES
