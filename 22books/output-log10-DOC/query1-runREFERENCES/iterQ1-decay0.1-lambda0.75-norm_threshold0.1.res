###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 221.708, activation diff: 209.666, ratio: 0.946
#   pulse 3: activated nodes: 8515, borderline nodes: 3425, overall activation: 587.325, activation diff: 365.617, ratio: 0.623
#   pulse 4: activated nodes: 10724, borderline nodes: 4139, overall activation: 1463.627, activation diff: 876.302, ratio: 0.599
#   pulse 5: activated nodes: 11197, borderline nodes: 1966, overall activation: 2501.439, activation diff: 1037.812, ratio: 0.415
#   pulse 6: activated nodes: 11279, borderline nodes: 716, overall activation: 3502.043, activation diff: 1000.604, ratio: 0.286
#   pulse 7: activated nodes: 11313, borderline nodes: 296, overall activation: 4382.123, activation diff: 880.081, ratio: 0.201
#   pulse 8: activated nodes: 11323, borderline nodes: 114, overall activation: 5123.422, activation diff: 741.299, ratio: 0.145
#   pulse 9: activated nodes: 11330, borderline nodes: 60, overall activation: 5732.606, activation diff: 609.183, ratio: 0.106
#   pulse 10: activated nodes: 11333, borderline nodes: 44, overall activation: 6225.616, activation diff: 493.010, ratio: 0.079
#   pulse 11: activated nodes: 11334, borderline nodes: 23, overall activation: 6620.709, activation diff: 395.094, ratio: 0.060
#   pulse 12: activated nodes: 11334, borderline nodes: 17, overall activation: 6935.238, activation diff: 314.529, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 6935.2
#   number of spread. activ. pulses: 12
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869424   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9827664   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9807835   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.980105   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.977135   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9674139   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8554337   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8552561   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.85471433   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8545852   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.8545545   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.8544766   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.85402656   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8532605   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.8532296   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.8529598   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.8528298   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.85256445   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.85256344   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.8525614   REFERENCES
