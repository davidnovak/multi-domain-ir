###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 534.312, activation diff: 521.454, ratio: 0.976
#   pulse 3: activated nodes: 8214, borderline nodes: 3919, overall activation: 999.193, activation diff: 470.089, ratio: 0.470
#   pulse 4: activated nodes: 10252, borderline nodes: 4776, overall activation: 2582.440, activation diff: 1583.247, ratio: 0.613
#   pulse 5: activated nodes: 11162, borderline nodes: 2516, overall activation: 3867.392, activation diff: 1284.952, ratio: 0.332
#   pulse 6: activated nodes: 11256, borderline nodes: 961, overall activation: 4739.078, activation diff: 871.686, ratio: 0.184
#   pulse 7: activated nodes: 11295, borderline nodes: 562, overall activation: 5278.679, activation diff: 539.601, ratio: 0.102
#   pulse 8: activated nodes: 11313, borderline nodes: 448, overall activation: 5599.652, activation diff: 320.973, ratio: 0.057
#   pulse 9: activated nodes: 11316, borderline nodes: 338, overall activation: 5786.657, activation diff: 187.006, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 5786.7
#   number of spread. activ. pulses: 9
#   running time: 427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99773425   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99698937   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9963851   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99604833   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.995322   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99142474   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.795822   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.7958146   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7957899   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.79572815   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.7956768   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.7955242   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.79548705   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7954325   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.79542667   REFERENCES
1   Q1   politicalsketche00retsrich_156   16   0.79533124   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.79528457   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7952652   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.7952652   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.7952529   REFERENCES
