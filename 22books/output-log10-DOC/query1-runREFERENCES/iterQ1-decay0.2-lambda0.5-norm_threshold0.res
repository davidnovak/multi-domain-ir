###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 890.205, activation diff: 870.340, ratio: 0.978
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1748.875, activation diff: 858.670, ratio: 0.491
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3377.623, activation diff: 1628.748, ratio: 0.482
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4562.984, activation diff: 1185.361, ratio: 0.260
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5302.798, activation diff: 739.814, ratio: 0.140
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 5738.996, activation diff: 436.198, ratio: 0.076
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 5989.357, activation diff: 250.361, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5989.4
#   number of spread. activ. pulses: 8
#   running time: 434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959933   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954038   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9949101   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947777   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9935657   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99003065   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.79302937   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7929541   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.7929054   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.7928353   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.79280066   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.79275835   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.79268765   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7924478   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.79244244   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.7924248   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7923626   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.7923626   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7923578   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.7923045   REFERENCES
