###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 187.493, activation diff: 175.451, ratio: 0.936
#   pulse 3: activated nodes: 8515, borderline nodes: 3425, overall activation: 474.479, activation diff: 286.985, ratio: 0.605
#   pulse 4: activated nodes: 10703, borderline nodes: 4250, overall activation: 1057.706, activation diff: 583.228, ratio: 0.551
#   pulse 5: activated nodes: 11174, borderline nodes: 2250, overall activation: 1730.658, activation diff: 672.952, ratio: 0.389
#   pulse 6: activated nodes: 11228, borderline nodes: 973, overall activation: 2376.512, activation diff: 645.853, ratio: 0.272
#   pulse 7: activated nodes: 11285, borderline nodes: 600, overall activation: 2945.134, activation diff: 568.622, ratio: 0.193
#   pulse 8: activated nodes: 11298, borderline nodes: 529, overall activation: 3424.408, activation diff: 479.274, ratio: 0.140
#   pulse 9: activated nodes: 11299, borderline nodes: 516, overall activation: 3818.801, activation diff: 394.393, ratio: 0.103
#   pulse 10: activated nodes: 11303, borderline nodes: 476, overall activation: 4138.656, activation diff: 319.855, ratio: 0.077
#   pulse 11: activated nodes: 11304, borderline nodes: 473, overall activation: 4395.608, activation diff: 256.952, ratio: 0.058
#   pulse 12: activated nodes: 11305, borderline nodes: 472, overall activation: 4600.669, activation diff: 205.061, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4600.7
#   number of spread. activ. pulses: 12
#   running time: 496

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98694086   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98274577   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98074734   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98005533   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9770809   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9671724   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7127813   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.71267533   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.71220446   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7120647   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.71203446   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.71197903   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.71159   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.71078295   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7107745   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.7104391   REFERENCES
1   Q1   essentialsinmod01howegoog_293   17   0.71042335   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.71033865   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.71033484   REFERENCES
1   Q1   politicalsketche00retsrich_153   20   0.7102543   REFERENCES
