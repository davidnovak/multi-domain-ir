###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 867.719, activation diff: 849.648, ratio: 0.979
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1747.698, activation diff: 879.979, ratio: 0.504
#   pulse 4: activated nodes: 10793, borderline nodes: 2890, overall activation: 3911.104, activation diff: 2163.406, ratio: 0.553
#   pulse 5: activated nodes: 11264, borderline nodes: 969, overall activation: 5559.192, activation diff: 1648.088, ratio: 0.296
#   pulse 6: activated nodes: 11324, borderline nodes: 145, overall activation: 6616.715, activation diff: 1057.522, ratio: 0.160
#   pulse 7: activated nodes: 11334, borderline nodes: 46, overall activation: 7249.008, activation diff: 632.293, ratio: 0.087
#   pulse 8: activated nodes: 11334, borderline nodes: 18, overall activation: 7615.755, activation diff: 366.748, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7615.8
#   number of spread. activ. pulses: 8
#   running time: 428

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99591494   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9951098   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99434626   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9943363   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9931207   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9887775   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8918267   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.8917105   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.8916892   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.8916446   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.8915071   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.8914368   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.8912401   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.89122224   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.8911253   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   16   0.89108086   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.8909843   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8909806   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.8909806   REFERENCES
1   Q1   politicalsketche00retsrich_156   20   0.89096487   REFERENCES
