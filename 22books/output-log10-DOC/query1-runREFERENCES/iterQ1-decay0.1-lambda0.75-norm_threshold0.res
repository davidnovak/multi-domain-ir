###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 288.448, activation diff: 275.729, ratio: 0.956
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 745.807, activation diff: 457.359, ratio: 0.613
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1717.208, activation diff: 971.401, ratio: 0.566
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2799.292, activation diff: 1082.084, ratio: 0.387
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3805.216, activation diff: 1005.925, ratio: 0.264
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4671.964, activation diff: 866.747, ratio: 0.186
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5391.670, activation diff: 719.707, ratio: 0.133
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 5976.862, activation diff: 585.192, ratio: 0.098
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 6446.641, activation diff: 469.779, ratio: 0.073
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 6820.665, activation diff: 374.024, ratio: 0.055
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 7116.776, activation diff: 296.111, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7116.8
#   number of spread. activ. pulses: 12
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756194   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9839867   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9824734   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98160297   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97861505   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9704655   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.85656834   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.85643446   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.85588187   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8558422   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.8556402   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.8555837   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.85548395   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.85445917   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.85443324   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.85419756   REFERENCES
1   Q1   essentialsinmod01howegoog_293   17   0.85412157   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8539349   REFERENCES
1   Q1   politicalsketche00retsrich_159   19   0.8539346   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.8538475   REFERENCES
