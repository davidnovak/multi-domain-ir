###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1810.713, activation diff: 1805.707, ratio: 0.997
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 2812.964, activation diff: 1270.813, ratio: 0.452
#   pulse 4: activated nodes: 11398, borderline nodes: 2351, overall activation: 7009.849, activation diff: 4197.282, ratio: 0.599
#   pulse 5: activated nodes: 11448, borderline nodes: 177, overall activation: 8625.931, activation diff: 1616.096, ratio: 0.187
#   pulse 6: activated nodes: 11458, borderline nodes: 29, overall activation: 9164.348, activation diff: 538.417, ratio: 0.059
#   pulse 7: activated nodes: 11461, borderline nodes: 11, overall activation: 9338.298, activation diff: 173.950, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 9338.3
#   number of spread. activ. pulses: 7
#   running time: 1377

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996547   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995185   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99948084   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848914   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9972707   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89962184   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.8995491   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8995384   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8995241   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.89950794   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.8994918   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8994868   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.89945   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.89944285   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.8994379   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   17   0.89941907   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.8994123   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   19   0.8994115   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.8994097   REFERENCES:SIMDATES:SIMLOC
