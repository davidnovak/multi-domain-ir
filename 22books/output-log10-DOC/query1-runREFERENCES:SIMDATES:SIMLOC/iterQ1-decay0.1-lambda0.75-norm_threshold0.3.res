###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 113.165, activation diff: 102.605, ratio: 0.907
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 335.677, activation diff: 222.551, ratio: 0.663
#   pulse 4: activated nodes: 10765, borderline nodes: 6520, overall activation: 1009.904, activation diff: 674.227, ratio: 0.668
#   pulse 5: activated nodes: 11203, borderline nodes: 3634, overall activation: 2035.367, activation diff: 1025.463, ratio: 0.504
#   pulse 6: activated nodes: 11350, borderline nodes: 1653, overall activation: 3240.827, activation diff: 1205.460, ratio: 0.372
#   pulse 7: activated nodes: 11416, borderline nodes: 612, overall activation: 4398.969, activation diff: 1158.143, ratio: 0.263
#   pulse 8: activated nodes: 11439, borderline nodes: 252, overall activation: 5399.575, activation diff: 1000.605, ratio: 0.185
#   pulse 9: activated nodes: 11448, borderline nodes: 108, overall activation: 6222.944, activation diff: 823.370, ratio: 0.132
#   pulse 10: activated nodes: 11452, borderline nodes: 71, overall activation: 6884.775, activation diff: 661.831, ratio: 0.096
#   pulse 11: activated nodes: 11454, borderline nodes: 50, overall activation: 7409.984, activation diff: 525.208, ratio: 0.071
#   pulse 12: activated nodes: 11455, borderline nodes: 33, overall activation: 7823.548, activation diff: 413.564, ratio: 0.053
#   pulse 13: activated nodes: 11455, borderline nodes: 25, overall activation: 8147.565, activation diff: 324.017, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 8147.6
#   number of spread. activ. pulses: 13
#   running time: 1590

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886815   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.984601   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9822705   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.98175156   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97966886   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96891224   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8643047   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.86415863   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8639177   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8638561   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8638148   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8632373   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8627674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8626593   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.862609   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.8625776   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.86211324   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.8620347   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.86202496   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.861907   REFERENCES:SIMDATES:SIMLOC
