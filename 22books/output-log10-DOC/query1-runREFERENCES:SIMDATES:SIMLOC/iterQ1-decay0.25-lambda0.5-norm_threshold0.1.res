###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 735.952, activation diff: 717.568, ratio: 0.975
#   pulse 3: activated nodes: 9582, borderline nodes: 3878, overall activation: 1512.169, activation diff: 776.218, ratio: 0.513
#   pulse 4: activated nodes: 11356, borderline nodes: 3120, overall activation: 3426.992, activation diff: 1914.823, ratio: 0.559
#   pulse 5: activated nodes: 11433, borderline nodes: 365, overall activation: 4887.947, activation diff: 1460.955, ratio: 0.299
#   pulse 6: activated nodes: 11452, borderline nodes: 98, overall activation: 5776.012, activation diff: 888.064, ratio: 0.154
#   pulse 7: activated nodes: 11455, borderline nodes: 52, overall activation: 6280.919, activation diff: 504.907, ratio: 0.080
#   pulse 8: activated nodes: 11457, borderline nodes: 45, overall activation: 6561.487, activation diff: 280.568, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6561.5
#   number of spread. activ. pulses: 8
#   running time: 1438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959232   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9952437   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99458766   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99453074   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931394   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98927855   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74325895   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7432415   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7431438   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.74312663   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74300086   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.74292964   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7428377   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.74274814   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7427037   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7426911   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.74264205   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7426232   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7425077   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.74248874   REFERENCES:SIMDATES:SIMLOC
