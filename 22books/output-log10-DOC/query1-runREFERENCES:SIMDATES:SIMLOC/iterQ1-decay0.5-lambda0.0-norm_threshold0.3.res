###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1305.622, activation diff: 1330.190, ratio: 1.019
#   pulse 3: activated nodes: 9351, borderline nodes: 3990, overall activation: 363.270, activation diff: 1647.196, ratio: 4.534
#   pulse 4: activated nodes: 10823, borderline nodes: 4409, overall activation: 2972.650, activation diff: 2920.465, ratio: 0.982
#   pulse 5: activated nodes: 10953, borderline nodes: 3632, overall activation: 2816.729, activation diff: 940.394, ratio: 0.334
#   pulse 6: activated nodes: 10962, borderline nodes: 3544, overall activation: 3455.361, activation diff: 646.919, ratio: 0.187
#   pulse 7: activated nodes: 10967, borderline nodes: 3531, overall activation: 3485.820, activation diff: 31.445, ratio: 0.009

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3485.8
#   number of spread. activ. pulses: 7
#   running time: 1348

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998933   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883854   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4999999   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49999964   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.4999993   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   11   0.49999902   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49999902   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.49999902   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.49999815   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.4999979   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.4999971   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.4999971   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.49999708   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49999687   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.49999672   REFERENCES:SIMDATES:SIMLOC
