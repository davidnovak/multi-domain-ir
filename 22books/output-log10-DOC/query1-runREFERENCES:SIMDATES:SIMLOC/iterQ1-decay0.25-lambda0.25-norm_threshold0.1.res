###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1512.009, activation diff: 1507.003, ratio: 0.997
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 2196.964, activation diff: 917.065, ratio: 0.417
#   pulse 4: activated nodes: 11392, borderline nodes: 2443, overall activation: 5175.445, activation diff: 2978.802, ratio: 0.576
#   pulse 5: activated nodes: 11443, borderline nodes: 224, overall activation: 6358.625, activation diff: 1183.180, ratio: 0.186
#   pulse 6: activated nodes: 11454, borderline nodes: 59, overall activation: 6755.206, activation diff: 396.581, ratio: 0.059
#   pulse 7: activated nodes: 11457, borderline nodes: 42, overall activation: 6884.120, activation diff: 128.913, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6884.1
#   number of spread. activ. pulses: 7
#   running time: 1403

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99965453   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995184   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99948084   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848896   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99727064   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74967957   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.74961185   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74960935   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.74960196   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74958444   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.7495731   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7495655   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7495277   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.74951553   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.74951243   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.74950486   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.74949706   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   19   0.74948394   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.7494837   REFERENCES:SIMDATES:SIMLOC
