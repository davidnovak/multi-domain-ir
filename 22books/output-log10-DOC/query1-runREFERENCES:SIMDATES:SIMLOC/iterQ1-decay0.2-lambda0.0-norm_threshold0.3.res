###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2084.792, activation diff: 2109.360, ratio: 1.012
#   pulse 3: activated nodes: 9351, borderline nodes: 3990, overall activation: 1029.685, activation diff: 3069.718, ratio: 2.981
#   pulse 4: activated nodes: 11345, borderline nodes: 3430, overall activation: 6473.592, activation diff: 6273.745, ratio: 0.969
#   pulse 5: activated nodes: 11428, borderline nodes: 427, overall activation: 6949.876, activation diff: 1377.075, ratio: 0.198
#   pulse 6: activated nodes: 11450, borderline nodes: 73, overall activation: 7551.989, activation diff: 614.132, ratio: 0.081
#   pulse 7: activated nodes: 11455, borderline nodes: 32, overall activation: 7595.048, activation diff: 44.722, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7595.0
#   number of spread. activ. pulses: 7
#   running time: 1406

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   9   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   10   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   12   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   14   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_501   15   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   16   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_505   17   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   19   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.8   REFERENCES:SIMDATES:SIMLOC
