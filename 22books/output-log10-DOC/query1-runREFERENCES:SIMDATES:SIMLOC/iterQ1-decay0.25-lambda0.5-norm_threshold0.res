###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 864.771, activation diff: 844.907, ratio: 0.977
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1853.974, activation diff: 989.202, ratio: 0.534
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3853.951, activation diff: 1999.977, ratio: 0.519
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5261.901, activation diff: 1407.950, ratio: 0.268
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6092.672, activation diff: 830.771, ratio: 0.136
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 6558.684, activation diff: 466.012, ratio: 0.071
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 6815.226, activation diff: 256.542, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6815.2
#   number of spread. activ. pulses: 8
#   running time: 1382

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9954963   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99503875   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949559   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935834   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99044204   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7435616   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74351853   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.74338615   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.74337673   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7433213   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.7432922   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7432604   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.7430822   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7430506   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.74302953   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.7429707   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.74297047   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7428924   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7428178   REFERENCES:SIMDATES:SIMLOC
