###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 503.537, activation diff: 489.641, ratio: 0.972
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 984.166, activation diff: 483.488, ratio: 0.491
#   pulse 4: activated nodes: 11186, borderline nodes: 5654, overall activation: 2708.861, activation diff: 1724.695, ratio: 0.637
#   pulse 5: activated nodes: 11379, borderline nodes: 1333, overall activation: 4263.557, activation diff: 1554.695, ratio: 0.365
#   pulse 6: activated nodes: 11424, borderline nodes: 433, overall activation: 5314.192, activation diff: 1050.636, ratio: 0.198
#   pulse 7: activated nodes: 11441, borderline nodes: 174, overall activation: 5939.763, activation diff: 625.571, ratio: 0.105
#   pulse 8: activated nodes: 11446, borderline nodes: 122, overall activation: 6296.093, activation diff: 356.329, ratio: 0.057
#   pulse 9: activated nodes: 11448, borderline nodes: 107, overall activation: 6495.776, activation diff: 199.684, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 6495.8
#   number of spread. activ. pulses: 9
#   running time: 1479

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99774945   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9971335   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99661154   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9962572   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99533737   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.991789   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.74616337   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7461355   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.74611723   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.7461058   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74604607   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7459241   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7458187   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7458172   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7457696   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.74572694   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.7457055   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7457049   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   19   0.7456634   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   20   0.7456404   REFERENCES:SIMDATES:SIMLOC
