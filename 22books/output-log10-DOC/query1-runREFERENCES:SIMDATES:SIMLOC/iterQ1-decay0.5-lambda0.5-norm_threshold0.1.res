###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 497.760, activation diff: 479.376, ratio: 0.963
#   pulse 3: activated nodes: 9582, borderline nodes: 3878, overall activation: 934.527, activation diff: 436.767, ratio: 0.467
#   pulse 4: activated nodes: 10870, borderline nodes: 4022, overall activation: 1804.992, activation diff: 870.465, ratio: 0.482
#   pulse 5: activated nodes: 10958, borderline nodes: 3576, overall activation: 2503.465, activation diff: 698.473, ratio: 0.279
#   pulse 6: activated nodes: 10967, borderline nodes: 3526, overall activation: 2951.979, activation diff: 448.515, ratio: 0.152
#   pulse 7: activated nodes: 10969, borderline nodes: 3522, overall activation: 3212.877, activation diff: 260.897, ratio: 0.081
#   pulse 8: activated nodes: 10969, borderline nodes: 3520, overall activation: 3359.678, activation diff: 146.801, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 3359.7
#   number of spread. activ. pulses: 8
#   running time: 1419

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99592316   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99524   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99458003   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9945302   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931183   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9892529   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49549887   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49549   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49541605   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49540955   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49530965   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.49527997   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.4952163   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.49510098   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49509   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.49508557   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.49505818   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.4950234   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.4949789   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.4949658   REFERENCES:SIMDATES:SIMLOC
