###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2034.344, activation diff: 2025.542, ratio: 0.996
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 3542.648, activation diff: 1575.081, ratio: 0.445
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 7451.344, activation diff: 3908.696, ratio: 0.525
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 8858.355, activation diff: 1407.011, ratio: 0.159
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 9310.385, activation diff: 452.029, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 9310.4
#   number of spread. activ. pulses: 6
#   running time: 1350

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991438   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9989252   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9986696   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.998552   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9973587   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9956127   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89886016   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   8   0.89861757   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8985983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8985864   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8984883   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.8984729   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.89841694   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89841056   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.89840555   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.89837235   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.89835614   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.89831626   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   19   0.8982573   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.89820707   REFERENCES:SIMDATES:SIMLOC
