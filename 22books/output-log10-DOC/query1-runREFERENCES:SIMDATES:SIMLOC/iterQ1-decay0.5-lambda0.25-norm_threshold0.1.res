###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1014.168, activation diff: 1009.162, ratio: 0.995
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 1271.460, activation diff: 424.039, ratio: 0.334
#   pulse 4: activated nodes: 10889, borderline nodes: 3770, overall activation: 2650.723, activation diff: 1379.312, ratio: 0.520
#   pulse 5: activated nodes: 10966, borderline nodes: 3536, overall activation: 3238.035, activation diff: 587.312, ratio: 0.181
#   pulse 6: activated nodes: 10969, borderline nodes: 3521, overall activation: 3440.803, activation diff: 202.768, ratio: 0.059
#   pulse 7: activated nodes: 10969, borderline nodes: 3520, overall activation: 3507.710, activation diff: 66.907, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 3507.7
#   number of spread. activ. pulses: 7
#   running time: 1386

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99973714   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99965394   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9995181   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99948084   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9984871   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99727046   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49978006   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49973252   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.4997316   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.49972248   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49971238   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.49971092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49969578   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.49968025   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.49967074   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49964988   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.49964356   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.49963582   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.49962774   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.49962658   REFERENCES:SIMDATES:SIMLOC
