###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 131.215, activation diff: 119.173, ratio: 0.908
#   pulse 3: activated nodes: 9266, borderline nodes: 3994, overall activation: 323.103, activation diff: 191.888, ratio: 0.594
#   pulse 4: activated nodes: 10772, borderline nodes: 4594, overall activation: 672.178, activation diff: 349.075, ratio: 0.519
#   pulse 5: activated nodes: 10882, borderline nodes: 3870, overall activation: 1090.483, activation diff: 418.305, ratio: 0.384
#   pulse 6: activated nodes: 10954, borderline nodes: 3625, overall activation: 1514.737, activation diff: 424.253, ratio: 0.280
#   pulse 7: activated nodes: 10959, borderline nodes: 3549, overall activation: 1905.156, activation diff: 390.419, ratio: 0.205
#   pulse 8: activated nodes: 10965, borderline nodes: 3528, overall activation: 2240.485, activation diff: 335.329, ratio: 0.150
#   pulse 9: activated nodes: 10968, borderline nodes: 3523, overall activation: 2516.385, activation diff: 275.900, ratio: 0.110
#   pulse 10: activated nodes: 10968, borderline nodes: 3522, overall activation: 2738.213, activation diff: 221.828, ratio: 0.081
#   pulse 11: activated nodes: 10969, borderline nodes: 3522, overall activation: 2914.301, activation diff: 176.088, ratio: 0.060
#   pulse 12: activated nodes: 10969, borderline nodes: 3520, overall activation: 3052.988, activation diff: 138.687, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 3053.0
#   number of spread. activ. pulses: 12
#   running time: 1583

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869466   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9828505   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98078656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98026204   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9769504   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96725726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.47519985   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.47516778   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4748203   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.47470978   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.4746852   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.47455263   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.47442052   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.4737972   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.4736895   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.47362942   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.47362712   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.4736265   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.47356564   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.47353333   REFERENCES:SIMDATES:SIMLOC
