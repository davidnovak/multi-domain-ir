###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 199.954, activation diff: 187.911, ratio: 0.940
#   pulse 3: activated nodes: 9266, borderline nodes: 3994, overall activation: 532.297, activation diff: 332.342, ratio: 0.624
#   pulse 4: activated nodes: 11257, borderline nodes: 4378, overall activation: 1330.259, activation diff: 797.962, ratio: 0.600
#   pulse 5: activated nodes: 11395, borderline nodes: 1038, overall activation: 2353.182, activation diff: 1022.923, ratio: 0.435
#   pulse 6: activated nodes: 11432, borderline nodes: 354, overall activation: 3370.493, activation diff: 1017.310, ratio: 0.302
#   pulse 7: activated nodes: 11451, borderline nodes: 127, overall activation: 4258.547, activation diff: 888.054, ratio: 0.209
#   pulse 8: activated nodes: 11453, borderline nodes: 66, overall activation: 4990.808, activation diff: 732.261, ratio: 0.147
#   pulse 9: activated nodes: 11455, borderline nodes: 44, overall activation: 5578.915, activation diff: 588.107, ratio: 0.105
#   pulse 10: activated nodes: 11456, borderline nodes: 29, overall activation: 6044.702, activation diff: 465.787, ratio: 0.077
#   pulse 11: activated nodes: 11457, borderline nodes: 22, overall activation: 6410.587, activation diff: 365.885, ratio: 0.057
#   pulse 12: activated nodes: 11457, borderline nodes: 18, overall activation: 6696.494, activation diff: 285.907, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6696.5
#   number of spread. activ. pulses: 12
#   running time: 1591

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98695123   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98291004   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98091036   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98038083   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97711873   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9678216   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7604233   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7603593   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.75977486   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7596552   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7596391   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7596311   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.75921667   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7584897   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.75842714   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.75824785   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.75824636   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7579566   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7578487   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7578297   REFERENCES:SIMDATES:SIMLOC
