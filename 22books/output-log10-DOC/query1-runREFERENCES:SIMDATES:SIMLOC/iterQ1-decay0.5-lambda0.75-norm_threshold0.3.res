###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 69.194, activation diff: 58.633, ratio: 0.847
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 186.887, activation diff: 117.733, ratio: 0.630
#   pulse 4: activated nodes: 10062, borderline nodes: 6040, overall activation: 447.333, activation diff: 260.446, ratio: 0.582
#   pulse 5: activated nodes: 10661, borderline nodes: 4921, overall activation: 790.854, activation diff: 343.521, ratio: 0.434
#   pulse 6: activated nodes: 10815, borderline nodes: 4461, overall activation: 1171.076, activation diff: 380.222, ratio: 0.325
#   pulse 7: activated nodes: 10879, borderline nodes: 3943, overall activation: 1548.870, activation diff: 377.794, ratio: 0.244
#   pulse 8: activated nodes: 10929, borderline nodes: 3702, overall activation: 1901.228, activation diff: 352.358, ratio: 0.185
#   pulse 9: activated nodes: 10951, borderline nodes: 3626, overall activation: 2212.467, activation diff: 311.239, ratio: 0.141
#   pulse 10: activated nodes: 10959, borderline nodes: 3578, overall activation: 2474.624, activation diff: 262.157, ratio: 0.106
#   pulse 11: activated nodes: 10959, borderline nodes: 3557, overall activation: 2688.761, activation diff: 214.137, ratio: 0.080
#   pulse 12: activated nodes: 10959, borderline nodes: 3546, overall activation: 2860.743, activation diff: 171.982, ratio: 0.060
#   pulse 13: activated nodes: 10963, borderline nodes: 3539, overall activation: 2997.555, activation diff: 136.813, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10963
#   final overall activation: 2997.6
#   number of spread. activ. pulses: 13
#   running time: 1507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886637   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98442024   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.98197556   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9811665   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97936004   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9677353   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.48004857   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.479922   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4798504   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.4797806   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.47965798   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.47943407   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.47903356   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47888777   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.47883183   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   16   0.47879007   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.47878003   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.47867095   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.47864226   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.47862262   REFERENCES:SIMDATES:SIMLOC
