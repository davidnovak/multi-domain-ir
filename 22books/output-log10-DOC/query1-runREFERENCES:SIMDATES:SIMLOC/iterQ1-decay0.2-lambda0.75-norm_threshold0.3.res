###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 102.172, activation diff: 91.612, ratio: 0.897
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 297.145, activation diff: 195.012, ratio: 0.656
#   pulse 4: activated nodes: 10742, borderline nodes: 6522, overall activation: 842.105, activation diff: 544.961, ratio: 0.647
#   pulse 5: activated nodes: 11189, borderline nodes: 3971, overall activation: 1647.729, activation diff: 805.624, ratio: 0.489
#   pulse 6: activated nodes: 11340, borderline nodes: 1979, overall activation: 2594.179, activation diff: 946.450, ratio: 0.365
#   pulse 7: activated nodes: 11410, borderline nodes: 814, overall activation: 3522.241, activation diff: 928.062, ratio: 0.263
#   pulse 8: activated nodes: 11426, borderline nodes: 385, overall activation: 4336.877, activation diff: 814.636, ratio: 0.188
#   pulse 9: activated nodes: 11435, borderline nodes: 190, overall activation: 5013.340, activation diff: 676.462, ratio: 0.135
#   pulse 10: activated nodes: 11447, borderline nodes: 119, overall activation: 5560.395, activation diff: 547.055, ratio: 0.098
#   pulse 11: activated nodes: 11451, borderline nodes: 95, overall activation: 5996.417, activation diff: 436.022, ratio: 0.073
#   pulse 12: activated nodes: 11452, borderline nodes: 76, overall activation: 6340.947, activation diff: 344.530, ratio: 0.054
#   pulse 13: activated nodes: 11453, borderline nodes: 61, overall activation: 6611.699, activation diff: 270.752, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6611.7
#   number of spread. activ. pulses: 13
#   running time: 1655

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886786   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9845716   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9822228   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9816542   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97961926   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96870476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.76823664   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7681081   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.76787215   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.76784956   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.767798   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7672803   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.76678133   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.766713   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7666811   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7666371   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7662281   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.76614064   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7660958   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.76608497   REFERENCES:SIMDATES:SIMLOC
