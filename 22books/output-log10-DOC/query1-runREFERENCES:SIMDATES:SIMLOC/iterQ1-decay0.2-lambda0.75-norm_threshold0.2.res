###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 145.274, activation diff: 133.951, ratio: 0.922
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 401.954, activation diff: 256.695, ratio: 0.639
#   pulse 4: activated nodes: 11080, borderline nodes: 5658, overall activation: 1069.767, activation diff: 667.813, ratio: 0.624
#   pulse 5: activated nodes: 11301, borderline nodes: 2457, overall activation: 1992.236, activation diff: 922.468, ratio: 0.463
#   pulse 6: activated nodes: 11403, borderline nodes: 956, overall activation: 2988.918, activation diff: 996.682, ratio: 0.333
#   pulse 7: activated nodes: 11426, borderline nodes: 375, overall activation: 3903.762, activation diff: 914.844, ratio: 0.234
#   pulse 8: activated nodes: 11444, borderline nodes: 167, overall activation: 4677.167, activation diff: 773.405, ratio: 0.165
#   pulse 9: activated nodes: 11450, borderline nodes: 94, overall activation: 5306.925, activation diff: 629.758, ratio: 0.119
#   pulse 10: activated nodes: 11453, borderline nodes: 69, overall activation: 5810.072, activation diff: 503.147, ratio: 0.087
#   pulse 11: activated nodes: 11455, borderline nodes: 50, overall activation: 6207.774, activation diff: 397.702, ratio: 0.064
#   pulse 12: activated nodes: 11455, borderline nodes: 43, overall activation: 6520.040, activation diff: 312.266, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6520.0
#   number of spread. activ. pulses: 12
#   running time: 1617

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98609704   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98139465   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9786945   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9785849   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97538817   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9641027   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7591847   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7590769   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7585801   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7585212   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.75848204   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.75817376   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.75760645   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.75718945   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.75717616   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7570273   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7567512   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7565303   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.7564764   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.756415   REFERENCES:SIMDATES:SIMLOC
