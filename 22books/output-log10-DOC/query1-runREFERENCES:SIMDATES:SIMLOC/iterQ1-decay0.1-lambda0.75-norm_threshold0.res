###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 297.044, activation diff: 284.325, ratio: 0.957
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 803.544, activation diff: 506.500, ratio: 0.630
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1990.626, activation diff: 1187.081, ratio: 0.596
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3365.851, activation diff: 1375.225, ratio: 0.409
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 4623.208, activation diff: 1257.357, ratio: 0.272
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5674.289, activation diff: 1051.081, ratio: 0.185
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 6521.549, activation diff: 847.259, ratio: 0.130
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 7192.421, activation diff: 670.872, ratio: 0.093
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 7718.317, activation diff: 525.896, ratio: 0.068
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 8128.038, activation diff: 409.721, ratio: 0.050
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 8445.979, activation diff: 317.942, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8446.0
#   number of spread. activ. pulses: 12
#   running time: 1603

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756874   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98411393   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98258895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9818554   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9786318   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9708948   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8566592   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8566512   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.8559758   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8558944   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.85573816   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.8557121   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.855628   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.854639   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.85453993   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.85450596   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.8543246   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.85409737   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.85401297   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.8539489   REFERENCES:SIMDATES:SIMLOC
