###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2344.515, activation diff: 2369.083, ratio: 1.010
#   pulse 3: activated nodes: 9351, borderline nodes: 3990, overall activation: 1304.580, activation diff: 3590.982, ratio: 2.753
#   pulse 4: activated nodes: 11346, borderline nodes: 3349, overall activation: 7946.725, activation diff: 7620.759, ratio: 0.959
#   pulse 5: activated nodes: 11434, borderline nodes: 376, overall activation: 8657.418, activation diff: 1519.476, ratio: 0.176
#   pulse 6: activated nodes: 11453, borderline nodes: 52, overall activation: 9246.064, activation diff: 600.213, ratio: 0.065
#   pulse 7: activated nodes: 11456, borderline nodes: 16, overall activation: 9293.864, activation diff: 49.505, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 9293.9
#   number of spread. activ. pulses: 7
#   running time: 1393

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   7   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   8   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   14   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   16   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   18   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   19   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   20   0.9   REFERENCES:SIMDATES:SIMLOC
