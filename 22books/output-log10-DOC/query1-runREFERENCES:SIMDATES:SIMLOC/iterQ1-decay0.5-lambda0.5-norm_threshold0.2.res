###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 416.280, activation diff: 400.027, ratio: 0.961
#   pulse 3: activated nodes: 9231, borderline nodes: 4103, overall activation: 764.690, activation diff: 348.589, ratio: 0.456
#   pulse 4: activated nodes: 10754, borderline nodes: 4806, overall activation: 1605.648, activation diff: 840.959, ratio: 0.524
#   pulse 5: activated nodes: 10927, borderline nodes: 3724, overall activation: 2328.572, activation diff: 722.924, ratio: 0.310
#   pulse 6: activated nodes: 10959, borderline nodes: 3568, overall activation: 2827.630, activation diff: 499.058, ratio: 0.176
#   pulse 7: activated nodes: 10962, borderline nodes: 3532, overall activation: 3128.196, activation diff: 300.565, ratio: 0.096
#   pulse 8: activated nodes: 10967, borderline nodes: 3524, overall activation: 3300.567, activation diff: 172.371, ratio: 0.052
#   pulse 9: activated nodes: 10967, borderline nodes: 3524, overall activation: 3397.580, activation diff: 97.013, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3397.6
#   number of spread. activ. pulses: 9
#   running time: 1463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978901   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99742234   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969773   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99686575   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9957181   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9927961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.4976134   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.49760985   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49757886   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.4975726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49752304   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.4974519   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49739897   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.49739194   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49736664   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.4973649   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.4973414   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.49733084   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.49727714   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.4972517   REFERENCES:SIMDATES:SIMLOC
