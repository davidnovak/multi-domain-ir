###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 535.973, activation diff: 522.077, ratio: 0.974
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 1061.309, activation diff: 528.380, ratio: 0.498
#   pulse 4: activated nodes: 11201, borderline nodes: 5623, overall activation: 3033.815, activation diff: 1972.505, ratio: 0.650
#   pulse 5: activated nodes: 11381, borderline nodes: 1242, overall activation: 4796.144, activation diff: 1762.329, ratio: 0.367
#   pulse 6: activated nodes: 11431, borderline nodes: 361, overall activation: 5974.245, activation diff: 1178.101, ratio: 0.197
#   pulse 7: activated nodes: 11448, borderline nodes: 117, overall activation: 6672.572, activation diff: 698.327, ratio: 0.105
#   pulse 8: activated nodes: 11453, borderline nodes: 71, overall activation: 7069.755, activation diff: 397.183, ratio: 0.056
#   pulse 9: activated nodes: 11455, borderline nodes: 43, overall activation: 7292.177, activation diff: 222.422, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7292.2
#   number of spread. activ. pulses: 9
#   running time: 1507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977495   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99713415   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.996613   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9962573   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99534   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99179506   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.79591155   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7958784   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.79586077   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.7958462   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7957835   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7956707   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7955456   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79554343   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7954883   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.79544663   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7954466   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.79542637   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   19   0.7954068   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   20   0.7953508   REFERENCES:SIMDATES:SIMLOC
