###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 161.500, activation diff: 150.177, ratio: 0.930
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 455.992, activation diff: 294.507, ratio: 0.646
#   pulse 4: activated nodes: 11094, borderline nodes: 5632, overall activation: 1292.860, activation diff: 836.868, ratio: 0.647
#   pulse 5: activated nodes: 11322, borderline nodes: 2201, overall activation: 2470.891, activation diff: 1178.031, ratio: 0.477
#   pulse 6: activated nodes: 11410, borderline nodes: 750, overall activation: 3722.819, activation diff: 1251.928, ratio: 0.336
#   pulse 7: activated nodes: 11439, borderline nodes: 274, overall activation: 4852.748, activation diff: 1129.929, ratio: 0.233
#   pulse 8: activated nodes: 11450, borderline nodes: 98, overall activation: 5797.972, activation diff: 945.224, ratio: 0.163
#   pulse 9: activated nodes: 11454, borderline nodes: 53, overall activation: 6562.699, activation diff: 764.727, ratio: 0.117
#   pulse 10: activated nodes: 11455, borderline nodes: 33, overall activation: 7171.018, activation diff: 608.319, ratio: 0.085
#   pulse 11: activated nodes: 11455, borderline nodes: 23, overall activation: 7650.207, activation diff: 479.189, ratio: 0.063
#   pulse 12: activated nodes: 11456, borderline nodes: 16, overall activation: 8025.392, activation diff: 375.185, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 8025.4
#   number of spread. activ. pulses: 12
#   running time: 1619

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860988   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9814145   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97874737   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.978621   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97543377   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.964283   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8541185   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.8539803   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.85342777   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.8533875   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8533317   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8529917   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.85236174   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8519762   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.8519292   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8518041   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8514496   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.8511553   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8511476   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.8510926   REFERENCES:SIMDATES:SIMLOC
