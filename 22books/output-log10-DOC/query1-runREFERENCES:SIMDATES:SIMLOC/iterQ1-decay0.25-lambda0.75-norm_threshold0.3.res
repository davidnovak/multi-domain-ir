###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 96.676, activation diff: 86.116, ratio: 0.891
#   pulse 3: activated nodes: 8555, borderline nodes: 5557, overall activation: 278.203, activation diff: 181.566, ratio: 0.653
#   pulse 4: activated nodes: 10718, borderline nodes: 6517, overall activation: 765.828, activation diff: 487.625, ratio: 0.637
#   pulse 5: activated nodes: 11177, borderline nodes: 4174, overall activation: 1474.167, activation diff: 708.339, ratio: 0.481
#   pulse 6: activated nodes: 11327, borderline nodes: 2177, overall activation: 2300.380, activation diff: 826.214, ratio: 0.359
#   pulse 7: activated nodes: 11406, borderline nodes: 941, overall activation: 3117.780, activation diff: 817.399, ratio: 0.262
#   pulse 8: activated nodes: 11424, borderline nodes: 469, overall activation: 3841.769, activation diff: 723.990, ratio: 0.188
#   pulse 9: activated nodes: 11431, borderline nodes: 263, overall activation: 4445.757, activation diff: 603.988, ratio: 0.136
#   pulse 10: activated nodes: 11438, borderline nodes: 189, overall activation: 4935.542, activation diff: 489.785, ratio: 0.099
#   pulse 11: activated nodes: 11443, borderline nodes: 152, overall activation: 5326.592, activation diff: 391.050, ratio: 0.073
#   pulse 12: activated nodes: 11444, borderline nodes: 140, overall activation: 5635.927, activation diff: 309.335, ratio: 0.055
#   pulse 13: activated nodes: 11446, borderline nodes: 130, overall activation: 5879.206, activation diff: 243.279, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 5879.2
#   number of spread. activ. pulses: 13
#   running time: 1652

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9886769   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.984554   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9821943   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9815967   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9795899   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.968586   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7202034   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.72008014   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.71984756   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7198448   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7197914   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7193029   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7187848   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71874005   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.71873474   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.71865666   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7182877   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.71819687   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.7181851   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.71817696   REFERENCES:SIMDATES:SIMLOC
