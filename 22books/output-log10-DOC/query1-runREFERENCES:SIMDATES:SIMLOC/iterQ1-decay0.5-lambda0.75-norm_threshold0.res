###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 172.926, activation diff: 160.207, ratio: 0.926
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 414.791, activation diff: 241.865, ratio: 0.583
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 815.701, activation diff: 400.910, ratio: 0.491
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1267.276, activation diff: 451.575, ratio: 0.356
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1700.983, activation diff: 433.707, ratio: 0.255
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2080.401, activation diff: 379.418, ratio: 0.182
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2394.932, activation diff: 314.530, ratio: 0.131
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2648.491, activation diff: 253.560, ratio: 0.096
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 2849.838, activation diff: 201.346, ratio: 0.071
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 3008.287, activation diff: 158.449, ratio: 0.053
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 3132.260, activation diff: 123.973, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 3132.3
#   number of spread. activ. pulses: 12
#   running time: 1544

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98756576   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9840702   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9825251   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98176605   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97848463   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9704617   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4758611   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.47585136   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.47545406   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.47545266   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.47532985   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.47525817   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.47519693   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47454166   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.47451755   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.47439936   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.4743515   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.4743477   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.47432467   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.47420484   REFERENCES:SIMDATES:SIMLOC
