###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1331.776, activation diff: 1330.665, ratio: 0.999
#   pulse 3: activated nodes: 9450, borderline nodes: 3922, overall activation: 1714.733, activation diff: 841.515, ratio: 0.491
#   pulse 4: activated nodes: 11346, borderline nodes: 3328, overall activation: 4800.133, activation diff: 3088.265, ratio: 0.643
#   pulse 5: activated nodes: 11426, borderline nodes: 451, overall activation: 6134.076, activation diff: 1333.962, ratio: 0.217
#   pulse 6: activated nodes: 11450, borderline nodes: 118, overall activation: 6604.637, activation diff: 470.561, ratio: 0.071
#   pulse 7: activated nodes: 11454, borderline nodes: 60, overall activation: 6762.387, activation diff: 157.750, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6762.4
#   number of spread. activ. pulses: 7
#   running time: 1351

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999522   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993684   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99920535   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99824214   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967985   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7495389   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.74950385   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.7494621   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7494359   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.74943423   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.7494319   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.749419   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.74939096   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.74936664   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.74935406   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.74935204   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7493361   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.7493001   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.74929976   REFERENCES:SIMDATES:SIMLOC
