###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 188.498, activation diff: 176.455, ratio: 0.936
#   pulse 3: activated nodes: 9266, borderline nodes: 3994, overall activation: 496.166, activation diff: 307.669, ratio: 0.620
#   pulse 4: activated nodes: 11249, borderline nodes: 4422, overall activation: 1201.094, activation diff: 704.927, ratio: 0.587
#   pulse 5: activated nodes: 11393, borderline nodes: 1131, overall activation: 2098.412, activation diff: 897.318, ratio: 0.428
#   pulse 6: activated nodes: 11427, borderline nodes: 418, overall activation: 2997.615, activation diff: 899.203, ratio: 0.300
#   pulse 7: activated nodes: 11443, borderline nodes: 171, overall activation: 3787.493, activation diff: 789.879, ratio: 0.209
#   pulse 8: activated nodes: 11449, borderline nodes: 113, overall activation: 4441.029, activation diff: 653.536, ratio: 0.147
#   pulse 9: activated nodes: 11450, borderline nodes: 82, overall activation: 4966.979, activation diff: 525.949, ratio: 0.106
#   pulse 10: activated nodes: 11450, borderline nodes: 71, overall activation: 5384.060, activation diff: 417.081, ratio: 0.077
#   pulse 11: activated nodes: 11451, borderline nodes: 67, overall activation: 5711.987, activation diff: 327.928, ratio: 0.057
#   pulse 12: activated nodes: 11451, borderline nodes: 65, overall activation: 5968.434, activation diff: 256.446, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5968.4
#   number of spread. activ. pulses: 12
#   running time: 1604

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869507   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98290336   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9808974   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98036766   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9770993   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96775466   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7128843   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.71283066   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.71227986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7121537   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.71214783   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.71213776   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.71174765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71104103   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7109672   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.71081555   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.71080035   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.71056336   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.71046257   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.71042025   REFERENCES:SIMDATES:SIMLOC
