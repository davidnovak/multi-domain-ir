###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 137.161, activation diff: 125.838, ratio: 0.917
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 375.417, activation diff: 238.271, ratio: 0.635
#   pulse 4: activated nodes: 11073, borderline nodes: 5681, overall activation: 968.170, activation diff: 592.753, ratio: 0.612
#   pulse 5: activated nodes: 11291, borderline nodes: 2664, overall activation: 1775.468, activation diff: 807.299, ratio: 0.455
#   pulse 6: activated nodes: 11399, borderline nodes: 1082, overall activation: 2651.334, activation diff: 875.866, ratio: 0.330
#   pulse 7: activated nodes: 11424, borderline nodes: 459, overall activation: 3462.619, activation diff: 811.285, ratio: 0.234
#   pulse 8: activated nodes: 11430, borderline nodes: 222, overall activation: 4152.301, activation diff: 689.683, ratio: 0.166
#   pulse 9: activated nodes: 11438, borderline nodes: 154, overall activation: 4715.607, activation diff: 563.305, ratio: 0.119
#   pulse 10: activated nodes: 11442, borderline nodes: 126, overall activation: 5166.486, activation diff: 450.880, ratio: 0.087
#   pulse 11: activated nodes: 11444, borderline nodes: 113, overall activation: 5523.289, activation diff: 356.803, ratio: 0.065
#   pulse 12: activated nodes: 11444, borderline nodes: 99, overall activation: 5803.693, activation diff: 280.404, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 5803.7
#   number of spread. activ. pulses: 12
#   running time: 1537

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9860959   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9813826   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9786624   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9785631   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97536147   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96399844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7117187   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7116228   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.7111578   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.71108174   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.71105903   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7107661   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.71023065   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.70979875   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7097917   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.70964456   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7094039   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.70922095   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.70913744   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.70913726   REFERENCES:SIMDATES:SIMLOC
