###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 222.867, activation diff: 210.824, ratio: 0.946
#   pulse 3: activated nodes: 9266, borderline nodes: 3994, overall activation: 605.940, activation diff: 383.073, ratio: 0.632
#   pulse 4: activated nodes: 11271, borderline nodes: 4305, overall activation: 1610.156, activation diff: 1004.216, ratio: 0.624
#   pulse 5: activated nodes: 11404, borderline nodes: 915, overall activation: 2901.819, activation diff: 1291.662, ratio: 0.445
#   pulse 6: activated nodes: 11443, borderline nodes: 255, overall activation: 4165.911, activation diff: 1264.092, ratio: 0.303
#   pulse 7: activated nodes: 11451, borderline nodes: 80, overall activation: 5255.925, activation diff: 1090.013, ratio: 0.207
#   pulse 8: activated nodes: 11455, borderline nodes: 38, overall activation: 6148.257, activation diff: 892.332, ratio: 0.145
#   pulse 9: activated nodes: 11456, borderline nodes: 19, overall activation: 6861.528, activation diff: 713.271, ratio: 0.104
#   pulse 10: activated nodes: 11458, borderline nodes: 14, overall activation: 7424.393, activation diff: 562.865, ratio: 0.076
#   pulse 11: activated nodes: 11459, borderline nodes: 12, overall activation: 7865.169, activation diff: 440.776, ratio: 0.056
#   pulse 12: activated nodes: 11460, borderline nodes: 11, overall activation: 8208.654, activation diff: 343.485, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8208.7
#   number of spread. activ. pulses: 12
#   running time: 1503

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9869522   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98292124   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9809306   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9804028   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97715175   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96793485   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8555023   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.85541403   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.8547677   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.85464835   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.85462487   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8546191   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.85415745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8533854   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.85333955   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8531521   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.85311073   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.85274875   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8526417   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.85262704   REFERENCES:SIMDATES:SIMLOC
