###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 733.922, activation diff: 717.669, ratio: 0.978
#   pulse 3: activated nodes: 9231, borderline nodes: 4103, overall activation: 1520.733, activation diff: 787.125, ratio: 0.518
#   pulse 4: activated nodes: 11308, borderline nodes: 4245, overall activation: 4182.964, activation diff: 2662.230, ratio: 0.636
#   pulse 5: activated nodes: 11414, borderline nodes: 636, overall activation: 6311.152, activation diff: 2128.188, ratio: 0.337
#   pulse 6: activated nodes: 11448, borderline nodes: 112, overall activation: 7634.784, activation diff: 1323.632, ratio: 0.173
#   pulse 7: activated nodes: 11455, borderline nodes: 38, overall activation: 8394.522, activation diff: 759.739, ratio: 0.091
#   pulse 8: activated nodes: 11456, borderline nodes: 18, overall activation: 8818.800, activation diff: 424.277, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 8818.8
#   number of spread. activ. pulses: 8
#   running time: 1477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99578035   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99486434   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99407446   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99374425   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9926013   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98783   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8914211   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.8914057   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.8914045   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8913329   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8911457   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.89093626   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.89083993   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89071554   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.8906795   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.8906584   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.89062977   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.8906112   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.8905759   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.8905482   REFERENCES:SIMDATES:SIMLOC
