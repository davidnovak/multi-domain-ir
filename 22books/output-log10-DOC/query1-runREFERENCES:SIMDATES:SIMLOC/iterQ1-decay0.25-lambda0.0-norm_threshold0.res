###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2652.696, activation diff: 2677.003, ratio: 1.009
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2273.628, activation diff: 3356.465, ratio: 1.476
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 6713.262, activation diff: 4648.714, ratio: 0.692
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 7056.768, activation diff: 360.858, ratio: 0.051
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 7112.901, activation diff: 56.286, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7112.9
#   number of spread. activ. pulses: 6
#   running time: 1478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999922   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992514   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9991399   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835074   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   20   0.75   REFERENCES:SIMDATES:SIMLOC
