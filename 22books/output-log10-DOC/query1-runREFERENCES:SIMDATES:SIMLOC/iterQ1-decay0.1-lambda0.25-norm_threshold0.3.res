###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1379.745, activation diff: 1381.577, ratio: 1.001
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 1695.662, activation diff: 1049.771, ratio: 0.619
#   pulse 4: activated nodes: 11322, borderline nodes: 4277, overall activation: 6146.968, activation diff: 4467.790, ratio: 0.727
#   pulse 5: activated nodes: 11418, borderline nodes: 633, overall activation: 8160.709, activation diff: 2013.877, ratio: 0.247
#   pulse 6: activated nodes: 11447, borderline nodes: 84, overall activation: 8904.942, activation diff: 744.232, ratio: 0.084
#   pulse 7: activated nodes: 11455, borderline nodes: 30, overall activation: 9160.112, activation diff: 255.170, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 9160.1
#   number of spread. activ. pulses: 7
#   running time: 1439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993116   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991318   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9987718   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979365   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99622345   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.899218   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.899182   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.8991204   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   10   0.8991127   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.8991072   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.89909804   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.899094   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8990666   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.89904547   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.89900255   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.89899665   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   18   0.89898795   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.8989848   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.89898336   REFERENCES:SIMDATES:SIMLOC
