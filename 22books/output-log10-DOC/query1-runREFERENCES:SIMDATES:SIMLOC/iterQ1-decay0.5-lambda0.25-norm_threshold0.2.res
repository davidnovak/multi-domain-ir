###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 893.035, activation diff: 891.924, ratio: 0.999
#   pulse 3: activated nodes: 9450, borderline nodes: 3922, overall activation: 990.035, activation diff: 413.538, ratio: 0.418
#   pulse 4: activated nodes: 10845, borderline nodes: 4247, overall activation: 2452.406, activation diff: 1462.610, ratio: 0.596
#   pulse 5: activated nodes: 10955, borderline nodes: 3619, overall activation: 3132.764, activation diff: 680.358, ratio: 0.217
#   pulse 6: activated nodes: 10962, borderline nodes: 3531, overall activation: 3386.069, activation diff: 253.305, ratio: 0.075
#   pulse 7: activated nodes: 10967, borderline nodes: 3524, overall activation: 3473.380, activation diff: 87.311, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3473.4
#   number of spread. activ. pulses: 7
#   running time: 1401

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99952114   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9993678   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99920535   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982383   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967973   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4996874   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.4996603   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4996218   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.49962112   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.49961635   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.49961323   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49959683   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   14   0.49955153   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.49954015   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.49953133   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.4995245   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.49952012   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.49951026   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.4995063   REFERENCES:SIMDATES:SIMLOC
