###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 96.596, activation diff: 85.274, ratio: 0.883
#   pulse 3: activated nodes: 8881, borderline nodes: 4918, overall activation: 248.035, activation diff: 151.454, ratio: 0.611
#   pulse 4: activated nodes: 10534, borderline nodes: 5464, overall activation: 552.521, activation diff: 304.485, ratio: 0.551
#   pulse 5: activated nodes: 10780, borderline nodes: 4536, overall activation: 933.097, activation diff: 380.577, ratio: 0.408
#   pulse 6: activated nodes: 10878, borderline nodes: 3956, overall activation: 1337.938, activation diff: 404.840, ratio: 0.303
#   pulse 7: activated nodes: 10933, borderline nodes: 3670, overall activation: 1726.902, activation diff: 388.964, ratio: 0.225
#   pulse 8: activated nodes: 10958, borderline nodes: 3591, overall activation: 2075.829, activation diff: 348.927, ratio: 0.168
#   pulse 9: activated nodes: 10959, borderline nodes: 3550, overall activation: 2371.864, activation diff: 296.035, ratio: 0.125
#   pulse 10: activated nodes: 10961, borderline nodes: 3537, overall activation: 2614.245, activation diff: 242.381, ratio: 0.093
#   pulse 11: activated nodes: 10966, borderline nodes: 3530, overall activation: 2808.895, activation diff: 194.651, ratio: 0.069
#   pulse 12: activated nodes: 10967, borderline nodes: 3525, overall activation: 2963.510, activation diff: 154.615, ratio: 0.052
#   pulse 13: activated nodes: 10967, borderline nodes: 3525, overall activation: 3085.474, activation diff: 121.964, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3085.5
#   number of spread. activ. pulses: 13
#   running time: 1626

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98956585   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98596066   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98379874   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9837557   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9810031   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.971733   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.48080033   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.48074237   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.48053843   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.4804584   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.48034236   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.48031074   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.4800315   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47968218   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.4795701   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.47955856   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.47952342   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.47950518   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.4794715   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.47945988   REFERENCES:SIMDATES:SIMLOC
