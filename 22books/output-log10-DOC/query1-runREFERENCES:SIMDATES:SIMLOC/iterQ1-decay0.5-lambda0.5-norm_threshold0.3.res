###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 341.356, activation diff: 327.460, ratio: 0.959
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 619.473, activation diff: 280.050, ratio: 0.452
#   pulse 4: activated nodes: 10599, borderline nodes: 5688, overall activation: 1420.249, activation diff: 800.776, ratio: 0.564
#   pulse 5: activated nodes: 10872, borderline nodes: 4044, overall activation: 2142.883, activation diff: 722.634, ratio: 0.337
#   pulse 6: activated nodes: 10939, borderline nodes: 3662, overall activation: 2683.187, activation diff: 540.304, ratio: 0.201
#   pulse 7: activated nodes: 10959, borderline nodes: 3564, overall activation: 3027.743, activation diff: 344.556, ratio: 0.114
#   pulse 8: activated nodes: 10961, borderline nodes: 3540, overall activation: 3230.249, activation diff: 202.506, ratio: 0.063
#   pulse 9: activated nodes: 10966, borderline nodes: 3530, overall activation: 3346.168, activation diff: 115.919, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10966
#   final overall activation: 3346.2
#   number of spread. activ. pulses: 9
#   running time: 1389

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99774945   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99712825   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9966   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9962543   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99530935   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99171627   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.49742842   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49742   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.49740338   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49740314   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.4973463   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.4972003   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49718487   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.4971755   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.49714231   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.49712825   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.4971118   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.49708825   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.4970433   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.49703735   REFERENCES:SIMDATES:SIMLOC
