###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1698.843, activation diff: 1690.041, ratio: 0.995
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2760.794, activation diff: 1122.263, ratio: 0.407
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5581.285, activation diff: 2820.491, ratio: 0.505
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6624.911, activation diff: 1043.626, ratio: 0.158
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6963.916, activation diff: 339.006, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6963.9
#   number of spread. activ. pulses: 6
#   running time: 1329

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991438   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.998925   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9986696   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.998552   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99735856   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9956126   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7490299   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.7488248   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7488119   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.74881065   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7487212   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.7487183   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7486552   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7486516   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.7486255   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   16   0.74861145   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.7485955   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.74858457   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.74847317   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.7484564   REFERENCES:SIMDATES:SIMLOC
