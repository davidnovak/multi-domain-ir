###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1463.524, activation diff: 1488.645, ratio: 1.017
#   pulse 3: activated nodes: 9517, borderline nodes: 3858, overall activation: 465.849, activation diff: 1848.020, ratio: 3.967
#   pulse 4: activated nodes: 10873, borderline nodes: 3960, overall activation: 3119.302, activation diff: 2900.505, ratio: 0.930
#   pulse 5: activated nodes: 10963, borderline nodes: 3562, overall activation: 3275.656, activation diff: 472.752, ratio: 0.144
#   pulse 6: activated nodes: 10965, borderline nodes: 3528, overall activation: 3499.047, activation diff: 224.509, ratio: 0.064
#   pulse 7: activated nodes: 10968, borderline nodes: 3523, overall activation: 3515.084, activation diff: 16.142, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 3515.1
#   number of spread. activ. pulses: 7
#   running time: 1384

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999046   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99894935   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4999999   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4999997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.49999937   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   11   0.49999914   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4999991   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.4999991   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.49999833   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49999812   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.4999976   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.4999974   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.49999738   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49999717   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.49999702   REFERENCES:SIMDATES:SIMLOC
