###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1954.930, activation diff: 1979.498, ratio: 1.013
#   pulse 3: activated nodes: 9351, borderline nodes: 3990, overall activation: 901.325, activation diff: 2816.743, ratio: 3.125
#   pulse 4: activated nodes: 11345, borderline nodes: 3489, overall activation: 5766.825, activation diff: 5613.585, ratio: 0.973
#   pulse 5: activated nodes: 11427, borderline nodes: 471, overall activation: 6132.026, activation diff: 1294.548, ratio: 0.211
#   pulse 6: activated nodes: 11447, borderline nodes: 127, overall activation: 6730.985, activation diff: 610.881, ratio: 0.091
#   pulse 7: activated nodes: 11454, borderline nodes: 77, overall activation: 6771.067, activation diff: 41.658, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6771.1
#   number of spread. activ. pulses: 7
#   running time: 1401

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989897   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883914   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.75   REFERENCES:SIMDATES:SIMLOC
