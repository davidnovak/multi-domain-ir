###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1227.891, activation diff: 1229.723, ratio: 1.001
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 1440.864, activation diff: 869.117, ratio: 0.603
#   pulse 4: activated nodes: 11314, borderline nodes: 4365, overall activation: 4993.647, activation diff: 3567.415, ratio: 0.714
#   pulse 5: activated nodes: 11408, borderline nodes: 736, overall activation: 6645.112, activation diff: 1651.557, ratio: 0.249
#   pulse 6: activated nodes: 11445, borderline nodes: 138, overall activation: 7263.973, activation diff: 618.861, ratio: 0.085
#   pulse 7: activated nodes: 11453, borderline nodes: 49, overall activation: 7477.909, activation diff: 213.936, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 7477.9
#   number of spread. activ. pulses: 7
#   running time: 1415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993114   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99913156   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9987718   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99793595   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9962233   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.7992997   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7992709   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.7992081   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7991954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7991908   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7991857   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.7991693   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.799165   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.7991514   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7991044   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.799096   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7990868   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7990663   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.79905367   REFERENCES:SIMDATES:SIMLOC
