###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 920.860, activation diff: 900.996, ratio: 0.978
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2008.820, activation diff: 1087.959, ratio: 0.542
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 4270.695, activation diff: 2261.875, ratio: 0.530
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5846.214, activation diff: 1575.519, ratio: 0.269
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6771.174, activation diff: 924.960, ratio: 0.137
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7288.480, activation diff: 517.307, ratio: 0.071
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7572.671, activation diff: 284.191, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7572.7
#   number of spread. activ. pulses: 8
#   running time: 1446

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99549663   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99503875   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949564   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935844   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99044263   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79313636   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.79308814   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.79294753   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.7929386   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7928786   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.79284847   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7928139   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.79263043   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.79259324   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7925813   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.79250896   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7925081   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.79242575   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7923633   REFERENCES:SIMDATES:SIMLOC
