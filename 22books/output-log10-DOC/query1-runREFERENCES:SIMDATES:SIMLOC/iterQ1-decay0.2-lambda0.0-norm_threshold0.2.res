###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2336.857, activation diff: 2361.978, ratio: 1.011
#   pulse 3: activated nodes: 9517, borderline nodes: 3858, overall activation: 1297.184, activation diff: 3448.273, ratio: 2.658
#   pulse 4: activated nodes: 11362, borderline nodes: 2698, overall activation: 6748.658, activation diff: 6156.306, ratio: 0.912
#   pulse 5: activated nodes: 11442, borderline nodes: 277, overall activation: 7395.196, activation diff: 905.974, ratio: 0.123
#   pulse 6: activated nodes: 11455, borderline nodes: 48, overall activation: 7641.384, activation diff: 248.757, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7641.4
#   number of spread. activ. pulses: 6
#   running time: 1334

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990857   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   9   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_514   10   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   12   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_501   14   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_505   16   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   18   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.8   REFERENCES:SIMDATES:SIMLOC
