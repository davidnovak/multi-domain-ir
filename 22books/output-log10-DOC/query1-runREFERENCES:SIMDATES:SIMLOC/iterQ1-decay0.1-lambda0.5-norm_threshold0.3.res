###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 600.845, activation diff: 586.950, ratio: 0.977
#   pulse 3: activated nodes: 8944, borderline nodes: 4607, overall activation: 1219.442, activation diff: 622.011, ratio: 0.510
#   pulse 4: activated nodes: 11214, borderline nodes: 5562, overall activation: 3738.104, activation diff: 2518.662, ratio: 0.674
#   pulse 5: activated nodes: 11393, borderline nodes: 1077, overall activation: 5931.481, activation diff: 2193.377, ratio: 0.370
#   pulse 6: activated nodes: 11440, borderline nodes: 232, overall activation: 7368.784, activation diff: 1437.303, ratio: 0.195
#   pulse 7: activated nodes: 11450, borderline nodes: 74, overall activation: 8211.881, activation diff: 843.098, ratio: 0.103
#   pulse 8: activated nodes: 11455, borderline nodes: 36, overall activation: 8688.542, activation diff: 476.660, ratio: 0.055
#   pulse 9: activated nodes: 11455, borderline nodes: 21, overall activation: 8954.437, activation diff: 265.895, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 8954.4
#   number of spread. activ. pulses: 9
#   running time: 1458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9977495   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9971353   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99661547   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99625754   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9953437   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9918034   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.89540875   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89536434   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.8953484   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.89532715   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8952581   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.89516413   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.894999   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.894992   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.89492774   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.8949253   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   17   0.8948937   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.89488566   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.8948697   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.894819   REFERENCES:SIMDATES:SIMLOC
