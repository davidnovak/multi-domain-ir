###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1595.021, activation diff: 1593.910, ratio: 0.999
#   pulse 3: activated nodes: 9450, borderline nodes: 3922, overall activation: 2202.569, activation diff: 1147.497, ratio: 0.521
#   pulse 4: activated nodes: 11352, borderline nodes: 3190, overall activation: 6581.207, activation diff: 4382.050, ratio: 0.666
#   pulse 5: activated nodes: 11431, borderline nodes: 359, overall activation: 8401.522, activation diff: 1820.315, ratio: 0.217
#   pulse 6: activated nodes: 11454, borderline nodes: 53, overall activation: 9036.690, activation diff: 635.168, ratio: 0.070
#   pulse 7: activated nodes: 11458, borderline nodes: 16, overall activation: 9248.176, activation diff: 211.486, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 9248.2
#   number of spread. activ. pulses: 7
#   running time: 1407

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996104   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9995223   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99936855   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99920535   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982424   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967985   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89945215   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.8994126   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.8993709   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.89932925   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.89932287   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.8993216   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.89931154   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.89930874   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.8993029   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.8992373   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.89923203   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.89923203   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.8992119   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   20   0.8992027   REFERENCES:SIMDATES:SIMLOC
