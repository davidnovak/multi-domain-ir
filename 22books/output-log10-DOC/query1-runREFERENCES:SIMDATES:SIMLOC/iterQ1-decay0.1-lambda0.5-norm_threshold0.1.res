###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 878.867, activation diff: 860.483, ratio: 0.979
#   pulse 3: activated nodes: 9582, borderline nodes: 3878, overall activation: 1886.621, activation diff: 1007.755, ratio: 0.534
#   pulse 4: activated nodes: 11358, borderline nodes: 2962, overall activation: 4642.473, activation diff: 2755.851, ratio: 0.594
#   pulse 5: activated nodes: 11435, borderline nodes: 288, overall activation: 6672.132, activation diff: 2029.659, ratio: 0.304
#   pulse 6: activated nodes: 11455, borderline nodes: 50, overall activation: 7886.517, activation diff: 1214.385, ratio: 0.154
#   pulse 7: activated nodes: 11459, borderline nodes: 17, overall activation: 8571.210, activation diff: 684.693, ratio: 0.080
#   pulse 8: activated nodes: 11460, borderline nodes: 11, overall activation: 8949.186, activation diff: 377.976, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8949.2
#   number of spread. activ. pulses: 8
#   running time: 1378

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959232   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9952451   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9945899   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9945308   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9931437   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.989282   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.891918   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8918925   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8917849   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.89175963   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8916091   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8915198   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.891412   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.891353   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.891261   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.8912575   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.89119124   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.89117   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.891067   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.89106464   REFERENCES:SIMDATES:SIMLOC
