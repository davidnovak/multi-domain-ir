###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 250.500, activation diff: 237.781, ratio: 0.949
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 651.831, activation diff: 401.331, ratio: 0.616
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1493.781, activation diff: 841.950, ratio: 0.564
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2480.420, activation diff: 986.639, ratio: 0.398
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 3403.160, activation diff: 922.740, ratio: 0.271
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 4185.692, activation diff: 782.532, ratio: 0.187
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 4822.401, activation diff: 636.709, ratio: 0.132
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 5329.977, activation diff: 507.575, ratio: 0.095
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 5730.022, activation diff: 400.045, ratio: 0.070
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 6043.134, activation diff: 313.112, ratio: 0.052
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 6287.105, activation diff: 243.971, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6287.1
#   number of spread. activ. pulses: 12
#   running time: 1496

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.987568   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9841025   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98257524   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.981833   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9785936   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97078604   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7138614   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7138467   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7132746   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.71322185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7130693   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.7130606   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.71298134   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71210307   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.71201897   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7119657   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.71179837   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.71169674   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7116231   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7115307   REFERENCES:SIMDATES:SIMLOC
