###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 772.328, activation diff: 774.161, ratio: 1.002
#   pulse 3: activated nodes: 9185, borderline nodes: 4230, overall activation: 763.493, activation diff: 408.421, ratio: 0.535
#   pulse 4: activated nodes: 10742, borderline nodes: 4974, overall activation: 2254.769, activation diff: 1493.161, ratio: 0.662
#   pulse 5: activated nodes: 10933, borderline nodes: 3724, overall activation: 3007.263, activation diff: 752.494, ratio: 0.250
#   pulse 6: activated nodes: 10959, borderline nodes: 3567, overall activation: 3319.225, activation diff: 311.962, ratio: 0.094
#   pulse 7: activated nodes: 10963, borderline nodes: 3534, overall activation: 3432.301, activation diff: 113.076, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10963
#   final overall activation: 3432.3
#   number of spread. activ. pulses: 7
#   running time: 1333

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938405   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99930966   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991301   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99877167   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979275   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99621534   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   7   0.49955064   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.49954045   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49949062   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.4994813   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.49948028   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.4994686   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49945873   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.49942303   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   15   0.49940768   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.49939573   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   17   0.499367   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.49935892   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.49935034   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.4993467   REFERENCES:SIMDATES:SIMLOC
