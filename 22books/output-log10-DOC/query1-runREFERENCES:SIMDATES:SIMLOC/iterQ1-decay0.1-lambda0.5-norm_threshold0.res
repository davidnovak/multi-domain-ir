###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1033.038, activation diff: 1013.174, ratio: 0.981
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2326.723, activation diff: 1293.685, ratio: 0.556
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5135.823, activation diff: 2809.100, ratio: 0.547
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 7051.997, activation diff: 1916.173, ratio: 0.272
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8165.218, activation diff: 1113.221, ratio: 0.136
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8783.279, activation diff: 618.061, ratio: 0.070
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 9120.751, activation diff: 337.472, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 9120.8
#   number of spread. activ. pulses: 8
#   running time: 1453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959976   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9954971   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99503875   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949571   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935859   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99044335   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89228714   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89222765   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.89207095   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.89206314   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.89199394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.89196146   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8919215   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.89172876   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.89168566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.8916785   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.8915844   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.8915831   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.8914944   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.8914553   REFERENCES:SIMDATES:SIMLOC
