###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 584.327, activation diff: 564.462, ratio: 0.966
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1126.333, activation diff: 542.006, ratio: 0.481
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 2012.505, activation diff: 886.172, ratio: 0.440
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 2665.883, activation diff: 653.378, ratio: 0.245
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 3061.574, activation diff: 395.691, ratio: 0.129
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 3286.137, activation diff: 224.563, ratio: 0.068
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 3410.620, activation diff: 124.483, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 3410.6
#   number of spread. activ. pulses: 8
#   running time: 1393

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99599755   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9954939   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9950385   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949516   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99356973   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9904305   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49569464   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49567258   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4955827   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.4955727   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.49553788   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.4955041   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49549645   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.49535024   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.49533436   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49529263   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.49528137   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.4952513   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.49523664   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.49515927   REFERENCES:SIMDATES:SIMLOC
