###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 266.015, activation diff: 253.296, ratio: 0.952
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 701.672, activation diff: 435.657, ratio: 0.621
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1652.692, activation diff: 951.020, ratio: 0.575
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2765.031, activation diff: 1112.339, ratio: 0.402
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3797.343, activation diff: 1032.313, ratio: 0.272
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4668.510, activation diff: 871.167, ratio: 0.187
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5375.109, activation diff: 706.599, ratio: 0.131
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5937.172, activation diff: 562.063, ratio: 0.095
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 6379.425, activation diff: 442.253, ratio: 0.069
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 6725.104, activation diff: 345.679, ratio: 0.051
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 6994.140, activation diff: 269.036, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6994.1
#   number of spread. activ. pulses: 12
#   running time: 1609

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9875683   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9841068   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9825806   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9818413   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97860765   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9708273   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76146054   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7614478   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.76084125   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7607785   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.76062787   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.7606101   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.76052934   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7596154   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.75952476   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.75948113   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.75930446   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.75916195   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.75908446   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7590054   REFERENCES:SIMDATES:SIMLOC
