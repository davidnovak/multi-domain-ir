###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 614.806, activation diff: 598.554, ratio: 0.974
#   pulse 3: activated nodes: 9231, borderline nodes: 4103, overall activation: 1223.732, activation diff: 609.189, ratio: 0.498
#   pulse 4: activated nodes: 11298, borderline nodes: 4400, overall activation: 3057.064, activation diff: 1833.332, ratio: 0.600
#   pulse 5: activated nodes: 11407, borderline nodes: 792, overall activation: 4578.086, activation diff: 1521.023, ratio: 0.332
#   pulse 6: activated nodes: 11437, borderline nodes: 219, overall activation: 5544.922, activation diff: 966.836, ratio: 0.174
#   pulse 7: activated nodes: 11447, borderline nodes: 112, overall activation: 6105.746, activation diff: 560.824, ratio: 0.092
#   pulse 8: activated nodes: 11450, borderline nodes: 75, overall activation: 6421.009, activation diff: 315.263, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 6421.0
#   number of spread. activ. pulses: 8
#   running time: 1396

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99578035   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99486226   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9940702   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.993744   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99259436   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9878205   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7428489   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.74283445   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74282616   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7427699   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7426158   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.74239445   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.7423647   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7422488   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7422125   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7422109   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7421691   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7421546   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.7420811   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.7420695   REFERENCES:SIMDATES:SIMLOC
