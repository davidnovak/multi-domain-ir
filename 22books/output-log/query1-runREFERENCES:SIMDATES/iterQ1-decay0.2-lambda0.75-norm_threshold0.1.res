###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.055, activation diff: 3.055, ratio: 0.505
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 5.851, activation diff: 1.947, ratio: 0.333
#   pulse 3: activated nodes: 5501, borderline nodes: 5239, overall activation: 7.502, activation diff: 3.259, ratio: 0.434
#   pulse 4: activated nodes: 6100, borderline nodes: 5206, overall activation: 9.974, activation diff: 3.852, ratio: 0.386
#   pulse 5: activated nodes: 6259, borderline nodes: 5167, overall activation: 15.900, activation diff: 7.081, ratio: 0.445
#   pulse 6: activated nodes: 6532, borderline nodes: 5002, overall activation: 29.667, activation diff: 14.600, ratio: 0.492
#   pulse 7: activated nodes: 7413, borderline nodes: 5311, overall activation: 62.391, activation diff: 33.168, ratio: 0.532
#   pulse 8: activated nodes: 8140, borderline nodes: 5345, overall activation: 133.115, activation diff: 70.892, ratio: 0.533
#   pulse 9: activated nodes: 9181, borderline nodes: 5438, overall activation: 263.310, activation diff: 130.237, ratio: 0.495
#   pulse 10: activated nodes: 9893, borderline nodes: 5084, overall activation: 465.897, activation diff: 202.587, ratio: 0.435
#   pulse 11: activated nodes: 10568, borderline nodes: 4222, overall activation: 745.901, activation diff: 280.004, ratio: 0.375
#   pulse 12: activated nodes: 10901, borderline nodes: 3129, overall activation: 1097.427, activation diff: 351.526, ratio: 0.320
#   pulse 13: activated nodes: 11091, borderline nodes: 2038, overall activation: 1499.809, activation diff: 402.381, ratio: 0.268
#   pulse 14: activated nodes: 11183, borderline nodes: 1346, overall activation: 1922.911, activation diff: 423.102, ratio: 0.220
#   pulse 15: activated nodes: 11223, borderline nodes: 902, overall activation: 2338.916, activation diff: 416.005, ratio: 0.178

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11223
#   final overall activation: 2338.9
#   number of spread. activ. pulses: 15
#   running time: 567

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.85863984   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.75147694   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.7238693   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.7055546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   5   0.6647687   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.6575627   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.6504324   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   8   0.64900184   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   9   0.64869016   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.6472218   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.6467252   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   12   0.6428786   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.6428209   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   14   0.6424182   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   15   0.6363343   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   16   0.63607484   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   17   0.6359824   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.634037   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   19   0.63293326   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   20   0.6308639   REFERENCES:SIMDATES
