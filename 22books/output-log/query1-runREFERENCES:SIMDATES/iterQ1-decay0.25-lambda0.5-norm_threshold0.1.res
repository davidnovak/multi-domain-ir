###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.111, activation diff: 6.111, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 19.233, activation diff: 16.850, ratio: 0.876
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 22.580, activation diff: 6.545, ratio: 0.290
#   pulse 4: activated nodes: 7289, borderline nodes: 5642, overall activation: 145.192, activation diff: 123.505, ratio: 0.851
#   pulse 5: activated nodes: 8433, borderline nodes: 5013, overall activation: 306.149, activation diff: 161.030, ratio: 0.526
#   pulse 6: activated nodes: 9696, borderline nodes: 5363, overall activation: 793.247, activation diff: 487.098, ratio: 0.614
#   pulse 7: activated nodes: 10682, borderline nodes: 3887, overall activation: 1414.478, activation diff: 621.231, ratio: 0.439
#   pulse 8: activated nodes: 11055, borderline nodes: 2112, overall activation: 2121.557, activation diff: 707.080, ratio: 0.333
#   pulse 9: activated nodes: 11202, borderline nodes: 1142, overall activation: 2750.159, activation diff: 628.601, ratio: 0.229
#   pulse 10: activated nodes: 11234, borderline nodes: 722, overall activation: 3234.948, activation diff: 484.789, ratio: 0.150
#   pulse 11: activated nodes: 11247, borderline nodes: 606, overall activation: 3579.266, activation diff: 344.318, ratio: 0.096
#   pulse 12: activated nodes: 11291, borderline nodes: 598, overall activation: 3812.420, activation diff: 233.154, ratio: 0.061
#   pulse 13: activated nodes: 11294, borderline nodes: 583, overall activation: 3965.794, activation diff: 153.374, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11294
#   final overall activation: 3965.8
#   number of spread. activ. pulses: 13
#   running time: 528

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99631095   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9783392   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9751916   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9599389   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9134712   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8772419   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7468803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74603415   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7453065   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7441814   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.74415857   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   12   0.74409914   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.743955   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   14   0.74377143   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.7437644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   16   0.7435031   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7434728   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.7433127   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   19   0.7430731   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   20   0.7429652   REFERENCES:SIMDATES
