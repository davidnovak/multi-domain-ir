###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.312, activation diff: 9.312, ratio: 2.811
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 22.978, activation diff: 26.290, ratio: 1.144
#   pulse 3: activated nodes: 4656, borderline nodes: 3767, overall activation: 4.543, activation diff: 27.521, ratio: 6.058
#   pulse 4: activated nodes: 6156, borderline nodes: 5255, overall activation: 486.990, activation diff: 491.533, ratio: 1.009
#   pulse 5: activated nodes: 7816, borderline nodes: 4618, overall activation: 136.763, activation diff: 623.753, ratio: 4.561
#   pulse 6: activated nodes: 9605, borderline nodes: 5531, overall activation: 2476.050, activation diff: 2612.813, ratio: 1.055
#   pulse 7: activated nodes: 10891, borderline nodes: 3333, overall activation: 796.452, activation diff: 3272.501, ratio: 4.109
#   pulse 8: activated nodes: 11130, borderline nodes: 1269, overall activation: 3361.674, activation diff: 4156.369, ratio: 1.236
#   pulse 9: activated nodes: 11238, borderline nodes: 752, overall activation: 1025.555, activation diff: 4385.471, ratio: 4.276
#   pulse 10: activated nodes: 11287, borderline nodes: 387, overall activation: 3438.772, activation diff: 4462.270, ratio: 1.298
#   pulse 11: activated nodes: 11299, borderline nodes: 364, overall activation: 1047.619, activation diff: 4484.335, ratio: 4.281
#   pulse 12: activated nodes: 11301, borderline nodes: 335, overall activation: 3446.705, activation diff: 4492.259, ratio: 1.303
#   pulse 13: activated nodes: 11301, borderline nodes: 330, overall activation: 1050.398, activation diff: 4495.038, ratio: 4.279
#   pulse 14: activated nodes: 11301, borderline nodes: 327, overall activation: 3447.941, activation diff: 4496.274, ratio: 1.304
#   pulse 15: activated nodes: 11301, borderline nodes: 325, overall activation: 1050.963, activation diff: 4496.839, ratio: 4.279

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11301
#   final overall activation: 1051.0
#   number of spread. activ. pulses: 15
#   running time: 557

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES:SIMDATES
