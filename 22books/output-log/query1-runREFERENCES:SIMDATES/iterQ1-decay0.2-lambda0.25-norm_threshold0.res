###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.480, activation diff: 11.480, ratio: 1.354
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 246.623, activation diff: 245.042, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 312.000, activation diff: 181.869, ratio: 0.583
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1982.602, activation diff: 1671.000, ratio: 0.843
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3109.483, activation diff: 1126.881, ratio: 0.362
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4203.242, activation diff: 1093.758, ratio: 0.260
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 4788.335, activation diff: 585.093, ratio: 0.122
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 5059.902, activation diff: 271.567, ratio: 0.054
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 5179.214, activation diff: 119.312, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5179.2
#   number of spread. activ. pulses: 9
#   running time: 471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99940926   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9899188   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98858714   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.973194   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9305708   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9083405   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7997881   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.79961383   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.79951847   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7994447   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.79943496   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7994325   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.79941493   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7993874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   15   0.7993562   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.79935116   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   17   0.79933167   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.7993154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   19   0.79927933   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.79925966   REFERENCES:SIMDATES
