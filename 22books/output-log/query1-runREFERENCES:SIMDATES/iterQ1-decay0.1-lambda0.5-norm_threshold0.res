###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.654, activation diff: 7.654, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 127.861, activation diff: 121.867, ratio: 0.953
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 245.127, activation diff: 118.126, ratio: 0.482
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1122.103, activation diff: 877.022, ratio: 0.782
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2267.045, activation diff: 1144.942, ratio: 0.505
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3611.544, activation diff: 1344.500, ratio: 0.372
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4737.501, activation diff: 1125.957, ratio: 0.238
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 5556.536, activation diff: 819.035, ratio: 0.147
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 6110.625, activation diff: 554.089, ratio: 0.091
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 6471.305, activation diff: 360.679, ratio: 0.056
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 6701.164, activation diff: 229.859, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6701.2
#   number of spread. activ. pulses: 11
#   running time: 500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9963473   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98209083   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9817585   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96484685   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9215205   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89628685   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.89605117   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.8954272   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   9   0.8952432   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.8952252   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   11   0.89518213   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   12   0.8951062   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   13   0.8949673   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.89482594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   15   0.89481413   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   16   0.89479506   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   17   0.894714   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   18   0.894709   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.89469504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.89455783   REFERENCES:SIMDATES
