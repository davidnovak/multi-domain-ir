###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.670, activation diff: 7.670, ratio: 4.593
#   pulse 2: activated nodes: 3428, borderline nodes: 3399, overall activation: 0.606, activation diff: 2.276, ratio: 3.756
#   pulse 3: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.606, ratio: Infinity
#   pulse 4: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 5: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 6: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 7: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 8: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 9: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 10: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 11: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 12: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 13: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 14: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 15: activated nodes: 3428, borderline nodes: 3367, overall activation: 0.000, activation diff: 0.000, ratio: Infinity

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3428
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 218

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_376   1   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_381   2   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_385   3   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_383   4   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_368   5   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_367   6   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_365   7   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_370   8   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_374   9   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   10   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_375   11   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   12   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_366   13   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   14   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_200   15   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_384   16   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_380   17   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_382   18   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_377   19   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_379   20   0.0   REFERENCES:SIMDATES
