###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.654, activation diff: 7.654, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 107.695, activation diff: 101.701, ratio: 0.944
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 192.851, activation diff: 86.022, ratio: 0.446
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 771.067, activation diff: 578.269, ratio: 0.750
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1479.661, activation diff: 708.593, ratio: 0.479
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2289.588, activation diff: 809.928, ratio: 0.354
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2976.155, activation diff: 686.567, ratio: 0.231
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 3483.958, activation diff: 507.803, ratio: 0.146
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 3832.736, activation diff: 348.778, ratio: 0.091
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 4062.403, activation diff: 229.667, ratio: 0.057
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 4209.876, activation diff: 147.474, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4209.9
#   number of spread. activ. pulses: 11
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996127   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9805953   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9803574   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.962445   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91924906   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8888094   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74642336   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74535346   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7452996   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.74390405   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.74385405   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   12   0.7438085   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.7437905   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.74378216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.74374866   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.74367756   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   17   0.7436397   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.74358   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.74351716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   20   0.74347293   REFERENCES:SIMDATES
