###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.480, activation diff: 11.480, ratio: 1.354
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 156.163, activation diff: 154.582, ratio: 0.990
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 143.327, activation diff: 60.608, ratio: 0.423
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 795.281, activation diff: 652.504, ratio: 0.820
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1081.581, activation diff: 286.309, ratio: 0.265
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1344.144, activation diff: 262.563, ratio: 0.195
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1463.202, activation diff: 119.057, ratio: 0.081
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1512.389, activation diff: 49.187, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1512.4
#   number of spread. activ. pulses: 8
#   running time: 411

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9983009   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9816117   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98064363   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9611365   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.918669   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.87952054   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49768928   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49549973   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49402818   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49253082   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4911968   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.48841903   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4865119   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.48485285   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.4841956   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.483261   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.48087204   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.4808519   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.48058772   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   20   0.4802577   REFERENCES:SIMDATES
