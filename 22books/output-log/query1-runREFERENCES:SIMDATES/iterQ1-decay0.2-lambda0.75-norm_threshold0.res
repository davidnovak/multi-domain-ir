###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.827, activation diff: 3.827, ratio: 0.561
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 33.990, activation diff: 29.073, ratio: 0.855
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 73.560, activation diff: 40.834, ratio: 0.555
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 184.839, activation diff: 111.826, ratio: 0.605
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 388.463, activation diff: 203.722, ratio: 0.524
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 701.570, activation diff: 313.107, ratio: 0.446
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 1107.771, activation diff: 406.201, ratio: 0.367
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 1570.864, activation diff: 463.093, ratio: 0.295
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 2048.740, activation diff: 477.876, ratio: 0.233
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 2508.838, activation diff: 460.098, ratio: 0.183
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 2931.697, activation diff: 422.859, ratio: 0.144
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 3308.193, activation diff: 376.495, ratio: 0.114
#   pulse 13: activated nodes: 11342, borderline nodes: 14, overall activation: 3635.994, activation diff: 327.801, ratio: 0.090
#   pulse 14: activated nodes: 11342, borderline nodes: 14, overall activation: 3916.798, activation diff: 280.805, ratio: 0.072
#   pulse 15: activated nodes: 11342, borderline nodes: 14, overall activation: 4154.450, activation diff: 237.652, ratio: 0.057

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 4154.5
#   number of spread. activ. pulses: 15
#   running time: 596

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9684975   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.93116856   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.92080116   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9029685   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8580399   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.80013186   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7609602   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7593647   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7542103   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.75377524   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.75315255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.7519417   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.75158703   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7510842   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.7509165   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.7508364   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.75028414   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   18   0.74974185   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.74952364   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   20   0.74918634   REFERENCES:SIMDATES
