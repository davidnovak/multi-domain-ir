###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.312, activation diff: 9.312, ratio: 2.811
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 25.765, activation diff: 29.078, ratio: 1.129
#   pulse 3: activated nodes: 4656, borderline nodes: 3767, overall activation: 5.231, activation diff: 30.996, ratio: 5.926
#   pulse 4: activated nodes: 6161, borderline nodes: 5259, overall activation: 626.063, activation diff: 631.294, ratio: 1.008
#   pulse 5: activated nodes: 7891, borderline nodes: 4499, overall activation: 221.994, activation diff: 848.058, ratio: 3.820
#   pulse 6: activated nodes: 9757, borderline nodes: 5258, overall activation: 3431.265, activation diff: 3653.259, ratio: 1.065
#   pulse 7: activated nodes: 11023, borderline nodes: 3062, overall activation: 1319.780, activation diff: 4751.045, ratio: 3.600
#   pulse 8: activated nodes: 11207, borderline nodes: 816, overall activation: 4522.115, activation diff: 5836.681, ratio: 1.291
#   pulse 9: activated nodes: 11272, borderline nodes: 378, overall activation: 1638.591, activation diff: 6155.492, ratio: 3.757
#   pulse 10: activated nodes: 11315, borderline nodes: 156, overall activation: 4616.328, activation diff: 6247.500, ratio: 1.353
#   pulse 11: activated nodes: 11322, borderline nodes: 119, overall activation: 1672.135, activation diff: 6281.044, ratio: 3.756
#   pulse 12: activated nodes: 11323, borderline nodes: 103, overall activation: 4630.120, activation diff: 6294.624, ratio: 1.359
#   pulse 13: activated nodes: 11325, borderline nodes: 95, overall activation: 1677.562, activation diff: 6300.052, ratio: 3.755
#   pulse 14: activated nodes: 11326, borderline nodes: 94, overall activation: 4632.948, activation diff: 6302.849, ratio: 1.360
#   pulse 15: activated nodes: 11326, borderline nodes: 92, overall activation: 1678.679, activation diff: 6303.965, ratio: 3.755

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 1678.7
#   number of spread. activ. pulses: 15
#   running time: 666

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES:SIMDATES
