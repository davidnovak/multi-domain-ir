###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.222, activation diff: 12.222, ratio: 1.964
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 94.133, activation diff: 100.355, ratio: 1.066
#   pulse 3: activated nodes: 7510, borderline nodes: 4657, overall activation: 22.002, activation diff: 116.135, ratio: 5.278
#   pulse 4: activated nodes: 7775, borderline nodes: 4868, overall activation: 935.832, activation diff: 957.834, ratio: 1.024
#   pulse 5: activated nodes: 8780, borderline nodes: 3761, overall activation: 75.687, activation diff: 1011.519, ratio: 13.364
#   pulse 6: activated nodes: 8786, borderline nodes: 3755, overall activation: 1329.539, activation diff: 1404.302, ratio: 1.056
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 87.872, activation diff: 1416.487, ratio: 16.120
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1363.674, activation diff: 1449.482, ratio: 1.063
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 88.727, activation diff: 1450.293, ratio: 16.346
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1365.136, activation diff: 1451.630, ratio: 1.063
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 88.801, activation diff: 1451.678, ratio: 16.348
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1365.249, activation diff: 1451.757, ratio: 1.063
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 88.813, activation diff: 1451.758, ratio: 16.346
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 1365.265, activation diff: 1451.762, ratio: 1.063
#   pulse 15: activated nodes: 8935, borderline nodes: 3614, overall activation: 88.816, activation diff: 1451.761, ratio: 16.346

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 88.8
#   number of spread. activ. pulses: 15
#   running time: 542

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_118   1   0.007444709   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_280   2   0.0074225636   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.005982071   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.0056731766   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_197   5   0.005578017   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   6   0.0029230814   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_240   7   0.0028688707   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.0023443997   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_177   9   0.0017351479   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_200   10   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   11   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   12   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   14   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_350   15   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_181   16   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_216   17   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   18   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_215   20   0.0   REFERENCES:SIMDATES
