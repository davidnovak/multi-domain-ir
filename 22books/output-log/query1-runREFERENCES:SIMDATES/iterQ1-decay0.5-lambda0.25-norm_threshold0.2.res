###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.984, activation diff: 6.984, ratio: 1.753
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 3.710, activation diff: 5.129, ratio: 1.383
#   pulse 3: activated nodes: 4229, borderline nodes: 3823, overall activation: 1.039, activation diff: 2.754, ratio: 2.651
#   pulse 4: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.260, activation diff: 0.779, ratio: 3.000
#   pulse 5: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.065, activation diff: 0.195, ratio: 3.000
#   pulse 6: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.016, activation diff: 0.049, ratio: 3.000
#   pulse 7: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.004, activation diff: 0.012, ratio: 3.000
#   pulse 8: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.001, activation diff: 0.003, ratio: 3.000
#   pulse 9: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.001, ratio: 3.000
#   pulse 10: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 11: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 12: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 13: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 14: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 15: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4229
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 310

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   1.9957576E-9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   1.9663449E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   3   1.9426358E-9   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   1.6514036E-9   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   1.1486558E-9   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   6   9.483192E-10   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_657   7   3.737934E-10   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_241   8   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_18   9   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_147   10   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_119   11   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_134   12   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_136   13   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_137   14   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_164   15   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_151   16   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_62   17   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_281   18   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_0   19   3.6823306E-10   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_14   20   3.6823306E-10   REFERENCES:SIMDATES
