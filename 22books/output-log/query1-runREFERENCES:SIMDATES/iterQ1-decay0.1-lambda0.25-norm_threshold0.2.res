###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.984, activation diff: 6.984, ratio: 1.753
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 5.651, activation diff: 7.071, ratio: 1.251
#   pulse 3: activated nodes: 4229, borderline nodes: 3823, overall activation: 1.760, activation diff: 4.355, ratio: 2.475
#   pulse 4: activated nodes: 4229, borderline nodes: 3823, overall activation: 1.348, activation diff: 1.594, ratio: 1.182
#   pulse 5: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.337, activation diff: 1.011, ratio: 3.000
#   pulse 6: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.084, activation diff: 0.253, ratio: 3.000
#   pulse 7: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.021, activation diff: 0.063, ratio: 3.000
#   pulse 8: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.005, activation diff: 0.016, ratio: 3.000
#   pulse 9: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.001, activation diff: 0.004, ratio: 3.000
#   pulse 10: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.001, ratio: 3.000
#   pulse 11: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 12: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 13: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 14: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 15: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4229
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 323

###################################
# top k results in TREC format: 

1   Q1   bostoncollegebul0405bost_147   1   7.404691E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_46   2   7.404691E-9   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_241   3   7.404691E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_18   4   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_281   5   7.404691E-9   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_0   6   7.404691E-9   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_14   7   7.404691E-9   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_119   8   7.404691E-9   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_62   9   7.404691E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_514   10   7.404691E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_491   11   7.404691E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_341   12   7.404691E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_383   13   7.404691E-9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_497   14   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_134   15   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_136   16   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_137   17   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_164   18   7.404691E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_151   19   7.404691E-9   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_102   20   7.404691E-9   REFERENCES:SIMDATES
