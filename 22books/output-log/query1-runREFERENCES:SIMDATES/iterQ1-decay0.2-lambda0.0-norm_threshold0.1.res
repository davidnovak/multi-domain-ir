###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.222, activation diff: 12.222, ratio: 1.964
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 149.624, activation diff: 155.846, ratio: 1.042
#   pulse 3: activated nodes: 7510, borderline nodes: 4657, overall activation: 57.308, activation diff: 206.932, ratio: 3.611
#   pulse 4: activated nodes: 9229, borderline nodes: 5808, overall activation: 2117.639, activation diff: 2174.947, ratio: 1.027
#   pulse 5: activated nodes: 10613, borderline nodes: 3655, overall activation: 761.874, activation diff: 2879.513, ratio: 3.780
#   pulse 6: activated nodes: 11062, borderline nodes: 1359, overall activation: 3632.961, activation diff: 4384.481, ratio: 1.207
#   pulse 7: activated nodes: 11263, borderline nodes: 630, overall activation: 1185.830, activation diff: 4801.067, ratio: 4.049
#   pulse 8: activated nodes: 11307, borderline nodes: 194, overall activation: 3771.644, activation diff: 4920.933, ratio: 1.305
#   pulse 9: activated nodes: 11315, borderline nodes: 162, overall activation: 1273.962, activation diff: 4908.616, ratio: 3.853
#   pulse 10: activated nodes: 11318, borderline nodes: 151, overall activation: 3852.457, activation diff: 4849.659, ratio: 1.259
#   pulse 11: activated nodes: 11318, borderline nodes: 150, overall activation: 3019.021, activation diff: 3112.747, ratio: 1.031
#   pulse 12: activated nodes: 11318, borderline nodes: 149, overall activation: 4551.115, activation diff: 2418.421, ratio: 0.531
#   pulse 13: activated nodes: 11318, borderline nodes: 147, overall activation: 4848.059, activation diff: 600.182, ratio: 0.124
#   pulse 14: activated nodes: 11319, borderline nodes: 141, overall activation: 4964.416, activation diff: 192.289, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 4964.4
#   number of spread. activ. pulses: 14
#   running time: 595

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996051   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99015254   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98853254   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97207737   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92533755   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90270007   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79998535   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.79992336   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7998716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.79985976   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.7998442   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7998196   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.7997947   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.79978293   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.7997783   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7997694   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   17   0.79976594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.79972965   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.79971814   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.79970616   REFERENCES:SIMDATES
