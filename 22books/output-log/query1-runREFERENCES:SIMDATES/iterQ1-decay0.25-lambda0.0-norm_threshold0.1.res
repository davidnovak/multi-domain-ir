###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.222, activation diff: 12.222, ratio: 1.964
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 140.376, activation diff: 146.598, ratio: 1.044
#   pulse 3: activated nodes: 7510, borderline nodes: 4657, overall activation: 49.440, activation diff: 189.816, ratio: 3.839
#   pulse 4: activated nodes: 9215, borderline nodes: 5826, overall activation: 1865.936, activation diff: 1915.376, ratio: 1.026
#   pulse 5: activated nodes: 10567, borderline nodes: 3772, overall activation: 592.020, activation diff: 2457.955, ratio: 4.152
#   pulse 6: activated nodes: 11007, borderline nodes: 1605, overall activation: 3137.690, activation diff: 3722.463, ratio: 1.186
#   pulse 7: activated nodes: 11236, borderline nodes: 765, overall activation: 922.058, activation diff: 4049.635, ratio: 4.392
#   pulse 8: activated nodes: 11269, borderline nodes: 654, overall activation: 3252.174, activation diff: 4156.860, ratio: 1.278
#   pulse 9: activated nodes: 11285, borderline nodes: 639, overall activation: 961.034, activation diff: 4179.501, ratio: 4.349
#   pulse 10: activated nodes: 11285, borderline nodes: 635, overall activation: 3271.372, activation diff: 4176.118, ratio: 1.277
#   pulse 11: activated nodes: 11285, borderline nodes: 635, overall activation: 1167.921, activation diff: 3974.825, ratio: 3.403
#   pulse 12: activated nodes: 11285, borderline nodes: 635, overall activation: 3409.638, activation diff: 3838.012, ratio: 1.126
#   pulse 13: activated nodes: 11285, borderline nodes: 635, overall activation: 3432.608, activation diff: 1575.011, ratio: 0.459
#   pulse 14: activated nodes: 11285, borderline nodes: 635, overall activation: 4048.752, activation diff: 936.872, ratio: 0.231
#   pulse 15: activated nodes: 11285, borderline nodes: 635, overall activation: 4165.544, activation diff: 212.001, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11285
#   final overall activation: 4165.5
#   number of spread. activ. pulses: 15
#   running time: 567

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995637   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9901168   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9872347   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97019684   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9229047   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89734554   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74996877   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7497826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7497394   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.74967223   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7495493   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.74951744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7494276   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   14   0.7494081   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7493792   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.74927056   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.7492283   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   18   0.7491939   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   19   0.74916184   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.74911547   REFERENCES:SIMDATES
