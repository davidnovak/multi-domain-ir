###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.328, activation diff: 2.328, ratio: 0.437
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 4.385, activation diff: 1.366, ratio: 0.311
#   pulse 3: activated nodes: 3956, borderline nodes: 3901, overall activation: 3.450, activation diff: 0.969, ratio: 0.281
#   pulse 4: activated nodes: 3956, borderline nodes: 3901, overall activation: 2.646, activation diff: 0.803, ratio: 0.304
#   pulse 5: activated nodes: 3956, borderline nodes: 3901, overall activation: 2.007, activation diff: 0.639, ratio: 0.318
#   pulse 6: activated nodes: 3956, borderline nodes: 3901, overall activation: 1.507, activation diff: 0.500, ratio: 0.332
#   pulse 7: activated nodes: 3956, borderline nodes: 3901, overall activation: 1.131, activation diff: 0.377, ratio: 0.333
#   pulse 8: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.848, activation diff: 0.283, ratio: 0.333
#   pulse 9: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.636, activation diff: 0.212, ratio: 0.333
#   pulse 10: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.477, activation diff: 0.159, ratio: 0.333
#   pulse 11: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.358, activation diff: 0.119, ratio: 0.333
#   pulse 12: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.268, activation diff: 0.089, ratio: 0.333
#   pulse 13: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.201, activation diff: 0.067, ratio: 0.333
#   pulse 14: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.151, activation diff: 0.050, ratio: 0.333
#   pulse 15: activated nodes: 3956, borderline nodes: 3901, overall activation: 0.113, activation diff: 0.038, ratio: 0.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3956
#   final overall activation: 0.1
#   number of spread. activ. pulses: 15
#   running time: 242

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   0.013363461   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   2   0.013363461   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   3   0.013363461   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.013363461   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   5   0.013363461   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   6   0.013363461   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_200   11   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_368   12   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_367   13   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_365   14   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   15   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_206   16   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_205   17   0.0   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_366   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   20   0.0   REFERENCES:SIMDATES
