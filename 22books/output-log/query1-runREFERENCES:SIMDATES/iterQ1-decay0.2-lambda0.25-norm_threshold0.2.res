###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.984, activation diff: 6.984, ratio: 1.753
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 5.166, activation diff: 6.585, ratio: 1.275
#   pulse 3: activated nodes: 4229, borderline nodes: 3823, overall activation: 1.572, activation diff: 3.938, ratio: 2.505
#   pulse 4: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.822, activation diff: 1.146, ratio: 1.394
#   pulse 5: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.205, activation diff: 0.616, ratio: 3.000
#   pulse 6: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.051, activation diff: 0.154, ratio: 3.000
#   pulse 7: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.013, activation diff: 0.039, ratio: 3.000
#   pulse 8: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.003, activation diff: 0.010, ratio: 3.000
#   pulse 9: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.001, activation diff: 0.002, ratio: 3.000
#   pulse 10: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.001, ratio: 3.000
#   pulse 11: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 12: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 13: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 14: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 15: activated nodes: 4229, borderline nodes: 3823, overall activation: 0.000, activation diff: 0.000, ratio: 3.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4229
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 324

###################################
# top k results in TREC format: 

1   Q1   bostoncollegebul0405bost_147   1   4.12126E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_46   2   4.12126E-9   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_241   3   4.12126E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_18   4   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_281   5   4.12126E-9   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_0   6   4.12126E-9   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_14   7   4.12126E-9   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_119   8   4.12126E-9   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_62   9   4.12126E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_514   10   4.12126E-9   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_491   11   4.12126E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_341   12   4.12126E-9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_383   13   4.12126E-9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_497   14   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_134   15   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_136   16   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_137   17   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_164   18   4.12126E-9   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_151   19   4.12126E-9   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_102   20   4.12126E-9   REFERENCES:SIMDATES
