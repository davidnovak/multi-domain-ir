###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.055, activation diff: 3.055, ratio: 0.505
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 5.886, activation diff: 1.982, ratio: 0.337
#   pulse 3: activated nodes: 5501, borderline nodes: 5239, overall activation: 7.833, activation diff: 3.553, ratio: 0.454
#   pulse 4: activated nodes: 6100, borderline nodes: 5206, overall activation: 10.781, activation diff: 4.313, ratio: 0.400
#   pulse 5: activated nodes: 6262, borderline nodes: 5164, overall activation: 18.383, activation diff: 8.728, ratio: 0.475
#   pulse 6: activated nodes: 6592, borderline nodes: 5009, overall activation: 36.716, activation diff: 19.115, ratio: 0.521
#   pulse 7: activated nodes: 7561, borderline nodes: 5341, overall activation: 81.849, activation diff: 45.500, ratio: 0.556
#   pulse 8: activated nodes: 8507, borderline nodes: 5551, overall activation: 180.405, activation diff: 98.684, ratio: 0.547
#   pulse 9: activated nodes: 9500, borderline nodes: 5417, overall activation: 362.294, activation diff: 181.912, ratio: 0.502
#   pulse 10: activated nodes: 10273, borderline nodes: 4679, overall activation: 652.069, activation diff: 289.775, ratio: 0.444
#   pulse 11: activated nodes: 10766, borderline nodes: 3624, overall activation: 1062.096, activation diff: 410.027, ratio: 0.386
#   pulse 12: activated nodes: 11053, borderline nodes: 2321, overall activation: 1579.747, activation diff: 517.651, ratio: 0.328
#   pulse 13: activated nodes: 11185, borderline nodes: 1440, overall activation: 2161.946, activation diff: 582.199, ratio: 0.269
#   pulse 14: activated nodes: 11225, borderline nodes: 828, overall activation: 2758.599, activation diff: 596.653, ratio: 0.216
#   pulse 15: activated nodes: 11248, borderline nodes: 491, overall activation: 3331.924, activation diff: 573.325, ratio: 0.172

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11248
#   final overall activation: 3331.9
#   number of spread. activ. pulses: 15
#   running time: 522

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.87286294   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.77672696   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   3   0.76864034   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7606056   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   5   0.75511473   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   6   0.75195664   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   7   0.75069594   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   8   0.7496541   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   9   0.74872917   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.7481788   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   11   0.7480896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   12   0.74793136   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   13   0.7445581   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.7426709   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.7422814   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.74200636   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.7404038   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   18   0.73855424   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.73764104   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_77   20   0.7351905   REFERENCES:SIMDATES
