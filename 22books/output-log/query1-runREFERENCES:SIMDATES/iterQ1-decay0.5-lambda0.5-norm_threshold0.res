###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.654, activation diff: 7.654, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 74.085, activation diff: 68.091, ratio: 0.919
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 116.785, activation diff: 43.575, ratio: 0.373
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 375.363, activation diff: 258.641, ratio: 0.689
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 653.274, activation diff: 277.911, ratio: 0.425
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 923.872, activation diff: 270.598, ratio: 0.293
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1136.819, activation diff: 212.947, ratio: 0.187
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1287.330, activation diff: 150.511, ratio: 0.117
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1386.511, activation diff: 99.181, ratio: 0.072
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1448.942, activation diff: 62.432, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1448.9
#   number of spread. activ. pulses: 10
#   running time: 444

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911467   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9638778   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.95717263   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.94008034   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.89816   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8409326   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4909839   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.48714846   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.48655722   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.48583937   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48351732   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.48229164   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.47887027   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.47781882   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.4734306   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.47341973   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.47334677   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.47311816   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.47086713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.47044724   REFERENCES:SIMDATES
