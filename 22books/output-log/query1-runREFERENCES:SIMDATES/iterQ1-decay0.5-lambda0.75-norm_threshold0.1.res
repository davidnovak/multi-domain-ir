###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.055, activation diff: 3.055, ratio: 0.505
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 5.746, activation diff: 1.842, ratio: 0.321
#   pulse 3: activated nodes: 5501, borderline nodes: 5239, overall activation: 6.509, activation diff: 2.377, ratio: 0.365
#   pulse 4: activated nodes: 6100, borderline nodes: 5206, overall activation: 7.647, activation diff: 2.570, ratio: 0.336
#   pulse 5: activated nodes: 6243, borderline nodes: 5168, overall activation: 9.741, activation diff: 3.346, ratio: 0.344
#   pulse 6: activated nodes: 6403, borderline nodes: 5098, overall activation: 13.933, activation diff: 5.195, ratio: 0.373
#   pulse 7: activated nodes: 6627, borderline nodes: 4975, overall activation: 22.454, activation diff: 9.240, ratio: 0.411
#   pulse 8: activated nodes: 6959, borderline nodes: 4831, overall activation: 39.601, activation diff: 17.553, ratio: 0.443
#   pulse 9: activated nodes: 7362, borderline nodes: 4711, overall activation: 72.195, activation diff: 32.771, ratio: 0.454
#   pulse 10: activated nodes: 7752, borderline nodes: 4507, overall activation: 127.915, activation diff: 55.781, ratio: 0.436
#   pulse 11: activated nodes: 8207, borderline nodes: 4325, overall activation: 208.283, activation diff: 80.375, ratio: 0.386
#   pulse 12: activated nodes: 8461, borderline nodes: 4197, overall activation: 308.011, activation diff: 99.729, ratio: 0.324
#   pulse 13: activated nodes: 8587, borderline nodes: 3983, overall activation: 419.897, activation diff: 111.885, ratio: 0.266
#   pulse 14: activated nodes: 8717, borderline nodes: 3836, overall activation: 536.610, activation diff: 116.714, ratio: 0.218
#   pulse 15: activated nodes: 8815, borderline nodes: 3722, overall activation: 652.245, activation diff: 115.635, ratio: 0.177

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8815
#   final overall activation: 652.2
#   number of spread. activ. pulses: 15
#   running time: 483

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.760573   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.60083395   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   3   0.5966625   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.5791626   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.36531776   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   6   0.35672098   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   7   0.35463077   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_37   8   0.34380978   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.3434745   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   10   0.340757   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   11   0.34042472   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   12   0.3398686   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.3368721   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   14   0.33542758   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.33441046   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   16   0.33434385   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_398   17   0.33379084   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.3313105   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.33106047   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_216   20   0.33018583   REFERENCES:SIMDATES
