###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.827, activation diff: 3.827, ratio: 0.561
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 37.357, activation diff: 32.440, ratio: 0.868
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 83.206, activation diff: 47.112, ratio: 0.566
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 225.474, activation diff: 142.787, ratio: 0.633
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 494.229, activation diff: 268.841, ratio: 0.544
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 923.940, activation diff: 429.711, ratio: 0.465
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 1492.594, activation diff: 568.654, ratio: 0.381
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 2140.235, activation diff: 647.641, ratio: 0.303
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 2802.149, activation diff: 661.914, ratio: 0.236
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 3432.812, activation diff: 630.663, ratio: 0.184
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 4006.920, activation diff: 574.108, ratio: 0.143
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 4513.742, activation diff: 506.822, ratio: 0.112
#   pulse 13: activated nodes: 11348, borderline nodes: 0, overall activation: 4951.689, activation diff: 437.948, ratio: 0.088
#   pulse 14: activated nodes: 11348, borderline nodes: 0, overall activation: 5324.361, activation diff: 372.672, ratio: 0.070
#   pulse 15: activated nodes: 11348, borderline nodes: 0, overall activation: 5637.936, activation diff: 313.575, ratio: 0.056

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 5637.9
#   number of spread. activ. pulses: 15
#   running time: 573

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96986544   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.93509483   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.92635876   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.90798694   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8623539   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.85902035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.85842276   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.85271585   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   9   0.852581   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8523965   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   11   0.85221857   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   12   0.8517749   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.8516204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   14   0.85144615   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.8514159   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.85140026   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.8506253   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   18   0.8503407   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   19   0.8502103   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   20   0.85001665   REFERENCES:SIMDATES
