###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.222, activation diff: 12.222, ratio: 1.964
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 168.122, activation diff: 174.344, ratio: 1.037
#   pulse 3: activated nodes: 7510, borderline nodes: 4657, overall activation: 75.584, activation diff: 243.705, ratio: 3.224
#   pulse 4: activated nodes: 9288, borderline nodes: 5805, overall activation: 2696.634, activation diff: 2772.218, ratio: 1.028
#   pulse 5: activated nodes: 10734, borderline nodes: 3497, overall activation: 1178.458, activation diff: 3875.092, ratio: 3.288
#   pulse 6: activated nodes: 11125, borderline nodes: 1042, overall activation: 4730.372, activation diff: 5886.364, ratio: 1.244
#   pulse 7: activated nodes: 11280, borderline nodes: 433, overall activation: 1824.963, activation diff: 6499.860, ratio: 3.562
#   pulse 8: activated nodes: 11332, borderline nodes: 112, overall activation: 4945.617, activation diff: 6628.760, ratio: 1.340
#   pulse 9: activated nodes: 11334, borderline nodes: 51, overall activation: 2818.483, activation diff: 5749.356, ratio: 2.040
#   pulse 10: activated nodes: 11337, borderline nodes: 29, overall activation: 5615.849, activation diff: 5119.331, ratio: 0.912
#   pulse 11: activated nodes: 11337, borderline nodes: 23, overall activation: 6296.268, activation diff: 1665.766, ratio: 0.265
#   pulse 12: activated nodes: 11337, borderline nodes: 23, overall activation: 6675.081, activation diff: 621.119, ratio: 0.093
#   pulse 13: activated nodes: 11337, borderline nodes: 23, overall activation: 6762.370, activation diff: 171.338, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 6762.4
#   number of spread. activ. pulses: 13
#   running time: 1480

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99961144   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99016035   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9885902   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97250754   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92558706   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9028915   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8999964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.8999921   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.899988   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   10   0.8999755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   11   0.89996994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.899969   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   13   0.8999666   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.8999649   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   15   0.8999631   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   16   0.8999629   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.8999606   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.8999583   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.8999566   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.8999528   REFERENCES:SIMDATES
