###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.307, activation diff: 15.307, ratio: 1.645
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 429.572, activation diff: 438.755, ratio: 1.021
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 268.471, activation diff: 693.594, ratio: 2.583
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3227.999, activation diff: 3472.965, ratio: 1.076
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1313.082, activation diff: 4237.273, ratio: 3.227
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4050.371, activation diff: 4734.040, ratio: 1.169
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 4108.868, activation diff: 2253.018, ratio: 0.548
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 5012.183, activation diff: 1369.962, ratio: 0.273
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 5203.536, activation diff: 311.778, ratio: 0.060
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 5253.936, activation diff: 82.650, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5253.9
#   number of spread. activ. pulses: 10
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996463   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9910871   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98977095   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97488   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9323403   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.91167265   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79998696   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.79993296   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.79988515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.79987806   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.7998645   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7998418   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.79981846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.79980814   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.79980296   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7997989   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   17   0.7997983   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.7997621   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.7997516   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.7997381   REFERENCES:SIMDATES
