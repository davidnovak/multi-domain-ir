###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.166, activation diff: 9.166, ratio: 1.487
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 62.892, activation diff: 63.915, ratio: 1.016
#   pulse 3: activated nodes: 7192, borderline nodes: 4816, overall activation: 32.463, activation diff: 59.783, ratio: 1.842
#   pulse 4: activated nodes: 8582, borderline nodes: 6014, overall activation: 789.645, activation diff: 774.406, ratio: 0.981
#   pulse 5: activated nodes: 9986, borderline nodes: 4448, overall activation: 867.896, activation diff: 333.974, ratio: 0.385
#   pulse 6: activated nodes: 10711, borderline nodes: 2949, overall activation: 2233.704, activation diff: 1376.984, ratio: 0.616
#   pulse 7: activated nodes: 11183, borderline nodes: 1638, overall activation: 3082.103, activation diff: 848.433, ratio: 0.275
#   pulse 8: activated nodes: 11225, borderline nodes: 798, overall activation: 3671.541, activation diff: 589.438, ratio: 0.161
#   pulse 9: activated nodes: 11284, borderline nodes: 642, overall activation: 3972.794, activation diff: 301.253, ratio: 0.076
#   pulse 10: activated nodes: 11290, borderline nodes: 610, overall activation: 4113.360, activation diff: 140.566, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 4113.4
#   number of spread. activ. pulses: 10
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99914515   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98769075   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98600423   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96898407   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92213094   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89547724   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74958944   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7492175   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.748891   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.74885464   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.7487674   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7487531   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.74867606   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.748661   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7485603   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.7485528   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.74841857   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   18   0.7483996   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.7483895   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.7482871   REFERENCES:SIMDATES
