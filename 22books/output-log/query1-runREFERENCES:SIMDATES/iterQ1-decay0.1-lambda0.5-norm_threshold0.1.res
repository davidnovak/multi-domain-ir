###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.111, activation diff: 6.111, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 22.230, activation diff: 19.848, ratio: 0.893
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 26.714, activation diff: 7.835, ratio: 0.293
#   pulse 4: activated nodes: 7315, borderline nodes: 5663, overall activation: 208.168, activation diff: 182.247, ratio: 0.875
#   pulse 5: activated nodes: 8540, borderline nodes: 4839, overall activation: 454.998, activation diff: 246.860, ratio: 0.543
#   pulse 6: activated nodes: 9992, borderline nodes: 5192, overall activation: 1254.223, activation diff: 799.226, ratio: 0.637
#   pulse 7: activated nodes: 10935, borderline nodes: 3073, overall activation: 2328.109, activation diff: 1073.886, ratio: 0.461
#   pulse 8: activated nodes: 11185, borderline nodes: 1324, overall activation: 3531.641, activation diff: 1203.531, ratio: 0.341
#   pulse 9: activated nodes: 11245, borderline nodes: 535, overall activation: 4556.357, activation diff: 1024.717, ratio: 0.225
#   pulse 10: activated nodes: 11307, borderline nodes: 237, overall activation: 5319.142, activation diff: 762.785, ratio: 0.143
#   pulse 11: activated nodes: 11326, borderline nodes: 137, overall activation: 5846.750, activation diff: 527.608, ratio: 0.090
#   pulse 12: activated nodes: 11331, borderline nodes: 90, overall activation: 6197.475, activation diff: 350.725, ratio: 0.057
#   pulse 13: activated nodes: 11332, borderline nodes: 65, overall activation: 6425.591, activation diff: 228.117, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6425.6
#   number of spread. activ. pulses: 13
#   running time: 533

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99683434   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98112977   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.97894895   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96370196   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9170066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   6   0.8971298   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89712214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   8   0.8964485   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   9   0.8962006   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.89597297   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.89590406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   12   0.8958436   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   13   0.8957349   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.89572215   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   15   0.89569354   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   16   0.89566255   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   17   0.8955788   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   18   0.8955586   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_136   19   0.8955504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   20   0.89553356   REFERENCES:SIMDATES
