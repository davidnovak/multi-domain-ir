###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.480, activation diff: 11.480, ratio: 1.354
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 276.777, activation diff: 275.196, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 381.923, activation diff: 235.940, ratio: 0.618
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2572.668, activation diff: 2191.110, ratio: 0.852
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4166.212, activation diff: 1593.543, ratio: 0.382
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5651.572, activation diff: 1485.361, ratio: 0.263
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 6438.661, activation diff: 787.089, ratio: 0.122
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 6800.838, activation diff: 362.176, ratio: 0.053
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 6959.737, activation diff: 158.899, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6959.7
#   number of spread. activ. pulses: 9
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994287   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99006623   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98897815   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97394663   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.931285   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9093158   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8997965   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.89970505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   9   0.89968693   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.89968455   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.89967626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   12   0.8996621   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.8996604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   14   0.89965665   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.8996562   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   16   0.8996487   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.8996479   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.89963686   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   19   0.89963233   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   20   0.8996234   REFERENCES:SIMDATES
