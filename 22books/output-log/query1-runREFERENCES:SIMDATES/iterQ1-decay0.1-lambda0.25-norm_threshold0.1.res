###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.166, activation diff: 9.166, ratio: 1.487
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 74.952, activation diff: 75.975, ratio: 1.014
#   pulse 3: activated nodes: 7192, borderline nodes: 4816, overall activation: 41.873, activation diff: 74.778, ratio: 1.786
#   pulse 4: activated nodes: 8709, borderline nodes: 6082, overall activation: 1122.111, activation diff: 1104.693, ratio: 0.984
#   pulse 5: activated nodes: 10196, borderline nodes: 4306, overall activation: 1382.392, activation diff: 593.681, ratio: 0.429
#   pulse 6: activated nodes: 10822, borderline nodes: 2365, overall activation: 3578.553, activation diff: 2217.240, ratio: 0.620
#   pulse 7: activated nodes: 11221, borderline nodes: 1125, overall activation: 5054.397, activation diff: 1475.927, ratio: 0.292
#   pulse 8: activated nodes: 11298, borderline nodes: 237, overall activation: 5972.349, activation diff: 917.952, ratio: 0.154
#   pulse 9: activated nodes: 11328, borderline nodes: 135, overall activation: 6425.656, activation diff: 453.307, ratio: 0.071
#   pulse 10: activated nodes: 11332, borderline nodes: 70, overall activation: 6635.684, activation diff: 210.028, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6635.7
#   number of spread. activ. pulses: 10
#   running time: 481

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99924815   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9885461   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9872161   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97084624   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9240164   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8996285   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.89949834   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   8   0.89944184   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.8994417   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.89941984   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.8994193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   12   0.8993791   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.8993416   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   14   0.8993298   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   15   0.8993042   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.8993027   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   17   0.8993008   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   18   0.8992938   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   19   0.89928377   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   20   0.8992778   REFERENCES:SIMDATES
