###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.656, activation diff: 4.656, ratio: 1.000
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 2.720, activation diff: 2.187, ratio: 0.804
#   pulse 3: activated nodes: 3957, borderline nodes: 3868, overall activation: 1.378, activation diff: 1.341, ratio: 0.973
#   pulse 4: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.689, activation diff: 0.689, ratio: 1.000
#   pulse 5: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.345, activation diff: 0.345, ratio: 1.000
#   pulse 6: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.172, activation diff: 0.172, ratio: 1.000
#   pulse 7: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.086, activation diff: 0.086, ratio: 1.000
#   pulse 8: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.043, activation diff: 0.043, ratio: 1.000
#   pulse 9: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.022, activation diff: 0.022, ratio: 1.000
#   pulse 10: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.011, activation diff: 0.011, ratio: 1.000
#   pulse 11: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.005, activation diff: 0.005, ratio: 1.000
#   pulse 12: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.003, activation diff: 0.003, ratio: 1.000
#   pulse 13: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 14: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 15: activated nodes: 3957, borderline nodes: 3868, overall activation: 0.000, activation diff: 0.000, ratio: 1.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3957
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 248

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   3.2369775E-5   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_239   2   3.226183E-5   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   3   3.2174845E-5   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   3.1108426E-5   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   3.0517578E-5   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   3.0517578E-5   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_657   7   5.185628E-7   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_241   8   4.880644E-7   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_18   9   4.880644E-7   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_147   10   4.880644E-7   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_119   11   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_134   12   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_136   13   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_137   14   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_164   15   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_151   16   4.880644E-7   REFERENCES:SIMDATES
1   Q1   graduateschoolof9192bran_62   17   4.880644E-7   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_281   18   4.880644E-7   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_0   19   4.880644E-7   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_14   20   4.880644E-7   REFERENCES:SIMDATES
