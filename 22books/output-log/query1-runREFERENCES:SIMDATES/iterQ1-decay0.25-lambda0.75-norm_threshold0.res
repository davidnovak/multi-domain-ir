###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.827, activation diff: 3.827, ratio: 0.561
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 32.306, activation diff: 27.389, ratio: 0.848
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 68.847, activation diff: 37.804, ratio: 0.549
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 166.513, activation diff: 98.228, ratio: 0.590
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 342.355, activation diff: 175.945, ratio: 0.514
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 607.570, activation diff: 265.216, ratio: 0.437
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 946.788, activation diff: 339.217, ratio: 0.358
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 1331.707, activation diff: 384.920, ratio: 0.289
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 1729.629, activation diff: 397.922, ratio: 0.230
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 2114.162, activation diff: 384.533, ratio: 0.182
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 2468.892, activation diff: 354.729, ratio: 0.144
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 2785.803, activation diff: 316.912, ratio: 0.114
#   pulse 13: activated nodes: 11328, borderline nodes: 416, overall activation: 3062.573, activation diff: 276.770, ratio: 0.090
#   pulse 14: activated nodes: 11328, borderline nodes: 416, overall activation: 3300.318, activation diff: 237.745, ratio: 0.072
#   pulse 15: activated nodes: 11328, borderline nodes: 416, overall activation: 3502.027, activation diff: 201.709, ratio: 0.058

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 3502.0
#   number of spread. activ. pulses: 15
#   running time: 564

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96767104   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.92873   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.91720414   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.89988685   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.85541487   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.79441226   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7117766   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.70935524   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7046923   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7043152   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.70394206   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.7004082   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.7000042   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   14   0.7000002   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.6993566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.69901896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.6988954   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.69875705   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.69838786   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.6981726   REFERENCES:SIMDATES
