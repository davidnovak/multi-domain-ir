###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.166, activation diff: 9.166, ratio: 1.487
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 66.912, activation diff: 67.935, ratio: 1.015
#   pulse 3: activated nodes: 7192, borderline nodes: 4816, overall activation: 35.397, activation diff: 64.577, ratio: 1.824
#   pulse 4: activated nodes: 8687, borderline nodes: 6093, overall activation: 890.948, activation diff: 874.909, ratio: 0.982
#   pulse 5: activated nodes: 10144, borderline nodes: 4453, overall activation: 1020.374, activation diff: 407.024, ratio: 0.399
#   pulse 6: activated nodes: 10777, borderline nodes: 2733, overall activation: 2638.437, activation diff: 1631.813, ratio: 0.618
#   pulse 7: activated nodes: 11201, borderline nodes: 1440, overall activation: 3674.516, activation diff: 1036.118, ratio: 0.282
#   pulse 8: activated nodes: 11272, borderline nodes: 509, overall activation: 4366.163, activation diff: 691.647, ratio: 0.158
#   pulse 9: activated nodes: 11301, borderline nodes: 305, overall activation: 4715.306, activation diff: 349.143, ratio: 0.074
#   pulse 10: activated nodes: 11318, borderline nodes: 196, overall activation: 4876.780, activation diff: 161.474, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4876.8
#   number of spread. activ. pulses: 10
#   running time: 483

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991858   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9880575   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9865178   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9697615   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9229369   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8969816   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79961085   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7993244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.79915655   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.7991356   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.7991078   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7991029   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.79909885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.7990935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   15   0.79908466   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.79903865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   17   0.7990271   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   18   0.7990144   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.79898435   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.79895145   REFERENCES:SIMDATES
