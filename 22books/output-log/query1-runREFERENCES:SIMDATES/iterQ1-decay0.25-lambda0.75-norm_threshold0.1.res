###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.055, activation diff: 3.055, ratio: 0.505
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 5.833, activation diff: 1.930, ratio: 0.331
#   pulse 3: activated nodes: 5501, borderline nodes: 5239, overall activation: 7.336, activation diff: 3.112, ratio: 0.424
#   pulse 4: activated nodes: 6100, borderline nodes: 5206, overall activation: 9.576, activation diff: 3.628, ratio: 0.379
#   pulse 5: activated nodes: 6259, borderline nodes: 5168, overall activation: 14.739, activation diff: 6.332, ratio: 0.430
#   pulse 6: activated nodes: 6508, borderline nodes: 5008, overall activation: 26.495, activation diff: 12.618, ratio: 0.476
#   pulse 7: activated nodes: 7338, borderline nodes: 5324, overall activation: 53.860, activation diff: 27.850, ratio: 0.517
#   pulse 8: activated nodes: 8050, borderline nodes: 5344, overall activation: 112.645, activation diff: 58.972, ratio: 0.524
#   pulse 9: activated nodes: 9075, borderline nodes: 5495, overall activation: 221.675, activation diff: 109.083, ratio: 0.492
#   pulse 10: activated nodes: 9756, borderline nodes: 5214, overall activation: 390.200, activation diff: 168.525, ratio: 0.432
#   pulse 11: activated nodes: 10403, borderline nodes: 4523, overall activation: 620.642, activation diff: 230.442, ratio: 0.371
#   pulse 12: activated nodes: 10784, borderline nodes: 3527, overall activation: 907.096, activation diff: 286.454, ratio: 0.316
#   pulse 13: activated nodes: 11008, borderline nodes: 2450, overall activation: 1235.100, activation diff: 328.004, ratio: 0.266
#   pulse 14: activated nodes: 11147, borderline nodes: 1671, overall activation: 1582.920, activation diff: 347.820, ratio: 0.220
#   pulse 15: activated nodes: 11199, borderline nodes: 1165, overall activation: 1928.579, activation diff: 345.659, ratio: 0.179

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11199
#   final overall activation: 1928.6
#   number of spread. activ. pulses: 15
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8495879   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.7357739   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.7085421   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.6939194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   5   0.6114218   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.60470265   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.5991101   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   8   0.59890294   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.5976794   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.5956511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.59432137   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   12   0.58975327   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   13   0.589702   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.58944297   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.58842283   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.58528626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   17   0.5820258   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   18   0.58201456   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.5816426   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   20   0.5811663   REFERENCES:SIMDATES
