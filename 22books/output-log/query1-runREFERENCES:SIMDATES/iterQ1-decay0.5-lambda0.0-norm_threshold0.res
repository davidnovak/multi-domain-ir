###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.307, activation diff: 15.307, ratio: 1.645
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 269.456, activation diff: 278.639, ratio: 1.034
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 49.852, activation diff: 316.893, ratio: 6.357
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1311.890, activation diff: 1356.059, ratio: 1.034
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 115.580, activation diff: 1366.584, ratio: 11.824
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1461.060, activation diff: 1490.394, ratio: 1.020
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 706.486, activation diff: 905.129, ratio: 1.281
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1522.322, activation diff: 852.944, ratio: 0.560
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1493.837, activation diff: 66.257, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1493.8
#   number of spread. activ. pulses: 9
#   running time: 507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988312   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.97701836   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.97396636   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9505738   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9104743   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84321815   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4979582   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49574572   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49407655   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49270955   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49028382   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.48936757   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.48655474   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.48467827   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.48189777   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.48166758   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.48121247   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.48115358   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.4811158   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   20   0.48047158   REFERENCES:SIMDATES
