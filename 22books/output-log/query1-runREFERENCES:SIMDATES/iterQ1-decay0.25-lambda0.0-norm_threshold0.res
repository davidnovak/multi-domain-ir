###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.307, activation diff: 15.307, ratio: 1.645
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 402.886, activation diff: 412.069, ratio: 1.023
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 222.881, activation diff: 621.692, ratio: 2.789
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2815.266, activation diff: 3019.307, ratio: 1.072
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1028.365, activation diff: 3607.089, ratio: 3.508
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 3484.437, activation diff: 4049.087, ratio: 1.162
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 3327.330, activation diff: 2002.289, ratio: 0.602
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 4216.246, activation diff: 1332.557, ratio: 0.316
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 4391.960, activation diff: 294.846, ratio: 0.067
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 4440.135, activation diff: 78.768, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4440.1
#   number of spread. activ. pulses: 10
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99964124   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9910798   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9896365   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9745469   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9320107   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.91133505   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7499737   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7498112   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7497923   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7497419   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7496415   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7496057   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.74953675   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.74952376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   15   0.74951416   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.74943525   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   17   0.7493975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.74937767   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.7493453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.74928004   REFERENCES:SIMDATES
