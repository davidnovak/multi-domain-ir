###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.111, activation diff: 6.111, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 20.232, activation diff: 17.849, ratio: 0.882
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 23.950, activation diff: 6.963, ratio: 0.291
#   pulse 4: activated nodes: 7305, borderline nodes: 5657, overall activation: 164.922, activation diff: 141.832, ratio: 0.860
#   pulse 5: activated nodes: 8468, borderline nodes: 4925, overall activation: 351.985, activation diff: 187.121, ratio: 0.532
#   pulse 6: activated nodes: 9806, borderline nodes: 5335, overall activation: 927.740, activation diff: 575.755, ratio: 0.621
#   pulse 7: activated nodes: 10783, borderline nodes: 3608, overall activation: 1678.946, activation diff: 751.207, ratio: 0.447
#   pulse 8: activated nodes: 11114, borderline nodes: 1818, overall activation: 2536.485, activation diff: 857.538, ratio: 0.338
#   pulse 9: activated nodes: 11227, borderline nodes: 896, overall activation: 3289.030, activation diff: 752.545, ratio: 0.229
#   pulse 10: activated nodes: 11248, borderline nodes: 505, overall activation: 3862.603, activation diff: 573.572, ratio: 0.148
#   pulse 11: activated nodes: 11303, borderline nodes: 330, overall activation: 4266.540, activation diff: 403.938, ratio: 0.095
#   pulse 12: activated nodes: 11311, borderline nodes: 239, overall activation: 4538.304, activation diff: 271.763, ratio: 0.060
#   pulse 13: activated nodes: 11321, borderline nodes: 178, overall activation: 4716.196, activation diff: 177.892, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 4716.2
#   number of spread. activ. pulses: 13
#   running time: 529

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99651676   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9794656   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.97675014   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9614383   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9148946   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88052416   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79700035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7965934   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7956175   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.7954134   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   11   0.7952622   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   12   0.7950255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.7949197   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.7948235   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.7946228   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   16   0.7944391   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7944269   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   18   0.79426175   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   19   0.79416716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.7941508   REFERENCES:SIMDATES
