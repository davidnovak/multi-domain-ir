###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.827, activation diff: 3.827, ratio: 0.561
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 23.887, activation diff: 18.970, ratio: 0.794
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 46.375, activation diff: 23.751, ratio: 0.512
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 92.398, activation diff: 46.672, ratio: 0.505
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 168.966, activation diff: 76.744, ratio: 0.454
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 277.163, activation diff: 108.207, ratio: 0.390
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 406.336, activation diff: 129.173, ratio: 0.318
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 544.150, activation diff: 137.814, ratio: 0.253
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 680.822, activation diff: 136.672, ratio: 0.201
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 809.726, activation diff: 128.904, ratio: 0.159
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 926.933, activation diff: 117.206, ratio: 0.126
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1030.591, activation diff: 103.658, ratio: 0.101
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1120.326, activation diff: 89.735, ratio: 0.080
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 1196.722, activation diff: 76.396, ratio: 0.064
#   pulse 15: activated nodes: 8935, borderline nodes: 3614, overall activation: 1260.908, activation diff: 64.186, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1260.9
#   number of spread. activ. pulses: 15
#   running time: 513

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9608429   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9073881   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.87893   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.8740365   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8343707   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.744604   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4611546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.45717764   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.45618546   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.454149   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4512059   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.45062414   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   13   0.447421   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.4467594   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   15   0.4426802   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.44223493   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.4418378   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.4415987   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.43901494   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.43867475   REFERENCES:SIMDATES
