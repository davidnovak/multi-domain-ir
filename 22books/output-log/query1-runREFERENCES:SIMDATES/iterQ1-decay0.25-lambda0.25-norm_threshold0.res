###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.480, activation diff: 11.480, ratio: 1.354
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 231.547, activation diff: 229.966, ratio: 0.993
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 279.554, activation diff: 157.340, ratio: 0.563
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1723.151, activation diff: 1444.012, ratio: 0.838
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2652.612, activation diff: 929.460, ratio: 0.350
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 3565.689, activation diff: 913.077, ratio: 0.256
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 4053.709, activation diff: 488.021, ratio: 0.120
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 4280.998, activation diff: 227.289, ratio: 0.053
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 4380.985, activation diff: 99.987, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4381.0
#   number of spread. activ. pulses: 9
#   running time: 472

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993954   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9898191   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9883133   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97267807   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.93004894   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90755373   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7497701   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7495216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7492772   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7492323   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.749153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7491511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.74906063   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7489588   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.74891114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   16   0.7489026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.74881196   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   18   0.74880075   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.74878395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   20   0.748724   REFERENCES:SIMDATES
