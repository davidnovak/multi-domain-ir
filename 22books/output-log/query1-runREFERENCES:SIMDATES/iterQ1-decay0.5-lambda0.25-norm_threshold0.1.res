###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.166, activation diff: 9.166, ratio: 1.487
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 42.793, activation diff: 43.816, ratio: 1.024
#   pulse 3: activated nodes: 7192, borderline nodes: 4816, overall activation: 20.241, activation diff: 38.327, ratio: 1.894
#   pulse 4: activated nodes: 7348, borderline nodes: 4957, overall activation: 391.640, activation diff: 381.220, ratio: 0.973
#   pulse 5: activated nodes: 8398, borderline nodes: 4037, overall activation: 331.373, activation diff: 126.807, ratio: 0.383
#   pulse 6: activated nodes: 8513, borderline nodes: 4101, overall activation: 872.002, activation diff: 541.465, ratio: 0.621
#   pulse 7: activated nodes: 8914, borderline nodes: 3631, overall activation: 1118.237, activation diff: 246.317, ratio: 0.220
#   pulse 8: activated nodes: 8914, borderline nodes: 3631, overall activation: 1305.970, activation diff: 187.733, ratio: 0.144
#   pulse 9: activated nodes: 8929, borderline nodes: 3611, overall activation: 1392.001, activation diff: 86.030, ratio: 0.062
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1429.413, activation diff: 37.412, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1429.4
#   number of spread. activ. pulses: 10
#   running time: 438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99866426   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9805001   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9795344   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9596674   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91282547   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8711654   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49806878   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49582815   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4941443   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49247384   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49110228   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.48791015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.48601612   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.48419768   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.48412043   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.482782   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.479904   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.47972324   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.47939125   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.4791219   REFERENCES:SIMDATES
