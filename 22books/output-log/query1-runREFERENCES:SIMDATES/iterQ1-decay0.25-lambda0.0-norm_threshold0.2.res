###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.312, activation diff: 9.312, ratio: 2.811
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 21.585, activation diff: 24.897, ratio: 1.153
#   pulse 3: activated nodes: 4656, borderline nodes: 3767, overall activation: 4.209, activation diff: 25.794, ratio: 6.128
#   pulse 4: activated nodes: 6154, borderline nodes: 5254, overall activation: 422.797, activation diff: 427.006, ratio: 1.010
#   pulse 5: activated nodes: 7784, borderline nodes: 4673, overall activation: 104.217, activation diff: 527.013, ratio: 5.057
#   pulse 6: activated nodes: 9519, borderline nodes: 5651, overall activation: 2073.188, activation diff: 2177.405, ratio: 1.050
#   pulse 7: activated nodes: 10794, borderline nodes: 3521, overall activation: 593.979, activation diff: 2667.167, ratio: 4.490
#   pulse 8: activated nodes: 11070, borderline nodes: 1803, overall activation: 2834.308, activation diff: 3427.402, ratio: 1.209
#   pulse 9: activated nodes: 11215, borderline nodes: 1194, overall activation: 779.625, activation diff: 3613.048, ratio: 4.634
#   pulse 10: activated nodes: 11226, borderline nodes: 1062, overall activation: 2900.907, activation diff: 3679.345, ratio: 1.268
#   pulse 11: activated nodes: 11240, borderline nodes: 1029, overall activation: 797.736, activation diff: 3697.455, ratio: 4.635
#   pulse 12: activated nodes: 11270, borderline nodes: 1044, overall activation: 2907.071, activation diff: 3703.610, ratio: 1.274
#   pulse 13: activated nodes: 11271, borderline nodes: 1038, overall activation: 799.658, activation diff: 3705.532, ratio: 4.634
#   pulse 14: activated nodes: 11274, borderline nodes: 1039, overall activation: 2907.796, activation diff: 3706.256, ratio: 1.275
#   pulse 15: activated nodes: 11275, borderline nodes: 1039, overall activation: 799.919, activation diff: 3706.517, ratio: 4.634

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11275
#   final overall activation: 799.9
#   number of spread. activ. pulses: 15
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES:SIMDATES
