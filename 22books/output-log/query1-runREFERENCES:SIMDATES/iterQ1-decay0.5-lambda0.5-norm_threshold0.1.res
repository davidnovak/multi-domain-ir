###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.111, activation diff: 6.111, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 14.238, activation diff: 11.855, ratio: 0.833
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 15.820, activation diff: 4.596, ratio: 0.291
#   pulse 4: activated nodes: 6661, borderline nodes: 5030, overall activation: 65.481, activation diff: 50.746, ratio: 0.775
#   pulse 5: activated nodes: 7577, borderline nodes: 4686, overall activation: 128.351, activation diff: 63.064, ratio: 0.491
#   pulse 6: activated nodes: 7918, borderline nodes: 4592, overall activation: 330.164, activation diff: 201.829, ratio: 0.611
#   pulse 7: activated nodes: 8515, borderline nodes: 4121, overall activation: 562.465, activation diff: 232.301, ratio: 0.413
#   pulse 8: activated nodes: 8697, borderline nodes: 3879, overall activation: 797.742, activation diff: 235.278, ratio: 0.295
#   pulse 9: activated nodes: 8883, borderline nodes: 3668, overall activation: 997.181, activation diff: 199.439, ratio: 0.200
#   pulse 10: activated nodes: 8918, borderline nodes: 3622, overall activation: 1150.546, activation diff: 153.365, ratio: 0.133
#   pulse 11: activated nodes: 8929, borderline nodes: 3613, overall activation: 1259.735, activation diff: 109.189, ratio: 0.087
#   pulse 12: activated nodes: 8929, borderline nodes: 3611, overall activation: 1333.005, activation diff: 73.270, ratio: 0.055
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1380.091, activation diff: 47.086, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1380.1
#   number of spread. activ. pulses: 13
#   running time: 465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9941202   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.96587074   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9521922   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9445169   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8993574   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8358893   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4936284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.490028   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.4889174   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.48866516   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48629013   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.484861   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.48160523   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.48042518   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.4760226   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.47557533   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.475561   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.47529078   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.4729193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.4728644   REFERENCES:SIMDATES
