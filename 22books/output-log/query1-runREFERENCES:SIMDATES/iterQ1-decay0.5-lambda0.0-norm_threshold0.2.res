###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.312, activation diff: 9.312, ratio: 2.811
#   pulse 2: activated nodes: 3956, borderline nodes: 3901, overall activation: 14.617, activation diff: 17.929, ratio: 1.227
#   pulse 3: activated nodes: 4656, borderline nodes: 3767, overall activation: 2.645, activation diff: 17.262, ratio: 6.526
#   pulse 4: activated nodes: 5340, borderline nodes: 4448, overall activation: 159.725, activation diff: 162.370, ratio: 1.017
#   pulse 5: activated nodes: 6605, borderline nodes: 4083, overall activation: 21.283, activation diff: 181.008, ratio: 8.505
#   pulse 6: activated nodes: 7492, borderline nodes: 4928, overall activation: 883.536, activation diff: 904.820, ratio: 1.024
#   pulse 7: activated nodes: 8542, borderline nodes: 3944, overall activation: 71.474, activation diff: 955.010, ratio: 13.362
#   pulse 8: activated nodes: 8607, borderline nodes: 3974, overall activation: 1210.233, activation diff: 1281.690, ratio: 1.059
#   pulse 9: activated nodes: 8890, borderline nodes: 3658, overall activation: 85.825, activation diff: 1296.041, ratio: 15.101
#   pulse 10: activated nodes: 8890, borderline nodes: 3658, overall activation: 1256.015, activation diff: 1341.752, ratio: 1.068
#   pulse 11: activated nodes: 8894, borderline nodes: 3655, overall activation: 87.127, activation diff: 1343.054, ratio: 15.415
#   pulse 12: activated nodes: 8894, borderline nodes: 3655, overall activation: 1258.539, activation diff: 1345.572, ratio: 1.069
#   pulse 13: activated nodes: 8894, borderline nodes: 3655, overall activation: 87.232, activation diff: 1345.677, ratio: 15.426
#   pulse 14: activated nodes: 8894, borderline nodes: 3655, overall activation: 1258.715, activation diff: 1345.853, ratio: 1.069
#   pulse 15: activated nodes: 8896, borderline nodes: 3655, overall activation: 87.242, activation diff: 1345.863, ratio: 15.427

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8896
#   final overall activation: 87.2
#   number of spread. activ. pulses: 15
#   running time: 496

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   4   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_214   5   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_350   6   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_181   7   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_216   8   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   9   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   10   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_215   11   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_219   12   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_180   13   0.0   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_351   14   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_206   15   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_205   16   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES:SIMDATES
