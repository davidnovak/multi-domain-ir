###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.846, activation diff: 3.846, ratio: 0.562
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 34.047, activation diff: 29.109, ratio: 0.855
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 73.760, activation diff: 40.973, ratio: 0.555
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 185.056, activation diff: 111.841, ratio: 0.604
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 388.534, activation diff: 203.573, ratio: 0.524
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 700.758, activation diff: 312.225, ratio: 0.446
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 1104.984, activation diff: 404.225, ratio: 0.366
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 1565.409, activation diff: 460.425, ratio: 0.294
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 2040.888, activation diff: 475.479, ratio: 0.233
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 2499.437, activation diff: 458.548, ratio: 0.183
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 2921.635, activation diff: 422.198, ratio: 0.145
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 3298.152, activation diff: 376.517, ratio: 0.114
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 3626.411, activation diff: 328.259, ratio: 0.091
#   pulse 14: activated nodes: 11335, borderline nodes: 30, overall activation: 3907.907, activation diff: 281.496, ratio: 0.072
#   pulse 15: activated nodes: 11335, borderline nodes: 30, overall activation: 4146.339, activation diff: 238.432, ratio: 0.058

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 4146.3
#   number of spread. activ. pulses: 15
#   running time: 530

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96857107   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.93132335   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9211578   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.90305984   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8581369   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8000656   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7603555   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.75905967   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7534349   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   10   0.75242925   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.75210744   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.75158596   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.75157374   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   14   0.7508927   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.75050235   REFERENCES
1   Q1   essentialsinmod01howegoog_14   16   0.7501794   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.75016665   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.74974   REFERENCES
1   Q1   europesincenapol00leveuoft_353   19   0.7486137   REFERENCES
1   Q1   essentialsinmod01howegoog_471   20   0.74851966   REFERENCES
