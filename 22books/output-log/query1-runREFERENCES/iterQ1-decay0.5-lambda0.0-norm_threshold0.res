###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.385, activation diff: 15.385, ratio: 1.639
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 269.715, activation diff: 279.100, ratio: 1.035
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 39.801, activation diff: 309.516, ratio: 7.777
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1279.896, activation diff: 1319.697, ratio: 1.031
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 58.378, activation diff: 1338.274, ratio: 22.924
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1373.760, activation diff: 1432.137, ratio: 1.042
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 59.363, activation diff: 1433.122, ratio: 24.142
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1375.860, activation diff: 1435.223, ratio: 1.043
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 59.424, activation diff: 1435.284, ratio: 24.153
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1375.969, activation diff: 1435.393, ratio: 1.043
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 59.430, activation diff: 1435.399, ratio: 24.153
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1375.980, activation diff: 1435.410, ratio: 1.043
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 59.431, activation diff: 1435.411, ratio: 24.153
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1375.981, activation diff: 1435.412, ratio: 1.043
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 59.431, activation diff: 1435.412, ratio: 24.153

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 59.4
#   number of spread. activ. pulses: 15
#   running time: 594

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
