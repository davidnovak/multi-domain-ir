###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.390, activation diff: 9.390, ratio: 2.770
#   pulse 2: activated nodes: 3890, borderline nodes: 3835, overall activation: 23.063, activation diff: 26.453, ratio: 1.147
#   pulse 3: activated nodes: 4650, borderline nodes: 3759, overall activation: 4.571, activation diff: 27.635, ratio: 6.045
#   pulse 4: activated nodes: 6150, borderline nodes: 5247, overall activation: 489.546, activation diff: 494.118, ratio: 1.009
#   pulse 5: activated nodes: 7818, borderline nodes: 4616, overall activation: 139.153, activation diff: 628.699, ratio: 4.518
#   pulse 6: activated nodes: 9593, borderline nodes: 5406, overall activation: 2465.616, activation diff: 2604.768, ratio: 1.056
#   pulse 7: activated nodes: 10901, borderline nodes: 3243, overall activation: 847.840, activation diff: 3313.456, ratio: 3.908
#   pulse 8: activated nodes: 11127, borderline nodes: 1228, overall activation: 3330.923, activation diff: 4178.763, ratio: 1.255
#   pulse 9: activated nodes: 11240, borderline nodes: 702, overall activation: 1089.877, activation diff: 4420.799, ratio: 4.056
#   pulse 10: activated nodes: 11279, borderline nodes: 331, overall activation: 3407.423, activation diff: 4497.300, ratio: 1.320
#   pulse 11: activated nodes: 11291, borderline nodes: 305, overall activation: 1113.782, activation diff: 4521.205, ratio: 4.059
#   pulse 12: activated nodes: 11298, borderline nodes: 275, overall activation: 3415.537, activation diff: 4529.319, ratio: 1.326
#   pulse 13: activated nodes: 11304, borderline nodes: 277, overall activation: 1117.218, activation diff: 4532.755, ratio: 4.057
#   pulse 14: activated nodes: 11304, borderline nodes: 277, overall activation: 3416.939, activation diff: 4534.157, ratio: 1.327
#   pulse 15: activated nodes: 11304, borderline nodes: 277, overall activation: 1118.047, activation diff: 4534.986, ratio: 4.056

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 1118.0
#   number of spread. activ. pulses: 15
#   running time: 541

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
