###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.225, activation diff: 9.225, ratio: 1.482
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 75.176, activation diff: 76.214, ratio: 1.014
#   pulse 3: activated nodes: 7198, borderline nodes: 4816, overall activation: 42.223, activation diff: 75.194, ratio: 1.781
#   pulse 4: activated nodes: 8712, borderline nodes: 6071, overall activation: 1125.539, activation diff: 1108.190, ratio: 0.985
#   pulse 5: activated nodes: 10201, borderline nodes: 4290, overall activation: 1414.733, activation diff: 623.690, ratio: 0.441
#   pulse 6: activated nodes: 10808, borderline nodes: 2250, overall activation: 3603.353, activation diff: 2212.315, ratio: 0.614
#   pulse 7: activated nodes: 11224, borderline nodes: 1037, overall activation: 5120.113, activation diff: 1516.842, ratio: 0.296
#   pulse 8: activated nodes: 11294, borderline nodes: 217, overall activation: 6051.834, activation diff: 931.721, ratio: 0.154
#   pulse 9: activated nodes: 11324, borderline nodes: 117, overall activation: 6511.232, activation diff: 459.398, ratio: 0.071
#   pulse 10: activated nodes: 11331, borderline nodes: 59, overall activation: 6722.775, activation diff: 211.543, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 6722.8
#   number of spread. activ. pulses: 10
#   running time: 473

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992502   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98855007   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9872244   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97086084   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9240188   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.89962876   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8994837   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   8   0.89944303   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.8994421   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.8994112   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.899393   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.899376   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   13   0.89933795   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   14   0.8993157   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.89929193   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   16   0.8992908   REFERENCES
1   Q1   encyclopediaame28unkngoog_127   17   0.8992895   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.8992816   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   19   0.8992608   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   20   0.8992544   REFERENCES
