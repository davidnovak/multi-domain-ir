###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.692, activation diff: 7.692, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 128.029, activation diff: 121.988, ratio: 0.953
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 247.338, activation diff: 120.168, ratio: 0.486
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1125.030, activation diff: 877.738, ratio: 0.780
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2281.280, activation diff: 1156.250, ratio: 0.507
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3637.754, activation diff: 1356.474, ratio: 0.373
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4777.258, activation diff: 1139.504, ratio: 0.239
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5606.449, activation diff: 829.191, ratio: 0.148
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 6166.846, activation diff: 560.398, ratio: 0.091
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 6530.916, activation diff: 364.070, ratio: 0.056
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 6762.290, activation diff: 231.374, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6762.3
#   number of spread. activ. pulses: 11
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99635714   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98212445   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9818251   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96487784   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9215447   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8962701   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8960326   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.89534366   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   9   0.895245   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.89520997   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   11   0.89517534   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.89511687   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   13   0.89493847   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   14   0.89480937   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   15   0.894782   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   16   0.894704   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.89469707   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.8946682   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.8946296   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   20   0.89452934   REFERENCES
