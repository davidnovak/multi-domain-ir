###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.692, activation diff: 7.692, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 107.840, activation diff: 101.799, ratio: 0.944
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 193.347, activation diff: 86.371, ratio: 0.447
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 767.574, activation diff: 574.279, ratio: 0.748
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1471.906, activation diff: 704.333, ratio: 0.479
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2270.648, activation diff: 798.741, ratio: 0.352
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2949.667, activation diff: 679.020, ratio: 0.230
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3453.864, activation diff: 504.197, ratio: 0.146
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.304, activation diff: 347.440, ratio: 0.091
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4030.649, activation diff: 229.345, ratio: 0.057
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 4178.178, activation diff: 147.530, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4178.2
#   number of spread. activ. pulses: 11
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9961372   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98062485   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9804164   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9624549   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91923845   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88868797   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7462379   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74510753   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74448955   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.7438   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7437154   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.74352044   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.74346626   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   14   0.7434593   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.74323225   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   16   0.7429212   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   17   0.7426801   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7426536   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   19   0.7426309   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   20   0.7425585   REFERENCES
