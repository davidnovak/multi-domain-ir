###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.538, activation diff: 11.538, ratio: 1.351
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 246.906, activation diff: 245.391, ratio: 0.994
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 314.873, activation diff: 186.345, ratio: 0.592
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1969.615, activation diff: 1655.278, ratio: 0.840
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3108.123, activation diff: 1138.508, ratio: 0.366
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4194.806, activation diff: 1086.683, ratio: 0.259
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 4783.033, activation diff: 588.227, ratio: 0.123
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 5056.049, activation diff: 273.016, ratio: 0.054
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 5175.880, activation diff: 119.831, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5175.9
#   number of spread. activ. pulses: 9
#   running time: 443

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99940956   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9899027   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9885866   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9731933   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.93055236   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9082794   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79976   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.7994292   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7994107   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7993512   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7993461   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.7992929   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   13   0.79929066   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   14   0.79925966   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   15   0.79925776   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.799228   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   17   0.7991918   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   18   0.7991877   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.79918456   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   20   0.7991842   REFERENCES
