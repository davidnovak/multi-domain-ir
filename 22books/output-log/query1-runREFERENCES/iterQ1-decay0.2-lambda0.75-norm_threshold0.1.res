###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.075, activation diff: 3.075, ratio: 0.506
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 5.881, activation diff: 1.955, ratio: 0.333
#   pulse 3: activated nodes: 5479, borderline nodes: 5217, overall activation: 7.542, activation diff: 3.265, ratio: 0.433
#   pulse 4: activated nodes: 6095, borderline nodes: 5201, overall activation: 10.026, activation diff: 3.862, ratio: 0.385
#   pulse 5: activated nodes: 6258, borderline nodes: 5163, overall activation: 15.984, activation diff: 7.115, ratio: 0.445
#   pulse 6: activated nodes: 6540, borderline nodes: 5003, overall activation: 29.829, activation diff: 14.680, ratio: 0.492
#   pulse 7: activated nodes: 7421, borderline nodes: 5314, overall activation: 62.737, activation diff: 33.352, ratio: 0.532
#   pulse 8: activated nodes: 8148, borderline nodes: 5350, overall activation: 133.801, activation diff: 71.232, ratio: 0.532
#   pulse 9: activated nodes: 9192, borderline nodes: 5424, overall activation: 264.385, activation diff: 130.626, ratio: 0.494
#   pulse 10: activated nodes: 9895, borderline nodes: 5055, overall activation: 467.456, activation diff: 203.071, ratio: 0.434
#   pulse 11: activated nodes: 10573, borderline nodes: 4091, overall activation: 748.190, activation diff: 280.734, ratio: 0.375
#   pulse 12: activated nodes: 10894, borderline nodes: 2915, overall activation: 1100.338, activation diff: 352.147, ratio: 0.320
#   pulse 13: activated nodes: 11092, borderline nodes: 1891, overall activation: 1503.020, activation diff: 402.682, ratio: 0.268
#   pulse 14: activated nodes: 11182, borderline nodes: 1238, overall activation: 1926.602, activation diff: 423.582, ratio: 0.220
#   pulse 15: activated nodes: 11223, borderline nodes: 841, overall activation: 2343.651, activation diff: 417.049, ratio: 0.178

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11223
#   final overall activation: 2343.7
#   number of spread. activ. pulses: 15
#   running time: 516

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.85933214   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.7523371   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.7247292   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.7064672   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   5   0.66453546   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.6564432   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.6491326   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   8   0.64899325   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.6484476   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.64697546   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   11   0.6459081   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   12   0.64351976   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.64309376   REFERENCES
1   Q1   essentialsinmod01howegoog_14   14   0.6416153   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.63571715   REFERENCES
1   Q1   essentialsinmod01howegoog_471   16   0.635668   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   17   0.63551354   REFERENCES
1   Q1   politicalsketche00retsrich_154   18   0.63318783   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.6330815   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.6311789   REFERENCES
