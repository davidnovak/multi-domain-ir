###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.538, activation diff: 11.538, ratio: 1.351
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 277.094, activation diff: 275.579, ratio: 0.995
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 388.691, activation diff: 244.617, ratio: 0.629
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2571.580, activation diff: 2183.372, ratio: 0.849
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4200.458, activation diff: 1628.878, ratio: 0.388
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5697.903, activation diff: 1497.445, ratio: 0.263
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 6496.740, activation diff: 798.837, ratio: 0.123
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 6862.210, activation diff: 365.470, ratio: 0.053
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 7021.390, activation diff: 159.180, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7021.4
#   number of spread. activ. pulses: 9
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99942905   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99004793   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9889769   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9739493   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.931279   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90928173   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8997916   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   8   0.89967906   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8996719   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.89967066   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   11   0.8996667   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.8996486   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.89964765   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   14   0.8996474   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   15   0.8996446   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   16   0.89962554   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.8996203   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   18   0.8996183   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   19   0.89960766   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   20   0.899604   REFERENCES
