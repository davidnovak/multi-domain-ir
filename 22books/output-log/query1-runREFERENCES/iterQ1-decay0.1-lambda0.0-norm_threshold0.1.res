###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.300, activation diff: 12.300, ratio: 1.952
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 168.581, activation diff: 174.882, ratio: 1.037
#   pulse 3: activated nodes: 7515, borderline nodes: 4653, overall activation: 77.414, activation diff: 245.995, ratio: 3.178
#   pulse 4: activated nodes: 9286, borderline nodes: 5727, overall activation: 2703.061, activation diff: 2780.475, ratio: 1.029
#   pulse 5: activated nodes: 10740, borderline nodes: 3428, overall activation: 1263.201, activation diff: 3966.261, ratio: 3.140
#   pulse 6: activated nodes: 11118, borderline nodes: 995, overall activation: 4715.349, activation diff: 5978.550, ratio: 1.268
#   pulse 7: activated nodes: 11278, borderline nodes: 393, overall activation: 1920.263, activation diff: 6635.612, ratio: 3.456
#   pulse 8: activated nodes: 11328, borderline nodes: 100, overall activation: 4883.677, activation diff: 6803.939, ratio: 1.393
#   pulse 9: activated nodes: 11333, borderline nodes: 41, overall activation: 1979.159, activation diff: 6862.836, ratio: 3.468
#   pulse 10: activated nodes: 11334, borderline nodes: 19, overall activation: 4901.281, activation diff: 6880.440, ratio: 1.404
#   pulse 11: activated nodes: 11334, borderline nodes: 16, overall activation: 1986.189, activation diff: 6887.470, ratio: 3.468
#   pulse 12: activated nodes: 11334, borderline nodes: 16, overall activation: 4903.816, activation diff: 6890.005, ratio: 1.405
#   pulse 13: activated nodes: 11334, borderline nodes: 16, overall activation: 1987.328, activation diff: 6891.145, ratio: 3.468
#   pulse 14: activated nodes: 11334, borderline nodes: 15, overall activation: 4904.247, activation diff: 6891.575, ratio: 1.405
#   pulse 15: activated nodes: 11334, borderline nodes: 15, overall activation: 1987.562, activation diff: 6891.809, ratio: 3.467

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 1987.6
#   number of spread. activ. pulses: 15
#   running time: 1454

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
