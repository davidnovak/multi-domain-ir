###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.846, activation diff: 3.846, ratio: 0.562
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 23.933, activation diff: 18.995, ratio: 0.794
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 46.283, activation diff: 23.611, ratio: 0.510
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 91.827, activation diff: 46.190, ratio: 0.503
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 167.266, activation diff: 75.614, ratio: 0.452
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 273.189, activation diff: 105.933, ratio: 0.388
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 398.501, activation diff: 125.313, ratio: 0.314
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 530.662, activation diff: 132.160, ratio: 0.249
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 660.046, activation diff: 129.384, ratio: 0.196
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 780.506, activation diff: 120.460, ratio: 0.154
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 888.730, activation diff: 108.224, ratio: 0.122
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 983.441, activation diff: 94.711, ratio: 0.096
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 1064.694, activation diff: 81.252, ratio: 0.076
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1133.334, activation diff: 68.640, ratio: 0.061
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 1190.619, activation diff: 57.286, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1190.6
#   number of spread. activ. pulses: 15
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9608713   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9069548   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.8767623   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.8735964   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8341094   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.7417248   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.45451534   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.45412758   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.4531939   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.4510725   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.44823694   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.44736302   REFERENCES
1   Q1   politicalsketche00retsrich_159   13   0.4416447   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.44156456   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   15   0.4407407   REFERENCES
1   Q1   politicalsketche00retsrich_153   16   0.4390156   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.43863815   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.43804964   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.43606585   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.43343621   REFERENCES
