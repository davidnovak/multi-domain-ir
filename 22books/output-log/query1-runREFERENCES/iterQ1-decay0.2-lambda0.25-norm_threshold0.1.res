###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.225, activation diff: 9.225, ratio: 1.482
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 67.114, activation diff: 68.152, ratio: 1.015
#   pulse 3: activated nodes: 7198, borderline nodes: 4816, overall activation: 35.557, activation diff: 64.794, ratio: 1.822
#   pulse 4: activated nodes: 8692, borderline nodes: 6081, overall activation: 892.655, activation diff: 876.599, ratio: 0.982
#   pulse 5: activated nodes: 10154, borderline nodes: 4447, overall activation: 1035.254, activation diff: 420.106, ratio: 0.406
#   pulse 6: activated nodes: 10768, borderline nodes: 2599, overall activation: 2634.560, activation diff: 1615.139, ratio: 0.613
#   pulse 7: activated nodes: 11199, borderline nodes: 1314, overall activation: 3688.825, activation diff: 1054.306, ratio: 0.286
#   pulse 8: activated nodes: 11240, borderline nodes: 451, overall activation: 4387.742, activation diff: 698.916, ratio: 0.159
#   pulse 9: activated nodes: 11297, borderline nodes: 280, overall activation: 4742.279, activation diff: 354.537, ratio: 0.075
#   pulse 10: activated nodes: 11314, borderline nodes: 175, overall activation: 4906.253, activation diff: 163.974, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11314
#   final overall activation: 4906.3
#   number of spread. activ. pulses: 10
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991878   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98805606   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9865258   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9697728   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.922922   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8968948   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7995876   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.7991049   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.7990953   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   10   0.7990762   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.7990383   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.79899323   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.79898536   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.79897285   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   15   0.7989652   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   16   0.7989098   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.79886365   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   18   0.7988561   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   19   0.79882425   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.7988241   REFERENCES
