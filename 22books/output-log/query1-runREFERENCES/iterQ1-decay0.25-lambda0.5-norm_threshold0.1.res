###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.150, activation diff: 6.150, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 19.316, activation diff: 16.903, ratio: 0.875
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 22.662, activation diff: 6.564, ratio: 0.290
#   pulse 4: activated nodes: 7289, borderline nodes: 5642, overall activation: 145.703, activation diff: 123.942, ratio: 0.851
#   pulse 5: activated nodes: 8434, borderline nodes: 5010, overall activation: 306.867, activation diff: 161.237, ratio: 0.525
#   pulse 6: activated nodes: 9702, borderline nodes: 5323, overall activation: 793.197, activation diff: 486.329, ratio: 0.613
#   pulse 7: activated nodes: 10684, borderline nodes: 3744, overall activation: 1413.842, activation diff: 620.645, ratio: 0.439
#   pulse 8: activated nodes: 11046, borderline nodes: 1978, overall activation: 2116.998, activation diff: 703.156, ratio: 0.332
#   pulse 9: activated nodes: 11207, borderline nodes: 1078, overall activation: 2744.203, activation diff: 627.205, ratio: 0.229
#   pulse 10: activated nodes: 11232, borderline nodes: 699, overall activation: 3230.488, activation diff: 486.286, ratio: 0.151
#   pulse 11: activated nodes: 11244, borderline nodes: 581, overall activation: 3577.496, activation diff: 347.007, ratio: 0.097
#   pulse 12: activated nodes: 11282, borderline nodes: 582, overall activation: 3813.310, activation diff: 235.814, ratio: 0.062
#   pulse 13: activated nodes: 11284, borderline nodes: 568, overall activation: 3968.860, activation diff: 155.551, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11284
#   final overall activation: 3968.9
#   number of spread. activ. pulses: 13
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99632895   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97838616   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9753126   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95997536   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9134873   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8771244   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.74672437   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7457826   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74444866   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.7441039   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.7439536   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.7438631   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.7437642   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.74350095   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   15   0.7431841   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   16   0.7426302   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7426137   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   18   0.74229604   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   19   0.7422608   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.74220055   REFERENCES
