###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.846, activation diff: 3.846, ratio: 0.562
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 37.419, activation diff: 32.480, ratio: 0.868
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 83.564, activation diff: 47.406, ratio: 0.567
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 226.345, activation diff: 143.297, ratio: 0.633
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 496.365, activation diff: 270.106, ratio: 0.544
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 928.273, activation diff: 431.908, ratio: 0.465
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 1500.255, activation diff: 571.982, ratio: 0.381
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 2152.559, activation diff: 652.303, ratio: 0.303
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 2820.363, activation diff: 667.804, ratio: 0.237
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 3457.547, activation diff: 637.185, ratio: 0.184
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 4038.152, activation diff: 580.604, ratio: 0.144
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 4550.980, activation diff: 512.828, ratio: 0.113
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 4994.198, activation diff: 443.218, ratio: 0.089
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 5371.319, activation diff: 377.121, ratio: 0.070
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 5688.533, activation diff: 317.215, ratio: 0.056

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5688.5
#   number of spread. activ. pulses: 15
#   running time: 533

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9699395   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9352729   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.92678773   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9081182   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8624861   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8589129   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8583737   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   8   0.8525304   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8522866   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.85227615   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.8522301   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.8518659   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.8516084   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.8514571   REFERENCES
1   Q1   essentialsinmod01howegoog_14   15   0.85127443   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   16   0.8510471   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.8506399   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   18   0.85024184   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   19   0.85021746   REFERENCES
1   Q1   essentialsinmod01howegoog_471   20   0.84986144   REFERENCES
