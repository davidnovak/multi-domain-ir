###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.225, activation diff: 9.225, ratio: 1.482
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 42.928, activation diff: 43.966, ratio: 1.024
#   pulse 3: activated nodes: 7198, borderline nodes: 4816, overall activation: 20.242, activation diff: 38.338, ratio: 1.894
#   pulse 4: activated nodes: 7198, borderline nodes: 4816, overall activation: 391.874, activation diff: 381.388, ratio: 0.973
#   pulse 5: activated nodes: 8243, borderline nodes: 3893, overall activation: 324.781, activation diff: 119.330, ratio: 0.367
#   pulse 6: activated nodes: 8243, borderline nodes: 3893, overall activation: 843.434, activation diff: 519.503, ratio: 0.616
#   pulse 7: activated nodes: 8636, borderline nodes: 3431, overall activation: 1072.497, activation diff: 229.171, ratio: 0.214
#   pulse 8: activated nodes: 8636, borderline nodes: 3431, overall activation: 1228.980, activation diff: 156.483, ratio: 0.127
#   pulse 9: activated nodes: 8654, borderline nodes: 3403, overall activation: 1297.490, activation diff: 68.510, ratio: 0.053
#   pulse 10: activated nodes: 8660, borderline nodes: 3406, overall activation: 1326.177, activation diff: 28.687, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 1326.2
#   number of spread. activ. pulses: 10
#   running time: 407

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9986457   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9793111   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.97910464   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9592788   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9124455   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8681457   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.49131006   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49122787   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.4910567   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.49079627   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.4879109   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.48410368   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.47990224   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.47984844   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.47967467   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.47720206   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.47478428   REFERENCES
1   Q1   essentialsinmod01howegoog_446   18   0.4744662   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.4740955   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.474073   REFERENCES
