###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.390, activation diff: 9.390, ratio: 2.770
#   pulse 2: activated nodes: 3890, borderline nodes: 3835, overall activation: 25.859, activation diff: 29.249, ratio: 1.131
#   pulse 3: activated nodes: 4650, borderline nodes: 3759, overall activation: 5.269, activation diff: 31.128, ratio: 5.907
#   pulse 4: activated nodes: 6169, borderline nodes: 5264, overall activation: 629.283, activation diff: 634.553, ratio: 1.008
#   pulse 5: activated nodes: 7908, borderline nodes: 4508, overall activation: 232.395, activation diff: 861.678, ratio: 3.708
#   pulse 6: activated nodes: 9728, borderline nodes: 5072, overall activation: 3434.554, activation diff: 3666.949, ratio: 1.068
#   pulse 7: activated nodes: 11030, borderline nodes: 2918, overall activation: 1420.406, activation diff: 4854.959, ratio: 3.418
#   pulse 8: activated nodes: 11204, borderline nodes: 783, overall activation: 4515.682, activation diff: 5936.088, ratio: 1.315
#   pulse 9: activated nodes: 11268, borderline nodes: 343, overall activation: 1754.409, activation diff: 6270.092, ratio: 3.574
#   pulse 10: activated nodes: 11312, borderline nodes: 134, overall activation: 4609.071, activation diff: 6363.480, ratio: 1.381
#   pulse 11: activated nodes: 11320, borderline nodes: 92, overall activation: 1790.435, activation diff: 6399.506, ratio: 3.574
#   pulse 12: activated nodes: 11320, borderline nodes: 81, overall activation: 4622.531, activation diff: 6412.966, ratio: 1.387
#   pulse 13: activated nodes: 11324, borderline nodes: 81, overall activation: 1795.884, activation diff: 6418.415, ratio: 3.574
#   pulse 14: activated nodes: 11324, borderline nodes: 81, overall activation: 4625.076, activation diff: 6420.960, ratio: 1.388
#   pulse 15: activated nodes: 11324, borderline nodes: 77, overall activation: 1796.906, activation diff: 6421.983, ratio: 3.574

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 1796.9
#   number of spread. activ. pulses: 15
#   running time: 644

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
