###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.075, activation diff: 3.075, ratio: 0.506
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 5.916, activation diff: 1.990, ratio: 0.336
#   pulse 3: activated nodes: 5479, borderline nodes: 5217, overall activation: 7.874, activation diff: 3.560, ratio: 0.452
#   pulse 4: activated nodes: 6095, borderline nodes: 5201, overall activation: 10.837, activation diff: 4.325, ratio: 0.399
#   pulse 5: activated nodes: 6261, borderline nodes: 5159, overall activation: 18.478, activation diff: 8.770, ratio: 0.475
#   pulse 6: activated nodes: 6608, borderline nodes: 5019, overall activation: 36.915, activation diff: 19.219, ratio: 0.521
#   pulse 7: activated nodes: 7573, borderline nodes: 5345, overall activation: 82.294, activation diff: 45.749, ratio: 0.556
#   pulse 8: activated nodes: 8507, borderline nodes: 5538, overall activation: 181.352, activation diff: 99.180, ratio: 0.547
#   pulse 9: activated nodes: 9507, borderline nodes: 5405, overall activation: 364.148, activation diff: 182.819, ratio: 0.502
#   pulse 10: activated nodes: 10286, borderline nodes: 4606, overall activation: 656.152, activation diff: 292.003, ratio: 0.445
#   pulse 11: activated nodes: 10762, borderline nodes: 3432, overall activation: 1070.572, activation diff: 414.420, ratio: 0.387
#   pulse 12: activated nodes: 11054, borderline nodes: 2120, overall activation: 1594.351, activation diff: 523.779, ratio: 0.329
#   pulse 13: activated nodes: 11182, borderline nodes: 1314, overall activation: 2183.978, activation diff: 589.626, ratio: 0.270
#   pulse 14: activated nodes: 11225, borderline nodes: 766, overall activation: 2788.902, activation diff: 604.925, ratio: 0.217
#   pulse 15: activated nodes: 11244, borderline nodes: 446, overall activation: 3370.721, activation diff: 581.818, ratio: 0.173

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11244
#   final overall activation: 3370.7
#   number of spread. activ. pulses: 15
#   running time: 512

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8734745   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.7775962   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   3   0.7688601   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.7609511   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   5   0.755571   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   6   0.7516568   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.75063753   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   8   0.7495361   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   9   0.7492646   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   10   0.74859935   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7484443   REFERENCES
1   Q1   essentialsinmod01howegoog_14   12   0.74820983   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7447734   REFERENCES
1   Q1   essentialsinmod01howegoog_471   14   0.74275416   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   15   0.7425128   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7422499   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.74052835   REFERENCES
1   Q1   europesincenapol00leveuoft_353   18   0.73887014   REFERENCES
1   Q1   essentialsinmod01howegoog_474   19   0.7379819   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   20   0.73548895   REFERENCES
