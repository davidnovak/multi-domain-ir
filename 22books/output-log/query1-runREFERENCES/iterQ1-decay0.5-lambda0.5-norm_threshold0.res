###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.692, activation diff: 7.692, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 74.191, activation diff: 68.150, ratio: 0.919
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 115.615, activation diff: 42.297, ratio: 0.366
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 370.114, activation diff: 254.560, ratio: 0.688
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 639.159, activation diff: 269.045, ratio: 0.421
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 893.171, activation diff: 254.012, ratio: 0.284
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1086.968, activation diff: 193.798, ratio: 0.178
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1220.224, activation diff: 133.255, ratio: 0.109
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1306.116, activation diff: 85.892, ratio: 0.066
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1359.200, activation diff: 53.084, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1359.2
#   number of spread. activ. pulses: 10
#   running time: 404

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99115217   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9635869   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9561533   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93976474   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8979049   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.83853817   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.48461276   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.48411256   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.48345846   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.4824287   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48228914   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.4777934   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.47341222   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.4733457   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   15   0.47315758   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.46896553   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.4688033   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.4686618   REFERENCES
1   Q1   politicalsketche00retsrich_153   19   0.46742767   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.46723753   REFERENCES
