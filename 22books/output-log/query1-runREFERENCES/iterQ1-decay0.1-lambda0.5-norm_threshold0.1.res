###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.150, activation diff: 6.150, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 22.323, activation diff: 19.909, ratio: 0.892
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 26.815, activation diff: 7.861, ratio: 0.293
#   pulse 4: activated nodes: 7315, borderline nodes: 5663, overall activation: 208.942, activation diff: 182.930, ratio: 0.876
#   pulse 5: activated nodes: 8543, borderline nodes: 4839, overall activation: 457.392, activation diff: 248.479, ratio: 0.543
#   pulse 6: activated nodes: 9989, borderline nodes: 5094, overall activation: 1261.662, activation diff: 804.270, ratio: 0.637
#   pulse 7: activated nodes: 10931, borderline nodes: 2885, overall activation: 2351.477, activation diff: 1089.815, ratio: 0.463
#   pulse 8: activated nodes: 11182, borderline nodes: 1233, overall activation: 3573.336, activation diff: 1221.859, ratio: 0.342
#   pulse 9: activated nodes: 11244, borderline nodes: 496, overall activation: 4615.641, activation diff: 1042.305, ratio: 0.226
#   pulse 10: activated nodes: 11302, borderline nodes: 209, overall activation: 5391.403, activation diff: 775.762, ratio: 0.144
#   pulse 11: activated nodes: 11321, borderline nodes: 118, overall activation: 5927.404, activation diff: 536.001, ratio: 0.090
#   pulse 12: activated nodes: 11327, borderline nodes: 99, overall activation: 6283.151, activation diff: 355.747, ratio: 0.057
#   pulse 13: activated nodes: 11330, borderline nodes: 57, overall activation: 6514.044, activation diff: 230.893, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 6514.0
#   number of spread. activ. pulses: 13
#   running time: 496

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9968499   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9811793   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9790678   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9637543   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91705525   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8971273   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.89711756   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   8   0.89644146   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   9   0.89620876   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.89592946   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.8959179   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.89585173   REFERENCES
1   Q1   essentialsinmod01howegoog_474   13   0.89574325   REFERENCES
1   Q1   europesincenapol00leveuoft_353   14   0.89570063   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.8956841   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   16   0.8956525   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   17   0.8955482   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   18   0.89552605   REFERENCES
1   Q1   essentialsinmod01howegoog_471   19   0.89552575   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   20   0.89551866   REFERENCES
