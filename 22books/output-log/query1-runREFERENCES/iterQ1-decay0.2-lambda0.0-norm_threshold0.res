###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.385, activation diff: 15.385, ratio: 1.639
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 430.015, activation diff: 439.400, ratio: 1.022
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 274.633, activation diff: 704.649, ratio: 2.566
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3187.727, activation diff: 3462.360, ratio: 1.086
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1223.675, activation diff: 4411.402, ratio: 3.605
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3828.187, activation diff: 5051.863, ratio: 1.320
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 1385.555, activation diff: 5213.743, ratio: 3.763
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 3866.509, activation diff: 5252.064, ratio: 1.358
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 1397.913, activation diff: 5264.422, ratio: 3.766
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 3870.954, activation diff: 5268.868, ratio: 1.361
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 1399.316, activation diff: 5270.271, ratio: 3.766
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 3871.826, activation diff: 5271.142, ratio: 1.361
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 1399.530, activation diff: 5271.355, ratio: 3.767
#   pulse 14: activated nodes: 11335, borderline nodes: 30, overall activation: 3872.023, activation diff: 5271.553, ratio: 1.361
#   pulse 15: activated nodes: 11335, borderline nodes: 30, overall activation: 1399.568, activation diff: 5271.591, ratio: 3.767

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 1399.6
#   number of spread. activ. pulses: 15
#   running time: 573

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
