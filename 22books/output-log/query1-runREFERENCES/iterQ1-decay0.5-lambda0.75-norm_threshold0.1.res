###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.075, activation diff: 3.075, ratio: 0.506
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 5.775, activation diff: 1.850, ratio: 0.320
#   pulse 3: activated nodes: 5479, borderline nodes: 5217, overall activation: 6.546, activation diff: 2.380, ratio: 0.364
#   pulse 4: activated nodes: 6095, borderline nodes: 5201, overall activation: 7.691, activation diff: 2.575, ratio: 0.335
#   pulse 5: activated nodes: 6240, borderline nodes: 5164, overall activation: 9.797, activation diff: 3.362, ratio: 0.343
#   pulse 6: activated nodes: 6407, borderline nodes: 5097, overall activation: 14.014, activation diff: 5.227, ratio: 0.373
#   pulse 7: activated nodes: 6630, borderline nodes: 4972, overall activation: 22.592, activation diff: 9.302, ratio: 0.412
#   pulse 8: activated nodes: 6959, borderline nodes: 4823, overall activation: 39.850, activation diff: 17.664, ratio: 0.443
#   pulse 9: activated nodes: 7361, borderline nodes: 4708, overall activation: 72.618, activation diff: 32.946, ratio: 0.454
#   pulse 10: activated nodes: 7709, borderline nodes: 4467, overall activation: 128.454, activation diff: 55.897, ratio: 0.435
#   pulse 11: activated nodes: 8021, borderline nodes: 4162, overall activation: 208.468, activation diff: 80.021, ratio: 0.384
#   pulse 12: activated nodes: 8196, borderline nodes: 3989, overall activation: 306.854, activation diff: 98.386, ratio: 0.321
#   pulse 13: activated nodes: 8310, borderline nodes: 3785, overall activation: 415.912, activation diff: 109.059, ratio: 0.262
#   pulse 14: activated nodes: 8437, borderline nodes: 3641, overall activation: 528.003, activation diff: 112.091, ratio: 0.212
#   pulse 15: activated nodes: 8524, borderline nodes: 3534, overall activation: 637.213, activation diff: 109.209, ratio: 0.171

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8524
#   final overall activation: 637.2
#   number of spread. activ. pulses: 15
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.7617798   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.6018873   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   3   0.59798527   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.5804437   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   5   0.3660531   REFERENCES
1   Q1   politicalsketche00retsrich_153   6   0.35698774   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.35495132   REFERENCES
1   Q1   essentialsinmod01howegoog_37   8   0.34409794   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.34378776   REFERENCES
1   Q1   politicalsketche00retsrich_156   10   0.3407145   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   11   0.34017235   REFERENCES
1   Q1   cambridgemodern09protgoog_449   12   0.337282   REFERENCES
1   Q1   politicalsketche00retsrich_148   13   0.33710402   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.33440933   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.33240426   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.33165628   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.33133087   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   18   0.33081242   REFERENCES
1   Q1   ouroldworldbackg00bearrich_398   19   0.3294371   REFERENCES
1   Q1   ouroldworldbackg00bearrich_447   20   0.32845703   REFERENCES
