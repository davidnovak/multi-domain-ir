###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.695, activation diff: 4.695, ratio: 1.000
#   pulse 2: activated nodes: 3890, borderline nodes: 3835, overall activation: 2.703, activation diff: 2.159, ratio: 0.799
#   pulse 3: activated nodes: 3891, borderline nodes: 3802, overall activation: 1.369, activation diff: 1.334, ratio: 0.975
#   pulse 4: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.684, activation diff: 0.684, ratio: 1.000
#   pulse 5: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.342, activation diff: 0.342, ratio: 1.000
#   pulse 6: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.171, activation diff: 0.171, ratio: 1.000
#   pulse 7: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.086, activation diff: 0.086, ratio: 1.000
#   pulse 8: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.043, activation diff: 0.043, ratio: 1.000
#   pulse 9: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.021, activation diff: 0.021, ratio: 1.000
#   pulse 10: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.011, activation diff: 0.011, ratio: 1.000
#   pulse 11: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.005, activation diff: 0.005, ratio: 1.000
#   pulse 12: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.003, activation diff: 0.003, ratio: 1.000
#   pulse 13: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 14: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 15: activated nodes: 3891, borderline nodes: 3802, overall activation: 0.000, activation diff: 0.000, ratio: 1.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3891
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 225

###################################
# top k results in TREC format: 

1   Q1   selwynhouseschoo1940selw_21   1   3.244502E-5   REFERENCES
1   Q1   historyofuniteds07good_239   2   3.2343647E-5   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   3   3.221259E-5   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   3.1140033E-5   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   3.0517578E-5   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   3.0517578E-5   REFERENCES
1   Q1   generalhistoryfo00myerrich_657   7   3.4570851E-7   REFERENCES
1   Q1   shorthistoryofmo00haslrich_241   8   3.2537628E-7   REFERENCES
1   Q1   generalhistoryfo00myerrich_18   9   3.2537628E-7   REFERENCES
1   Q1   bostoncollegebul0405bost_147   10   3.2537628E-7   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_119   11   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_134   12   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_136   13   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_137   14   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_164   15   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_151   16   3.2537628E-7   REFERENCES
1   Q1   graduateschoolof9192bran_62   17   3.2537628E-7   REFERENCES
1   Q1   ouroldworldbackg00bearrich_281   18   3.2537628E-7   REFERENCES
1   Q1   europesincenapol00leveuoft_0   19   3.2537628E-7   REFERENCES
1   Q1   shorthistoryofmo00haslrich_14   20   3.2537628E-7   REFERENCES
