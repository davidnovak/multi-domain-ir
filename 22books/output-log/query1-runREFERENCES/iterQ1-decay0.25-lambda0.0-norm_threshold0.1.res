###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.300, activation diff: 12.300, ratio: 1.952
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 140.762, activation diff: 147.062, ratio: 1.045
#   pulse 3: activated nodes: 7515, borderline nodes: 4653, overall activation: 49.251, activation diff: 190.012, ratio: 3.858
#   pulse 4: activated nodes: 9216, borderline nodes: 5764, overall activation: 1863.560, activation diff: 1912.811, ratio: 1.026
#   pulse 5: activated nodes: 10574, borderline nodes: 3758, overall activation: 618.726, activation diff: 2482.287, ratio: 4.012
#   pulse 6: activated nodes: 10994, borderline nodes: 1612, overall activation: 3103.865, activation diff: 3722.591, ratio: 1.199
#   pulse 7: activated nodes: 11231, borderline nodes: 744, overall activation: 963.359, activation diff: 4067.224, ratio: 4.222
#   pulse 8: activated nodes: 11257, borderline nodes: 643, overall activation: 3215.074, activation diff: 4178.432, ratio: 1.300
#   pulse 9: activated nodes: 11274, borderline nodes: 624, overall activation: 996.119, activation diff: 4211.192, ratio: 4.228
#   pulse 10: activated nodes: 11274, borderline nodes: 624, overall activation: 3223.332, activation diff: 4219.451, ratio: 1.309
#   pulse 11: activated nodes: 11274, borderline nodes: 599, overall activation: 999.131, activation diff: 4222.463, ratio: 4.226
#   pulse 12: activated nodes: 11274, borderline nodes: 599, overall activation: 3224.207, activation diff: 4223.338, ratio: 1.310
#   pulse 13: activated nodes: 11274, borderline nodes: 599, overall activation: 999.494, activation diff: 4223.701, ratio: 4.226
#   pulse 14: activated nodes: 11274, borderline nodes: 599, overall activation: 3224.355, activation diff: 4223.849, ratio: 1.310
#   pulse 15: activated nodes: 11274, borderline nodes: 599, overall activation: 999.551, activation diff: 4223.907, ratio: 4.226

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11274
#   final overall activation: 999.6
#   number of spread. activ. pulses: 15
#   running time: 532

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
