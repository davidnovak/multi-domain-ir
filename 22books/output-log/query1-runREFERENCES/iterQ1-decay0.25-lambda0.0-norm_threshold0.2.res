###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.390, activation diff: 9.390, ratio: 2.770
#   pulse 2: activated nodes: 3890, borderline nodes: 3835, overall activation: 21.665, activation diff: 25.056, ratio: 1.156
#   pulse 3: activated nodes: 4650, borderline nodes: 3759, overall activation: 4.236, activation diff: 25.902, ratio: 6.114
#   pulse 4: activated nodes: 6148, borderline nodes: 5246, overall activation: 425.121, activation diff: 429.358, ratio: 1.010
#   pulse 5: activated nodes: 7784, borderline nodes: 4672, overall activation: 104.253, activation diff: 529.374, ratio: 5.078
#   pulse 6: activated nodes: 9507, borderline nodes: 5531, overall activation: 2060.695, activation diff: 2164.948, ratio: 1.051
#   pulse 7: activated nodes: 10791, borderline nodes: 3433, overall activation: 624.666, activation diff: 2685.361, ratio: 4.299
#   pulse 8: activated nodes: 11052, borderline nodes: 1754, overall activation: 2794.868, activation diff: 3419.534, ratio: 1.224
#   pulse 9: activated nodes: 11214, borderline nodes: 1137, overall activation: 822.135, activation diff: 3617.003, ratio: 4.400
#   pulse 10: activated nodes: 11221, borderline nodes: 1047, overall activation: 2860.982, activation diff: 3683.117, ratio: 1.287
#   pulse 11: activated nodes: 11233, borderline nodes: 1015, overall activation: 841.882, activation diff: 3702.864, ratio: 4.398
#   pulse 12: activated nodes: 11237, borderline nodes: 1010, overall activation: 2867.189, activation diff: 3709.071, ratio: 1.294
#   pulse 13: activated nodes: 11240, borderline nodes: 1005, overall activation: 844.115, activation diff: 3711.304, ratio: 4.397
#   pulse 14: activated nodes: 11240, borderline nodes: 1005, overall activation: 2867.952, activation diff: 3712.067, ratio: 1.294
#   pulse 15: activated nodes: 11241, borderline nodes: 1004, overall activation: 844.474, activation diff: 3712.425, ratio: 4.396

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11241
#   final overall activation: 844.5
#   number of spread. activ. pulses: 15
#   running time: 547

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
