###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.538, activation diff: 11.538, ratio: 1.351
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 231.812, activation diff: 230.297, ratio: 0.993
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 280.761, activation diff: 160.005, ratio: 0.570
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1706.254, activation diff: 1426.057, ratio: 0.836
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2637.996, activation diff: 931.742, ratio: 0.353
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 3535.113, activation diff: 897.117, ratio: 0.254
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 4021.728, activation diff: 486.616, ratio: 0.121
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4249.248, activation diff: 227.520, ratio: 0.054
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 4349.596, activation diff: 100.348, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4349.6
#   number of spread. activ. pulses: 9
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99939555   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98980355   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9883114   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9726712   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9300175   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90746033   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7497012   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.7490597   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   9   0.74901533   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.74888885   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7487805   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.748745   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.748703   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7486619   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.74861014   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   16   0.74860394   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.7485868   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.7485551   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.7484939   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.7484419   REFERENCES
