###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.300, activation diff: 12.300, ratio: 1.952
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 150.035, activation diff: 156.335, ratio: 1.042
#   pulse 3: activated nodes: 7515, borderline nodes: 4653, overall activation: 57.660, activation diff: 207.695, ratio: 3.602
#   pulse 4: activated nodes: 9233, borderline nodes: 5745, overall activation: 2117.329, activation diff: 2174.989, ratio: 1.027
#   pulse 5: activated nodes: 10617, borderline nodes: 3614, overall activation: 805.956, activation diff: 2923.285, ratio: 3.627
#   pulse 6: activated nodes: 11043, borderline nodes: 1329, overall activation: 3605.764, activation diff: 4411.720, ratio: 1.224
#   pulse 7: activated nodes: 11258, borderline nodes: 597, overall activation: 1247.611, activation diff: 4853.375, ratio: 3.890
#   pulse 8: activated nodes: 11303, borderline nodes: 174, overall activation: 3734.318, activation diff: 4981.929, ratio: 1.334
#   pulse 9: activated nodes: 11312, borderline nodes: 164, overall activation: 1287.551, activation diff: 5021.869, ratio: 3.900
#   pulse 10: activated nodes: 11315, borderline nodes: 153, overall activation: 3744.050, activation diff: 5031.600, ratio: 1.344
#   pulse 11: activated nodes: 11317, borderline nodes: 153, overall activation: 1291.668, activation diff: 5035.717, ratio: 3.899
#   pulse 12: activated nodes: 11317, borderline nodes: 153, overall activation: 3745.159, activation diff: 5036.826, ratio: 1.345
#   pulse 13: activated nodes: 11317, borderline nodes: 151, overall activation: 1292.292, activation diff: 5037.451, ratio: 3.898
#   pulse 14: activated nodes: 11317, borderline nodes: 151, overall activation: 3745.338, activation diff: 5037.630, ratio: 1.345
#   pulse 15: activated nodes: 11317, borderline nodes: 151, overall activation: 1292.415, activation diff: 5037.753, ratio: 3.898

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 1292.4
#   number of spread. activ. pulses: 15
#   running time: 614

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
