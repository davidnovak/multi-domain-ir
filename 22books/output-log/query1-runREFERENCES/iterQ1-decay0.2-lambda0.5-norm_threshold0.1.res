###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.150, activation diff: 6.150, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 20.318, activation diff: 17.905, ratio: 0.881
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 24.037, activation diff: 6.983, ratio: 0.291
#   pulse 4: activated nodes: 7305, borderline nodes: 5657, overall activation: 165.512, activation diff: 142.341, ratio: 0.860
#   pulse 5: activated nodes: 8474, borderline nodes: 4927, overall activation: 353.071, activation diff: 187.617, ratio: 0.531
#   pulse 6: activated nodes: 9803, borderline nodes: 5273, overall activation: 929.305, activation diff: 576.234, ratio: 0.620
#   pulse 7: activated nodes: 10779, borderline nodes: 3460, overall activation: 1684.004, activation diff: 754.699, ratio: 0.448
#   pulse 8: activated nodes: 11108, borderline nodes: 1698, overall activation: 2544.135, activation diff: 860.131, ratio: 0.338
#   pulse 9: activated nodes: 11224, borderline nodes: 850, overall activation: 3301.976, activation diff: 757.841, ratio: 0.230
#   pulse 10: activated nodes: 11247, borderline nodes: 480, overall activation: 3881.712, activation diff: 579.737, ratio: 0.149
#   pulse 11: activated nodes: 11295, borderline nodes: 302, overall activation: 4290.934, activation diff: 409.222, ratio: 0.095
#   pulse 12: activated nodes: 11303, borderline nodes: 208, overall activation: 4566.620, activation diff: 275.686, ratio: 0.060
#   pulse 13: activated nodes: 11318, borderline nodes: 160, overall activation: 4747.214, activation diff: 180.594, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4747.2
#   number of spread. activ. pulses: 13
#   running time: 517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965339   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97951734   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9768762   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9614854   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9149277   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.880493   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79693615   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7964791   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   9   0.7952982   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.79527026   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.7952154   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.7949829   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.7948367   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.79468286   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   15   0.7943307   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   16   0.79419184   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7941475   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   18   0.7940239   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   19   0.79396915   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.79394084   REFERENCES
