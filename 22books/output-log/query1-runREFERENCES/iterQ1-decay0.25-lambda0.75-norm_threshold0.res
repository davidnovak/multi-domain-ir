###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.846, activation diff: 3.846, ratio: 0.562
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 32.362, activation diff: 27.423, ratio: 0.847
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 68.979, activation diff: 37.878, ratio: 0.549
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 166.487, activation diff: 98.068, ratio: 0.589
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 341.726, activation diff: 175.341, ratio: 0.513
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 605.158, activation diff: 263.432, ratio: 0.435
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 940.852, activation diff: 335.694, ratio: 0.357
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 1320.751, activation diff: 379.899, ratio: 0.288
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1713.254, activation diff: 392.504, ratio: 0.229
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 2092.958, activation diff: 379.704, ratio: 0.181
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 2443.873, activation diff: 350.914, ratio: 0.144
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 2758.000, activation diff: 314.127, ratio: 0.114
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 3032.856, activation diff: 274.857, ratio: 0.091
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 3269.358, activation diff: 236.502, ratio: 0.072
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 3470.311, activation diff: 200.953, ratio: 0.058

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 3470.3
#   number of spread. activ. pulses: 15
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96774316   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9288591   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9174849   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.89994377   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8554845   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.79417646   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.71073645   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.70879704   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7038242   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.7024993   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.7016711   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.6999641   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.6997115   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   14   0.6987955   REFERENCES
1   Q1   essentialsinmod01howegoog_14   15   0.6987536   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.69861394   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.69837165   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.6982709   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   19   0.69814974   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.6969968   REFERENCES
