###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.922, activation diff: 1.922, ratio: 0.390
#   pulse 2: activated nodes: 3409, borderline nodes: 3380, overall activation: 3.857, activation diff: 1.227, ratio: 0.318
#   pulse 3: activated nodes: 3409, borderline nodes: 3380, overall activation: 2.955, activation diff: 0.904, ratio: 0.306
#   pulse 4: activated nodes: 3409, borderline nodes: 3380, overall activation: 2.234, activation diff: 0.721, ratio: 0.323
#   pulse 5: activated nodes: 3409, borderline nodes: 3380, overall activation: 1.675, activation diff: 0.558, ratio: 0.333
#   pulse 6: activated nodes: 3409, borderline nodes: 3380, overall activation: 1.256, activation diff: 0.419, ratio: 0.333
#   pulse 7: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.942, activation diff: 0.314, ratio: 0.333
#   pulse 8: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.707, activation diff: 0.236, ratio: 0.333
#   pulse 9: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.530, activation diff: 0.177, ratio: 0.333
#   pulse 10: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.398, activation diff: 0.133, ratio: 0.333
#   pulse 11: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.298, activation diff: 0.099, ratio: 0.333
#   pulse 12: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.224, activation diff: 0.075, ratio: 0.333
#   pulse 13: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.168, activation diff: 0.056, ratio: 0.333
#   pulse 14: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.126, activation diff: 0.042, ratio: 0.333
#   pulse 15: activated nodes: 3409, borderline nodes: 3380, overall activation: 0.094, activation diff: 0.031, ratio: 0.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3409
#   final overall activation: 0.1
#   number of spread. activ. pulses: 15
#   running time: 210

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   0.013363461   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   2   0.013363461   REFERENCES
1   Q1   historyofuniteds07good_239   3   0.013363461   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.013363461   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   5   0.013363461   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   6   0.013363461   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_368   11   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_367   12   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_365   13   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_370   14   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_374   15   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   16   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_375   17   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   18   0.0   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_366   19   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   20   0.0   REFERENCES
