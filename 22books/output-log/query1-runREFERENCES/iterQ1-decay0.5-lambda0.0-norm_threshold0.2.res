###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.390, activation diff: 9.390, ratio: 2.770
#   pulse 2: activated nodes: 3890, borderline nodes: 3835, overall activation: 14.677, activation diff: 18.067, ratio: 1.231
#   pulse 3: activated nodes: 4650, borderline nodes: 3759, overall activation: 2.660, activation diff: 17.337, ratio: 6.517
#   pulse 4: activated nodes: 5334, borderline nodes: 4440, overall activation: 160.950, activation diff: 163.610, ratio: 1.017
#   pulse 5: activated nodes: 6605, borderline nodes: 4083, overall activation: 20.359, activation diff: 181.309, ratio: 8.905
#   pulse 6: activated nodes: 7286, borderline nodes: 4753, overall activation: 882.242, activation diff: 902.601, ratio: 1.023
#   pulse 7: activated nodes: 8356, borderline nodes: 3743, overall activation: 51.446, activation diff: 933.688, ratio: 18.149
#   pulse 8: activated nodes: 8356, borderline nodes: 3743, overall activation: 1157.619, activation diff: 1209.065, ratio: 1.044
#   pulse 9: activated nodes: 8650, borderline nodes: 3406, overall activation: 57.460, activation diff: 1215.079, ratio: 21.147
#   pulse 10: activated nodes: 8650, borderline nodes: 3406, overall activation: 1188.308, activation diff: 1245.768, ratio: 1.048
#   pulse 11: activated nodes: 8654, borderline nodes: 3403, overall activation: 57.944, activation diff: 1246.252, ratio: 21.508
#   pulse 12: activated nodes: 8654, borderline nodes: 3403, overall activation: 1189.862, activation diff: 1247.806, ratio: 1.049
#   pulse 13: activated nodes: 8654, borderline nodes: 3403, overall activation: 57.988, activation diff: 1247.850, ratio: 21.519
#   pulse 14: activated nodes: 8654, borderline nodes: 3403, overall activation: 1189.967, activation diff: 1247.955, ratio: 1.049
#   pulse 15: activated nodes: 8654, borderline nodes: 3403, overall activation: 57.993, activation diff: 1247.960, ratio: 21.519

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8654
#   final overall activation: 58.0
#   number of spread. activ. pulses: 15
#   running time: 495

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
