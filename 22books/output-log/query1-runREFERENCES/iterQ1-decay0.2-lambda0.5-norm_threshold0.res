###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.692, activation diff: 7.692, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 114.569, activation diff: 108.529, ratio: 0.947
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 210.734, activation diff: 97.027, ratio: 0.460
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 875.891, activation diff: 665.207, ratio: 0.759
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1713.767, activation diff: 837.876, ratio: 0.489
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2681.446, activation diff: 967.678, ratio: 0.361
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3503.378, activation diff: 821.933, ratio: 0.235
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4110.599, activation diff: 607.221, ratio: 0.148
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 4526.677, activation diff: 416.078, ratio: 0.092
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 4799.927, activation diff: 273.250, ratio: 0.057
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 4974.986, activation diff: 175.059, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 4975.0
#   number of spread. activ. pulses: 11
#   running time: 465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9962232   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9812248   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.980988   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96341425   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.920166   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89071125   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79630196   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79563475   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7949855   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.7946553   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7945771   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.79451764   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7943828   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   14   0.7942245   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.79399073   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   16   0.7938974   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.7938172   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   18   0.79378974   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   19   0.7937553   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   20   0.7936963   REFERENCES
