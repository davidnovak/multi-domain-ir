###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.225, activation diff: 9.225, ratio: 1.482
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 63.083, activation diff: 64.121, ratio: 1.016
#   pulse 3: activated nodes: 7198, borderline nodes: 4816, overall activation: 32.551, activation diff: 59.921, ratio: 1.841
#   pulse 4: activated nodes: 8626, borderline nodes: 6039, overall activation: 790.757, activation diff: 775.469, ratio: 0.981
#   pulse 5: activated nodes: 10030, borderline nodes: 4475, overall activation: 876.110, activation diff: 340.620, ratio: 0.389
#   pulse 6: activated nodes: 10699, borderline nodes: 2795, overall activation: 2219.145, activation diff: 1355.970, ratio: 0.611
#   pulse 7: activated nodes: 11183, borderline nodes: 1498, overall activation: 3075.209, activation diff: 856.098, ratio: 0.278
#   pulse 8: activated nodes: 11220, borderline nodes: 787, overall activation: 3666.560, activation diff: 591.351, ratio: 0.161
#   pulse 9: activated nodes: 11251, borderline nodes: 606, overall activation: 3971.536, activation diff: 304.976, ratio: 0.077
#   pulse 10: activated nodes: 11277, borderline nodes: 607, overall activation: 4114.316, activation diff: 142.779, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11277
#   final overall activation: 4114.3
#   number of spread. activ. pulses: 10
#   running time: 428

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991469   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98768485   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9860077   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9689863   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92209727   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8953253   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.74951977   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.74868923   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   9   0.7485775   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7484771   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.74843913   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.74840474   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.7482712   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7482597   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.74818504   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   16   0.74818206   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.74806994   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   18   0.74806565   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.74792874   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   20   0.7478551   REFERENCES
