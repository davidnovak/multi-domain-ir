###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.075, activation diff: 3.075, ratio: 0.506
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 5.863, activation diff: 1.938, ratio: 0.331
#   pulse 3: activated nodes: 5479, borderline nodes: 5217, overall activation: 7.376, activation diff: 3.118, ratio: 0.423
#   pulse 4: activated nodes: 6095, borderline nodes: 5201, overall activation: 9.627, activation diff: 3.637, ratio: 0.378
#   pulse 5: activated nodes: 6258, borderline nodes: 5165, overall activation: 14.818, activation diff: 6.363, ratio: 0.429
#   pulse 6: activated nodes: 6514, borderline nodes: 5012, overall activation: 26.641, activation diff: 12.687, ratio: 0.476
#   pulse 7: activated nodes: 7338, borderline nodes: 5322, overall activation: 54.163, activation diff: 28.008, ratio: 0.517
#   pulse 8: activated nodes: 8054, borderline nodes: 5344, overall activation: 113.234, activation diff: 59.261, ratio: 0.523
#   pulse 9: activated nodes: 9082, borderline nodes: 5493, overall activation: 222.561, activation diff: 109.379, ratio: 0.491
#   pulse 10: activated nodes: 9817, borderline nodes: 5257, overall activation: 391.168, activation diff: 168.607, ratio: 0.431
#   pulse 11: activated nodes: 10400, borderline nodes: 4426, overall activation: 621.403, activation diff: 230.235, ratio: 0.371
#   pulse 12: activated nodes: 10779, borderline nodes: 3333, overall activation: 906.971, activation diff: 285.567, ratio: 0.315
#   pulse 13: activated nodes: 11003, borderline nodes: 2259, overall activation: 1233.035, activation diff: 326.065, ratio: 0.264
#   pulse 14: activated nodes: 11144, borderline nodes: 1543, overall activation: 1578.372, activation diff: 345.337, ratio: 0.219
#   pulse 15: activated nodes: 11195, borderline nodes: 1080, overall activation: 1921.802, activation diff: 343.430, ratio: 0.179

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11195
#   final overall activation: 1921.8
#   number of spread. activ. pulses: 15
#   running time: 522

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8503312   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.7366407   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.7094369   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.6948738   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   5   0.61081827   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.6023666   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   7   0.59915817   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.59727556   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.59683734   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   10   0.59451354   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   11   0.59385276   REFERENCES
1   Q1   cambridgemodern09protgoog_449   12   0.59197265   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   13   0.59031594   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.5897187   REFERENCES
1   Q1   essentialsinmod01howegoog_14   15   0.587013   REFERENCES
1   Q1   politicalsketche00retsrich_154   16   0.5855433   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.58185077   REFERENCES
1   Q1   politicalsketche00retsrich_156   18   0.5814013   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   19   0.5811485   REFERENCES
1   Q1   essentialsinmod01howegoog_471   20   0.58085656   REFERENCES
