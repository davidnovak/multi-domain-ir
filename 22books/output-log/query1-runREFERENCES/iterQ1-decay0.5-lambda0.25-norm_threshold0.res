###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.538, activation diff: 11.538, ratio: 1.351
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 156.341, activation diff: 154.826, ratio: 0.990
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 139.105, activation diff: 57.214, ratio: 0.411
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 778.569, activation diff: 640.218, ratio: 0.822
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1046.875, activation diff: 268.315, ratio: 0.256
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1274.942, activation diff: 228.068, ratio: 0.179
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1374.441, activation diff: 99.499, ratio: 0.072
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1413.504, activation diff: 39.063, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1413.5
#   number of spread. activ. pulses: 8
#   running time: 378

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9982899   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98092353   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9803791   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9608483   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9183892   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8774811   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.49146506   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49133313   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49115485   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.4909301   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48841774   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.48483604   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.48086786   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.48086447   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.48079878   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.4781549   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.47603923   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.4758733   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.47579864   REFERENCES
1   Q1   essentialsinmod01howegoog_446   20   0.47569185   REFERENCES
