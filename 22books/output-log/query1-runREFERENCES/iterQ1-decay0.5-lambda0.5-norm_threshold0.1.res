###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.150, activation diff: 6.150, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 14.305, activation diff: 11.891, ratio: 0.831
#   pulse 3: activated nodes: 6657, borderline nodes: 5027, overall activation: 15.882, activation diff: 4.609, ratio: 0.290
#   pulse 4: activated nodes: 6657, borderline nodes: 5027, overall activation: 65.729, activation diff: 50.945, ratio: 0.775
#   pulse 5: activated nodes: 7573, borderline nodes: 4682, overall activation: 128.630, activation diff: 63.096, ratio: 0.491
#   pulse 6: activated nodes: 7730, borderline nodes: 4424, overall activation: 329.430, activation diff: 200.816, ratio: 0.610
#   pulse 7: activated nodes: 8248, borderline nodes: 3910, overall activation: 556.513, activation diff: 227.083, ratio: 0.408
#   pulse 8: activated nodes: 8422, borderline nodes: 3686, overall activation: 778.904, activation diff: 222.390, ratio: 0.286
#   pulse 9: activated nodes: 8602, borderline nodes: 3479, overall activation: 960.141, activation diff: 181.237, ratio: 0.189
#   pulse 10: activated nodes: 8646, borderline nodes: 3424, overall activation: 1094.204, activation diff: 134.064, ratio: 0.123
#   pulse 11: activated nodes: 8654, borderline nodes: 3405, overall activation: 1186.804, activation diff: 92.600, ratio: 0.078
#   pulse 12: activated nodes: 8654, borderline nodes: 3403, overall activation: 1247.565, activation diff: 60.760, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8654
#   final overall activation: 1247.6
#   number of spread. activ. pulses: 12
#   running time: 433

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890534   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.95105517   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.92868185   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9183775   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.88580126   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.7983601   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.48326212   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.48234993   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.48166466   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.4811449   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.4788723   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47656   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.472026   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.47119945   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   15   0.4711   REFERENCES
1   Q1   politicalsketche00retsrich_148   16   0.46700752   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.4665571   REFERENCES
1   Q1   politicalsketche00retsrich_153   18   0.46622753   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.46502477   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.4649367   REFERENCES
