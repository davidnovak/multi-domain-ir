###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.454, activation diff: 3.454, ratio: 0.535
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 31.240, activation diff: 26.732, ratio: 0.856
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 66.733, activation diff: 36.799, ratio: 0.551
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 164.840, activation diff: 98.722, ratio: 0.599
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 345.390, activation diff: 180.671, ratio: 0.523
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 625.648, activation diff: 280.258, ratio: 0.448
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 993.305, activation diff: 367.657, ratio: 0.370
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 1422.229, activation diff: 428.924, ratio: 0.302
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 1880.281, activation diff: 458.052, ratio: 0.244
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 2338.779, activation diff: 458.498, ratio: 0.196
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 2776.653, activation diff: 437.873, ratio: 0.158
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 3180.441, activation diff: 403.788, ratio: 0.127
#   pulse 13: activated nodes: 11455, borderline nodes: 19, overall activation: 3543.061, activation diff: 362.620, ratio: 0.102
#   pulse 14: activated nodes: 11455, borderline nodes: 19, overall activation: 3862.400, activation diff: 319.339, ratio: 0.083
#   pulse 15: activated nodes: 11455, borderline nodes: 19, overall activation: 4139.604, activation diff: 277.205, ratio: 0.067

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 4139.6
#   number of spread. activ. pulses: 15
#   running time: 1642

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96502763   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.92235696   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.91525215   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.88975054   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.84529924   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.78076214   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.71108454   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.70992   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.70419735   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7032805   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7029067   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.7027788   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.70270133   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.70170367   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7016944   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.70126116   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.70124686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.7011452   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.70025086   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   20   0.6997969   REFERENCES:SIMLOC
