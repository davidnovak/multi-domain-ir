###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.363, activation diff: 10.363, ratio: 1.407
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 152.625, activation diff: 151.000, ratio: 0.989
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 153.315, activation diff: 68.526, ratio: 0.447
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 897.802, activation diff: 744.760, ratio: 0.830
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1282.622, activation diff: 384.821, ratio: 0.300
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1718.246, activation diff: 435.624, ratio: 0.254
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1980.604, activation diff: 262.358, ratio: 0.132
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 2138.524, activation diff: 157.920, ratio: 0.074
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2233.335, activation diff: 94.811, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2233.3
#   number of spread. activ. pulses: 9
#   running time: 1398

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990703   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98793465   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98392385   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96445405   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9173385   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8883494   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4981984   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4969927   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49620685   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49585527   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49561828   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49450928   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.493778   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.49376735   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   15   0.49334908   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.49334234   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   17   0.4931538   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.49293268   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.49279138   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49254555   REFERENCES:SIMLOC
