###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.454, activation diff: 3.454, ratio: 0.535
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 22.985, activation diff: 18.476, ratio: 0.804
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 45.420, activation diff: 23.742, ratio: 0.523
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 93.208, activation diff: 48.486, ratio: 0.520
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 175.564, activation diff: 82.569, ratio: 0.470
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 297.370, activation diff: 121.822, ratio: 0.410
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 449.657, activation diff: 152.287, ratio: 0.339
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 619.673, activation diff: 170.016, ratio: 0.274
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 796.112, activation diff: 176.439, ratio: 0.222
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 970.430, activation diff: 174.318, ratio: 0.180
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 1136.726, activation diff: 166.296, ratio: 0.146
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 1291.196, activation diff: 154.470, ratio: 0.120
#   pulse 13: activated nodes: 10899, borderline nodes: 3506, overall activation: 1431.698, activation diff: 140.502, ratio: 0.098
#   pulse 14: activated nodes: 10899, borderline nodes: 3506, overall activation: 1557.452, activation diff: 125.753, ratio: 0.081
#   pulse 15: activated nodes: 10899, borderline nodes: 3506, overall activation: 1668.672, activation diff: 111.220, ratio: 0.067

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 1668.7
#   number of spread. activ. pulses: 15
#   running time: 1611

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9575511   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.90004236   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.8798606   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.8612032   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.82389855   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.72966564   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46041554   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.46004948   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.45825005   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.45600742   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.4558676   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.45574123   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.45327482   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   14   0.45299077   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.45239332   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.45115256   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.44929343   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.4489031   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.44874686   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.44864786   REFERENCES:SIMLOC
