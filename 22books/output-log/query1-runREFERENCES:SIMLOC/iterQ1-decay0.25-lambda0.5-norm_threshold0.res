###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.908, activation diff: 6.908, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 104.949, activation diff: 99.838, ratio: 0.951
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 186.842, activation diff: 82.798, ratio: 0.443
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 797.987, activation diff: 611.159, ratio: 0.766
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1553.272, activation diff: 755.286, ratio: 0.486
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 2481.592, activation diff: 928.320, ratio: 0.374
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 3329.668, activation diff: 848.076, ratio: 0.255
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 4011.838, activation diff: 682.169, ratio: 0.170
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 4516.970, activation diff: 505.132, ratio: 0.112
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 4873.234, activation diff: 356.264, ratio: 0.073
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 5117.313, activation diff: 244.079, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5117.3
#   number of spread. activ. pulses: 11
#   running time: 1441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.995713   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9798966   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.978793   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9594796   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91314787   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8828301   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7463238   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7455945   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.74484926   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7448065   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7442595   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.7442572   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.7441758   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.744163   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.744134   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.74401   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.7438169   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.74352455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.7434634   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.7434225   REFERENCES:SIMLOC
