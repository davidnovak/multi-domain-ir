###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.709, activation diff: 10.709, ratio: 2.274
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 130.827, activation diff: 135.535, ratio: 1.036
#   pulse 3: activated nodes: 8514, borderline nodes: 5750, overall activation: 41.823, activation diff: 172.649, ratio: 4.128
#   pulse 4: activated nodes: 10563, borderline nodes: 7431, overall activation: 1894.961, activation diff: 1936.270, ratio: 1.022
#   pulse 5: activated nodes: 10870, borderline nodes: 4133, overall activation: 491.144, activation diff: 2378.535, ratio: 4.843
#   pulse 6: activated nodes: 11322, borderline nodes: 2763, overall activation: 3531.444, activation diff: 3883.564, ratio: 1.100
#   pulse 7: activated nodes: 11413, borderline nodes: 680, overall activation: 1378.543, activation diff: 3761.538, ratio: 2.729
#   pulse 8: activated nodes: 11419, borderline nodes: 378, overall activation: 4328.809, activation diff: 3733.171, ratio: 0.862
#   pulse 9: activated nodes: 11423, borderline nodes: 243, overall activation: 4445.188, activation diff: 1002.366, ratio: 0.225
#   pulse 10: activated nodes: 11423, borderline nodes: 211, overall activation: 5026.044, activation diff: 630.896, ratio: 0.126
#   pulse 11: activated nodes: 11427, borderline nodes: 182, overall activation: 5120.865, activation diff: 112.224, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11427
#   final overall activation: 5120.9
#   number of spread. activ. pulses: 11
#   running time: 1507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995092   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99011844   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9872302   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9718133   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91877687   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89930046   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74996126   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7497823   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.749702   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7496866   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7496699   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7496135   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.74961096   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.74957746   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7495682   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.74955153   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.74950814   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.74949145   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.74948776   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.74943537   REFERENCES:SIMLOC
