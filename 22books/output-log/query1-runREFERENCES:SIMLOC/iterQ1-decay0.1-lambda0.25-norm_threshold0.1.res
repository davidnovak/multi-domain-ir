###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.031, activation diff: 8.031, ratio: 1.596
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 68.875, activation diff: 69.785, ratio: 1.013
#   pulse 3: activated nodes: 8369, borderline nodes: 6075, overall activation: 35.756, activation diff: 66.771, ratio: 1.867
#   pulse 4: activated nodes: 10228, borderline nodes: 7756, overall activation: 1067.137, activation diff: 1050.368, ratio: 0.984
#   pulse 5: activated nodes: 10589, borderline nodes: 4829, overall activation: 1208.169, activation diff: 485.542, ratio: 0.402
#   pulse 6: activated nodes: 11259, borderline nodes: 3936, overall activation: 3518.950, activation diff: 2316.479, ratio: 0.658
#   pulse 7: activated nodes: 11378, borderline nodes: 1115, overall activation: 5011.936, activation diff: 1492.989, ratio: 0.298
#   pulse 8: activated nodes: 11423, borderline nodes: 376, overall activation: 6198.131, activation diff: 1186.196, ratio: 0.191
#   pulse 9: activated nodes: 11439, borderline nodes: 110, overall activation: 6872.691, activation diff: 674.560, ratio: 0.098
#   pulse 10: activated nodes: 11445, borderline nodes: 60, overall activation: 7230.467, activation diff: 357.776, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 7230.5
#   number of spread. activ. pulses: 10
#   running time: 1463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99910986   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9883163   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9859301   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9695351   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9196626   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89959013   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89946663   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.899434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.8994178   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.89940786   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.8993698   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.89935756   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   13   0.899315   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   14   0.89930654   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8993044   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.899279   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.89927614   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.89925945   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.899248   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.89923596   REFERENCES:SIMLOC
