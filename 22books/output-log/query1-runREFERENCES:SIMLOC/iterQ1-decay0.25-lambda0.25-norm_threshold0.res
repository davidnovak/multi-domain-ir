###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.363, activation diff: 10.363, ratio: 1.407
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 226.572, activation diff: 224.948, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 268.541, activation diff: 141.634, ratio: 0.527
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1843.522, activation diff: 1575.135, ratio: 0.854
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2849.835, activation diff: 1006.313, ratio: 0.353
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 4056.931, activation diff: 1207.097, ratio: 0.298
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 4783.930, activation diff: 726.998, ratio: 0.152
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 5183.355, activation diff: 399.425, ratio: 0.077
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 5390.485, activation diff: 207.131, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5390.5
#   number of spread. activ. pulses: 9
#   running time: 1453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993036   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98971045   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9874859   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97177804   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.925567   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.904768   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7497543   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7493621   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7492915   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.7492532   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.74917865   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.7491528   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.7491501   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7491176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.74911726   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.7491129   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   17   0.7490905   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.74899864   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.74895096   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.74894094   REFERENCES:SIMLOC
