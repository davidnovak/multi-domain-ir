###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.354, activation diff: 5.354, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 12.405, activation diff: 10.871, ratio: 0.876
#   pulse 3: activated nodes: 8062, borderline nodes: 6554, overall activation: 13.618, activation diff: 4.091, ratio: 0.300
#   pulse 4: activated nodes: 8537, borderline nodes: 7025, overall activation: 55.364, activation diff: 42.537, ratio: 0.768
#   pulse 5: activated nodes: 9003, borderline nodes: 6248, overall activation: 108.497, activation diff: 53.299, ratio: 0.491
#   pulse 6: activated nodes: 9527, borderline nodes: 6345, overall activation: 308.339, activation diff: 199.861, ratio: 0.648
#   pulse 7: activated nodes: 9915, borderline nodes: 5416, overall activation: 563.801, activation diff: 255.463, ratio: 0.453
#   pulse 8: activated nodes: 10531, borderline nodes: 5226, overall activation: 858.650, activation diff: 294.849, ratio: 0.343
#   pulse 9: activated nodes: 10679, borderline nodes: 4717, overall activation: 1134.681, activation diff: 276.031, ratio: 0.243
#   pulse 10: activated nodes: 10755, borderline nodes: 4431, overall activation: 1371.244, activation diff: 236.563, ratio: 0.173
#   pulse 11: activated nodes: 10781, borderline nodes: 4104, overall activation: 1565.126, activation diff: 193.882, ratio: 0.124
#   pulse 12: activated nodes: 10838, borderline nodes: 3850, overall activation: 1719.365, activation diff: 154.239, ratio: 0.090
#   pulse 13: activated nodes: 10853, borderline nodes: 3738, overall activation: 1838.581, activation diff: 119.217, ratio: 0.065
#   pulse 14: activated nodes: 10860, borderline nodes: 3679, overall activation: 1929.278, activation diff: 90.696, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10860
#   final overall activation: 1929.3
#   number of spread. activ. pulses: 14
#   running time: 1495

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99597776   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9708829   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9685662   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9460012   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8972517   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8436252   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49534786   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49411696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.49264956   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.49263683   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.49242812   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49010813   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.4900177   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.48945785   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4893178   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.4892556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.48895484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.48887306   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.48876292   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.488352   REFERENCES:SIMLOC
