###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.908, activation diff: 6.908, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 111.542, activation diff: 106.431, ratio: 0.954
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 201.668, activation diff: 91.031, ratio: 0.451
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 896.659, activation diff: 695.003, ratio: 0.775
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1767.825, activation diff: 871.166, ratio: 0.493
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 2852.573, activation diff: 1084.748, ratio: 0.380
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 3838.762, activation diff: 986.189, ratio: 0.257
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 4618.576, activation diff: 779.815, ratio: 0.169
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 5185.541, activation diff: 566.965, ratio: 0.109
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 5578.945, activation diff: 393.404, ratio: 0.071
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 5843.935, activation diff: 264.991, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 5843.9
#   number of spread. activ. pulses: 11
#   running time: 1466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99581814   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9804795   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9795109   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9607287   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9145441   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8854105   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79630125   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7958388   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7951251   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.79503393   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.7947626   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.7947204   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.79467356   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7943889   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.79438496   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7943519   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   17   0.7942879   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.7942631   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.7942555   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.793985   REFERENCES:SIMLOC
