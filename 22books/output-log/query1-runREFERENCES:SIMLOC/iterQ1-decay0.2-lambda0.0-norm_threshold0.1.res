###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.709, activation diff: 10.709, ratio: 2.274
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 139.457, activation diff: 144.165, ratio: 1.034
#   pulse 3: activated nodes: 8514, borderline nodes: 5750, overall activation: 47.152, activation diff: 186.609, ratio: 3.958
#   pulse 4: activated nodes: 10583, borderline nodes: 7436, overall activation: 2123.774, activation diff: 2170.259, ratio: 1.022
#   pulse 5: activated nodes: 10886, borderline nodes: 3970, overall activation: 595.502, activation diff: 2709.734, ratio: 4.550
#   pulse 6: activated nodes: 11336, borderline nodes: 2502, overall activation: 4062.107, activation diff: 4469.159, ratio: 1.100
#   pulse 7: activated nodes: 11416, borderline nodes: 565, overall activation: 1831.067, activation diff: 4183.422, ratio: 2.285
#   pulse 8: activated nodes: 11429, borderline nodes: 231, overall activation: 5079.495, activation diff: 4048.541, ratio: 0.797
#   pulse 9: activated nodes: 11439, borderline nodes: 144, overall activation: 5343.240, activation diff: 918.372, ratio: 0.172
#   pulse 10: activated nodes: 11440, borderline nodes: 107, overall activation: 5814.675, activation diff: 510.613, ratio: 0.088
#   pulse 11: activated nodes: 11441, borderline nodes: 79, overall activation: 5898.001, activation diff: 95.579, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 5898.0
#   number of spread. activ. pulses: 11
#   running time: 1611

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99953073   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9901459   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879356   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9724696   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92066646   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9009901   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999822   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79993165   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.79986006   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.79985976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7998592   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.79984057   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.79983246   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.79983175   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.7998298   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.79981565   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.79981536   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7997934   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7997904   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.79977864   REFERENCES:SIMLOC
