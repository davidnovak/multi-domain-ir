###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.354, activation diff: 5.354, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 17.782, activation diff: 16.247, ratio: 0.914
#   pulse 3: activated nodes: 8062, borderline nodes: 6554, overall activation: 20.751, activation diff: 6.142, ratio: 0.296
#   pulse 4: activated nodes: 8668, borderline nodes: 7148, overall activation: 142.197, activation diff: 122.041, ratio: 0.858
#   pulse 5: activated nodes: 9240, borderline nodes: 5890, overall activation: 301.701, activation diff: 159.560, ratio: 0.529
#   pulse 6: activated nodes: 10705, borderline nodes: 6577, overall activation: 862.400, activation diff: 560.706, ratio: 0.650
#   pulse 7: activated nodes: 11117, borderline nodes: 4365, overall activation: 1600.513, activation diff: 738.112, ratio: 0.461
#   pulse 8: activated nodes: 11291, borderline nodes: 2744, overall activation: 2494.863, activation diff: 894.350, ratio: 0.358
#   pulse 9: activated nodes: 11370, borderline nodes: 1238, overall activation: 3345.978, activation diff: 851.115, ratio: 0.254
#   pulse 10: activated nodes: 11412, borderline nodes: 553, overall activation: 4072.883, activation diff: 726.905, ratio: 0.178
#   pulse 11: activated nodes: 11429, borderline nodes: 304, overall activation: 4642.658, activation diff: 569.774, ratio: 0.123
#   pulse 12: activated nodes: 11435, borderline nodes: 177, overall activation: 5063.603, activation diff: 420.945, ratio: 0.083
#   pulse 13: activated nodes: 11437, borderline nodes: 118, overall activation: 5363.925, activation diff: 300.322, ratio: 0.056
#   pulse 14: activated nodes: 11438, borderline nodes: 90, overall activation: 5573.387, activation diff: 209.462, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5573.4
#   number of spread. activ. pulses: 14
#   running time: 1566

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9976596   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9823251   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9814838   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96322876   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9130237   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.884341   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79830945   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7979828   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.79755956   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.79740715   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7973809   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.7972194   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.7972103   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.7970725   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.79691035   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   16   0.79686177   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.79681826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.79677   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.79672027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7965982   REFERENCES:SIMLOC
