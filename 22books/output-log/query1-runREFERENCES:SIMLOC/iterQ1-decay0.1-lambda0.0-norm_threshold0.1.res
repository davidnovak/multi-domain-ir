###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.709, activation diff: 10.709, ratio: 2.274
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 156.717, activation diff: 161.426, ratio: 1.030
#   pulse 3: activated nodes: 8514, borderline nodes: 5750, overall activation: 59.256, activation diff: 215.973, ratio: 3.645
#   pulse 4: activated nodes: 10634, borderline nodes: 7438, overall activation: 2634.820, activation diff: 2692.993, ratio: 1.022
#   pulse 5: activated nodes: 10971, borderline nodes: 3754, overall activation: 837.729, activation diff: 3457.836, ratio: 4.128
#   pulse 6: activated nodes: 11348, borderline nodes: 2034, overall activation: 5247.519, activation diff: 5764.216, ratio: 1.098
#   pulse 7: activated nodes: 11429, borderline nodes: 389, overall activation: 2967.240, activation diff: 4939.537, ratio: 1.665
#   pulse 8: activated nodes: 11439, borderline nodes: 126, overall activation: 6719.159, activation diff: 4541.716, ratio: 0.676
#   pulse 9: activated nodes: 11447, borderline nodes: 62, overall activation: 7142.505, activation diff: 816.035, ratio: 0.114
#   pulse 10: activated nodes: 11448, borderline nodes: 40, overall activation: 7471.372, activation diff: 364.467, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7471.4
#   number of spread. activ. pulses: 10
#   running time: 2501

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995515   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99015945   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.988399   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9729358   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9229557   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90234464   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8999961   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.89999264   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.8999883   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.8999796   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.89997524   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.8999737   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.89997286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.8999715   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   15   0.8999715   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.89997005   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.8999664   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   18   0.8999661   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   19   0.8999625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.8999571   REFERENCES:SIMLOC
