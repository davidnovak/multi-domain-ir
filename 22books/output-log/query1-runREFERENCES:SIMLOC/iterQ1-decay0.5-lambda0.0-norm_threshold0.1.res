###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.709, activation diff: 10.709, ratio: 2.274
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 87.676, activation diff: 92.385, ratio: 1.054
#   pulse 3: activated nodes: 8514, borderline nodes: 5750, overall activation: 21.978, activation diff: 109.654, ratio: 4.989
#   pulse 4: activated nodes: 9620, borderline nodes: 6756, overall activation: 988.065, activation diff: 1009.919, ratio: 1.022
#   pulse 5: activated nodes: 10035, borderline nodes: 4762, overall activation: 129.574, activation diff: 1116.637, ratio: 8.618
#   pulse 6: activated nodes: 10698, borderline nodes: 4887, overall activation: 1603.540, activation diff: 1716.181, ratio: 1.070
#   pulse 7: activated nodes: 10791, borderline nodes: 4107, overall activation: 225.879, activation diff: 1772.529, ratio: 7.847
#   pulse 8: activated nodes: 10817, borderline nodes: 3948, overall activation: 1781.122, activation diff: 1876.030, ratio: 1.053
#   pulse 9: activated nodes: 10863, borderline nodes: 3757, overall activation: 546.074, activation diff: 1638.372, ratio: 3.000
#   pulse 10: activated nodes: 10866, borderline nodes: 3737, overall activation: 1921.455, activation diff: 1605.234, ratio: 0.835
#   pulse 11: activated nodes: 10868, borderline nodes: 3709, overall activation: 1738.884, activation diff: 469.328, ratio: 0.270
#   pulse 12: activated nodes: 10868, borderline nodes: 3684, overall activation: 2099.010, activation diff: 387.507, ratio: 0.185
#   pulse 13: activated nodes: 10871, borderline nodes: 3646, overall activation: 2137.292, activation diff: 67.049, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10871
#   final overall activation: 2137.3
#   number of spread. activ. pulses: 13
#   running time: 1562

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992436   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9884202   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98190576   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9630084   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90731615   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8760771   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4982837   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4968018   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4960286   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49567807   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49546075   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4943671   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49429336   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.4936037   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49317706   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.49291813   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49287063   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.49251753   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.49238735   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49203983   REFERENCES:SIMLOC
