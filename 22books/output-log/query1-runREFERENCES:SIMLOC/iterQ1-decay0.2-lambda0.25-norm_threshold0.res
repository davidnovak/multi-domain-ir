###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.363, activation diff: 10.363, ratio: 1.407
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 241.362, activation diff: 239.738, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 294.828, activation diff: 159.379, ratio: 0.541
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2086.687, activation diff: 1791.993, ratio: 0.859
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3266.325, activation diff: 1179.638, ratio: 0.361
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4663.369, activation diff: 1397.044, ratio: 0.300
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 5482.165, activation diff: 818.795, ratio: 0.149
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 5917.933, activation diff: 435.768, ratio: 0.074
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 6136.405, activation diff: 218.472, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6136.4
#   number of spread. activ. pulses: 9
#   running time: 1444

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932355   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9898207   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9878765   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97249216   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9266307   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9060522   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79977804   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.799527   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7995046   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.7994946   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.7994724   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.79945064   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.79943913   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7994147   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   15   0.79940957   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.799388   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.7993828   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.79937065   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.79935837   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.79933876   REFERENCES:SIMLOC
