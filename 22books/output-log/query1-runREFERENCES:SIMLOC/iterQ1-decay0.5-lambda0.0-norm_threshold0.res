###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.817, activation diff: 13.817, ratio: 1.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 264.298, activation diff: 271.822, ratio: 1.028
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 78.973, activation diff: 327.131, ratio: 4.142
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1510.076, activation diff: 1542.947, ratio: 1.022
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 473.343, activation diff: 1462.994, ratio: 3.091
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1979.446, activation diff: 1690.782, ratio: 0.854
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1908.845, activation diff: 423.152, ratio: 0.222
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 2249.001, activation diff: 354.534, ratio: 0.158
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2304.051, activation diff: 66.611, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2304.1
#   number of spread. activ. pulses: 9
#   running time: 1375

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993993   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99001473   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9853155   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9683318   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91895354   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8939848   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49856257   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49738914   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49671075   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49634486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.496016   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4952284   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49517775   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.49450928   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.49421555   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.4940842   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49395254   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.4933186   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.4932513   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49310163   REFERENCES:SIMLOC
