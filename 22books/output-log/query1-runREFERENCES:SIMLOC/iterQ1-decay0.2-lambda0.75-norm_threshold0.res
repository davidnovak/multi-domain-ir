###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.454, activation diff: 3.454, ratio: 0.535
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 32.891, activation diff: 28.383, ratio: 0.863
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 71.133, activation diff: 39.549, ratio: 0.556
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 181.868, activation diff: 111.335, ratio: 0.612
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 387.976, activation diff: 206.220, ratio: 0.532
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 712.391, activation diff: 324.415, ratio: 0.455
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 1142.522, activation diff: 430.131, ratio: 0.376
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 1646.248, activation diff: 503.726, ratio: 0.306
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 2183.568, activation diff: 537.320, ratio: 0.246
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 2719.052, activation diff: 535.484, ratio: 0.197
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 3226.429, activation diff: 507.377, ratio: 0.157
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 3689.718, activation diff: 463.289, ratio: 0.126
#   pulse 13: activated nodes: 11457, borderline nodes: 2, overall activation: 4101.907, activation diff: 412.189, ratio: 0.100
#   pulse 14: activated nodes: 11457, borderline nodes: 2, overall activation: 4461.947, activation diff: 360.040, ratio: 0.081
#   pulse 15: activated nodes: 11457, borderline nodes: 2, overall activation: 4772.174, activation diff: 310.227, ratio: 0.065

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 4772.2
#   number of spread. activ. pulses: 15
#   running time: 1580

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9659458   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9250044   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9188113   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.89332104   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.84819186   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.7870684   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7602413   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.75945824   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7530429   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7529238   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7526429   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.75243115   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.7522657   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.75194764   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   15   0.75183487   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7512177   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.75100183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.75096947   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.7509482   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7506477   REFERENCES:SIMLOC
