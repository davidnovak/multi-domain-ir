###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.901, activation diff: 7.901, ratio: 4.156
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 21.982, activation diff: 23.883, ratio: 1.086
#   pulse 3: activated nodes: 5193, borderline nodes: 4383, overall activation: 3.883, activation diff: 25.865, ratio: 6.661
#   pulse 4: activated nodes: 7015, borderline nodes: 6195, overall activation: 486.065, activation diff: 489.948, ratio: 1.008
#   pulse 5: activated nodes: 8166, borderline nodes: 5167, overall activation: 134.644, activation diff: 620.709, ratio: 4.610
#   pulse 6: activated nodes: 10863, borderline nodes: 7214, overall activation: 3217.167, activation diff: 3350.635, ratio: 1.041
#   pulse 7: activated nodes: 11092, borderline nodes: 3482, overall activation: 872.811, activation diff: 4077.031, ratio: 4.671
#   pulse 8: activated nodes: 11338, borderline nodes: 2222, overall activation: 4974.675, activation diff: 5594.697, ratio: 1.125
#   pulse 9: activated nodes: 11412, borderline nodes: 702, overall activation: 2268.644, activation diff: 5097.823, ratio: 2.247
#   pulse 10: activated nodes: 11426, borderline nodes: 467, overall activation: 6047.667, activation diff: 4846.298, ratio: 0.801
#   pulse 11: activated nodes: 11434, borderline nodes: 289, overall activation: 6333.993, activation diff: 1161.817, ratio: 0.183
#   pulse 12: activated nodes: 11434, borderline nodes: 215, overall activation: 6913.063, activation diff: 652.176, ratio: 0.094
#   pulse 13: activated nodes: 11437, borderline nodes: 143, overall activation: 7018.117, activation diff: 133.041, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 7018.1
#   number of spread. activ. pulses: 13
#   running time: 1616

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99948925   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98912233   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98686886   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9701307   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91376   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89999527   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.8999895   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.8999843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   9   0.8999713   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.8999674   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   11   0.8999658   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.89996475   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   13   0.8999625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.8999619   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.89995813   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.8999556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.89995265   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.8999488   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.89994437   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.8999371   REFERENCES:SIMLOC
