###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.354, activation diff: 5.354, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 16.886, activation diff: 15.351, ratio: 0.909
#   pulse 3: activated nodes: 8062, borderline nodes: 6554, overall activation: 19.553, activation diff: 5.790, ratio: 0.296
#   pulse 4: activated nodes: 8647, borderline nodes: 7128, overall activation: 125.011, activation diff: 106.074, ratio: 0.849
#   pulse 5: activated nodes: 9226, borderline nodes: 5981, overall activation: 262.623, activation diff: 137.677, ratio: 0.524
#   pulse 6: activated nodes: 10655, borderline nodes: 6645, overall activation: 745.470, activation diff: 482.856, ratio: 0.648
#   pulse 7: activated nodes: 11090, borderline nodes: 4660, overall activation: 1373.196, activation diff: 627.726, ratio: 0.457
#   pulse 8: activated nodes: 11264, borderline nodes: 3034, overall activation: 2130.579, activation diff: 757.382, ratio: 0.355
#   pulse 9: activated nodes: 11348, borderline nodes: 1533, overall activation: 2854.479, activation diff: 723.901, ratio: 0.254
#   pulse 10: activated nodes: 11405, borderline nodes: 718, overall activation: 3477.000, activation diff: 622.520, ratio: 0.179
#   pulse 11: activated nodes: 11421, borderline nodes: 401, overall activation: 3973.396, activation diff: 496.397, ratio: 0.125
#   pulse 12: activated nodes: 11426, borderline nodes: 276, overall activation: 4348.875, activation diff: 375.479, ratio: 0.086
#   pulse 13: activated nodes: 11429, borderline nodes: 198, overall activation: 4622.787, activation diff: 273.912, ratio: 0.059
#   pulse 14: activated nodes: 11432, borderline nodes: 165, overall activation: 4818.040, activation diff: 195.252, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11432
#   final overall activation: 4818.0
#   number of spread. activ. pulses: 14
#   running time: 1535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99751747   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9814198   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9805347   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9616387   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9112432   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8811133   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74823034   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74762917   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.7472168   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.74699795   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7467668   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   12   0.74670374   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.7466296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.74657536   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.7464307   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.7464236   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.7462583   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   18   0.74595225   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   19   0.74591655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   20   0.7458745   REFERENCES:SIMLOC
