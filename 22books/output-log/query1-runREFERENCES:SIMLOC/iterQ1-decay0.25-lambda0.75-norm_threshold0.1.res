###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.677, activation diff: 2.677, ratio: 0.472
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 5.178, activation diff: 1.690, ratio: 0.326
#   pulse 3: activated nodes: 7483, borderline nodes: 7244, overall activation: 6.245, activation diff: 2.792, ratio: 0.447
#   pulse 4: activated nodes: 7763, borderline nodes: 6938, overall activation: 7.915, activation diff: 3.149, ratio: 0.398
#   pulse 5: activated nodes: 7830, borderline nodes: 6834, overall activation: 11.622, activation diff: 4.870, ratio: 0.419
#   pulse 6: activated nodes: 7966, borderline nodes: 6651, overall activation: 19.896, activation diff: 9.110, ratio: 0.458
#   pulse 7: activated nodes: 8599, borderline nodes: 6839, overall activation: 38.954, activation diff: 19.566, ratio: 0.502
#   pulse 8: activated nodes: 8967, borderline nodes: 6580, overall activation: 81.211, activation diff: 42.444, ratio: 0.523
#   pulse 9: activated nodes: 9747, borderline nodes: 6658, overall activation: 166.145, activation diff: 84.995, ratio: 0.512
#   pulse 10: activated nodes: 10482, borderline nodes: 6363, overall activation: 310.240, activation diff: 144.104, ratio: 0.464
#   pulse 11: activated nodes: 10833, borderline nodes: 5522, overall activation: 521.752, activation diff: 211.514, ratio: 0.405
#   pulse 12: activated nodes: 11121, borderline nodes: 4611, overall activation: 798.479, activation diff: 276.727, ratio: 0.347
#   pulse 13: activated nodes: 11222, borderline nodes: 3473, overall activation: 1129.232, activation diff: 330.754, ratio: 0.293
#   pulse 14: activated nodes: 11295, borderline nodes: 2519, overall activation: 1494.942, activation diff: 365.709, ratio: 0.245
#   pulse 15: activated nodes: 11335, borderline nodes: 1826, overall activation: 1875.102, activation diff: 380.160, ratio: 0.203

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 1875.1
#   number of spread. activ. pulses: 15
#   running time: 1505

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8147129   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.68316084   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.65344936   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.65133536   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.59527266   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.58224684   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   7   0.58152866   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   8   0.5806602   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   9   0.5770466   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   10   0.5768187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.57626885   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.5733832   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.5730982   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.5726242   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.56662273   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   16   0.56599545   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   17   0.5659917   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.5638059   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.5630353   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.5600993   REFERENCES:SIMLOC
