###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.908, activation diff: 6.908, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 124.727, activation diff: 119.616, ratio: 0.959
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 232.354, activation diff: 108.531, ratio: 0.467
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1115.998, activation diff: 883.655, ratio: 0.792
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2251.594, activation diff: 1135.596, ratio: 0.504
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3684.384, activation diff: 1432.790, ratio: 0.389
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 4956.061, activation diff: 1271.677, ratio: 0.257
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 5926.251, activation diff: 970.190, ratio: 0.164
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 6610.501, activation diff: 684.250, ratio: 0.104
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 7070.548, activation diff: 460.048, ratio: 0.065
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 7370.577, activation diff: 300.028, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7370.6
#   number of spread. activ. pulses: 11
#   running time: 1401

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959846   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9813575   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9806725   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96266437   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9168397   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.896188   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89600396   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.8953114   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.89518654   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.89514434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.89513326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89503455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   13   0.8949189   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   14   0.8948221   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.89472914   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   16   0.894621   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.89461946   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.8946159   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8945526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.8945476   REFERENCES:SIMLOC
