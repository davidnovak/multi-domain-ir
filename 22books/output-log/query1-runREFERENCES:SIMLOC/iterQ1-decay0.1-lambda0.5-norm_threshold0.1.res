###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.354, activation diff: 5.354, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 19.574, activation diff: 18.040, ratio: 0.922
#   pulse 3: activated nodes: 8062, borderline nodes: 6554, overall activation: 23.158, activation diff: 6.862, ratio: 0.296
#   pulse 4: activated nodes: 8740, borderline nodes: 7214, overall activation: 179.769, activation diff: 157.174, ratio: 0.874
#   pulse 5: activated nodes: 9335, borderline nodes: 5823, overall activation: 388.421, activation diff: 208.699, ratio: 0.537
#   pulse 6: activated nodes: 10818, borderline nodes: 6388, overall activation: 1135.114, activation diff: 746.698, ratio: 0.658
#   pulse 7: activated nodes: 11160, borderline nodes: 3814, overall activation: 2137.516, activation diff: 1002.402, ratio: 0.469
#   pulse 8: activated nodes: 11317, borderline nodes: 2159, overall activation: 3344.988, activation diff: 1207.472, ratio: 0.361
#   pulse 9: activated nodes: 11404, borderline nodes: 787, overall activation: 4480.472, activation diff: 1135.484, ratio: 0.253
#   pulse 10: activated nodes: 11431, borderline nodes: 324, overall activation: 5415.833, activation diff: 935.361, ratio: 0.173
#   pulse 11: activated nodes: 11436, borderline nodes: 170, overall activation: 6115.574, activation diff: 699.742, ratio: 0.114
#   pulse 12: activated nodes: 11440, borderline nodes: 78, overall activation: 6612.652, activation diff: 497.078, ratio: 0.075
#   pulse 13: activated nodes: 11443, borderline nodes: 53, overall activation: 6953.783, activation diff: 341.131, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 6953.8
#   number of spread. activ. pulses: 13
#   running time: 1482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99623275   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9784553   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9774631   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.95977736   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9105414   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.8968735   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.896784   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   8   0.8961006   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.89585626   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8955649   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.89555484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.89553297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8954869   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.895475   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8954481   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   16   0.89532876   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   17   0.89529127   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.89526594   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8952577   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   20   0.8951939   REFERENCES:SIMLOC
