###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.677, activation diff: 2.677, ratio: 0.472
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 5.191, activation diff: 1.703, ratio: 0.328
#   pulse 3: activated nodes: 7483, borderline nodes: 7244, overall activation: 6.383, activation diff: 2.918, ratio: 0.457
#   pulse 4: activated nodes: 7763, borderline nodes: 6938, overall activation: 8.238, activation diff: 3.328, ratio: 0.404
#   pulse 5: activated nodes: 7832, borderline nodes: 6833, overall activation: 12.508, activation diff: 5.424, ratio: 0.434
#   pulse 6: activated nodes: 7970, borderline nodes: 6629, overall activation: 22.253, activation diff: 10.562, ratio: 0.475
#   pulse 7: activated nodes: 8625, borderline nodes: 6800, overall activation: 45.246, activation diff: 23.473, ratio: 0.519
#   pulse 8: activated nodes: 9062, borderline nodes: 6604, overall activation: 96.704, activation diff: 51.620, ratio: 0.534
#   pulse 9: activated nodes: 9812, borderline nodes: 6557, overall activation: 199.477, activation diff: 102.818, ratio: 0.515
#   pulse 10: activated nodes: 10546, borderline nodes: 6255, overall activation: 372.568, activation diff: 173.100, ratio: 0.465
#   pulse 11: activated nodes: 10988, borderline nodes: 5260, overall activation: 625.889, activation diff: 253.322, ratio: 0.405
#   pulse 12: activated nodes: 11166, borderline nodes: 4206, overall activation: 958.164, activation diff: 332.275, ratio: 0.347
#   pulse 13: activated nodes: 11244, borderline nodes: 3040, overall activation: 1354.772, activation diff: 396.608, ratio: 0.293
#   pulse 14: activated nodes: 11315, borderline nodes: 2158, overall activation: 1790.467, activation diff: 435.695, ratio: 0.243
#   pulse 15: activated nodes: 11357, borderline nodes: 1401, overall activation: 2240.763, activation diff: 450.296, ratio: 0.201

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11357
#   final overall activation: 2240.8
#   number of spread. activ. pulses: 15
#   running time: 1532

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.82672286   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.70230854   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.66952014   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.6669582   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.6481344   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.63591737   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   7   0.63309646   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.62951416   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   9   0.6288509   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   10   0.6282964   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.62779385   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   12   0.62598187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.62469804   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.6245307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.61875284   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.61646366   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.61469406   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.61455864   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.612822   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.61221343   REFERENCES:SIMLOC
