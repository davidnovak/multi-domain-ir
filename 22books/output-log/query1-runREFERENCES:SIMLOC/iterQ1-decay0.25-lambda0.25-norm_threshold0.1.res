###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.031, activation diff: 8.031, ratio: 1.596
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 57.739, activation diff: 58.650, ratio: 1.016
#   pulse 3: activated nodes: 8369, borderline nodes: 6075, overall activation: 28.604, activation diff: 54.402, ratio: 1.902
#   pulse 4: activated nodes: 10058, borderline nodes: 7628, overall activation: 770.715, activation diff: 756.575, ratio: 0.982
#   pulse 5: activated nodes: 10432, borderline nodes: 4944, overall activation: 799.904, activation diff: 319.192, ratio: 0.399
#   pulse 6: activated nodes: 11222, borderline nodes: 4341, overall activation: 2308.071, activation diff: 1513.744, ratio: 0.656
#   pulse 7: activated nodes: 11323, borderline nodes: 1718, overall activation: 3211.294, activation diff: 903.235, ratio: 0.281
#   pulse 8: activated nodes: 11400, borderline nodes: 796, overall activation: 4020.168, activation diff: 808.874, ratio: 0.201
#   pulse 9: activated nodes: 11417, borderline nodes: 362, overall activation: 4527.334, activation diff: 507.166, ratio: 0.112
#   pulse 10: activated nodes: 11421, borderline nodes: 242, overall activation: 4827.305, activation diff: 299.971, ratio: 0.062
#   pulse 11: activated nodes: 11424, borderline nodes: 190, overall activation: 4997.997, activation diff: 170.692, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11424
#   final overall activation: 4998.0
#   number of spread. activ. pulses: 11
#   running time: 1485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999379   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9893568   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98632115   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9697654   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9186425   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89696467   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7498574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7495165   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.74950594   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7494063   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.74940306   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7493659   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7493654   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.749321   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.74929094   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7492812   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.74925953   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.7492308   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.7491838   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.74916416   REFERENCES:SIMLOC
