###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.438, activation diff: 3.438, ratio: 1.000
#   pulse 2: activated nodes: 3967, borderline nodes: 3948, overall activation: 1.747, activation diff: 1.691, ratio: 0.968
#   pulse 3: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.873, activation diff: 0.873, ratio: 1.000
#   pulse 4: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.437, activation diff: 0.437, ratio: 1.000
#   pulse 5: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.218, activation diff: 0.218, ratio: 1.000
#   pulse 6: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.109, activation diff: 0.109, ratio: 1.000
#   pulse 7: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.055, activation diff: 0.055, ratio: 1.000
#   pulse 8: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.027, activation diff: 0.027, ratio: 1.000
#   pulse 9: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.014, activation diff: 0.014, ratio: 1.000
#   pulse 10: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.007, activation diff: 0.007, ratio: 1.000
#   pulse 11: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.003, activation diff: 0.003, ratio: 1.000
#   pulse 12: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.002, activation diff: 0.002, ratio: 1.000
#   pulse 13: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 14: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.000, activation diff: 0.000, ratio: 1.000
#   pulse 15: activated nodes: 3967, borderline nodes: 3948, overall activation: 0.000, activation diff: 0.000, ratio: 1.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3967
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 511

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   3   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   6   3.0517578E-5   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_368   11   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_366   12   0.0   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_375   14   0.0   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_350   16   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_374   17   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_370   18   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_365   19   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_367   20   0.0   REFERENCES:SIMLOC
