###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.363, activation diff: 10.363, ratio: 1.407
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 270.941, activation diff: 269.317, ratio: 0.994
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 350.545, activation diff: 197.893, ratio: 0.565
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2625.893, activation diff: 2275.449, ratio: 0.867
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4201.719, activation diff: 1575.826, ratio: 0.375
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 5977.055, activation diff: 1775.336, ratio: 0.297
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 6965.390, activation diff: 988.335, ratio: 0.142
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 7461.609, activation diff: 496.219, ratio: 0.067
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 7694.749, activation diff: 233.140, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7694.7
#   number of spread. activ. pulses: 9
#   running time: 1366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99935305   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9899843   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9885167   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97352195   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9283163   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90778244   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.899791   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   8   0.89969355   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8996819   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   10   0.89967453   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.89967316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   12   0.89967096   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   13   0.89966446   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.89965093   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.89964443   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.8996395   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.89963484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8996309   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.8996301   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.89962566   REFERENCES:SIMLOC
