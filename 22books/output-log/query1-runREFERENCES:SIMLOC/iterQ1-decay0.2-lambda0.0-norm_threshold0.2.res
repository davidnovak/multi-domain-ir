###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.901, activation diff: 7.901, ratio: 4.156
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 19.585, activation diff: 21.486, ratio: 1.097
#   pulse 3: activated nodes: 5193, borderline nodes: 4383, overall activation: 3.391, activation diff: 22.975, ratio: 6.776
#   pulse 4: activated nodes: 6849, borderline nodes: 6031, overall activation: 371.307, activation diff: 374.697, ratio: 1.009
#   pulse 5: activated nodes: 7956, borderline nodes: 5143, overall activation: 85.051, activation diff: 456.357, ratio: 5.366
#   pulse 6: activated nodes: 10695, borderline nodes: 7385, overall activation: 2389.235, activation diff: 2474.013, ratio: 1.035
#   pulse 7: activated nodes: 10949, borderline nodes: 3917, overall activation: 571.357, activation diff: 2955.902, ratio: 5.173
#   pulse 8: activated nodes: 11308, borderline nodes: 2850, overall activation: 3650.428, activation diff: 4124.237, ratio: 1.130
#   pulse 9: activated nodes: 11395, borderline nodes: 1125, overall activation: 1092.906, activation diff: 4209.287, ratio: 3.851
#   pulse 10: activated nodes: 11412, borderline nodes: 819, overall activation: 4208.097, activation diff: 4259.049, ratio: 1.012
#   pulse 11: activated nodes: 11419, borderline nodes: 577, overall activation: 3510.689, activation diff: 2072.950, ratio: 0.590
#   pulse 12: activated nodes: 11422, borderline nodes: 521, overall activation: 4928.826, activation diff: 1647.372, ratio: 0.334
#   pulse 13: activated nodes: 11425, borderline nodes: 380, overall activation: 5081.769, activation diff: 270.023, ratio: 0.053
#   pulse 14: activated nodes: 11425, borderline nodes: 308, overall activation: 5208.260, activation diff: 140.067, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11425
#   final overall activation: 5208.3
#   number of spread. activ. pulses: 14
#   running time: 1579

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999478   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98909456   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9861557   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96935445   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9125787   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89104784   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999772   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79988736   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7998296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7997861   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   11   0.79978055   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.79974556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.79973984   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.79973596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.79972214   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7997073   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.7996983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.79969543   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.799685   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.79967415   REFERENCES:SIMLOC
