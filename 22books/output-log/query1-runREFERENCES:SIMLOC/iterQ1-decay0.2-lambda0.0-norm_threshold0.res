###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.817, activation diff: 13.817, ratio: 1.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 421.409, activation diff: 428.933, ratio: 1.018
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 239.788, activation diff: 630.473, ratio: 2.629
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3523.161, activation diff: 3621.693, ratio: 1.028
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2096.215, activation diff: 3458.062, ratio: 1.650
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 5409.814, activation diff: 3870.307, ratio: 0.715
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 5860.837, activation diff: 764.770, ratio: 0.130
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 6220.193, activation diff: 380.688, ratio: 0.061
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 6299.008, activation diff: 84.347, ratio: 0.013

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6299.0
#   number of spread. activ. pulses: 9
#   running time: 1427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995901   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9910853   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9896028   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9752567   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.929216   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.91065997   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999427   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.79988927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.79988647   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7998771   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.79986286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.79986066   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7998535   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7998523   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7998469   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.79983544   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7998231   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7998118   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7998039   REFERENCES:SIMLOC
