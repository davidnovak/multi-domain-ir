###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.926, activation diff: 5.926, ratio: 2.025
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 4.305, activation diff: 5.529, ratio: 1.284
#   pulse 3: activated nodes: 4905, borderline nodes: 4571, overall activation: 1.249, activation diff: 3.262, ratio: 2.613
#   pulse 4: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.349, activation diff: 0.900, ratio: 2.580
#   pulse 5: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.087, activation diff: 0.262, ratio: 3.000
#   pulse 6: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.022, activation diff: 0.065, ratio: 3.000
#   pulse 7: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.005, activation diff: 0.016, ratio: 3.000
#   pulse 8: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.001, activation diff: 0.004, ratio: 3.000
#   pulse 9: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.001, ratio: 3.000
#   pulse 10: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 11: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 12: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 13: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 14: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 15: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4905
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 713

###################################
# top k results in TREC format: 

1   Q1   generalhistoryfo00myerrich_787   1   1.6981033E-9   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   1.3347319E-9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   3   1.3233912E-9   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   1.1104683E-9   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_241   5   9.870447E-10   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_18   6   9.870447E-10   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_14   7   9.870447E-10   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_147   8   9.870447E-10   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_119   9   9.870447E-10   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_497   10   9.870447E-10   REFERENCES:SIMLOC
1   Q1   graduateschoolof9192bran_62   11   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_134   12   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_136   13   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_137   14   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_164   15   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_151   16   9.870447E-10   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_46   17   9.870447E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_281   18   9.870447E-10   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_0   19   9.870447E-10   REFERENCES:SIMLOC
1   Q1   graduateschoolof9192bran_102   20   9.870447E-10   REFERENCES:SIMLOC
