###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.817, activation diff: 13.817, ratio: 1.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 395.224, activation diff: 402.748, ratio: 1.019
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 207.353, activation diff: 574.514, ratio: 2.771
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3118.126, activation diff: 3204.822, ratio: 1.028
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1715.642, activation diff: 3080.484, ratio: 1.796
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 4713.488, activation diff: 3510.948, ratio: 0.745
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 5087.688, activation diff: 731.076, ratio: 0.144
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 5466.495, activation diff: 399.297, ratio: 0.073
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 5549.152, activation diff: 87.956, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5549.2
#   number of spread. activ. pulses: 9
#   running time: 1423

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99957937   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99107414   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9892358   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9748394   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92819136   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9098835   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7499668   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74982387   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.74976844   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7497322   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7497176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7496747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.749668   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7496605   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.7496487   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.74961877   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.749612   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7495668   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.7495515   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7495052   REFERENCES:SIMLOC
