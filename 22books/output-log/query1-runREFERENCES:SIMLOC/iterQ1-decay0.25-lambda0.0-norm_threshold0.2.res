###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.901, activation diff: 7.901, ratio: 4.156
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 18.386, activation diff: 20.287, ratio: 1.103
#   pulse 3: activated nodes: 5193, borderline nodes: 4383, overall activation: 3.146, activation diff: 21.532, ratio: 6.844
#   pulse 4: activated nodes: 6845, borderline nodes: 6028, overall activation: 318.415, activation diff: 321.561, ratio: 1.010
#   pulse 5: activated nodes: 7928, borderline nodes: 5212, overall activation: 66.264, activation diff: 384.679, ratio: 5.805
#   pulse 6: activated nodes: 10626, borderline nodes: 7492, overall activation: 2041.177, activation diff: 2107.286, ratio: 1.032
#   pulse 7: activated nodes: 10887, borderline nodes: 4182, overall activation: 446.686, activation diff: 2486.190, ratio: 5.566
#   pulse 8: activated nodes: 11285, borderline nodes: 3209, overall activation: 3085.412, activation diff: 3480.249, ratio: 1.128
#   pulse 9: activated nodes: 11365, borderline nodes: 1398, overall activation: 738.962, activation diff: 3632.460, ratio: 4.916
#   pulse 10: activated nodes: 11391, borderline nodes: 1129, overall activation: 3458.997, activation diff: 3752.570, ratio: 1.085
#   pulse 11: activated nodes: 11409, borderline nodes: 814, overall activation: 1731.353, activation diff: 2934.640, ratio: 1.695
#   pulse 12: activated nodes: 11409, borderline nodes: 756, overall activation: 3897.020, activation diff: 2706.798, ratio: 0.695
#   pulse 13: activated nodes: 11412, borderline nodes: 646, overall activation: 4006.512, activation diff: 575.464, ratio: 0.144
#   pulse 14: activated nodes: 11416, borderline nodes: 584, overall activation: 4308.688, activation diff: 336.351, ratio: 0.078
#   pulse 15: activated nodes: 11419, borderline nodes: 462, overall activation: 4367.517, activation diff: 73.552, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11419
#   final overall activation: 4367.5
#   number of spread. activ. pulses: 15
#   running time: 1617

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994481   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98904127   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98524386   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96864426   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9099448   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88850796   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74994874   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7496072   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7495563   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7495029   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7493869   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.74932915   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.7493261   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7493043   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.7493   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.74919844   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.7491721   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.7491717   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.7491458   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.74913085   REFERENCES:SIMLOC
