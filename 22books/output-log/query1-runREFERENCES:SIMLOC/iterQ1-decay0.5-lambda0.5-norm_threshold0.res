###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.908, activation diff: 6.908, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 71.986, activation diff: 66.875, ratio: 0.929
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 117.881, activation diff: 46.804, ratio: 0.397
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 404.014, activation diff: 286.155, ratio: 0.708
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 732.900, activation diff: 328.886, ratio: 0.449
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1091.269, activation diff: 358.368, ratio: 0.328
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1407.122, activation diff: 315.853, ratio: 0.224
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 1661.551, activation diff: 254.429, ratio: 0.153
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 1855.153, activation diff: 193.602, ratio: 0.104
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 1998.088, activation diff: 142.934, ratio: 0.072
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 2102.389, activation diff: 104.302, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2102.4
#   number of spread. activ. pulses: 11
#   running time: 1443

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99472404   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9728379   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9721311   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9480115   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9025519   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8565874   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49431336   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49314487   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49178004   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49171042   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.49116206   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.48923522   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.48911119   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.4885478   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.48838058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   16   0.48817307   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.48815084   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.4879375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.487884   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.48753643   REFERENCES:SIMLOC
