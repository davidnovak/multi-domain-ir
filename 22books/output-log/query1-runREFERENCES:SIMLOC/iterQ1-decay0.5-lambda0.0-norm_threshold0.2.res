###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.901, activation diff: 7.901, ratio: 4.156
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 12.393, activation diff: 14.294, ratio: 1.153
#   pulse 3: activated nodes: 5193, borderline nodes: 4383, overall activation: 1.948, activation diff: 14.341, ratio: 7.362
#   pulse 4: activated nodes: 6117, borderline nodes: 5304, overall activation: 106.186, activation diff: 108.134, ratio: 1.018
#   pulse 5: activated nodes: 6886, borderline nodes: 4722, overall activation: 16.449, activation diff: 122.635, ratio: 7.456
#   pulse 6: activated nodes: 8871, borderline nodes: 6652, overall activation: 845.314, activation diff: 861.762, ratio: 1.019
#   pulse 7: activated nodes: 9441, borderline nodes: 4909, overall activation: 83.943, activation diff: 929.245, ratio: 11.070
#   pulse 8: activated nodes: 10126, borderline nodes: 5357, overall activation: 1330.984, activation diff: 1413.528, ratio: 1.062
#   pulse 9: activated nodes: 10248, borderline nodes: 4781, overall activation: 116.673, activation diff: 1445.706, ratio: 12.391
#   pulse 10: activated nodes: 10356, borderline nodes: 4782, overall activation: 1406.961, activation diff: 1518.817, ratio: 1.080
#   pulse 11: activated nodes: 10372, borderline nodes: 4694, overall activation: 124.461, activation diff: 1524.202, ratio: 12.246
#   pulse 12: activated nodes: 10381, borderline nodes: 4684, overall activation: 1419.533, activation diff: 1534.890, ratio: 1.081
#   pulse 13: activated nodes: 10386, borderline nodes: 4652, overall activation: 131.248, activation diff: 1535.583, ratio: 11.700
#   pulse 14: activated nodes: 10400, borderline nodes: 4654, overall activation: 1434.287, activation diff: 1543.369, ratio: 1.076
#   pulse 15: activated nodes: 10405, borderline nodes: 4636, overall activation: 156.257, activation diff: 1544.731, ratio: 9.886

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10405
#   final overall activation: 156.3
#   number of spread. activ. pulses: 15
#   running time: 1509

###################################
# top k results in TREC format: 

1   Q1   bookman44unkngoog_700   1   0.08586375   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_4   2   0.052453943   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_73   3   0.052453943   REFERENCES:SIMLOC
1   Q1   timetable1974752univ_86   4   0.052453943   REFERENCES:SIMLOC
1   Q1   timetable1974752univ_88   5   0.052453943   REFERENCES:SIMLOC
1   Q1   youngmidshipman00morrgoog_44   6   0.052453943   REFERENCES:SIMLOC
1   Q1   youngmidshipman00morrgoog_42   7   0.052453943   REFERENCES:SIMLOC
1   Q1   youngmidshipman00morrgoog_26   8   0.052453943   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_18   9   0.052453943   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_49   10   0.052453943   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_9   11   0.052453943   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_7   12   0.052453943   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_22   13   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_285   14   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_284   15   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_283   16   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_282   17   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_286   18   0.052453943   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_291   19   0.052453943   REFERENCES:SIMLOC
1   Q1   youngmidshipman00morrgoog_5   20   0.052453943   REFERENCES:SIMLOC
