###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.926, activation diff: 5.926, ratio: 2.025
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 2.770, activation diff: 3.994, ratio: 1.442
#   pulse 3: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.741, activation diff: 2.038, ratio: 2.748
#   pulse 4: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.185, activation diff: 0.556, ratio: 3.000
#   pulse 5: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.046, activation diff: 0.139, ratio: 3.000
#   pulse 6: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.012, activation diff: 0.035, ratio: 3.000
#   pulse 7: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.003, activation diff: 0.009, ratio: 3.000
#   pulse 8: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.001, activation diff: 0.002, ratio: 3.000
#   pulse 9: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.001, ratio: 3.000
#   pulse 10: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 11: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 12: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 13: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 14: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000
#   pulse 15: activated nodes: 4905, borderline nodes: 4571, overall activation: 0.000, activation diff: 0.000, ratio: 3.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4905
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 687

###################################
# top k results in TREC format: 

1   Q1   generalhistoryfo00myerrich_787   1   1.6981033E-9   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   1.3347319E-9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   3   1.3233912E-9   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   1.1104683E-9   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   9.313226E-10   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   9.313226E-10   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_241   7   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_18   8   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_147   9   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_119   10   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_134   11   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_136   12   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_137   13   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_164   14   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_151   15   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   graduateschoolof9192bran_62   16   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_281   17   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_0   18   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_14   19   3.5451403E-10   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_46   20   3.5451403E-10   REFERENCES:SIMLOC
