###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.031, activation diff: 8.031, ratio: 1.596
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 39.180, activation diff: 40.090, ratio: 1.023
#   pulse 3: activated nodes: 8369, borderline nodes: 6075, overall activation: 18.456, activation diff: 35.656, ratio: 1.932
#   pulse 4: activated nodes: 9118, borderline nodes: 6790, overall activation: 383.676, activation diff: 374.367, ratio: 0.976
#   pulse 5: activated nodes: 9577, borderline nodes: 5128, overall activation: 325.103, activation diff: 150.267, ratio: 0.462
#   pulse 6: activated nodes: 10513, borderline nodes: 5709, overall activation: 988.266, activation diff: 666.277, ratio: 0.674
#   pulse 7: activated nodes: 10644, borderline nodes: 4688, overall activation: 1313.825, activation diff: 325.647, ratio: 0.248
#   pulse 8: activated nodes: 10755, borderline nodes: 4403, overall activation: 1632.014, activation diff: 318.190, ratio: 0.195
#   pulse 9: activated nodes: 10811, borderline nodes: 3883, overall activation: 1834.109, activation diff: 202.095, ratio: 0.110
#   pulse 10: activated nodes: 10854, borderline nodes: 3715, overall activation: 1966.659, activation diff: 132.550, ratio: 0.067
#   pulse 11: activated nodes: 10867, borderline nodes: 3663, overall activation: 2052.491, activation diff: 85.831, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10867
#   final overall activation: 2052.5
#   number of spread. activ. pulses: 11
#   running time: 1465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99908197   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9867109   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9815537   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9596827   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90817475   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8754282   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49811366   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4966603   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49580514   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4955362   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4952559   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49392277   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.49310178   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.49274573   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   15   0.49271786   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.49269408   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   17   0.49232697   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.49220765   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.49199533   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49184337   REFERENCES:SIMLOC
