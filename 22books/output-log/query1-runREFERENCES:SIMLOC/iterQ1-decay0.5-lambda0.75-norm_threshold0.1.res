###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.677, activation diff: 2.677, ratio: 0.472
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 5.113, activation diff: 1.626, ratio: 0.318
#   pulse 3: activated nodes: 7483, borderline nodes: 7244, overall activation: 5.552, activation diff: 2.166, ratio: 0.390
#   pulse 4: activated nodes: 7763, borderline nodes: 6938, overall activation: 6.339, activation diff: 2.289, ratio: 0.361
#   pulse 5: activated nodes: 7822, borderline nodes: 6849, overall activation: 7.774, activation diff: 2.646, ratio: 0.340
#   pulse 6: activated nodes: 7903, borderline nodes: 6723, overall activation: 10.588, activation diff: 3.739, ratio: 0.353
#   pulse 7: activated nodes: 7999, borderline nodes: 6569, overall activation: 16.107, activation diff: 6.176, ratio: 0.383
#   pulse 8: activated nodes: 8623, borderline nodes: 6814, overall activation: 27.174, activation diff: 11.471, ratio: 0.422
#   pulse 9: activated nodes: 8813, borderline nodes: 6508, overall activation: 48.969, activation diff: 21.963, ratio: 0.449
#   pulse 10: activated nodes: 9178, borderline nodes: 6364, overall activation: 89.578, activation diff: 40.677, ratio: 0.454
#   pulse 11: activated nodes: 9457, borderline nodes: 5846, overall activation: 156.735, activation diff: 67.173, ratio: 0.429
#   pulse 12: activated nodes: 9829, borderline nodes: 5711, overall activation: 251.560, activation diff: 94.830, ratio: 0.377
#   pulse 13: activated nodes: 10142, borderline nodes: 5459, overall activation: 369.827, activation diff: 118.268, ratio: 0.320
#   pulse 14: activated nodes: 10496, borderline nodes: 5275, overall activation: 504.142, activation diff: 134.315, ratio: 0.266
#   pulse 15: activated nodes: 10596, borderline nodes: 4989, overall activation: 646.640, activation diff: 142.498, ratio: 0.220

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10596
#   final overall activation: 646.6
#   number of spread. activ. pulses: 15
#   running time: 1559

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.6928262   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   2   0.53787684   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.52011335   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.50098526   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   5   0.33592007   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   6   0.3318488   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   7   0.32304555   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   8   0.32189193   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.3179013   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   10   0.3170228   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.31660822   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.3143723   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.3132525   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_148   14   0.31292713   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   15   0.31196454   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_295   16   0.31127885   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.3112622   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_216   18   0.31048957   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_484   19   0.31046337   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_319   20   0.3100497   REFERENCES:SIMLOC
