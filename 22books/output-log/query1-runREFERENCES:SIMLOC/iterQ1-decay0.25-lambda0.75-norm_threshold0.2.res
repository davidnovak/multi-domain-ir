###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.975, activation diff: 1.975, ratio: 0.397
#   pulse 2: activated nodes: 4730, borderline nodes: 4689, overall activation: 3.945, activation diff: 1.251, ratio: 0.317
#   pulse 3: activated nodes: 4730, borderline nodes: 4689, overall activation: 3.039, activation diff: 0.918, ratio: 0.302
#   pulse 4: activated nodes: 4730, borderline nodes: 4689, overall activation: 2.302, activation diff: 0.737, ratio: 0.320
#   pulse 5: activated nodes: 4730, borderline nodes: 4689, overall activation: 1.734, activation diff: 0.568, ratio: 0.327
#   pulse 6: activated nodes: 4730, borderline nodes: 4689, overall activation: 1.301, activation diff: 0.434, ratio: 0.333
#   pulse 7: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.976, activation diff: 0.325, ratio: 0.333
#   pulse 8: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.732, activation diff: 0.244, ratio: 0.333
#   pulse 9: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.549, activation diff: 0.183, ratio: 0.333
#   pulse 10: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.412, activation diff: 0.137, ratio: 0.333
#   pulse 11: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.309, activation diff: 0.103, ratio: 0.333
#   pulse 12: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.232, activation diff: 0.077, ratio: 0.333
#   pulse 13: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.174, activation diff: 0.058, ratio: 0.333
#   pulse 14: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.130, activation diff: 0.043, ratio: 0.333
#   pulse 15: activated nodes: 4730, borderline nodes: 4689, overall activation: 0.098, activation diff: 0.033, ratio: 0.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4730
#   final overall activation: 0.1
#   number of spread. activ. pulses: 15
#   running time: 688

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   0.013363461   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.013363461   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_239   3   0.013363461   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.013363461   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   0.013363461   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   6   0.013363461   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES:SIMLOC
1   Q1   miningcampsstudy00shin_200   11   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_368   12   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_367   13   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_365   14   0.0   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_197   15   0.0   REFERENCES:SIMLOC
1   Q1   miningcampsstudy00shin_206   16   0.0   REFERENCES:SIMLOC
1   Q1   miningcampsstudy00shin_205   17   0.0   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_366   18   0.0   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_191   19   0.0   REFERENCES:SIMLOC
1   Q1   miningcampsstudy00shin_202   20   0.0   REFERENCES:SIMLOC
