###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.454, activation diff: 3.454, ratio: 0.535
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 36.193, activation diff: 31.685, ratio: 0.875
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 80.071, activation diff: 45.184, ratio: 0.564
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 218.853, activation diff: 139.353, ratio: 0.637
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 482.854, activation diff: 264.098, ratio: 0.547
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 910.837, activation diff: 427.983, ratio: 0.470
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 1487.567, activation diff: 576.730, ratio: 0.388
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 2163.609, activation diff: 676.042, ratio: 0.312
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 2877.987, activation diff: 714.379, ratio: 0.248
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 3576.754, activation diff: 698.767, ratio: 0.195
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 4225.230, activation diff: 648.475, ratio: 0.153
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 4806.942, activation diff: 581.712, ratio: 0.121
#   pulse 13: activated nodes: 11457, borderline nodes: 0, overall activation: 5316.707, activation diff: 509.765, ratio: 0.096
#   pulse 14: activated nodes: 11457, borderline nodes: 0, overall activation: 5755.734, activation diff: 439.027, ratio: 0.076
#   pulse 15: activated nodes: 11457, borderline nodes: 0, overall activation: 6128.785, activation diff: 373.051, ratio: 0.061

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6128.8
#   number of spread. activ. pulses: 15
#   running time: 1561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96747327   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9293479   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.92439145   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.8992347   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.8582378   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.8579471   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.85314786   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.851972   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   9   0.85179067   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.8516381   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   11   0.85132754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.85129625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.85124034   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.8512341   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.85068583   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.8501818   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.85005707   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   18   0.849935   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.84981835   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   20   0.84936684   REFERENCES:SIMLOC
