###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.031, activation diff: 8.031, ratio: 1.596
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 61.451, activation diff: 62.362, ratio: 1.015
#   pulse 3: activated nodes: 8369, borderline nodes: 6075, overall activation: 30.882, activation diff: 58.415, ratio: 1.892
#   pulse 4: activated nodes: 10066, borderline nodes: 7626, overall activation: 863.564, activation diff: 848.518, ratio: 0.983
#   pulse 5: activated nodes: 10440, borderline nodes: 4875, overall activation: 925.029, activation diff: 368.461, ratio: 0.398
#   pulse 6: activated nodes: 11236, borderline nodes: 4218, overall activation: 2677.762, activation diff: 1758.409, ratio: 0.657
#   pulse 7: activated nodes: 11339, borderline nodes: 1456, overall activation: 3757.516, activation diff: 1079.784, ratio: 0.287
#   pulse 8: activated nodes: 11408, borderline nodes: 595, overall activation: 4696.799, activation diff: 939.283, ratio: 0.200
#   pulse 9: activated nodes: 11431, borderline nodes: 262, overall activation: 5267.850, activation diff: 571.051, ratio: 0.108
#   pulse 10: activated nodes: 11436, borderline nodes: 134, overall activation: 5592.005, activation diff: 324.155, ratio: 0.058
#   pulse 11: activated nodes: 11439, borderline nodes: 91, overall activation: 5768.862, activation diff: 176.857, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 5768.9
#   number of spread. activ. pulses: 11
#   running time: 1499

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99940306   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9894855   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9868225   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9706539   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9199504   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89846945   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7998799   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79971987   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.79967874   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7996442   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.7996336   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   12   0.7996317   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.79962826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.79962766   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7996254   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.79962206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7996081   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.7995959   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7995844   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7995663   REFERENCES:SIMLOC
