###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.677, activation diff: 2.677, ratio: 0.472
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 5.217, activation diff: 1.729, ratio: 0.331
#   pulse 3: activated nodes: 7483, borderline nodes: 7244, overall activation: 6.661, activation diff: 3.168, ratio: 0.476
#   pulse 4: activated nodes: 7763, borderline nodes: 6938, overall activation: 8.890, activation diff: 3.694, ratio: 0.416
#   pulse 5: activated nodes: 7843, borderline nodes: 6838, overall activation: 14.403, activation diff: 6.648, ratio: 0.462
#   pulse 6: activated nodes: 8002, borderline nodes: 6583, overall activation: 27.467, activation diff: 13.850, ratio: 0.504
#   pulse 7: activated nodes: 8659, borderline nodes: 6733, overall activation: 59.615, activation diff: 32.564, ratio: 0.546
#   pulse 8: activated nodes: 9186, borderline nodes: 6557, overall activation: 132.649, activation diff: 73.164, ratio: 0.552
#   pulse 9: activated nodes: 10202, borderline nodes: 6584, overall activation: 276.800, activation diff: 144.180, ratio: 0.521
#   pulse 10: activated nodes: 10713, borderline nodes: 5876, overall activation: 519.200, activation diff: 242.404, ratio: 0.467
#   pulse 11: activated nodes: 11097, borderline nodes: 4796, overall activation: 875.847, activation diff: 356.647, ratio: 0.407
#   pulse 12: activated nodes: 11218, borderline nodes: 3450, overall activation: 1345.468, activation diff: 469.621, ratio: 0.349
#   pulse 13: activated nodes: 11308, borderline nodes: 2343, overall activation: 1898.994, activation diff: 553.526, ratio: 0.291
#   pulse 14: activated nodes: 11354, borderline nodes: 1491, overall activation: 2498.185, activation diff: 599.191, ratio: 0.240
#   pulse 15: activated nodes: 11403, borderline nodes: 842, overall activation: 3110.380, activation diff: 612.195, ratio: 0.197

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11403
#   final overall activation: 3110.4
#   number of spread. activ. pulses: 15
#   running time: 1543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84539044   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   2   0.75141495   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   3   0.7401609   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   4   0.7364917   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   0.733127   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   6   0.73154724   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.73085296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   8   0.73030746   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7295835   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.72689295   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7256617   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.7246214   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.72335494   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   14   0.7231676   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   15   0.7217052   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.71951145   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.716405   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.71314305   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.71169245   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.71168184   REFERENCES:SIMLOC
