###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.630, activation diff: 10.630, ratio: 2.296
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 87.418, activation diff: 92.048, ratio: 1.053
#   pulse 3: activated nodes: 8514, borderline nodes: 5756, overall activation: 22.925, activation diff: 110.343, ratio: 4.813
#   pulse 4: activated nodes: 9763, borderline nodes: 6852, overall activation: 991.506, activation diff: 1014.308, ratio: 1.023
#   pulse 5: activated nodes: 10166, borderline nodes: 4834, overall activation: 152.894, activation diff: 1143.422, ratio: 7.479
#   pulse 6: activated nodes: 10781, borderline nodes: 4895, overall activation: 1658.808, activation diff: 1793.618, ratio: 1.081
#   pulse 7: activated nodes: 10872, borderline nodes: 4119, overall activation: 261.299, activation diff: 1860.780, ratio: 7.121
#   pulse 8: activated nodes: 10894, borderline nodes: 3947, overall activation: 1847.640, activation diff: 1970.124, ratio: 1.066
#   pulse 9: activated nodes: 10937, borderline nodes: 3756, overall activation: 602.876, activation diff: 1713.307, ratio: 2.842
#   pulse 10: activated nodes: 10938, borderline nodes: 3729, overall activation: 2003.315, activation diff: 1663.973, ratio: 0.831
#   pulse 11: activated nodes: 10940, borderline nodes: 3698, overall activation: 1846.687, activation diff: 479.326, ratio: 0.260
#   pulse 12: activated nodes: 10940, borderline nodes: 3675, overall activation: 2199.516, activation diff: 379.493, ratio: 0.173
#   pulse 13: activated nodes: 10944, borderline nodes: 3639, overall activation: 2239.925, activation diff: 64.755, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10944
#   final overall activation: 2239.9
#   number of spread. activ. pulses: 13
#   running time: 1625

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99927735   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9890317   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9824701   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96365166   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9083384   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8795022   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49978015   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4989037   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49730214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49714544   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.49649665   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49612463   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.4957694   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.49499136   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.49490982   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.49460444   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.49448198   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.49399826   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.4937265   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.49371073   REFERENCES:SIMDATES:SIMLOC
