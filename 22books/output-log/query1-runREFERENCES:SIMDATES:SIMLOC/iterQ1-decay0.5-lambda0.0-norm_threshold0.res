###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.739, activation diff: 13.739, ratio: 1.775
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 264.038, activation diff: 271.361, ratio: 1.028
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 88.688, activation diff: 334.171, ratio: 3.768
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 1538.209, activation diff: 1574.542, ratio: 1.024
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 536.403, activation diff: 1488.241, ratio: 2.774
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 2056.104, activation diff: 1729.003, ratio: 0.841
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2016.178, activation diff: 426.267, ratio: 0.211
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2343.081, activation diff: 340.519, ratio: 0.145
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2398.339, activation diff: 64.736, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2398.3
#   number of spread. activ. pulses: 9
#   running time: 1404

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99941874   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99037325   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98565966   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.968703   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9196269   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8961412   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49981958   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4990519   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49778363   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4974976   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.49712723   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49676704   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.4964024   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.4959681   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   15   0.49552837   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.49536833   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.49529943   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.4949532   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.49485403   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.4945698   REFERENCES:SIMDATES:SIMLOC
