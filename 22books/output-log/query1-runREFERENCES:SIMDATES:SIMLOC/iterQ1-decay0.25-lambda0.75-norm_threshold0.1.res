###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.658, activation diff: 2.658, ratio: 0.470
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 5.149, activation diff: 1.682, ratio: 0.327
#   pulse 3: activated nodes: 7513, borderline nodes: 7276, overall activation: 6.207, activation diff: 2.789, ratio: 0.449
#   pulse 4: activated nodes: 7764, borderline nodes: 6941, overall activation: 7.868, activation diff: 3.141, ratio: 0.399
#   pulse 5: activated nodes: 7833, borderline nodes: 6840, overall activation: 11.553, activation diff: 4.844, ratio: 0.419
#   pulse 6: activated nodes: 7964, borderline nodes: 6651, overall activation: 19.776, activation diff: 9.051, ratio: 0.458
#   pulse 7: activated nodes: 8599, borderline nodes: 6844, overall activation: 38.699, activation diff: 19.428, ratio: 0.502
#   pulse 8: activated nodes: 8965, borderline nodes: 6583, overall activation: 80.687, activation diff: 42.172, ratio: 0.523
#   pulse 9: activated nodes: 9710, borderline nodes: 6640, overall activation: 165.199, activation diff: 84.573, ratio: 0.512
#   pulse 10: activated nodes: 10474, borderline nodes: 6368, overall activation: 308.970, activation diff: 143.780, ratio: 0.465
#   pulse 11: activated nodes: 10835, borderline nodes: 5571, overall activation: 520.386, activation diff: 211.418, ratio: 0.406
#   pulse 12: activated nodes: 11121, borderline nodes: 4769, overall activation: 797.486, activation diff: 277.100, ratio: 0.347
#   pulse 13: activated nodes: 11226, borderline nodes: 3669, overall activation: 1129.629, activation diff: 332.143, ratio: 0.294
#   pulse 14: activated nodes: 11294, borderline nodes: 2656, overall activation: 1497.576, activation diff: 367.947, ratio: 0.246
#   pulse 15: activated nodes: 11336, borderline nodes: 1931, overall activation: 1880.019, activation diff: 382.443, ratio: 0.203

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 1880.0
#   number of spread. activ. pulses: 15
#   running time: 1686

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8138044   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.6821215   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.6523026   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.6502237   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.59563553   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.5844226   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   7   0.581177   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   8   0.58033395   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   9   0.5785413   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.57794535   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   11   0.5761477   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.5741823   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   13   0.57372504   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.5727154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.5665688   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.56629264   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   17   0.5657065   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.56389767   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.56344956   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.5596552   REFERENCES:SIMDATES:SIMLOC
