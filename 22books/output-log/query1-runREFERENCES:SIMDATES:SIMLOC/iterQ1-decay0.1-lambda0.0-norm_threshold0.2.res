###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.824, activation diff: 7.824, ratio: 4.290
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 21.897, activation diff: 23.721, ratio: 1.083
#   pulse 3: activated nodes: 5198, borderline nodes: 4392, overall activation: 3.857, activation diff: 25.755, ratio: 6.677
#   pulse 4: activated nodes: 7021, borderline nodes: 6205, overall activation: 483.087, activation diff: 486.944, ratio: 1.008
#   pulse 5: activated nodes: 8166, borderline nodes: 5178, overall activation: 128.978, activation diff: 612.064, ratio: 4.746
#   pulse 6: activated nodes: 10869, borderline nodes: 7342, overall activation: 3214.188, activation diff: 3342.028, ratio: 1.040
#   pulse 7: activated nodes: 11073, borderline nodes: 3579, overall activation: 777.125, activation diff: 3978.564, ratio: 5.120
#   pulse 8: activated nodes: 11340, borderline nodes: 2268, overall activation: 4985.450, activation diff: 5506.169, ratio: 1.104
#   pulse 9: activated nodes: 11413, borderline nodes: 745, overall activation: 2159.788, activation diff: 4980.576, ratio: 2.306
#   pulse 10: activated nodes: 11429, borderline nodes: 486, overall activation: 6058.538, activation diff: 4731.842, ratio: 0.781
#   pulse 11: activated nodes: 11438, borderline nodes: 314, overall activation: 6257.826, activation diff: 1015.150, ratio: 0.162
#   pulse 12: activated nodes: 11438, borderline nodes: 233, overall activation: 6832.521, activation diff: 608.787, ratio: 0.089
#   pulse 13: activated nodes: 11440, borderline nodes: 163, overall activation: 6929.064, activation diff: 117.012, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6929.1
#   number of spread. activ. pulses: 13
#   running time: 1628

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994899   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891215   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9868775   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9701345   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9138409   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8999967   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.8999917   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.899985   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.89997613   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.89997613   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.8999723   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   12   0.899971   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   13   0.8999677   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.8999659   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.8999614   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.89996004   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.89996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8999536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.89995253   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.8999499   REFERENCES:SIMDATES:SIMLOC
