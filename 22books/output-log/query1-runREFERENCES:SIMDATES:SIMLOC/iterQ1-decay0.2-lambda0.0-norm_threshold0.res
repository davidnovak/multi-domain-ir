###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.739, activation diff: 13.739, ratio: 1.775
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 420.964, activation diff: 428.287, ratio: 1.017
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 234.124, activation diff: 619.915, ratio: 2.648
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3545.942, activation diff: 3617.832, ratio: 1.020
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2169.625, activation diff: 3275.697, ratio: 1.510
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5459.714, activation diff: 3680.444, ratio: 0.674
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5864.878, activation diff: 641.507, ratio: 0.109
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 6191.655, activation diff: 332.987, ratio: 0.054
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6264.027, activation diff: 75.041, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6264.0
#   number of spread. activ. pulses: 9
#   running time: 1454

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995911   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9910843   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9896286   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97527385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9293171   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.91073203   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999932   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79996467   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.79993933   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7999271   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.79991806   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   12   0.79990643   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.79990256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.7998917   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.79988533   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7998757   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.79987085   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.79987013   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7998623   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.7998608   REFERENCES:SIMDATES:SIMLOC
