###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.305, activation diff: 10.305, ratio: 1.411
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 241.079, activation diff: 239.389, ratio: 0.993
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 292.172, activation diff: 155.126, ratio: 0.531
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2092.820, activation diff: 1800.710, ratio: 0.860
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3256.324, activation diff: 1163.505, ratio: 0.357
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 4646.233, activation diff: 1389.908, ratio: 0.299
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5454.631, activation diff: 808.399, ratio: 0.148
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5884.960, activation diff: 430.329, ratio: 0.073
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6100.871, activation diff: 215.912, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6100.9
#   number of spread. activ. pulses: 9
#   running time: 1343

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932337   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9898372   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98787886   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9724959   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9266686   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9061291   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7997967   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.79965556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.79956037   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7995345   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7995274   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.79952526   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.7995107   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.7995026   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7994868   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.7994801   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.79946804   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.79946387   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.79942507   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   20   0.7994181   REFERENCES:SIMDATES:SIMLOC
