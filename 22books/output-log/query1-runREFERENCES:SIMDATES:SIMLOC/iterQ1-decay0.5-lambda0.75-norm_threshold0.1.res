###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.658, activation diff: 2.658, ratio: 0.470
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 5.084, activation diff: 1.618, ratio: 0.318
#   pulse 3: activated nodes: 7513, borderline nodes: 7276, overall activation: 5.516, activation diff: 2.164, ratio: 0.392
#   pulse 4: activated nodes: 7764, borderline nodes: 6941, overall activation: 6.298, activation diff: 2.284, ratio: 0.363
#   pulse 5: activated nodes: 7825, borderline nodes: 6853, overall activation: 7.724, activation diff: 2.632, ratio: 0.341
#   pulse 6: activated nodes: 7903, borderline nodes: 6726, overall activation: 10.519, activation diff: 3.711, ratio: 0.353
#   pulse 7: activated nodes: 7999, borderline nodes: 6571, overall activation: 15.992, activation diff: 6.124, ratio: 0.383
#   pulse 8: activated nodes: 8619, borderline nodes: 6816, overall activation: 26.963, activation diff: 11.370, ratio: 0.422
#   pulse 9: activated nodes: 8812, borderline nodes: 6515, overall activation: 48.583, activation diff: 21.785, ratio: 0.448
#   pulse 10: activated nodes: 9181, borderline nodes: 6386, overall activation: 88.922, activation diff: 40.405, ratio: 0.454
#   pulse 11: activated nodes: 9493, borderline nodes: 5885, overall activation: 155.940, activation diff: 67.036, ratio: 0.430
#   pulse 12: activated nodes: 9959, borderline nodes: 5801, overall activation: 251.271, activation diff: 95.337, ratio: 0.379
#   pulse 13: activated nodes: 10255, borderline nodes: 5514, overall activation: 371.324, activation diff: 120.053, ratio: 0.323
#   pulse 14: activated nodes: 10590, borderline nodes: 5291, overall activation: 509.230, activation diff: 137.906, ratio: 0.271
#   pulse 15: activated nodes: 10681, borderline nodes: 4984, overall activation: 657.388, activation diff: 148.158, ratio: 0.225

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10681
#   final overall activation: 657.4
#   number of spread. activ. pulses: 15
#   running time: 1476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.69124305   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   2   0.53623074   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.51868075   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.49933708   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   5   0.33550304   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   6   0.33134648   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   7   0.3225841   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   8   0.32144764   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.31795257   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.31740975   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   11   0.316554   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   12   0.31610972   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.3160345   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   14   0.31271103   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_216   15   0.31174794   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   16   0.31150898   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.31100345   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.31088778   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_295   19   0.31081042   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_484   20   0.31004304   REFERENCES:SIMDATES:SIMLOC
