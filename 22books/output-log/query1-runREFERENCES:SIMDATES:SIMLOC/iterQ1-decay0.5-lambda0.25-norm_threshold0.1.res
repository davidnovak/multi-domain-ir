###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.973, activation diff: 7.973, ratio: 1.603
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 39.050, activation diff: 39.950, ratio: 1.023
#   pulse 3: activated nodes: 8363, borderline nodes: 6078, overall activation: 18.435, activation diff: 35.613, ratio: 1.932
#   pulse 4: activated nodes: 9160, borderline nodes: 6833, overall activation: 383.206, activation diff: 373.950, ratio: 0.976
#   pulse 5: activated nodes: 9624, borderline nodes: 5169, overall activation: 331.376, activation diff: 157.465, ratio: 0.475
#   pulse 6: activated nodes: 10603, borderline nodes: 5735, overall activation: 1014.306, activation diff: 686.385, ratio: 0.677
#   pulse 7: activated nodes: 10729, borderline nodes: 4706, overall activation: 1358.746, activation diff: 344.496, ratio: 0.254
#   pulse 8: activated nodes: 10835, borderline nodes: 4401, overall activation: 1706.942, activation diff: 348.196, ratio: 0.204
#   pulse 9: activated nodes: 10886, borderline nodes: 3872, overall activation: 1924.365, activation diff: 217.423, ratio: 0.113
#   pulse 10: activated nodes: 10932, borderline nodes: 3716, overall activation: 2063.450, activation diff: 139.085, ratio: 0.067
#   pulse 11: activated nodes: 10941, borderline nodes: 3658, overall activation: 2151.463, activation diff: 88.013, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10941
#   final overall activation: 2151.5
#   number of spread. activ. pulses: 11
#   running time: 1556

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990966   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9870304   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9818129   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9600437   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90853524   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.87714356   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49964148   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4986488   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49713457   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4969504   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49584538   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   12   0.49558616   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.49547094   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.49479914   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.49414524   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.493992   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.49315214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.49310023   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49297953   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.49283424   REFERENCES:SIMDATES:SIMLOC
