###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.315, activation diff: 5.315, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 12.339, activation diff: 10.836, ratio: 0.878
#   pulse 3: activated nodes: 8058, borderline nodes: 6554, overall activation: 13.558, activation diff: 4.076, ratio: 0.301
#   pulse 4: activated nodes: 8537, borderline nodes: 7028, overall activation: 55.111, activation diff: 42.326, ratio: 0.768
#   pulse 5: activated nodes: 9006, borderline nodes: 6250, overall activation: 108.128, activation diff: 53.178, ratio: 0.492
#   pulse 6: activated nodes: 9645, borderline nodes: 6450, overall activation: 308.363, activation diff: 200.256, ratio: 0.649
#   pulse 7: activated nodes: 10051, borderline nodes: 5506, overall activation: 568.127, activation diff: 259.766, ratio: 0.457
#   pulse 8: activated nodes: 10621, borderline nodes: 5248, overall activation: 875.169, activation diff: 307.041, ratio: 0.351
#   pulse 9: activated nodes: 10763, borderline nodes: 4731, overall activation: 1169.277, activation diff: 294.108, ratio: 0.252
#   pulse 10: activated nodes: 10836, borderline nodes: 4440, overall activation: 1425.187, activation diff: 255.910, ratio: 0.180
#   pulse 11: activated nodes: 10861, borderline nodes: 4094, overall activation: 1635.267, activation diff: 210.080, ratio: 0.128
#   pulse 12: activated nodes: 10914, borderline nodes: 3842, overall activation: 1801.118, activation diff: 165.851, ratio: 0.092
#   pulse 13: activated nodes: 10926, borderline nodes: 3732, overall activation: 1927.875, activation diff: 126.758, ratio: 0.066
#   pulse 14: activated nodes: 10939, borderline nodes: 3678, overall activation: 2023.141, activation diff: 95.266, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10939
#   final overall activation: 2023.1
#   number of spread. activ. pulses: 14
#   running time: 1554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959755   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97112167   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.96908075   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.94630396   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.89750135   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8457149   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49748054   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49513805   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4946968   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49452338   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49267283   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.4925505   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.49247187   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.49030483   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.4901719   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.48937207   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.48932517   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.48898256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.48890597   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.48884758   REFERENCES:SIMDATES:SIMLOC
