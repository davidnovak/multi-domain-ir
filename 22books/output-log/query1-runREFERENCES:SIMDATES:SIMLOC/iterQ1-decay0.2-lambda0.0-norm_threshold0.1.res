###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.630, activation diff: 10.630, ratio: 2.296
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 139.053, activation diff: 143.683, ratio: 1.033
#   pulse 3: activated nodes: 8514, borderline nodes: 5756, overall activation: 46.964, activation diff: 186.018, ratio: 3.961
#   pulse 4: activated nodes: 10591, borderline nodes: 7491, overall activation: 2124.485, activation diff: 2170.791, ratio: 1.022
#   pulse 5: activated nodes: 10884, borderline nodes: 4003, overall activation: 552.371, activation diff: 2667.397, ratio: 4.829
#   pulse 6: activated nodes: 11350, borderline nodes: 2535, overall activation: 4098.017, activation diff: 4452.135, ratio: 1.086
#   pulse 7: activated nodes: 11420, borderline nodes: 597, overall activation: 1799.237, activation diff: 4110.752, ratio: 2.285
#   pulse 8: activated nodes: 11434, borderline nodes: 249, overall activation: 5131.395, activation diff: 3965.667, ratio: 0.773
#   pulse 9: activated nodes: 11443, borderline nodes: 165, overall activation: 5357.187, activation diff: 793.570, ratio: 0.148
#   pulse 10: activated nodes: 11444, borderline nodes: 123, overall activation: 5798.386, activation diff: 458.551, ratio: 0.079
#   pulse 11: activated nodes: 11446, borderline nodes: 91, overall activation: 5875.449, activation diff: 85.528, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 5875.4
#   number of spread. activ. pulses: 11
#   running time: 1622

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99953246   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99014527   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879669   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97248775   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92085135   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9011185   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79999226   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79995793   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.79993016   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7999166   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7999077   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7998869   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.79988444   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.7998782   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.799859   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.799851   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.7998507   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7998449   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7998435   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.7998349   REFERENCES:SIMDATES:SIMLOC
