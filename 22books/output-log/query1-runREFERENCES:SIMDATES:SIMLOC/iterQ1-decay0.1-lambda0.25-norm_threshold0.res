###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.305, activation diff: 10.305, ratio: 1.411
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 270.623, activation diff: 268.933, ratio: 0.994
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 344.186, activation diff: 189.639, ratio: 0.551
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2622.107, activation diff: 2277.978, ratio: 0.869
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4160.248, activation diff: 1538.141, ratio: 0.370
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5913.564, activation diff: 1753.317, ratio: 0.296
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6886.570, activation diff: 973.006, ratio: 0.141
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7377.567, activation diff: 490.997, ratio: 0.067
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 7609.622, activation diff: 232.055, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7609.6
#   number of spread. activ. pulses: 9
#   running time: 1405

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993527   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9900025   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98851854   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97352004   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92833054   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90782225   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997954   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.8997115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   9   0.8996999   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.899687   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.89967954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   12   0.89967626   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.89967334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.899662   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   15   0.89966124   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.8996594   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.89965576   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   18   0.89965004   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.8996463   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.89964116   REFERENCES:SIMDATES:SIMLOC
