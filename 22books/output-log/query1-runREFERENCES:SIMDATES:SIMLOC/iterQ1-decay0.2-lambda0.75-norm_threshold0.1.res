###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.658, activation diff: 2.658, ratio: 0.470
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 5.161, activation diff: 1.695, ratio: 0.328
#   pulse 3: activated nodes: 7513, borderline nodes: 7276, overall activation: 6.345, activation diff: 2.914, ratio: 0.459
#   pulse 4: activated nodes: 7764, borderline nodes: 6941, overall activation: 8.189, activation diff: 3.319, ratio: 0.405
#   pulse 5: activated nodes: 7836, borderline nodes: 6837, overall activation: 12.435, activation diff: 5.395, ratio: 0.434
#   pulse 6: activated nodes: 7968, borderline nodes: 6630, overall activation: 22.119, activation diff: 10.495, ratio: 0.474
#   pulse 7: activated nodes: 8625, borderline nodes: 6803, overall activation: 44.956, activation diff: 23.315, ratio: 0.519
#   pulse 8: activated nodes: 9057, borderline nodes: 6604, overall activation: 96.093, activation diff: 51.297, ratio: 0.534
#   pulse 9: activated nodes: 9800, borderline nodes: 6559, overall activation: 198.374, activation diff: 102.328, ratio: 0.516
#   pulse 10: activated nodes: 10539, borderline nodes: 6267, overall activation: 370.919, activation diff: 172.554, ratio: 0.465
#   pulse 11: activated nodes: 10985, borderline nodes: 5345, overall activation: 623.473, activation diff: 252.555, ratio: 0.405
#   pulse 12: activated nodes: 11171, borderline nodes: 4397, overall activation: 954.882, activation diff: 331.408, ratio: 0.347
#   pulse 13: activated nodes: 11247, borderline nodes: 3236, overall activation: 1350.990, activation diff: 396.108, ratio: 0.293
#   pulse 14: activated nodes: 11318, borderline nodes: 2284, overall activation: 1786.264, activation diff: 435.274, ratio: 0.244
#   pulse 15: activated nodes: 11352, borderline nodes: 1477, overall activation: 2235.701, activation diff: 449.437, ratio: 0.201

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11352
#   final overall activation: 2235.7
#   number of spread. activ. pulses: 15
#   running time: 1650

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8258672   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.7012605   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.6684586   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.66587096   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.64822626   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.6370131   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   7   0.6326887   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   8   0.6299614   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.6291748   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.6290007   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   11   0.6275828   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   12   0.6265908   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.6250734   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.6243232   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.6190119   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.61695683   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.614943   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.6142384   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.6129837   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.6123867   REFERENCES:SIMDATES:SIMLOC
