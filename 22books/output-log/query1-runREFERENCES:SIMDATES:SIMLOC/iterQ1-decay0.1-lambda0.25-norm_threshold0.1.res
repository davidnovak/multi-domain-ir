###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.973, activation diff: 7.973, ratio: 1.603
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 68.661, activation diff: 69.561, ratio: 1.013
#   pulse 3: activated nodes: 8363, borderline nodes: 6078, overall activation: 35.454, activation diff: 66.400, ratio: 1.873
#   pulse 4: activated nodes: 10225, borderline nodes: 7770, overall activation: 1063.888, activation diff: 1047.082, ratio: 0.984
#   pulse 5: activated nodes: 10583, borderline nodes: 4841, overall activation: 1178.388, activation diff: 458.408, ratio: 0.389
#   pulse 6: activated nodes: 11275, borderline nodes: 4055, overall activation: 3498.108, activation diff: 2322.521, ratio: 0.664
#   pulse 7: activated nodes: 11379, borderline nodes: 1216, overall activation: 4949.549, activation diff: 1451.443, ratio: 0.293
#   pulse 8: activated nodes: 11428, borderline nodes: 400, overall activation: 6121.226, activation diff: 1171.678, ratio: 0.191
#   pulse 9: activated nodes: 11444, borderline nodes: 127, overall activation: 6789.717, activation diff: 668.491, ratio: 0.098
#   pulse 10: activated nodes: 11449, borderline nodes: 70, overall activation: 7145.947, activation diff: 356.230, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7145.9
#   number of spread. activ. pulses: 10
#   running time: 1466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991077   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9883141   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98591876   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9695153   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9196676   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89958924   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8994799   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.8994327   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.8994174   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.8994148   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.8993924   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.89936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   13   0.89932626   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.8993083   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   15   0.8993083   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   16   0.8992891   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.89928746   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.89927804   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.899261   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.89924467   REFERENCES:SIMDATES:SIMLOC
