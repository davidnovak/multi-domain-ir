###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.435, activation diff: 3.435, ratio: 0.534
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 31.184, activation diff: 26.697, ratio: 0.856
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 66.606, activation diff: 36.730, ratio: 0.551
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 164.752, activation diff: 98.764, ratio: 0.599
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 345.565, activation diff: 180.935, ratio: 0.524
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 626.797, activation diff: 281.233, ratio: 0.449
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 996.440, activation diff: 369.642, ratio: 0.371
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 1427.953, activation diff: 431.513, ratio: 0.302
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 1888.191, activation diff: 460.238, ratio: 0.244
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 2347.788, activation diff: 459.596, ratio: 0.196
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 2785.578, activation diff: 437.791, ratio: 0.157
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 3188.344, activation diff: 402.766, ratio: 0.126
#   pulse 13: activated nodes: 11464, borderline nodes: 21, overall activation: 3549.329, activation diff: 360.985, ratio: 0.102
#   pulse 14: activated nodes: 11464, borderline nodes: 21, overall activation: 3866.710, activation diff: 317.381, ratio: 0.082
#   pulse 15: activated nodes: 11464, borderline nodes: 21, overall activation: 4141.847, activation diff: 275.137, ratio: 0.066

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4141.8
#   number of spread. activ. pulses: 15
#   running time: 1575

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9649476   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.922215   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9149495   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.88970125   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.84523284   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.781001   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.71195614   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7103461   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.70542705   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.70462614   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7045334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.7029472   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7027111   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.7026427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.7017992   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.701705   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.7012543   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.70100874   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.7007947   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   20   0.700631   REFERENCES:SIMDATES:SIMLOC
