###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.824, activation diff: 7.824, ratio: 4.290
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 18.312, activation diff: 20.136, ratio: 1.100
#   pulse 3: activated nodes: 5198, borderline nodes: 4392, overall activation: 3.126, activation diff: 21.438, ratio: 6.858
#   pulse 4: activated nodes: 6850, borderline nodes: 6037, overall activation: 316.208, activation diff: 319.334, ratio: 1.010
#   pulse 5: activated nodes: 7928, borderline nodes: 5212, overall activation: 66.555, activation diff: 382.763, ratio: 5.751
#   pulse 6: activated nodes: 10635, borderline nodes: 7545, overall activation: 2047.396, activation diff: 2113.796, ratio: 1.032
#   pulse 7: activated nodes: 10884, borderline nodes: 4204, overall activation: 417.117, activation diff: 2462.895, ratio: 5.905
#   pulse 8: activated nodes: 11297, borderline nodes: 3283, overall activation: 3130.725, activation diff: 3495.039, ratio: 1.116
#   pulse 9: activated nodes: 11368, borderline nodes: 1483, overall activation: 696.032, activation diff: 3632.447, ratio: 5.219
#   pulse 10: activated nodes: 11394, borderline nodes: 1157, overall activation: 3510.099, activation diff: 3751.506, ratio: 1.069
#   pulse 11: activated nodes: 11412, borderline nodes: 845, overall activation: 1720.964, activation diff: 2897.133, ratio: 1.683
#   pulse 12: activated nodes: 11414, borderline nodes: 790, overall activation: 3950.580, activation diff: 2668.410, ratio: 0.675
#   pulse 13: activated nodes: 11417, borderline nodes: 682, overall activation: 4029.265, activation diff: 507.613, ratio: 0.126
#   pulse 14: activated nodes: 11417, borderline nodes: 613, overall activation: 4322.330, activation diff: 310.882, ratio: 0.072
#   pulse 15: activated nodes: 11420, borderline nodes: 490, overall activation: 4376.285, activation diff: 65.253, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11420
#   final overall activation: 4376.3
#   number of spread. activ. pulses: 15
#   running time: 1659

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994507   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9890423   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98529935   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96867394   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91015804   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8888466   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.749982   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.74981636   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7498   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.7497863   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74970245   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   12   0.7496337   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.74961925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.74958223   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7494263   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7494229   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.74937946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.74936473   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.7493639   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.7493526   REFERENCES:SIMDATES:SIMLOC
