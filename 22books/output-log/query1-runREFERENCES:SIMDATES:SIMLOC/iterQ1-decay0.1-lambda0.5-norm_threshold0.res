###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.870, activation diff: 6.870, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 124.558, activation diff: 119.494, ratio: 0.959
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 230.269, activation diff: 106.615, ratio: 0.463
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1112.013, activation diff: 881.755, ratio: 0.793
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2234.771, activation diff: 1122.758, ratio: 0.502
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3650.873, activation diff: 1416.101, ratio: 0.388
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4904.232, activation diff: 1253.359, ratio: 0.256
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5860.301, activation diff: 956.069, ratio: 0.163
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6535.333, activation diff: 675.032, ratio: 0.103
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 6990.027, activation diff: 454.695, ratio: 0.065
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 7287.302, activation diff: 297.275, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7287.3
#   number of spread. activ. pulses: 11
#   running time: 1520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99597347   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.981288   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98063624   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9626336   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91682017   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89620554   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89602107   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.89539146   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.89518434   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.89515793   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.895139   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89502317   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   13   0.8949441   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   14   0.8948252   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   15   0.8947416   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.8947268   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.8946336   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.89463127   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.89461434   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.89459217   REFERENCES:SIMDATES:SIMLOC
