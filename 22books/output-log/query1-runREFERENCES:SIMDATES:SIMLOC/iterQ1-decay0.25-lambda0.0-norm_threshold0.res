###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.739, activation diff: 13.739, ratio: 1.775
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 394.810, activation diff: 402.133, ratio: 1.019
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 205.262, activation diff: 567.934, ratio: 2.767
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3144.354, activation diff: 3211.805, ratio: 1.021
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1783.345, activation diff: 2949.474, ratio: 1.654
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 4773.114, activation diff: 3369.362, ratio: 0.706
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 5120.860, activation diff: 620.880, ratio: 0.121
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 5462.327, activation diff: 348.187, ratio: 0.064
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 5538.421, activation diff: 78.874, ratio: 0.014

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5538.4
#   number of spread. activ. pulses: 9
#   running time: 1430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995811   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9910735   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9892724   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97486854   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92834383   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9100136   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74998844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.749904   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7498849   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7498765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74985254   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7498047   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7497492   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.7497433   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.74973845   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7497333   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7497255   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   18   0.7497139   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7497097   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   20   0.74970186   REFERENCES:SIMDATES:SIMLOC
