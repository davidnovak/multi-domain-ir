###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.973, activation diff: 7.973, ratio: 1.603
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 57.557, activation diff: 58.457, ratio: 1.016
#   pulse 3: activated nodes: 8363, borderline nodes: 6078, overall activation: 28.520, activation diff: 54.271, ratio: 1.903
#   pulse 4: activated nodes: 10047, borderline nodes: 7635, overall activation: 769.557, activation diff: 755.464, ratio: 0.982
#   pulse 5: activated nodes: 10423, borderline nodes: 4952, overall activation: 792.059, activation diff: 313.178, ratio: 0.395
#   pulse 6: activated nodes: 11233, borderline nodes: 4507, overall activation: 2326.121, activation diff: 1537.336, ratio: 0.661
#   pulse 7: activated nodes: 11321, borderline nodes: 1866, overall activation: 3220.578, activation diff: 894.469, ratio: 0.278
#   pulse 8: activated nodes: 11404, borderline nodes: 815, overall activation: 4029.600, activation diff: 809.022, ratio: 0.201
#   pulse 9: activated nodes: 11422, borderline nodes: 379, overall activation: 4534.521, activation diff: 504.921, ratio: 0.111
#   pulse 10: activated nodes: 11430, borderline nodes: 248, overall activation: 4832.796, activation diff: 298.275, ratio: 0.062
#   pulse 11: activated nodes: 11433, borderline nodes: 201, overall activation: 5002.307, activation diff: 169.512, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11433
#   final overall activation: 5002.3
#   number of spread. activ. pulses: 11
#   running time: 1514

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99937916   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9893583   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9863253   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.969771   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9187091   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89708656   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74988526   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.74971503   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.7496514   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7496446   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74959946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7495489   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.74951285   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.7495067   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7494724   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.74938273   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7493814   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7493735   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.74937034   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.749367   REFERENCES:SIMDATES:SIMLOC
