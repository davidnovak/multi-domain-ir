###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.315, activation diff: 5.315, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 16.805, activation diff: 15.301, ratio: 0.911
#   pulse 3: activated nodes: 8058, borderline nodes: 6554, overall activation: 19.477, activation diff: 5.769, ratio: 0.296
#   pulse 4: activated nodes: 8643, borderline nodes: 7128, overall activation: 124.506, activation diff: 105.629, ratio: 0.848
#   pulse 5: activated nodes: 9226, borderline nodes: 5982, overall activation: 261.843, activation diff: 137.400, ratio: 0.525
#   pulse 6: activated nodes: 10659, borderline nodes: 6670, overall activation: 745.081, activation diff: 483.247, ratio: 0.649
#   pulse 7: activated nodes: 11084, borderline nodes: 4768, overall activation: 1372.829, activation diff: 627.748, ratio: 0.457
#   pulse 8: activated nodes: 11265, borderline nodes: 3178, overall activation: 2134.765, activation diff: 761.936, ratio: 0.357
#   pulse 9: activated nodes: 11351, borderline nodes: 1598, overall activation: 2861.306, activation diff: 726.541, ratio: 0.254
#   pulse 10: activated nodes: 11409, borderline nodes: 751, overall activation: 3483.721, activation diff: 622.415, ratio: 0.179
#   pulse 11: activated nodes: 11425, borderline nodes: 431, overall activation: 3978.607, activation diff: 494.886, ratio: 0.124
#   pulse 12: activated nodes: 11427, borderline nodes: 293, overall activation: 4352.277, activation diff: 373.670, ratio: 0.086
#   pulse 13: activated nodes: 11435, borderline nodes: 209, overall activation: 4624.536, activation diff: 272.258, ratio: 0.059
#   pulse 14: activated nodes: 11438, borderline nodes: 180, overall activation: 4818.427, activation diff: 193.891, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 4818.4
#   number of spread. activ. pulses: 14
#   running time: 1516

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9975078   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9813439   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9805064   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9616226   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9112784   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8812479   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74830115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7477869   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.74748206   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7472131   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7469175   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.7467623   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7467017   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.7466874   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.746566   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7464273   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.7464224   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   18   0.74639976   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   19   0.7463901   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   20   0.74632233   REFERENCES:SIMDATES:SIMLOC
