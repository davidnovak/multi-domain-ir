###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.824, activation diff: 7.824, ratio: 4.290
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 19.507, activation diff: 21.331, ratio: 1.093
#   pulse 3: activated nodes: 5198, borderline nodes: 4392, overall activation: 3.368, activation diff: 22.875, ratio: 6.792
#   pulse 4: activated nodes: 6850, borderline nodes: 6037, overall activation: 368.848, activation diff: 372.216, ratio: 1.009
#   pulse 5: activated nodes: 7952, borderline nodes: 5144, overall activation: 84.277, activation diff: 453.125, ratio: 5.377
#   pulse 6: activated nodes: 10707, borderline nodes: 7468, overall activation: 2396.694, activation diff: 2480.702, ratio: 1.035
#   pulse 7: activated nodes: 10946, borderline nodes: 3960, overall activation: 522.292, activation diff: 2914.380, ratio: 5.580
#   pulse 8: activated nodes: 11313, borderline nodes: 2894, overall activation: 3689.206, activation diff: 4112.153, ratio: 1.115
#   pulse 9: activated nodes: 11390, borderline nodes: 1176, overall activation: 1029.336, activation diff: 4177.043, ratio: 4.058
#   pulse 10: activated nodes: 11411, borderline nodes: 843, overall activation: 4263.848, activation diff: 4213.604, ratio: 0.988
#   pulse 11: activated nodes: 11418, borderline nodes: 615, overall activation: 3536.365, activation diff: 1933.352, ratio: 0.547
#   pulse 12: activated nodes: 11425, borderline nodes: 554, overall activation: 4938.751, activation diff: 1557.283, ratio: 0.315
#   pulse 13: activated nodes: 11428, borderline nodes: 405, overall activation: 5067.556, activation diff: 228.893, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11428
#   final overall activation: 5067.6
#   number of spread. activ. pulses: 13
#   running time: 1562

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99937016   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.988924   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98420143   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9680386   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9053062   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.878529   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999882   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79991245   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.799898   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.79986185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.79984957   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   12   0.7997981   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7997877   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.79977816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   15   0.799769   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7997622   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.79975724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7997432   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.7997363   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7997223   REFERENCES:SIMDATES:SIMLOC
