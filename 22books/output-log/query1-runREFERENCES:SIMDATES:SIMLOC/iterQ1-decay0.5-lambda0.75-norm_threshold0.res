###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.435, activation diff: 3.435, ratio: 0.534
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 22.939, activation diff: 18.452, ratio: 0.804
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 45.506, activation diff: 23.876, ratio: 0.525
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 93.752, activation diff: 48.947, ratio: 0.522
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 177.218, activation diff: 83.681, ratio: 0.472
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 301.351, activation diff: 124.148, ratio: 0.412
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 457.704, activation diff: 156.353, ratio: 0.342
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 633.772, activation diff: 176.068, ratio: 0.278
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 818.012, activation diff: 184.240, ratio: 0.225
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 1001.226, activation diff: 183.214, ratio: 0.183
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 1176.718, activation diff: 175.492, ratio: 0.149
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 1340.006, activation diff: 163.288, ratio: 0.122
#   pulse 13: activated nodes: 10971, borderline nodes: 3512, overall activation: 1488.506, activation diff: 148.500, ratio: 0.100
#   pulse 14: activated nodes: 10971, borderline nodes: 3512, overall activation: 1621.223, activation diff: 132.717, ratio: 0.082
#   pulse 15: activated nodes: 10971, borderline nodes: 3512, overall activation: 1738.326, activation diff: 117.102, ratio: 0.067

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 1738.3
#   number of spread. activ. pulses: 15
#   running time: 1653

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9575099   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9003555   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.881189   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.86160624   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.82407385   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.73197   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46637008   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.46114606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.46114284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.45898357   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.45728466   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.45602655   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.45585683   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.45337713   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.4533103   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   16   0.45304382   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   17   0.451221   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   18   0.45064068   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.4494265   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.44929412   REFERENCES:SIMDATES:SIMLOC
