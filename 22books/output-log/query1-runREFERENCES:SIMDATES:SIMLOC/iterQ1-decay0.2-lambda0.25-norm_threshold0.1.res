###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.973, activation diff: 7.973, ratio: 1.603
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 61.258, activation diff: 62.158, ratio: 1.015
#   pulse 3: activated nodes: 8363, borderline nodes: 6078, overall activation: 30.745, activation diff: 58.227, ratio: 1.894
#   pulse 4: activated nodes: 10065, borderline nodes: 7643, overall activation: 861.882, activation diff: 846.866, ratio: 0.983
#   pulse 5: activated nodes: 10438, borderline nodes: 4887, overall activation: 911.063, activation diff: 356.534, ratio: 0.391
#   pulse 6: activated nodes: 11247, borderline nodes: 4367, overall activation: 2685.720, activation diff: 1777.815, ratio: 0.662
#   pulse 7: activated nodes: 11338, borderline nodes: 1594, overall activation: 3746.702, activation diff: 1061.008, ratio: 0.283
#   pulse 8: activated nodes: 11412, borderline nodes: 628, overall activation: 4680.086, activation diff: 933.383, ratio: 0.199
#   pulse 9: activated nodes: 11434, borderline nodes: 287, overall activation: 5246.996, activation diff: 566.910, ratio: 0.108
#   pulse 10: activated nodes: 11442, borderline nodes: 154, overall activation: 5569.087, activation diff: 322.092, ratio: 0.058
#   pulse 11: activated nodes: 11444, borderline nodes: 112, overall activation: 5744.796, activation diff: 175.709, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 5744.8
#   number of spread. activ. pulses: 11
#   running time: 1513

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994029   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9894858   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98682195   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9706504   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91999125   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8985374   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79989094   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7997651   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.7997573   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.799733   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7997204   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7996967   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7996788   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.7996721   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.79966766   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.7996627   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.799659   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7996471   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7996459   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.79964334   REFERENCES:SIMDATES:SIMLOC
