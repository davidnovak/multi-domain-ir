###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.912, activation diff: 3.912, ratio: 1.000
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 2.184, activation diff: 1.978, ratio: 0.906
#   pulse 3: activated nodes: 4796, borderline nodes: 4723, overall activation: 1.098, activation diff: 1.086, ratio: 0.989
#   pulse 4: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.549, activation diff: 0.549, ratio: 1.000
#   pulse 5: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.274, activation diff: 0.274, ratio: 1.000
#   pulse 6: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.137, activation diff: 0.137, ratio: 1.000
#   pulse 7: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.069, activation diff: 0.069, ratio: 1.000
#   pulse 8: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.034, activation diff: 0.034, ratio: 1.000
#   pulse 9: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.017, activation diff: 0.017, ratio: 1.000
#   pulse 10: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.009, activation diff: 0.009, ratio: 1.000
#   pulse 11: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.004, activation diff: 0.004, ratio: 1.000
#   pulse 12: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.002, activation diff: 0.002, ratio: 1.000
#   pulse 13: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 14: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.001, activation diff: 0.001, ratio: 1.000
#   pulse 15: activated nodes: 4796, borderline nodes: 4723, overall activation: 0.000, activation diff: 0.000, ratio: 1.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4796
#   final overall activation: 0.0
#   number of spread. activ. pulses: 15
#   running time: 704

###################################
# top k results in TREC format: 

1   Q1   generalhistoryfo00myerrich_787   1   3.1241423E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   3.0517578E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   3   3.0517578E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   3.0517578E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   3.0517578E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   3.0517578E-5   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_241   7   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_18   8   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_147   9   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_119   10   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_134   11   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_136   12   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_137   13   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_164   14   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_151   15   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   graduateschoolof9192bran_62   16   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_281   17   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_0   18   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_14   19   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_46   20   4.9540006E-7   REFERENCES:SIMDATES:SIMLOC
