###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.870, activation diff: 6.870, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 71.880, activation diff: 66.816, ratio: 0.930
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 119.004, activation diff: 48.033, ratio: 0.404
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 409.022, activation diff: 290.041, ratio: 0.709
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 747.113, activation diff: 338.091, ratio: 0.453
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1122.671, activation diff: 375.558, ratio: 0.335
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1457.381, activation diff: 334.709, ratio: 0.230
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 1727.487, activation diff: 270.106, ratio: 0.156
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 1931.989, activation diff: 204.502, ratio: 0.106
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 2081.714, activation diff: 149.725, ratio: 0.072
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 2189.904, activation diff: 108.190, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2189.9
#   number of spread. activ. pulses: 11
#   running time: 1452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99472356   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9732331   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9723083   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9482868   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90279233   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8581772   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4964565   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49429584   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49367863   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49347878   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49182183   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.49149966   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.49120563   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.4892961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.48919958   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.48860154   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.48821574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.4881996   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.48801968   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.48791945   REFERENCES:SIMDATES:SIMLOC
