###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.630, activation diff: 10.630, ratio: 2.296
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 130.447, activation diff: 135.077, ratio: 1.035
#   pulse 3: activated nodes: 8514, borderline nodes: 5756, overall activation: 42.057, activation diff: 172.504, ratio: 4.102
#   pulse 4: activated nodes: 10571, borderline nodes: 7481, overall activation: 1897.586, activation diff: 1939.136, ratio: 1.022
#   pulse 5: activated nodes: 10867, borderline nodes: 4135, overall activation: 464.230, activation diff: 2354.315, ratio: 5.071
#   pulse 6: activated nodes: 11340, borderline nodes: 2770, overall activation: 3575.214, activation diff: 3893.284, ratio: 1.089
#   pulse 7: activated nodes: 11418, borderline nodes: 713, overall activation: 1356.938, activation diff: 3731.382, ratio: 2.750
#   pulse 8: activated nodes: 11427, borderline nodes: 387, overall activation: 4393.469, activation diff: 3686.807, ratio: 0.839
#   pulse 9: activated nodes: 11431, borderline nodes: 258, overall activation: 4495.740, activation diff: 885.408, ratio: 0.197
#   pulse 10: activated nodes: 11432, borderline nodes: 221, overall activation: 5038.610, activation diff: 568.854, ratio: 0.113
#   pulse 11: activated nodes: 11436, borderline nodes: 190, overall activation: 5125.896, activation diff: 100.066, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 5125.9
#   number of spread. activ. pulses: 11
#   running time: 1515

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99951273   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9901215   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9872941   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.971854   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91912514   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8995971   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7499865   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74988174   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7498649   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.74986005   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7498275   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.74976873   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7497072   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.74968946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7496771   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.74967086   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7496687   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.74964976   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.74963856   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   20   0.7496233   REFERENCES:SIMDATES:SIMLOC
