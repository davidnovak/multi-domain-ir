###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.870, activation diff: 6.870, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 111.389, activation diff: 106.325, ratio: 0.955
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 200.725, activation diff: 90.242, ratio: 0.450
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 896.910, activation diff: 696.197, ratio: 0.776
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1765.582, activation diff: 868.673, ratio: 0.492
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 2848.252, activation diff: 1082.670, ratio: 0.380
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 3827.802, activation diff: 979.551, ratio: 0.256
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 4600.081, activation diff: 772.279, ratio: 0.168
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5160.775, activation diff: 560.694, ratio: 0.109
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 5549.619, activation diff: 388.844, ratio: 0.070
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 5811.525, activation diff: 261.906, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5811.5
#   number of spread. activ. pulses: 11
#   running time: 1712

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99580675   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9804113   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9794762   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96071124   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9145542   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.885484   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7963834   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7959281   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7954619   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7950326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.7948301   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.7948036   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.7947161   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.79449415   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.7944691   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7944051   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   17   0.7944025   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.7943847   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   19   0.79436296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   20   0.79435706   REFERENCES:SIMDATES:SIMLOC
