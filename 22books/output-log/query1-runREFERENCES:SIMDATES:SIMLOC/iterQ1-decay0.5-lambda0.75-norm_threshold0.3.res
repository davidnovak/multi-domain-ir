###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.715, activation diff: 1.715, ratio: 0.364
#   pulse 2: activated nodes: 3986, borderline nodes: 3967, overall activation: 3.608, activation diff: 1.169, ratio: 0.324
#   pulse 3: activated nodes: 3986, borderline nodes: 3967, overall activation: 2.727, activation diff: 0.881, ratio: 0.323
#   pulse 4: activated nodes: 3986, borderline nodes: 3967, overall activation: 2.051, activation diff: 0.676, ratio: 0.329
#   pulse 5: activated nodes: 3986, borderline nodes: 3967, overall activation: 1.539, activation diff: 0.513, ratio: 0.333
#   pulse 6: activated nodes: 3986, borderline nodes: 3967, overall activation: 1.154, activation diff: 0.385, ratio: 0.333
#   pulse 7: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.865, activation diff: 0.288, ratio: 0.333
#   pulse 8: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.649, activation diff: 0.216, ratio: 0.333
#   pulse 9: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.487, activation diff: 0.162, ratio: 0.333
#   pulse 10: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.365, activation diff: 0.122, ratio: 0.333
#   pulse 11: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.274, activation diff: 0.091, ratio: 0.333
#   pulse 12: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.205, activation diff: 0.068, ratio: 0.333
#   pulse 13: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.154, activation diff: 0.051, ratio: 0.333
#   pulse 14: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.116, activation diff: 0.039, ratio: 0.333
#   pulse 15: activated nodes: 3986, borderline nodes: 3967, overall activation: 0.087, activation diff: 0.029, ratio: 0.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 3986
#   final overall activation: 0.1
#   number of spread. activ. pulses: 15
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   3   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   6   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_368   11   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_366   12   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_375   14   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_350   16   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_374   17   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_370   18   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_365   19   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_367   20   0.0   REFERENCES:SIMDATES:SIMLOC
