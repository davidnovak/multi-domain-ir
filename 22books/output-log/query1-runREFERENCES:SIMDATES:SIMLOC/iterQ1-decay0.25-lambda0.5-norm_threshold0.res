###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.870, activation diff: 6.870, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 104.804, activation diff: 99.740, ratio: 0.952
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 186.386, activation diff: 82.488, ratio: 0.443
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 799.809, activation diff: 613.438, ratio: 0.767
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1556.520, activation diff: 756.711, ratio: 0.486
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 2488.937, activation diff: 932.417, ratio: 0.375
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 3336.246, activation diff: 847.310, ratio: 0.254
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 4014.724, activation diff: 678.478, ratio: 0.169
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 4515.655, activation diff: 500.931, ratio: 0.111
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 4868.311, activation diff: 352.656, ratio: 0.072
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 5109.632, activation diff: 241.321, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5109.6
#   number of spread. activ. pulses: 11
#   running time: 1502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9957018   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.97983503   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9787624   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9594753   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91317976   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8829751   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7464612   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74576294   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.74545836   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.74480677   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.74441814   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.74440944   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.74434876   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.74426574   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.74416435   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7441417   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.74403   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   18   0.74402857   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.74401104   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   20   0.7439858   REFERENCES:SIMDATES:SIMLOC
