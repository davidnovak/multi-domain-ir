###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.658, activation diff: 2.658, ratio: 0.470
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 5.187, activation diff: 1.721, ratio: 0.332
#   pulse 3: activated nodes: 7513, borderline nodes: 7276, overall activation: 6.621, activation diff: 3.163, ratio: 0.478
#   pulse 4: activated nodes: 7764, borderline nodes: 6941, overall activation: 8.839, activation diff: 3.684, ratio: 0.417
#   pulse 5: activated nodes: 7840, borderline nodes: 6837, overall activation: 14.321, activation diff: 6.613, ratio: 0.462
#   pulse 6: activated nodes: 7984, borderline nodes: 6584, overall activation: 27.303, activation diff: 13.765, ratio: 0.504
#   pulse 7: activated nodes: 8659, borderline nodes: 6736, overall activation: 59.242, activation diff: 32.354, ratio: 0.546
#   pulse 8: activated nodes: 9185, borderline nodes: 6564, overall activation: 131.830, activation diff: 72.715, ratio: 0.552
#   pulse 9: activated nodes: 10203, borderline nodes: 6594, overall activation: 275.205, activation diff: 143.404, ratio: 0.521
#   pulse 10: activated nodes: 10716, borderline nodes: 5910, overall activation: 515.985, activation diff: 240.785, ratio: 0.467
#   pulse 11: activated nodes: 11092, borderline nodes: 4942, overall activation: 869.140, activation diff: 353.155, ratio: 0.406
#   pulse 12: activated nodes: 11223, borderline nodes: 3660, overall activation: 1333.352, activation diff: 464.213, ratio: 0.348
#   pulse 13: activated nodes: 11307, borderline nodes: 2487, overall activation: 1880.085, activation diff: 546.733, ratio: 0.291
#   pulse 14: activated nodes: 11349, borderline nodes: 1594, overall activation: 2471.394, activation diff: 591.308, ratio: 0.239
#   pulse 15: activated nodes: 11402, borderline nodes: 895, overall activation: 3075.174, activation diff: 603.781, ratio: 0.196

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11402
#   final overall activation: 3075.2
#   number of spread. activ. pulses: 15
#   running time: 1657

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84462327   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   2   0.7511063   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   3   0.73975235   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   4   0.7359264   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   0.73210716   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   6   0.73184   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   7   0.73064524   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7298214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   9   0.7295367   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7265325   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.72569656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.7242986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.723061   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   14   0.7228285   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   15   0.7215265   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.7192596   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.7159851   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7126855   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.7112939   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.7112424   REFERENCES:SIMDATES:SIMLOC
