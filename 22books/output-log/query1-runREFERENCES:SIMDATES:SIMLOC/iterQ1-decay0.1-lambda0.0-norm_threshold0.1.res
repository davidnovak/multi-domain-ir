###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.630, activation diff: 10.630, ratio: 2.296
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 156.265, activation diff: 160.895, ratio: 1.030
#   pulse 3: activated nodes: 8514, borderline nodes: 5756, overall activation: 57.906, activation diff: 214.171, ratio: 3.699
#   pulse 4: activated nodes: 10639, borderline nodes: 7504, overall activation: 2628.997, activation diff: 2685.833, ratio: 1.022
#   pulse 5: activated nodes: 10966, borderline nodes: 3811, overall activation: 756.678, activation diff: 3371.088, ratio: 4.455
#   pulse 6: activated nodes: 11360, borderline nodes: 2081, overall activation: 5265.676, activation diff: 5681.343, ratio: 1.079
#   pulse 7: activated nodes: 11431, borderline nodes: 430, overall activation: 2918.814, activation diff: 4762.946, ratio: 1.632
#   pulse 8: activated nodes: 11444, borderline nodes: 138, overall activation: 6728.312, activation diff: 4382.546, ratio: 0.651
#   pulse 9: activated nodes: 11450, borderline nodes: 72, overall activation: 7077.543, activation diff: 684.891, ratio: 0.097
#   pulse 10: activated nodes: 11452, borderline nodes: 50, overall activation: 7394.450, activation diff: 330.400, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7394.5
#   number of spread. activ. pulses: 10
#   running time: 2549

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995517   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9901586   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98840374   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9729448   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92298365   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90236235   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89999735   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.8999942   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.89998883   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.899983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.8999807   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.89997923   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.8999769   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.8999757   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   15   0.8999755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.8999724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.8999678   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.8999662   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.89996225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.89996123   REFERENCES:SIMDATES:SIMLOC
