###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.305, activation diff: 10.305, ratio: 1.411
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 226.307, activation diff: 224.617, ratio: 0.993
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 267.467, activation diff: 139.104, ratio: 0.520
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1853.389, activation diff: 1585.991, ratio: 0.856
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2852.858, activation diff: 999.469, ratio: 0.350
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 4060.697, activation diff: 1207.838, ratio: 0.297
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 4780.657, activation diff: 719.961, ratio: 0.151
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 5175.245, activation diff: 394.588, ratio: 0.076
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 5379.643, activation diff: 204.398, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5379.6
#   number of spread. activ. pulses: 9
#   running time: 1442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993037   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9897265   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98748994   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97178894   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9256237   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9048821   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7497919   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.74960405   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.74941504   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.74941206   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7493978   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7493632   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7493303   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.74930865   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7492873   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7492481   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   17   0.749221   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.74919903   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7491612   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   20   0.74911696   REFERENCES:SIMDATES:SIMLOC
