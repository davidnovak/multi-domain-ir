###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.315, activation diff: 5.315, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 17.698, activation diff: 16.194, ratio: 0.915
#   pulse 3: activated nodes: 8058, borderline nodes: 6554, overall activation: 20.671, activation diff: 6.120, ratio: 0.296
#   pulse 4: activated nodes: 8664, borderline nodes: 7148, overall activation: 141.621, activation diff: 121.532, ratio: 0.858
#   pulse 5: activated nodes: 9239, borderline nodes: 5894, overall activation: 300.662, activation diff: 159.095, ratio: 0.529
#   pulse 6: activated nodes: 10704, borderline nodes: 6613, overall activation: 860.809, activation diff: 560.154, ratio: 0.651
#   pulse 7: activated nodes: 11114, borderline nodes: 4505, overall activation: 1595.325, activation diff: 734.516, ratio: 0.460
#   pulse 8: activated nodes: 11294, borderline nodes: 2862, overall activation: 2488.155, activation diff: 892.829, ratio: 0.359
#   pulse 9: activated nodes: 11370, borderline nodes: 1287, overall activation: 3335.234, activation diff: 847.079, ratio: 0.254
#   pulse 10: activated nodes: 11416, borderline nodes: 586, overall activation: 4057.142, activation diff: 721.908, ratio: 0.178
#   pulse 11: activated nodes: 11433, borderline nodes: 328, overall activation: 4622.539, activation diff: 565.397, ratio: 0.122
#   pulse 12: activated nodes: 11441, borderline nodes: 197, overall activation: 5040.168, activation diff: 417.629, ratio: 0.083
#   pulse 13: activated nodes: 11443, borderline nodes: 135, overall activation: 5338.110, activation diff: 297.941, ratio: 0.056
#   pulse 14: activated nodes: 11443, borderline nodes: 107, overall activation: 5545.892, activation diff: 207.782, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 5545.9
#   number of spread. activ. pulses: 14
#   running time: 1746

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99765   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9822482   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98145163   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9632004   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.913033   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8843961   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79834235   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7980621   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7976202   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7975547   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7974721   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.79726434   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.79723895   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.797069   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.7970021   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   16   0.79690635   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.79690576   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.7968793   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.79684854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.79683805   REFERENCES:SIMDATES:SIMLOC
