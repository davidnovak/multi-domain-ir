###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.435, activation diff: 3.435, ratio: 0.534
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 32.833, activation diff: 28.346, ratio: 0.863
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 70.942, activation diff: 39.417, ratio: 0.556
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 181.544, activation diff: 111.205, ratio: 0.613
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 387.464, activation diff: 206.033, ratio: 0.532
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 711.915, activation diff: 324.451, ratio: 0.456
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 1142.387, activation diff: 430.471, ratio: 0.377
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 1646.256, activation diff: 503.869, ratio: 0.306
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 2182.750, activation diff: 536.494, ratio: 0.246
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 2716.197, activation diff: 533.448, ratio: 0.196
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 3220.592, activation diff: 504.395, ratio: 0.157
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 3680.379, activation diff: 459.787, ratio: 0.125
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 4088.919, activation diff: 408.540, ratio: 0.100
#   pulse 14: activated nodes: 11464, borderline nodes: 0, overall activation: 4445.422, activation diff: 356.503, ratio: 0.080
#   pulse 15: activated nodes: 11464, borderline nodes: 0, overall activation: 4752.377, activation diff: 306.955, ratio: 0.065

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4752.4
#   number of spread. activ. pulses: 15
#   running time: 1591

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9658642   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9248369   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.91843975   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.893234   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.84809846   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.7871447   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7607915   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7597097   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.75450164   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.75336134   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.75321186   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7526405   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.75250363   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.7524259   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.7522466   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7514814   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7511264   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.75100243   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.7509448   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7504988   REFERENCES:SIMDATES:SIMLOC
