###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 5.315, activation diff: 5.315, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 19.484, activation diff: 17.980, ratio: 0.923
#   pulse 3: activated nodes: 8058, borderline nodes: 6554, overall activation: 23.068, activation diff: 6.835, ratio: 0.296
#   pulse 4: activated nodes: 8736, borderline nodes: 7214, overall activation: 179.028, activation diff: 156.513, ratio: 0.874
#   pulse 5: activated nodes: 9330, borderline nodes: 5821, overall activation: 386.449, activation diff: 207.469, ratio: 0.537
#   pulse 6: activated nodes: 10818, borderline nodes: 6457, overall activation: 1128.952, activation diff: 742.508, ratio: 0.658
#   pulse 7: activated nodes: 11161, borderline nodes: 3983, overall activation: 2116.667, activation diff: 987.714, ratio: 0.467
#   pulse 8: activated nodes: 11318, borderline nodes: 2272, overall activation: 3306.751, activation diff: 1190.085, ratio: 0.360
#   pulse 9: activated nodes: 11406, borderline nodes: 834, overall activation: 4424.589, activation diff: 1117.838, ratio: 0.253
#   pulse 10: activated nodes: 11436, borderline nodes: 346, overall activation: 5346.745, activation diff: 922.156, ratio: 0.172
#   pulse 11: activated nodes: 11442, borderline nodes: 192, overall activation: 6037.877, activation diff: 691.132, ratio: 0.114
#   pulse 12: activated nodes: 11445, borderline nodes: 94, overall activation: 6529.727, activation diff: 491.850, ratio: 0.075
#   pulse 13: activated nodes: 11448, borderline nodes: 67, overall activation: 6867.903, activation diff: 338.176, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 6867.9
#   number of spread. activ. pulses: 13
#   running time: 1507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9962143   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97839636   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9773282   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9597119   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91048956   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.8968837   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89677787   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   8   0.89610493   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.89584625   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.89561915   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.8955459   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.89552486   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.8954638   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.895457   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.89543796   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   16   0.89540344   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.8953133   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   18   0.89529705   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.895296   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_136   20   0.8952391   REFERENCES:SIMDATES:SIMLOC
