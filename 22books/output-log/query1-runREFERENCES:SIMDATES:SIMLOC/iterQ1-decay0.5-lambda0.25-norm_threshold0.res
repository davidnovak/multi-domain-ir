###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.305, activation diff: 10.305, ratio: 1.411
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 152.447, activation diff: 150.756, ratio: 0.989
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 157.384, activation diff: 71.766, ratio: 0.456
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 913.432, activation diff: 756.174, ratio: 0.828
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1318.198, activation diff: 404.767, ratio: 0.307
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1785.141, activation diff: 466.943, ratio: 0.262
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2063.216, activation diff: 278.075, ratio: 0.135
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2227.638, activation diff: 164.422, ratio: 0.074
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2324.365, activation diff: 96.727, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2324.4
#   number of spread. activ. pulses: 9
#   running time: 1399

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990792   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9881345   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9840739   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96468806   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9176034   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8894993   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49957597   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49863964   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4973978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49713945   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4962363   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   12   0.4958892   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.49586284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.49516928   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.4947426   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.49455637   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.49393097   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.493802   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.49360698   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.49340346   REFERENCES:SIMDATES:SIMLOC
