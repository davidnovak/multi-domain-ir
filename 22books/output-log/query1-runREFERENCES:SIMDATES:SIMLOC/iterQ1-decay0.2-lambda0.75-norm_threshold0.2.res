###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.956, activation diff: 1.956, ratio: 0.395
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 3.923, activation diff: 1.250, ratio: 0.318
#   pulse 3: activated nodes: 4796, borderline nodes: 4755, overall activation: 3.020, activation diff: 0.915, ratio: 0.303
#   pulse 4: activated nodes: 4796, borderline nodes: 4755, overall activation: 2.287, activation diff: 0.733, ratio: 0.320
#   pulse 5: activated nodes: 4796, borderline nodes: 4755, overall activation: 1.723, activation diff: 0.564, ratio: 0.327
#   pulse 6: activated nodes: 4796, borderline nodes: 4755, overall activation: 1.292, activation diff: 0.431, ratio: 0.333
#   pulse 7: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.969, activation diff: 0.323, ratio: 0.333
#   pulse 8: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.727, activation diff: 0.242, ratio: 0.333
#   pulse 9: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.545, activation diff: 0.182, ratio: 0.333
#   pulse 10: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.409, activation diff: 0.136, ratio: 0.333
#   pulse 11: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.307, activation diff: 0.102, ratio: 0.333
#   pulse 12: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.230, activation diff: 0.077, ratio: 0.333
#   pulse 13: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.172, activation diff: 0.057, ratio: 0.333
#   pulse 14: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.129, activation diff: 0.043, ratio: 0.333
#   pulse 15: activated nodes: 4796, borderline nodes: 4755, overall activation: 0.097, activation diff: 0.032, ratio: 0.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 4796
#   final overall activation: 0.1
#   number of spread. activ. pulses: 15
#   running time: 740

###################################
# top k results in TREC format: 

1   Q1   lifeofstratfordc02laneuoft_185   1   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   2   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_239   3   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   5   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   6   0.013363461   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_376   7   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_381   8   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_385   9   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_383   10   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_200   11   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_368   12   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_367   13   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_365   14   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_197   15   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_206   16   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_205   17   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_366   18   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_191   19   0.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   miningcampsstudy00shin_202   20   0.0   REFERENCES:SIMDATES:SIMLOC
