###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 1.824, activation diff: 7.824, ratio: 4.290
#   pulse 2: activated nodes: 4796, borderline nodes: 4755, overall activation: 12.338, activation diff: 14.161, ratio: 1.148
#   pulse 3: activated nodes: 5198, borderline nodes: 4392, overall activation: 1.935, activation diff: 14.273, ratio: 7.375
#   pulse 4: activated nodes: 6122, borderline nodes: 5313, overall activation: 105.084, activation diff: 107.020, ratio: 1.018
#   pulse 5: activated nodes: 6884, borderline nodes: 4727, overall activation: 16.582, activation diff: 121.666, ratio: 7.337
#   pulse 6: activated nodes: 8918, borderline nodes: 6697, overall activation: 843.375, activation diff: 859.957, ratio: 1.020
#   pulse 7: activated nodes: 9484, borderline nodes: 4962, overall activation: 102.584, activation diff: 945.948, ratio: 9.221
#   pulse 8: activated nodes: 10241, borderline nodes: 5426, overall activation: 1377.502, activation diff: 1478.688, ratio: 1.073
#   pulse 9: activated nodes: 10357, borderline nodes: 4864, overall activation: 148.058, activation diff: 1523.548, ratio: 10.290
#   pulse 10: activated nodes: 10464, borderline nodes: 4846, overall activation: 1469.263, activation diff: 1611.899, ratio: 1.097
#   pulse 11: activated nodes: 10481, borderline nodes: 4757, overall activation: 157.824, activation diff: 1618.628, ratio: 10.256
#   pulse 12: activated nodes: 10488, borderline nodes: 4739, overall activation: 1483.523, activation diff: 1630.311, ratio: 1.099
#   pulse 13: activated nodes: 10494, borderline nodes: 4693, overall activation: 166.905, activation diff: 1631.786, ratio: 9.777
#   pulse 14: activated nodes: 10496, borderline nodes: 4686, overall activation: 1502.618, activation diff: 1640.384, ratio: 1.092
#   pulse 15: activated nodes: 10499, borderline nodes: 4663, overall activation: 201.905, activation diff: 1634.662, ratio: 8.096

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10499
#   final overall activation: 201.9
#   number of spread. activ. pulses: 15
#   running time: 1657

###################################
# top k results in TREC format: 

1   Q1   bookman44unkngoog_700   1   0.12556635   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_4   2   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_73   3   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   timetable1974752univ_86   4   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   timetable1974752univ_88   5   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   youngmidshipman00morrgoog_44   6   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   youngmidshipman00morrgoog_42   7   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   youngmidshipman00morrgoog_26   8   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_18   9   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_49   10   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_9   11   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_7   12   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_22   13   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_285   14   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_284   15   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_283   16   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_282   17   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_286   18   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_291   19   0.09186708   REFERENCES:SIMDATES:SIMLOC
1   Q1   youngmidshipman00morrgoog_5   20   0.09186708   REFERENCES:SIMDATES:SIMLOC
