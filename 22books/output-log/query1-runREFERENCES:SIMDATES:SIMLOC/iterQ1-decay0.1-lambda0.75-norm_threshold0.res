###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.435, activation diff: 3.435, ratio: 0.534
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 36.132, activation diff: 31.645, ratio: 0.876
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 79.730, activation diff: 44.907, ratio: 0.563
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 217.932, activation diff: 138.777, ratio: 0.637
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 480.474, activation diff: 262.640, ratio: 0.547
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 905.679, activation diff: 425.205, ratio: 0.469
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 1477.926, activation diff: 572.246, ratio: 0.387
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 2147.573, activation diff: 669.647, ratio: 0.312
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 2853.947, activation diff: 706.374, ratio: 0.248
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 3543.921, activation diff: 689.974, ratio: 0.195
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 4183.648, activation diff: 639.726, ratio: 0.153
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 4757.236, activation diff: 573.588, ratio: 0.121
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 5259.813, activation diff: 502.577, ratio: 0.096
#   pulse 14: activated nodes: 11464, borderline nodes: 0, overall activation: 5692.706, activation diff: 432.893, ratio: 0.076
#   pulse 15: activated nodes: 11464, borderline nodes: 0, overall activation: 6060.669, activation diff: 367.963, ratio: 0.061

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6060.7
#   number of spread. activ. pulses: 15
#   running time: 1603

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96739024   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9291533   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.923947   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.8990989   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.8583491   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.8579922   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.85301197   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.8523897   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   9   0.85183847   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.8516219   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   11   0.85145307   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.85123986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.85123765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.85114276   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.8508145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.8505742   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.85004246   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   18   0.8500236   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.84980124   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   20   0.84951454   REFERENCES:SIMDATES:SIMLOC
