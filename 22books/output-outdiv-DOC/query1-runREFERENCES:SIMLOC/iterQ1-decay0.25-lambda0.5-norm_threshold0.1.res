###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 4230, borderline nodes: 4207, overall activation: 16.961, activation diff: 16.352, ratio: 0.964
#   pulse 3: activated nodes: 5573, borderline nodes: 4074, overall activation: 18.371, activation diff: 4.583, ratio: 0.249
#   pulse 4: activated nodes: 7575, borderline nodes: 6065, overall activation: 225.249, activation diff: 206.927, ratio: 0.919
#   pulse 5: activated nodes: 8477, borderline nodes: 4835, overall activation: 463.731, activation diff: 238.482, ratio: 0.514
#   pulse 6: activated nodes: 10751, borderline nodes: 6577, overall activation: 1239.712, activation diff: 775.981, ratio: 0.626
#   pulse 7: activated nodes: 11127, borderline nodes: 4058, overall activation: 2047.924, activation diff: 808.212, ratio: 0.395
#   pulse 8: activated nodes: 11296, borderline nodes: 2799, overall activation: 2828.199, activation diff: 780.275, ratio: 0.276
#   pulse 9: activated nodes: 11375, borderline nodes: 1686, overall activation: 3467.194, activation diff: 638.995, ratio: 0.184
#   pulse 10: activated nodes: 11400, borderline nodes: 1265, overall activation: 3939.139, activation diff: 471.945, ratio: 0.120
#   pulse 11: activated nodes: 11413, borderline nodes: 1046, overall activation: 4265.139, activation diff: 326.000, ratio: 0.076
#   pulse 12: activated nodes: 11418, borderline nodes: 949, overall activation: 4478.448, activation diff: 213.309, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11418
#   final overall activation: 4478.4
#   number of spread. activ. pulses: 12
#   running time: 1867

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9966107   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99499995   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.994066   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9897249   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9883975   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9771155   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.74740016   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.74727154   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.7472399   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   10   0.7471906   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7471794   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7471503   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.7471357   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.7470472   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.7470211   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   16   0.74701107   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.74700737   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.74697447   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.74696976   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.7469585   REFERENCES:SIMLOC
