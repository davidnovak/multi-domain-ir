###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 4230, borderline nodes: 4207, overall activation: 61.499, activation diff: 61.341, ratio: 0.997
#   pulse 3: activated nodes: 5699, borderline nodes: 3925, overall activation: 23.836, activation diff: 53.643, ratio: 2.251
#   pulse 4: activated nodes: 9205, borderline nodes: 7371, overall activation: 1262.871, activation diff: 1246.419, ratio: 0.987
#   pulse 5: activated nodes: 10121, borderline nodes: 4516, overall activation: 1368.491, activation diff: 318.853, ratio: 0.233
#   pulse 6: activated nodes: 11217, borderline nodes: 4589, overall activation: 3233.564, activation diff: 1865.287, ratio: 0.577
#   pulse 7: activated nodes: 11358, borderline nodes: 1597, overall activation: 4321.952, activation diff: 1088.388, ratio: 0.252
#   pulse 8: activated nodes: 11407, borderline nodes: 1048, overall activation: 4995.592, activation diff: 673.640, ratio: 0.135
#   pulse 9: activated nodes: 11418, borderline nodes: 747, overall activation: 5322.599, activation diff: 327.007, ratio: 0.061
#   pulse 10: activated nodes: 11427, borderline nodes: 643, overall activation: 5472.400, activation diff: 149.801, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11427
#   final overall activation: 5472.4
#   number of spread. activ. pulses: 10
#   running time: 1767

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998908   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99973214   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99964327   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9993131   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9970083   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.992691   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7999238   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.79991263   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.7999084   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.79990506   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.79990077   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.7998987   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.7998984   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.7998977   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7998888   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   16   0.7998886   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   17   0.79988223   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.79988104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_315   19   0.7998806   REFERENCES:SIMLOC
1   Q1   frenchschoolsth02instgoog_135   20   0.799878   REFERENCES:SIMLOC
