###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 291.406, activation diff: 289.561, ratio: 0.994
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 355.864, activation diff: 149.595, ratio: 0.420
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3470.682, activation diff: 3114.822, ratio: 0.897
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5624.659, activation diff: 2153.977, ratio: 0.383
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7145.228, activation diff: 1520.569, ratio: 0.213
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 7783.480, activation diff: 638.251, ratio: 0.082
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 8043.316, activation diff: 259.836, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8043.3
#   number of spread. activ. pulses: 8
#   running time: 2719

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993271   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992784   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99917865   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99900186   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9966265   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99253416   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8994147   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89938354   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.89935136   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.89934206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89933765   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.8993228   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.8993194   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.89931893   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.89931893   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.8993185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.89931625   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.8993113   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.89930725   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   20   0.89930534   REFERENCES:SIMLOC
