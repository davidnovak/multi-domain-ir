###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 118.242, activation diff: 115.263, ratio: 0.975
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 210.721, activation diff: 93.173, ratio: 0.442
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1306.601, activation diff: 1095.881, ratio: 0.839
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2609.371, activation diff: 1302.769, ratio: 0.499
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4037.622, activation diff: 1428.251, ratio: 0.354
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 5106.578, activation diff: 1068.956, ratio: 0.209
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 5799.690, activation diff: 693.111, ratio: 0.120
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 6223.541, activation diff: 423.852, ratio: 0.068
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 6477.428, activation diff: 253.887, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6477.4
#   number of spread. activ. pulses: 10
#   running time: 1782

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99513185   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99416095   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9932767   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9902519   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.98879   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9794445   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79538524   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79526925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79503953   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.79491055   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.7948984   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.794886   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.79484034   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.79483145   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7947866   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.7947824   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.79477334   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.7947165   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.7947049   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7946561   REFERENCES:SIMLOC
