###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 111.092, activation diff: 108.112, ratio: 0.973
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 196.141, activation diff: 85.744, ratio: 0.437
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1174.322, activation diff: 978.181, ratio: 0.833
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2320.676, activation diff: 1146.354, ratio: 0.494
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 3594.155, activation diff: 1273.479, ratio: 0.354
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 4568.892, activation diff: 974.737, ratio: 0.213
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 5207.884, activation diff: 638.992, ratio: 0.123
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 5602.577, activation diff: 394.693, ratio: 0.070
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 5839.559, activation diff: 236.982, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5839.6
#   number of spread. activ. pulses: 10
#   running time: 1837

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951265   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99410313   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99318457   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.98992   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9885223   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9787382   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7456661   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74555826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74533343   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7452131   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.7452062   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.74519086   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   13   0.7451335   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.74513286   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7450925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.7450923   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.7450893   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.74503404   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.74501586   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.74497116   REFERENCES:SIMLOC
