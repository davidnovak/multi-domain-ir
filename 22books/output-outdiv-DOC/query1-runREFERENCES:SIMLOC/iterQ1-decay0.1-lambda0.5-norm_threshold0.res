###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 132.543, activation diff: 129.564, ratio: 0.978
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 240.558, activation diff: 108.710, ratio: 0.452
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1593.264, activation diff: 1352.705, ratio: 0.849
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3237.366, activation diff: 1644.103, ratio: 0.508
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4979.258, activation diff: 1741.892, ratio: 0.350
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 6235.261, activation diff: 1256.003, ratio: 0.201
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 7031.804, activation diff: 796.542, ratio: 0.113
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 7514.442, activation diff: 482.639, ratio: 0.064
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 7804.089, activation diff: 289.647, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7804.1
#   number of spread. activ. pulses: 10
#   running time: 1686

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951395   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9942546   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9934292   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.990824   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.989208   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98059756   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89482176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8946893   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8944479   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.8943032   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.8942799   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.8942729   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.8942523   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.89422333   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89417005   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.8941574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.89413875   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.89408076   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8940777   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.8940357   REFERENCES:SIMLOC
