###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 259.398, activation diff: 257.553, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 305.121, activation diff: 123.338, ratio: 0.404
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2844.038, activation diff: 2538.923, ratio: 0.893
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4577.449, activation diff: 1733.411, ratio: 0.379
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 5888.625, activation diff: 1311.176, ratio: 0.223
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 6457.183, activation diff: 568.558, ratio: 0.088
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 6687.356, activation diff: 230.173, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6687.4
#   number of spread. activ. pulses: 8
#   running time: 1615

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932706   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999273   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991648   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9988971   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9964352   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99210215   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79947793   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7994505   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.79942036   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.79941475   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7994097   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.79939675   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79939455   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.79939413   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.79939187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.7993916   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.79939044   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.79938483   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.799382   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   20   0.799379   REFERENCES:SIMLOC
