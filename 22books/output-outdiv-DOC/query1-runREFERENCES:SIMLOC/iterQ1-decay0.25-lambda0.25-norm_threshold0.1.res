###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 4230, borderline nodes: 4207, overall activation: 57.728, activation diff: 57.570, ratio: 0.997
#   pulse 3: activated nodes: 5699, borderline nodes: 3925, overall activation: 22.240, activation diff: 50.202, ratio: 2.257
#   pulse 4: activated nodes: 9179, borderline nodes: 7348, overall activation: 1118.269, activation diff: 1102.907, ratio: 0.986
#   pulse 5: activated nodes: 10041, borderline nodes: 4574, overall activation: 1176.223, activation diff: 258.292, ratio: 0.220
#   pulse 6: activated nodes: 11208, borderline nodes: 4781, overall activation: 2725.826, activation diff: 1549.790, ratio: 0.569
#   pulse 7: activated nodes: 11343, borderline nodes: 1910, overall activation: 3607.007, activation diff: 881.181, ratio: 0.244
#   pulse 8: activated nodes: 11382, borderline nodes: 1413, overall activation: 4169.569, activation diff: 562.563, ratio: 0.135
#   pulse 9: activated nodes: 11397, borderline nodes: 1096, overall activation: 4453.569, activation diff: 284.000, ratio: 0.064
#   pulse 10: activated nodes: 11402, borderline nodes: 1000, overall activation: 4594.731, activation diff: 141.162, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11402
#   final overall activation: 4594.7
#   number of spread. activ. pulses: 10
#   running time: 1814

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99987954   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997113   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996209   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99929035   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9967876   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9924034   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.74992025   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.74990803   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.74990416   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7499013   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.74989617   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.74989486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.74989384   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.7498927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7498841   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   16   0.7498834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_315   17   0.7498754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.7498751   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   19   0.74987435   REFERENCES:SIMLOC
1   Q1   frenchschoolsth02instgoog_135   20   0.7498716   REFERENCES:SIMLOC
