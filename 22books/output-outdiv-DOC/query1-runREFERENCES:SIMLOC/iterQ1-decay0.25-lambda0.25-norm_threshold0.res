###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 243.394, activation diff: 241.549, ratio: 0.992
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 280.860, activation diff: 111.201, ratio: 0.396
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2554.054, activation diff: 2273.202, ratio: 0.890
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4084.660, activation diff: 1530.607, ratio: 0.375
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 5287.671, activation diff: 1203.011, ratio: 0.228
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 5818.021, activation diff: 530.350, ratio: 0.091
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 6035.012, activation diff: 216.990, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6035.0
#   number of spread. activ. pulses: 8
#   running time: 1762

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932706   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992695   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99915564   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9988235   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99630666   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99181944   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7495097   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7494841   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.74945486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7494509   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74944574   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.7494337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.749432   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7494316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7494291   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.7494286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.7494261   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.74942136   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.7494194   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.7494171   REFERENCES:SIMLOC
