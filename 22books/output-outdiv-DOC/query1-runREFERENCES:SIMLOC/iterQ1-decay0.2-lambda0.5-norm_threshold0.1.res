###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 4230, borderline nodes: 4207, overall activation: 17.956, activation diff: 17.348, ratio: 0.966
#   pulse 3: activated nodes: 5573, borderline nodes: 4074, overall activation: 19.534, activation diff: 4.859, ratio: 0.249
#   pulse 4: activated nodes: 7603, borderline nodes: 6092, overall activation: 257.053, activation diff: 237.567, ratio: 0.924
#   pulse 5: activated nodes: 8670, borderline nodes: 4894, overall activation: 530.737, activation diff: 273.684, ratio: 0.516
#   pulse 6: activated nodes: 10814, borderline nodes: 6544, overall activation: 1428.901, activation diff: 898.164, ratio: 0.629
#   pulse 7: activated nodes: 11167, borderline nodes: 3790, overall activation: 2387.803, activation diff: 958.902, ratio: 0.402
#   pulse 8: activated nodes: 11319, borderline nodes: 2436, overall activation: 3342.555, activation diff: 954.752, ratio: 0.286
#   pulse 9: activated nodes: 11394, borderline nodes: 1356, overall activation: 4120.810, activation diff: 778.255, ratio: 0.189
#   pulse 10: activated nodes: 11413, borderline nodes: 964, overall activation: 4694.782, activation diff: 573.972, ratio: 0.122
#   pulse 11: activated nodes: 11420, borderline nodes: 749, overall activation: 5080.227, activation diff: 385.445, ratio: 0.076
#   pulse 12: activated nodes: 11425, borderline nodes: 659, overall activation: 5326.396, activation diff: 246.170, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11425
#   final overall activation: 5326.4
#   number of spread. activ. pulses: 12
#   running time: 1914

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9967729   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99521667   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9943467   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9902855   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.989372   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97853553   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.79736817   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7972261   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.79719734   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   10   0.7971372   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.79713297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7970979   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.79708934   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.79699564   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.79697204   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   16   0.79696107   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.796955   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   18   0.79692245   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7969197   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.79690516   REFERENCES:SIMLOC
