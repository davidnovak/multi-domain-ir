###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 4230, borderline nodes: 4207, overall activation: 69.040, activation diff: 68.882, ratio: 0.998
#   pulse 3: activated nodes: 5699, borderline nodes: 3925, overall activation: 27.119, activation diff: 60.616, ratio: 2.235
#   pulse 4: activated nodes: 9509, borderline nodes: 7660, overall activation: 1588.454, activation diff: 1569.826, ratio: 0.988
#   pulse 5: activated nodes: 10344, borderline nodes: 4567, overall activation: 1815.395, activation diff: 469.902, ratio: 0.259
#   pulse 6: activated nodes: 11247, borderline nodes: 4302, overall activation: 4412.244, activation diff: 2597.062, ratio: 0.589
#   pulse 7: activated nodes: 11384, borderline nodes: 1297, overall activation: 5949.497, activation diff: 1537.253, ratio: 0.258
#   pulse 8: activated nodes: 11422, borderline nodes: 768, overall activation: 6803.026, activation diff: 853.528, ratio: 0.125
#   pulse 9: activated nodes: 11435, borderline nodes: 498, overall activation: 7171.781, activation diff: 368.755, ratio: 0.051
#   pulse 10: activated nodes: 11435, borderline nodes: 423, overall activation: 7330.475, activation diff: 158.694, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11435
#   final overall activation: 7330.5
#   number of spread. activ. pulses: 10
#   running time: 1963

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9999074   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99976695   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99968004   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9993421   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9973066   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99310285   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8999279   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.89991903   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.8999148   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.89991045   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.8999078   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   12   0.8999054   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.89990515   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8999046   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.89989686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8998962   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   17   0.8998912   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   18   0.8998911   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   19   0.8998911   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_136   20   0.89988923   REFERENCES:SIMLOC
