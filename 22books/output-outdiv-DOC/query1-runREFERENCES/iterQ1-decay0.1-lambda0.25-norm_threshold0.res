###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 285.421, activation diff: 283.917, ratio: 0.995
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 276.751, activation diff: 130.166, ratio: 0.470
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2887.480, activation diff: 2610.883, ratio: 0.904
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3991.127, activation diff: 1103.647, ratio: 0.277
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5123.244, activation diff: 1132.117, ratio: 0.221
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 5612.502, activation diff: 489.258, ratio: 0.087
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5834.657, activation diff: 222.155, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5834.7
#   number of spread. activ. pulses: 8
#   running time: 1769

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993145   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992156   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99885464   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9986323   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99578375   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9893551   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.899367   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89935404   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89933383   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.8993093   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.8993063   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   12   0.8993048   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.89930475   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.8993035   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.8992902   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.8992862   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.8992857   REFERENCES
1   Q1   englishcyclopae05kniggoog_330   18   0.8992839   REFERENCES
1   Q1   essentialsinmod01howegoog_283   19   0.8992832   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   20   0.8992793   REFERENCES
