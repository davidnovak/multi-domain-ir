###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 108.859, activation diff: 105.880, ratio: 0.973
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 175.606, activation diff: 67.502, ratio: 0.384
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1005.551, activation diff: 829.948, ratio: 0.825
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1785.849, activation diff: 780.298, ratio: 0.437
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2566.618, activation diff: 780.769, ratio: 0.304
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 3132.766, activation diff: 566.148, ratio: 0.181
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3506.953, activation diff: 374.187, ratio: 0.107
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 3743.175, activation diff: 236.221, ratio: 0.063
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3889.018, activation diff: 145.843, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 3889.0
#   number of spread. activ. pulses: 10
#   running time: 647

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9950952   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9937979   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9921412   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9890662   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9875115   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97148   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.74556696   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7454967   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.745293   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.7451639   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.74515617   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.74512005   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.7450843   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.745055   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   15   0.7450414   REFERENCES
1   Q1   europesincenapol00leveuoft_16   16   0.74498177   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7449715   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.7449391   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.74493194   REFERENCES
1   Q1   essentialsinmod01howegoog_14   20   0.74490994   REFERENCES
