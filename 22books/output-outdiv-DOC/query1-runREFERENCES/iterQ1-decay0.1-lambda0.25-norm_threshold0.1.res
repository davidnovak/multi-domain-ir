###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 3471, borderline nodes: 3448, overall activation: 69.040, activation diff: 68.883, ratio: 0.998
#   pulse 3: activated nodes: 5327, borderline nodes: 3553, overall activation: 26.941, activation diff: 60.496, ratio: 2.246
#   pulse 4: activated nodes: 8210, borderline nodes: 6361, overall activation: 1561.070, activation diff: 1543.721, ratio: 0.989
#   pulse 5: activated nodes: 9767, borderline nodes: 4388, overall activation: 1583.274, activation diff: 407.089, ratio: 0.257
#   pulse 6: activated nodes: 10709, borderline nodes: 4286, overall activation: 3721.346, activation diff: 2140.430, ratio: 0.575
#   pulse 7: activated nodes: 11131, borderline nodes: 2928, overall activation: 4586.586, activation diff: 865.242, ratio: 0.189
#   pulse 8: activated nodes: 11248, borderline nodes: 2314, overall activation: 5093.299, activation diff: 506.712, ratio: 0.099
#   pulse 9: activated nodes: 11279, borderline nodes: 1932, overall activation: 5317.063, activation diff: 223.764, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11279
#   final overall activation: 5317.1
#   number of spread. activ. pulses: 9
#   running time: 677

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995895   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9989561   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99850494   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9973628   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9951041   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98616004   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.89966905   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   8   0.8996301   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.89962834   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.899607   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   11   0.8996028   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.8995918   REFERENCES
1   Q1   essentialsinmod01howegoog_14   13   0.8995747   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.89957166   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   15   0.8995701   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.8995515   REFERENCES
1   Q1   essentialsinmod01howegoog_331   17   0.89953554   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   18   0.89953065   REFERENCES
1   Q1   frenchschoolsth02instgoog_135   19   0.89951754   REFERENCES
1   Q1   essentialsinmod01howegoog_187   20   0.89951503   REFERENCES
