###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 115.867, activation diff: 112.888, ratio: 0.974
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 188.347, activation diff: 73.235, ratio: 0.389
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1125.539, activation diff: 937.195, ratio: 0.833
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2009.333, activation diff: 883.794, ratio: 0.440
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2906.811, activation diff: 897.478, ratio: 0.309
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3557.951, activation diff: 651.140, ratio: 0.183
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 3990.063, activation diff: 432.112, ratio: 0.108
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 4264.602, activation diff: 274.539, ratio: 0.064
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 4435.477, activation diff: 170.875, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 4435.5
#   number of spread. activ. pulses: 10
#   running time: 577

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99510103   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9938716   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99228907   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.98942316   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.98780173   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9729433   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7952826   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79520607   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7950046   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.79485977   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.79485285   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7948201   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.79478776   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.7947485   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   15   0.79472804   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.79467225   REFERENCES
1   Q1   europesincenapol00leveuoft_16   17   0.79466534   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.794655   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7946181   REFERENCES
1   Q1   politicalsketche00retsrich_154   20   0.79459745   REFERENCES
