###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 3471, borderline nodes: 3448, overall activation: 16.959, activation diff: 16.354, ratio: 0.964
#   pulse 3: activated nodes: 5165, borderline nodes: 3666, overall activation: 18.151, activation diff: 4.462, ratio: 0.246
#   pulse 4: activated nodes: 6921, borderline nodes: 5411, overall activation: 223.477, activation diff: 205.383, ratio: 0.919
#   pulse 5: activated nodes: 8013, borderline nodes: 4382, overall activation: 452.893, activation diff: 229.416, ratio: 0.507
#   pulse 6: activated nodes: 9750, borderline nodes: 5678, overall activation: 1174.404, activation diff: 721.511, ratio: 0.614
#   pulse 7: activated nodes: 10723, borderline nodes: 4304, overall activation: 1836.357, activation diff: 661.953, ratio: 0.360
#   pulse 8: activated nodes: 10999, borderline nodes: 3680, overall activation: 2387.426, activation diff: 551.069, ratio: 0.231
#   pulse 9: activated nodes: 11127, borderline nodes: 3101, overall activation: 2778.517, activation diff: 391.091, ratio: 0.141
#   pulse 10: activated nodes: 11172, borderline nodes: 2808, overall activation: 3037.735, activation diff: 259.218, ratio: 0.085
#   pulse 11: activated nodes: 11186, borderline nodes: 2630, overall activation: 3203.668, activation diff: 165.934, ratio: 0.052
#   pulse 12: activated nodes: 11198, borderline nodes: 2496, overall activation: 3308.064, activation diff: 104.395, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11198
#   final overall activation: 3308.1
#   number of spread. activ. pulses: 12
#   running time: 717

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965795   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99489653   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99352854   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.98899996   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9882517   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9705415   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.74733984   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74724764   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   9   0.7472104   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.7471723   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   11   0.74716204   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   12   0.74710834   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7470861   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7470244   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   15   0.746994   REFERENCES
1   Q1   essentialsinmod01howegoog_331   16   0.7469865   REFERENCES
1   Q1   essentialsinmod01howegoog_14   17   0.74696755   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.7469378   REFERENCES
1   Q1   politicalsketche00retsrich_156   19   0.74693346   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   20   0.74692655   REFERENCES
