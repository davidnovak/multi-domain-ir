###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 3471, borderline nodes: 3448, overall activation: 61.498, activation diff: 61.341, ratio: 0.997
#   pulse 3: activated nodes: 5327, borderline nodes: 3553, overall activation: 23.578, activation diff: 53.437, ratio: 2.266
#   pulse 4: activated nodes: 8039, borderline nodes: 6205, overall activation: 1236.073, activation diff: 1220.551, ratio: 0.987
#   pulse 5: activated nodes: 9568, borderline nodes: 4305, overall activation: 1219.338, activation diff: 291.088, ratio: 0.239
#   pulse 6: activated nodes: 10652, borderline nodes: 4467, overall activation: 2779.666, activation diff: 1561.905, ratio: 0.562
#   pulse 7: activated nodes: 11093, borderline nodes: 3162, overall activation: 3403.524, activation diff: 623.860, ratio: 0.183
#   pulse 8: activated nodes: 11192, borderline nodes: 2676, overall activation: 3768.160, activation diff: 364.636, ratio: 0.097
#   pulse 9: activated nodes: 11216, borderline nodes: 2320, overall activation: 3928.985, activation diff: 160.825, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11216
#   final overall activation: 3929.0
#   number of spread. activ. pulses: 9
#   running time: 570

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995183   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9988249   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9983182   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9972356   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9946139   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9840539   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.79964197   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   8   0.7996016   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.7996015   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.7995786   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   11   0.7995732   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7995689   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7995447   REFERENCES
1   Q1   essentialsinmod01howegoog_14   14   0.79953814   REFERENCES
1   Q1   encyclopediaame28unkngoog_320   15   0.7995354   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.7995227   REFERENCES
1   Q1   essentialsinmod01howegoog_331   17   0.79950804   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   18   0.79949087   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.799477   REFERENCES
1   Q1   frenchschoolsth02instgoog_135   20   0.7994769   REFERENCES
