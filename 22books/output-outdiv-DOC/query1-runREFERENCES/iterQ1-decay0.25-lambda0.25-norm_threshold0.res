###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 238.371, activation diff: 236.867, ratio: 0.994
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 219.661, activation diff: 97.204, ratio: 0.443
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2084.840, activation diff: 1865.356, ratio: 0.895
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2832.963, activation diff: 748.123, ratio: 0.264
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 3575.126, activation diff: 742.163, ratio: 0.208
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 3888.020, activation diff: 312.893, ratio: 0.080
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4021.021, activation diff: 133.001, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4021.0
#   number of spread. activ. pulses: 8
#   running time: 553

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993145   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9991952   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99876076   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9983239   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9953855   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9873444   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7494725   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7494617   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.74944407   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.74942315   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7494204   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7494201   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.7494186   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.7494134   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.7494078   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   16   0.7494042   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7494039   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.7493962   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   19   0.7493961   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.74939585   REFERENCES
