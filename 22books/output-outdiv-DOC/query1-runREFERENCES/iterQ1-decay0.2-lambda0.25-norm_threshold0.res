###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 254.055, activation diff: 252.550, ratio: 0.994
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 238.179, activation diff: 107.679, ratio: 0.452
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2336.104, activation diff: 2098.094, ratio: 0.898
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3193.448, activation diff: 857.345, ratio: 0.268
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4057.903, activation diff: 864.454, ratio: 0.213
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 4426.349, activation diff: 368.446, ratio: 0.083
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4587.462, activation diff: 161.112, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 4587.5
#   number of spread. activ. pulses: 8
#   running time: 561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993145   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992032   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99879724   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9984431   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9955337   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9881522   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7994374   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7994259   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7994075   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.7993854   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.79938257   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7993818   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.79938096   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.7993773   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.7993688   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.7993649   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.7993648   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   18   0.79935753   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7993569   REFERENCES
1   Q1   europesincenapol00leveuoft_16   20   0.7993561   REFERENCES
