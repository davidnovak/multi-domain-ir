###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 129.883, activation diff: 126.904, ratio: 0.977
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 214.317, activation diff: 85.188, ratio: 0.397
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1390.981, activation diff: 1176.668, ratio: 0.846
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2505.890, activation diff: 1114.909, ratio: 0.445
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3662.455, activation diff: 1156.565, ratio: 0.316
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4500.522, activation diff: 838.067, ratio: 0.186
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5059.777, activation diff: 559.255, ratio: 0.111
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 5417.626, activation diff: 357.850, ratio: 0.066
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 5642.096, activation diff: 224.469, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5642.1
#   number of spread. activ. pulses: 10
#   running time: 580

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951094   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9939904   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99252826   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9900353   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9882773   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9752618   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8947083   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.8946196   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89442027   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   10   0.8942473   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.89423895   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.89421415   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.89418745   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.8941273   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   15   0.89409614   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.89407814   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.89406216   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.8940262   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.89398706   REFERENCES
1   Q1   politicalsketche00retsrich_154   20   0.8939735   REFERENCES
