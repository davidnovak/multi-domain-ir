###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 238.518, activation diff: 236.902, ratio: 0.993
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 224.214, activation diff: 95.836, ratio: 0.427
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2104.829, activation diff: 1880.694, ratio: 0.894
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2881.588, activation diff: 776.758, ratio: 0.270
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 3647.520, activation diff: 765.932, ratio: 0.210
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 3975.089, activation diff: 327.569, ratio: 0.082
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 4115.370, activation diff: 140.280, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4115.4
#   number of spread. activ. pulses: 8
#   running time: 539

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993145   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992064   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987696   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9986117   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.995413   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98837847   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7494911   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7494618   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7494512   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.7494235   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   11   0.7494207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.7494204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.7494201   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.74941885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   15   0.74940807   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.74940574   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   17   0.7494042   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   18   0.74940026   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.74939626   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_77   20   0.74939615   REFERENCES:SIMDATES
