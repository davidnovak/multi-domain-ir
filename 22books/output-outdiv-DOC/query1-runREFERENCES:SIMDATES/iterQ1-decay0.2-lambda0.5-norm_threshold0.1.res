###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 17.955, activation diff: 17.349, ratio: 0.966
#   pulse 3: activated nodes: 5165, borderline nodes: 3666, overall activation: 19.301, activation diff: 4.731, ratio: 0.245
#   pulse 4: activated nodes: 6947, borderline nodes: 5436, overall activation: 255.149, activation diff: 235.905, ratio: 0.925
#   pulse 5: activated nodes: 8157, borderline nodes: 4402, overall activation: 518.499, activation diff: 263.350, ratio: 0.508
#   pulse 6: activated nodes: 9788, borderline nodes: 5654, overall activation: 1358.100, activation diff: 839.601, ratio: 0.618
#   pulse 7: activated nodes: 10792, borderline nodes: 4203, overall activation: 2141.272, activation diff: 783.172, ratio: 0.366
#   pulse 8: activated nodes: 11054, borderline nodes: 3468, overall activation: 2808.648, activation diff: 667.375, ratio: 0.238
#   pulse 9: activated nodes: 11161, borderline nodes: 2921, overall activation: 3287.806, activation diff: 479.158, ratio: 0.146
#   pulse 10: activated nodes: 11203, borderline nodes: 2569, overall activation: 3608.356, activation diff: 320.551, ratio: 0.089
#   pulse 11: activated nodes: 11241, borderline nodes: 2374, overall activation: 3815.603, activation diff: 207.247, ratio: 0.054
#   pulse 12: activated nodes: 11253, borderline nodes: 2236, overall activation: 3947.579, activation diff: 131.976, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11253
#   final overall activation: 3947.6
#   number of spread. activ. pulses: 12
#   running time: 730

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9967412   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99512744   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9938696   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.98954153   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.989383   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9741823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.79730797   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7972033   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   9   0.7971686   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.79711884   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.7971152   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   12   0.79706275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.79703534   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.7969732   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.7969464   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   16   0.79693943   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   17   0.79691434   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   18   0.7968898   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.7968837   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.796874   REFERENCES:SIMDATES
