###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 254.207, activation diff: 252.591, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 242.909, activation diff: 105.966, ratio: 0.436
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2355.766, activation diff: 2112.932, ratio: 0.897
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3244.166, activation diff: 888.399, ratio: 0.274
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4127.398, activation diff: 883.232, ratio: 0.214
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 4508.549, activation diff: 381.151, ratio: 0.085
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 4676.435, activation diff: 167.886, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 4676.4
#   number of spread. activ. pulses: 8
#   running time: 576

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993145   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9992138   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9988057   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9987055   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99555874   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98900366   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79945767   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.799426   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.799415   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.79938567   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   11   0.79938316   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   12   0.79938304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7993826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.79938185   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   15   0.79936904   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.79936683   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   17   0.7993648   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   18   0.7993612   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   19   0.799361   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_77   20   0.7993576   REFERENCES:SIMDATES
