###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 69.040, activation diff: 68.883, ratio: 0.998
#   pulse 3: activated nodes: 5327, borderline nodes: 3553, overall activation: 26.942, activation diff: 60.497, ratio: 2.245
#   pulse 4: activated nodes: 8210, borderline nodes: 6361, overall activation: 1561.119, activation diff: 1543.771, ratio: 0.989
#   pulse 5: activated nodes: 9767, borderline nodes: 4388, overall activation: 1583.981, activation diff: 407.784, ratio: 0.257
#   pulse 6: activated nodes: 10718, borderline nodes: 4295, overall activation: 3735.752, activation diff: 2153.794, ratio: 0.577
#   pulse 7: activated nodes: 11134, borderline nodes: 2918, overall activation: 4616.521, activation diff: 880.770, ratio: 0.191
#   pulse 8: activated nodes: 11250, borderline nodes: 2307, overall activation: 5122.799, activation diff: 506.279, ratio: 0.099
#   pulse 9: activated nodes: 11283, borderline nodes: 1914, overall activation: 5349.597, activation diff: 226.798, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11283
#   final overall activation: 5349.6
#   number of spread. activ. pulses: 9
#   running time: 794

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9989575   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9985064   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99737346   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9951208   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9869804   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.89966905   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   8   0.8996301   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   9   0.8996284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.899607   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   11   0.8996028   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.8995918   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   13   0.89957476   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.89957166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.8995701   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.8995515   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   17   0.89953566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   18   0.89953077   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   19   0.8995198   REFERENCES:SIMDATES
1   Q1   frenchschoolsth02instgoog_135   20   0.89951754   REFERENCES:SIMDATES
