###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 61.498, activation diff: 61.341, ratio: 0.997
#   pulse 3: activated nodes: 5327, borderline nodes: 3553, overall activation: 23.579, activation diff: 53.438, ratio: 2.266
#   pulse 4: activated nodes: 8039, borderline nodes: 6205, overall activation: 1236.116, activation diff: 1220.596, ratio: 0.987
#   pulse 5: activated nodes: 9568, borderline nodes: 4305, overall activation: 1220.429, activation diff: 292.168, ratio: 0.239
#   pulse 6: activated nodes: 10659, borderline nodes: 4474, overall activation: 2791.234, activation diff: 1572.213, ratio: 0.563
#   pulse 7: activated nodes: 11094, borderline nodes: 3152, overall activation: 3428.231, activation diff: 636.998, ratio: 0.186
#   pulse 8: activated nodes: 11194, borderline nodes: 2673, overall activation: 3794.014, activation diff: 365.783, ratio: 0.096
#   pulse 9: activated nodes: 11220, borderline nodes: 2306, overall activation: 3956.523, activation diff: 162.509, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11220
#   final overall activation: 3956.5
#   number of spread. activ. pulses: 9
#   running time: 604

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995183   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.998827   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9983207   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9972624   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9946391   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9852686   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.79964197   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   8   0.7996016   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   9   0.79960155   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.7995786   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   11   0.7995732   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7995689   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7995447   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   14   0.79953825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.7995354   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.7995227   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   17   0.79950845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   18   0.79949105   REFERENCES:SIMDATES
1   Q1   frenchschoolsth02instgoog_135   19   0.79947704   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.799477   REFERENCES:SIMDATES
