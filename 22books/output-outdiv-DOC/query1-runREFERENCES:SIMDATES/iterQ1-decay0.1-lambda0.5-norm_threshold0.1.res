###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 19.946, activation diff: 19.340, ratio: 0.970
#   pulse 3: activated nodes: 5165, borderline nodes: 3666, overall activation: 21.615, activation diff: 5.285, ratio: 0.245
#   pulse 4: activated nodes: 7161, borderline nodes: 5645, overall activation: 325.251, activation diff: 303.687, ratio: 0.934
#   pulse 5: activated nodes: 8335, borderline nodes: 4534, overall activation: 663.919, activation diff: 338.668, ratio: 0.510
#   pulse 6: activated nodes: 9990, borderline nodes: 5605, overall activation: 1790.937, activation diff: 1127.018, ratio: 0.629
#   pulse 7: activated nodes: 10920, borderline nodes: 3935, overall activation: 2850.197, activation diff: 1059.260, ratio: 0.372
#   pulse 8: activated nodes: 11119, borderline nodes: 3171, overall activation: 3757.992, activation diff: 907.795, ratio: 0.242
#   pulse 9: activated nodes: 11202, borderline nodes: 2568, overall activation: 4406.018, activation diff: 648.026, ratio: 0.147
#   pulse 10: activated nodes: 11249, borderline nodes: 2216, overall activation: 4839.865, activation diff: 433.847, ratio: 0.090
#   pulse 11: activated nodes: 11279, borderline nodes: 1987, overall activation: 5122.190, activation diff: 282.325, ratio: 0.055
#   pulse 12: activated nodes: 11286, borderline nodes: 1813, overall activation: 5303.540, activation diff: 181.350, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11286
#   final overall activation: 5303.5
#   number of spread. activ. pulses: 12
#   running time: 613

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99702376   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9955   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9944203   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9908601   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9904187   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97720563   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8972591   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.89712566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   9   0.8970934   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.8970318   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   11   0.8970189   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   12   0.8969848   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.89694035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.89688176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.8968657   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   16   0.8968404   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   17   0.8968287   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_187   18   0.8968226   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.8967848   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   20   0.8967788   REFERENCES:SIMDATES
