###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 57.727, activation diff: 57.571, ratio: 0.997
#   pulse 3: activated nodes: 5327, borderline nodes: 3553, overall activation: 21.960, activation diff: 49.971, ratio: 2.276
#   pulse 4: activated nodes: 8025, borderline nodes: 6194, overall activation: 1092.007, activation diff: 1077.423, ratio: 0.987
#   pulse 5: activated nodes: 9527, borderline nodes: 4385, overall activation: 1061.658, activation diff: 244.481, ratio: 0.230
#   pulse 6: activated nodes: 10624, borderline nodes: 4638, overall activation: 2375.192, activation diff: 1314.669, ratio: 0.554
#   pulse 7: activated nodes: 11078, borderline nodes: 3269, overall activation: 2903.513, activation diff: 528.323, ratio: 0.182
#   pulse 8: activated nodes: 11137, borderline nodes: 2901, overall activation: 3200.282, activation diff: 296.769, ratio: 0.093
#   pulse 9: activated nodes: 11169, borderline nodes: 2550, overall activation: 3330.700, activation diff: 130.417, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11169
#   final overall activation: 3330.7
#   number of spread. activ. pulses: 9
#   running time: 541

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99947155   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9987488   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9982027   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9971765   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99434227   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98414505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.74962354   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7495843   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   9   0.74958324   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.74956113   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   11   0.74955475   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.74955446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.74952877   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   14   0.74951667   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.749515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.7495059   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   17   0.749488   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   18   0.7494683   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7494627   REFERENCES:SIMDATES
1   Q1   frenchschoolsth02instgoog_135   20   0.7494513   REFERENCES:SIMDATES
