###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 3490, borderline nodes: 3467, overall activation: 16.959, activation diff: 16.354, ratio: 0.964
#   pulse 3: activated nodes: 5165, borderline nodes: 3666, overall activation: 18.151, activation diff: 4.462, ratio: 0.246
#   pulse 4: activated nodes: 6921, borderline nodes: 5411, overall activation: 223.477, activation diff: 205.383, ratio: 0.919
#   pulse 5: activated nodes: 8013, borderline nodes: 4382, overall activation: 452.942, activation diff: 229.465, ratio: 0.507
#   pulse 6: activated nodes: 9752, borderline nodes: 5680, overall activation: 1175.277, activation diff: 722.335, ratio: 0.615
#   pulse 7: activated nodes: 10724, borderline nodes: 4301, overall activation: 1840.591, activation diff: 665.314, ratio: 0.361
#   pulse 8: activated nodes: 11001, borderline nodes: 3659, overall activation: 2397.439, activation diff: 556.848, ratio: 0.232
#   pulse 9: activated nodes: 11128, borderline nodes: 3083, overall activation: 2794.449, activation diff: 397.010, ratio: 0.142
#   pulse 10: activated nodes: 11173, borderline nodes: 2803, overall activation: 3058.240, activation diff: 263.791, ratio: 0.086
#   pulse 11: activated nodes: 11189, borderline nodes: 2622, overall activation: 3227.377, activation diff: 169.137, ratio: 0.052
#   pulse 12: activated nodes: 11199, borderline nodes: 2488, overall activation: 3333.949, activation diff: 106.572, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11199
#   final overall activation: 3333.9
#   number of spread. activ. pulses: 12
#   running time: 638

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965795   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99490243   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99353147   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9890197   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.9884064   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9721462   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.74733984   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7472478   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   9   0.7472104   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.74717236   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.7471621   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   12   0.74710834   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7470861   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.7470248   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_320   15   0.746994   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_331   16   0.74698734   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   17   0.74696755   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.7469378   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.74693346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   20   0.74692655   REFERENCES:SIMDATES
