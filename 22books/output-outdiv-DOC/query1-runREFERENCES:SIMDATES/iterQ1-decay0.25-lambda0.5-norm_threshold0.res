###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 108.924, activation diff: 105.945, ratio: 0.973
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 177.022, activation diff: 68.838, ratio: 0.389
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1012.746, activation diff: 835.728, ratio: 0.825
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1804.865, activation diff: 792.119, ratio: 0.439
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2604.866, activation diff: 800.001, ratio: 0.307
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 3189.840, activation diff: 584.974, ratio: 0.183
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 3578.497, activation diff: 388.656, ratio: 0.109
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 3824.807, activation diff: 246.311, ratio: 0.064
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 3977.433, activation diff: 152.626, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 3977.4
#   number of spread. activ. pulses: 10
#   running time: 694

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99509525   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9938277   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9921546   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.98953706   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.98755145   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97297674   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74561507   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7454978   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.74531615   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.7451668   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.7451565   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.74512005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.74509305   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   14   0.74505705   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   15   0.7450414   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.74498403   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.7449769   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7449558   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7449321   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   20   0.74492776   REFERENCES:SIMDATES
