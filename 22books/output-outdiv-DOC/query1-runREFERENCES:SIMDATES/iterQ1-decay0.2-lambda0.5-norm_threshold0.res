###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 115.935, activation diff: 112.956, ratio: 0.974
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 189.817, activation diff: 74.622, ratio: 0.393
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1132.647, activation diff: 942.832, ratio: 0.832
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2028.651, activation diff: 896.004, ratio: 0.442
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2944.346, activation diff: 915.695, ratio: 0.311
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3612.811, activation diff: 668.465, ratio: 0.185
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 4057.946, activation diff: 445.135, ratio: 0.110
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 4341.618, activation diff: 283.672, ratio: 0.065
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 4518.802, activation diff: 177.184, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 4518.8
#   number of spread. activ. pulses: 10
#   running time: 575

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951011   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9939   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9923014   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9898704   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9878364   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9742054   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79533255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7952069   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.795027   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.79486185   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.7948531   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.7948201   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.794796   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   14   0.7947501   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   15   0.7947281   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.7946776   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.7946719   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   18   0.7946669   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.79461825   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   20   0.79461193   REFERENCES:SIMDATES
