###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 129.956, activation diff: 126.977, ratio: 0.977
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 215.882, activation diff: 86.666, ratio: 0.401
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1397.116, activation diff: 1181.237, ratio: 0.845
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2524.155, activation diff: 1127.038, ratio: 0.447
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3694.118, activation diff: 1169.964, ratio: 0.317
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4544.808, activation diff: 850.690, ratio: 0.187
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 5114.001, activation diff: 569.193, ratio: 0.111
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 5479.802, activation diff: 365.801, ratio: 0.067
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 5710.717, activation diff: 230.916, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 5710.7
#   number of spread. activ. pulses: 10
#   running time: 519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99510944   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9940165   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9925394   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99044347   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9883052   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9761827   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8947638   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89462006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8944423   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   10   0.89424837   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.89423907   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.8942143   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.8941951   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   14   0.8941284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   15   0.89409614   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.8940953   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.89406765   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   18   0.8940268   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.8939872   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.8939737   REFERENCES:SIMDATES
