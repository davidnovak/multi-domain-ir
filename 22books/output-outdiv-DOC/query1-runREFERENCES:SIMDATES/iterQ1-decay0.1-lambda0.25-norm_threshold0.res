###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 285.584, activation diff: 283.968, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 281.792, activation diff: 127.679, ratio: 0.453
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2904.382, activation diff: 2622.659, ratio: 0.903
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4043.685, activation diff: 1139.302, ratio: 0.282
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5181.481, activation diff: 1137.796, ratio: 0.220
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 5680.151, activation diff: 498.671, ratio: 0.088
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 5909.574, activation diff: 229.423, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 5909.6
#   number of spread. activ. pulses: 8
#   running time: 1551

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99931455   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99922526   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9988625   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9988502   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9958066   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98995477   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8993908   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8993542   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89934224   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   10   0.8993096   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   11   0.8993094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   12   0.8993073   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.8993063   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.89930475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   15   0.89929044   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   16   0.8992884   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.8992883   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   18   0.8992857   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_330   19   0.8992841   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.8992829   REFERENCES:SIMDATES
