###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4226, overall activation: 19.947, activation diff: 19.339, ratio: 0.969
#   pulse 3: activated nodes: 5573, borderline nodes: 4074, overall activation: 21.871, activation diff: 5.424, ratio: 0.248
#   pulse 4: activated nodes: 7856, borderline nodes: 6340, overall activation: 327.230, activation diff: 305.400, ratio: 0.933
#   pulse 5: activated nodes: 8730, borderline nodes: 4904, overall activation: 679.334, activation diff: 352.104, ratio: 0.518
#   pulse 6: activated nodes: 10936, borderline nodes: 6404, overall activation: 1878.675, activation diff: 1199.341, ratio: 0.638
#   pulse 7: activated nodes: 11233, borderline nodes: 3218, overall activation: 3226.141, activation diff: 1347.466, ratio: 0.418
#   pulse 8: activated nodes: 11374, borderline nodes: 1927, overall activation: 4558.197, activation diff: 1332.055, ratio: 0.292
#   pulse 9: activated nodes: 11414, borderline nodes: 1013, overall activation: 5627.988, activation diff: 1069.791, ratio: 0.190
#   pulse 10: activated nodes: 11429, borderline nodes: 682, overall activation: 6370.672, activation diff: 742.684, ratio: 0.117
#   pulse 11: activated nodes: 11439, borderline nodes: 510, overall activation: 6842.846, activation diff: 472.174, ratio: 0.069
#   pulse 12: activated nodes: 11439, borderline nodes: 434, overall activation: 7133.420, activation diff: 290.575, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 7133.4
#   number of spread. activ. pulses: 12
#   running time: 1956

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99705464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9955791   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99481416   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99121124   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9909322   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9812757   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89731395   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.8971474   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.8971203   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.897051   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.8970374   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   12   0.89700854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8969988   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.8969053   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.8968878   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   16   0.8968702   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   17   0.89686084   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   18   0.89685357   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.89681363   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   20   0.8968049   REFERENCES:SIMDATES:SIMLOC
