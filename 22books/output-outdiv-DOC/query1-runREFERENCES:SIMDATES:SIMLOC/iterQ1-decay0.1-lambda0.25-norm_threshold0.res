###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 291.568, activation diff: 289.612, ratio: 0.993
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 360.895, activation diff: 149.098, ratio: 0.413
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3482.817, activation diff: 3121.922, ratio: 0.896
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5659.263, activation diff: 2176.446, ratio: 0.385
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 7182.431, activation diff: 1523.168, ratio: 0.212
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7828.415, activation diff: 645.984, ratio: 0.083
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 8095.831, activation diff: 267.415, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8095.8
#   number of spread. activ. pulses: 8
#   running time: 2629

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993271   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992837   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991795   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9990712   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9966427   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9928704   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89943737   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89938366   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.89935696   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.89935046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89933765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.89933056   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.8993249   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.8993206   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.89931947   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.89931905   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.899319   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.8993187   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.8993113   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   20   0.8993094   REFERENCES:SIMDATES:SIMLOC
