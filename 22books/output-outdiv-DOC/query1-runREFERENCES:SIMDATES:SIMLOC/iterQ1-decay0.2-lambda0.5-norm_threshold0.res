###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 118.310, activation diff: 115.330, ratio: 0.975
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 212.190, activation diff: 94.559, ratio: 0.446
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1311.837, activation diff: 1099.647, ratio: 0.838
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2623.436, activation diff: 1311.599, ratio: 0.500
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 4061.415, activation diff: 1437.979, ratio: 0.354
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5138.792, activation diff: 1077.377, ratio: 0.210
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5838.063, activation diff: 699.271, ratio: 0.120
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6266.454, activation diff: 428.392, ratio: 0.068
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 6523.786, activation diff: 257.331, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6523.8
#   number of spread. activ. pulses: 10
#   running time: 2100

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99513185   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9941821   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9932815   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99060464   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.98881745   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98018074   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79543245   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79526985   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79506063   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.79491055   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.7948986   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.7948878   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7948547   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.7948388   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.79479146   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.7947837   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.79477334   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.79471767   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.79470503   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   20   0.7946633   REFERENCES:SIMDATES:SIMLOC
