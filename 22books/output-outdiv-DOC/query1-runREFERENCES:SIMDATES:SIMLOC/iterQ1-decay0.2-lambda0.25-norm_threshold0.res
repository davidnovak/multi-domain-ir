###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 259.550, activation diff: 257.593, ratio: 0.992
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 309.843, activation diff: 123.186, ratio: 0.398
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2856.648, activation diff: 2546.805, ratio: 0.892
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4610.403, activation diff: 1753.754, ratio: 0.380
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5928.114, activation diff: 1317.711, ratio: 0.222
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6502.722, activation diff: 574.608, ratio: 0.088
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 6737.032, activation diff: 234.310, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6737.0
#   number of spread. activ. pulses: 8
#   running time: 1665

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932706   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992786   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991658   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9989917   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9964528   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99256396   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7994973   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79945064   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.79942536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.79942214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7994097   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79940075   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.79939854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.79939455   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.79939425   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.79939365   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7993923   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.79938936   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.7993849   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   20   0.79938316   REFERENCES:SIMDATES:SIMLOC
