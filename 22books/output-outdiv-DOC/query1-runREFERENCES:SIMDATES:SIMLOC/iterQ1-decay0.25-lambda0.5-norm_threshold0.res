###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 111.157, activation diff: 108.177, ratio: 0.973
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 197.556, activation diff: 87.078, ratio: 0.441
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1179.632, activation diff: 982.076, ratio: 0.833
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2334.562, activation diff: 1154.930, ratio: 0.495
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 3618.363, activation diff: 1283.801, ratio: 0.355
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 4601.943, activation diff: 983.580, ratio: 0.214
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 5247.206, activation diff: 645.263, ratio: 0.123
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 5646.198, activation diff: 398.992, ratio: 0.071
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 5886.170, activation diff: 239.972, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5886.2
#   number of spread. activ. pulses: 10
#   running time: 2278

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951265   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9941252   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9931896   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9902946   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9885533   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9795769   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74571055   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74555904   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74535453   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.74521315   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.74520636   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.74519336   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7451469   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.74514115   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.74509716   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.74509394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.7450893   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.74503565   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.745016   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   20   0.7449823   REFERENCES:SIMDATES:SIMLOC
