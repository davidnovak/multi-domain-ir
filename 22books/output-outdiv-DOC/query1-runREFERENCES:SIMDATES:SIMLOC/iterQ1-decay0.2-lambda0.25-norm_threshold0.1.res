###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 2.176, activation diff: 5.176, ratio: 2.379
#   pulse 2: activated nodes: 4249, borderline nodes: 4226, overall activation: 61.499, activation diff: 61.341, ratio: 0.997
#   pulse 3: activated nodes: 5699, borderline nodes: 3925, overall activation: 23.837, activation diff: 53.644, ratio: 2.250
#   pulse 4: activated nodes: 9205, borderline nodes: 7371, overall activation: 1262.915, activation diff: 1246.464, ratio: 0.987
#   pulse 5: activated nodes: 10121, borderline nodes: 4516, overall activation: 1369.593, activation diff: 319.944, ratio: 0.234
#   pulse 6: activated nodes: 11223, borderline nodes: 4595, overall activation: 3244.929, activation diff: 1875.470, ratio: 0.578
#   pulse 7: activated nodes: 11359, borderline nodes: 1590, overall activation: 4344.339, activation diff: 1099.410, ratio: 0.253
#   pulse 8: activated nodes: 11409, borderline nodes: 1041, overall activation: 5019.199, activation diff: 674.860, ratio: 0.134
#   pulse 9: activated nodes: 11421, borderline nodes: 730, overall activation: 5347.765, activation diff: 328.567, ratio: 0.061
#   pulse 10: activated nodes: 11433, borderline nodes: 625, overall activation: 5498.890, activation diff: 151.124, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11433
#   final overall activation: 5498.9
#   number of spread. activ. pulses: 10
#   running time: 1849

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998908   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997325   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99964345   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9993181   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9970153   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931101   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7999238   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7999127   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.7999084   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.79990506   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.79990077   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.7998987   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.7998984   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.79989773   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7998888   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   16   0.7998886   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   17   0.7998824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.7998811   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_315   19   0.7998806   REFERENCES:SIMDATES:SIMLOC
1   Q1   frenchschoolsth02instgoog_135   20   0.799878   REFERENCES:SIMDATES:SIMLOC
