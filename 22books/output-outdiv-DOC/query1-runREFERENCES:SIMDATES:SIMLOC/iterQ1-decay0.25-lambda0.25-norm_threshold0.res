###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.740, activation diff: 6.740, ratio: 1.802
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 243.540, activation diff: 241.584, ratio: 0.992
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 285.406, activation diff: 111.201, ratio: 0.390
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2566.712, activation diff: 2281.306, ratio: 0.889
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4116.642, activation diff: 1549.930, ratio: 0.377
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 5327.930, activation diff: 1211.288, ratio: 0.227
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 5864.191, activation diff: 536.260, ratio: 0.091
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 6084.469, activation diff: 220.278, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6084.5
#   number of spread. activ. pulses: 8
#   running time: 1926

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932706   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992753   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991568   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9989353   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99632573   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99236405   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74952745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7494842   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.74945956   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.74945796   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74944574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7494358   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.7494354   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7494321   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.74943185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.74943095   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7494289   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.74942356   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.7494215   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   20   0.7494196   REFERENCES:SIMDATES:SIMLOC
