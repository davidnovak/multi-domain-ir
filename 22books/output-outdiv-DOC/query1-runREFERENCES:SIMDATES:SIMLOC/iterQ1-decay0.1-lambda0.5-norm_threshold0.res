###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 4.493, activation diff: 4.493, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 132.616, activation diff: 129.636, ratio: 0.978
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 242.122, activation diff: 110.186, ratio: 0.455
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1598.067, activation diff: 1355.945, ratio: 0.848
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3251.302, activation diff: 1653.235, ratio: 0.508
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5001.069, activation diff: 1749.767, ratio: 0.350
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6264.514, activation diff: 1263.445, ratio: 0.202
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7067.607, activation diff: 803.093, ratio: 0.114
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 7556.275, activation diff: 488.668, ratio: 0.065
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 7851.389, activation diff: 295.114, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7851.4
#   number of spread. activ. pulses: 10
#   running time: 1783

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951395   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99427414   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9934336   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9911376   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.989231   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9811729   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89487535   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8946897   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89446944   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.8943032   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.89427996   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.8942739   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.89426756   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.8942306   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89417523   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.8941584   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.89413875   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.8940808   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8940782   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.8940362   REFERENCES:SIMDATES:SIMLOC
