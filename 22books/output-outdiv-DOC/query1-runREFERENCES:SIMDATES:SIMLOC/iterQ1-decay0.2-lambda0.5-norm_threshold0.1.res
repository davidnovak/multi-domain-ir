###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumOutDivSAFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 3.450, activation diff: 3.450, ratio: 1.000
#   pulse 2: activated nodes: 4249, borderline nodes: 4226, overall activation: 17.956, activation diff: 17.348, ratio: 0.966
#   pulse 3: activated nodes: 5573, borderline nodes: 4074, overall activation: 19.534, activation diff: 4.859, ratio: 0.249
#   pulse 4: activated nodes: 7603, borderline nodes: 6092, overall activation: 257.053, activation diff: 237.567, ratio: 0.924
#   pulse 5: activated nodes: 8670, borderline nodes: 4894, overall activation: 530.796, activation diff: 273.743, ratio: 0.516
#   pulse 6: activated nodes: 10817, borderline nodes: 6547, overall activation: 1430.011, activation diff: 899.215, ratio: 0.629
#   pulse 7: activated nodes: 11169, borderline nodes: 3790, overall activation: 2393.131, activation diff: 963.119, ratio: 0.402
#   pulse 8: activated nodes: 11320, borderline nodes: 2426, overall activation: 3354.049, activation diff: 960.919, ratio: 0.286
#   pulse 9: activated nodes: 11396, borderline nodes: 1350, overall activation: 4137.969, activation diff: 783.920, ratio: 0.189
#   pulse 10: activated nodes: 11415, borderline nodes: 958, overall activation: 4715.980, activation diff: 578.011, ratio: 0.123
#   pulse 11: activated nodes: 11422, borderline nodes: 738, overall activation: 5104.198, activation diff: 388.218, ratio: 0.076
#   pulse 12: activated nodes: 11432, borderline nodes: 641, overall activation: 5352.344, activation diff: 248.146, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11432
#   final overall activation: 5352.3
#   number of spread. activ. pulses: 12
#   running time: 2212

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9967729   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9952202   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9943478   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9902992   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9894918   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9793863   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.79736817   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7972262   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   9   0.79719734   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   10   0.79713726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.79713297   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7970979   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   13   0.79708934   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   14   0.79699576   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_320   15   0.79697204   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_331   16   0.7969614   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.796955   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   18   0.7969265   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7969197   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.79690516   REFERENCES:SIMDATES:SIMLOC
