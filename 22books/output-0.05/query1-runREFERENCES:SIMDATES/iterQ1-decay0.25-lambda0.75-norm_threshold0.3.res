###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 91.878, activation diff: 81.260, ratio: 0.884
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 263.094, activation diff: 171.296, ratio: 0.651
#   pulse 4: activated nodes: 9819, borderline nodes: 5856, overall activation: 702.822, activation diff: 439.728, ratio: 0.626
#   pulse 5: activated nodes: 10777, borderline nodes: 4310, overall activation: 1263.498, activation diff: 560.676, ratio: 0.444
#   pulse 6: activated nodes: 11038, borderline nodes: 3237, overall activation: 1831.821, activation diff: 568.323, ratio: 0.310
#   pulse 7: activated nodes: 11139, borderline nodes: 2341, overall activation: 2351.306, activation diff: 519.485, ratio: 0.221
#   pulse 8: activated nodes: 11213, borderline nodes: 1454, overall activation: 2802.377, activation diff: 451.071, ratio: 0.161
#   pulse 9: activated nodes: 11233, borderline nodes: 1086, overall activation: 3182.819, activation diff: 380.442, ratio: 0.120
#   pulse 10: activated nodes: 11266, borderline nodes: 825, overall activation: 3497.856, activation diff: 315.037, ratio: 0.090
#   pulse 11: activated nodes: 11273, borderline nodes: 748, overall activation: 3755.574, activation diff: 257.718, ratio: 0.069
#   pulse 12: activated nodes: 11289, borderline nodes: 695, overall activation: 3964.595, activation diff: 209.022, ratio: 0.053
#   pulse 13: activated nodes: 11296, borderline nodes: 665, overall activation: 4133.055, activation diff: 168.459, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11296
#   final overall activation: 4133.1
#   number of spread. activ. pulses: 13
#   running time: 519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911115   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9870448   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9848458   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9834968   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97520673   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95496356   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7244991   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.72380054   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7230779   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7228956   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.7219885   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7219844   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.72195494   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.72156715   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   15   0.7215616   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7215579   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.7215319   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   18   0.721429   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7213424   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.72127867   REFERENCES:SIMDATES
