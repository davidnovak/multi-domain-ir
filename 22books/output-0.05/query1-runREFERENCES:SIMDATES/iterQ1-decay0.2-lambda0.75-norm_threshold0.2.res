###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 135.180, activation diff: 123.806, ratio: 0.916
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 375.315, activation diff: 240.188, ratio: 0.640
#   pulse 4: activated nodes: 10329, borderline nodes: 5402, overall activation: 949.224, activation diff: 573.909, ratio: 0.605
#   pulse 5: activated nodes: 11004, borderline nodes: 3459, overall activation: 1635.253, activation diff: 686.029, ratio: 0.420
#   pulse 6: activated nodes: 11199, borderline nodes: 1877, overall activation: 2308.547, activation diff: 673.295, ratio: 0.292
#   pulse 7: activated nodes: 11236, borderline nodes: 1089, overall activation: 2912.684, activation diff: 604.136, ratio: 0.207
#   pulse 8: activated nodes: 11279, borderline nodes: 750, overall activation: 3430.583, activation diff: 517.899, ratio: 0.151
#   pulse 9: activated nodes: 11309, borderline nodes: 579, overall activation: 3863.222, activation diff: 432.639, ratio: 0.112
#   pulse 10: activated nodes: 11317, borderline nodes: 476, overall activation: 4218.877, activation diff: 355.655, ratio: 0.084
#   pulse 11: activated nodes: 11320, borderline nodes: 395, overall activation: 4508.089, activation diff: 289.212, ratio: 0.064
#   pulse 12: activated nodes: 11321, borderline nodes: 352, overall activation: 4741.483, activation diff: 233.394, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 4741.5
#   number of spread. activ. pulses: 12
#   running time: 526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885731   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98414254   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9820307   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98002744   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97064906   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95011616   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7643644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7633964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7626791   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7625552   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7615448   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.76120335   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.76116794   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   14   0.7607928   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.7607904   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.7607474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.76072985   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.7606252   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.76044726   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.76042455   REFERENCES:SIMDATES
