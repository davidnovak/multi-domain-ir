###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2575.483, activation diff: 2605.425, ratio: 1.012
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 1537.443, activation diff: 4053.950, ratio: 2.637
#   pulse 4: activated nodes: 10878, borderline nodes: 2211, overall activation: 5558.622, activation diff: 6537.624, ratio: 1.176
#   pulse 5: activated nodes: 11316, borderline nodes: 501, overall activation: 6488.206, activation diff: 2678.769, ratio: 0.413
#   pulse 6: activated nodes: 11337, borderline nodes: 73, overall activation: 7352.678, activation diff: 1067.127, ratio: 0.145
#   pulse 7: activated nodes: 11345, borderline nodes: 21, overall activation: 7459.304, activation diff: 145.374, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11345
#   final overall activation: 7459.3
#   number of spread. activ. pulses: 7
#   running time: 1814

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999991   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996305   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMDATES
