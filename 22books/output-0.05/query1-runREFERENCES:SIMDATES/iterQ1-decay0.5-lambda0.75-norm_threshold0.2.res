###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 90.245, activation diff: 78.871, ratio: 0.874
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 222.444, activation diff: 132.252, ratio: 0.595
#   pulse 4: activated nodes: 8604, borderline nodes: 4435, overall activation: 448.735, activation diff: 226.291, ratio: 0.504
#   pulse 5: activated nodes: 8929, borderline nodes: 3613, overall activation: 696.088, activation diff: 247.353, ratio: 0.355
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 927.487, activation diff: 231.398, ratio: 0.249
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1129.718, activation diff: 202.231, ratio: 0.179
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1300.274, activation diff: 170.556, ratio: 0.131
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1441.085, activation diff: 140.811, ratio: 0.098
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1555.727, activation diff: 114.642, ratio: 0.074
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1648.153, activation diff: 92.426, ratio: 0.056
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1722.132, activation diff: 73.979, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1722.1
#   number of spread. activ. pulses: 12
#   running time: 546

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98857284   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9841013   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9819379   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9798653   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97030306   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9483613   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4777251   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.47708803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.47665015   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.47657228   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.47592422   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.47556147   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.4755379   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.47536343   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.47532976   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.47528678   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.47523537   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.4752307   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   19   0.47522694   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.4751965   REFERENCES:SIMDATES
