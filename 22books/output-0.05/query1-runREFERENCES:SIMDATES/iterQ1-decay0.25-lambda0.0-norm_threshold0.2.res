###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1948.358, activation diff: 1976.620, ratio: 1.015
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 823.749, activation diff: 2767.850, ratio: 3.360
#   pulse 4: activated nodes: 10837, borderline nodes: 2544, overall activation: 3785.586, activation diff: 4509.943, ratio: 1.191
#   pulse 5: activated nodes: 11269, borderline nodes: 886, overall activation: 1888.695, activation diff: 4029.542, ratio: 2.134
#   pulse 6: activated nodes: 11314, borderline nodes: 533, overall activation: 4417.630, activation diff: 3463.935, ratio: 0.784
#   pulse 7: activated nodes: 11318, borderline nodes: 477, overall activation: 4826.827, activation diff: 553.368, ratio: 0.115
#   pulse 8: activated nodes: 11318, borderline nodes: 476, overall activation: 4895.478, activation diff: 90.848, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4895.5
#   number of spread. activ. pulses: 8
#   running time: 617

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999989   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999959   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   7   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   10   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   14   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   18   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_549   20   0.75   REFERENCES:SIMDATES
