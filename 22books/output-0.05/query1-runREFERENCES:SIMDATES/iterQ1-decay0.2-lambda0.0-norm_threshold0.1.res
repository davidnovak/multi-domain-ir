###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2290.065, activation diff: 2320.007, ratio: 1.013
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 1124.687, activation diff: 3364.175, ratio: 2.991
#   pulse 4: activated nodes: 10878, borderline nodes: 2211, overall activation: 4454.524, activation diff: 5195.718, ratio: 1.166
#   pulse 5: activated nodes: 11311, borderline nodes: 512, overall activation: 5004.075, activation diff: 1995.496, ratio: 0.399
#   pulse 6: activated nodes: 11330, borderline nodes: 152, overall activation: 5701.385, activation diff: 849.050, ratio: 0.149
#   pulse 7: activated nodes: 11333, borderline nodes: 97, overall activation: 5780.028, activation diff: 103.061, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 5780.0
#   number of spread. activ. pulses: 7
#   running time: 533

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999905   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999963   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_169   15   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_71   16   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_75   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_807   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_805   20   0.8   REFERENCES:SIMDATES
