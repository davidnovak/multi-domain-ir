###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1433.809, activation diff: 1463.751, ratio: 1.021
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 108.293, activation diff: 1513.221, ratio: 13.973
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1993.342, activation diff: 1993.891, ratio: 1.000
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1693.482, activation diff: 409.126, ratio: 0.242
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2047.892, activation diff: 354.775, ratio: 0.173
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2047.944, activation diff: 0.336, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2047.9
#   number of spread. activ. pulses: 7
#   running time: 435

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999989   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996156   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9979815   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99453336   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999997   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.49999997   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.49999988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.49999988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   18   0.49999988   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.49999967   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.49999943   REFERENCES:SIMDATES
