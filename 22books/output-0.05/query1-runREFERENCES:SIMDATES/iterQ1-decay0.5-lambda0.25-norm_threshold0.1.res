###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 903.900, activation diff: 902.271, ratio: 0.998
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 859.036, activation diff: 202.158, ratio: 0.235
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1624.054, activation diff: 765.018, ratio: 0.471
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1913.441, activation diff: 289.387, ratio: 0.151
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2007.422, activation diff: 93.981, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2007.4
#   number of spread. activ. pulses: 6
#   running time: 413

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99921596   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99872077   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99846065   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99790347   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9950746   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9885555   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49945068   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49934366   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4993041   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49918246   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.49911806   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.4991114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.49910748   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.49908873   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.49905717   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.49905622   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.49905074   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.4990446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.49899036   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.49898553   REFERENCES:SIMDATES
