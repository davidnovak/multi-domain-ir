###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 203.830, activation diff: 191.765, ratio: 0.941
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 560.191, activation diff: 356.391, ratio: 0.636
#   pulse 4: activated nodes: 10772, borderline nodes: 3967, overall activation: 1374.977, activation diff: 814.787, ratio: 0.593
#   pulse 5: activated nodes: 11202, borderline nodes: 1894, overall activation: 2311.330, activation diff: 936.352, ratio: 0.405
#   pulse 6: activated nodes: 11246, borderline nodes: 741, overall activation: 3205.684, activation diff: 894.355, ratio: 0.279
#   pulse 7: activated nodes: 11314, borderline nodes: 404, overall activation: 3994.584, activation diff: 788.900, ratio: 0.197
#   pulse 8: activated nodes: 11322, borderline nodes: 221, overall activation: 4663.503, activation diff: 668.919, ratio: 0.143
#   pulse 9: activated nodes: 11327, borderline nodes: 142, overall activation: 5218.192, activation diff: 554.690, ratio: 0.106
#   pulse 10: activated nodes: 11336, borderline nodes: 88, overall activation: 5671.779, activation diff: 453.587, ratio: 0.080
#   pulse 11: activated nodes: 11338, borderline nodes: 48, overall activation: 6039.216, activation diff: 367.436, ratio: 0.061
#   pulse 12: activated nodes: 11338, borderline nodes: 44, overall activation: 6334.871, activation diff: 295.656, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11338
#   final overall activation: 6334.9
#   number of spread. activ. pulses: 12
#   running time: 531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98884827   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9852272   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.983673   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98164004   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97290695   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95582247   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8604256   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8594061   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8588519   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8588103   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.8579222   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.85730505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.8572823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.8571392   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.8570366   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.8569201   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.85689855   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.85688114   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.8567093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.8566246   REFERENCES:SIMDATES
