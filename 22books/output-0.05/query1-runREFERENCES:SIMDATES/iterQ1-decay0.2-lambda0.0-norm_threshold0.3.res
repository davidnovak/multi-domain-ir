###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1845.692, activation diff: 1872.094, ratio: 1.014
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 894.123, activation diff: 2739.433, ratio: 3.064
#   pulse 4: activated nodes: 10827, borderline nodes: 2956, overall activation: 4175.251, activation diff: 4986.545, ratio: 1.194
#   pulse 5: activated nodes: 11251, borderline nodes: 1314, overall activation: 1663.022, activation diff: 5062.468, ratio: 3.044
#   pulse 6: activated nodes: 11315, borderline nodes: 382, overall activation: 4748.814, activation diff: 4604.494, ratio: 0.970
#   pulse 7: activated nodes: 11323, borderline nodes: 308, overall activation: 5341.572, activation diff: 961.906, ratio: 0.180
#   pulse 8: activated nodes: 11323, borderline nodes: 274, overall activation: 5517.968, activation diff: 220.980, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 5518.0
#   number of spread. activ. pulses: 8
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999988   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995476   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_169   15   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_71   16   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_75   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_807   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_805   20   0.8   REFERENCES:SIMDATES
