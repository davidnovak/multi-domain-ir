###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2483.751, activation diff: 2515.187, ratio: 1.013
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1294.289, activation diff: 3596.481, ratio: 2.779
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4723.952, activation diff: 5137.761, ratio: 1.088
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5596.103, activation diff: 1435.391, ratio: 0.256
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5896.675, activation diff: 352.622, ratio: 0.060
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5928.646, activation diff: 40.394, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5928.6
#   number of spread. activ. pulses: 7
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999665   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981779   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950548   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_71   15   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_75   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_807   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_805   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_816   20   0.8   REFERENCES:SIMDATES
