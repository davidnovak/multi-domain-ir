###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 172.612, activation diff: 160.547, ratio: 0.930
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 449.976, activation diff: 277.393, ratio: 0.616
#   pulse 4: activated nodes: 10741, borderline nodes: 4075, overall activation: 1008.175, activation diff: 558.200, ratio: 0.554
#   pulse 5: activated nodes: 11181, borderline nodes: 2141, overall activation: 1633.534, activation diff: 625.359, ratio: 0.383
#   pulse 6: activated nodes: 11230, borderline nodes: 1087, overall activation: 2225.377, activation diff: 591.842, ratio: 0.266
#   pulse 7: activated nodes: 11276, borderline nodes: 697, overall activation: 2744.928, activation diff: 519.551, ratio: 0.189
#   pulse 8: activated nodes: 11306, borderline nodes: 586, overall activation: 3183.919, activation diff: 438.991, ratio: 0.138
#   pulse 9: activated nodes: 11312, borderline nodes: 538, overall activation: 3546.780, activation diff: 362.861, ratio: 0.102
#   pulse 10: activated nodes: 11313, borderline nodes: 524, overall activation: 3842.582, activation diff: 295.802, ratio: 0.077
#   pulse 11: activated nodes: 11315, borderline nodes: 518, overall activation: 4081.484, activation diff: 238.902, ratio: 0.059
#   pulse 12: activated nodes: 11315, borderline nodes: 488, overall activation: 4273.173, activation diff: 191.689, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 4273.2
#   number of spread. activ. pulses: 12
#   running time: 526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888482   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98522097   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9836641   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98160875   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9728309   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9554447   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7170212   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.71616983   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.71570265   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7156679   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7149204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7143949   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7143637   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.714256   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.71417654   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   16   0.7140802   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.7140658   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.71404326   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7138767   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7138226   REFERENCES:SIMDATES
