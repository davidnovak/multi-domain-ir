###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1094.547, activation diff: 1098.918, ratio: 1.004
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1255.979, activation diff: 857.145, ratio: 0.682
#   pulse 4: activated nodes: 10508, borderline nodes: 4190, overall activation: 3579.078, activation diff: 2395.079, ratio: 0.669
#   pulse 5: activated nodes: 11216, borderline nodes: 1772, overall activation: 4755.108, activation diff: 1176.379, ratio: 0.247
#   pulse 6: activated nodes: 11289, borderline nodes: 663, overall activation: 5240.790, activation diff: 485.682, ratio: 0.093
#   pulse 7: activated nodes: 11319, borderline nodes: 422, overall activation: 5421.966, activation diff: 181.176, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 5422.0
#   number of spread. activ. pulses: 7
#   running time: 457

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993398   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9990612   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99895155   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99635684   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9909173   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7996446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.79956377   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79947484   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.7993747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.7993747   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.79934776   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.79931515   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.79930884   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   15   0.7993062   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.7992893   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.79928505   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.7992822   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   19   0.7992697   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.79925704   REFERENCES:SIMDATES
