###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 448.698, activation diff: 430.559, ratio: 0.960
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 744.040, activation diff: 295.342, ratio: 0.397
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1220.404, activation diff: 476.364, ratio: 0.390
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1558.713, activation diff: 338.310, ratio: 0.217
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1768.157, activation diff: 209.443, ratio: 0.118
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1891.067, activation diff: 122.910, ratio: 0.065
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1961.179, activation diff: 70.113, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1961.2
#   number of spread. activ. pulses: 8
#   running time: 431

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99567604   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99530476   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99479884   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9910613   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9820826   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4960551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49598864   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49594134   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4959182   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4958171   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.49575245   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.49574643   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.49570352   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.4956989   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   16   0.49569374   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.49568608   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.4956832   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4956714   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.4956708   REFERENCES:SIMDATES
