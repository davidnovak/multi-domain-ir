###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 794.678, activation diff: 796.891, ratio: 1.003
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 679.424, activation diff: 250.979, ratio: 0.369
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1523.616, activation diff: 844.199, ratio: 0.554
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1845.713, activation diff: 322.097, ratio: 0.175
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1950.942, activation diff: 105.229, ratio: 0.054
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1983.244, activation diff: 32.302, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1983.2
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995412   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9993981   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992265   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.996822   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9919585   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4998346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49979466   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49976733   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   10   0.49970213   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   11   0.4996987   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.4996969   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.49968302   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.4996791   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.49967474   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.4996715   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.49965948   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.49965376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.4996534   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.49964783   REFERENCES:SIMDATES
