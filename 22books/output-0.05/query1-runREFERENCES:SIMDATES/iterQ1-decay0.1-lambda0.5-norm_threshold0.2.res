###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 657.240, activation diff: 641.523, ratio: 0.976
#   pulse 3: activated nodes: 8336, borderline nodes: 4231, overall activation: 1384.550, activation diff: 727.310, ratio: 0.525
#   pulse 4: activated nodes: 10508, borderline nodes: 4315, overall activation: 3276.080, activation diff: 1891.530, ratio: 0.577
#   pulse 5: activated nodes: 11215, borderline nodes: 1610, overall activation: 4786.841, activation diff: 1510.761, ratio: 0.316
#   pulse 6: activated nodes: 11300, borderline nodes: 478, overall activation: 5793.067, activation diff: 1006.226, ratio: 0.174
#   pulse 7: activated nodes: 11322, borderline nodes: 228, overall activation: 6416.421, activation diff: 623.354, ratio: 0.097
#   pulse 8: activated nodes: 11327, borderline nodes: 139, overall activation: 6789.789, activation diff: 373.368, ratio: 0.055
#   pulse 9: activated nodes: 11331, borderline nodes: 109, overall activation: 7009.692, activation diff: 219.903, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 7009.7
#   number of spread. activ. pulses: 9
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9977251   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9974083   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99714124   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9939594   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9867059   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8964212   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89634466   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8962671   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8962192   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.89607346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.896073   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.89607286   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.89599395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.8959906   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.89597356   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.89596826   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   18   0.89595366   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.89594996   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.8959476   REFERENCES:SIMDATES
