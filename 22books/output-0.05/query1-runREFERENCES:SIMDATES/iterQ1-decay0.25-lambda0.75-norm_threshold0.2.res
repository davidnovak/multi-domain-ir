###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 127.691, activation diff: 116.317, ratio: 0.911
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 348.298, activation diff: 220.661, ratio: 0.634
#   pulse 4: activated nodes: 10312, borderline nodes: 5413, overall activation: 852.203, activation diff: 503.905, ratio: 0.591
#   pulse 5: activated nodes: 10969, borderline nodes: 3557, overall activation: 1447.897, activation diff: 595.694, ratio: 0.411
#   pulse 6: activated nodes: 11180, borderline nodes: 2128, overall activation: 2029.467, activation diff: 581.570, ratio: 0.287
#   pulse 7: activated nodes: 11226, borderline nodes: 1284, overall activation: 2549.817, activation diff: 520.349, ratio: 0.204
#   pulse 8: activated nodes: 11264, borderline nodes: 853, overall activation: 2995.124, activation diff: 445.308, ratio: 0.149
#   pulse 9: activated nodes: 11280, borderline nodes: 729, overall activation: 3366.554, activation diff: 371.430, ratio: 0.110
#   pulse 10: activated nodes: 11303, borderline nodes: 667, overall activation: 3671.452, activation diff: 304.898, ratio: 0.083
#   pulse 11: activated nodes: 11310, borderline nodes: 630, overall activation: 3919.092, activation diff: 247.640, ratio: 0.063
#   pulse 12: activated nodes: 11311, borderline nodes: 597, overall activation: 4118.710, activation diff: 199.618, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11311
#   final overall activation: 4118.7
#   number of spread. activ. pulses: 12
#   running time: 530

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885731   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9841382   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9820214   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9800097   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9706109   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9499134   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7165916   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7156826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7150077   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7148914   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7139405   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7136117   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.71357405   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   14   0.7132294   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.7132238   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.71317923   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.7131681   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.7130615   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.71290857   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.7128961   REFERENCES:SIMDATES
