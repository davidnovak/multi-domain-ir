###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 770.872, activation diff: 751.007, ratio: 0.974
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1530.663, activation diff: 759.791, ratio: 0.496
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2813.823, activation diff: 1283.160, ratio: 0.456
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3744.371, activation diff: 930.548, ratio: 0.249
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4329.091, activation diff: 584.720, ratio: 0.135
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 4677.599, activation diff: 348.508, ratio: 0.075
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 4880.123, activation diff: 202.524, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4880.1
#   number of spread. activ. pulses: 8
#   running time: 457

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9958174   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9955956   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9951482   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9917921   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98423016   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74410886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74403304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7439951   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.74398047   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.74388576   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.7437867   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.7437851   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7437843   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.7437631   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.7437339   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.74373037   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.7437135   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.74370253   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.743692   REFERENCES:SIMDATES
