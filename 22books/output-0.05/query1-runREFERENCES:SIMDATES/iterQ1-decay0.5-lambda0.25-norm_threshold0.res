###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1005.370, activation diff: 999.926, ratio: 0.995
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1047.606, activation diff: 161.520, ratio: 0.154
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1722.919, activation diff: 675.312, ratio: 0.392
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1978.073, activation diff: 255.154, ratio: 0.129
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2060.863, activation diff: 82.790, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2060.9
#   number of spread. activ. pulses: 6
#   running time: 465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99925435   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99899435   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9988788   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9984314   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99582326   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99031436   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4994921   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49942642   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49941614   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49936277   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.49934733   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.4993136   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.49927843   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.4992747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.49926275   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.49924874   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   17   0.4992389   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.49923006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.49922293   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.49920908   REFERENCES:SIMDATES
