###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 545.039, activation diff: 531.892, ratio: 0.976
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1136.831, activation diff: 595.236, ratio: 0.524
#   pulse 4: activated nodes: 10447, borderline nodes: 4845, overall activation: 2974.844, activation diff: 1838.013, ratio: 0.618
#   pulse 5: activated nodes: 11192, borderline nodes: 2248, overall activation: 4495.324, activation diff: 1520.480, ratio: 0.338
#   pulse 6: activated nodes: 11271, borderline nodes: 733, overall activation: 5536.376, activation diff: 1041.052, ratio: 0.188
#   pulse 7: activated nodes: 11316, borderline nodes: 418, overall activation: 6192.059, activation diff: 655.683, ratio: 0.106
#   pulse 8: activated nodes: 11321, borderline nodes: 274, overall activation: 6589.514, activation diff: 397.454, ratio: 0.060
#   pulse 9: activated nodes: 11326, borderline nodes: 190, overall activation: 6825.754, activation diff: 236.241, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 6825.8
#   number of spread. activ. pulses: 9
#   running time: 491

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9975415   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9969965   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9967923   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9933336   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98489845   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8963661   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89627194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89613116   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8960278   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.89590114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.89590096   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8958108   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.8957894   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.8957792   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.89575857   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.8957355   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.8957149   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.89568794   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   20   0.895684   REFERENCES:SIMDATES
