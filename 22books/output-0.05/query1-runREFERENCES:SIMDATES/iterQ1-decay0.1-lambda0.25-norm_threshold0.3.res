###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1229.897, activation diff: 1234.267, ratio: 1.004
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1539.033, activation diff: 1091.741, ratio: 0.709
#   pulse 4: activated nodes: 10509, borderline nodes: 4118, overall activation: 4502.596, activation diff: 3063.428, ratio: 0.680
#   pulse 5: activated nodes: 11225, borderline nodes: 1661, overall activation: 6084.213, activation diff: 1582.777, ratio: 0.260
#   pulse 6: activated nodes: 11299, borderline nodes: 380, overall activation: 6754.632, activation diff: 670.419, ratio: 0.099
#   pulse 7: activated nodes: 11323, borderline nodes: 199, overall activation: 7009.182, activation diff: 254.550, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 7009.2
#   number of spread. activ. pulses: 7
#   running time: 503

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993398   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9990612   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99895203   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9963579   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9909326   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8996   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89950913   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89940906   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.8992964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.8992964   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.8992664   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.89922947   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.89922255   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   15   0.8992211   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.89921093   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.8992003   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   18   0.89919555   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   19   0.899183   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.8991641   REFERENCES:SIMDATES
