###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 150.158, activation diff: 138.785, ratio: 0.924
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 431.024, activation diff: 280.919, ratio: 0.652
#   pulse 4: activated nodes: 10335, borderline nodes: 5351, overall activation: 1159.897, activation diff: 728.873, ratio: 0.628
#   pulse 5: activated nodes: 11062, borderline nodes: 3158, overall activation: 2046.778, activation diff: 886.881, ratio: 0.433
#   pulse 6: activated nodes: 11218, borderline nodes: 1589, overall activation: 2922.624, activation diff: 875.846, ratio: 0.300
#   pulse 7: activated nodes: 11244, borderline nodes: 769, overall activation: 3710.666, activation diff: 788.042, ratio: 0.212
#   pulse 8: activated nodes: 11310, borderline nodes: 512, overall activation: 4387.595, activation diff: 676.929, ratio: 0.154
#   pulse 9: activated nodes: 11319, borderline nodes: 363, overall activation: 4954.064, activation diff: 566.469, ratio: 0.114
#   pulse 10: activated nodes: 11321, borderline nodes: 268, overall activation: 5420.560, activation diff: 466.496, ratio: 0.086
#   pulse 11: activated nodes: 11324, borderline nodes: 189, overall activation: 5800.716, activation diff: 380.156, ratio: 0.066
#   pulse 12: activated nodes: 11327, borderline nodes: 152, overall activation: 6108.173, activation diff: 307.456, ratio: 0.050
#   pulse 13: activated nodes: 11329, borderline nodes: 129, overall activation: 6355.447, activation diff: 247.275, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 6355.4
#   number of spread. activ. pulses: 13
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99142987   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9881118   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.98653364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9850259   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.977302   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.960852   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.86993253   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8691169   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8685163   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.86841166   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.8675653   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.86728346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.8672614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.86693954   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.86693394   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.86690474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.866884   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.86680955   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.8666427   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.8666316   REFERENCES:SIMDATES
