###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 820.754, activation diff: 800.889, ratio: 0.976
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1675.052, activation diff: 854.298, ratio: 0.510
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3170.751, activation diff: 1495.699, ratio: 0.472
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4265.987, activation diff: 1095.236, ratio: 0.257
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4958.159, activation diff: 692.171, ratio: 0.140
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5372.473, activation diff: 414.314, ratio: 0.077
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 5614.111, activation diff: 241.638, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5614.1
#   number of spread. activ. pulses: 8
#   running time: 469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9958174   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9955956   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9951489   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99179405   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9842345   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7937163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.79363525   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79359484   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7935792   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.79347825   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.7933743   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.79337084   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.79337037   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.7933476   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.79331625   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.79331315   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.79329455   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.79328406   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.79327226   REFERENCES:SIMDATES
