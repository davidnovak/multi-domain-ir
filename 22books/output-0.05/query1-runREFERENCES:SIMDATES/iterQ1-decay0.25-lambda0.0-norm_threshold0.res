###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2328.960, activation diff: 2360.396, ratio: 1.013
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1076.929, activation diff: 3239.911, ratio: 3.008
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4175.605, activation diff: 4518.516, ratio: 1.082
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4867.523, activation diff: 1172.320, ratio: 0.241
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 5117.284, activation diff: 291.101, ratio: 0.057
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5142.166, activation diff: 31.408, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5142.2
#   number of spread. activ. pulses: 7
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996644   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981779   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950547   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   7   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   10   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   14   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_542   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   18   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_549   20   0.75   REFERENCES:SIMDATES
