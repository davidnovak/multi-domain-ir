###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1185.123, activation diff: 1187.336, ratio: 1.002
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 1396.529, activation diff: 760.359, ratio: 0.544
#   pulse 4: activated nodes: 10827, borderline nodes: 2945, overall activation: 3374.029, activation diff: 2011.647, ratio: 0.596
#   pulse 5: activated nodes: 11235, borderline nodes: 1340, overall activation: 4325.385, activation diff: 951.399, ratio: 0.220
#   pulse 6: activated nodes: 11293, borderline nodes: 606, overall activation: 4695.570, activation diff: 370.184, ratio: 0.079
#   pulse 7: activated nodes: 11315, borderline nodes: 514, overall activation: 4829.753, activation diff: 134.184, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 4829.8
#   number of spread. activ. pulses: 7
#   running time: 506

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995414   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9993981   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99922925   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99683267   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99200046   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7497525   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74969214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7496511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   10   0.7495614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   11   0.7495613   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.74954545   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.74953747   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7495252   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.74952066   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7495124   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.74949354   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   18   0.74949205   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.74948525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.7494826   REFERENCES:SIMDATES
