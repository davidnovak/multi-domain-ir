###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1596.959, activation diff: 1591.515, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2317.759, activation diff: 907.379, ratio: 0.391
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4432.234, activation diff: 2114.475, ratio: 0.477
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5391.480, activation diff: 959.245, ratio: 0.178
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5746.272, activation diff: 354.792, ratio: 0.062
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5870.043, activation diff: 123.771, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5870.0
#   number of spread. activ. pulses: 7
#   running time: 453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99974805   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9997192   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995826   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756926   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9938132   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7997979   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7997707   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79976726   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.7997465   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7997452   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7997256   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.7997138   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7997136   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7997094   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.7997031   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.79969573   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.7996929   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.7996912   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.79968745   REFERENCES:SIMDATES
