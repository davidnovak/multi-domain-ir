###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 550.783, activation diff: 535.067, ratio: 0.971
#   pulse 3: activated nodes: 8336, borderline nodes: 4231, overall activation: 1076.765, activation diff: 525.982, ratio: 0.488
#   pulse 4: activated nodes: 10499, borderline nodes: 4469, overall activation: 2336.489, activation diff: 1259.724, ratio: 0.539
#   pulse 5: activated nodes: 11209, borderline nodes: 1879, overall activation: 3312.837, activation diff: 976.348, ratio: 0.295
#   pulse 6: activated nodes: 11243, borderline nodes: 797, overall activation: 3951.787, activation diff: 638.950, ratio: 0.162
#   pulse 7: activated nodes: 11277, borderline nodes: 619, overall activation: 4342.376, activation diff: 390.590, ratio: 0.090
#   pulse 8: activated nodes: 11286, borderline nodes: 557, overall activation: 4573.622, activation diff: 231.245, ratio: 0.051
#   pulse 9: activated nodes: 11287, borderline nodes: 529, overall activation: 4708.111, activation diff: 134.489, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11287
#   final overall activation: 4708.1
#   number of spread. activ. pulses: 9
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9977249   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9974083   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9971389   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9939533   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9866774   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7470176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74695385   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.74688923   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7468493   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7467277   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.74672735   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.74672675   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.74666005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.74665874   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7466411   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.74663997   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   18   0.746628   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.7466237   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.7466229   REFERENCES:SIMDATES
