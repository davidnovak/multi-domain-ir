###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1730.714, activation diff: 1757.117, ratio: 1.015
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 737.623, activation diff: 2467.959, ratio: 3.346
#   pulse 4: activated nodes: 10827, borderline nodes: 2966, overall activation: 3698.535, activation diff: 4378.105, ratio: 1.184
#   pulse 5: activated nodes: 11239, borderline nodes: 1349, overall activation: 1303.849, activation diff: 4462.946, ratio: 3.423
#   pulse 6: activated nodes: 11294, borderline nodes: 602, overall activation: 4108.187, activation diff: 4135.504, ratio: 1.007
#   pulse 7: activated nodes: 11312, borderline nodes: 514, overall activation: 4605.008, activation diff: 859.497, ratio: 0.187
#   pulse 8: activated nodes: 11313, borderline nodes: 513, overall activation: 4775.649, activation diff: 212.953, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11313
#   final overall activation: 4775.6
#   number of spread. activ. pulses: 8
#   running time: 602

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999988   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995464   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975411   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   7   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   10   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   14   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   18   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_549   20   0.75   REFERENCES:SIMDATES
