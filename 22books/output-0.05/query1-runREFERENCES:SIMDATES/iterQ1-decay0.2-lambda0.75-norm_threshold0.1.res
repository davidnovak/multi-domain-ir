###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 183.018, activation diff: 170.953, ratio: 0.934
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 485.976, activation diff: 302.988, ratio: 0.623
#   pulse 4: activated nodes: 10756, borderline nodes: 4039, overall activation: 1124.112, activation diff: 638.135, ratio: 0.568
#   pulse 5: activated nodes: 11194, borderline nodes: 2052, overall activation: 1846.589, activation diff: 722.478, ratio: 0.391
#   pulse 6: activated nodes: 11237, borderline nodes: 938, overall activation: 2533.244, activation diff: 686.655, ratio: 0.271
#   pulse 7: activated nodes: 11296, borderline nodes: 592, overall activation: 3137.386, activation diff: 604.141, ratio: 0.193
#   pulse 8: activated nodes: 11318, borderline nodes: 405, overall activation: 3648.746, activation diff: 511.360, ratio: 0.140
#   pulse 9: activated nodes: 11321, borderline nodes: 301, overall activation: 4072.033, activation diff: 423.287, ratio: 0.104
#   pulse 10: activated nodes: 11323, borderline nodes: 251, overall activation: 4417.588, activation diff: 345.555, ratio: 0.078
#   pulse 11: activated nodes: 11325, borderline nodes: 164, overall activation: 4697.054, activation diff: 279.466, ratio: 0.059
#   pulse 12: activated nodes: 11330, borderline nodes: 141, overall activation: 4921.568, activation diff: 224.515, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 4921.6
#   number of spread. activ. pulses: 12
#   running time: 522

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888482   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98522335   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9836676   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98162043   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9728592   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95558536   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7648226   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7639155   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.76341915   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.76338214   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.76258767   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7620339   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7620052   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.7618853   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.76179856   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   16   0.7616866   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.7616865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.7616579   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7614894   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.76142025   REFERENCES:SIMDATES
