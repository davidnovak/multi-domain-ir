###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 157.911, activation diff: 145.207, ratio: 0.920
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 349.398, activation diff: 191.498, ratio: 0.548
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 608.856, activation diff: 259.457, ratio: 0.426
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 866.829, activation diff: 257.973, ratio: 0.298
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1097.415, activation diff: 230.585, ratio: 0.210
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1293.693, activation diff: 196.279, ratio: 0.152
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1456.351, activation diff: 162.658, ratio: 0.112
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1588.935, activation diff: 132.583, ratio: 0.083
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1695.811, activation diff: 106.876, ratio: 0.063
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1781.286, activation diff: 85.475, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1781.3
#   number of spread. activ. pulses: 11
#   running time: 472

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9853725   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9813996   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.979856   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9771491   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9669954   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94792527   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.47096664   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.47024742   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.46998653   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.469977   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4694523   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.4688868   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   13   0.46885154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.46876276   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.46873802   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.46869346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.46855876   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.46850276   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.46847475   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.46833885   REFERENCES:SIMDATES
