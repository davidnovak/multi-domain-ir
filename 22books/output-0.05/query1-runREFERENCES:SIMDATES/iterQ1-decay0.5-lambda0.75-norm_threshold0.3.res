###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 65.964, activation diff: 55.346, ratio: 0.839
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 171.316, activation diff: 105.432, ratio: 0.615
#   pulse 4: activated nodes: 8336, borderline nodes: 4777, overall activation: 371.865, activation diff: 200.549, ratio: 0.539
#   pulse 5: activated nodes: 8872, borderline nodes: 3788, overall activation: 607.754, activation diff: 235.888, ratio: 0.388
#   pulse 6: activated nodes: 8929, borderline nodes: 3615, overall activation: 836.541, activation diff: 228.787, ratio: 0.273
#   pulse 7: activated nodes: 8929, borderline nodes: 3613, overall activation: 1040.427, activation diff: 203.886, ratio: 0.196
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1214.513, activation diff: 174.086, ratio: 0.143
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1359.485, activation diff: 144.972, ratio: 0.107
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1478.282, activation diff: 118.798, ratio: 0.080
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1574.553, activation diff: 96.270, ratio: 0.061
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1651.934, activation diff: 77.381, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1651.9
#   number of spread. activ. pulses: 12
#   running time: 523

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98814803   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9826554   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9795854   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97778845   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9676081   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9407225   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4773261   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.47666502   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4760368   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4758773   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4750465   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.47487956   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.47485286   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.47451526   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.4744997   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.4744994   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   17   0.47447598   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.47443676   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.4744348   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.47423905   REFERENCES:SIMDATES
