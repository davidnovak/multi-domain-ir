###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1263.212, activation diff: 1265.425, ratio: 1.002
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 1558.633, activation diff: 880.893, ratio: 0.565
#   pulse 4: activated nodes: 10827, borderline nodes: 2936, overall activation: 3836.156, activation diff: 2319.809, ratio: 0.605
#   pulse 5: activated nodes: 11243, borderline nodes: 1310, overall activation: 4964.402, activation diff: 1128.345, ratio: 0.227
#   pulse 6: activated nodes: 11306, borderline nodes: 422, overall activation: 5411.048, activation diff: 446.646, ratio: 0.083
#   pulse 7: activated nodes: 11321, borderline nodes: 244, overall activation: 5574.705, activation diff: 163.657, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5574.7
#   number of spread. activ. pulses: 7
#   running time: 509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99954146   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9993981   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99922943   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9968331   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99200284   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79973614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.79967165   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79962784   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   10   0.79953223   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   11   0.7995322   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7995151   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.79950684   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.79949373   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7994887   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7994807   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.7994598   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   18   0.79945886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.79945093   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.7994502   REFERENCES:SIMDATES
