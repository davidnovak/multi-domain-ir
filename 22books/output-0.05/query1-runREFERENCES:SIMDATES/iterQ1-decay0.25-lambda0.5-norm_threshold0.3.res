###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 456.920, activation diff: 443.773, ratio: 0.971
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 885.338, activation diff: 431.288, ratio: 0.487
#   pulse 4: activated nodes: 10419, borderline nodes: 4959, overall activation: 2123.115, activation diff: 1237.777, ratio: 0.583
#   pulse 5: activated nodes: 11094, borderline nodes: 2914, overall activation: 3113.043, activation diff: 989.928, ratio: 0.318
#   pulse 6: activated nodes: 11230, borderline nodes: 1199, overall activation: 3778.596, activation diff: 665.553, ratio: 0.176
#   pulse 7: activated nodes: 11268, borderline nodes: 796, overall activation: 4192.326, activation diff: 413.730, ratio: 0.099
#   pulse 8: activated nodes: 11297, borderline nodes: 675, overall activation: 4440.340, activation diff: 248.014, ratio: 0.056
#   pulse 9: activated nodes: 11304, borderline nodes: 622, overall activation: 4586.153, activation diff: 145.813, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 4586.2
#   number of spread. activ. pulses: 9
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9975411   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9969965   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99678874   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9933238   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98482716   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7469718   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7468933   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7467759   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7466898   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.7465841   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.74658334   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.746507   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.7464909   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7464825   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7464613   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.7464459   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.7464274   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.7464061   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   20   0.7464032   REFERENCES:SIMDATES
