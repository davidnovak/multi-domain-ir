###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1869.379, activation diff: 1860.577, ratio: 0.995
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 3394.465, activation diff: 1578.198, ratio: 0.465
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 6921.107, activation diff: 3526.641, ratio: 0.510
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 8241.348, activation diff: 1320.241, ratio: 0.160
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8680.155, activation diff: 438.807, ratio: 0.051
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8822.205, activation diff: 142.050, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8822.2
#   number of spread. activ. pulses: 7
#   running time: 1226

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998145   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997821   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99973047   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99968845   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99759465   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99400324   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997785   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8997623   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.89974844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8997475   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.8997441   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8997431   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.8997421   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.8997339   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.8997325   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.8997319   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.8997311   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.899726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.8997226   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.89971226   REFERENCES:SIMDATES:SIMLOC
