###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 850.673, activation diff: 830.809, ratio: 0.977
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1913.704, activation diff: 1063.030, ratio: 0.555
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3973.633, activation diff: 2059.929, ratio: 0.518
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5419.487, activation diff: 1445.854, ratio: 0.267
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6283.648, activation diff: 864.161, ratio: 0.138
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6776.505, activation diff: 492.857, ratio: 0.073
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7052.284, activation diff: 275.779, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7052.3
#   number of spread. activ. pulses: 8
#   running time: 1222

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996089   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99585176   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9956081   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9952854   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9918201   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9846501   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79372215   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79364896   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7936013   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7935976   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.79350996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.79344654   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7934216   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7933984   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.793393   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7933712   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.79336953   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.7933658   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.7933356   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7933075   REFERENCES:SIMDATES:SIMLOC
