###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 278.533, activation diff: 265.830, ratio: 0.954
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 771.022, activation diff: 492.495, ratio: 0.639
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1872.136, activation diff: 1101.114, ratio: 0.588
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3121.976, activation diff: 1249.840, ratio: 0.400
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 4260.805, activation diff: 1138.829, ratio: 0.267
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5218.988, activation diff: 958.183, ratio: 0.184
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5999.125, activation diff: 780.137, ratio: 0.130
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6623.765, activation diff: 624.640, ratio: 0.094
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 7118.992, activation diff: 495.227, ratio: 0.070
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 7509.130, activation diff: 390.138, ratio: 0.052
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 7815.150, activation diff: 306.019, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7815.1
#   number of spread. activ. pulses: 12
#   running time: 1875

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98612416   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98492545   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.983123   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97483724   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9606862   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.86083627   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.85990226   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.8595437   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.85951066   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.85886085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.85825396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8581344   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.85810685   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.8580648   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.8580196   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.8578813   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.85777247   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.8576854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.857589   REFERENCES:SIMDATES:SIMLOC
