###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 546.576, activation diff: 532.681, ratio: 0.975
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 1182.353, activation diff: 639.017, ratio: 0.540
#   pulse 4: activated nodes: 11253, borderline nodes: 5542, overall activation: 3472.849, activation diff: 2290.496, ratio: 0.660
#   pulse 5: activated nodes: 11401, borderline nodes: 999, overall activation: 5445.790, activation diff: 1972.941, ratio: 0.362
#   pulse 6: activated nodes: 11438, borderline nodes: 309, overall activation: 6730.993, activation diff: 1285.203, ratio: 0.191
#   pulse 7: activated nodes: 11452, borderline nodes: 113, overall activation: 7497.540, activation diff: 766.547, ratio: 0.102
#   pulse 8: activated nodes: 11455, borderline nodes: 59, overall activation: 7939.787, activation diff: 442.247, ratio: 0.056
#   pulse 9: activated nodes: 11455, borderline nodes: 51, overall activation: 8191.215, activation diff: 251.428, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 8191.2
#   number of spread. activ. pulses: 9
#   running time: 1320

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9976109   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9970224   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9969794   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99335665   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9852483   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89638495   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8962885   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8961383   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8960571   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89595896   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.895927   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8958457   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.89583886   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8958297   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8958153   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8958068   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   18   0.89580166   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8957894   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.89577454   REFERENCES:SIMDATES:SIMLOC
