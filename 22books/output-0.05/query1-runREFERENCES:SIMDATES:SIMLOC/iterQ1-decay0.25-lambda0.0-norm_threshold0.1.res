###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2204.796, activation diff: 2230.148, ratio: 1.011
#   pulse 3: activated nodes: 9963, borderline nodes: 3894, overall activation: 1539.674, activation diff: 3208.517, ratio: 2.084
#   pulse 4: activated nodes: 11402, borderline nodes: 2123, overall activation: 5933.228, activation diff: 4803.079, ratio: 0.810
#   pulse 5: activated nodes: 11453, borderline nodes: 162, overall activation: 6376.317, activation diff: 505.216, ratio: 0.079
#   pulse 6: activated nodes: 11457, borderline nodes: 56, overall activation: 6473.935, activation diff: 97.938, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6473.9
#   number of spread. activ. pulses: 6
#   running time: 1197

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999934   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999631   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_470   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_807   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_805   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   20   0.75   REFERENCES:SIMDATES:SIMLOC
