###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 90.456, activation diff: 79.082, ratio: 0.874
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 234.005, activation diff: 143.598, ratio: 0.614
#   pulse 4: activated nodes: 10582, borderline nodes: 5981, overall activation: 523.665, activation diff: 289.660, ratio: 0.553
#   pulse 5: activated nodes: 10810, borderline nodes: 4433, overall activation: 885.258, activation diff: 361.594, ratio: 0.408
#   pulse 6: activated nodes: 10908, borderline nodes: 3839, overall activation: 1268.651, activation diff: 383.393, ratio: 0.302
#   pulse 7: activated nodes: 10946, borderline nodes: 3637, overall activation: 1635.566, activation diff: 366.914, ratio: 0.224
#   pulse 8: activated nodes: 10959, borderline nodes: 3567, overall activation: 1962.190, activation diff: 326.624, ratio: 0.166
#   pulse 9: activated nodes: 10959, borderline nodes: 3549, overall activation: 2237.979, activation diff: 275.789, ratio: 0.123
#   pulse 10: activated nodes: 10963, borderline nodes: 3535, overall activation: 2463.779, activation diff: 225.800, ratio: 0.092
#   pulse 11: activated nodes: 10966, borderline nodes: 3528, overall activation: 2645.585, activation diff: 181.806, ratio: 0.069
#   pulse 12: activated nodes: 10967, borderline nodes: 3526, overall activation: 2790.516, activation diff: 144.931, ratio: 0.052
#   pulse 13: activated nodes: 10967, borderline nodes: 3526, overall activation: 2905.293, activation diff: 114.778, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 2905.3
#   number of spread. activ. pulses: 13
#   running time: 1572

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99143183   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98815596   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9864739   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98512685   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97701746   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96024394   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48330763   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.48284024   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.48250163   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.4824528   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.48198375   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.48179626   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.4817927   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.48165452   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4816089   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4815945   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.4815191   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.48147988   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.48146433   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.4814446   REFERENCES:SIMDATES:SIMLOC
