###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 589.583, activation diff: 573.330, ratio: 0.972
#   pulse 3: activated nodes: 9188, borderline nodes: 4940, overall activation: 1254.744, activation diff: 665.163, ratio: 0.530
#   pulse 4: activated nodes: 11316, borderline nodes: 4937, overall activation: 3173.363, activation diff: 1918.618, ratio: 0.605
#   pulse 5: activated nodes: 11420, borderline nodes: 711, overall activation: 4734.213, activation diff: 1560.850, ratio: 0.330
#   pulse 6: activated nodes: 11442, borderline nodes: 252, overall activation: 5720.859, activation diff: 986.646, ratio: 0.172
#   pulse 7: activated nodes: 11453, borderline nodes: 86, overall activation: 6299.604, activation diff: 578.746, ratio: 0.092
#   pulse 8: activated nodes: 11455, borderline nodes: 61, overall activation: 6629.662, activation diff: 330.058, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6629.7
#   number of spread. activ. pulses: 8
#   running time: 1242

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960693   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9955386   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9948503   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99459636   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9903121   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98039865   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.793656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7935225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79337144   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.793309   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.79309654   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.79308176   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79305124   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79298437   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.79295564   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.79292977   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7929177   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.7929143   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.792885   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.79284215   REFERENCES:SIMDATES:SIMLOC
