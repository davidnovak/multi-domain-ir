###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1198.079, activation diff: 1196.968, ratio: 0.999
#   pulse 3: activated nodes: 9459, borderline nodes: 3915, overall activation: 1661.790, activation diff: 852.887, ratio: 0.513
#   pulse 4: activated nodes: 11344, borderline nodes: 3274, overall activation: 4462.506, activation diff: 2802.519, ratio: 0.628
#   pulse 5: activated nodes: 11428, borderline nodes: 471, overall activation: 5681.843, activation diff: 1219.337, ratio: 0.215
#   pulse 6: activated nodes: 11451, borderline nodes: 149, overall activation: 6115.743, activation diff: 433.900, ratio: 0.071
#   pulse 7: activated nodes: 11455, borderline nodes: 76, overall activation: 6263.358, activation diff: 147.616, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6263.4
#   number of spread. activ. pulses: 7
#   running time: 1210

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99966764   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9994967   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9994385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9968753   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.992288   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74978966   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74973565   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74966776   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.74966663   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74966276   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7496428   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.74962854   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.74961686   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   15   0.7496152   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.74961126   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   17   0.7496092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.74960387   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.74958897   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.7495835   REFERENCES:SIMDATES:SIMLOC
