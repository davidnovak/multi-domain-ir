###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 375.699, activation diff: 359.446, ratio: 0.957
#   pulse 3: activated nodes: 9188, borderline nodes: 4940, overall activation: 711.176, activation diff: 335.477, ratio: 0.472
#   pulse 4: activated nodes: 10745, borderline nodes: 5512, overall activation: 1505.781, activation diff: 794.605, ratio: 0.528
#   pulse 5: activated nodes: 10932, borderline nodes: 3679, overall activation: 2193.594, activation diff: 687.813, ratio: 0.314
#   pulse 6: activated nodes: 10959, borderline nodes: 3558, overall activation: 2663.526, activation diff: 469.933, ratio: 0.176
#   pulse 7: activated nodes: 10964, borderline nodes: 3532, overall activation: 2946.777, activation diff: 283.251, ratio: 0.096
#   pulse 8: activated nodes: 10967, borderline nodes: 3525, overall activation: 3110.245, activation diff: 163.468, ratio: 0.053
#   pulse 9: activated nodes: 10967, borderline nodes: 3524, overall activation: 3202.857, activation diff: 92.612, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3202.9
#   number of spread. activ. pulses: 9
#   running time: 1335

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99803466   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9977683   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9974246   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99727154   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.993944   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9868894   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4980173   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49797508   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4979285   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.497909   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.49784005   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49783754   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.49782807   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.4978051   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49779594   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.49778488   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.49778458   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.49778077   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.49777228   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.49775803   REFERENCES:SIMDATES:SIMLOC
