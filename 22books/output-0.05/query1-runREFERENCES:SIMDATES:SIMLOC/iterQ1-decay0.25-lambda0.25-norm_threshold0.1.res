###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1375.586, activation diff: 1370.580, ratio: 0.996
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 2106.098, activation diff: 935.889, ratio: 0.444
#   pulse 4: activated nodes: 11394, borderline nodes: 2615, overall activation: 4822.344, activation diff: 2716.482, ratio: 0.563
#   pulse 5: activated nodes: 11445, borderline nodes: 248, overall activation: 5917.658, activation diff: 1095.313, ratio: 0.185
#   pulse 6: activated nodes: 11456, borderline nodes: 69, overall activation: 6291.757, activation diff: 374.099, ratio: 0.059
#   pulse 7: activated nodes: 11457, borderline nodes: 50, overall activation: 6415.311, activation diff: 123.555, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6415.3
#   number of spread. activ. pulses: 7
#   running time: 1241

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997456   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9996363   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996183   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99726903   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9932144   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7498092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7497801   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7497499   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7497448   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.74974364   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.74973524   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.74973166   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7497253   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   15   0.74971974   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.74971807   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.74971485   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7497036   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.74969435   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.74969196   REFERENCES:SIMDATES:SIMLOC
