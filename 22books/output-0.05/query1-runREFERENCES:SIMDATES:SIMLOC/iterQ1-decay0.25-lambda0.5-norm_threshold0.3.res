###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 458.314, activation diff: 444.419, ratio: 0.970
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 945.055, activation diff: 489.466, ratio: 0.518
#   pulse 4: activated nodes: 11221, borderline nodes: 5665, overall activation: 2537.175, activation diff: 1592.121, ratio: 0.628
#   pulse 5: activated nodes: 11391, borderline nodes: 1486, overall activation: 3955.205, activation diff: 1418.030, ratio: 0.359
#   pulse 6: activated nodes: 11428, borderline nodes: 481, overall activation: 4897.357, activation diff: 942.152, ratio: 0.192
#   pulse 7: activated nodes: 11438, borderline nodes: 231, overall activation: 5462.268, activation diff: 564.911, ratio: 0.103
#   pulse 8: activated nodes: 11446, borderline nodes: 163, overall activation: 5788.324, activation diff: 326.057, ratio: 0.056
#   pulse 9: activated nodes: 11448, borderline nodes: 127, overall activation: 5973.559, activation diff: 185.235, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5973.6
#   number of spread. activ. pulses: 9
#   running time: 1234

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9976107   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9970224   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9969775   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99334747   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9852006   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74698746   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7469067   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74678195   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7467143   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74663115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.7466058   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.74653804   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.74653137   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.74652326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.74651265   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.74650264   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   18   0.74649984   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7464894   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.74647474   REFERENCES:SIMDATES:SIMLOC
