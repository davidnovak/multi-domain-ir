###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1100.309, activation diff: 1102.142, ratio: 1.002
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 1418.379, activation diff: 884.760, ratio: 0.624
#   pulse 4: activated nodes: 11323, borderline nodes: 4905, overall activation: 4617.005, activation diff: 3207.923, ratio: 0.695
#   pulse 5: activated nodes: 11415, borderline nodes: 674, overall activation: 6108.365, activation diff: 1491.368, ratio: 0.244
#   pulse 6: activated nodes: 11442, borderline nodes: 195, overall activation: 6666.031, activation diff: 557.665, ratio: 0.084
#   pulse 7: activated nodes: 11454, borderline nodes: 74, overall activation: 6861.734, activation diff: 195.703, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6861.7
#   number of spread. activ. pulses: 7
#   running time: 1239

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999527   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992944   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9990801   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99640805   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99122286   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79971975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7996356   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.7995124   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7995085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.79949695   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.79945093   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7994419   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.79944134   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.79944   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.79941607   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   17   0.79939425   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7993877   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7993767   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.799366   REFERENCES:SIMDATES:SIMLOC
