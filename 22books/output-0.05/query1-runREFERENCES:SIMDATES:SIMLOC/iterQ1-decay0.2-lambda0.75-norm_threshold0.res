###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 249.565, activation diff: 236.861, ratio: 0.949
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 671.603, activation diff: 422.044, ratio: 0.628
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1560.944, activation diff: 889.341, ratio: 0.570
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2580.192, activation diff: 1019.248, ratio: 0.395
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3520.266, activation diff: 940.074, ratio: 0.267
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4316.322, activation diff: 796.056, ratio: 0.184
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 4966.729, activation diff: 650.407, ratio: 0.131
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5488.625, activation diff: 521.896, ratio: 0.095
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 5903.049, activation diff: 414.424, ratio: 0.070
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 6229.951, activation diff: 326.902, ratio: 0.052
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 6486.657, activation diff: 256.706, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6486.7
#   number of spread. activ. pulses: 12
#   running time: 1964

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9861221   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98492336   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.983114   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97480357   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96057415   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7651858   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76435506   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7640355   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7640062   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.76342636   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.76288074   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.76277405   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7627547   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7627173   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.76267385   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7625509   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.76245415   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.76236767   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.76229066   REFERENCES:SIMDATES:SIMLOC
