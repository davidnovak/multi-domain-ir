###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 923.210, activation diff: 918.205, ratio: 0.995
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 1201.732, activation diff: 426.555, ratio: 0.355
#   pulse 4: activated nodes: 10891, borderline nodes: 3755, overall activation: 2501.958, activation diff: 1300.260, ratio: 0.520
#   pulse 5: activated nodes: 10967, borderline nodes: 3534, overall activation: 3063.646, activation diff: 561.688, ratio: 0.183
#   pulse 6: activated nodes: 10969, borderline nodes: 3521, overall activation: 3259.076, activation diff: 195.430, ratio: 0.060
#   pulse 7: activated nodes: 10969, borderline nodes: 3519, overall activation: 3323.972, activation diff: 64.895, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 3324.0
#   number of spread. activ. pulses: 7
#   running time: 1202

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997456   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9996363   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996182   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972658   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9932133   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4998721   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49985164   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.49983263   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   10   0.4998244   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.4998231   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.4998201   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.4998194   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.49980852   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.49980775   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.49980542   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.4998015   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.49979356   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.49979347   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.49979028   REFERENCES:SIMDATES:SIMLOC
