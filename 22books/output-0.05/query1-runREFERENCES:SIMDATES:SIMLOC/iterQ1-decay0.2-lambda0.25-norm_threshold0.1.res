###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1466.061, activation diff: 1461.056, ratio: 0.997
#   pulse 3: activated nodes: 9885, borderline nodes: 3956, overall activation: 2302.246, activation diff: 1052.289, ratio: 0.457
#   pulse 4: activated nodes: 11396, borderline nodes: 2601, overall activation: 5361.629, activation diff: 3059.624, ratio: 0.571
#   pulse 5: activated nodes: 11445, borderline nodes: 224, overall activation: 6587.443, activation diff: 1225.814, ratio: 0.186
#   pulse 6: activated nodes: 11457, borderline nodes: 53, overall activation: 7007.096, activation diff: 419.653, ratio: 0.060
#   pulse 7: activated nodes: 11459, borderline nodes: 27, overall activation: 7146.131, activation diff: 139.035, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11459
#   final overall activation: 7146.1
#   number of spread. activ. pulses: 7
#   running time: 1251

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997456   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9996363   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996183   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972692   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9932144   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7997967   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.799766   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79973346   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7997298   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.799728   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7997186   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.7997143   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7997103   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   15   0.79970634   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7997004   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.79969656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7996841   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7996757   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.79967475   REFERENCES:SIMDATES:SIMLOC
