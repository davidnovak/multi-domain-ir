###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1276.916, activation diff: 1275.805, ratio: 0.999
#   pulse 3: activated nodes: 9459, borderline nodes: 3915, overall activation: 1820.363, activation diff: 955.720, ratio: 0.525
#   pulse 4: activated nodes: 11344, borderline nodes: 3246, overall activation: 4984.506, activation diff: 3166.104, ratio: 0.635
#   pulse 5: activated nodes: 11429, borderline nodes: 429, overall activation: 6348.753, activation diff: 1364.247, ratio: 0.215
#   pulse 6: activated nodes: 11454, borderline nodes: 104, overall activation: 6834.900, activation diff: 486.147, ratio: 0.071
#   pulse 7: activated nodes: 11455, borderline nodes: 53, overall activation: 7000.698, activation diff: 165.799, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7000.7
#   number of spread. activ. pulses: 7
#   running time: 1191

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996677   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99949676   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9994385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9968755   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9922881   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79977596   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79971874   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.7996471   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7996457   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7996416   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7996205   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.79960537   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   14   0.7995996   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   15   0.7995904   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.79958546   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   17   0.7995833   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.7995817   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7995616   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.7995558   REFERENCES:SIMDATES:SIMLOC
