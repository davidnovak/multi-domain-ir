###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 235.081, activation diff: 222.377, ratio: 0.946
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 623.046, activation diff: 387.971, ratio: 0.623
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1413.664, activation diff: 790.618, ratio: 0.559
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2322.252, activation diff: 908.588, ratio: 0.391
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 3165.936, activation diff: 843.684, ratio: 0.266
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 3882.994, activation diff: 717.058, ratio: 0.185
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 4470.045, activation diff: 587.051, ratio: 0.131
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 4941.651, activation diff: 471.606, ratio: 0.095
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 5316.413, activation diff: 374.762, ratio: 0.070
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 5612.176, activation diff: 295.764, ratio: 0.053
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 5844.515, activation diff: 232.338, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5844.5
#   number of spread. activ. pulses: 12
#   running time: 3544

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9861208   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98492205   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98310864   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9747835   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9605065   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7173608   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7165816   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.71628165   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7162542   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.71570945   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7151943   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.715094   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7150782   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7150442   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7150011   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7148858   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7147937   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7147095   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7146412   REFERENCES:SIMDATES:SIMLOC
