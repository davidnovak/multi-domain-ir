###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 249.387, activation diff: 236.683, ratio: 0.949
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 667.764, activation diff: 418.384, ratio: 0.627
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1549.485, activation diff: 881.721, ratio: 0.569
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2562.119, activation diff: 1012.634, ratio: 0.395
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3496.815, activation diff: 934.696, ratio: 0.267
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 4288.436, activation diff: 791.621, ratio: 0.185
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 4935.135, activation diff: 646.698, ratio: 0.131
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 5453.905, activation diff: 518.770, ratio: 0.095
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 5865.682, activation diff: 411.777, ratio: 0.070
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 6190.339, activation diff: 324.657, ratio: 0.052
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 6445.140, activation diff: 254.801, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6445.1
#   number of spread. activ. pulses: 12
#   running time: 1519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9861114   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98486054   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.983114   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9748034   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96048445   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7651729   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7643547   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7640352   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7639895   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.76341504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7628765   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.76277405   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.762754   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.76267385   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.7626222   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.76254326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7624381   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7623511   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.76229066   REFERENCES:SIMLOC
