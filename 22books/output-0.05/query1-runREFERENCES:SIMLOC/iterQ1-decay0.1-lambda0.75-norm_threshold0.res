###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 278.340, activation diff: 265.636, ratio: 0.954
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 768.248, activation diff: 489.915, ratio: 0.638
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1864.387, activation diff: 1096.139, ratio: 0.588
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3109.523, activation diff: 1245.136, ratio: 0.400
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4243.872, activation diff: 1134.349, ratio: 0.267
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 5197.781, activation diff: 953.909, ratio: 0.184
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 5973.899, activation diff: 776.119, ratio: 0.130
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 6594.836, activation diff: 620.937, ratio: 0.094
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 7086.719, activation diff: 491.882, ratio: 0.069
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 7473.886, activation diff: 387.167, ratio: 0.052
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 7777.302, activation diff: 303.416, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7777.3
#   number of spread. activ. pulses: 12
#   running time: 1490

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98611355   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9848628   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.983123   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9748372   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96059823   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8608211   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8599019   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.85954356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.85949266   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8588481   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.85824907   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8581344   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.8581064   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8580196   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.8579533   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.8578727   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.85775566   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.857667   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.857589   REFERENCES:SIMLOC
