###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 458.095, activation diff: 444.489, ratio: 0.970
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 934.423, activation diff: 479.315, ratio: 0.513
#   pulse 4: activated nodes: 11184, borderline nodes: 5628, overall activation: 2508.474, activation diff: 1574.052, ratio: 0.627
#   pulse 5: activated nodes: 11385, borderline nodes: 1544, overall activation: 3916.161, activation diff: 1407.687, ratio: 0.359
#   pulse 6: activated nodes: 11419, borderline nodes: 479, overall activation: 4851.971, activation diff: 935.810, ratio: 0.193
#   pulse 7: activated nodes: 11429, borderline nodes: 232, overall activation: 5412.562, activation diff: 560.591, ratio: 0.104
#   pulse 8: activated nodes: 11436, borderline nodes: 165, overall activation: 5735.584, activation diff: 323.023, ratio: 0.056
#   pulse 9: activated nodes: 11439, borderline nodes: 133, overall activation: 5918.712, activation diff: 183.127, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 5918.7
#   number of spread. activ. pulses: 9
#   running time: 1409

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99759954   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969775   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99689907   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9933474   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9850941   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.746979   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7469067   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7467661   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7467143   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74663115   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.7466058   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7465308   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7465173   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.74651474   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.74651253   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.74649984   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.7464894   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7464837   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.74647474   REFERENCES:SIMLOC
