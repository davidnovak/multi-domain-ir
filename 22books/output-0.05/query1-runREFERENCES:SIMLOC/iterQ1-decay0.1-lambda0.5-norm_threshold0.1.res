###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 801.728, activation diff: 783.413, ratio: 0.977
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1794.104, activation diff: 992.376, ratio: 0.553
#   pulse 4: activated nodes: 11318, borderline nodes: 2985, overall activation: 4275.124, activation diff: 2481.019, ratio: 0.580
#   pulse 5: activated nodes: 11430, borderline nodes: 312, overall activation: 6113.639, activation diff: 1838.516, ratio: 0.301
#   pulse 6: activated nodes: 11449, borderline nodes: 67, overall activation: 7229.997, activation diff: 1116.358, ratio: 0.154
#   pulse 7: activated nodes: 11452, borderline nodes: 28, overall activation: 7871.050, activation diff: 641.052, ratio: 0.081
#   pulse 8: activated nodes: 11454, borderline nodes: 12, overall activation: 8231.036, activation diff: 359.987, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8231.0
#   number of spread. activ. pulses: 8
#   running time: 1366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957229   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9952332   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9950031   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99113846   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9825665   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8929062   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8927995   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89268816   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8926778   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.89250547   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8924512   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.8924275   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89240944   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89234096   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.8923247   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.8923046   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8922991   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8922769   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.8922765   REFERENCES:SIMLOC
