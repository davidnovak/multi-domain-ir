###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 849.937, activation diff: 830.073, ratio: 0.977
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1900.162, activation diff: 1050.224, ratio: 0.553
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3946.208, activation diff: 2046.046, ratio: 0.518
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5384.784, activation diff: 1438.576, ratio: 0.267
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6243.947, activation diff: 859.163, ratio: 0.138
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 6733.177, activation diff: 489.231, ratio: 0.073
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 7006.335, activation diff: 273.158, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7006.3
#   number of spread. activ. pulses: 8
#   running time: 1363

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996089   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99584615   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9955529   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9952854   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9918201   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9845109   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79371905   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79364884   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7935976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7935926   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.793502   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7934444   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7934216   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.79339814   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.7933712   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.79336405   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7933556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.79332626   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.79331315   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7933075   REFERENCES:SIMLOC
