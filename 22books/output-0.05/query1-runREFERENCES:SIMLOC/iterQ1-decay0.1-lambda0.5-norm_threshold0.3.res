###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 546.357, activation diff: 532.750, ratio: 0.975
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 1175.765, activation diff: 632.962, ratio: 0.538
#   pulse 4: activated nodes: 11200, borderline nodes: 5489, overall activation: 3454.549, activation diff: 2278.784, ratio: 0.660
#   pulse 5: activated nodes: 11394, borderline nodes: 1003, overall activation: 5420.936, activation diff: 1966.388, ratio: 0.363
#   pulse 6: activated nodes: 11432, borderline nodes: 306, overall activation: 6699.057, activation diff: 1278.121, ratio: 0.191
#   pulse 7: activated nodes: 11444, borderline nodes: 111, overall activation: 7458.665, activation diff: 759.608, ratio: 0.102
#   pulse 8: activated nodes: 11449, borderline nodes: 56, overall activation: 7894.977, activation diff: 436.312, ratio: 0.055
#   pulse 9: activated nodes: 11449, borderline nodes: 45, overall activation: 8141.761, activation diff: 246.784, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 8141.8
#   number of spread. activ. pulses: 9
#   running time: 1317

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99759984   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969794   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9968991   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9933566   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9851424   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8963748   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8962885   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8961194   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8960571   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89595896   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.895927   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.89583886   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.8958208   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89581954   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8958153   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.89580166   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.8957894   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.89578426   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.89577454   REFERENCES:SIMLOC
