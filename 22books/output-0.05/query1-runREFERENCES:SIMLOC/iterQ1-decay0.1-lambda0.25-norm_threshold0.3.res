###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1235.802, activation diff: 1238.469, ratio: 1.002
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 1666.153, activation diff: 1081.915, ratio: 0.649
#   pulse 4: activated nodes: 11258, borderline nodes: 4757, overall activation: 5619.170, activation diff: 3978.103, ratio: 0.708
#   pulse 5: activated nodes: 11416, borderline nodes: 590, overall activation: 7440.943, activation diff: 1821.788, ratio: 0.245
#   pulse 6: activated nodes: 11443, borderline nodes: 130, overall activation: 8110.795, activation diff: 669.852, ratio: 0.083
#   pulse 7: activated nodes: 11449, borderline nodes: 48, overall activation: 8341.212, activation diff: 230.417, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 8341.2
#   number of spread. activ. pulses: 7
#   running time: 1329

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99950397   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99929446   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99883974   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99640864   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99111724   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8996496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89959216   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.899455   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.89941853   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8993842   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.8993809   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.8993744   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.899372   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89936084   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.8993432   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.899311   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.89929956   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8992815   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.8992814   REFERENCES:SIMLOC
