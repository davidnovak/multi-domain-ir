###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 121.214, activation diff: 109.148, ratio: 0.900
#   pulse 3: activated nodes: 9338, borderline nodes: 3902, overall activation: 297.784, activation diff: 176.597, ratio: 0.593
#   pulse 4: activated nodes: 10715, borderline nodes: 4529, overall activation: 617.258, activation diff: 319.474, ratio: 0.518
#   pulse 5: activated nodes: 10839, borderline nodes: 3800, overall activation: 1000.817, activation diff: 383.559, ratio: 0.383
#   pulse 6: activated nodes: 10883, borderline nodes: 3596, overall activation: 1391.926, activation diff: 391.108, ratio: 0.281
#   pulse 7: activated nodes: 10887, borderline nodes: 3537, overall activation: 1752.711, activation diff: 360.786, ratio: 0.206
#   pulse 8: activated nodes: 10893, borderline nodes: 3521, overall activation: 2063.286, activation diff: 310.575, ratio: 0.151
#   pulse 9: activated nodes: 10896, borderline nodes: 3517, overall activation: 2320.031, activation diff: 256.745, ratio: 0.111
#   pulse 10: activated nodes: 10896, borderline nodes: 3516, overall activation: 2527.747, activation diff: 207.716, ratio: 0.082
#   pulse 11: activated nodes: 10897, borderline nodes: 3516, overall activation: 2693.730, activation diff: 165.983, ratio: 0.062
#   pulse 12: activated nodes: 10897, borderline nodes: 3514, overall activation: 2825.324, activation diff: 131.594, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2825.3
#   number of spread. activ. pulses: 12
#   running time: 1500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888499   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98526764   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98356384   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98178315   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97263974   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9552238   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4780185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.47744396   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.47711745   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4771167   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.47661975   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.47625634   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.47625163   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.4762153   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.47610253   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.476067   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.4759683   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.4759114   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.47589546   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.47588363   REFERENCES:SIMLOC
