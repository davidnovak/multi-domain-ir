###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1099.892, activation diff: 1102.559, ratio: 1.002
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 1400.129, activation diff: 883.027, ratio: 0.631
#   pulse 4: activated nodes: 11258, borderline nodes: 4840, overall activation: 4571.933, activation diff: 3191.053, ratio: 0.698
#   pulse 5: activated nodes: 11409, borderline nodes: 673, overall activation: 6063.028, activation diff: 1491.103, ratio: 0.246
#   pulse 6: activated nodes: 11435, borderline nodes: 191, overall activation: 6616.227, activation diff: 553.198, ratio: 0.084
#   pulse 7: activated nodes: 11447, borderline nodes: 71, overall activation: 6807.913, activation diff: 191.687, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6807.9
#   number of spread. activ. pulses: 7
#   running time: 1280

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99950397   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992944   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99883974   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99640787   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9911157   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79968774   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7996356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.7995124   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.79947615   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.79945093   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.7994498   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7994419   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79944   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7994278   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.799416   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7993877   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.7993767   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.79936147   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.7993613   REFERENCES:SIMLOC
