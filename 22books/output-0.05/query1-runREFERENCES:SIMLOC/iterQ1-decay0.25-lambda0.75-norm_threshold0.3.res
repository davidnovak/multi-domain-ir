###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 91.986, activation diff: 81.368, ratio: 0.885
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 267.867, activation diff: 175.958, ratio: 0.657
#   pulse 4: activated nodes: 10863, borderline nodes: 6848, overall activation: 745.945, activation diff: 478.077, ratio: 0.641
#   pulse 5: activated nodes: 11215, borderline nodes: 4159, overall activation: 1414.489, activation diff: 668.545, ratio: 0.473
#   pulse 6: activated nodes: 11342, borderline nodes: 2196, overall activation: 2173.080, activation diff: 758.591, ratio: 0.349
#   pulse 7: activated nodes: 11405, borderline nodes: 1225, overall activation: 2904.084, activation diff: 731.004, ratio: 0.252
#   pulse 8: activated nodes: 11417, borderline nodes: 569, overall activation: 3542.242, activation diff: 638.157, ratio: 0.180
#   pulse 9: activated nodes: 11423, borderline nodes: 389, overall activation: 4073.089, activation diff: 530.847, ratio: 0.130
#   pulse 10: activated nodes: 11429, borderline nodes: 250, overall activation: 4504.649, activation diff: 431.560, ratio: 0.096
#   pulse 11: activated nodes: 11432, borderline nodes: 204, overall activation: 4850.816, activation diff: 346.167, ratio: 0.071
#   pulse 12: activated nodes: 11435, borderline nodes: 172, overall activation: 5126.194, activation diff: 275.378, ratio: 0.054
#   pulse 13: activated nodes: 11437, borderline nodes: 160, overall activation: 5344.059, activation diff: 217.865, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 5344.1
#   number of spread. activ. pulses: 13
#   running time: 1583

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911155   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9871273   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9847573   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9837662   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9752291   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95578045   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7245102   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72381556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7230666   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7229229   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.72203404   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.72203326   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7220189   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.7216554   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.72159517   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7215884   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.72157407   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7215623   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7213894   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7213456   REFERENCES:SIMLOC
