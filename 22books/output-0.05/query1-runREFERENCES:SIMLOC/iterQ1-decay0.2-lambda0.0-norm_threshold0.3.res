###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1860.383, activation diff: 1885.280, ratio: 1.013
#   pulse 3: activated nodes: 9395, borderline nodes: 3931, overall activation: 1090.718, activation diff: 2904.710, ratio: 2.663
#   pulse 4: activated nodes: 11304, borderline nodes: 3303, overall activation: 5884.951, activation diff: 5714.764, ratio: 0.971
#   pulse 5: activated nodes: 11423, borderline nodes: 433, overall activation: 6347.374, activation diff: 1296.249, ratio: 0.204
#   pulse 6: activated nodes: 11446, borderline nodes: 102, overall activation: 6889.564, activation diff: 559.241, ratio: 0.081
#   pulse 7: activated nodes: 11450, borderline nodes: 53, overall activation: 6925.784, activation diff: 38.196, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 6925.8
#   number of spread. activ. pulses: 7
#   running time: 1286

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995494   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   10   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_348   12   0.8   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_930   13   0.8   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   14   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   15   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   16   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   17   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   18   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   19   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   20   0.8   REFERENCES:SIMLOC
