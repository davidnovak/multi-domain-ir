###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 803.320, activation diff: 803.317, ratio: 1.000
#   pulse 3: activated nodes: 9457, borderline nodes: 3914, overall activation: 906.522, activation diff: 386.488, ratio: 0.426
#   pulse 4: activated nodes: 10774, borderline nodes: 4128, overall activation: 2251.060, activation diff: 1344.816, ratio: 0.597
#   pulse 5: activated nodes: 10883, borderline nodes: 3594, overall activation: 2894.304, activation diff: 643.243, ratio: 0.222
#   pulse 6: activated nodes: 10891, borderline nodes: 3526, overall activation: 3134.587, activation diff: 240.283, ratio: 0.077
#   pulse 7: activated nodes: 10895, borderline nodes: 3518, overall activation: 3218.055, activation diff: 83.468, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3218.1
#   number of spread. activ. pulses: 7
#   running time: 1275

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996511   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9994965   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9992517   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9968686   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9921836   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4998485   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.4998215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.49977094   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   10   0.49975705   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49975657   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.49975377   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.4997406   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.49974036   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4997393   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.49973887   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.49972066   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.49971893   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.49971017   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.49970475   REFERENCES:SIMLOC
