###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2642.506, activation diff: 2669.415, ratio: 1.010
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 2117.078, activation diff: 4129.130, ratio: 1.950
#   pulse 4: activated nodes: 11373, borderline nodes: 2059, overall activation: 7871.776, activation diff: 6468.979, ratio: 0.822
#   pulse 5: activated nodes: 11445, borderline nodes: 145, overall activation: 8555.686, activation diff: 783.234, ratio: 0.092
#   pulse 6: activated nodes: 11452, borderline nodes: 24, overall activation: 8681.218, activation diff: 129.754, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 8681.2
#   number of spread. activ. pulses: 6
#   running time: 2411

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999934   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999631   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMLOC
