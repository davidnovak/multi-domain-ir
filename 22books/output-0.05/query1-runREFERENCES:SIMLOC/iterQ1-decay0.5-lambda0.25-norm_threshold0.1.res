###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 922.482, activation diff: 918.656, ratio: 0.996
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 1167.345, activation diff: 407.226, ratio: 0.349
#   pulse 4: activated nodes: 10816, borderline nodes: 3746, overall activation: 2444.784, activation diff: 1277.473, ratio: 0.523
#   pulse 5: activated nodes: 10895, borderline nodes: 3528, overall activation: 3004.925, activation diff: 560.142, ratio: 0.186
#   pulse 6: activated nodes: 10897, borderline nodes: 3515, overall activation: 3201.480, activation diff: 196.555, ratio: 0.061
#   pulse 7: activated nodes: 10897, borderline nodes: 3513, overall activation: 3267.122, activation diff: 65.642, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 3267.1
#   number of spread. activ. pulses: 7
#   running time: 1301

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999737   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996182   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9995358   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972657   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931298   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49986827   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49985164   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.49982423   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4998216   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.4998201   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.4998194   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.4998149   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.49980366   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.49980146   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.49979693   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.49979347   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.49978697   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   19   0.49978542   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.49978277   REFERENCES:SIMLOC
