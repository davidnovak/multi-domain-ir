###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 671.627, activation diff: 653.312, ratio: 0.973
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1424.023, activation diff: 752.395, ratio: 0.528
#   pulse 4: activated nodes: 11316, borderline nodes: 3075, overall activation: 3166.581, activation diff: 1742.559, ratio: 0.550
#   pulse 5: activated nodes: 11425, borderline nodes: 413, overall activation: 4496.720, activation diff: 1330.138, ratio: 0.296
#   pulse 6: activated nodes: 11440, borderline nodes: 136, overall activation: 5312.195, activation diff: 815.476, ratio: 0.154
#   pulse 7: activated nodes: 11442, borderline nodes: 79, overall activation: 5782.023, activation diff: 469.828, ratio: 0.081
#   pulse 8: activated nodes: 11444, borderline nodes: 63, overall activation: 6046.440, activation diff: 264.417, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 6046.4
#   number of spread. activ. pulses: 8
#   running time: 1396

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99572265   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9952332   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99500203   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99113196   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9825543   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74408805   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7439985   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7439065   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.74389756   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.74375314   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7437062   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.7436853   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.74367404   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.74361414   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7435955   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7435869   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7435786   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7435622   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.74356186   REFERENCES:SIMLOC
