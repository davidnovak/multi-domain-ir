###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 589.347, activation diff: 573.283, ratio: 0.973
#   pulse 3: activated nodes: 9185, borderline nodes: 4938, overall activation: 1244.966, activation diff: 655.620, ratio: 0.527
#   pulse 4: activated nodes: 11254, borderline nodes: 4913, overall activation: 3147.448, activation diff: 1902.482, ratio: 0.604
#   pulse 5: activated nodes: 11413, borderline nodes: 712, overall activation: 4699.487, activation diff: 1552.039, ratio: 0.330
#   pulse 6: activated nodes: 11436, borderline nodes: 249, overall activation: 5679.764, activation diff: 980.276, ratio: 0.173
#   pulse 7: activated nodes: 11447, borderline nodes: 90, overall activation: 6253.557, activation diff: 573.794, ratio: 0.092
#   pulse 8: activated nodes: 11449, borderline nodes: 56, overall activation: 6579.861, activation diff: 326.304, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6579.9
#   number of spread. activ. pulses: 8
#   running time: 1417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960693   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99552447   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9946981   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99459636   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9903121   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9802119   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79364717   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7935225   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79335093   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.793309   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.79309654   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.7930559   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79305124   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79298437   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7929431   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.7929177   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7929137   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.79290324   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.792885   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.79284215   REFERENCES:SIMLOC
