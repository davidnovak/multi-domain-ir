###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 135.361, activation diff: 123.988, ratio: 0.916
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 383.271, activation diff: 247.960, ratio: 0.647
#   pulse 4: activated nodes: 11119, borderline nodes: 6069, overall activation: 1020.977, activation diff: 637.706, ratio: 0.625
#   pulse 5: activated nodes: 11317, borderline nodes: 2553, overall activation: 1879.790, activation diff: 858.813, ratio: 0.457
#   pulse 6: activated nodes: 11402, borderline nodes: 1002, overall activation: 2781.374, activation diff: 901.584, ratio: 0.324
#   pulse 7: activated nodes: 11422, borderline nodes: 444, overall activation: 3597.357, activation diff: 815.983, ratio: 0.227
#   pulse 8: activated nodes: 11438, borderline nodes: 259, overall activation: 4285.766, activation diff: 688.409, ratio: 0.161
#   pulse 9: activated nodes: 11443, borderline nodes: 142, overall activation: 4848.533, activation diff: 562.767, ratio: 0.116
#   pulse 10: activated nodes: 11447, borderline nodes: 101, overall activation: 5301.021, activation diff: 452.488, ratio: 0.085
#   pulse 11: activated nodes: 11449, borderline nodes: 71, overall activation: 5661.275, activation diff: 360.254, ratio: 0.064
#   pulse 12: activated nodes: 11449, borderline nodes: 58, overall activation: 5946.270, activation diff: 284.995, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 5946.3
#   number of spread. activ. pulses: 12
#   running time: 1538

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885762   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9842254   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9819441   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98031366   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97067416   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95085645   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7643755   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76341313   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7626655   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.76258236   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.76157755   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.76124656   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.76124185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7609392   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.76083803   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.76081663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.76076096   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.76065755   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7604954   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7604789   REFERENCES:SIMLOC
