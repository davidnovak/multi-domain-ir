###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1628.077, activation diff: 1654.677, ratio: 1.016
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 971.489, activation diff: 1790.790, ratio: 1.843
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 3196.462, activation diff: 2272.414, ratio: 0.711
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 3305.019, activation diff: 119.109, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3305.0
#   number of spread. activ. pulses: 5
#   running time: 1210

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.999999   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996656   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99783695   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99496514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.5   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   14   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.5   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   20   0.49999997   REFERENCES:SIMLOC
