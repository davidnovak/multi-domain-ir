###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1662.341, activation diff: 1654.778, ratio: 0.995
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2852.383, activation diff: 1252.667, ratio: 0.439
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5720.217, activation diff: 2867.834, ratio: 0.501
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6809.283, activation diff: 1089.066, ratio: 0.160
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7170.089, activation diff: 360.806, ratio: 0.050
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 7286.061, activation diff: 115.972, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7286.1
#   number of spread. activ. pulses: 7
#   running time: 1336

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998145   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997777   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996884   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99967873   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9975946   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99393445   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7998009   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79978794   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.7997735   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7997699   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7997656   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7997649   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7997607   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7997546   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7997527   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7997509   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.79974484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.79974306   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   19   0.79973805   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.7997371   REFERENCES:SIMLOC
