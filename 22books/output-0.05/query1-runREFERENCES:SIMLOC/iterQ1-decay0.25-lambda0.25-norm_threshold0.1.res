###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1374.788, activation diff: 1370.962, ratio: 0.997
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2078.201, activation diff: 930.291, ratio: 0.448
#   pulse 4: activated nodes: 11359, borderline nodes: 2583, overall activation: 4772.926, activation diff: 2695.270, ratio: 0.565
#   pulse 5: activated nodes: 11436, borderline nodes: 253, overall activation: 5865.490, activation diff: 1092.564, ratio: 0.186
#   pulse 6: activated nodes: 11445, borderline nodes: 86, overall activation: 6236.496, activation diff: 371.006, ratio: 0.059
#   pulse 7: activated nodes: 11447, borderline nodes: 62, overall activation: 6357.864, activation diff: 121.368, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6357.9
#   number of spread. activ. pulses: 7
#   running time: 1331

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997371   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996183   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9995358   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99726903   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931309   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7498038   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7497801   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.74974334   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.74973524   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.74973404   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.7497335   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.74973166   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.74971116   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7497036   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7497002   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.7496991   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.74969435   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7496915   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.74968493   REFERENCES:SIMLOC
