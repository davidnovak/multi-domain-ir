###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 553.700, activation diff: 537.636, ratio: 0.971
#   pulse 3: activated nodes: 9185, borderline nodes: 4938, overall activation: 1147.727, activation diff: 594.027, ratio: 0.518
#   pulse 4: activated nodes: 11253, borderline nodes: 4958, overall activation: 2817.892, activation diff: 1670.165, ratio: 0.593
#   pulse 5: activated nodes: 11409, borderline nodes: 768, overall activation: 4196.436, activation diff: 1378.545, ratio: 0.329
#   pulse 6: activated nodes: 11425, borderline nodes: 304, overall activation: 5071.430, activation diff: 874.993, ratio: 0.173
#   pulse 7: activated nodes: 11437, borderline nodes: 156, overall activation: 5584.066, activation diff: 512.636, ratio: 0.092
#   pulse 8: activated nodes: 11438, borderline nodes: 107, overall activation: 5875.598, activation diff: 291.532, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5875.6
#   number of spread. activ. pulses: 8
#   running time: 1310

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960693   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9955243   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9946981   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99459565   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9903077   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98019755   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7440441   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.743927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7437664   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7437271   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.74352694   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.74348974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74348545   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7434217   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.74338293   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.74336016   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7433555   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.74334407   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7433289   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.74328786   REFERENCES:SIMLOC
