###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 692.162, activation diff: 694.829, ratio: 1.004
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 697.271, activation diff: 376.306, ratio: 0.540
#   pulse 4: activated nodes: 10659, borderline nodes: 5610, overall activation: 2059.261, activation diff: 1363.085, ratio: 0.662
#   pulse 5: activated nodes: 10861, borderline nodes: 3683, overall activation: 2768.943, activation diff: 709.682, ratio: 0.256
#   pulse 6: activated nodes: 10887, borderline nodes: 3556, overall activation: 3057.368, activation diff: 288.425, ratio: 0.094
#   pulse 7: activated nodes: 10892, borderline nodes: 3527, overall activation: 3162.501, activation diff: 105.133, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10892
#   final overall activation: 3162.5
#   number of spread. activ. pulses: 7
#   running time: 1305

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99950373   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99929386   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99883974   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99639404   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.991076   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49980307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49976844   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.4996888   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.4996586   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49965605   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.49965277   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.49965104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.49964228   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4996344   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.49962905   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.49961343   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.49960893   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.4996009   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.49960065   REFERENCES:SIMLOC
