###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 173.501, activation diff: 161.435, ratio: 0.930
#   pulse 3: activated nodes: 9338, borderline nodes: 3902, overall activation: 466.893, activation diff: 293.419, ratio: 0.628
#   pulse 4: activated nodes: 11248, borderline nodes: 4209, overall activation: 1128.371, activation diff: 661.478, ratio: 0.586
#   pulse 5: activated nodes: 11395, borderline nodes: 1163, overall activation: 1955.747, activation diff: 827.376, ratio: 0.423
#   pulse 6: activated nodes: 11419, borderline nodes: 461, overall activation: 2773.359, activation diff: 817.612, ratio: 0.295
#   pulse 7: activated nodes: 11434, borderline nodes: 254, overall activation: 3488.776, activation diff: 715.417, ratio: 0.205
#   pulse 8: activated nodes: 11439, borderline nodes: 149, overall activation: 4082.655, activation diff: 593.879, ratio: 0.145
#   pulse 9: activated nodes: 11440, borderline nodes: 100, overall activation: 4563.483, activation diff: 480.828, ratio: 0.105
#   pulse 10: activated nodes: 11440, borderline nodes: 86, overall activation: 4947.495, activation diff: 384.012, ratio: 0.078
#   pulse 11: activated nodes: 11441, borderline nodes: 83, overall activation: 5251.663, activation diff: 304.168, ratio: 0.058
#   pulse 12: activated nodes: 11442, borderline nodes: 75, overall activation: 5491.288, activation diff: 239.626, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5491.3
#   number of spread. activ. pulses: 12
#   running time: 1510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9852847   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9835995   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98184997   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9728543   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9560381   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7170304   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.71618694   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7156933   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.715688   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7149481   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7144437   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7144298   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7143777   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.71420187   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7141551   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7140661   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   18   0.7140167   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.71390486   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.71389437   REFERENCES:SIMLOC
