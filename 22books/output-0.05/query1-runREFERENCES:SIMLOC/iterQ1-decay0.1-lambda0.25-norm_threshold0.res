###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1867.547, activation diff: 1859.984, ratio: 0.996
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 3360.300, activation diff: 1559.672, ratio: 0.464
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 6876.528, activation diff: 3516.228, ratio: 0.511
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 8198.275, activation diff: 1321.748, ratio: 0.161
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 8633.759, activation diff: 435.484, ratio: 0.050
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8772.906, activation diff: 139.146, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8772.9
#   number of spread. activ. pulses: 7
#   running time: 1293

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998145   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997777   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99968845   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99967873   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99759465   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99393445   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89977616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89976215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.89974713   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8997421   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.89973867   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.89973676   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8997325   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.89972925   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.8997232   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.899722   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.89971876   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.8997146   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.89971215   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   20   0.8997098   REFERENCES:SIMLOC
