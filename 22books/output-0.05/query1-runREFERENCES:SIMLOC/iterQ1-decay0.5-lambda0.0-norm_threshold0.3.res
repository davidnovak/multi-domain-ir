###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1165.293, activation diff: 1190.190, ratio: 1.021
#   pulse 3: activated nodes: 9395, borderline nodes: 3931, overall activation: 352.009, activation diff: 1494.773, ratio: 4.246
#   pulse 4: activated nodes: 10768, borderline nodes: 4223, overall activation: 2757.826, activation diff: 2665.682, ratio: 0.967
#   pulse 5: activated nodes: 10881, borderline nodes: 3606, overall activation: 2655.358, activation diff: 789.477, ratio: 0.297
#   pulse 6: activated nodes: 10890, borderline nodes: 3538, overall activation: 3186.736, activation diff: 537.301, ratio: 0.169
#   pulse 7: activated nodes: 10895, borderline nodes: 3524, overall activation: 3212.986, activation diff: 27.230, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3213.0
#   number of spread. activ. pulses: 7
#   running time: 1324

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995494   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99754006   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.5   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   14   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.5   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   20   0.49999997   REFERENCES:SIMLOC
