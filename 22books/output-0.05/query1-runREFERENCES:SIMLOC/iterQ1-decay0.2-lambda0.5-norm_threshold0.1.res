###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 714.994, activation diff: 696.679, ratio: 0.974
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 1545.182, activation diff: 830.187, ratio: 0.537
#   pulse 4: activated nodes: 11317, borderline nodes: 3033, overall activation: 3524.317, activation diff: 1979.135, ratio: 0.562
#   pulse 5: activated nodes: 11426, borderline nodes: 374, overall activation: 5019.463, activation diff: 1495.147, ratio: 0.298
#   pulse 6: activated nodes: 11447, borderline nodes: 93, overall activation: 5933.312, activation diff: 913.848, ratio: 0.154
#   pulse 7: activated nodes: 11450, borderline nodes: 46, overall activation: 6459.583, activation diff: 526.271, ratio: 0.081
#   pulse 8: activated nodes: 11452, borderline nodes: 33, overall activation: 6755.740, activation diff: 296.157, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6755.7
#   number of spread. activ. pulses: 8
#   running time: 1309

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957228   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9952332   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99500245   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9911347   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9825596   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79369414   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7935989   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7935004   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.793491   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7933372   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7932879   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.7932659   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.79325247   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.79318964   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7931716   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7931596   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.793152   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.79313374   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.7931336   REFERENCES:SIMLOC
