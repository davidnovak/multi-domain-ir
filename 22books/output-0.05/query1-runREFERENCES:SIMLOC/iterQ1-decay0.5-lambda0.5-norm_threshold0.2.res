###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 375.465, activation diff: 359.400, ratio: 0.957
#   pulse 3: activated nodes: 9185, borderline nodes: 4938, overall activation: 694.025, activation diff: 318.561, ratio: 0.459
#   pulse 4: activated nodes: 10664, borderline nodes: 5535, overall activation: 1460.809, activation diff: 766.784, ratio: 0.525
#   pulse 5: activated nodes: 10860, borderline nodes: 3675, overall activation: 2136.867, activation diff: 676.058, ratio: 0.316
#   pulse 6: activated nodes: 10887, borderline nodes: 3555, overall activation: 2603.376, activation diff: 466.509, ratio: 0.179
#   pulse 7: activated nodes: 10891, borderline nodes: 3526, overall activation: 2886.205, activation diff: 282.830, ratio: 0.098
#   pulse 8: activated nodes: 10895, borderline nodes: 3519, overall activation: 3050.120, activation diff: 163.915, ratio: 0.054
#   pulse 9: activated nodes: 10895, borderline nodes: 3518, overall activation: 3143.301, activation diff: 93.181, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3143.3
#   number of spread. activ. pulses: 9
#   running time: 1356

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99803466   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99776125   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99734855   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99727154   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9939438   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98679274   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4980145   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49797502   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.49792194   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.49790898   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.49784005   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49782947   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.49782795   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.497805   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49779195   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.49777853   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49777627   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.49777478   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.49777228   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.49775803   REFERENCES:SIMLOC
