###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 798.264, activation diff: 778.400, ratio: 0.975
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1749.420, activation diff: 951.156, ratio: 0.544
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3564.923, activation diff: 1815.503, ratio: 0.509
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4853.060, activation diff: 1288.137, ratio: 0.265
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 5625.080, activation diff: 772.021, ratio: 0.137
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6065.456, activation diff: 440.376, ratio: 0.073
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 6311.627, activation diff: 246.171, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6311.6
#   number of spread. activ. pulses: 8
#   running time: 1356

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996089   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9958461   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9955529   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99528515   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99181837   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9845089   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7441115   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7440454   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7439973   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.74399275   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7439074   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7438524   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7438315   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7438101   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.7437831   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7437773   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.743768   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7437432   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7437301   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.743725   REFERENCES:SIMLOC
