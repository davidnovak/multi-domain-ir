###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 150.348, activation diff: 138.974, ratio: 0.924
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 438.028, activation diff: 287.730, ratio: 0.657
#   pulse 4: activated nodes: 11125, borderline nodes: 6013, overall activation: 1236.489, activation diff: 798.461, ratio: 0.646
#   pulse 5: activated nodes: 11333, borderline nodes: 2215, overall activation: 2326.598, activation diff: 1090.109, ratio: 0.469
#   pulse 6: activated nodes: 11412, borderline nodes: 855, overall activation: 3450.828, activation diff: 1124.230, ratio: 0.326
#   pulse 7: activated nodes: 11434, borderline nodes: 322, overall activation: 4453.716, activation diff: 1002.888, ratio: 0.225
#   pulse 8: activated nodes: 11442, borderline nodes: 152, overall activation: 5293.964, activation diff: 840.248, ratio: 0.159
#   pulse 9: activated nodes: 11448, borderline nodes: 86, overall activation: 5978.391, activation diff: 684.427, ratio: 0.114
#   pulse 10: activated nodes: 11449, borderline nodes: 55, overall activation: 6527.539, activation diff: 549.148, ratio: 0.084
#   pulse 11: activated nodes: 11449, borderline nodes: 49, overall activation: 6964.089, activation diff: 436.550, ratio: 0.063
#   pulse 12: activated nodes: 11451, borderline nodes: 40, overall activation: 7309.040, activation diff: 344.951, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 7309.0
#   number of spread. activ. pulses: 12
#   running time: 1526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885762   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9842317   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9819593   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9803361   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9707369   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95113385   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8599225   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8588412   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8580055   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8579099   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8567834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8564193   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.8564092   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.8560736   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8559567   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.85592854   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.85588086   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.8557777   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8555685   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.8555577   REFERENCES:SIMLOC
