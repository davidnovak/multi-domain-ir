###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 204.873, activation diff: 192.808, ratio: 0.941
#   pulse 3: activated nodes: 9338, borderline nodes: 3902, overall activation: 575.819, activation diff: 370.973, ratio: 0.644
#   pulse 4: activated nodes: 11263, borderline nodes: 4087, overall activation: 1514.576, activation diff: 938.757, ratio: 0.620
#   pulse 5: activated nodes: 11405, borderline nodes: 982, overall activation: 2692.374, activation diff: 1177.798, ratio: 0.437
#   pulse 6: activated nodes: 11434, borderline nodes: 327, overall activation: 3828.681, activation diff: 1136.307, ratio: 0.297
#   pulse 7: activated nodes: 11445, borderline nodes: 118, overall activation: 4808.313, activation diff: 979.632, ratio: 0.204
#   pulse 8: activated nodes: 11449, borderline nodes: 58, overall activation: 5615.534, activation diff: 807.221, ratio: 0.144
#   pulse 9: activated nodes: 11450, borderline nodes: 44, overall activation: 6266.541, activation diff: 651.007, ratio: 0.104
#   pulse 10: activated nodes: 11452, borderline nodes: 25, overall activation: 6785.271, activation diff: 518.730, ratio: 0.076
#   pulse 11: activated nodes: 11453, borderline nodes: 17, overall activation: 7195.472, activation diff: 410.201, ratio: 0.057
#   pulse 12: activated nodes: 11454, borderline nodes: 13, overall activation: 7518.195, activation diff: 322.723, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7518.2
#   number of spread. activ. pulses: 12
#   running time: 1550

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885006   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9852902   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98360866   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98187244   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9729279   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.956329   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.86043704   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8594272   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.85883963   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8588306   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8579471   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.85735524   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.85732996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.85727346   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8570534   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.8570044   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.8569106   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   18   0.85682404   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.85673577   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.8567059   REFERENCES:SIMLOC
