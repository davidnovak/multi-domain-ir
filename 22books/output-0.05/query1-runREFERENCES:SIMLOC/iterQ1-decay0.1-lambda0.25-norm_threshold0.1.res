###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1646.172, activation diff: 1642.346, ratio: 0.998
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2683.288, activation diff: 1299.971, ratio: 0.484
#   pulse 4: activated nodes: 11364, borderline nodes: 2508, overall activation: 6444.230, activation diff: 3761.738, ratio: 0.584
#   pulse 5: activated nodes: 11437, borderline nodes: 208, overall activation: 7941.289, activation diff: 1497.060, ratio: 0.189
#   pulse 6: activated nodes: 11451, borderline nodes: 34, overall activation: 8448.466, activation diff: 507.177, ratio: 0.060
#   pulse 7: activated nodes: 11454, borderline nodes: 9, overall activation: 8613.890, activation diff: 165.424, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8613.9
#   number of spread. activ. pulses: 7
#   running time: 1317

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997371   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996183   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9995358   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972693   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.993131   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997653   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89973783   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.8996965   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.8996883   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.8996856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.8996809   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.8996797   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.89965725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8996538   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.8996451   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.8996433   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.8996415   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   19   0.8996376   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.8996356   REFERENCES:SIMLOC
