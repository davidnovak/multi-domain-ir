###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1197.496, activation diff: 1197.493, ratio: 1.000
#   pulse 3: activated nodes: 9457, borderline nodes: 3914, overall activation: 1637.151, activation diff: 850.586, ratio: 0.520
#   pulse 4: activated nodes: 11306, borderline nodes: 3236, overall activation: 4414.542, activation diff: 2781.499, ratio: 0.630
#   pulse 5: activated nodes: 11421, borderline nodes: 470, overall activation: 5632.387, activation diff: 1217.845, ratio: 0.216
#   pulse 6: activated nodes: 11437, borderline nodes: 159, overall activation: 6063.343, activation diff: 430.955, ratio: 0.071
#   pulse 7: activated nodes: 11440, borderline nodes: 89, overall activation: 6208.499, activation diff: 145.156, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6208.5
#   number of spread. activ. pulses: 7
#   running time: 1310

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99965113   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9994967   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9992517   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99687517   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9921905   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7497747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74973565   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.74966276   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7496455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.7496428   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.74963516   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.7496166   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.7496152   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.74961126   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.7496092   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.74958885   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7495835   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7495736   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7495669   REFERENCES:SIMLOC
