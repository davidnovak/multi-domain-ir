###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 97.169, activation diff: 86.551, ratio: 0.891
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 287.421, activation diff: 190.329, ratio: 0.662
#   pulse 4: activated nodes: 10913, borderline nodes: 6871, overall activation: 825.181, activation diff: 537.760, ratio: 0.652
#   pulse 5: activated nodes: 11229, borderline nodes: 3960, overall activation: 1587.446, activation diff: 762.265, ratio: 0.480
#   pulse 6: activated nodes: 11344, borderline nodes: 2003, overall activation: 2452.174, activation diff: 864.728, ratio: 0.353
#   pulse 7: activated nodes: 11410, borderline nodes: 934, overall activation: 3277.018, activation diff: 824.845, ratio: 0.252
#   pulse 8: activated nodes: 11422, borderline nodes: 487, overall activation: 3991.803, activation diff: 714.784, ratio: 0.179
#   pulse 9: activated nodes: 11432, borderline nodes: 292, overall activation: 4584.561, activation diff: 592.759, ratio: 0.129
#   pulse 10: activated nodes: 11438, borderline nodes: 190, overall activation: 5065.582, activation diff: 481.021, ratio: 0.095
#   pulse 11: activated nodes: 11443, borderline nodes: 143, overall activation: 5451.073, activation diff: 385.490, ratio: 0.071
#   pulse 12: activated nodes: 11445, borderline nodes: 114, overall activation: 5757.629, activation diff: 306.557, ratio: 0.053
#   pulse 13: activated nodes: 11447, borderline nodes: 95, overall activation: 6000.142, activation diff: 242.513, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6000.1
#   number of spread. activ. pulses: 13
#   running time: 1545

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111557   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9871333   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98477703   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9837836   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9752686   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.955961   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.772811   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.77207124   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.771275   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.77112055   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7701786   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.77017784   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7701588   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.76977515   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.769721   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7697166   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.76968974   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.76967466   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.76948696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.76944685   REFERENCES:SIMLOC
