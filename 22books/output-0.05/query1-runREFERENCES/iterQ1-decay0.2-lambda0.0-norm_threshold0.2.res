###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2077.230, activation diff: 2106.395, ratio: 1.014
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 983.843, activation diff: 3061.073, ratio: 3.111
#   pulse 4: activated nodes: 10798, borderline nodes: 2462, overall activation: 4190.385, activation diff: 5174.228, ratio: 1.235
#   pulse 5: activated nodes: 11261, borderline nodes: 827, overall activation: 1362.312, activation diff: 5552.696, ratio: 4.076
#   pulse 6: activated nodes: 11316, borderline nodes: 246, overall activation: 4236.495, activation diff: 5598.807, ratio: 1.322
#   pulse 7: activated nodes: 11320, borderline nodes: 197, overall activation: 1372.111, activation diff: 5608.606, ratio: 4.088
#   pulse 8: activated nodes: 11321, borderline nodes: 181, overall activation: 4238.925, activation diff: 5611.036, ratio: 1.324
#   pulse 9: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.798, activation diff: 5611.724, ratio: 4.088
#   pulse 10: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.159, activation diff: 5611.957, ratio: 1.324
#   pulse 11: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.883, activation diff: 5612.042, ratio: 4.088
#   pulse 12: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.206, activation diff: 5612.089, ratio: 1.324
#   pulse 13: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.899, activation diff: 5612.105, ratio: 4.088
#   pulse 14: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.220, activation diff: 5612.119, ratio: 1.324
#   pulse 15: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.904, activation diff: 5612.123, ratio: 4.088

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11322
#   final overall activation: 1372.9
#   number of spread. activ. pulses: 15
#   running time: 577

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
