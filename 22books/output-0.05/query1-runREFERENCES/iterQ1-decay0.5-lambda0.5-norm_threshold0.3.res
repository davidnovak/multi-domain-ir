###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 309.836, activation diff: 296.978, ratio: 0.959
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 498.873, activation diff: 191.103, ratio: 0.383
#   pulse 4: activated nodes: 8333, borderline nodes: 4229, overall activation: 951.134, activation diff: 452.260, ratio: 0.475
#   pulse 5: activated nodes: 8654, borderline nodes: 3405, overall activation: 1300.896, activation diff: 349.762, ratio: 0.269
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1526.391, activation diff: 225.495, ratio: 0.148
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1662.151, activation diff: 135.760, ratio: 0.082
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1741.032, activation diff: 78.881, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1741.0
#   number of spread. activ. pulses: 8
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99602765   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99505365   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9937381   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9936075   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.98917925   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9760215   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4959473   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.4958548   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49567753   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49558502   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.49541566   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.49541566   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.49528757   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.4952823   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.49527824   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.4952764   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.49521425   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.49518684   REFERENCES
1   Q1   politicalsketche00retsrich_154   19   0.4951836   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.49517542   REFERENCES
