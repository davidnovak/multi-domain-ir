###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1418.802, activation diff: 1422.122, ratio: 1.002
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1878.169, activation diff: 1146.343, ratio: 0.610
#   pulse 4: activated nodes: 10791, borderline nodes: 2777, overall activation: 4778.527, activation diff: 2977.526, ratio: 0.623
#   pulse 5: activated nodes: 11249, borderline nodes: 1166, overall activation: 6305.924, activation diff: 1528.274, ratio: 0.242
#   pulse 6: activated nodes: 11314, borderline nodes: 234, overall activation: 6918.858, activation diff: 612.934, ratio: 0.089
#   pulse 7: activated nodes: 11323, borderline nodes: 125, overall activation: 7143.575, activation diff: 224.717, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 7143.6
#   number of spread. activ. pulses: 7
#   running time: 448

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99951136   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992298   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9991933   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99683344   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9918926   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8996608   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89963055   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8995378   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.89947367   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.89947367   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.8994545   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8994454   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.89942473   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.8994173   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.8993922   REFERENCES
1   Q1   politicalsketche00retsrich_154   17   0.89938563   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.89938223   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.89938223   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.8993516   REFERENCES
