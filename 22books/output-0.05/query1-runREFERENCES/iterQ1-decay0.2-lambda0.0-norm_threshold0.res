###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2480.692, activation diff: 2514.422, ratio: 1.014
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1189.886, activation diff: 3670.578, ratio: 3.085
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4322.444, activation diff: 5512.331, ratio: 1.275
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1478.518, activation diff: 5800.962, ratio: 3.923
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4353.623, activation diff: 5832.142, ratio: 1.340
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 1484.309, activation diff: 5837.932, ratio: 3.933
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4354.965, activation diff: 5839.274, ratio: 1.341
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 1484.664, activation diff: 5839.629, ratio: 3.933
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 4355.106, activation diff: 5839.770, ratio: 1.341
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 1484.702, activation diff: 5839.808, ratio: 3.933
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 4355.129, activation diff: 5839.831, ratio: 1.341
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 1484.707, activation diff: 5839.836, ratio: 3.933
#   pulse 14: activated nodes: 11335, borderline nodes: 30, overall activation: 4355.133, activation diff: 5839.841, ratio: 1.341
#   pulse 15: activated nodes: 11335, borderline nodes: 30, overall activation: 1484.708, activation diff: 5839.842, ratio: 3.933

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 1484.7
#   number of spread. activ. pulses: 15
#   running time: 674

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
