###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1229.480, activation diff: 1234.685, ratio: 1.004
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1524.553, activation diff: 1096.296, ratio: 0.719
#   pulse 4: activated nodes: 10444, borderline nodes: 4053, overall activation: 4459.067, activation diff: 3062.353, ratio: 0.687
#   pulse 5: activated nodes: 11218, borderline nodes: 1664, overall activation: 6046.711, activation diff: 1589.078, ratio: 0.263
#   pulse 6: activated nodes: 11293, borderline nodes: 379, overall activation: 6710.965, activation diff: 664.254, ratio: 0.099
#   pulse 7: activated nodes: 11316, borderline nodes: 194, overall activation: 6959.057, activation diff: 248.092, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 6959.1
#   number of spread. activ. pulses: 7
#   running time: 443

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999303   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99895203   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9988157   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99635774   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9908133   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8995335   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89950913   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89935255   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.8992964   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.8992964   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.89926636   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.89922947   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.8992211   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.8992003   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.89919555   REFERENCES
1   Q1   politicalsketche00retsrich_154   17   0.899183   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.8991676   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   19   0.8991641   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.899164   REFERENCES
