###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1792.323, activation diff: 1788.118, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2753.767, activation diff: 1205.695, ratio: 0.438
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 5492.717, activation diff: 2738.950, ratio: 0.499
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 6803.873, activation diff: 1311.155, ratio: 0.193
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 7296.646, activation diff: 492.773, ratio: 0.068
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 7470.322, activation diff: 173.677, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7470.3
#   number of spread. activ. pulses: 7
#   running time: 449

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9997392   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99966073   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9995827   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756944   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9937292   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.89976275   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89974177   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8997246   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8997132   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.89967775   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.89967775   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.89967674   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.89966166   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.8996574   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.8996558   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.89964795   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.89964795   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   19   0.8996477   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.8996431   REFERENCES
