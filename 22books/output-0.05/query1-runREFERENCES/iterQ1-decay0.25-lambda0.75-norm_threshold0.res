###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 227.877, activation diff: 215.174, ratio: 0.944
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 567.188, activation diff: 339.321, ratio: 0.598
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1166.659, activation diff: 599.471, ratio: 0.514
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1807.163, activation diff: 640.504, ratio: 0.354
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2397.056, activation diff: 589.893, ratio: 0.246
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2906.371, activation diff: 509.316, ratio: 0.175
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3331.831, activation diff: 425.460, ratio: 0.128
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 3680.488, activation diff: 348.657, ratio: 0.095
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3962.738, activation diff: 282.250, ratio: 0.071
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 4189.340, activation diff: 226.602, ratio: 0.054
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4370.189, activation diff: 180.849, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4370.2
#   number of spread. activ. pulses: 12
#   running time: 557

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.986049   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98484313   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9829073   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9747592   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9598386   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.717327   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7165569   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7162534   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.7162243   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7156528   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   12   0.71506   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.7150344   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7150103   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.71493334   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.714931   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7147769   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.71475047   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   19   0.7145811   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7145719   REFERENCES
