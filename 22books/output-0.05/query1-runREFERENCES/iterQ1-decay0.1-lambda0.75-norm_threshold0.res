###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 269.938, activation diff: 257.234, ratio: 0.953
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 714.197, activation diff: 444.271, ratio: 0.622
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1604.710, activation diff: 890.512, ratio: 0.555
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2576.005, activation diff: 971.296, ratio: 0.377
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3476.212, activation diff: 900.207, ratio: 0.259
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4255.925, activation diff: 779.713, ratio: 0.183
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 4908.775, activation diff: 652.851, ratio: 0.133
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 5444.841, activation diff: 536.065, ratio: 0.098
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 5879.607, activation diff: 434.766, ratio: 0.074
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 6229.291, activation diff: 349.684, ratio: 0.056
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 6508.893, activation diff: 279.602, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6508.9
#   number of spread. activ. pulses: 12
#   running time: 525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9860529   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98484695   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9829289   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97481596   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96009564   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8607925   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.85987043   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.8595109   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.85947835   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.85879636   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   12   0.85809267   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.8580656   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.85804796   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.8579372   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.8579205   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.85776293   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.85772824   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   19   0.8575336   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.8575115   REFERENCES
