###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1346.742, activation diff: 1346.294, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1704.211, activation diff: 754.064, ratio: 0.442
#   pulse 4: activated nodes: 10802, borderline nodes: 2442, overall activation: 3573.393, activation diff: 1883.403, ratio: 0.527
#   pulse 5: activated nodes: 11264, borderline nodes: 817, overall activation: 4450.453, activation diff: 877.064, ratio: 0.197
#   pulse 6: activated nodes: 11298, borderline nodes: 543, overall activation: 4777.267, activation diff: 326.815, ratio: 0.068
#   pulse 7: activated nodes: 11303, borderline nodes: 484, overall activation: 4891.619, activation diff: 114.351, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 4891.6
#   number of spread. activ. pulses: 7
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99966294   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99950236   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999448   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972359   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9928795   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.749779   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74975395   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7497202   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.74969345   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.7496693   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7496692   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7496474   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.74964577   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.74963844   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.7496342   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.74962544   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.7496252   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   19   0.7496212   REFERENCES
1   Q1   europesincenapol00leveuoft_17   20   0.74961025   REFERENCES
