###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2326.021, activation diff: 2359.750, ratio: 1.015
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 977.671, activation diff: 3303.692, ratio: 3.379
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3829.827, activation diff: 4807.499, ratio: 1.255
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1190.457, activation diff: 5020.284, ratio: 4.217
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 3850.257, activation diff: 5040.714, ratio: 1.309
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 1194.399, activation diff: 5044.657, ratio: 4.224
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3851.188, activation diff: 5045.587, ratio: 1.310
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1194.600, activation diff: 5045.788, ratio: 4.224
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3851.249, activation diff: 5045.849, ratio: 1.310
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1194.618, activation diff: 5045.866, ratio: 4.224
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 3851.256, activation diff: 5045.874, ratio: 1.310
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1194.620, activation diff: 5045.876, ratio: 4.224
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 3851.257, activation diff: 5045.877, ratio: 1.310
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1194.620, activation diff: 5045.877, ratio: 4.224

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 1194.6
#   number of spread. activ. pulses: 15
#   running time: 650

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
