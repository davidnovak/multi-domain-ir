###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 203.772, activation diff: 191.707, ratio: 0.941
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 558.279, activation diff: 354.538, ratio: 0.635
#   pulse 4: activated nodes: 10740, borderline nodes: 3935, overall activation: 1368.568, activation diff: 810.290, ratio: 0.592
#   pulse 5: activated nodes: 11196, borderline nodes: 1892, overall activation: 2300.459, activation diff: 931.890, ratio: 0.405
#   pulse 6: activated nodes: 11239, borderline nodes: 744, overall activation: 3190.412, activation diff: 889.953, ratio: 0.279
#   pulse 7: activated nodes: 11308, borderline nodes: 401, overall activation: 3974.965, activation diff: 784.553, ratio: 0.197
#   pulse 8: activated nodes: 11316, borderline nodes: 217, overall activation: 4639.653, activation diff: 664.688, ratio: 0.143
#   pulse 9: activated nodes: 11321, borderline nodes: 141, overall activation: 5190.323, activation diff: 550.670, ratio: 0.106
#   pulse 10: activated nodes: 11330, borderline nodes: 86, overall activation: 5640.171, activation diff: 449.849, ratio: 0.080
#   pulse 11: activated nodes: 11332, borderline nodes: 49, overall activation: 6004.193, activation diff: 364.022, ratio: 0.061
#   pulse 12: activated nodes: 11332, borderline nodes: 41, overall activation: 6296.786, activation diff: 292.593, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6296.8
#   number of spread. activ. pulses: 12
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98884827   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98521256   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9835883   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98164   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97290677   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95571804   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8604126   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.859406   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8588326   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.85881007   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.85790396   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.8573048   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.8572823   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.8571389   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.8570366   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.8569089   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.8568797   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   18   0.8568094   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.8567055   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.8566246   REFERENCES
