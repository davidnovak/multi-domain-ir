###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1845.500, activation diff: 1872.232, ratio: 1.014
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 880.933, activation diff: 2726.434, ratio: 3.095
#   pulse 4: activated nodes: 10788, borderline nodes: 2917, overall activation: 4115.835, activation diff: 4996.768, ratio: 1.214
#   pulse 5: activated nodes: 11245, borderline nodes: 1315, overall activation: 1301.508, activation diff: 5417.343, ratio: 4.162
#   pulse 6: activated nodes: 11309, borderline nodes: 378, overall activation: 4172.945, activation diff: 5474.453, ratio: 1.312
#   pulse 7: activated nodes: 11317, borderline nodes: 307, overall activation: 1313.751, activation diff: 5486.696, ratio: 4.176
#   pulse 8: activated nodes: 11317, borderline nodes: 272, overall activation: 4176.243, activation diff: 5489.994, ratio: 1.315
#   pulse 9: activated nodes: 11319, borderline nodes: 271, overall activation: 1314.640, activation diff: 5490.883, ratio: 4.177
#   pulse 10: activated nodes: 11319, borderline nodes: 271, overall activation: 4176.507, activation diff: 5491.147, ratio: 1.315
#   pulse 11: activated nodes: 11319, borderline nodes: 270, overall activation: 1314.747, activation diff: 5491.254, ratio: 4.177
#   pulse 12: activated nodes: 11319, borderline nodes: 270, overall activation: 4176.545, activation diff: 5491.292, ratio: 1.315
#   pulse 13: activated nodes: 11319, borderline nodes: 270, overall activation: 1314.764, activation diff: 5491.310, ratio: 4.177
#   pulse 14: activated nodes: 11319, borderline nodes: 270, overall activation: 4176.553, activation diff: 5491.317, ratio: 1.315
#   pulse 15: activated nodes: 11319, borderline nodes: 270, overall activation: 1314.768, activation diff: 5491.321, ratio: 4.177

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 1314.8
#   number of spread. activ. pulses: 15
#   running time: 539

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
