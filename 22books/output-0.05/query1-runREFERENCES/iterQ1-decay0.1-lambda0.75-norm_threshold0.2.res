###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 150.104, activation diff: 138.730, ratio: 0.924
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 429.362, activation diff: 279.313, ratio: 0.651
#   pulse 4: activated nodes: 10315, borderline nodes: 5331, overall activation: 1154.089, activation diff: 724.727, ratio: 0.628
#   pulse 5: activated nodes: 11050, borderline nodes: 3202, overall activation: 2036.757, activation diff: 882.668, ratio: 0.433
#   pulse 6: activated nodes: 11212, borderline nodes: 1591, overall activation: 2908.555, activation diff: 871.798, ratio: 0.300
#   pulse 7: activated nodes: 11238, borderline nodes: 766, overall activation: 3692.670, activation diff: 784.115, ratio: 0.212
#   pulse 8: activated nodes: 11300, borderline nodes: 511, overall activation: 4365.755, activation diff: 673.086, ratio: 0.154
#   pulse 9: activated nodes: 11312, borderline nodes: 362, overall activation: 4928.551, activation diff: 562.796, ratio: 0.114
#   pulse 10: activated nodes: 11315, borderline nodes: 266, overall activation: 5391.578, activation diff: 463.027, ratio: 0.086
#   pulse 11: activated nodes: 11318, borderline nodes: 185, overall activation: 5768.544, activation diff: 376.966, ratio: 0.065
#   pulse 12: activated nodes: 11321, borderline nodes: 147, overall activation: 6073.113, activation diff: 304.569, ratio: 0.050
#   pulse 13: activated nodes: 11324, borderline nodes: 131, overall activation: 6317.789, activation diff: 244.676, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 6317.8
#   number of spread. activ. pulses: 13
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99142987   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9880976   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98644936   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9850259   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9773018   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96075714   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8699188   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.8691169   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.86849713   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8684113   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.8675469   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.8672832   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.8672614   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.8669387   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.86693394   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.8668947   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.86688244   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   18   0.86680615   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.8666427   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.8666316   REFERENCES
