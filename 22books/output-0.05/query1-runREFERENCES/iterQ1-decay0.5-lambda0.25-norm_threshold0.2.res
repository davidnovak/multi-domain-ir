###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 794.105, activation diff: 797.425, ratio: 1.004
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 646.639, activation diff: 234.308, ratio: 0.362
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1439.333, activation diff: 792.705, ratio: 0.551
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1751.053, activation diff: 311.720, ratio: 0.178
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1854.438, activation diff: 103.385, ratio: 0.056
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1886.483, activation diff: 32.045, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1886.5
#   number of spread. activ. pulses: 7
#   running time: 408

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995111   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99922645   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9991933   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99682134   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99184257   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4998116   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49979427   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49974307   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.4996987   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.4996987   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.49969685   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4996715   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.49967146   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.49966842   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   16   0.49964783   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.49964783   REFERENCES
1   Q1   politicalsketche00retsrich_154   18   0.499646   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   19   0.49963874   REFERENCES
1   Q1   europesincenapol00leveuoft_17   20   0.49962917   REFERENCES
