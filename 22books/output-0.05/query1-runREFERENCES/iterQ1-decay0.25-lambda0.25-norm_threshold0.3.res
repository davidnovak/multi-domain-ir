###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1026.455, activation diff: 1031.660, ratio: 1.005
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1101.357, activation diff: 742.713, ratio: 0.674
#   pulse 4: activated nodes: 10442, borderline nodes: 4222, overall activation: 3108.866, activation diff: 2080.353, ratio: 0.669
#   pulse 5: activated nodes: 11203, borderline nodes: 1910, overall activation: 4103.672, activation diff: 994.942, ratio: 0.242
#   pulse 6: activated nodes: 11228, borderline nodes: 775, overall activation: 4506.121, activation diff: 402.449, ratio: 0.089
#   pulse 7: activated nodes: 11257, borderline nodes: 624, overall activation: 4653.186, activation diff: 147.065, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11257
#   final overall activation: 4653.2
#   number of spread. activ. pulses: 7
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993029   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9989512   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9988157   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9963555   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9907859   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7496113   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.749591   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.74946046   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   10   0.7494137   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7494136   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.7493879   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.74935794   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.74934864   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.7493337   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.7493297   REFERENCES
1   Q1   politicalsketche00retsrich_154   17   0.7493131   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.7493034   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7493031   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.7492683   REFERENCES
