###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1947.773, activation diff: 1976.938, ratio: 1.015
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 805.716, activation diff: 2753.489, ratio: 3.417
#   pulse 4: activated nodes: 10798, borderline nodes: 2505, overall activation: 3715.083, activation diff: 4520.799, ratio: 1.217
#   pulse 5: activated nodes: 11260, borderline nodes: 884, overall activation: 1096.249, activation diff: 4811.331, ratio: 4.389
#   pulse 6: activated nodes: 11298, borderline nodes: 551, overall activation: 3747.468, activation diff: 4843.716, ratio: 1.293
#   pulse 7: activated nodes: 11303, borderline nodes: 497, overall activation: 1103.638, activation diff: 4851.106, ratio: 4.396
#   pulse 8: activated nodes: 11303, borderline nodes: 497, overall activation: 3749.442, activation diff: 4853.080, ratio: 1.294
#   pulse 9: activated nodes: 11303, borderline nodes: 495, overall activation: 1104.105, activation diff: 4853.547, ratio: 4.396
#   pulse 10: activated nodes: 11303, borderline nodes: 495, overall activation: 3749.634, activation diff: 4853.739, ratio: 1.294
#   pulse 11: activated nodes: 11303, borderline nodes: 495, overall activation: 1104.152, activation diff: 4853.785, ratio: 4.396
#   pulse 12: activated nodes: 11303, borderline nodes: 495, overall activation: 3749.654, activation diff: 4853.805, ratio: 1.294
#   pulse 13: activated nodes: 11303, borderline nodes: 495, overall activation: 1104.157, activation diff: 4853.811, ratio: 4.396
#   pulse 14: activated nodes: 11303, borderline nodes: 495, overall activation: 3749.656, activation diff: 4853.813, ratio: 1.294
#   pulse 15: activated nodes: 11303, borderline nodes: 495, overall activation: 1104.158, activation diff: 4853.814, ratio: 4.396

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 1104.2
#   number of spread. activ. pulses: 15
#   running time: 668

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
