###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 91.827, activation diff: 81.209, ratio: 0.884
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 260.955, activation diff: 169.210, ratio: 0.648
#   pulse 4: activated nodes: 9812, borderline nodes: 5849, overall activation: 693.181, activation diff: 432.225, ratio: 0.624
#   pulse 5: activated nodes: 10770, borderline nodes: 4328, overall activation: 1245.636, activation diff: 552.456, ratio: 0.444
#   pulse 6: activated nodes: 11030, borderline nodes: 3263, overall activation: 1807.122, activation diff: 561.486, ratio: 0.311
#   pulse 7: activated nodes: 11130, borderline nodes: 2408, overall activation: 2321.230, activation diff: 514.107, ratio: 0.221
#   pulse 8: activated nodes: 11205, borderline nodes: 1464, overall activation: 2768.218, activation diff: 446.989, ratio: 0.161
#   pulse 9: activated nodes: 11227, borderline nodes: 1084, overall activation: 3145.540, activation diff: 377.322, ratio: 0.120
#   pulse 10: activated nodes: 11260, borderline nodes: 820, overall activation: 3458.157, activation diff: 312.617, ratio: 0.090
#   pulse 11: activated nodes: 11265, borderline nodes: 750, overall activation: 3713.955, activation diff: 255.798, ratio: 0.069
#   pulse 12: activated nodes: 11279, borderline nodes: 685, overall activation: 3921.440, activation diff: 207.485, ratio: 0.053
#   pulse 13: activated nodes: 11286, borderline nodes: 657, overall activation: 4088.650, activation diff: 167.210, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11286
#   final overall activation: 4088.6
#   number of spread. activ. pulses: 13
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911115   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.987026   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9847301   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9834967   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.975206   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9548234   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7244821   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7237998   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7230532   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.72289383   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.7219871   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.7219639   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.72195494   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7215577   REFERENCES
1   Q1   europesincenapol00leveuoft_17   15   0.7215514   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7215413   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7215319   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.72142553   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7213424   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.72127867   REFERENCES
