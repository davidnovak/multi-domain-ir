###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 586.034, activation diff: 570.506, ratio: 0.974
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1166.929, activation diff: 580.896, ratio: 0.498
#   pulse 4: activated nodes: 10434, borderline nodes: 4401, overall activation: 2609.015, activation diff: 1442.086, ratio: 0.553
#   pulse 5: activated nodes: 11204, borderline nodes: 1729, overall activation: 3744.381, activation diff: 1135.366, ratio: 0.303
#   pulse 6: activated nodes: 11274, borderline nodes: 698, overall activation: 4492.134, activation diff: 747.752, ratio: 0.166
#   pulse 7: activated nodes: 11306, borderline nodes: 435, overall activation: 4950.648, activation diff: 458.514, ratio: 0.093
#   pulse 8: activated nodes: 11315, borderline nodes: 314, overall activation: 5222.357, activation diff: 271.709, ratio: 0.052
#   pulse 9: activated nodes: 11317, borderline nodes: 259, overall activation: 5380.357, activation diff: 158.000, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 5380.4
#   number of spread. activ. pulses: 9
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99771583   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.997329   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9971398   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99395585   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98658735   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79681265   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7967508   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7966713   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7966393   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.79650927   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7965089   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.79649484   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.79643786   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.7964361   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.79641855   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7964082   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.7964033   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   19   0.7963979   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.79638314   REFERENCES
