###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 241.897, activation diff: 229.194, ratio: 0.947
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 615.238, activation diff: 373.351, ratio: 0.607
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1305.936, activation diff: 690.699, ratio: 0.529
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2050.122, activation diff: 744.186, ratio: 0.363
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2737.470, activation diff: 687.349, ratio: 0.251
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3331.854, activation diff: 594.383, ratio: 0.178
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 3828.965, activation diff: 497.111, ratio: 0.130
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 4236.752, activation diff: 407.786, ratio: 0.096
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 4567.169, activation diff: 330.417, ratio: 0.072
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 4832.661, activation diff: 265.492, ratio: 0.055
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 5044.710, activation diff: 212.049, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5044.7
#   number of spread. activ. pulses: 12
#   running time: 531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9860505   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9848446   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98291534   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9747805   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9599354   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7651488   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7643283   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7640061   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.7639758   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.76336735   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   12   0.76273894   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.76271343   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7626912   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.7626028   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.7625947   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7624411   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.76241195   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   19   0.76223403   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7622179   REFERENCES
