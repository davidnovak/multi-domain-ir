###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 448.424, activation diff: 430.353, ratio: 0.960
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 726.045, activation diff: 277.621, ratio: 0.382
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1166.031, activation diff: 439.986, ratio: 0.377
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1484.356, activation diff: 318.325, ratio: 0.214
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1684.033, activation diff: 199.678, ratio: 0.119
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1802.352, activation diff: 118.319, ratio: 0.066
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1870.351, activation diff: 67.998, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1870.4
#   number of spread. activ. pulses: 8
#   running time: 407

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99566436   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99520844   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99479884   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.991061   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98190427   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.49605078   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49598756   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49593258   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.495918   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.49580625   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.49574643   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.49574643   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.4957019   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.49568608   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.4956832   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.49568254   REFERENCES
1   Q1   europesincenapol00leveuoft_17   18   0.49566352   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.49564752   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.4956392   REFERENCES
