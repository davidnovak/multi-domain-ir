###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 456.700, activation diff: 443.843, ratio: 0.972
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 874.717, activation diff: 421.116, ratio: 0.481
#   pulse 4: activated nodes: 10382, borderline nodes: 4922, overall activation: 2095.713, activation diff: 1220.995, ratio: 0.583
#   pulse 5: activated nodes: 11085, borderline nodes: 2974, overall activation: 3077.362, activation diff: 981.650, ratio: 0.319
#   pulse 6: activated nodes: 11224, borderline nodes: 1203, overall activation: 3738.644, activation diff: 661.282, ratio: 0.177
#   pulse 7: activated nodes: 11259, borderline nodes: 793, overall activation: 4149.669, activation diff: 411.025, ratio: 0.099
#   pulse 8: activated nodes: 11284, borderline nodes: 674, overall activation: 4395.788, activation diff: 246.119, ratio: 0.056
#   pulse 9: activated nodes: 11290, borderline nodes: 628, overall activation: 4540.260, activation diff: 144.472, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 4540.3
#   number of spread. activ. pulses: 9
#   running time: 473

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99752694   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9968688   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99678874   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9933237   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9847133   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7469605   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7468933   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7467596   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.74668974   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.74658406   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.74658334   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.746506   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7464824   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.74646753   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7464613   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7464355   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.7464061   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7464043   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.7464032   REFERENCES
