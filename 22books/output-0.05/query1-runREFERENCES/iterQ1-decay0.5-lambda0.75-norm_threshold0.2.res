###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 90.191, activation diff: 78.817, ratio: 0.874
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 218.753, activation diff: 128.617, ratio: 0.588
#   pulse 4: activated nodes: 8333, borderline nodes: 4229, overall activation: 432.359, activation diff: 213.606, ratio: 0.494
#   pulse 5: activated nodes: 8654, borderline nodes: 3405, overall activation: 663.581, activation diff: 231.222, ratio: 0.348
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 880.125, activation diff: 216.544, ratio: 0.246
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1070.192, activation diff: 190.066, ratio: 0.178
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1231.262, activation diff: 161.070, ratio: 0.131
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1364.855, activation diff: 133.593, ratio: 0.098
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1474.082, activation diff: 109.227, ratio: 0.074
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1562.481, activation diff: 88.399, ratio: 0.057
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1633.483, activation diff: 71.002, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1633.5
#   number of spread. activ. pulses: 12
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98857284   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98408085   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9818175   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97986495   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9703002   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9481673   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.47770825   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.4770731   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.4766222   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.47656697   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.4759101   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.4755414   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.4755379   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.4753492   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.47532976   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.4752307   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.47521773   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.4751975   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.47509563   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   20   0.4750657   REFERENCES
