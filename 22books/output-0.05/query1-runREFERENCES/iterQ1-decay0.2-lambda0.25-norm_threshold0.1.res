###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1435.456, activation diff: 1435.008, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1899.827, activation diff: 887.397, ratio: 0.467
#   pulse 4: activated nodes: 10802, borderline nodes: 2442, overall activation: 4071.795, activation diff: 2189.903, ratio: 0.538
#   pulse 5: activated nodes: 11265, borderline nodes: 803, overall activation: 5117.961, activation diff: 1046.173, ratio: 0.204
#   pulse 6: activated nodes: 11319, borderline nodes: 192, overall activation: 5513.501, activation diff: 395.540, ratio: 0.072
#   pulse 7: activated nodes: 11324, borderline nodes: 121, overall activation: 5653.183, activation diff: 139.682, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 5653.2
#   number of spread. activ. pulses: 7
#   running time: 453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99966294   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99950236   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994481   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99723613   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99287987   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7997643   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7997376   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7997016   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7996731   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.79964733   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7996473   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7996243   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7996222   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.79961497   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.79960984   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.79960054   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.79960036   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   19   0.799596   REFERENCES
1   Q1   politicalsketche00retsrich_154   20   0.7995844   REFERENCES
