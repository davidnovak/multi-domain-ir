###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1184.540, activation diff: 1187.861, ratio: 1.003
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1371.511, activation diff: 759.495, ratio: 0.554
#   pulse 4: activated nodes: 10788, borderline nodes: 2906, overall activation: 3326.808, activation diff: 1999.149, ratio: 0.601
#   pulse 5: activated nodes: 11229, borderline nodes: 1339, overall activation: 4279.213, activation diff: 952.473, ratio: 0.223
#   pulse 6: activated nodes: 11278, borderline nodes: 617, overall activation: 4648.169, activation diff: 368.956, ratio: 0.079
#   pulse 7: activated nodes: 11299, borderline nodes: 531, overall activation: 4780.575, activation diff: 132.406, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11299
#   final overall activation: 4780.6
#   number of spread. activ. pulses: 7
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99951136   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99922925   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9991933   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9968325   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99188685   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7497174   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74969214   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7496149   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   10   0.7495614   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7495613   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.74954545   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7495371   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.74952066   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.7495124   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.74949354   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.74948525   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.7494849   REFERENCES
1   Q1   politicalsketche00retsrich_154   19   0.7494826   REFERENCES
1   Q1   europesincenapol00leveuoft_17   20   0.74945724   REFERENCES
