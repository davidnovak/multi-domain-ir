###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 182.960, activation diff: 170.895, ratio: 0.934
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 483.003, activation diff: 300.074, ratio: 0.621
#   pulse 4: activated nodes: 10733, borderline nodes: 4016, overall activation: 1113.779, activation diff: 630.777, ratio: 0.566
#   pulse 5: activated nodes: 11188, borderline nodes: 2058, overall activation: 1829.426, activation diff: 715.647, ratio: 0.391
#   pulse 6: activated nodes: 11228, borderline nodes: 934, overall activation: 2510.443, activation diff: 681.016, ratio: 0.271
#   pulse 7: activated nodes: 11286, borderline nodes: 593, overall activation: 3109.912, activation diff: 599.469, ratio: 0.193
#   pulse 8: activated nodes: 11312, borderline nodes: 406, overall activation: 3617.327, activation diff: 507.415, ratio: 0.140
#   pulse 9: activated nodes: 11315, borderline nodes: 298, overall activation: 4037.249, activation diff: 419.922, ratio: 0.104
#   pulse 10: activated nodes: 11317, borderline nodes: 248, overall activation: 4379.905, activation diff: 342.656, ratio: 0.078
#   pulse 11: activated nodes: 11318, borderline nodes: 166, overall activation: 4656.882, activation diff: 276.977, ratio: 0.059
#   pulse 12: activated nodes: 11324, borderline nodes: 142, overall activation: 4879.262, activation diff: 222.380, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 4879.3
#   number of spread. activ. pulses: 12
#   running time: 541

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888482   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98520863   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98358256   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98162043   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97285897   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9554768   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7648111   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7639153   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7634   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7633814   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.76257133   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.762033   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7620052   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.76188385   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.76179856   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.7616765   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.7616529   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   18   0.76160604   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.76147866   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.76142025   REFERENCES
