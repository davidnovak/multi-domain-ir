###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1094.130, activation diff: 1099.335, ratio: 1.005
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1237.628, activation diff: 855.779, ratio: 0.691
#   pulse 4: activated nodes: 10443, borderline nodes: 4125, overall activation: 3535.919, activation diff: 2388.831, ratio: 0.676
#   pulse 5: activated nodes: 11210, borderline nodes: 1770, overall activation: 4713.431, activation diff: 1177.916, ratio: 0.250
#   pulse 6: activated nodes: 11278, borderline nodes: 659, overall activation: 5196.169, activation diff: 482.738, ratio: 0.093
#   pulse 7: activated nodes: 11307, borderline nodes: 421, overall activation: 5373.953, activation diff: 177.784, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11307
#   final overall activation: 5374.0
#   number of spread. activ. pulses: 7
#   running time: 453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999303   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99895155   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9988157   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9963565   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9907972   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79958546   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79956377   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7994245   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.7993747   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.7993747   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.79934764   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.79931515   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.7993062   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.7992893   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.79928505   REFERENCES
1   Q1   politicalsketche00retsrich_154   17   0.7992697   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.79925704   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7992568   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.7992372   REFERENCES
