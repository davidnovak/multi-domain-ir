###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 657.005, activation diff: 641.477, ratio: 0.976
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1377.482, activation diff: 720.478, ratio: 0.523
#   pulse 4: activated nodes: 10439, borderline nodes: 4281, overall activation: 3257.421, activation diff: 1879.939, ratio: 0.577
#   pulse 5: activated nodes: 11207, borderline nodes: 1617, overall activation: 4761.821, activation diff: 1504.399, ratio: 0.316
#   pulse 6: activated nodes: 11289, borderline nodes: 470, overall activation: 5761.297, activation diff: 999.476, ratio: 0.173
#   pulse 7: activated nodes: 11316, borderline nodes: 223, overall activation: 6378.199, activation diff: 616.903, ratio: 0.097
#   pulse 8: activated nodes: 11321, borderline nodes: 139, overall activation: 6746.045, activation diff: 367.846, ratio: 0.055
#   pulse 9: activated nodes: 11325, borderline nodes: 108, overall activation: 6961.550, activation diff: 215.504, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11325
#   final overall activation: 6961.5
#   number of spread. activ. pulses: 9
#   running time: 484

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99771595   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.997329   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99714124   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9939594   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.986604   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8964142   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89634466   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89625514   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8962192   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.896073   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.89607286   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.8960568   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8959939   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.8959906   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.89597356   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.89595926   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.89595366   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   19   0.8959476   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.8959312   REFERENCES
