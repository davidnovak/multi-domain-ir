###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 688.082, activation diff: 693.287, ratio: 1.008
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 506.643, activation diff: 264.022, ratio: 0.521
#   pulse 4: activated nodes: 8333, borderline nodes: 4229, overall activation: 1343.992, activation diff: 837.448, ratio: 0.623
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1678.991, activation diff: 335.006, ratio: 0.200
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1791.397, activation diff: 112.406, ratio: 0.063
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1826.490, activation diff: 35.093, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1826.5
#   number of spread. activ. pulses: 7
#   running time: 405

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99930257   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9989471   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9988157   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99633527   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99065626   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4997409   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49972677   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.4996401   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.49959916   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.49959916   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.49956626   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49956194   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.49956185   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.49955568   REFERENCES
1   Q1   politicalsketche00retsrich_154   16   0.49953192   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   17   0.49952674   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.49952555   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.49952555   REFERENCES
1   Q1   europesincenapol00leveuoft_17   20   0.4994896   REFERENCES
