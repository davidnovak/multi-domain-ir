###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 157.777, activation diff: 145.074, ratio: 0.919
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 343.392, activation diff: 185.626, ratio: 0.541
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 587.912, activation diff: 244.520, ratio: 0.416
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 830.357, activation diff: 242.446, ratio: 0.292
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1047.875, activation diff: 217.517, ratio: 0.208
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1233.937, activation diff: 186.062, ratio: 0.151
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1388.864, activation diff: 154.928, ratio: 0.112
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1515.700, activation diff: 126.835, ratio: 0.084
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1618.347, activation diff: 102.647, ratio: 0.063
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1700.732, activation diff: 82.385, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1700.7
#   number of spread. activ. pulses: 11
#   running time: 454

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9853725   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98138416   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9797698   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97714883   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.96699405   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9477776   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4709524   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.47023296   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.4699826   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.4699525   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.46944198   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   12   0.46883523   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.4687392   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   14   0.4687388   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.46873802   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.46869346   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.46849468   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.46847475   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.4684549   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.46832788   REFERENCES
