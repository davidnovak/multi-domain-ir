###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 65.913, activation diff: 55.295, ratio: 0.839
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 168.918, activation diff: 103.087, ratio: 0.610
#   pulse 4: activated nodes: 8066, borderline nodes: 4568, overall activation: 359.392, activation diff: 190.474, ratio: 0.530
#   pulse 5: activated nodes: 8577, borderline nodes: 3608, overall activation: 579.600, activation diff: 220.208, ratio: 0.380
#   pulse 6: activated nodes: 8654, borderline nodes: 3410, overall activation: 792.632, activation diff: 213.032, ratio: 0.269
#   pulse 7: activated nodes: 8654, borderline nodes: 3405, overall activation: 983.083, activation diff: 190.452, ratio: 0.194
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1146.440, activation diff: 163.356, ratio: 0.142
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1283.105, activation diff: 136.666, ratio: 0.107
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1395.583, activation diff: 112.478, ratio: 0.081
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1487.096, activation diff: 91.513, ratio: 0.062
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1560.920, activation diff: 73.824, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1560.9
#   number of spread. activ. pulses: 12
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98814803   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98262787   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9794149   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97778773   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9676038   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94044733   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4773015   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.4766488   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.47600168   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.4758702   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.47502765   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.47486043   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.47485286   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.4744994   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.47447598   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.47442564   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.47442526   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.4744193   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.47439206   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.47423905   REFERENCES
