###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1004.078, activation diff: 999.872, ratio: 0.996
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1009.781, activation diff: 141.403, ratio: 0.140
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1644.248, activation diff: 634.466, ratio: 0.386
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1891.187, activation diff: 246.939, ratio: 0.131
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1972.555, activation diff: 81.369, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1972.6
#   number of spread. activ. pulses: 6
#   running time: 388

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99925435   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99895906   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99864477   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9984314   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9958229   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9899837   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.49947292   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49942574   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49938786   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.4993627   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.49928156   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.4992747   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.4992747   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.4992389   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.49923888   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   16   0.49921572   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.49921522   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.4992084   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.4992084   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.4992032   REFERENCES
