###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 127.636, activation diff: 116.263, ratio: 0.911
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 345.437, activation diff: 217.855, ratio: 0.631
#   pulse 4: activated nodes: 10294, borderline nodes: 5395, overall activation: 841.209, activation diff: 495.772, ratio: 0.589
#   pulse 5: activated nodes: 10962, borderline nodes: 3577, overall activation: 1428.873, activation diff: 587.664, ratio: 0.411
#   pulse 6: activated nodes: 11173, borderline nodes: 2143, overall activation: 2004.089, activation diff: 575.216, ratio: 0.287
#   pulse 7: activated nodes: 11220, borderline nodes: 1283, overall activation: 2519.645, activation diff: 515.556, ratio: 0.205
#   pulse 8: activated nodes: 11230, borderline nodes: 857, overall activation: 2961.325, activation diff: 441.681, ratio: 0.149
#   pulse 9: activated nodes: 11271, borderline nodes: 729, overall activation: 3329.953, activation diff: 368.627, ratio: 0.111
#   pulse 10: activated nodes: 11289, borderline nodes: 663, overall activation: 3632.641, activation diff: 302.688, ratio: 0.083
#   pulse 11: activated nodes: 11295, borderline nodes: 631, overall activation: 3878.503, activation diff: 245.862, ratio: 0.063
#   pulse 12: activated nodes: 11297, borderline nodes: 594, overall activation: 4076.678, activation diff: 198.174, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11297
#   final overall activation: 4076.7
#   number of spread. activ. pulses: 12
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885731   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98411894   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98190737   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98000956   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97061026   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94977546   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7165761   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.715682   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7149824   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.71488976   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7139198   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.71360993   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.71357405   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.7132294   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   15   0.71322036   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.7131678   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.7131578   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   18   0.7130425   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.71290857   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.71286875   REFERENCES
