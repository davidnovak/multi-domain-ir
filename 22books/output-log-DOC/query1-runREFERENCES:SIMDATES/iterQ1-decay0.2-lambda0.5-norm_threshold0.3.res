###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 147.481, activation diff: 134.708, ratio: 0.913
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 306.759, activation diff: 159.952, ratio: 0.521
#   pulse 4: activated nodes: 9226, borderline nodes: 5931, overall activation: 1052.329, activation diff: 745.570, ratio: 0.708
#   pulse 5: activated nodes: 10647, borderline nodes: 4021, overall activation: 1966.296, activation diff: 913.967, ratio: 0.465
#   pulse 6: activated nodes: 11050, borderline nodes: 2691, overall activation: 2843.960, activation diff: 877.664, ratio: 0.309
#   pulse 7: activated nodes: 11203, borderline nodes: 1586, overall activation: 3522.491, activation diff: 678.531, ratio: 0.193
#   pulse 8: activated nodes: 11253, borderline nodes: 978, overall activation: 3995.537, activation diff: 473.046, ratio: 0.118
#   pulse 9: activated nodes: 11284, borderline nodes: 747, overall activation: 4307.852, activation diff: 312.315, ratio: 0.072
#   pulse 10: activated nodes: 11295, borderline nodes: 641, overall activation: 4508.033, activation diff: 200.181, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11295
#   final overall activation: 4508.0
#   number of spread. activ. pulses: 10
#   running time: 501

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99639165   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9797752   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9781339   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95846754   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9015509   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.86664826   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79578626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.79474926   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7943301   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.793213   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.79320645   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   12   0.79311514   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.79310596   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7930262   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   15   0.79299027   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   16   0.792983   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   17   0.7928915   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   18   0.7928308   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.7927215   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   20   0.7926152   REFERENCES:SIMDATES
