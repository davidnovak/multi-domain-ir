###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 79.758, activation diff: 68.684, ratio: 0.861
#   pulse 3: activated nodes: 8023, borderline nodes: 4134, overall activation: 222.664, activation diff: 143.360, ratio: 0.644
#   pulse 4: activated nodes: 10221, borderline nodes: 4999, overall activation: 595.083, activation diff: 372.442, ratio: 0.626
#   pulse 5: activated nodes: 10985, borderline nodes: 3339, overall activation: 1178.721, activation diff: 583.637, ratio: 0.495
#   pulse 6: activated nodes: 11182, borderline nodes: 2071, overall activation: 1884.561, activation diff: 705.840, ratio: 0.375
#   pulse 7: activated nodes: 11237, borderline nodes: 1010, overall activation: 2613.812, activation diff: 729.252, ratio: 0.279
#   pulse 8: activated nodes: 11294, borderline nodes: 567, overall activation: 3303.162, activation diff: 689.350, ratio: 0.209
#   pulse 9: activated nodes: 11310, borderline nodes: 343, overall activation: 3921.918, activation diff: 618.755, ratio: 0.158
#   pulse 10: activated nodes: 11322, borderline nodes: 215, overall activation: 4460.022, activation diff: 538.104, ratio: 0.121
#   pulse 11: activated nodes: 11326, borderline nodes: 144, overall activation: 4918.264, activation diff: 458.242, ratio: 0.093
#   pulse 12: activated nodes: 11334, borderline nodes: 93, overall activation: 5302.760, activation diff: 384.496, ratio: 0.073
#   pulse 13: activated nodes: 11337, borderline nodes: 69, overall activation: 5621.855, activation diff: 319.095, ratio: 0.057
#   pulse 14: activated nodes: 11337, borderline nodes: 56, overall activation: 5884.510, activation diff: 262.655, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 5884.5
#   number of spread. activ. pulses: 14
#   running time: 612

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9840801   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9588631   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9585309   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93590224   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8842234   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.86677814   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.86512864   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.8623321   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.861855   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.8609951   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.8603039   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   12   0.8602115   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.8599075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.85956275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.8595563   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.85952425   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.8592147   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   18   0.8592136   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.859007   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   20   0.85897946   REFERENCES:SIMDATES
