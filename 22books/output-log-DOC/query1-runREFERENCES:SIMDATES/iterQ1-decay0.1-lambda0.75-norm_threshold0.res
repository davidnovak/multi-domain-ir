###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 139.110, activation diff: 127.302, ratio: 0.915
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 372.264, activation diff: 233.494, ratio: 0.627
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 900.311, activation diff: 528.049, ratio: 0.587
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1619.060, activation diff: 718.749, ratio: 0.444
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2401.750, activation diff: 782.690, ratio: 0.326
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 3158.527, activation diff: 756.776, ratio: 0.240
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 3844.343, activation diff: 685.817, ratio: 0.178
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 4442.593, activation diff: 598.250, ratio: 0.135
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 4951.859, activation diff: 509.265, ratio: 0.103
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 5378.209, activation diff: 426.350, ratio: 0.079
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 5730.961, activation diff: 352.752, ratio: 0.062
#   pulse 13: activated nodes: 11348, borderline nodes: 0, overall activation: 6020.327, activation diff: 289.366, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6020.3
#   number of spread. activ. pulses: 13
#   running time: 582

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812691   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9567281   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.95596683   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93349624   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8847783   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8585066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.85624504   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.8538579   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.85281587   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.8520463   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.8515044   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.8514665   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.8513715   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.85106254   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.85101205   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.8508917   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   17   0.85071254   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.8505045   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.8503674   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.85026705   REFERENCES:SIMDATES
