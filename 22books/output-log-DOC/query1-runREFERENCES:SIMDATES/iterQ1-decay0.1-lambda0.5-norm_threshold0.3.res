###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 164.021, activation diff: 151.248, ratio: 0.922
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 356.499, activation diff: 193.212, ratio: 0.542
#   pulse 4: activated nodes: 9253, borderline nodes: 5911, overall activation: 1351.917, activation diff: 995.418, ratio: 0.736
#   pulse 5: activated nodes: 10783, borderline nodes: 3748, overall activation: 2605.700, activation diff: 1253.782, ratio: 0.481
#   pulse 6: activated nodes: 11128, borderline nodes: 2283, overall activation: 3820.194, activation diff: 1214.494, ratio: 0.318
#   pulse 7: activated nodes: 11227, borderline nodes: 1185, overall activation: 4755.440, activation diff: 935.246, ratio: 0.197
#   pulse 8: activated nodes: 11294, borderline nodes: 683, overall activation: 5403.387, activation diff: 647.947, ratio: 0.120
#   pulse 9: activated nodes: 11308, borderline nodes: 468, overall activation: 5829.614, activation diff: 426.227, ratio: 0.073
#   pulse 10: activated nodes: 11320, borderline nodes: 360, overall activation: 6102.584, activation diff: 272.970, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 6102.6
#   number of spread. activ. pulses: 10
#   running time: 525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99641836   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98007417   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9785724   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9588487   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.90176153   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89536357   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8949347   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.89399576   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   9   0.8937671   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.8933677   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   11   0.89336425   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8933556   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.89332175   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.89323205   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.8932307   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   16   0.89313304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.89310586   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   18   0.8930999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.89306355   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   20   0.8929422   REFERENCES:SIMDATES
