###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 38.054, activation diff: 27.752, ratio: 0.729
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 105.804, activation diff: 68.346, ratio: 0.646
#   pulse 4: activated nodes: 9123, borderline nodes: 5840, overall activation: 273.442, activation diff: 167.700, ratio: 0.613
#   pulse 5: activated nodes: 10154, borderline nodes: 4999, overall activation: 550.035, activation diff: 276.593, ratio: 0.503
#   pulse 6: activated nodes: 10705, borderline nodes: 4160, overall activation: 915.592, activation diff: 365.557, ratio: 0.399
#   pulse 7: activated nodes: 10995, borderline nodes: 3080, overall activation: 1323.875, activation diff: 408.283, ratio: 0.308
#   pulse 8: activated nodes: 11124, borderline nodes: 2287, overall activation: 1733.763, activation diff: 409.888, ratio: 0.236
#   pulse 9: activated nodes: 11195, borderline nodes: 1603, overall activation: 2119.292, activation diff: 385.529, ratio: 0.182
#   pulse 10: activated nodes: 11226, borderline nodes: 1165, overall activation: 2466.931, activation diff: 347.640, ratio: 0.141
#   pulse 11: activated nodes: 11256, borderline nodes: 918, overall activation: 2771.473, activation diff: 304.542, ratio: 0.110
#   pulse 12: activated nodes: 11279, borderline nodes: 781, overall activation: 3032.958, activation diff: 261.485, ratio: 0.086
#   pulse 13: activated nodes: 11292, borderline nodes: 691, overall activation: 3254.228, activation diff: 221.270, ratio: 0.068
#   pulse 14: activated nodes: 11294, borderline nodes: 653, overall activation: 3439.442, activation diff: 185.214, ratio: 0.054
#   pulse 15: activated nodes: 11300, borderline nodes: 604, overall activation: 3593.214, activation diff: 153.772, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11300
#   final overall activation: 3593.2
#   number of spread. activ. pulses: 15
#   running time: 760

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98583007   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9581134   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9570888   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9348101   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.88075227   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8383033   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.72593987   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7231679   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7212423   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7205138   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.72030777   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   12   0.7166394   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   13   0.7165085   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.71644163   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.71616805   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   16   0.7160647   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7159101   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.7156279   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   19   0.71509975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.7149583   REFERENCES:SIMDATES
