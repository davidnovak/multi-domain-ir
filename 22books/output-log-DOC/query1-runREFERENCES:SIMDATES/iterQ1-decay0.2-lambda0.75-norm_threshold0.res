###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 125.554, activation diff: 113.746, ratio: 0.906
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 322.280, activation diff: 197.066, ratio: 0.611
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 725.636, activation diff: 403.360, ratio: 0.556
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1258.520, activation diff: 532.883, ratio: 0.423
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 1834.391, activation diff: 575.871, ratio: 0.314
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 2391.680, activation diff: 557.289, ratio: 0.233
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 2898.297, activation diff: 506.617, ratio: 0.175
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 3341.780, activation diff: 443.483, ratio: 0.133
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 3720.686, activation diff: 378.906, ratio: 0.102
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 4039.121, activation diff: 318.434, ratio: 0.079
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 4303.599, activation diff: 264.478, ratio: 0.061
#   pulse 13: activated nodes: 11342, borderline nodes: 14, overall activation: 4521.348, activation diff: 217.749, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 4521.3
#   number of spread. activ. pulses: 13
#   running time: 587

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812066   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9564276   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9555776   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93288004   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.88443047   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84993476   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7625411   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.75938606   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.75785255   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7561188   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.7558093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7545116   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.7540093   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7540015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.75399595   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   16   0.7538191   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.7537707   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.7535395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   19   0.753431   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   20   0.7530496   REFERENCES:SIMDATES
