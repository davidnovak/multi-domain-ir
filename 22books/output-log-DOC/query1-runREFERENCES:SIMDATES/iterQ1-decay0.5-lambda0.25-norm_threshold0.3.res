###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 274.515, activation diff: 278.885, ratio: 1.016
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 199.056, activation diff: 183.756, ratio: 0.923
#   pulse 4: activated nodes: 8149, borderline nodes: 4445, overall activation: 784.323, activation diff: 592.820, ratio: 0.756
#   pulse 5: activated nodes: 8829, borderline nodes: 3623, overall activation: 1061.318, activation diff: 278.845, ratio: 0.263
#   pulse 6: activated nodes: 8907, borderline nodes: 3629, overall activation: 1203.461, activation diff: 142.142, ratio: 0.118
#   pulse 7: activated nodes: 8927, borderline nodes: 3613, overall activation: 1256.828, activation diff: 53.367, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8927
#   final overall activation: 1256.8
#   number of spread. activ. pulses: 7
#   running time: 431

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99726874   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98177624   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.97733074   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9574503   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.90314746   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.866208   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49739045   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4949309   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49263266   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49004287   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48893017   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.48499954   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48399702   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.4821647   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48018873   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.47971457   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.4785072   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.47653556   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   19   0.47510216   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.47422537   REFERENCES:SIMDATES
