###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 485.706, activation diff: 490.077, ratio: 1.009
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 569.488, activation diff: 548.394, ratio: 0.963
#   pulse 4: activated nodes: 9943, borderline nodes: 5240, overall activation: 2920.425, activation diff: 2478.315, ratio: 0.849
#   pulse 5: activated nodes: 11082, borderline nodes: 2961, overall activation: 4472.165, activation diff: 1566.880, ratio: 0.350
#   pulse 6: activated nodes: 11255, borderline nodes: 1026, overall activation: 5582.768, activation diff: 1110.613, ratio: 0.199
#   pulse 7: activated nodes: 11306, borderline nodes: 600, overall activation: 6128.268, activation diff: 545.500, ratio: 0.089
#   pulse 8: activated nodes: 11320, borderline nodes: 375, overall activation: 6370.083, activation diff: 241.815, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 6370.1
#   number of spread. activ. pulses: 8
#   running time: 491

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990021   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98678154   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9847162   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9663037   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.90931535   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8994139   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.8992183   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   8   0.89919066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   9   0.8991802   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8991507   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.8991407   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.8991394   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.89913464   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.89913243   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   15   0.89911944   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   16   0.8991175   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8991166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   18   0.899103   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   19   0.8990928   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.89907956   REFERENCES:SIMDATES
