###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1088.204, activation diff: 1116.466, ratio: 1.026
#   pulse 3: activated nodes: 8411, borderline nodes: 3562, overall activation: 609.977, activation diff: 1696.946, ratio: 2.782
#   pulse 4: activated nodes: 10566, borderline nodes: 3596, overall activation: 3574.274, activation diff: 4106.636, ratio: 1.149
#   pulse 5: activated nodes: 11216, borderline nodes: 1798, overall activation: 1384.170, activation diff: 4606.678, ratio: 3.328
#   pulse 6: activated nodes: 11295, borderline nodes: 447, overall activation: 4060.047, activation diff: 4469.020, ratio: 1.101
#   pulse 7: activated nodes: 11319, borderline nodes: 376, overall activation: 4492.927, activation diff: 1466.103, ratio: 0.326
#   pulse 8: activated nodes: 11321, borderline nodes: 312, overall activation: 4941.791, activation diff: 618.046, ratio: 0.125
#   pulse 9: activated nodes: 11324, borderline nodes: 297, overall activation: 5023.659, activation diff: 115.555, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 5023.7
#   number of spread. activ. pulses: 9
#   running time: 532

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995846   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98746425   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9711392   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91908765   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8940381   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.799985   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.79991835   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7998686   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.79986733   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.7998376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7998137   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.79980844   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7997913   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.7997896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.7997807   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7997624   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.7997492   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.79972494   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.79971695   REFERENCES:SIMDATES
