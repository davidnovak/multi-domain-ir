###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1465.709, activation diff: 1497.145, ratio: 1.021
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 781.551, activation diff: 2175.225, ratio: 2.783
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3531.285, activation diff: 3907.296, ratio: 1.106
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3765.119, activation diff: 1717.496, ratio: 0.456
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4473.763, activation diff: 925.261, ratio: 0.207
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 4594.031, activation diff: 154.152, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4594.0
#   number of spread. activ. pulses: 7
#   running time: 463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99965715   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.989805   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9762489   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9332608   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.91240364   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74997556   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7498233   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7498012   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7497791   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7496628   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7496353   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.74959254   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.7495609   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.74954885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.7495469   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.74950874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.74946654   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.7494508   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   20   0.74944067   REFERENCES:SIMDATES
