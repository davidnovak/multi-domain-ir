###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 878.216, activation diff: 872.772, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1239.373, activation diff: 517.775, ratio: 0.418
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2936.638, activation diff: 1697.264, ratio: 0.578
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3904.569, activation diff: 967.931, ratio: 0.248
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4333.160, activation diff: 428.592, ratio: 0.099
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 4506.749, activation diff: 173.589, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4506.7
#   number of spread. activ. pulses: 7
#   running time: 448

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9987993   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98923165   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98781943   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97349757   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.930316   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.908445   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74893326   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.74836564   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.74795324   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7478   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7477734   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7477242   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.74768573   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7476747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7475637   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   16   0.7475374   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.74747974   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   18   0.7474625   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.74745524   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7473447   REFERENCES:SIMDATES
