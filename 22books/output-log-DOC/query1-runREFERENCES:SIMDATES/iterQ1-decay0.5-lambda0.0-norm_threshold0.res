###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 979.313, activation diff: 1010.749, ratio: 1.032
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 113.504, activation diff: 1049.552, ratio: 9.247
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1501.956, activation diff: 1485.783, ratio: 0.989
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1267.696, activation diff: 333.153, ratio: 0.263
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1551.448, activation diff: 284.705, ratio: 0.184
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1551.592, activation diff: 0.861, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1551.6
#   number of spread. activ. pulses: 7
#   running time: 524

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996267   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99109733   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98868084   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9748575   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9329461   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9122115   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49928358   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4980411   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.495943   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4941988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49351183   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49268967   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.489513   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48837796   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48837772   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.48743373   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48646146   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.48590553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.48519838   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.48517433   REFERENCES:SIMDATES
