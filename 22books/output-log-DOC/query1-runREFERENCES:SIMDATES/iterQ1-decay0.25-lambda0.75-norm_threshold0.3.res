###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 21.661, activation diff: 12.164, ratio: 0.562
#   pulse 3: activated nodes: 6261, borderline nodes: 5235, overall activation: 59.908, activation diff: 39.011, ratio: 0.651
#   pulse 4: activated nodes: 7644, borderline nodes: 5529, overall activation: 158.061, activation diff: 98.341, ratio: 0.622
#   pulse 5: activated nodes: 9135, borderline nodes: 5657, overall activation: 340.648, activation diff: 182.597, ratio: 0.536
#   pulse 6: activated nodes: 10018, borderline nodes: 5105, overall activation: 614.865, activation diff: 274.217, ratio: 0.446
#   pulse 7: activated nodes: 10592, borderline nodes: 4401, overall activation: 960.329, activation diff: 345.464, ratio: 0.360
#   pulse 8: activated nodes: 10836, borderline nodes: 3522, overall activation: 1338.784, activation diff: 378.455, ratio: 0.283
#   pulse 9: activated nodes: 11009, borderline nodes: 2796, overall activation: 1716.188, activation diff: 377.404, ratio: 0.220
#   pulse 10: activated nodes: 11099, borderline nodes: 2238, overall activation: 2070.882, activation diff: 354.695, ratio: 0.171
#   pulse 11: activated nodes: 11168, borderline nodes: 1735, overall activation: 2391.553, activation diff: 320.671, ratio: 0.134
#   pulse 12: activated nodes: 11198, borderline nodes: 1411, overall activation: 2673.667, activation diff: 282.114, ratio: 0.106
#   pulse 13: activated nodes: 11220, borderline nodes: 1159, overall activation: 2917.149, activation diff: 243.482, ratio: 0.083
#   pulse 14: activated nodes: 11250, borderline nodes: 995, overall activation: 3124.268, activation diff: 207.119, ratio: 0.066
#   pulse 15: activated nodes: 11256, borderline nodes: 901, overall activation: 3298.561, activation diff: 174.293, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11256
#   final overall activation: 3298.6
#   number of spread. activ. pulses: 15
#   running time: 568

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9825746   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9479244   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.94332373   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.92265385   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.86613345   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8111708   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.72209907   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.71941197   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7162384   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7161092   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.7153562   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7112017   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   13   0.710528   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   14   0.710372   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   15   0.7099123   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   16   0.70953345   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.7094761   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   18   0.70930797   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.7092778   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7090268   REFERENCES:SIMDATES
