###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 544.254, activation diff: 570.656, ratio: 1.049
#   pulse 3: activated nodes: 8131, borderline nodes: 3942, overall activation: 90.273, activation diff: 634.165, ratio: 7.025
#   pulse 4: activated nodes: 8399, borderline nodes: 4145, overall activation: 1190.774, activation diff: 1266.781, ratio: 1.064
#   pulse 5: activated nodes: 8929, borderline nodes: 3613, overall activation: 97.376, activation diff: 1267.842, ratio: 13.020
#   pulse 6: activated nodes: 8929, borderline nodes: 3613, overall activation: 1205.116, activation diff: 1263.287, ratio: 1.048
#   pulse 7: activated nodes: 8935, borderline nodes: 3616, overall activation: 151.435, activation diff: 1209.483, ratio: 7.987
#   pulse 8: activated nodes: 8935, borderline nodes: 3616, overall activation: 1251.233, activation diff: 1163.677, ratio: 0.930
#   pulse 9: activated nodes: 8935, borderline nodes: 3616, overall activation: 1151.574, activation diff: 163.594, ratio: 0.142
#   pulse 10: activated nodes: 8935, borderline nodes: 3616, overall activation: 1282.644, activation diff: 132.243, ratio: 0.103
#   pulse 11: activated nodes: 8935, borderline nodes: 3616, overall activation: 1282.605, activation diff: 1.263, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1282.6
#   number of spread. activ. pulses: 11
#   running time: 490

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994659   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98800135   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98319095   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96544117   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91037494   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88298583   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49903306   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49735755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4945247   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49217987   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49125105   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49015647   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4858958   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48437542   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48437512   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.48308164   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48181105   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.4810678   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.48009792   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.48009083   REFERENCES:SIMDATES
