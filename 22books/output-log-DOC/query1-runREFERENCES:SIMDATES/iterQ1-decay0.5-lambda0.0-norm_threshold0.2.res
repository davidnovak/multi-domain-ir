###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 682.198, activation diff: 710.461, ratio: 1.041
#   pulse 3: activated nodes: 8411, borderline nodes: 3562, overall activation: 92.378, activation diff: 773.466, ratio: 8.373
#   pulse 4: activated nodes: 8667, borderline nodes: 3756, overall activation: 1289.340, activation diff: 1362.456, ratio: 1.057
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 107.713, activation diff: 1351.668, ratio: 12.549
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1320.694, activation diff: 1324.715, ratio: 1.003
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 859.438, activation diff: 573.119, ratio: 0.667
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1374.954, activation diff: 518.991, ratio: 0.377
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1375.033, activation diff: 3.452, ratio: 0.003

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1375.0
#   number of spread. activ. pulses: 9
#   running time: 443

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99948967   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98913485   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9835589   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9648905   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91676456   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88832796   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49912313   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49760684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49496925   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49282974   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49195912   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.4910686   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48722085   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48584116   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48583853   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.48455083   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48351327   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.482838   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.48194456   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.48168182   REFERENCES:SIMDATES
