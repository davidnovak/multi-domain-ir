###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 18.951, activation diff: 9.454, ratio: 0.499
#   pulse 3: activated nodes: 6261, borderline nodes: 5235, overall activation: 44.875, activation diff: 26.688, ratio: 0.595
#   pulse 4: activated nodes: 6996, borderline nodes: 4899, overall activation: 103.004, activation diff: 58.349, ratio: 0.566
#   pulse 5: activated nodes: 7761, borderline nodes: 4634, overall activation: 194.726, activation diff: 91.743, ratio: 0.471
#   pulse 6: activated nodes: 8249, borderline nodes: 4223, overall activation: 309.779, activation diff: 115.052, ratio: 0.371
#   pulse 7: activated nodes: 8504, borderline nodes: 3897, overall activation: 435.291, activation diff: 125.512, ratio: 0.288
#   pulse 8: activated nodes: 8657, borderline nodes: 3739, overall activation: 560.222, activation diff: 124.931, ratio: 0.223
#   pulse 9: activated nodes: 8766, borderline nodes: 3658, overall activation: 676.951, activation diff: 116.729, ratio: 0.172
#   pulse 10: activated nodes: 8842, borderline nodes: 3638, overall activation: 781.321, activation diff: 104.369, ratio: 0.134
#   pulse 11: activated nodes: 8883, borderline nodes: 3629, overall activation: 871.867, activation diff: 90.546, ratio: 0.104
#   pulse 12: activated nodes: 8908, borderline nodes: 3621, overall activation: 948.762, activation diff: 76.895, ratio: 0.081
#   pulse 13: activated nodes: 8921, borderline nodes: 3618, overall activation: 1013.042, activation diff: 64.280, ratio: 0.063
#   pulse 14: activated nodes: 8926, borderline nodes: 3619, overall activation: 1066.140, activation diff: 53.098, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8926
#   final overall activation: 1066.1
#   number of spread. activ. pulses: 14
#   running time: 521

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97568446   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.93004674   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9210615   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9008415   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8487071   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.7779122   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.46931124   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.46429306   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.46077153   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.45870805   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4576637   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.4553213   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.45093626   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.4498686   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.44217685   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.44203055   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.44100183   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.44082406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.43865064   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   20   0.437377   REFERENCES:SIMDATES
