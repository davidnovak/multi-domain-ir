###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 251.145, activation diff: 235.585, ratio: 0.938
#   pulse 3: activated nodes: 7949, borderline nodes: 4188, overall activation: 554.132, activation diff: 303.506, ratio: 0.548
#   pulse 4: activated nodes: 10018, borderline nodes: 5304, overall activation: 1814.040, activation diff: 1259.908, ratio: 0.695
#   pulse 5: activated nodes: 11079, borderline nodes: 2985, overall activation: 3215.224, activation diff: 1401.184, ratio: 0.436
#   pulse 6: activated nodes: 11226, borderline nodes: 1253, overall activation: 4404.446, activation diff: 1189.222, ratio: 0.270
#   pulse 7: activated nodes: 11299, borderline nodes: 590, overall activation: 5257.516, activation diff: 853.071, ratio: 0.162
#   pulse 8: activated nodes: 11319, borderline nodes: 350, overall activation: 5824.758, activation diff: 567.242, ratio: 0.097
#   pulse 9: activated nodes: 11322, borderline nodes: 211, overall activation: 6187.607, activation diff: 362.849, ratio: 0.059
#   pulse 10: activated nodes: 11329, borderline nodes: 147, overall activation: 6414.320, activation diff: 226.713, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 6414.3
#   number of spread. activ. pulses: 10
#   running time: 593

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.997033   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9834009   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9816661   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9636134   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9112716   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8958657   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8954293   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.8947115   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   9   0.8945031   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.89429957   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   11   0.8942677   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   12   0.89424384   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.8941551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   14   0.8940991   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.89409894   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   16   0.89403343   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.8940176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.8939605   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.89392966   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.8939214   REFERENCES:SIMDATES
