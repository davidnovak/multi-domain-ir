###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1246.001, activation diff: 1275.944, ratio: 1.024
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 623.762, activation diff: 1861.303, ratio: 2.984
#   pulse 4: activated nodes: 10842, borderline nodes: 2716, overall activation: 3263.671, activation diff: 3784.918, ratio: 1.160
#   pulse 5: activated nodes: 11264, borderline nodes: 1034, overall activation: 1514.908, activation diff: 3801.915, ratio: 2.510
#   pulse 6: activated nodes: 11307, borderline nodes: 511, overall activation: 3789.828, activation diff: 3473.630, ratio: 0.917
#   pulse 7: activated nodes: 11324, borderline nodes: 448, overall activation: 4227.662, activation diff: 827.799, ratio: 0.196
#   pulse 8: activated nodes: 11324, borderline nodes: 447, overall activation: 4394.880, activation diff: 244.740, ratio: 0.056
#   pulse 9: activated nodes: 11325, borderline nodes: 446, overall activation: 4433.568, activation diff: 54.662, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11325
#   final overall activation: 4433.6
#   number of spread. activ. pulses: 9
#   running time: 605

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99962413   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9887779   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9738497   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92650616   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363604   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74997294   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.74980444   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7497788   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7497553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7496332   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7495944   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.749565   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.74953026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.7495   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.74949396   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.74947137   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.7494153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.74940026   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   20   0.7493949   REFERENCES:SIMDATES
