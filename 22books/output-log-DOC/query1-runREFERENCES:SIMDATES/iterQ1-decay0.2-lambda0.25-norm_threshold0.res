###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 935.522, activation diff: 930.079, ratio: 0.994
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1382.542, activation diff: 613.532, ratio: 0.444
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3395.512, activation diff: 2012.971, ratio: 0.593
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4565.941, activation diff: 1170.428, ratio: 0.256
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5088.108, activation diff: 522.167, ratio: 0.103
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5300.434, activation diff: 212.326, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5300.4
#   number of spread. activ. pulses: 7
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99880207   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9892322   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9878887   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97357404   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9303373   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9084681   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7988825   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7983546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7981189   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.79798055   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7979669   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.79785085   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.79783374   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.797826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.79778415   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   16   0.7977611   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.7977113   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   18   0.797698   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.79767954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   20   0.7976493   REFERENCES:SIMDATES
