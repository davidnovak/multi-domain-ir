###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 326.918, activation diff: 308.874, ratio: 0.945
#   pulse 3: activated nodes: 8467, borderline nodes: 3479, overall activation: 689.616, activation diff: 362.715, ratio: 0.526
#   pulse 4: activated nodes: 10588, borderline nodes: 3958, overall activation: 1769.193, activation diff: 1079.577, ratio: 0.610
#   pulse 5: activated nodes: 11202, borderline nodes: 1991, overall activation: 2848.422, activation diff: 1079.229, ratio: 0.379
#   pulse 6: activated nodes: 11279, borderline nodes: 710, overall activation: 3684.971, activation diff: 836.550, ratio: 0.227
#   pulse 7: activated nodes: 11310, borderline nodes: 423, overall activation: 4258.329, activation diff: 573.357, ratio: 0.135
#   pulse 8: activated nodes: 11321, borderline nodes: 285, overall activation: 4628.719, activation diff: 370.390, ratio: 0.080
#   pulse 9: activated nodes: 11325, borderline nodes: 207, overall activation: 4860.594, activation diff: 231.875, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11325
#   final overall activation: 4860.6
#   number of spread. activ. pulses: 9
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9953392   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98094994   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.97943354   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9617443   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91383463   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8854333   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7933279   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7921041   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.79137486   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7904328   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7903284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.7902216   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.79005325   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.79004633   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.7900263   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   16   0.7899916   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   17   0.78989   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.789721   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   19   0.7896825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   20   0.78964853   REFERENCES:SIMDATES
