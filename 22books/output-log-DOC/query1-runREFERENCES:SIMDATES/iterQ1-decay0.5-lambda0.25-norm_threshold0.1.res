###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 478.067, activation diff: 476.438, ratio: 0.997
#   pulse 3: activated nodes: 8746, borderline nodes: 3493, overall activation: 432.785, activation diff: 165.039, ratio: 0.381
#   pulse 4: activated nodes: 8916, borderline nodes: 3612, overall activation: 1044.488, activation diff: 611.773, ratio: 0.586
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1318.516, activation diff: 274.028, ratio: 0.208
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1418.712, activation diff: 100.196, ratio: 0.071
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1451.666, activation diff: 32.955, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1451.7
#   number of spread. activ. pulses: 7
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9983913   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.987767   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98489904   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9686052   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92228156   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89756507   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4982295   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49644703   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49435127   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4921952   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49140316   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.48978052   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48705366   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.4856918   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.4851048   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.4839227   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48361272   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.4819409   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.48174804   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.48118055   REFERENCES:SIMDATES
