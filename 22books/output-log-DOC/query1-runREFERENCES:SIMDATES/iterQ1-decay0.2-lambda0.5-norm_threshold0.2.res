###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 225.179, activation diff: 209.619, ratio: 0.931
#   pulse 3: activated nodes: 7949, borderline nodes: 4188, overall activation: 473.659, activation diff: 248.946, ratio: 0.526
#   pulse 4: activated nodes: 10001, borderline nodes: 5350, overall activation: 1413.145, activation diff: 939.486, ratio: 0.665
#   pulse 5: activated nodes: 11008, borderline nodes: 3116, overall activation: 2435.436, activation diff: 1022.291, ratio: 0.420
#   pulse 6: activated nodes: 11202, borderline nodes: 1624, overall activation: 3297.893, activation diff: 862.457, ratio: 0.262
#   pulse 7: activated nodes: 11270, borderline nodes: 832, overall activation: 3918.587, activation diff: 620.694, ratio: 0.158
#   pulse 8: activated nodes: 11301, borderline nodes: 606, overall activation: 4331.619, activation diff: 413.031, ratio: 0.095
#   pulse 9: activated nodes: 11309, borderline nodes: 479, overall activation: 4595.712, activation diff: 264.093, ratio: 0.057
#   pulse 10: activated nodes: 11318, borderline nodes: 400, overall activation: 4761.061, activation diff: 165.349, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4761.1
#   number of spread. activ. pulses: 10
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99701786   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9832998   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98141015   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9633738   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9111378   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88170946   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7962787   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7953192   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7950939   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.7944106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.7943009   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.7942568   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   13   0.79417986   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.794142   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.79413366   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   16   0.7941245   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.79411745   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.7940532   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.79389775   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   20   0.7937253   REFERENCES:SIMDATES
