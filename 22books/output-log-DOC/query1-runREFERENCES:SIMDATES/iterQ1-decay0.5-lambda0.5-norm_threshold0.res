###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 286.238, activation diff: 266.410, ratio: 0.931
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 491.130, activation diff: 204.891, ratio: 0.417
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 832.012, activation diff: 340.883, ratio: 0.410
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1103.720, activation diff: 271.707, ratio: 0.246
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1285.324, activation diff: 181.604, ratio: 0.141
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1397.744, activation diff: 112.420, ratio: 0.080
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1464.471, activation diff: 66.727, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1464.5
#   number of spread. activ. pulses: 8
#   running time: 427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99223286   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.97558725   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.97400004   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9549381   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9116709   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8818115   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49101952   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.48700738   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4866979   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4839905   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48297548   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.4791956   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.4774084   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.47735518   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.47575927   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.47361287   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.47311515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.47198516   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.4706716   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.4704588   REFERENCES:SIMDATES
