###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 72.669, activation diff: 61.596, ratio: 0.848
#   pulse 3: activated nodes: 8023, borderline nodes: 4134, overall activation: 195.864, activation diff: 123.649, ratio: 0.631
#   pulse 4: activated nodes: 10211, borderline nodes: 5040, overall activation: 486.689, activation diff: 290.848, ratio: 0.598
#   pulse 5: activated nodes: 10888, borderline nodes: 3580, overall activation: 921.626, activation diff: 434.938, ratio: 0.472
#   pulse 6: activated nodes: 11142, borderline nodes: 2310, overall activation: 1438.296, activation diff: 516.670, ratio: 0.359
#   pulse 7: activated nodes: 11228, borderline nodes: 1322, overall activation: 1970.627, activation diff: 532.331, ratio: 0.270
#   pulse 8: activated nodes: 11271, borderline nodes: 788, overall activation: 2475.228, activation diff: 504.601, ratio: 0.204
#   pulse 9: activated nodes: 11297, borderline nodes: 592, overall activation: 2929.832, activation diff: 454.605, ratio: 0.155
#   pulse 10: activated nodes: 11309, borderline nodes: 466, overall activation: 3326.406, activation diff: 396.574, ratio: 0.119
#   pulse 11: activated nodes: 11320, borderline nodes: 382, overall activation: 3665.066, activation diff: 338.659, ratio: 0.092
#   pulse 12: activated nodes: 11320, borderline nodes: 291, overall activation: 3950.031, activation diff: 284.966, ratio: 0.072
#   pulse 13: activated nodes: 11321, borderline nodes: 247, overall activation: 4187.259, activation diff: 237.228, ratio: 0.057
#   pulse 14: activated nodes: 11324, borderline nodes: 204, overall activation: 4383.166, activation diff: 195.907, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 4383.2
#   number of spread. activ. pulses: 14
#   running time: 592

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9839952   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9583713   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9579952   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9351421   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.88379836   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8464273   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.76981777   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.76727533   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.76531935   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.76415014   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.7637119   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.76170206   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.76162773   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.7615721   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   15   0.7615512   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.7612302   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   17   0.76095486   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.76090616   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.7606619   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7605283   REFERENCES:SIMDATES
