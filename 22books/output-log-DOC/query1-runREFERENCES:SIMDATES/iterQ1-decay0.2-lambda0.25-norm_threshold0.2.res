###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 580.880, activation diff: 583.093, ratio: 1.004
#   pulse 3: activated nodes: 8277, borderline nodes: 3745, overall activation: 687.334, activation diff: 517.317, ratio: 0.753
#   pulse 4: activated nodes: 10410, borderline nodes: 4334, overall activation: 2589.059, activation diff: 1968.530, ratio: 0.760
#   pulse 5: activated nodes: 11170, borderline nodes: 2438, overall activation: 3797.029, activation diff: 1210.676, ratio: 0.319
#   pulse 6: activated nodes: 11273, borderline nodes: 785, overall activation: 4488.088, activation diff: 691.059, ratio: 0.154
#   pulse 7: activated nodes: 11307, borderline nodes: 524, overall activation: 4804.094, activation diff: 316.006, ratio: 0.066
#   pulse 8: activated nodes: 11320, borderline nodes: 412, overall activation: 4938.135, activation diff: 134.041, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 4938.1
#   number of spread. activ. pulses: 8
#   running time: 590

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991647   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98832846   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98640823   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9697428   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91780925   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8919178   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79954857   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7993014   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.79920197   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.7991646   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7991327   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.79912865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.7991106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.79911   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.79910135   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7990302   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.799026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   18   0.7989669   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.7989522   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7989477   REFERENCES:SIMDATES
