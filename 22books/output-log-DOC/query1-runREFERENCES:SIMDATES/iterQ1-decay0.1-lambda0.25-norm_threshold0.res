###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1050.135, activation diff: 1044.691, ratio: 0.995
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1682.039, activation diff: 818.062, ratio: 0.486
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4393.554, activation diff: 2711.516, ratio: 0.617
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 6012.986, activation diff: 1619.431, ratio: 0.269
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6738.876, activation diff: 725.891, ratio: 0.108
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7033.002, activation diff: 294.126, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7033.0
#   number of spread. activ. pulses: 7
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988061   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98923266   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9880148   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9736848   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9303646   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9084958   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89876413   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8983367   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.8982551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.8979367   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.8979313   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   12   0.8978886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.897863   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.89784443   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.8978303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.8978156   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   17   0.8978064   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.8978013   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   19   0.89777   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   20   0.89773893   REFERENCES:SIMDATES
