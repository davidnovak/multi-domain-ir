###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 365.325, activation diff: 347.281, ratio: 0.951
#   pulse 3: activated nodes: 8467, borderline nodes: 3479, overall activation: 810.691, activation diff: 445.385, ratio: 0.549
#   pulse 4: activated nodes: 10597, borderline nodes: 3864, overall activation: 2265.038, activation diff: 1454.347, ratio: 0.642
#   pulse 5: activated nodes: 11210, borderline nodes: 1849, overall activation: 3746.139, activation diff: 1481.101, ratio: 0.395
#   pulse 6: activated nodes: 11295, borderline nodes: 496, overall activation: 4895.502, activation diff: 1149.363, ratio: 0.235
#   pulse 7: activated nodes: 11322, borderline nodes: 237, overall activation: 5682.152, activation diff: 786.650, ratio: 0.138
#   pulse 8: activated nodes: 11335, borderline nodes: 75, overall activation: 6189.679, activation diff: 507.527, ratio: 0.082
#   pulse 9: activated nodes: 11338, borderline nodes: 54, overall activation: 6506.605, activation diff: 316.926, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11338
#   final overall activation: 6506.6
#   number of spread. activ. pulses: 9
#   running time: 488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9953524   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98099434   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9796399   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96200633   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9139782   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.892551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8918431   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.89054275   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.8904686   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   10   0.89012617   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.88983583   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   12   0.88977265   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.88972497   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   14   0.889702   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   15   0.889581   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.8895783   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.88949776   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.88945687   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   19   0.8893543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   20   0.8892767   REFERENCES:SIMDATES
