###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1328.667, activation diff: 1358.609, ratio: 1.023
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 752.308, activation diff: 2071.937, ratio: 2.754
#   pulse 4: activated nodes: 10842, borderline nodes: 2657, overall activation: 3768.197, activation diff: 4387.718, ratio: 1.164
#   pulse 5: activated nodes: 11272, borderline nodes: 970, overall activation: 1927.161, activation diff: 4396.626, ratio: 2.281
#   pulse 6: activated nodes: 11322, borderline nodes: 210, overall activation: 4479.685, activation diff: 3945.710, ratio: 0.881
#   pulse 7: activated nodes: 11331, borderline nodes: 137, overall activation: 5017.478, activation diff: 947.044, ratio: 0.189
#   pulse 8: activated nodes: 11332, borderline nodes: 103, overall activation: 5194.700, activation diff: 262.255, ratio: 0.050
#   pulse 9: activated nodes: 11332, borderline nodes: 100, overall activation: 5238.137, activation diff: 60.147, ratio: 0.011

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 5238.1
#   number of spread. activ. pulses: 9
#   running time: 567

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996271   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98885775   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9738774   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92650735   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79998654   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.79992765   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.79988205   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.7998819   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.79985803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7998375   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.7998307   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.79981846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.79981214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.79980445   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7997896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.7997757   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.79975355   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.79974955   REFERENCES:SIMDATES
