###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 69.125, activation diff: 58.051, ratio: 0.840
#   pulse 3: activated nodes: 8023, borderline nodes: 4134, overall activation: 182.884, activation diff: 114.213, ratio: 0.625
#   pulse 4: activated nodes: 10200, borderline nodes: 5052, overall activation: 438.572, activation diff: 255.712, ratio: 0.583
#   pulse 5: activated nodes: 10824, borderline nodes: 3774, overall activation: 809.942, activation diff: 371.370, ratio: 0.459
#   pulse 6: activated nodes: 11121, borderline nodes: 2476, overall activation: 1244.894, activation diff: 434.953, ratio: 0.349
#   pulse 7: activated nodes: 11212, borderline nodes: 1535, overall activation: 1690.567, activation diff: 445.673, ratio: 0.264
#   pulse 8: activated nodes: 11235, borderline nodes: 928, overall activation: 2112.531, activation diff: 421.963, ratio: 0.200
#   pulse 9: activated nodes: 11284, borderline nodes: 687, overall activation: 2492.946, activation diff: 380.415, ratio: 0.153
#   pulse 10: activated nodes: 11299, borderline nodes: 616, overall activation: 2825.172, activation diff: 332.226, ratio: 0.118
#   pulse 11: activated nodes: 11303, borderline nodes: 572, overall activation: 3109.152, activation diff: 283.981, ratio: 0.091
#   pulse 12: activated nodes: 11313, borderline nodes: 555, overall activation: 3348.268, activation diff: 239.115, ratio: 0.071
#   pulse 13: activated nodes: 11313, borderline nodes: 520, overall activation: 3547.442, activation diff: 199.174, ratio: 0.056
#   pulse 14: activated nodes: 11313, borderline nodes: 514, overall activation: 3712.000, activation diff: 164.558, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11313
#   final overall activation: 3712.0
#   number of spread. activ. pulses: 14
#   running time: 630

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9839449   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9580809   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9576756   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9346657   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.88353246   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8458381   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.72129536   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.71798384   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.71672106   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7151681   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.71506107   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   12   0.71228224   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7120158   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.7119156   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   15   0.71158445   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   16   0.71155363   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.71137094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.711326   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   19   0.71103513   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   20   0.7110153   REFERENCES:SIMDATES
