###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1562.988, activation diff: 1594.424, ratio: 1.020
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 939.924, activation diff: 2424.453, ratio: 2.579
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4070.910, activation diff: 4519.653, ratio: 1.110
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4447.334, activation diff: 2057.289, ratio: 0.463
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5263.740, activation diff: 1068.264, ratio: 0.203
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 5407.169, activation diff: 183.692, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5407.2
#   number of spread. activ. pulses: 7
#   running time: 542

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996607   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9898848   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9762978   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.93326503   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124044   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79998785   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.7999351   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.79989403   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.7998933   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.79987353   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7998504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.79984164   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7998314   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7998293   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.7998194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7998106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.7997953   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.7997774   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.7997686   REFERENCES:SIMDATES
