###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 418.460, activation diff: 398.632, ratio: 0.953
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 870.692, activation diff: 452.233, ratio: 0.519
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1875.363, activation diff: 1004.670, ratio: 0.536
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2793.924, activation diff: 918.562, ratio: 0.329
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 3464.030, activation diff: 670.105, ratio: 0.193
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 3907.707, activation diff: 443.678, ratio: 0.114
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 4187.883, activation diff: 280.175, ratio: 0.067
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 4360.296, activation diff: 172.413, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4360.3
#   number of spread. activ. pulses: 9
#   running time: 506

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99595845   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9831877   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9820584   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96592015   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92207736   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8967501   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74431324   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74264026   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.74257845   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7413311   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.7412667   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.74125576   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   13   0.7409611   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.7409543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   15   0.74079305   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.7407874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   17   0.7407778   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   18   0.7407422   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.7407253   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   20   0.7406255   REFERENCES:SIMDATES
