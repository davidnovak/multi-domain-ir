###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 23.287, activation diff: 13.790, ratio: 0.592
#   pulse 3: activated nodes: 6261, borderline nodes: 5235, overall activation: 69.077, activation diff: 46.554, ratio: 0.674
#   pulse 4: activated nodes: 7667, borderline nodes: 5547, overall activation: 196.390, activation diff: 127.488, ratio: 0.649
#   pulse 5: activated nodes: 9239, borderline nodes: 5569, overall activation: 457.865, activation diff: 261.480, ratio: 0.571
#   pulse 6: activated nodes: 10196, borderline nodes: 4832, overall activation: 887.462, activation diff: 429.597, ratio: 0.484
#   pulse 7: activated nodes: 10759, borderline nodes: 3782, overall activation: 1453.494, activation diff: 566.032, ratio: 0.389
#   pulse 8: activated nodes: 11034, borderline nodes: 2818, overall activation: 2081.579, activation diff: 628.086, ratio: 0.302
#   pulse 9: activated nodes: 11164, borderline nodes: 2069, overall activation: 2709.520, activation diff: 627.941, ratio: 0.232
#   pulse 10: activated nodes: 11215, borderline nodes: 1449, overall activation: 3297.801, activation diff: 588.281, ratio: 0.178
#   pulse 11: activated nodes: 11258, borderline nodes: 1050, overall activation: 3826.500, activation diff: 528.698, ratio: 0.138
#   pulse 12: activated nodes: 11284, borderline nodes: 819, overall activation: 4288.680, activation diff: 462.180, ratio: 0.108
#   pulse 13: activated nodes: 11299, borderline nodes: 643, overall activation: 4685.228, activation diff: 396.548, ratio: 0.085
#   pulse 14: activated nodes: 11308, borderline nodes: 518, overall activation: 5020.946, activation diff: 335.718, ratio: 0.067
#   pulse 15: activated nodes: 11314, borderline nodes: 438, overall activation: 5302.514, activation diff: 281.568, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11314
#   final overall activation: 5302.5
#   number of spread. activ. pulses: 15
#   running time: 715

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9829431   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.94983006   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9454815   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9249376   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.8682461   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   6   0.8675702   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   7   0.8674395   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   8   0.86392075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8632014   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.861707   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.86145043   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   12   0.8610363   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   13   0.86045   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.8601667   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.8601285   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   16   0.8599465   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.85937846   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.85889846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.85887325   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.8587627   REFERENCES:SIMDATES
