###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 51.403, activation diff: 40.330, ratio: 0.785
#   pulse 3: activated nodes: 8023, borderline nodes: 4134, overall activation: 122.286, activation diff: 71.336, ratio: 0.583
#   pulse 4: activated nodes: 8573, borderline nodes: 3941, overall activation: 246.474, activation diff: 124.219, ratio: 0.504
#   pulse 5: activated nodes: 8902, borderline nodes: 3614, overall activation: 397.778, activation diff: 151.303, ratio: 0.380
#   pulse 6: activated nodes: 8930, borderline nodes: 3611, overall activation: 554.564, activation diff: 156.787, ratio: 0.283
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 702.908, activation diff: 148.344, ratio: 0.211
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 835.876, activation diff: 132.967, ratio: 0.159
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 951.070, activation diff: 115.194, ratio: 0.121
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1048.612, activation diff: 97.543, ratio: 0.093
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1129.880, activation diff: 81.268, ratio: 0.072
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1196.777, activation diff: 66.897, ratio: 0.056
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1251.336, activation diff: 54.559, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1251.3
#   number of spread. activ. pulses: 13
#   running time: 551

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9782757   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9451945   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9451415   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9184046   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.86895037   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.82413304   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.47005534   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.464545   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.46284652   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.46149194   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.45936787   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.45573145   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4523808   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.45088115   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.4448523   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.44442442   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.4443615   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.4433699   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.44326392   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.4431497   REFERENCES:SIMDATES
