###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 211.698, activation diff: 193.653, ratio: 0.915
#   pulse 3: activated nodes: 8467, borderline nodes: 3479, overall activation: 369.314, activation diff: 157.627, ratio: 0.427
#   pulse 4: activated nodes: 8723, borderline nodes: 3669, overall activation: 703.304, activation diff: 333.990, ratio: 0.475
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 983.722, activation diff: 280.417, ratio: 0.285
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1176.291, activation diff: 192.570, ratio: 0.164
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1297.331, activation diff: 121.039, ratio: 0.093
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1369.897, activation diff: 72.567, ratio: 0.053
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1412.140, activation diff: 42.242, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1412.1
#   number of spread. activ. pulses: 9
#   running time: 455

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9952572   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98057866   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9780252   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9594968   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91270876   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8830699   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49450022   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49142987   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4901753   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.48764884   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4866535   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.48248297   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48244137   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48075938   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.47881413   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.478264   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.4772358   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.4756521   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.47370845   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.4735908   REFERENCES:SIMDATES
