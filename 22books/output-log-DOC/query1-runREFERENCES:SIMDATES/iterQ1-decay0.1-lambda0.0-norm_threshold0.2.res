###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1223.539, activation diff: 1251.802, ratio: 1.023
#   pulse 3: activated nodes: 8411, borderline nodes: 3562, overall activation: 832.398, activation diff: 2054.661, ratio: 2.468
#   pulse 4: activated nodes: 10572, borderline nodes: 3500, overall activation: 4637.533, activation diff: 5339.716, ratio: 1.151
#   pulse 5: activated nodes: 11229, borderline nodes: 1675, overall activation: 2105.798, activation diff: 6038.897, ratio: 2.868
#   pulse 6: activated nodes: 11306, borderline nodes: 245, overall activation: 5484.824, activation diff: 5713.719, ratio: 1.042
#   pulse 7: activated nodes: 11331, borderline nodes: 145, overall activation: 6234.670, activation diff: 1756.607, ratio: 0.282
#   pulse 8: activated nodes: 11332, borderline nodes: 99, overall activation: 6679.703, activation diff: 625.081, ratio: 0.094
#   pulse 9: activated nodes: 11332, borderline nodes: 94, overall activation: 6768.446, activation diff: 127.878, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6768.4
#   number of spread. activ. pulses: 9
#   running time: 626

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995904   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98766464   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97119063   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91909105   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.8999963   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.89999133   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.89998746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   9   0.89997715   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   10   0.8999756   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.8999696   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   12   0.89996654   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.89996445   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.8999642   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.89996374   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   16   0.89996034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.89995867   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.8999556   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   19   0.8999528   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.89994824   REFERENCES:SIMDATES
