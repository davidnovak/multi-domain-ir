###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 832.674, activation diff: 862.616, ratio: 1.036
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 95.316, activation diff: 922.133, ratio: 9.674
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1384.954, activation diff: 1447.564, ratio: 1.045
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 175.176, activation diff: 1370.309, ratio: 7.822
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1445.359, activation diff: 1311.987, ratio: 0.908
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1406.947, activation diff: 80.290, ratio: 0.057
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1466.075, activation diff: 59.629, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1466.1
#   number of spread. activ. pulses: 8
#   running time: 624

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995917   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98828727   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97320443   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92640823   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9036144   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4992085   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49783573   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4955287   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4936006   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.492851   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.4919293   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48842287   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48717132   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.4871713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.48615855   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48505887   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.48444632   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.4837053   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.48364154   REFERENCES:SIMDATES
