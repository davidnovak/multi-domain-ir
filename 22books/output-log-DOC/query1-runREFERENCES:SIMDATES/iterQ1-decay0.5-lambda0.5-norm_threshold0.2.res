###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 147.280, activation diff: 131.720, ratio: 0.894
#   pulse 3: activated nodes: 7949, borderline nodes: 4188, overall activation: 262.013, activation diff: 115.037, ratio: 0.439
#   pulse 4: activated nodes: 8254, borderline nodes: 4381, overall activation: 571.268, activation diff: 309.256, ratio: 0.541
#   pulse 5: activated nodes: 8905, borderline nodes: 3636, overall activation: 852.674, activation diff: 281.406, ratio: 0.330
#   pulse 6: activated nodes: 8928, borderline nodes: 3613, overall activation: 1056.131, activation diff: 203.456, ratio: 0.193
#   pulse 7: activated nodes: 8929, borderline nodes: 3611, overall activation: 1187.581, activation diff: 131.451, ratio: 0.111
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1267.760, activation diff: 80.179, ratio: 0.063
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1315.005, activation diff: 47.245, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1315.0
#   number of spread. activ. pulses: 9
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9943463   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9766729   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9736221   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9532685   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9027394   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8671863   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49365866   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49000964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.48893556   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4861427   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48496622   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.48054755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.47908443   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.47860807   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.47651798   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.47507194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.4738338   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.4725181   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.47087425   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.47077978   REFERENCES:SIMDATES
