###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 212.196, activation diff: 196.636, ratio: 0.927
#   pulse 3: activated nodes: 7949, borderline nodes: 4188, overall activation: 435.166, activation diff: 223.409, ratio: 0.513
#   pulse 4: activated nodes: 9995, borderline nodes: 5377, overall activation: 1236.604, activation diff: 801.438, ratio: 0.648
#   pulse 5: activated nodes: 10967, borderline nodes: 3245, overall activation: 2093.506, activation diff: 856.901, ratio: 0.409
#   pulse 6: activated nodes: 11165, borderline nodes: 1883, overall activation: 2810.188, activation diff: 716.682, ratio: 0.255
#   pulse 7: activated nodes: 11227, borderline nodes: 1021, overall activation: 3325.392, activation diff: 515.205, ratio: 0.155
#   pulse 8: activated nodes: 11274, borderline nodes: 784, overall activation: 3668.441, activation diff: 343.049, ratio: 0.094
#   pulse 9: activated nodes: 11287, borderline nodes: 685, overall activation: 3887.524, activation diff: 219.083, ratio: 0.056
#   pulse 10: activated nodes: 11289, borderline nodes: 657, overall activation: 4024.217, activation diff: 136.693, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11289
#   final overall activation: 4024.2
#   number of spread. activ. pulses: 10
#   running time: 525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970083   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98323536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9812542   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9632054   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91105163   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88146126   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74646544   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.74521935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7449801   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.74424005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.74411976   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.74399924   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.74397933   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7438705   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.74383247   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7437813   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   17   0.7435701   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   18   0.74352026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.74340665   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7433439   REFERENCES:SIMDATES
