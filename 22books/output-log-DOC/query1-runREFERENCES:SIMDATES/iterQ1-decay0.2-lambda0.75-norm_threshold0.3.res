###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 22.203, activation diff: 12.706, ratio: 0.572
#   pulse 3: activated nodes: 6261, borderline nodes: 5235, overall activation: 62.950, activation diff: 41.511, ratio: 0.659
#   pulse 4: activated nodes: 7657, borderline nodes: 5540, overall activation: 170.331, activation diff: 107.565, ratio: 0.632
#   pulse 5: activated nodes: 9181, borderline nodes: 5617, overall activation: 376.781, activation diff: 206.458, ratio: 0.548
#   pulse 6: activated nodes: 10068, borderline nodes: 5035, overall activation: 696.718, activation diff: 319.937, ratio: 0.459
#   pulse 7: activated nodes: 10653, borderline nodes: 4209, overall activation: 1107.156, activation diff: 410.438, ratio: 0.371
#   pulse 8: activated nodes: 10923, borderline nodes: 3233, overall activation: 1560.336, activation diff: 453.179, ratio: 0.290
#   pulse 9: activated nodes: 11084, borderline nodes: 2561, overall activation: 2013.599, activation diff: 453.263, ratio: 0.225
#   pulse 10: activated nodes: 11157, borderline nodes: 1918, overall activation: 2440.133, activation diff: 426.534, ratio: 0.175
#   pulse 11: activated nodes: 11208, borderline nodes: 1471, overall activation: 2825.529, activation diff: 385.396, ratio: 0.136
#   pulse 12: activated nodes: 11227, borderline nodes: 1140, overall activation: 3164.298, activation diff: 338.768, ratio: 0.107
#   pulse 13: activated nodes: 11257, borderline nodes: 952, overall activation: 3456.198, activation diff: 291.901, ratio: 0.084
#   pulse 14: activated nodes: 11264, borderline nodes: 824, overall activation: 3704.161, activation diff: 247.963, ratio: 0.067
#   pulse 15: activated nodes: 11288, borderline nodes: 733, overall activation: 3912.585, activation diff: 208.424, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11288
#   final overall activation: 3912.6
#   number of spread. activ. pulses: 15
#   running time: 586

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9827101   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9486408   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9441291   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9235281   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.86663204   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8125084   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7708447   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7690693   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.7654761   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.76523125   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.7642163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.76118356   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   13   0.7611699   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.76097983   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   15   0.7604121   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   16   0.7601702   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   17   0.76014006   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   18   0.7597585   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.75966686   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.75960755   REFERENCES:SIMDATES
