###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 367.828, activation diff: 370.040, ratio: 1.006
#   pulse 3: activated nodes: 8277, borderline nodes: 3745, overall activation: 296.987, activation diff: 186.678, ratio: 0.629
#   pulse 4: activated nodes: 8545, borderline nodes: 3948, overall activation: 915.560, activation diff: 619.576, ratio: 0.677
#   pulse 5: activated nodes: 8929, borderline nodes: 3611, overall activation: 1201.470, activation diff: 285.923, ratio: 0.238
#   pulse 6: activated nodes: 8929, borderline nodes: 3611, overall activation: 1317.782, activation diff: 116.312, ratio: 0.088
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1357.994, activation diff: 40.212, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1358.0
#   number of spread. activ. pulses: 7
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978557   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9856386   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9816897   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.963779   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9134382   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88395774   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49787426   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4958369   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49357057   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49117592   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49028707   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.4879868   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4855904   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48407066   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48308304   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.48178887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.4815857   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.4794111   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   19   0.47931725   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.4784749   REFERENCES:SIMDATES
