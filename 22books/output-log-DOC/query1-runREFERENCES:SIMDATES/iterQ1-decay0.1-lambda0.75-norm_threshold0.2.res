###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 42.711, activation diff: 32.409, ratio: 0.759
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 125.333, activation diff: 83.218, ratio: 0.664
#   pulse 4: activated nodes: 9163, borderline nodes: 5846, overall activation: 355.539, activation diff: 230.258, ratio: 0.648
#   pulse 5: activated nodes: 10278, borderline nodes: 4736, overall activation: 773.651, activation diff: 418.112, ratio: 0.540
#   pulse 6: activated nodes: 10866, borderline nodes: 3622, overall activation: 1360.762, activation diff: 587.111, ratio: 0.431
#   pulse 7: activated nodes: 11117, borderline nodes: 2529, overall activation: 2030.350, activation diff: 669.588, ratio: 0.330
#   pulse 8: activated nodes: 11210, borderline nodes: 1619, overall activation: 2706.460, activation diff: 676.110, ratio: 0.250
#   pulse 9: activated nodes: 11238, borderline nodes: 994, overall activation: 3340.880, activation diff: 634.420, ratio: 0.190
#   pulse 10: activated nodes: 11291, borderline nodes: 698, overall activation: 3909.771, activation diff: 568.891, ratio: 0.146
#   pulse 11: activated nodes: 11307, borderline nodes: 504, overall activation: 4405.216, activation diff: 495.445, ratio: 0.112
#   pulse 12: activated nodes: 11315, borderline nodes: 418, overall activation: 4828.480, activation diff: 423.264, ratio: 0.088
#   pulse 13: activated nodes: 11321, borderline nodes: 304, overall activation: 5185.198, activation diff: 356.719, ratio: 0.069
#   pulse 14: activated nodes: 11321, borderline nodes: 241, overall activation: 5482.702, activation diff: 297.504, ratio: 0.054
#   pulse 15: activated nodes: 11322, borderline nodes: 208, overall activation: 5728.832, activation diff: 246.130, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11322
#   final overall activation: 5728.8
#   number of spread. activ. pulses: 15
#   running time: 602

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98603135   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9592749   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.95822155   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93632746   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.881611   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.872422   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.8713689   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   8   0.8684021   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.86788464   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.8674469   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.8665365   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.8661609   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.8661486   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   14   0.86557496   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.86546457   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   16   0.86545813   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.86538506   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.86496365   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.86486167   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.8646188   REFERENCES:SIMDATES
