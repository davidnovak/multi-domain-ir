###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 709.238, activation diff: 707.610, ratio: 0.998
#   pulse 3: activated nodes: 8746, borderline nodes: 3493, overall activation: 890.418, activation diff: 495.449, ratio: 0.556
#   pulse 4: activated nodes: 10811, borderline nodes: 3157, overall activation: 2569.380, activation diff: 1695.997, ratio: 0.660
#   pulse 5: activated nodes: 11233, borderline nodes: 1521, overall activation: 3585.510, activation diff: 1016.168, ratio: 0.283
#   pulse 6: activated nodes: 11293, borderline nodes: 573, overall activation: 4080.044, activation diff: 494.534, ratio: 0.121
#   pulse 7: activated nodes: 11318, borderline nodes: 471, overall activation: 4292.170, activation diff: 212.126, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4292.2
#   number of spread. activ. pulses: 7
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9984316   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98783743   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9858817   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9701518   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9229251   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8986965   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74864733   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.748019   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.7475782   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74745613   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7474021   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.74735105   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.74730766   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7472894   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7472069   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   16   0.74719775   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.74704665   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   18   0.7470401   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.74697924   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7469071   REFERENCES:SIMDATES
