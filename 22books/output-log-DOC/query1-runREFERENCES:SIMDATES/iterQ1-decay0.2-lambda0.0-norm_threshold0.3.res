###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 867.792, activation diff: 894.194, ratio: 1.030
#   pulse 3: activated nodes: 8131, borderline nodes: 3942, overall activation: 490.427, activation diff: 1357.836, ratio: 2.769
#   pulse 4: activated nodes: 10270, borderline nodes: 4535, overall activation: 3371.475, activation diff: 3806.320, ratio: 1.129
#   pulse 5: activated nodes: 11162, borderline nodes: 2416, overall activation: 1216.012, activation diff: 4402.645, ratio: 3.621
#   pulse 6: activated nodes: 11271, borderline nodes: 726, overall activation: 3826.690, activation diff: 4433.115, ratio: 1.158
#   pulse 7: activated nodes: 11303, borderline nodes: 591, overall activation: 4009.610, activation diff: 1768.978, ratio: 0.441
#   pulse 8: activated nodes: 11309, borderline nodes: 496, overall activation: 4693.088, activation diff: 940.366, ratio: 0.200
#   pulse 9: activated nodes: 11316, borderline nodes: 492, overall activation: 4810.891, activation diff: 173.823, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 4810.9
#   number of spread. activ. pulses: 9
#   running time: 496

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99953634   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9858518   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96809626   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9109479   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88353914   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79998326   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.7999072   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7998536   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.79985017   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   11   0.7998132   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7997846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.79978055   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7997639   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.79975694   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.79975265   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.79973084   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.79971886   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.79969233   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.7996761   REFERENCES:SIMDATES
