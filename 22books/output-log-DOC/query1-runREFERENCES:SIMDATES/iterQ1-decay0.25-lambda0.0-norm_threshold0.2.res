###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1020.536, activation diff: 1048.799, ratio: 1.028
#   pulse 3: activated nodes: 8411, borderline nodes: 3562, overall activation: 507.309, activation diff: 1526.631, ratio: 3.009
#   pulse 4: activated nodes: 10563, borderline nodes: 3662, overall activation: 3091.754, activation diff: 3540.354, ratio: 1.145
#   pulse 5: activated nodes: 11207, borderline nodes: 1887, overall activation: 1084.914, activation diff: 3938.779, ratio: 3.630
#   pulse 6: activated nodes: 11255, borderline nodes: 657, overall activation: 3443.496, activation diff: 3858.964, ratio: 1.121
#   pulse 7: activated nodes: 11285, borderline nodes: 576, overall activation: 3716.785, activation diff: 1304.901, ratio: 0.351
#   pulse 8: activated nodes: 11291, borderline nodes: 544, overall activation: 4161.139, activation diff: 608.564, ratio: 0.146
#   pulse 9: activated nodes: 11292, borderline nodes: 536, overall activation: 4239.343, activation diff: 108.944, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11292
#   final overall activation: 4239.3
#   number of spread. activ. pulses: 9
#   running time: 792

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995799   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9873365   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.971076   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9190805   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8940367   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74996984   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.74978244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.74974984   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7497243   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7495783   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7495461   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.7494956   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.74946415   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.7494354   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.7494217   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.74939597   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.74934536   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.7493275   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   20   0.74930644   REFERENCES:SIMDATES
