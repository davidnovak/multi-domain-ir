###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 651.898, activation diff: 654.110, ratio: 1.003
#   pulse 3: activated nodes: 8277, borderline nodes: 3745, overall activation: 844.471, activation diff: 654.534, ratio: 0.775
#   pulse 4: activated nodes: 10418, borderline nodes: 4244, overall activation: 3379.100, activation diff: 2625.723, ratio: 0.777
#   pulse 5: activated nodes: 11183, borderline nodes: 2305, overall activation: 5054.314, activation diff: 1679.747, ratio: 0.332
#   pulse 6: activated nodes: 11284, borderline nodes: 525, overall activation: 6017.701, activation diff: 963.387, ratio: 0.160
#   pulse 7: activated nodes: 11322, borderline nodes: 271, overall activation: 6459.384, activation diff: 441.683, ratio: 0.068
#   pulse 8: activated nodes: 11329, borderline nodes: 138, overall activation: 6648.413, activation diff: 189.029, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 6648.4
#   number of spread. activ. pulses: 8
#   running time: 476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991716   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98834735   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9865921   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96984744   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9178587   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89950716   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.8993238   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   8   0.8992803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.8992746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.89926106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.89924306   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.8992387   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.89923376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.8992282   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.8992187   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   16   0.8992088   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8991978   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   18   0.89918125   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   19   0.8991764   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   20   0.89917016   REFERENCES:SIMDATES
