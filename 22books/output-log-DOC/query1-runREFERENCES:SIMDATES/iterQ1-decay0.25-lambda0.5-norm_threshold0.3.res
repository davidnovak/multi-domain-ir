###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 139.211, activation diff: 126.437, ratio: 0.908
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 283.053, activation diff: 144.485, ratio: 0.510
#   pulse 4: activated nodes: 9222, borderline nodes: 5943, overall activation: 922.832, activation diff: 639.779, ratio: 0.693
#   pulse 5: activated nodes: 10561, borderline nodes: 4170, overall activation: 1690.924, activation diff: 768.093, ratio: 0.454
#   pulse 6: activated nodes: 10980, borderline nodes: 2925, overall activation: 2420.860, activation diff: 729.936, ratio: 0.302
#   pulse 7: activated nodes: 11145, borderline nodes: 1886, overall activation: 2983.655, activation diff: 562.795, ratio: 0.189
#   pulse 8: activated nodes: 11211, borderline nodes: 1211, overall activation: 3376.649, activation diff: 392.993, ratio: 0.116
#   pulse 9: activated nodes: 11251, borderline nodes: 938, overall activation: 3636.469, activation diff: 259.820, ratio: 0.071
#   pulse 10: activated nodes: 11264, borderline nodes: 788, overall activation: 3803.042, activation diff: 166.573, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11264
#   final overall activation: 3803.0
#   number of spread. activ. pulses: 10
#   running time: 504

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9963752   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9795958   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9778632   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95819473   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.90141547   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.86623317   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7459687   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7444145   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7443323   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7428027   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.742705   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.742679   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.7426673   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.7425393   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7424017   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.7422863   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.74222547   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   18   0.74219006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.7420856   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7419851   REFERENCES:SIMDATES
