###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 307.715, activation diff: 289.670, ratio: 0.941
#   pulse 3: activated nodes: 8467, borderline nodes: 3479, overall activation: 631.577, activation diff: 323.878, ratio: 0.513
#   pulse 4: activated nodes: 10585, borderline nodes: 4009, overall activation: 1546.942, activation diff: 915.364, ratio: 0.592
#   pulse 5: activated nodes: 11195, borderline nodes: 2075, overall activation: 2447.787, activation diff: 900.846, ratio: 0.368
#   pulse 6: activated nodes: 11249, borderline nodes: 814, overall activation: 3143.210, activation diff: 695.423, ratio: 0.221
#   pulse 7: activated nodes: 11273, borderline nodes: 574, overall activation: 3619.607, activation diff: 476.397, ratio: 0.132
#   pulse 8: activated nodes: 11288, borderline nodes: 519, overall activation: 3927.228, activation diff: 307.621, ratio: 0.078
#   pulse 9: activated nodes: 11288, borderline nodes: 504, overall activation: 4119.643, activation diff: 192.416, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11288
#   final overall activation: 4119.6
#   number of spread. activ. pulses: 9
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9953309   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9809191   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.97931087   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96157026   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9137411   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8852148   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7436957   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74195904   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.74172425   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.7402774   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7402626   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.7402338   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.73996127   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   14   0.73995554   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.73984814   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   16   0.7397977   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   17   0.73976797   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.73970616   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.73965764   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.73965555   REFERENCES:SIMDATES
