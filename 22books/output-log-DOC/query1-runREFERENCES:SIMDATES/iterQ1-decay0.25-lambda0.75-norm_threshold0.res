###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 118.776, activation diff: 106.968, ratio: 0.901
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 298.130, activation diff: 179.695, ratio: 0.603
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 646.753, activation diff: 348.627, ratio: 0.539
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1098.350, activation diff: 451.596, ratio: 0.411
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 1582.778, activation diff: 484.428, ratio: 0.306
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2050.682, activation diff: 467.905, ratio: 0.228
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 2476.115, activation diff: 425.433, ratio: 0.172
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 2848.832, activation diff: 372.716, ratio: 0.131
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 3167.566, activation diff: 318.735, ratio: 0.101
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 3435.670, activation diff: 268.104, ratio: 0.078
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 3658.546, activation diff: 222.875, ratio: 0.061
#   pulse 13: activated nodes: 11328, borderline nodes: 416, overall activation: 3842.219, activation diff: 183.673, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 3842.2
#   number of spread. activ. pulses: 13
#   running time: 563

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9811696   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.95624536   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9553447   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9324917   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8842123   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8494631   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.71450275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7105886   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7097482   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.7076881   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7076543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.70544803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7054342   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.70517015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   15   0.7047317   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   16   0.7046009   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.70452505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.70431525   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.70414937   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.70404834   REFERENCES:SIMDATES
