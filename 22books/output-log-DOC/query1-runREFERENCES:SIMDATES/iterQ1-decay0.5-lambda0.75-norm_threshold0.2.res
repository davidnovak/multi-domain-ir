###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 30.292, activation diff: 19.990, ratio: 0.660
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 75.033, activation diff: 45.337, ratio: 0.604
#   pulse 4: activated nodes: 7853, borderline nodes: 4707, overall activation: 166.025, activation diff: 91.072, ratio: 0.549
#   pulse 5: activated nodes: 8451, borderline nodes: 4021, overall activation: 290.677, activation diff: 124.651, ratio: 0.429
#   pulse 6: activated nodes: 8711, borderline nodes: 3738, overall activation: 431.188, activation diff: 140.512, ratio: 0.326
#   pulse 7: activated nodes: 8872, borderline nodes: 3656, overall activation: 572.355, activation diff: 141.166, ratio: 0.247
#   pulse 8: activated nodes: 8917, borderline nodes: 3620, overall activation: 704.144, activation diff: 131.789, ratio: 0.187
#   pulse 9: activated nodes: 8928, borderline nodes: 3613, overall activation: 821.540, activation diff: 117.397, ratio: 0.143
#   pulse 10: activated nodes: 8929, borderline nodes: 3611, overall activation: 922.950, activation diff: 101.409, ratio: 0.110
#   pulse 11: activated nodes: 8931, borderline nodes: 3611, overall activation: 1008.716, activation diff: 85.767, ratio: 0.085
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1080.150, activation diff: 71.434, ratio: 0.066
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1138.964, activation diff: 58.813, ratio: 0.052
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 1186.952, activation diff: 47.988, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1187.0
#   number of spread. activ. pulses: 14
#   running time: 526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98058707   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9450588   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9428657   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.91826653   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.866711   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8149742   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4741276   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.4689389   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.4664713   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.46589303   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.46347022   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.46005177   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.45649746   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.45502532   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.4483059   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.44816113   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.44720355   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.44686022   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.44675183   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.4467149   REFERENCES:SIMDATES
