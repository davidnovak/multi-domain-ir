###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 813.869, activation diff: 840.271, ratio: 1.032
#   pulse 3: activated nodes: 8131, borderline nodes: 3942, overall activation: 410.219, activation diff: 1223.709, ratio: 2.983
#   pulse 4: activated nodes: 10264, borderline nodes: 4587, overall activation: 2913.652, activation diff: 3280.441, ratio: 1.126
#   pulse 5: activated nodes: 11149, borderline nodes: 2485, overall activation: 951.891, activation diff: 3746.537, ratio: 3.936
#   pulse 6: activated nodes: 11235, borderline nodes: 899, overall activation: 3244.156, activation diff: 3799.984, ratio: 1.171
#   pulse 7: activated nodes: 11267, borderline nodes: 759, overall activation: 3177.041, activation diff: 1671.243, ratio: 0.526
#   pulse 8: activated nodes: 11270, borderline nodes: 689, overall activation: 3920.981, activation diff: 1017.996, ratio: 0.260
#   pulse 9: activated nodes: 11270, borderline nodes: 678, overall activation: 4041.454, activation diff: 178.915, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11270
#   final overall activation: 4041.5
#   number of spread. activ. pulses: 9
#   running time: 690

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995278   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9855877   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96790206   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.91091406   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8834972   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7499664   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7497574   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7497138   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7496865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.74950963   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.74949026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.7494054   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.7493762   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.7493593   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.7493337   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.74929   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   18   0.74926335   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.7492415   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   20   0.74918336   REFERENCES:SIMDATES
