###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 84.885, activation diff: 73.078, ratio: 0.861
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 186.657, activation diff: 102.112, ratio: 0.547
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 337.289, activation diff: 150.642, ratio: 0.447
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 505.456, activation diff: 168.167, ratio: 0.333
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 670.442, activation diff: 164.986, ratio: 0.246
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 821.197, activation diff: 150.755, ratio: 0.184
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 953.239, activation diff: 132.042, ratio: 0.139
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1065.789, activation diff: 112.550, ratio: 0.106
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1159.950, activation diff: 94.161, ratio: 0.081
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1237.669, activation diff: 77.719, ratio: 0.063
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1301.166, activation diff: 63.497, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1301.2
#   number of spread. activ. pulses: 12
#   running time: 495

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9747034   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9435318   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.94257796   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9158238   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.8686031   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.82788175   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.46407935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.45822638   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.4568361   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4549884   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.453152   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.44936517   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.44621307   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.4447746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.43991897   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.43845034   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.4383828   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.43768767   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.43754822   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.4373695   REFERENCES:SIMDATES
