###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 497.793, activation diff: 477.965, ratio: 0.960
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1127.359, activation diff: 629.566, ratio: 0.558
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2734.594, activation diff: 1607.235, ratio: 0.588
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4242.176, activation diff: 1507.582, ratio: 0.355
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5343.801, activation diff: 1101.625, ratio: 0.206
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 6071.095, activation diff: 727.294, ratio: 0.120
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 6528.088, activation diff: 456.994, ratio: 0.070
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 6807.699, activation diff: 279.611, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6807.7
#   number of spread. activ. pulses: 9
#   running time: 518

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959716   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98321044   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98229384   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9662267   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9222249   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89700854   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89327335   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8924818   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.89148486   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.89146644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.8908937   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   12   0.890777   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   13   0.8907075   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   14   0.8906547   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.8906003   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.89050823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   17   0.89050317   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.8903698   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   19   0.8903412   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.8903258   REFERENCES:SIMDATES
