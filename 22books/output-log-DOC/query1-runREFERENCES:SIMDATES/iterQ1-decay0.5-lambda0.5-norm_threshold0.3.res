###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 97.859, activation diff: 85.086, ratio: 0.869
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 176.737, activation diff: 79.368, ratio: 0.449
#   pulse 4: activated nodes: 7736, borderline nodes: 4829, overall activation: 440.702, activation diff: 263.965, ratio: 0.599
#   pulse 5: activated nodes: 8568, borderline nodes: 3805, overall activation: 707.230, activation diff: 266.528, ratio: 0.377
#   pulse 6: activated nodes: 8797, borderline nodes: 3662, overall activation: 919.390, activation diff: 212.160, ratio: 0.231
#   pulse 7: activated nodes: 8903, borderline nodes: 3627, overall activation: 1063.933, activation diff: 144.544, ratio: 0.136
#   pulse 8: activated nodes: 8922, borderline nodes: 3616, overall activation: 1154.901, activation diff: 90.968, ratio: 0.079
#   pulse 9: activated nodes: 8927, borderline nodes: 3614, overall activation: 1209.631, activation diff: 54.730, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8927
#   final overall activation: 1209.6
#   number of spread. activ. pulses: 9
#   running time: 427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9930474   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9690666   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9672979   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.944921   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.891036   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84548914   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4923169   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.48766103   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4873368   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.48433483   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.48280358   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.47840095   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.47599962   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.4738552   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.4733767   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.4703378   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   17   0.46926618   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.46852475   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.46763784   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.467604   REFERENCES:SIMDATES
