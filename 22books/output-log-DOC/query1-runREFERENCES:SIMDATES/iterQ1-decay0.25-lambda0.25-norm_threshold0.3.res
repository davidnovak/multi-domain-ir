###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 406.509, activation diff: 410.880, ratio: 1.011
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 411.522, activation diff: 392.600, ratio: 0.954
#   pulse 4: activated nodes: 9932, borderline nodes: 5340, overall activation: 1924.503, activation diff: 1588.063, ratio: 0.825
#   pulse 5: activated nodes: 10991, borderline nodes: 3244, overall activation: 2846.263, activation diff: 925.830, ratio: 0.325
#   pulse 6: activated nodes: 11129, borderline nodes: 1679, overall activation: 3503.247, activation diff: 656.983, ratio: 0.188
#   pulse 7: activated nodes: 11221, borderline nodes: 1055, overall activation: 3824.578, activation diff: 321.332, ratio: 0.084
#   pulse 8: activated nodes: 11270, borderline nodes: 822, overall activation: 3964.204, activation diff: 139.626, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11270
#   final overall activation: 3964.2
#   number of spread. activ. pulses: 8
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9989839   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98666   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98430145   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96604776   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9091711   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8795633   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74947554   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7491201   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.7489081   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.7488692   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7487134   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.74870116   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7485717   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.748571   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7484546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.74835706   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   17   0.74830234   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   18   0.7482954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.7482468   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.74821025   REFERENCES:SIMDATES
