###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 755.473, activation diff: 753.844, ratio: 0.998
#   pulse 3: activated nodes: 8746, borderline nodes: 3493, overall activation: 995.454, activation diff: 574.982, ratio: 0.578
#   pulse 4: activated nodes: 10813, borderline nodes: 3122, overall activation: 2974.694, activation diff: 2000.225, ratio: 0.672
#   pulse 5: activated nodes: 11240, borderline nodes: 1463, overall activation: 4201.050, activation diff: 1226.446, ratio: 0.292
#   pulse 6: activated nodes: 11301, borderline nodes: 386, overall activation: 4803.436, activation diff: 602.386, ratio: 0.125
#   pulse 7: activated nodes: 11325, borderline nodes: 203, overall activation: 5063.407, activation diff: 259.971, ratio: 0.051
#   pulse 8: activated nodes: 11331, borderline nodes: 138, overall activation: 5171.067, activation diff: 107.659, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5171.1
#   number of spread. activ. pulses: 8
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99932534   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9895541   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98804796   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9728581   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9255012   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9022521   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79963416   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7994089   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.7993109   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.7992534   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7992461   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.7992428   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.7992311   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7992142   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7992099   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.79916054   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.7991184   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   18   0.7991066   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.7990953   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   20   0.79906386   REFERENCES:SIMDATES
