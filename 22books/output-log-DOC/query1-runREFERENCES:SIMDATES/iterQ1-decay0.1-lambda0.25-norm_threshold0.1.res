###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 847.941, activation diff: 846.313, ratio: 0.998
#   pulse 3: activated nodes: 8746, borderline nodes: 3493, overall activation: 1217.207, activation diff: 745.673, ratio: 0.613
#   pulse 4: activated nodes: 10816, borderline nodes: 3025, overall activation: 3868.909, activation diff: 2681.199, ratio: 0.693
#   pulse 5: activated nodes: 11249, borderline nodes: 1348, overall activation: 5565.260, activation diff: 1696.618, ratio: 0.305
#   pulse 6: activated nodes: 11315, borderline nodes: 185, overall activation: 6404.784, activation diff: 839.524, ratio: 0.131
#   pulse 7: activated nodes: 11337, borderline nodes: 67, overall activation: 6765.880, activation diff: 361.095, ratio: 0.053
#   pulse 8: activated nodes: 11341, borderline nodes: 39, overall activation: 6913.848, activation diff: 147.969, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6913.8
#   number of spread. activ. pulses: 8
#   running time: 479

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99933   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98955566   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98819077   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.972932   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92552304   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9022938   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89960194   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.8994322   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   9   0.899372   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.899364   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.89935505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.89934415   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   13   0.8993343   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.89933383   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   15   0.8993194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.8993047   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.8993035   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   18   0.8992864   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.89928186   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.8992796   REFERENCES:SIMDATES
