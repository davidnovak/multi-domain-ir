###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1493.998, activation diff: 1523.940, ratio: 1.020
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 1028.517, activation diff: 2512.226, ratio: 2.443
#   pulse 4: activated nodes: 10842, borderline nodes: 2621, overall activation: 4870.661, activation diff: 5686.807, ratio: 1.168
#   pulse 5: activated nodes: 11276, borderline nodes: 921, overall activation: 2910.838, activation diff: 5633.068, ratio: 1.935
#   pulse 6: activated nodes: 11332, borderline nodes: 112, overall activation: 6033.109, activation diff: 4869.641, ratio: 0.807
#   pulse 7: activated nodes: 11341, borderline nodes: 46, overall activation: 6767.243, activation diff: 1160.624, ratio: 0.172
#   pulse 8: activated nodes: 11341, borderline nodes: 20, overall activation: 6954.667, activation diff: 289.320, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6954.7
#   number of spread. activ. pulses: 8
#   running time: 1525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99963135   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9890478   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9739045   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.92650795   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89999664   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.89999235   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.899989   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   10   0.8999796   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   11   0.8999783   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.8999729   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   13   0.89997005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.899969   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.8999685   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.89996755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.8999651   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   18   0.8999634   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.8999603   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.8999574   REFERENCES:SIMDATES
