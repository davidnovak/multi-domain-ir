###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 432.908, activation diff: 437.279, ratio: 1.010
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 461.830, activation diff: 442.185, ratio: 0.957
#   pulse 4: activated nodes: 9938, borderline nodes: 5304, overall activation: 2229.978, activation diff: 1859.708, ratio: 0.834
#   pulse 5: activated nodes: 11032, borderline nodes: 3131, overall activation: 3342.896, activation diff: 1119.535, ratio: 0.335
#   pulse 6: activated nodes: 11174, borderline nodes: 1367, overall activation: 4141.624, activation diff: 798.728, ratio: 0.193
#   pulse 7: activated nodes: 11275, borderline nodes: 859, overall activation: 4533.534, activation diff: 391.910, ratio: 0.086
#   pulse 8: activated nodes: 11298, borderline nodes: 612, overall activation: 4704.686, activation diff: 171.152, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11298
#   final overall activation: 4704.7
#   number of spread. activ. pulses: 8
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9989912   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9867067   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98445857   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9661599   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9092263   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8797358   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7994632   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7991893   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.7990836   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.7990712   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.79899544   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7989849   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7989751   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7989603   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7989495   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   16   0.79893476   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   17   0.7988584   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   18   0.79882073   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.7988153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.79880697   REFERENCES:SIMDATES
