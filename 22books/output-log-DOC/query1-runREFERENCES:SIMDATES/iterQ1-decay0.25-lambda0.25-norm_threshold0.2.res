###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 545.372, activation diff: 547.584, ratio: 1.004
#   pulse 3: activated nodes: 8277, borderline nodes: 3745, overall activation: 613.486, activation diff: 453.426, ratio: 0.739
#   pulse 4: activated nodes: 10402, borderline nodes: 4374, overall activation: 2235.468, activation diff: 1676.990, ratio: 0.750
#   pulse 5: activated nodes: 11157, borderline nodes: 2530, overall activation: 3236.802, activation diff: 1002.853, ratio: 0.310
#   pulse 6: activated nodes: 11237, borderline nodes: 957, overall activation: 3803.875, activation diff: 567.072, ratio: 0.149
#   pulse 7: activated nodes: 11270, borderline nodes: 664, overall activation: 4062.153, activation diff: 258.279, ratio: 0.064
#   pulse 8: activated nodes: 11279, borderline nodes: 592, overall activation: 4170.799, activation diff: 108.646, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11279
#   final overall activation: 4170.8
#   number of spread. activ. pulses: 8
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99915993   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98831564   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9863041   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9696655   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9177764   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8918218   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7495591   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.7492434   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.7490789   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   10   0.74901223   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.74891174   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.74888074   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.74881876   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.7488034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.74868935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7486526   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.7485971   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   18   0.74859303   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.7485894   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   20   0.7485011   REFERENCES:SIMDATES
