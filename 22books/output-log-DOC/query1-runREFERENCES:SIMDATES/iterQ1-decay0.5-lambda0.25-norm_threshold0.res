###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 591.685, activation diff: 586.242, ratio: 0.991
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 601.507, activation diff: 116.087, ratio: 0.193
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1171.582, activation diff: 570.075, ratio: 0.487
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1422.078, activation diff: 250.496, ratio: 0.176
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1510.794, activation diff: 88.716, ratio: 0.059
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1539.456, activation diff: 28.662, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1539.5
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9987714   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.98921895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.98721886   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97243035   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9299511   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9080298   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49851185   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49691585   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49500734   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49306658   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49235544   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49101493   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.48836654   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.48712566   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.48668218   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.48575577   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.48524433   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.48385644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.48367882   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.4831925   REFERENCES:SIMDATES
