###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 975.638, activation diff: 1002.040, ratio: 1.027
#   pulse 3: activated nodes: 8131, borderline nodes: 3942, overall activation: 665.236, activation diff: 1640.485, ratio: 2.466
#   pulse 4: activated nodes: 10277, borderline nodes: 4453, overall activation: 4389.400, activation diff: 4964.971, ratio: 1.131
#   pulse 5: activated nodes: 11175, borderline nodes: 2295, overall activation: 1859.527, activation diff: 5827.213, ratio: 3.134
#   pulse 6: activated nodes: 11285, borderline nodes: 488, overall activation: 5183.126, activation diff: 5741.596, ratio: 1.108
#   pulse 7: activated nodes: 11318, borderline nodes: 345, overall activation: 5771.271, activation diff: 2039.403, ratio: 0.353
#   pulse 8: activated nodes: 11322, borderline nodes: 220, overall activation: 6413.175, activation diff: 882.742, ratio: 0.138
#   pulse 9: activated nodes: 11323, borderline nodes: 201, overall activation: 6525.149, activation diff: 176.253, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 6525.1
#   number of spread. activ. pulses: 9
#   running time: 519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999545   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9861644   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96819776   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9109598   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.89999586   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.8999902   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.8999856   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   9   0.8999745   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   10   0.8999725   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   11   0.89996564   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   12   0.89996266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.89995956   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.8999595   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   15   0.8999593   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   16   0.8999553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.8999532   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.8999501   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   19   0.89994746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.89994204   REFERENCES:SIMDATES
