###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 39.606, activation diff: 29.304, ratio: 0.740
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 112.220, activation diff: 73.210, ratio: 0.652
#   pulse 4: activated nodes: 9150, borderline nodes: 5853, overall activation: 299.070, activation diff: 186.908, ratio: 0.625
#   pulse 5: activated nodes: 10205, borderline nodes: 4902, overall activation: 617.793, activation diff: 318.723, ratio: 0.516
#   pulse 6: activated nodes: 10758, borderline nodes: 3953, overall activation: 1049.225, activation diff: 431.432, ratio: 0.411
#   pulse 7: activated nodes: 11035, borderline nodes: 2850, overall activation: 1536.063, activation diff: 486.838, ratio: 0.317
#   pulse 8: activated nodes: 11161, borderline nodes: 2060, overall activation: 2027.118, activation diff: 491.055, ratio: 0.242
#   pulse 9: activated nodes: 11222, borderline nodes: 1380, overall activation: 2489.608, activation diff: 462.490, ratio: 0.186
#   pulse 10: activated nodes: 11260, borderline nodes: 985, overall activation: 2906.370, activation diff: 416.763, ratio: 0.143
#   pulse 11: activated nodes: 11279, borderline nodes: 776, overall activation: 3271.021, activation diff: 364.651, ratio: 0.111
#   pulse 12: activated nodes: 11293, borderline nodes: 653, overall activation: 3583.630, activation diff: 312.609, ratio: 0.087
#   pulse 13: activated nodes: 11304, borderline nodes: 581, overall activation: 3847.769, activation diff: 264.140, ratio: 0.069
#   pulse 14: activated nodes: 11308, borderline nodes: 509, overall activation: 4068.602, activation diff: 220.832, ratio: 0.054
#   pulse 15: activated nodes: 11316, borderline nodes: 448, overall activation: 4251.858, activation diff: 183.256, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 4251.9
#   number of spread. activ. pulses: 15
#   running time: 599

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98590446   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.958547   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9575112   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9353927   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.88108087   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8390936   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7747877   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7728136   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.7701816   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.76990545   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   11   0.7693625   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   12   0.7669761   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   13   0.76680386   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7663844   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.76631975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.7658421   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.7657824   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.76571834   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   19   0.7656163   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   20   0.76556975   REFERENCES:SIMDATES
