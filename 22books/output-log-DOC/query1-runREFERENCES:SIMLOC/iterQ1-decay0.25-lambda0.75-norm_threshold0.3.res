###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 21.769, activation diff: 12.272, ratio: 0.564
#   pulse 3: activated nodes: 7900, borderline nodes: 6874, overall activation: 60.969, activation diff: 39.954, ratio: 0.655
#   pulse 4: activated nodes: 9004, borderline nodes: 6855, overall activation: 164.332, activation diff: 103.542, ratio: 0.630
#   pulse 5: activated nodes: 10470, borderline nodes: 6826, overall activation: 369.080, activation diff: 204.751, ratio: 0.555
#   pulse 6: activated nodes: 10999, borderline nodes: 5597, overall activation: 706.521, activation diff: 337.441, ratio: 0.478
#   pulse 7: activated nodes: 11208, borderline nodes: 4133, overall activation: 1177.043, activation diff: 470.522, ratio: 0.400
#   pulse 8: activated nodes: 11314, borderline nodes: 2538, overall activation: 1740.055, activation diff: 563.012, ratio: 0.324
#   pulse 9: activated nodes: 11376, borderline nodes: 1408, overall activation: 2335.758, activation diff: 595.704, ratio: 0.255
#   pulse 10: activated nodes: 11401, borderline nodes: 849, overall activation: 2900.231, activation diff: 564.473, ratio: 0.195
#   pulse 11: activated nodes: 11416, borderline nodes: 546, overall activation: 3398.512, activation diff: 498.281, ratio: 0.147
#   pulse 12: activated nodes: 11422, borderline nodes: 365, overall activation: 3823.316, activation diff: 424.804, ratio: 0.111
#   pulse 13: activated nodes: 11425, borderline nodes: 266, overall activation: 4177.822, activation diff: 354.506, ratio: 0.085
#   pulse 14: activated nodes: 11429, borderline nodes: 204, overall activation: 4469.424, activation diff: 291.602, ratio: 0.065
#   pulse 15: activated nodes: 11436, borderline nodes: 181, overall activation: 4706.945, activation diff: 237.522, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 4706.9
#   number of spread. activ. pulses: 15
#   running time: 1618

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98265696   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9514203   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9427521   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9273989   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8663527   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8153542   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7224612   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72082275   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7188258   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7168475   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.71543676   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.714952   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.71481013   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.7147952   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7141157   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.713766   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.7129648   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.71294296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   19   0.71290195   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.7128385   REFERENCES:SIMLOC
