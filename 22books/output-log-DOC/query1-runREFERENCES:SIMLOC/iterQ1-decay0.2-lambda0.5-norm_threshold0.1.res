###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 336.359, activation diff: 318.137, ratio: 0.946
#   pulse 3: activated nodes: 9486, borderline nodes: 4057, overall activation: 781.072, activation diff: 444.719, ratio: 0.569
#   pulse 4: activated nodes: 11269, borderline nodes: 4020, overall activation: 2272.202, activation diff: 1491.130, ratio: 0.656
#   pulse 5: activated nodes: 11407, borderline nodes: 743, overall activation: 3828.568, activation diff: 1556.366, ratio: 0.407
#   pulse 6: activated nodes: 11438, borderline nodes: 199, overall activation: 4961.749, activation diff: 1133.181, ratio: 0.228
#   pulse 7: activated nodes: 11448, borderline nodes: 78, overall activation: 5680.250, activation diff: 718.501, ratio: 0.126
#   pulse 8: activated nodes: 11449, borderline nodes: 42, overall activation: 6112.231, activation diff: 431.982, ratio: 0.071
#   pulse 9: activated nodes: 11452, borderline nodes: 31, overall activation: 6365.643, activation diff: 253.412, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6365.6
#   number of spread. activ. pulses: 9
#   running time: 1384

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9953948   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9813415   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9807544   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9634278   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9139441   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88639355   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7934123   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79248655   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.79155517   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7910174   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.7907855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79077405   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.790751   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.79072666   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   15   0.79054415   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.7903489   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.79025114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.7900638   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.790004   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.7899966   REFERENCES:SIMLOC
