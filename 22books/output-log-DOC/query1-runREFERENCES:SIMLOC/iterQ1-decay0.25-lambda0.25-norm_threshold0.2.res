###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 557.744, activation diff: 557.741, ratio: 1.000
#   pulse 3: activated nodes: 9304, borderline nodes: 4485, overall activation: 760.856, activation diff: 512.681, ratio: 0.674
#   pulse 4: activated nodes: 11224, borderline nodes: 4855, overall activation: 3080.626, activation diff: 2335.322, ratio: 0.758
#   pulse 5: activated nodes: 11393, borderline nodes: 1072, overall activation: 4600.278, activation diff: 1519.678, ratio: 0.330
#   pulse 6: activated nodes: 11425, borderline nodes: 295, overall activation: 5337.968, activation diff: 737.691, ratio: 0.138
#   pulse 7: activated nodes: 11437, borderline nodes: 133, overall activation: 5627.460, activation diff: 289.492, ratio: 0.051
#   pulse 8: activated nodes: 11438, borderline nodes: 106, overall activation: 5735.295, activation diff: 107.834, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5735.3
#   number of spread. activ. pulses: 8
#   running time: 1253

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991955   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98828065   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98817754   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9703927   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91785866   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.892489   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7495762   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7491495   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.74913305   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.749108   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.74906325   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.749035   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7490318   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.74899596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.7489801   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.74896777   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7489594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.7489139   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.748875   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7488601   REFERENCES:SIMLOC
