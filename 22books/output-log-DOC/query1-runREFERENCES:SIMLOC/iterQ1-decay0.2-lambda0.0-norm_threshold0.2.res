###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1119.259, activation diff: 1145.283, ratio: 1.023
#   pulse 3: activated nodes: 9398, borderline nodes: 4153, overall activation: 846.881, activation diff: 1897.814, ratio: 2.241
#   pulse 4: activated nodes: 11277, borderline nodes: 3885, overall activation: 5196.379, activation diff: 5040.147, ratio: 0.970
#   pulse 5: activated nodes: 11409, borderline nodes: 566, overall activation: 5243.643, activation diff: 1986.849, ratio: 0.379
#   pulse 6: activated nodes: 11443, borderline nodes: 114, overall activation: 6456.423, activation diff: 1286.019, ratio: 0.199
#   pulse 7: activated nodes: 11449, borderline nodes: 52, overall activation: 6547.900, activation diff: 101.663, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6547.9
#   number of spread. activ. pulses: 7
#   running time: 1362

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995927   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98913115   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121996   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9190913   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89403844   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79998255   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999465   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.79988784   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.7998817   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.79988164   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7998695   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.79986787   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7998523   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.79984874   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7998381   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7998232   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.7998175   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.79980797   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   20   0.79979986   REFERENCES:SIMLOC
