###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 52.036, activation diff: 40.963, ratio: 0.787
#   pulse 3: activated nodes: 9078, borderline nodes: 5009, overall activation: 128.861, activation diff: 77.270, ratio: 0.600
#   pulse 4: activated nodes: 10454, borderline nodes: 5218, overall activation: 284.396, activation diff: 155.562, ratio: 0.547
#   pulse 5: activated nodes: 10719, borderline nodes: 4326, overall activation: 505.896, activation diff: 221.499, ratio: 0.438
#   pulse 6: activated nodes: 10842, borderline nodes: 3886, overall activation: 770.414, activation diff: 264.519, ratio: 0.343
#   pulse 7: activated nodes: 10871, borderline nodes: 3663, overall activation: 1054.222, activation diff: 283.807, ratio: 0.269
#   pulse 8: activated nodes: 10886, borderline nodes: 3581, overall activation: 1335.936, activation diff: 281.714, ratio: 0.211
#   pulse 9: activated nodes: 10889, borderline nodes: 3540, overall activation: 1595.962, activation diff: 260.026, ratio: 0.163
#   pulse 10: activated nodes: 10892, borderline nodes: 3526, overall activation: 1823.032, activation diff: 227.071, ratio: 0.125
#   pulse 11: activated nodes: 10896, borderline nodes: 3517, overall activation: 2014.804, activation diff: 191.772, ratio: 0.095
#   pulse 12: activated nodes: 10896, borderline nodes: 3517, overall activation: 2173.709, activation diff: 158.904, ratio: 0.073
#   pulse 13: activated nodes: 10897, borderline nodes: 3516, overall activation: 2303.794, activation diff: 130.086, ratio: 0.056
#   pulse 14: activated nodes: 10897, borderline nodes: 3515, overall activation: 2409.375, activation diff: 105.581, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2409.4
#   number of spread. activ. pulses: 14
#   running time: 1409

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9836288   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95789427   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9555089   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9361387   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88163424   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84498054   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4770438   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4754101   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4739357   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.47328228   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.47028893   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.469585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.46880648   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.468745   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.46827897   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.4676141   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.46732062   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.46674824   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.46659085   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.4660088   REFERENCES:SIMLOC
