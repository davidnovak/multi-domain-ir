###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 140.386, activation diff: 127.153, ratio: 0.906
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 300.512, activation diff: 160.613, ratio: 0.534
#   pulse 4: activated nodes: 10769, borderline nodes: 7387, overall activation: 1057.631, activation diff: 757.118, ratio: 0.716
#   pulse 5: activated nodes: 11185, borderline nodes: 3837, overall activation: 2120.343, activation diff: 1062.713, ratio: 0.501
#   pulse 6: activated nodes: 11338, borderline nodes: 1884, overall activation: 3269.266, activation diff: 1148.923, ratio: 0.351
#   pulse 7: activated nodes: 11411, borderline nodes: 686, overall activation: 4158.040, activation diff: 888.774, ratio: 0.214
#   pulse 8: activated nodes: 11422, borderline nodes: 332, overall activation: 4739.420, activation diff: 581.380, ratio: 0.123
#   pulse 9: activated nodes: 11433, borderline nodes: 211, overall activation: 5096.861, activation diff: 357.441, ratio: 0.070
#   pulse 10: activated nodes: 11436, borderline nodes: 180, overall activation: 5310.425, activation diff: 213.564, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 5310.4
#   number of spread. activ. pulses: 10
#   running time: 1434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9964255   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98029554   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9793893   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96004057   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9015055   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.867903   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74600625   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74495614   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.74401915   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7438485   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7436712   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7435542   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.743448   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.7434094   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7433752   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.74312985   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7430028   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7428895   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   19   0.74270046   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.7425419   REFERENCES:SIMLOC
