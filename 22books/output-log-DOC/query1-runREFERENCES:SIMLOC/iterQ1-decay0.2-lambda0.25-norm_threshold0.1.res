###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 784.454, activation diff: 780.628, ratio: 0.995
#   pulse 3: activated nodes: 9816, borderline nodes: 3919, overall activation: 1267.645, activation diff: 702.064, ratio: 0.554
#   pulse 4: activated nodes: 11319, borderline nodes: 2998, overall activation: 4142.926, activation diff: 2876.953, ratio: 0.694
#   pulse 5: activated nodes: 11421, borderline nodes: 428, overall activation: 5720.127, activation diff: 1577.207, ratio: 0.276
#   pulse 6: activated nodes: 11446, borderline nodes: 84, overall activation: 6371.816, activation diff: 651.689, ratio: 0.102
#   pulse 7: activated nodes: 11451, borderline nodes: 35, overall activation: 6613.271, activation diff: 241.454, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 6613.3
#   number of spread. activ. pulses: 7
#   running time: 1364

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985174   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98783433   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98767716   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714824   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9231244   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8993237   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7986796   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79815394   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.79797435   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.79781777   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.7977854   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.7977383   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.79769564   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.79767936   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.7976489   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.7976479   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.797564   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.7975479   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.7975471   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   20   0.79749227   REFERENCES:SIMLOC
