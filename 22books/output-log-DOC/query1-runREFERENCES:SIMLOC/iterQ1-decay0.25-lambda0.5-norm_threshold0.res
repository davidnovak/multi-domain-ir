###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 445.853, activation diff: 426.024, ratio: 0.956
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1059.116, activation diff: 613.264, ratio: 0.579
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2581.312, activation diff: 1522.196, ratio: 0.590
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3936.132, activation diff: 1354.820, ratio: 0.344
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 4853.495, activation diff: 917.363, ratio: 0.189
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 5419.040, activation diff: 565.545, ratio: 0.104
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 5754.194, activation diff: 335.154, ratio: 0.058
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 5948.865, activation diff: 194.672, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5948.9
#   number of spread. activ. pulses: 9
#   running time: 1313

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99600387   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9837454   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9830216   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9672924   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9221965   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8974285   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.744382   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7431921   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.74272203   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7420907   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7419232   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.74192035   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.74187183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.7416092   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.74158084   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.7414688   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.74130166   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   18   0.74127525   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.7412406   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7411975   REFERENCES:SIMLOC
