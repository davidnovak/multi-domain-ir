###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 80.801, activation diff: 69.727, ratio: 0.863
#   pulse 3: activated nodes: 9078, borderline nodes: 5009, overall activation: 232.647, activation diff: 152.291, ratio: 0.655
#   pulse 4: activated nodes: 11094, borderline nodes: 5418, overall activation: 662.950, activation diff: 430.321, ratio: 0.649
#   pulse 5: activated nodes: 11310, borderline nodes: 2458, overall activation: 1417.797, activation diff: 754.847, ratio: 0.532
#   pulse 6: activated nodes: 11404, borderline nodes: 984, overall activation: 2381.934, activation diff: 964.137, ratio: 0.405
#   pulse 7: activated nodes: 11429, borderline nodes: 348, overall activation: 3369.532, activation diff: 987.598, ratio: 0.293
#   pulse 8: activated nodes: 11445, borderline nodes: 135, overall activation: 4267.223, activation diff: 897.691, ratio: 0.210
#   pulse 9: activated nodes: 11448, borderline nodes: 68, overall activation: 5038.788, activation diff: 771.565, ratio: 0.153
#   pulse 10: activated nodes: 11448, borderline nodes: 47, overall activation: 5682.858, activation diff: 644.070, ratio: 0.113
#   pulse 11: activated nodes: 11450, borderline nodes: 31, overall activation: 6211.260, activation diff: 528.403, ratio: 0.085
#   pulse 12: activated nodes: 11452, borderline nodes: 17, overall activation: 6639.786, activation diff: 428.526, ratio: 0.065
#   pulse 13: activated nodes: 11454, borderline nodes: 13, overall activation: 6984.489, activation diff: 344.703, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6984.5
#   number of spread. activ. pulses: 13
#   running time: 1440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9790059   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95177734   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9489036   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9279855   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.87283057   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.85584176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8539672   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.85071826   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8493506   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8490575   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.84867966   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.847809   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.84759945   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.8474246   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.84740126   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.84688216   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.8468164   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.8466269   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.84656876   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.84624636   REFERENCES:SIMLOC
