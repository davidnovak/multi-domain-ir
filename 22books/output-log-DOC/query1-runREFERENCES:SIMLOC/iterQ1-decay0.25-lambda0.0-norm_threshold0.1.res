###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1302.107, activation diff: 1329.016, ratio: 1.021
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1023.213, activation diff: 2136.845, ratio: 2.088
#   pulse 4: activated nodes: 11362, borderline nodes: 2533, overall activation: 5038.085, activation diff: 4583.593, ratio: 0.910
#   pulse 5: activated nodes: 11431, borderline nodes: 295, overall activation: 5441.506, activation diff: 1103.992, ratio: 0.203
#   pulse 6: activated nodes: 11443, borderline nodes: 90, overall activation: 5945.975, activation diff: 526.081, ratio: 0.088
#   pulse 7: activated nodes: 11448, borderline nodes: 59, overall activation: 5989.175, activation diff: 45.824, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5989.2
#   number of spread. activ. pulses: 7
#   running time: 1285

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996331   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99016404   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.973923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92650795   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74996746   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74985754   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7498138   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.74974406   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7497437   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.74974114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7497344   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.749711   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.74968296   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   16   0.74966156   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7496512   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.749616   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7495978   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   20   0.74958885   REFERENCES:SIMLOC
