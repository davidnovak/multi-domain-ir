###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 827.689, activation diff: 852.586, ratio: 1.030
#   pulse 3: activated nodes: 9179, borderline nodes: 4782, overall activation: 539.262, activation diff: 1340.414, ratio: 2.486
#   pulse 4: activated nodes: 11200, borderline nodes: 5298, overall activation: 4149.170, activation diff: 4163.894, ratio: 1.004
#   pulse 5: activated nodes: 11388, borderline nodes: 1086, overall activation: 3833.818, activation diff: 2343.788, ratio: 0.611
#   pulse 6: activated nodes: 11424, borderline nodes: 312, overall activation: 5486.936, activation diff: 1772.514, ratio: 0.323
#   pulse 7: activated nodes: 11436, borderline nodes: 159, overall activation: 5613.035, activation diff: 146.514, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 5613.0
#   number of spread. activ. pulses: 7
#   running time: 1259

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99953645   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98791236   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682412   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91095185   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88354325   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7499597   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74982023   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7497295   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7496786   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7496661   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.7496648   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.74965614   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7496253   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.7496083   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   16   0.7495651   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.74953496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.74951565   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7494942   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.74947315   REFERENCES:SIMLOC
