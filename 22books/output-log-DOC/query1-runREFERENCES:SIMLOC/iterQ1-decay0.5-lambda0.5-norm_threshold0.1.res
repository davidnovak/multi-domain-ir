###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 217.792, activation diff: 199.571, ratio: 0.916
#   pulse 3: activated nodes: 9486, borderline nodes: 4057, overall activation: 433.405, activation diff: 215.617, ratio: 0.497
#   pulse 4: activated nodes: 10715, borderline nodes: 4504, overall activation: 982.449, activation diff: 549.044, ratio: 0.559
#   pulse 5: activated nodes: 10867, borderline nodes: 3679, overall activation: 1557.445, activation diff: 574.996, ratio: 0.369
#   pulse 6: activated nodes: 10887, borderline nodes: 3553, overall activation: 2027.157, activation diff: 469.712, ratio: 0.232
#   pulse 7: activated nodes: 10893, borderline nodes: 3521, overall activation: 2344.251, activation diff: 317.094, ratio: 0.135
#   pulse 8: activated nodes: 10897, borderline nodes: 3516, overall activation: 2541.474, activation diff: 197.223, ratio: 0.078
#   pulse 9: activated nodes: 10897, borderline nodes: 3515, overall activation: 2659.975, activation diff: 118.501, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2660.0
#   number of spread. activ. pulses: 9
#   running time: 1409

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99532306   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9804795   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9803699   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96301043   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91304946   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8851403   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49411437   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.492855   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49207443   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4909354   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.49016744   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.48959354   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.4887975   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.4881573   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   15   0.48797047   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.48792204   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   17   0.48778546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.48749772   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.48748314   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48689032   REFERENCES:SIMLOC
