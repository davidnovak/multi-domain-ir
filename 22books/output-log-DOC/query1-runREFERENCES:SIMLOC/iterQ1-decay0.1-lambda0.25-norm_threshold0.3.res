###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 491.611, activation diff: 494.278, ratio: 1.005
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 630.489, activation diff: 547.634, ratio: 0.869
#   pulse 4: activated nodes: 11107, borderline nodes: 6277, overall activation: 3676.724, activation diff: 3100.456, ratio: 0.843
#   pulse 5: activated nodes: 11362, borderline nodes: 1503, overall activation: 5887.621, activation diff: 2213.210, ratio: 0.376
#   pulse 6: activated nodes: 11425, borderline nodes: 381, overall activation: 7168.094, activation diff: 1280.473, ratio: 0.179
#   pulse 7: activated nodes: 11445, borderline nodes: 97, overall activation: 7693.458, activation diff: 525.364, ratio: 0.068
#   pulse 8: activated nodes: 11448, borderline nodes: 53, overall activation: 7893.566, activation diff: 200.108, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7893.6
#   number of spread. activ. pulses: 8
#   running time: 1319

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990335   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9867214   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98671407   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96722174   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9094064   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89943767   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8992594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.89924014   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.8992098   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.899192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.8991762   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.8991696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.8991663   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.89916456   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.899155   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.89915186   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.8991488   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.89913595   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   19   0.8991258   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   20   0.8991242   REFERENCES:SIMLOC
