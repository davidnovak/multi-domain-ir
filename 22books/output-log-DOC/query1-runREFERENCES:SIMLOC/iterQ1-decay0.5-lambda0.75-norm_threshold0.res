###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 89.500, activation diff: 77.693, ratio: 0.868
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 213.399, activation diff: 124.231, ratio: 0.582
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 434.003, activation diff: 220.608, ratio: 0.508
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 719.310, activation diff: 285.307, ratio: 0.397
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1032.943, activation diff: 313.633, ratio: 0.304
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1341.081, activation diff: 308.138, ratio: 0.230
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 1620.247, activation diff: 279.166, ratio: 0.172
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 1860.910, activation diff: 240.663, ratio: 0.129
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 2062.662, activation diff: 201.751, ratio: 0.098
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 2228.943, activation diff: 166.281, ratio: 0.075
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 2364.427, activation diff: 135.484, ratio: 0.057
#   pulse 13: activated nodes: 10899, borderline nodes: 3506, overall activation: 2473.912, activation diff: 109.485, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2473.9
#   number of spread. activ. pulses: 13
#   running time: 1512

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98096675   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95604086   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9545714   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93425745   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8827946   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.849317   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4724199   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47060907   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.46923286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.46854192   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.46536565   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.46481296   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.4639171   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.46388304   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.46358114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.4627708   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.46245944   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.46186292   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.46168497   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.46128467   REFERENCES:SIMLOC
