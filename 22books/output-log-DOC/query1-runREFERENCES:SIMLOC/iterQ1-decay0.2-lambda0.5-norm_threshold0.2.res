###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 228.257, activation diff: 212.349, ratio: 0.930
#   pulse 3: activated nodes: 9032, borderline nodes: 5129, overall activation: 509.828, activation diff: 281.780, ratio: 0.553
#   pulse 4: activated nodes: 11101, borderline nodes: 6181, overall activation: 1703.056, activation diff: 1193.228, ratio: 0.701
#   pulse 5: activated nodes: 11348, borderline nodes: 1893, overall activation: 3179.965, activation diff: 1476.909, ratio: 0.464
#   pulse 6: activated nodes: 11412, borderline nodes: 649, overall activation: 4430.834, activation diff: 1250.869, ratio: 0.282
#   pulse 7: activated nodes: 11434, borderline nodes: 205, overall activation: 5272.548, activation diff: 841.715, ratio: 0.160
#   pulse 8: activated nodes: 11444, borderline nodes: 117, overall activation: 5792.953, activation diff: 520.405, ratio: 0.090
#   pulse 9: activated nodes: 11448, borderline nodes: 69, overall activation: 6103.575, activation diff: 310.622, ratio: 0.051
#   pulse 10: activated nodes: 11449, borderline nodes: 57, overall activation: 6285.840, activation diff: 182.266, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6285.8
#   number of spread. activ. pulses: 10
#   running time: 1422

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.997058   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9833782   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98317903   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96464956   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91120887   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8826673   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7963168   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7956439   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79483116   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7947352   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.79470646   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7946828   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.79464525   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.79446036   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   15   0.79445994   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.79443693   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7942729   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   18   0.7942246   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.7942122   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.79419655   REFERENCES:SIMLOC
