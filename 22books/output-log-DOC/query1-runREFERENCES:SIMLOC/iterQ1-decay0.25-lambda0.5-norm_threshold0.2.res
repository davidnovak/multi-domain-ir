###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 215.113, activation diff: 199.205, ratio: 0.926
#   pulse 3: activated nodes: 9032, borderline nodes: 5129, overall activation: 470.587, activation diff: 255.673, ratio: 0.543
#   pulse 4: activated nodes: 11094, borderline nodes: 6212, overall activation: 1502.072, activation diff: 1031.485, ratio: 0.687
#   pulse 5: activated nodes: 11341, borderline nodes: 2054, overall activation: 2778.673, activation diff: 1276.601, ratio: 0.459
#   pulse 6: activated nodes: 11409, borderline nodes: 769, overall activation: 3883.452, activation diff: 1104.778, ratio: 0.284
#   pulse 7: activated nodes: 11425, borderline nodes: 282, overall activation: 4634.387, activation diff: 750.935, ratio: 0.162
#   pulse 8: activated nodes: 11435, borderline nodes: 163, overall activation: 5100.931, activation diff: 466.544, ratio: 0.091
#   pulse 9: activated nodes: 11437, borderline nodes: 138, overall activation: 5379.698, activation diff: 278.767, ratio: 0.052
#   pulse 10: activated nodes: 11438, borderline nodes: 117, overall activation: 5543.267, activation diff: 163.569, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5543.3
#   number of spread. activ. pulses: 10
#   running time: 1384

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99704933   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9832933   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9831119   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9646038   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91113055   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88250244   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74649584   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7455158   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.74479353   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7447725   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7447086   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.744663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.74439937   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.74434984   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   15   0.7442862   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.7442399   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.74417555   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   18   0.74415636   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.74411356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.744105   REFERENCES:SIMLOC
