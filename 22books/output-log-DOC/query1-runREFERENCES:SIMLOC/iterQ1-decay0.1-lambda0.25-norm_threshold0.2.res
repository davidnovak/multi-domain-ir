###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 666.508, activation diff: 666.506, ratio: 1.000
#   pulse 3: activated nodes: 9304, borderline nodes: 4485, overall activation: 1003.293, activation diff: 702.930, ratio: 0.701
#   pulse 4: activated nodes: 11235, borderline nodes: 4696, overall activation: 4444.291, activation diff: 3465.362, ratio: 0.780
#   pulse 5: activated nodes: 11400, borderline nodes: 881, overall activation: 6582.549, activation diff: 2138.333, ratio: 0.325
#   pulse 6: activated nodes: 11440, borderline nodes: 161, overall activation: 7570.286, activation diff: 987.737, ratio: 0.130
#   pulse 7: activated nodes: 11448, borderline nodes: 53, overall activation: 7952.302, activation diff: 382.016, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7952.3
#   number of spread. activ. pulses: 7
#   running time: 1291

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99803907   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98600227   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98587584   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96823645   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91500807   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89815676   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8979159   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.89747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   9   0.89745104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.89744145   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.89733624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89728427   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.8972687   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.89722884   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   15   0.897215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.8971845   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.89713657   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.89709604   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   19   0.8970838   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.89708185   REFERENCES:SIMLOC
