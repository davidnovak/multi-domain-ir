###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 633.038, activation diff: 625.475, ratio: 0.988
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 924.462, activation diff: 336.952, ratio: 0.364
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2006.163, activation diff: 1081.702, ratio: 0.539
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2557.922, activation diff: 551.759, ratio: 0.216
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 2775.644, activation diff: 217.722, ratio: 0.078
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 2854.764, activation diff: 79.119, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2854.8
#   number of spread. activ. pulses: 7
#   running time: 1227

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988408   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9891796   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98907644   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743346   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93023616   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9086872   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49822214   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49747324   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49688628   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49591902   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4956172   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49537474   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49508944   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4948113   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49462217   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49411792   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.49404114   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.4938022   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4935766   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49330607   REFERENCES:SIMLOC
