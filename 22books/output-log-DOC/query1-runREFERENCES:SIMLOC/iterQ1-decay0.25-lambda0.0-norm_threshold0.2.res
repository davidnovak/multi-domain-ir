###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1049.721, activation diff: 1075.744, ratio: 1.025
#   pulse 3: activated nodes: 9398, borderline nodes: 4153, overall activation: 743.579, activation diff: 1730.314, ratio: 2.327
#   pulse 4: activated nodes: 11269, borderline nodes: 3942, overall activation: 4586.516, activation diff: 4446.284, ratio: 0.969
#   pulse 5: activated nodes: 11409, borderline nodes: 644, overall activation: 4560.527, activation diff: 1817.514, ratio: 0.399
#   pulse 6: activated nodes: 11434, borderline nodes: 176, overall activation: 5703.612, activation diff: 1212.115, ratio: 0.213
#   pulse 7: activated nodes: 11441, borderline nodes: 102, overall activation: 5786.871, activation diff: 92.984, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 5786.9
#   number of spread. activ. pulses: 7
#   running time: 1291

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995888   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9891221   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121996   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9190907   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89403844   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74996376   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74984   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.74977493   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7497127   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.749708   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.7497058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7497027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7496742   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.7496482   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   16   0.74962014   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7495961   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7495718   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7495496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.74952555   REFERENCES:SIMLOC
