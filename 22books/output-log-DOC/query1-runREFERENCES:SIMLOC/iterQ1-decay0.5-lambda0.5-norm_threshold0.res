###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 304.679, activation diff: 284.850, ratio: 0.935
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 624.133, activation diff: 319.454, ratio: 0.512
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1258.002, activation diff: 633.870, ratio: 0.504
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1845.279, activation diff: 587.277, ratio: 0.318
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 2261.497, activation diff: 416.218, ratio: 0.184
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 2523.839, activation diff: 262.342, ratio: 0.104
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 2681.534, activation diff: 157.694, ratio: 0.059
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2774.016, activation diff: 92.482, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2774.0
#   number of spread. activ. pulses: 9
#   running time: 1389

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.995965   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9833232   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98289645   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9670854   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92169845   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89692605   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49475566   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49370855   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49298555   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49187517   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.49122876   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49077845   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.49001324   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   14   0.48967248   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.48945615   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.48940524   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4892884   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.48880604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.4887131   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48830116   REFERENCES:SIMLOC
