###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 70.014, activation diff: 58.940, ratio: 0.842
#   pulse 3: activated nodes: 9078, borderline nodes: 5009, overall activation: 191.843, activation diff: 122.274, ratio: 0.637
#   pulse 4: activated nodes: 11081, borderline nodes: 5483, overall activation: 495.522, activation diff: 303.699, ratio: 0.613
#   pulse 5: activated nodes: 11282, borderline nodes: 3049, overall activation: 992.552, activation diff: 497.030, ratio: 0.501
#   pulse 6: activated nodes: 11388, borderline nodes: 1288, overall activation: 1632.779, activation diff: 640.228, ratio: 0.392
#   pulse 7: activated nodes: 11412, borderline nodes: 560, overall activation: 2311.317, activation diff: 678.537, ratio: 0.294
#   pulse 8: activated nodes: 11429, borderline nodes: 300, overall activation: 2944.083, activation diff: 632.767, ratio: 0.215
#   pulse 9: activated nodes: 11437, borderline nodes: 172, overall activation: 3495.990, activation diff: 551.906, ratio: 0.158
#   pulse 10: activated nodes: 11439, borderline nodes: 134, overall activation: 3961.149, activation diff: 465.159, ratio: 0.117
#   pulse 11: activated nodes: 11440, borderline nodes: 110, overall activation: 4345.232, activation diff: 384.084, ratio: 0.088
#   pulse 12: activated nodes: 11440, borderline nodes: 100, overall activation: 4658.264, activation diff: 313.032, ratio: 0.067
#   pulse 13: activated nodes: 11441, borderline nodes: 95, overall activation: 4911.151, activation diff: 252.887, ratio: 0.051
#   pulse 14: activated nodes: 11441, borderline nodes: 86, overall activation: 5114.171, activation diff: 203.020, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 5114.2
#   number of spread. activ. pulses: 14
#   running time: 1745

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98399454   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9600957   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95783883   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93782413   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8836963   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8482494   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7214283   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7190298   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7175218   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7156421   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.715551   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.71526843   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7146193   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7145858   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7144844   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7137455   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7135712   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.71354663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   19   0.7134933   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.71269464   REFERENCES:SIMLOC
