###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1574.099, activation diff: 1600.699, ratio: 1.017
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1586.241, activation diff: 2405.766, ratio: 1.517
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5566.855, activation diff: 4288.999, ratio: 0.770
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6036.542, activation diff: 574.450, ratio: 0.095
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 6181.802, activation diff: 149.934, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6181.8
#   number of spread. activ. pulses: 6
#   running time: 1261

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996668   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9910973   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9763753   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9332683   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124048   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74997073   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7498722   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.74984485   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.7497729   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.749766   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.74976516   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.749761   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7497399   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.74971354   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   16   0.749694   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7496891   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7496536   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7496379   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   20   0.7496258   REFERENCES:SIMLOC
