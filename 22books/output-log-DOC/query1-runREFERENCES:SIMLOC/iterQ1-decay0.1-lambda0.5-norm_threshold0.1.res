###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 375.881, activation diff: 357.659, ratio: 0.952
#   pulse 3: activated nodes: 9486, borderline nodes: 4057, overall activation: 909.442, activation diff: 533.567, ratio: 0.587
#   pulse 4: activated nodes: 11273, borderline nodes: 3914, overall activation: 2842.057, activation diff: 1932.615, ratio: 0.680
#   pulse 5: activated nodes: 11415, borderline nodes: 644, overall activation: 4805.373, activation diff: 1963.315, ratio: 0.409
#   pulse 6: activated nodes: 11443, borderline nodes: 133, overall activation: 6204.586, activation diff: 1399.214, ratio: 0.226
#   pulse 7: activated nodes: 11448, borderline nodes: 46, overall activation: 7082.609, activation diff: 878.022, ratio: 0.124
#   pulse 8: activated nodes: 11452, borderline nodes: 17, overall activation: 7607.175, activation diff: 524.567, ratio: 0.069
#   pulse 9: activated nodes: 11454, borderline nodes: 10, overall activation: 7912.957, activation diff: 305.782, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7913.0
#   number of spread. activ. pulses: 9
#   running time: 1438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9954069   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9814553   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98080075   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96349674   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91407603   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89265496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.892053   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.89101255   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   9   0.8903284   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8903275   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.8902826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.8900526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.89003056   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.88997734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.88996196   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.8898102   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.8897934   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.8897596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   19   0.88975865   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.88958   REFERENCES:SIMLOC
