###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1561.021, activation diff: 1587.930, ratio: 1.017
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1458.146, activation diff: 2767.299, ratio: 1.898
#   pulse 4: activated nodes: 11363, borderline nodes: 2423, overall activation: 7011.096, activation diff: 6355.490, ratio: 0.906
#   pulse 5: activated nodes: 11434, borderline nodes: 238, overall activation: 7722.520, activation diff: 1441.443, ratio: 0.187
#   pulse 6: activated nodes: 11450, borderline nodes: 36, overall activation: 8282.475, activation diff: 585.200, ratio: 0.071
#   pulse 7: activated nodes: 11454, borderline nodes: 10, overall activation: 8335.296, activation diff: 56.233, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8335.3
#   number of spread. activ. pulses: 7
#   running time: 2378

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99963844   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9901657   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.973923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92650807   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8999964   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.8999947   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.89999133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.8999834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   11   0.8999807   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.8999789   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.899977   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   14   0.8999767   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.899975   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   16   0.8999748   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.899973   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   18   0.8999729   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.89997226   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.8999716   REFERENCES:SIMLOC
