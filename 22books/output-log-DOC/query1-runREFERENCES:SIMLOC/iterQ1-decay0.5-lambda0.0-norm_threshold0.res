###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1052.384, activation diff: 1078.984, ratio: 1.025
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 704.300, activation diff: 1309.985, ratio: 1.860
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2665.392, activation diff: 2033.145, ratio: 0.763
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2808.957, activation diff: 202.298, ratio: 0.072
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 2886.742, activation diff: 77.946, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2886.7
#   number of spread. activ. pulses: 6
#   running time: 1202

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996492   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99107516   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9763751   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9332579   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124047   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49897   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49841148   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4978403   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49716282   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.4965686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49654245   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4964441   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49625605   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49588576   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49579766   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4953667   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49492455   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.49480754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.4944423   REFERENCES:SIMLOC
