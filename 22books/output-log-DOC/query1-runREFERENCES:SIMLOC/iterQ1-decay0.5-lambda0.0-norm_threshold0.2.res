###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 702.028, activation diff: 728.051, ratio: 1.037
#   pulse 3: activated nodes: 9398, borderline nodes: 4153, overall activation: 303.618, activation diff: 965.933, ratio: 3.181
#   pulse 4: activated nodes: 10701, borderline nodes: 4642, overall activation: 2135.606, activation diff: 2050.314, ratio: 0.960
#   pulse 5: activated nodes: 10871, borderline nodes: 3640, overall activation: 1860.689, activation diff: 1055.822, ratio: 0.567
#   pulse 6: activated nodes: 10887, borderline nodes: 3548, overall activation: 2679.744, activation diff: 836.263, ratio: 0.312
#   pulse 7: activated nodes: 10894, borderline nodes: 3526, overall activation: 2725.021, activation diff: 51.286, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10894
#   final overall activation: 2725.0
#   number of spread. activ. pulses: 7
#   running time: 1246

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995427   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98913103   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9884182   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121954   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9186924   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8940348   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49873215   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49801084   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49735162   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4964894   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.4957862   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49576652   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4956582   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4953658   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49497426   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4946259   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4943268   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.4938034   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.49366516   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.4930837   REFERENCES:SIMLOC
