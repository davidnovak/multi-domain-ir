###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 939.594, activation diff: 932.030, ratio: 0.992
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1682.164, activation diff: 800.973, ratio: 0.476
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4239.871, activation diff: 2557.706, ratio: 0.603
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5472.869, activation diff: 1232.998, ratio: 0.225
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 5945.955, activation diff: 473.086, ratio: 0.080
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6115.267, activation diff: 169.312, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6115.3
#   number of spread. activ. pulses: 7
#   running time: 1313

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988613   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9893976   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98909   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743723   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9304323   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9087294   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7490101   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7482775   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.74825555   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.74814504   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.74801165   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.74800193   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7479805   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7479215   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.74787855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.7478574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.7478483   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.74783593   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.7478143   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.7478111   REFERENCES:SIMLOC
