###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 736.486, activation diff: 732.661, ratio: 0.995
#   pulse 3: activated nodes: 9816, borderline nodes: 3919, overall activation: 1152.536, activation diff: 623.470, ratio: 0.541
#   pulse 4: activated nodes: 11316, borderline nodes: 3042, overall activation: 3664.310, activation diff: 2513.233, ratio: 0.686
#   pulse 5: activated nodes: 11419, borderline nodes: 467, overall activation: 5064.466, activation diff: 1400.156, ratio: 0.276
#   pulse 6: activated nodes: 11439, borderline nodes: 130, overall activation: 5648.629, activation diff: 584.163, ratio: 0.103
#   pulse 7: activated nodes: 11442, borderline nodes: 71, overall activation: 5865.262, activation diff: 216.632, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5865.3
#   number of spread. activ. pulses: 7
#   running time: 1311

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985142   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9877957   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98767275   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714752   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92309034   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8993141   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7487371   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7479663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.74790406   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.74779785   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7476439   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.74760914   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7475946   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.7475246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.74750143   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.7475008   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7474886   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7474697   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.74745953   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.7473789   REFERENCES:SIMLOC
