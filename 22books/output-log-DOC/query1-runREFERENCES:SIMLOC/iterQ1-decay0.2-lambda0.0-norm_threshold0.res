###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1678.442, activation diff: 1705.042, ratio: 1.016
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1787.071, activation diff: 2642.796, ratio: 1.479
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 6233.458, activation diff: 4796.672, ratio: 0.770
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6776.093, activation diff: 651.207, ratio: 0.096
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6931.647, activation diff: 161.168, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6931.6
#   number of spread. activ. pulses: 6
#   running time: 1349

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99966854   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9910974   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9763753   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93326837   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124048   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79998595   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79995716   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7999245   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.7999089   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.79990834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79989415   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7998932   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.7998814   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.79987663   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7998679   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.79985636   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.79985225   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7998454   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.7998431   REFERENCES:SIMLOC
