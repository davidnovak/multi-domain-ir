###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 593.999, activation diff: 593.996, ratio: 1.000
#   pulse 3: activated nodes: 9304, borderline nodes: 4485, overall activation: 839.095, activation diff: 573.703, ratio: 0.684
#   pulse 4: activated nodes: 11228, borderline nodes: 4808, overall activation: 3511.583, activation diff: 2690.923, ratio: 0.766
#   pulse 5: activated nodes: 11394, borderline nodes: 1005, overall activation: 5231.037, activation diff: 1719.498, ratio: 0.329
#   pulse 6: activated nodes: 11434, borderline nodes: 238, overall activation: 6051.353, activation diff: 820.316, ratio: 0.136
#   pulse 7: activated nodes: 11447, borderline nodes: 81, overall activation: 6371.560, activation diff: 320.207, ratio: 0.050
#   pulse 8: activated nodes: 11449, borderline nodes: 54, overall activation: 6491.092, activation diff: 119.532, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6491.1
#   number of spread. activ. pulses: 8
#   running time: 1342

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991991   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.988293   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9882009   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9703982   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91788644   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89252424   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7995714   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.79929376   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.79923856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7992072   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.79920274   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7991907   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   13   0.7991897   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7991813   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.79916537   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.7991573   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7991504   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.79913473   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.7991333   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.799107   REFERENCES:SIMLOC
