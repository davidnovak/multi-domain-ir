###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 992.071, activation diff: 1016.969, ratio: 1.025
#   pulse 3: activated nodes: 9179, borderline nodes: 4782, overall activation: 782.327, activation diff: 1742.289, ratio: 2.227
#   pulse 4: activated nodes: 11212, borderline nodes: 5137, overall activation: 5978.267, activation diff: 5979.499, ratio: 1.000
#   pulse 5: activated nodes: 11395, borderline nodes: 899, overall activation: 5884.263, activation diff: 2950.679, ratio: 0.501
#   pulse 6: activated nodes: 11437, borderline nodes: 161, overall activation: 7823.278, activation diff: 2070.208, ratio: 0.265
#   pulse 7: activated nodes: 11448, borderline nodes: 54, overall activation: 7978.021, activation diff: 174.863, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7978.0
#   number of spread. activ. pulses: 7
#   running time: 1284

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995523   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879964   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682412   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9109609   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8999955   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.8999933   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.8999885   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.8999782   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.8999759   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.8999712   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8999707   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.8999693   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   14   0.89996904   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.89996743   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.8999667   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.8999662   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.8999661   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   19   0.8999597   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.89995944   REFERENCES:SIMLOC
