###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 474.087, activation diff: 454.259, ratio: 0.958
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1153.202, activation diff: 679.115, ratio: 0.589
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2895.480, activation diff: 1742.277, ratio: 0.602
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4426.558, activation diff: 1531.078, ratio: 0.346
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 5455.203, activation diff: 1028.645, ratio: 0.189
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 6086.476, activation diff: 631.273, ratio: 0.104
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 6459.316, activation diff: 372.840, ratio: 0.058
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 6675.185, activation diff: 215.869, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6675.2
#   number of spread. activ. pulses: 9
#   running time: 1378

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960083   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9837867   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9830315   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9673159   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92224544   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89747256   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7940589   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7931297   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7924912   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.79191494   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.7917481   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7917092   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7916517   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.79155713   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   15   0.7915211   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.7912821   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7911718   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7911014   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   19   0.79105926   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.7910536   REFERENCES:SIMLOC
