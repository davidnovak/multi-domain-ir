###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 553.718, activation diff: 578.616, ratio: 1.045
#   pulse 3: activated nodes: 9179, borderline nodes: 4782, overall activation: 215.751, activation diff: 751.951, ratio: 3.485
#   pulse 4: activated nodes: 10576, borderline nodes: 5594, overall activation: 1876.564, activation diff: 1889.642, ratio: 1.007
#   pulse 5: activated nodes: 10851, borderline nodes: 3781, overall activation: 1393.309, activation diff: 1351.197, ratio: 0.970
#   pulse 6: activated nodes: 10879, borderline nodes: 3624, overall activation: 2544.963, activation diff: 1191.298, ratio: 0.468
#   pulse 7: activated nodes: 10889, borderline nodes: 3559, overall activation: 2613.942, activation diff: 89.478, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10889
#   final overall activation: 2613.9
#   number of spread. activ. pulses: 7
#   running time: 1266

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994303   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98781484   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98544896   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682403   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9075849   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.883374   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49856123   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49771515   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49698624   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49596396   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.49522126   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.4952186   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49517402   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49464783   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49442032   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4938628   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49364287   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49312493   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.49300182   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.492232   REFERENCES:SIMLOC
