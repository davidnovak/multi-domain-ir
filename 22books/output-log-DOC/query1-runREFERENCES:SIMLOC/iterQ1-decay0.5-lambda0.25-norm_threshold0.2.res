###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 376.469, activation diff: 376.466, ratio: 1.000
#   pulse 3: activated nodes: 9304, borderline nodes: 4485, overall activation: 412.814, activation diff: 248.008, ratio: 0.601
#   pulse 4: activated nodes: 10626, borderline nodes: 5180, overall activation: 1403.208, activation diff: 992.488, ratio: 0.707
#   pulse 5: activated nodes: 10858, borderline nodes: 3766, overall activation: 2073.712, activation diff: 670.504, ratio: 0.323
#   pulse 6: activated nodes: 10885, borderline nodes: 3603, overall activation: 2468.737, activation diff: 395.025, ratio: 0.160
#   pulse 7: activated nodes: 10891, borderline nodes: 3532, overall activation: 2637.141, activation diff: 168.404, ratio: 0.064
#   pulse 8: activated nodes: 10895, borderline nodes: 3521, overall activation: 2704.594, activation diff: 67.453, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 2704.6
#   number of spread. activ. pulses: 8
#   running time: 1358

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991583   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9881443   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9877553   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97032523   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9175525   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8920581   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.498425   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49762717   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4969597   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49603596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49540263   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49531862   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4949866   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4946891   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49434304   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.49391976   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49380517   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.493259   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.49316275   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49270618   REFERENCES:SIMLOC
