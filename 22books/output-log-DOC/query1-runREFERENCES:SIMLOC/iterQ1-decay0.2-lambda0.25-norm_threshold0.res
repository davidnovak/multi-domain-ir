###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1000.905, activation diff: 993.341, ratio: 0.992
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1848.419, activation diff: 908.254, ratio: 0.491
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4755.698, activation diff: 2907.279, ratio: 0.611
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6142.293, activation diff: 1386.595, ratio: 0.226
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6670.377, activation diff: 528.084, ratio: 0.079
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 6858.463, activation diff: 188.086, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6858.5
#   number of spread. activ. pulses: 7
#   running time: 1321

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988635   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98941755   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9890905   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743753   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9304437   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9087307   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7989698   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79845256   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7983209   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7981657   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.79815054   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.7981023   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.79803246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.79802954   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.79800224   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.79798603   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.797984   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.797972   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.7979122   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.79789937   REFERENCES:SIMLOC
