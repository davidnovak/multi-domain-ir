###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 19.059, activation diff: 9.562, ratio: 0.502
#   pulse 3: activated nodes: 7900, borderline nodes: 6874, overall activation: 45.667, activation diff: 27.362, ratio: 0.599
#   pulse 4: activated nodes: 8848, borderline nodes: 6710, overall activation: 107.468, activation diff: 62.007, ratio: 0.577
#   pulse 5: activated nodes: 9408, borderline nodes: 6117, overall activation: 215.383, activation diff: 107.929, ratio: 0.501
#   pulse 6: activated nodes: 10171, borderline nodes: 5562, overall activation: 367.824, activation diff: 152.441, ratio: 0.414
#   pulse 7: activated nodes: 10573, borderline nodes: 5060, overall activation: 560.219, activation diff: 192.395, ratio: 0.343
#   pulse 8: activated nodes: 10690, borderline nodes: 4540, overall activation: 776.325, activation diff: 216.106, ratio: 0.278
#   pulse 9: activated nodes: 10778, borderline nodes: 4116, overall activation: 1002.141, activation diff: 225.816, ratio: 0.225
#   pulse 10: activated nodes: 10838, borderline nodes: 3836, overall activation: 1230.695, activation diff: 228.554, ratio: 0.186
#   pulse 11: activated nodes: 10856, borderline nodes: 3712, overall activation: 1453.391, activation diff: 222.696, ratio: 0.153
#   pulse 12: activated nodes: 10870, borderline nodes: 3660, overall activation: 1658.804, activation diff: 205.413, ratio: 0.124
#   pulse 13: activated nodes: 10877, borderline nodes: 3619, overall activation: 1839.169, activation diff: 180.365, ratio: 0.098
#   pulse 14: activated nodes: 10879, borderline nodes: 3588, overall activation: 1992.279, activation diff: 153.110, ratio: 0.077
#   pulse 15: activated nodes: 10880, borderline nodes: 3570, overall activation: 2119.794, activation diff: 127.515, ratio: 0.060

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10880
#   final overall activation: 2119.8
#   number of spread. activ. pulses: 15
#   running time: 1462

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98170227   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.94630146   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9363481   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.92399347   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8624513   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.80766046   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4768003   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47517508   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.47316188   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.47307855   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.46883354   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.46840352   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.46784598   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.46705982   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.46673793   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.46668223   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.46566826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.46559834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.46549746   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.46546423   REFERENCES:SIMLOC
