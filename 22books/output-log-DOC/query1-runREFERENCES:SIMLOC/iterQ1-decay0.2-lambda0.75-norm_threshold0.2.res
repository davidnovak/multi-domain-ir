###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 39.788, activation diff: 29.486, ratio: 0.741
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 114.849, activation diff: 75.647, ratio: 0.659
#   pulse 4: activated nodes: 10424, borderline nodes: 6895, overall activation: 318.847, activation diff: 204.055, ratio: 0.640
#   pulse 5: activated nodes: 11024, borderline nodes: 5250, overall activation: 698.367, activation diff: 379.520, ratio: 0.543
#   pulse 6: activated nodes: 11258, borderline nodes: 3506, overall activation: 1269.598, activation diff: 571.231, ratio: 0.450
#   pulse 7: activated nodes: 11363, borderline nodes: 1687, overall activation: 1972.102, activation diff: 702.504, ratio: 0.356
#   pulse 8: activated nodes: 11403, borderline nodes: 827, overall activation: 2701.181, activation diff: 729.079, ratio: 0.270
#   pulse 9: activated nodes: 11421, borderline nodes: 446, overall activation: 3374.698, activation diff: 673.517, ratio: 0.200
#   pulse 10: activated nodes: 11430, borderline nodes: 238, overall activation: 3960.596, activation diff: 585.899, ratio: 0.148
#   pulse 11: activated nodes: 11440, borderline nodes: 163, overall activation: 4454.268, activation diff: 493.672, ratio: 0.111
#   pulse 12: activated nodes: 11444, borderline nodes: 129, overall activation: 4862.227, activation diff: 407.959, ratio: 0.084
#   pulse 13: activated nodes: 11446, borderline nodes: 100, overall activation: 5195.264, activation diff: 333.038, ratio: 0.064
#   pulse 14: activated nodes: 11448, borderline nodes: 76, overall activation: 5464.874, activation diff: 269.609, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5464.9
#   number of spread. activ. pulses: 14
#   running time: 1467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9814563   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95272017   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9473084   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9288056   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8708441   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8274579   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7666665   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76496065   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.76266354   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.76075655   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.75980514   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.75945914   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7589189   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.75885516   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.75868213   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.75810456   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.7576332   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.7575696   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.75736886   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.75692916   REFERENCES:SIMLOC
