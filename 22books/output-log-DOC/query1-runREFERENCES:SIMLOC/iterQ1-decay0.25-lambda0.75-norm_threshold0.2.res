###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 38.231, activation diff: 27.929, ratio: 0.731
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 108.385, activation diff: 70.740, ratio: 0.653
#   pulse 4: activated nodes: 10395, borderline nodes: 6885, overall activation: 292.937, activation diff: 184.611, ratio: 0.630
#   pulse 5: activated nodes: 10976, borderline nodes: 5329, overall activation: 626.480, activation diff: 333.543, ratio: 0.532
#   pulse 6: activated nodes: 11233, borderline nodes: 3734, overall activation: 1117.615, activation diff: 491.135, ratio: 0.439
#   pulse 7: activated nodes: 11359, borderline nodes: 1919, overall activation: 1718.420, activation diff: 600.806, ratio: 0.350
#   pulse 8: activated nodes: 11397, borderline nodes: 977, overall activation: 2351.207, activation diff: 632.787, ratio: 0.269
#   pulse 9: activated nodes: 11414, borderline nodes: 540, overall activation: 2943.212, activation diff: 592.004, ratio: 0.201
#   pulse 10: activated nodes: 11423, borderline nodes: 319, overall activation: 3461.373, activation diff: 518.161, ratio: 0.150
#   pulse 11: activated nodes: 11431, borderline nodes: 212, overall activation: 3899.983, activation diff: 438.610, ratio: 0.112
#   pulse 12: activated nodes: 11433, borderline nodes: 175, overall activation: 4263.648, activation diff: 363.665, ratio: 0.085
#   pulse 13: activated nodes: 11435, borderline nodes: 156, overall activation: 4561.129, activation diff: 297.481, ratio: 0.065
#   pulse 14: activated nodes: 11437, borderline nodes: 141, overall activation: 4802.281, activation diff: 241.152, ratio: 0.050
#   pulse 15: activated nodes: 11438, borderline nodes: 124, overall activation: 4996.530, activation diff: 194.249, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 4996.5
#   number of spread. activ. pulses: 15
#   running time: 1582

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9858824   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.96087086   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95676273   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9381543   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88091147   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84102374   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.72617185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72430825   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7228679   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.72083485   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.7204551   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.72024703   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7198578   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.71970725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.71919715   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.71899617   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7188517   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7184763   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.71801037   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.71769065   REFERENCES:SIMLOC
