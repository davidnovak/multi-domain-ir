###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 870.583, activation diff: 897.492, ratio: 1.031
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 425.573, activation diff: 1190.172, ratio: 2.797
#   pulse 4: activated nodes: 10822, borderline nodes: 3786, overall activation: 2399.700, activation diff: 2148.955, ratio: 0.896
#   pulse 5: activated nodes: 10893, borderline nodes: 3546, overall activation: 2442.734, activation diff: 548.361, ratio: 0.224
#   pulse 6: activated nodes: 10896, borderline nodes: 3518, overall activation: 2794.462, activation diff: 354.790, ratio: 0.127
#   pulse 7: activated nodes: 10897, borderline nodes: 3514, overall activation: 2817.470, activation diff: 23.768, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2817.5
#   number of spread. activ. pulses: 7
#   running time: 1290

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996076   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9901657   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98998994   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97392285   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92647374   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363604   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49886075   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49824077   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49761286   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4968587   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.4962065   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49617943   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49607152   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49585885   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.495455   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49530694   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4948825   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   18   0.49439567   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.49426457   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49383458   REFERENCES:SIMLOC
