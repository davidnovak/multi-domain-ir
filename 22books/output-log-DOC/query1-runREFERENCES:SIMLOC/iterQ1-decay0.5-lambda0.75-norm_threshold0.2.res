###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 30.449, activation diff: 20.146, ratio: 0.662
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 77.128, activation diff: 47.266, ratio: 0.613
#   pulse 4: activated nodes: 9414, borderline nodes: 6014, overall activation: 180.950, activation diff: 103.901, ratio: 0.574
#   pulse 5: activated nodes: 10268, borderline nodes: 5288, overall activation: 341.802, activation diff: 160.852, ratio: 0.471
#   pulse 6: activated nodes: 10662, borderline nodes: 4711, overall activation: 552.330, activation diff: 210.528, ratio: 0.381
#   pulse 7: activated nodes: 10764, borderline nodes: 4228, overall activation: 793.182, activation diff: 240.851, ratio: 0.304
#   pulse 8: activated nodes: 10840, borderline nodes: 3846, overall activation: 1047.464, activation diff: 254.283, ratio: 0.243
#   pulse 9: activated nodes: 10863, borderline nodes: 3688, overall activation: 1302.840, activation diff: 255.376, ratio: 0.196
#   pulse 10: activated nodes: 10876, borderline nodes: 3619, overall activation: 1544.277, activation diff: 241.437, ratio: 0.156
#   pulse 11: activated nodes: 10885, borderline nodes: 3581, overall activation: 1759.587, activation diff: 215.310, ratio: 0.122
#   pulse 12: activated nodes: 10887, borderline nodes: 3558, overall activation: 1943.847, activation diff: 184.260, ratio: 0.095
#   pulse 13: activated nodes: 10889, borderline nodes: 3547, overall activation: 2097.834, activation diff: 153.988, ratio: 0.073
#   pulse 14: activated nodes: 10892, borderline nodes: 3535, overall activation: 2224.778, activation diff: 126.944, ratio: 0.057
#   pulse 15: activated nodes: 10893, borderline nodes: 3529, overall activation: 2328.488, activation diff: 103.709, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10893
#   final overall activation: 2328.5
#   number of spread. activ. pulses: 15
#   running time: 1604

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9853625   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9578612   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95349103   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.936073   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8783736   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8365788   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4802426   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47875077   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.47714055   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.47656775   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.47357506   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.47275814   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   13   0.47214   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.47188187   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.4713747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.47088373   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.47058392   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.4700841   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.46988082   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.46915585   REFERENCES:SIMLOC
