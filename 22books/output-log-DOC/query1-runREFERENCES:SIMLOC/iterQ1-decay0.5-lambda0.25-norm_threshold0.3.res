###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 278.178, activation diff: 280.845, ratio: 1.010
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 257.960, activation diff: 211.106, ratio: 0.818
#   pulse 4: activated nodes: 10433, borderline nodes: 6282, overall activation: 1102.216, activation diff: 855.688, ratio: 0.776
#   pulse 5: activated nodes: 10747, borderline nodes: 4111, overall activation: 1707.974, activation diff: 606.213, ratio: 0.355
#   pulse 6: activated nodes: 10849, borderline nodes: 3761, overall activation: 2211.301, activation diff: 503.326, ratio: 0.228
#   pulse 7: activated nodes: 10878, borderline nodes: 3594, overall activation: 2474.725, activation diff: 263.424, ratio: 0.106
#   pulse 8: activated nodes: 10889, borderline nodes: 3553, overall activation: 2586.621, activation diff: 111.896, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10889
#   final overall activation: 2586.6
#   number of spread. activ. pulses: 8
#   running time: 1387

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9989609   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98610127   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98539615   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9669574   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90875006   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8798011   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49819016   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4972533   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49655643   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49551776   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49485537   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49471614   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49405107   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49375087   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49361777   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49302134   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.49250162   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.49247175   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4922073   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49181432   REFERENCES:SIMLOC
