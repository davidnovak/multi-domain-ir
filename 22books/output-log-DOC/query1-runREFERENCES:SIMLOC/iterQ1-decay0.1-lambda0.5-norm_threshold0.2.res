###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 254.547, activation diff: 238.639, ratio: 0.938
#   pulse 3: activated nodes: 9032, borderline nodes: 5129, overall activation: 591.171, activation diff: 336.857, ratio: 0.570
#   pulse 4: activated nodes: 11111, borderline nodes: 6122, overall activation: 2153.120, activation diff: 1561.949, ratio: 0.725
#   pulse 5: activated nodes: 11360, borderline nodes: 1677, overall activation: 4060.293, activation diff: 1907.173, ratio: 0.470
#   pulse 6: activated nodes: 11424, borderline nodes: 475, overall activation: 5615.838, activation diff: 1555.545, ratio: 0.277
#   pulse 7: activated nodes: 11444, borderline nodes: 132, overall activation: 6643.410, activation diff: 1027.573, ratio: 0.155
#   pulse 8: activated nodes: 11448, borderline nodes: 62, overall activation: 7273.614, activation diff: 630.204, ratio: 0.087
#   pulse 9: activated nodes: 11450, borderline nodes: 35, overall activation: 7647.753, activation diff: 374.139, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7647.8
#   number of spread. activ. pulses: 9
#   running time: 1412

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9945744   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9783772   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97782534   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.95894104   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9047978   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89183044   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89130116   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8898469   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   9   0.8893583   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8892585   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8892133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.88891935   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.88889545   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.8888883   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.88886356   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.88871837   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.88869166   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.88861895   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.8885927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   20   0.888549   REFERENCES:SIMLOC
