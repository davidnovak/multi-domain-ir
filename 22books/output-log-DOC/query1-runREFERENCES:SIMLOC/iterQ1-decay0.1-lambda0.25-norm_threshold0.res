###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1123.527, activation diff: 1115.963, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2193.747, activation diff: 1135.444, ratio: 0.518
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5837.714, activation diff: 3643.967, ratio: 0.624
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 7539.479, activation diff: 1701.765, ratio: 0.226
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 8177.363, activation diff: 637.885, ratio: 0.078
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8401.092, activation diff: 223.728, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8401.1
#   number of spread. activ. pulses: 7
#   running time: 1239

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.998867   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9894492   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98909104   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743793   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93045735   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9087319   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8988685   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89856184   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.89825815   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   10   0.89823663   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.89819616   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.8980781   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.8980638   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.8980576   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.89803463   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.8980188   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.8979527   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.89795053   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   19   0.89793   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.8979096   REFERENCES:SIMLOC
