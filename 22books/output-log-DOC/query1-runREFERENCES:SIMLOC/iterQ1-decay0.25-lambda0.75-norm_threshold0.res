###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 125.637, activation diff: 113.830, ratio: 0.906
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 336.783, activation diff: 211.478, ratio: 0.628
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 812.364, activation diff: 475.581, ratio: 0.585
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1490.878, activation diff: 678.514, ratio: 0.455
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 2232.917, activation diff: 742.039, ratio: 0.332
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 2931.167, activation diff: 698.251, ratio: 0.238
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 3543.438, activation diff: 612.271, ratio: 0.173
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 4060.822, activation diff: 517.384, ratio: 0.127
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 4488.655, activation diff: 427.833, ratio: 0.095
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 4837.615, activation diff: 348.960, ratio: 0.072
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 5119.595, activation diff: 281.980, ratio: 0.055
#   pulse 13: activated nodes: 11455, borderline nodes: 19, overall activation: 5345.926, activation diff: 226.331, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5345.9
#   number of spread. activ. pulses: 13
#   running time: 1502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812345   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95765364   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9560945   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9355943   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.884455   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.85178196   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7145859   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7116126   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.71014893   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.70856947   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7081935   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.70784885   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7076485   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7073257   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7070222   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.70701003   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.70618546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7060144   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.70593417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7056279   REFERENCES:SIMLOC
