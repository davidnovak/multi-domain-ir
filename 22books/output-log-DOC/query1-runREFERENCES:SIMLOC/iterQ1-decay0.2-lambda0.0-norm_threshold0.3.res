###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 882.483, activation diff: 907.381, ratio: 1.028
#   pulse 3: activated nodes: 9179, borderline nodes: 4782, overall activation: 616.465, activation diff: 1470.581, ratio: 2.386
#   pulse 4: activated nodes: 11205, borderline nodes: 5222, overall activation: 4730.680, activation diff: 4741.719, ratio: 1.002
#   pulse 5: activated nodes: 11392, borderline nodes: 1006, overall activation: 4481.345, activation diff: 2540.971, ratio: 0.567
#   pulse 6: activated nodes: 11430, borderline nodes: 246, overall activation: 6233.713, activation diff: 1876.069, ratio: 0.301
#   pulse 7: activated nodes: 11445, borderline nodes: 113, overall activation: 6369.518, activation diff: 156.556, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 6369.5
#   number of spread. activ. pulses: 7
#   running time: 1288

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995435   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879666   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682412   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9109586   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88354325   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999398   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.799864   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.79986346   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.79986286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.7998521   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7998509   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.79983187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.7998315   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7998197   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.79980177   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.7997938   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.79978377   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7997702   REFERENCES:SIMLOC
