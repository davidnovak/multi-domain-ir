###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 149.389, activation diff: 133.481, ratio: 0.894
#   pulse 3: activated nodes: 9032, borderline nodes: 5129, overall activation: 290.501, activation diff: 141.252, ratio: 0.486
#   pulse 4: activated nodes: 10466, borderline nodes: 6076, overall activation: 735.822, activation diff: 445.322, ratio: 0.605
#   pulse 5: activated nodes: 10757, borderline nodes: 4201, overall activation: 1249.767, activation diff: 513.945, ratio: 0.411
#   pulse 6: activated nodes: 10854, borderline nodes: 3736, overall activation: 1733.190, activation diff: 483.423, ratio: 0.279
#   pulse 7: activated nodes: 10885, borderline nodes: 3597, overall activation: 2110.875, activation diff: 377.685, ratio: 0.179
#   pulse 8: activated nodes: 10889, borderline nodes: 3550, overall activation: 2361.896, activation diff: 251.020, ratio: 0.106
#   pulse 9: activated nodes: 10893, borderline nodes: 3530, overall activation: 2517.934, activation diff: 156.039, ratio: 0.062
#   pulse 10: activated nodes: 10895, borderline nodes: 3522, overall activation: 2612.335, activation diff: 94.400, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 2612.3
#   number of spread. activ. pulses: 10
#   running time: 1509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99697626   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9825052   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9822854   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9642042   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9103639   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88114166   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49597943   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49484447   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49407613   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49271327   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.49259043   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49184453   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.49094164   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   14   0.49029595   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.4902163   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49020806   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49003696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.48940727   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.48924452   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48894998   REFERENCES:SIMLOC
