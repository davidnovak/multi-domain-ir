###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 132.864, activation diff: 121.057, ratio: 0.911
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 363.314, activation diff: 230.782, ratio: 0.635
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 905.067, activation diff: 541.752, ratio: 0.599
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1683.644, activation diff: 778.577, ratio: 0.462
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 2526.828, activation diff: 843.184, ratio: 0.334
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 3314.488, activation diff: 787.660, ratio: 0.238
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 4001.721, activation diff: 687.233, ratio: 0.172
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 4580.392, activation diff: 578.671, ratio: 0.126
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 5057.681, activation diff: 477.290, ratio: 0.094
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 5446.174, activation diff: 388.493, ratio: 0.071
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 5759.519, activation diff: 313.344, ratio: 0.054
#   pulse 13: activated nodes: 11457, borderline nodes: 2, overall activation: 6010.592, activation diff: 251.073, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6010.6
#   number of spread. activ. pulses: 13
#   running time: 1598

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98126996   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9578402   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9562784   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93576014   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8846455   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.85209394   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76263297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76008284   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7580844   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.75705385   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7562073   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.75611764   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.75593877   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7556102   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7551546   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7550434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.75501466   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7545239   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.7544047   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.75440186   REFERENCES:SIMLOC
