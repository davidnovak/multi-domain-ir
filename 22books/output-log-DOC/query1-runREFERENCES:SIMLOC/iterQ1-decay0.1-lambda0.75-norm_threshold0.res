###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 147.319, activation diff: 135.512, ratio: 0.920
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 418.080, activation diff: 271.093, ratio: 0.648
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1107.194, activation diff: 689.114, ratio: 0.622
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2100.888, activation diff: 993.694, ratio: 0.473
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3157.647, activation diff: 1056.759, ratio: 0.335
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 4131.507, activation diff: 973.860, ratio: 0.236
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 4973.532, activation diff: 842.025, ratio: 0.169
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 5678.052, activation diff: 704.520, ratio: 0.124
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 6256.160, activation diff: 578.108, ratio: 0.092
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 6724.538, activation diff: 468.378, ratio: 0.070
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 7100.696, activation diff: 376.159, ratio: 0.053
#   pulse 13: activated nodes: 11457, borderline nodes: 0, overall activation: 7400.900, activation diff: 300.204, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7400.9
#   number of spread. activ. pulses: 13
#   running time: 1561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98133004   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95814353   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9565812   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9360348   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8849554   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8586086   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8565855   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8539509   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.853487   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   10   0.8526199   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8524271   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.8522609   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.85215825   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.8521575   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.85189056   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.85134083   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.8512628   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.85124564   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.85112184   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.85088134   REFERENCES:SIMLOC
