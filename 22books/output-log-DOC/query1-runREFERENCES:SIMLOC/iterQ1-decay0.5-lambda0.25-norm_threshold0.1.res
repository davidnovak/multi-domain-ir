###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 496.649, activation diff: 492.823, ratio: 0.992
#   pulse 3: activated nodes: 9816, borderline nodes: 3919, overall activation: 632.995, activation diff: 283.292, ratio: 0.448
#   pulse 4: activated nodes: 10788, borderline nodes: 4008, overall activation: 1701.769, activation diff: 1068.822, ratio: 0.628
#   pulse 5: activated nodes: 10880, borderline nodes: 3580, overall activation: 2348.776, activation diff: 647.007, ratio: 0.275
#   pulse 6: activated nodes: 10894, borderline nodes: 3520, overall activation: 2642.760, activation diff: 293.984, ratio: 0.111
#   pulse 7: activated nodes: 10897, borderline nodes: 3515, overall activation: 2757.298, activation diff: 114.537, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 2757.3
#   number of spread. activ. pulses: 7
#   running time: 1361

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9984831   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98760116   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9873487   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97139144   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92264473   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89907867   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49790817   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49702808   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4964064   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4953475   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49503362   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49473006   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49431622   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4939645   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.493892   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.4933186   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49303398   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.4928827   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.49265972   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.49244225   REFERENCES:SIMLOC
