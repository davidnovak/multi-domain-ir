###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 165.339, activation diff: 152.106, ratio: 0.920
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 374.143, activation diff: 209.348, ratio: 0.560
#   pulse 4: activated nodes: 10808, borderline nodes: 7353, overall activation: 1508.434, activation diff: 1134.291, ratio: 0.752
#   pulse 5: activated nodes: 11240, borderline nodes: 3114, overall activation: 3166.668, activation diff: 1658.234, ratio: 0.524
#   pulse 6: activated nodes: 11389, borderline nodes: 1288, overall activation: 4851.628, activation diff: 1684.960, ratio: 0.347
#   pulse 7: activated nodes: 11424, borderline nodes: 376, overall activation: 6074.367, activation diff: 1222.739, ratio: 0.201
#   pulse 8: activated nodes: 11443, borderline nodes: 139, overall activation: 6854.247, activation diff: 779.880, ratio: 0.114
#   pulse 9: activated nodes: 11446, borderline nodes: 77, overall activation: 7328.382, activation diff: 474.134, ratio: 0.065
#   pulse 10: activated nodes: 11448, borderline nodes: 55, overall activation: 7609.993, activation diff: 281.611, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7610.0
#   number of spread. activ. pulses: 10
#   running time: 1369

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9964659   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.980747   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97989756   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9602797   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90182745   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89540637   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8950858   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   8   0.8939065   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8938446   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8937962   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.89369124   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.89355576   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.8935228   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.8935006   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.89343166   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.8933852   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.8932762   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   18   0.893266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.8932307   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.8931813   REFERENCES:SIMLOC
