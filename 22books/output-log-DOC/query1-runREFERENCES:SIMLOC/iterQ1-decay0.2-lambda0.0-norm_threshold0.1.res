###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1388.412, activation diff: 1415.320, ratio: 1.019
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1162.477, activation diff: 2342.439, ratio: 2.015
#   pulse 4: activated nodes: 11362, borderline nodes: 2468, overall activation: 5672.032, activation diff: 5157.336, ratio: 0.909
#   pulse 5: activated nodes: 11433, borderline nodes: 267, overall activation: 6171.072, activation diff: 1218.960, ratio: 0.198
#   pulse 6: activated nodes: 11449, borderline nodes: 53, overall activation: 6699.360, activation diff: 551.518, ratio: 0.082
#   pulse 7: activated nodes: 11454, borderline nodes: 26, overall activation: 6746.758, activation diff: 50.376, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6746.8
#   number of spread. activ. pulses: 7
#   running time: 1375

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99963534   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9901652   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.973923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.926508   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79998434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999523   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7999084   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.79989785   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.79989684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79988503   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7998815   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.79986835   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.7998635   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.799854   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.799841   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.79983664   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7998286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   20   0.7998261   REFERENCES:SIMLOC
