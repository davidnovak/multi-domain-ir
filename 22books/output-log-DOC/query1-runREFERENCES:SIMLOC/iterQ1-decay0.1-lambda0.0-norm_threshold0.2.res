###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1258.336, activation diff: 1284.360, ratio: 1.021
#   pulse 3: activated nodes: 9398, borderline nodes: 4153, overall activation: 1067.175, activation diff: 2245.653, ratio: 2.104
#   pulse 4: activated nodes: 11282, borderline nodes: 3748, overall activation: 6491.868, activation diff: 6297.517, ratio: 0.970
#   pulse 5: activated nodes: 11414, borderline nodes: 488, overall activation: 6714.609, activation diff: 2327.184, ratio: 0.347
#   pulse 6: activated nodes: 11444, borderline nodes: 78, overall activation: 8050.503, activation diff: 1416.140, ratio: 0.176
#   pulse 7: activated nodes: 11452, borderline nodes: 26, overall activation: 8154.245, activation diff: 114.466, ratio: 0.014

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 8154.2
#   number of spread. activ. pulses: 7
#   running time: 1557

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999598   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9891363   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121996   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91909146   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.899996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.8999941   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.89999   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.899981   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.89997846   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.89997417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.89997405   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.89997363   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.8999723   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.8999721   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.89997005   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.8999699   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.89996934   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   19   0.89996487   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.89996433   REFERENCES:SIMLOC
