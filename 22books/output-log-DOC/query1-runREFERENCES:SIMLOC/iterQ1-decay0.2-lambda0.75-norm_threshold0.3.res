###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 22.311, activation diff: 12.814, ratio: 0.574
#   pulse 3: activated nodes: 7900, borderline nodes: 6874, overall activation: 64.051, activation diff: 42.494, ratio: 0.663
#   pulse 4: activated nodes: 9032, borderline nodes: 6879, overall activation: 176.762, activation diff: 112.886, ratio: 0.639
#   pulse 5: activated nodes: 10510, borderline nodes: 6812, overall activation: 405.973, activation diff: 229.212, ratio: 0.565
#   pulse 6: activated nodes: 11020, borderline nodes: 5494, overall activation: 794.568, activation diff: 388.595, ratio: 0.489
#   pulse 7: activated nodes: 11228, borderline nodes: 3864, overall activation: 1345.220, activation diff: 550.652, ratio: 0.409
#   pulse 8: activated nodes: 11331, borderline nodes: 2139, overall activation: 2007.508, activation diff: 662.288, ratio: 0.330
#   pulse 9: activated nodes: 11394, borderline nodes: 1198, overall activation: 2696.642, activation diff: 689.134, ratio: 0.256
#   pulse 10: activated nodes: 11410, borderline nodes: 677, overall activation: 3338.654, activation diff: 642.012, ratio: 0.192
#   pulse 11: activated nodes: 11418, borderline nodes: 436, overall activation: 3900.865, activation diff: 562.211, ratio: 0.144
#   pulse 12: activated nodes: 11427, borderline nodes: 269, overall activation: 4377.553, activation diff: 476.687, ratio: 0.109
#   pulse 13: activated nodes: 11433, borderline nodes: 192, overall activation: 4773.619, activation diff: 396.066, ratio: 0.083
#   pulse 14: activated nodes: 11439, borderline nodes: 152, overall activation: 5098.424, activation diff: 324.805, ratio: 0.064
#   pulse 15: activated nodes: 11444, borderline nodes: 133, overall activation: 5362.477, activation diff: 264.053, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 5362.5
#   number of spread. activ. pulses: 15
#   running time: 1707

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9827875   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95203483   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9435558   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9278408   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8668077   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8163298   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7711684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.77004766   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7675717   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.76569223   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.7645156   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.764206   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7637203   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.76341486   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7633625   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.762917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.76254684   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.76254535   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.7622147   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.76203144   REFERENCES:SIMLOC
