###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 316.597, activation diff: 298.376, ratio: 0.942
#   pulse 3: activated nodes: 9486, borderline nodes: 4057, overall activation: 719.055, activation diff: 402.462, ratio: 0.560
#   pulse 4: activated nodes: 11266, borderline nodes: 4079, overall activation: 2010.897, activation diff: 1291.843, ratio: 0.642
#   pulse 5: activated nodes: 11398, borderline nodes: 818, overall activation: 3374.593, activation diff: 1363.696, ratio: 0.404
#   pulse 6: activated nodes: 11431, borderline nodes: 253, overall activation: 4379.361, activation diff: 1004.768, ratio: 0.229
#   pulse 7: activated nodes: 11440, borderline nodes: 117, overall activation: 5020.280, activation diff: 640.919, ratio: 0.128
#   pulse 8: activated nodes: 11442, borderline nodes: 87, overall activation: 5406.506, activation diff: 386.226, ratio: 0.071
#   pulse 9: activated nodes: 11442, borderline nodes: 70, overall activation: 5633.354, activation diff: 226.848, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5633.4
#   number of spread. activ. pulses: 9
#   running time: 1476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99538726   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98126835   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9807222   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96338546   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9138601   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8862771   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7437664   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7425401   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7418037   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7411814   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7409818   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7409601   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.74090797   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.7407185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   15   0.7405779   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   16   0.74053544   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7402704   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.74021137   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   19   0.74020195   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.74018   REFERENCES:SIMLOC
