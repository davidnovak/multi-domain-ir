###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 530.557, activation diff: 510.729, ratio: 0.963
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1347.599, activation diff: 817.042, ratio: 0.606
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3561.125, activation diff: 2213.526, ratio: 0.622
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5460.089, activation diff: 1898.964, ratio: 0.348
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6717.100, activation diff: 1257.011, ratio: 0.187
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 7481.201, activation diff: 764.101, ratio: 0.102
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 7928.618, activation diff: 447.417, ratio: 0.056
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 8185.605, activation diff: 256.987, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8185.6
#   number of spread. activ. pulses: 9
#   running time: 1429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960153   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98385304   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.983045   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96735394   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92232   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89753497   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8933742   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8927165   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.89199054   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   10   0.89131606   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.891256   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.8910581   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.89099824   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.89098936   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.89092666   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.8909163   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8908585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   18   0.89077127   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.89076805   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.89076245   REFERENCES:SIMLOC
