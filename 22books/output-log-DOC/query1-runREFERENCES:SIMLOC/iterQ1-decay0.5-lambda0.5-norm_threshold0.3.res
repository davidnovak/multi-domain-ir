###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 98.796, activation diff: 85.563, ratio: 0.866
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 191.494, activation diff: 93.086, ratio: 0.486
#   pulse 4: activated nodes: 9925, borderline nodes: 6808, overall activation: 538.692, activation diff: 347.199, ratio: 0.645
#   pulse 5: activated nodes: 10525, borderline nodes: 4832, overall activation: 971.163, activation diff: 432.471, ratio: 0.445
#   pulse 6: activated nodes: 10745, borderline nodes: 4278, overall activation: 1417.410, activation diff: 446.247, ratio: 0.315
#   pulse 7: activated nodes: 10847, borderline nodes: 3747, overall activation: 1820.957, activation diff: 403.547, ratio: 0.222
#   pulse 8: activated nodes: 10874, borderline nodes: 3632, overall activation: 2132.298, activation diff: 311.341, ratio: 0.146
#   pulse 9: activated nodes: 10886, borderline nodes: 3580, overall activation: 2338.654, activation diff: 206.356, ratio: 0.088
#   pulse 10: activated nodes: 10889, borderline nodes: 3553, overall activation: 2467.757, activation diff: 129.103, ratio: 0.052
#   pulse 11: activated nodes: 10892, borderline nodes: 3545, overall activation: 2546.888, activation diff: 79.130, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10892
#   final overall activation: 2546.9
#   number of spread. activ. pulses: 11
#   running time: 1363

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978918   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98270774   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9826616   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9634571   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9051028   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.87367404   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4969304   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4958697   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4950567   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49375188   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49350065   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.49290118   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.4918468   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   14   0.4914644   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.49131957   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49117208   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49076623   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.49012855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.48986477   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.489763   REFERENCES:SIMLOC
