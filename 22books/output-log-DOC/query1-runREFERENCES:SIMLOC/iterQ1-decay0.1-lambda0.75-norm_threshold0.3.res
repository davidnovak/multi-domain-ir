###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 23.395, activation diff: 13.898, ratio: 0.594
#   pulse 3: activated nodes: 7900, borderline nodes: 6874, overall activation: 70.249, activation diff: 47.608, ratio: 0.678
#   pulse 4: activated nodes: 9067, borderline nodes: 6912, overall activation: 202.893, activation diff: 132.812, ratio: 0.655
#   pulse 5: activated nodes: 10542, borderline nodes: 6712, overall activation: 487.218, activation diff: 284.325, ratio: 0.584
#   pulse 6: activated nodes: 11056, borderline nodes: 5250, overall activation: 996.144, activation diff: 508.926, ratio: 0.511
#   pulse 7: activated nodes: 11264, borderline nodes: 3333, overall activation: 1739.203, activation diff: 743.059, ratio: 0.427
#   pulse 8: activated nodes: 11366, borderline nodes: 1661, overall activation: 2617.302, activation diff: 878.100, ratio: 0.335
#   pulse 9: activated nodes: 11403, borderline nodes: 838, overall activation: 3501.683, activation diff: 884.380, ratio: 0.253
#   pulse 10: activated nodes: 11417, borderline nodes: 467, overall activation: 4305.080, activation diff: 803.398, ratio: 0.187
#   pulse 11: activated nodes: 11432, borderline nodes: 239, overall activation: 4998.844, activation diff: 693.764, ratio: 0.139
#   pulse 12: activated nodes: 11441, borderline nodes: 156, overall activation: 5581.366, activation diff: 582.522, ratio: 0.104
#   pulse 13: activated nodes: 11445, borderline nodes: 112, overall activation: 6062.434, activation diff: 481.068, ratio: 0.079
#   pulse 14: activated nodes: 11446, borderline nodes: 87, overall activation: 6455.363, activation diff: 392.929, ratio: 0.061
#   pulse 15: activated nodes: 11447, borderline nodes: 65, overall activation: 6773.783, activation diff: 318.420, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6773.8
#   number of spread. activ. pulses: 15
#   running time: 1584

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98301244   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9530483   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9449084   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9285953   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.8684967   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.86803454   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.8675588   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8650488   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8634034   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   10   0.86262476   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8624076   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   12   0.86137444   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.8612305   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   14   0.8611155   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8610127   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.86091375   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.86065686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.86050916   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.8604127   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.86030287   REFERENCES:SIMLOC
