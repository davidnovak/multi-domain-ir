###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 42.901, activation diff: 32.599, ratio: 0.760
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 128.008, activation diff: 85.693, ratio: 0.669
#   pulse 4: activated nodes: 10490, borderline nodes: 6936, overall activation: 374.741, activation diff: 246.783, ratio: 0.659
#   pulse 5: activated nodes: 11051, borderline nodes: 5057, overall activation: 860.581, activation diff: 485.840, ratio: 0.565
#   pulse 6: activated nodes: 11282, borderline nodes: 3063, overall activation: 1621.231, activation diff: 760.650, ratio: 0.469
#   pulse 7: activated nodes: 11384, borderline nodes: 1332, overall activation: 2544.377, activation diff: 923.146, ratio: 0.363
#   pulse 8: activated nodes: 11414, borderline nodes: 588, overall activation: 3476.066, activation diff: 931.689, ratio: 0.268
#   pulse 9: activated nodes: 11430, borderline nodes: 293, overall activation: 4319.979, activation diff: 843.913, ratio: 0.195
#   pulse 10: activated nodes: 11443, borderline nodes: 146, overall activation: 5045.653, activation diff: 725.675, ratio: 0.144
#   pulse 11: activated nodes: 11446, borderline nodes: 94, overall activation: 5652.516, activation diff: 606.862, ratio: 0.107
#   pulse 12: activated nodes: 11448, borderline nodes: 68, overall activation: 6151.640, activation diff: 499.125, ratio: 0.081
#   pulse 13: activated nodes: 11448, borderline nodes: 52, overall activation: 6557.616, activation diff: 405.976, ratio: 0.062
#   pulse 14: activated nodes: 11449, borderline nodes: 39, overall activation: 6885.218, activation diff: 327.602, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6885.2
#   number of spread. activ. pulses: 14
#   running time: 1745

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9816161   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95345867   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.94824547   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9294058   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8714508   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8634252   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8623278   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.85940814   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8577144   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   10   0.857056   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.85672754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.8566278   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.8558731   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.85551465   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.85540867   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.8552344   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.8550285   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.85496473   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.85468745   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.8544439   REFERENCES:SIMLOC
