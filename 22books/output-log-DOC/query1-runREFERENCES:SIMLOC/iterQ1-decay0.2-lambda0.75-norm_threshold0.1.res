###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 73.609, activation diff: 62.536, ratio: 0.850
#   pulse 3: activated nodes: 9078, borderline nodes: 5009, overall activation: 205.197, activation diff: 132.033, ratio: 0.643
#   pulse 4: activated nodes: 11087, borderline nodes: 5467, overall activation: 547.443, activation diff: 342.265, ratio: 0.625
#   pulse 5: activated nodes: 11289, borderline nodes: 2818, overall activation: 1122.267, activation diff: 574.824, ratio: 0.512
#   pulse 6: activated nodes: 11394, borderline nodes: 1168, overall activation: 1864.970, activation diff: 742.702, ratio: 0.398
#   pulse 7: activated nodes: 11423, borderline nodes: 481, overall activation: 2642.597, activation diff: 777.627, ratio: 0.294
#   pulse 8: activated nodes: 11431, borderline nodes: 204, overall activation: 3360.907, activation diff: 718.310, ratio: 0.214
#   pulse 9: activated nodes: 11446, borderline nodes: 124, overall activation: 3984.240, activation diff: 623.334, ratio: 0.156
#   pulse 10: activated nodes: 11447, borderline nodes: 76, overall activation: 4507.752, activation diff: 523.511, ratio: 0.116
#   pulse 11: activated nodes: 11448, borderline nodes: 62, overall activation: 4939.038, activation diff: 431.287, ratio: 0.087
#   pulse 12: activated nodes: 11449, borderline nodes: 45, overall activation: 5290.033, activation diff: 350.995, ratio: 0.066
#   pulse 13: activated nodes: 11450, borderline nodes: 38, overall activation: 5573.297, activation diff: 283.264, ratio: 0.051
#   pulse 14: activated nodes: 11452, borderline nodes: 31, overall activation: 5800.516, activation diff: 227.219, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 5800.5
#   number of spread. activ. pulses: 14
#   running time: 1658

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98404306   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9603555   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9581384   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93804103   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88393486   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84865904   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76994145   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7679666   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7659595   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7645926   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7640801   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.76404834   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.76328105   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.7632721   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.762812   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.76280105   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.76277995   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.7625997   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.762386   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.76187307   REFERENCES:SIMLOC
