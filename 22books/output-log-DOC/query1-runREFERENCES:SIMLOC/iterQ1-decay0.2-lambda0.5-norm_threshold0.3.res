###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 148.704, activation diff: 135.471, ratio: 0.911
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 324.396, activation diff: 176.198, ratio: 0.543
#   pulse 4: activated nodes: 10779, borderline nodes: 7378, overall activation: 1194.277, activation diff: 869.882, ratio: 0.728
#   pulse 5: activated nodes: 11210, borderline nodes: 3575, overall activation: 2439.127, activation diff: 1244.850, ratio: 0.510
#   pulse 6: activated nodes: 11347, borderline nodes: 1586, overall activation: 3763.149, activation diff: 1324.022, ratio: 0.352
#   pulse 7: activated nodes: 11413, borderline nodes: 555, overall activation: 4762.035, activation diff: 998.886, ratio: 0.210
#   pulse 8: activated nodes: 11432, borderline nodes: 240, overall activation: 5409.269, activation diff: 647.235, ratio: 0.120
#   pulse 9: activated nodes: 11442, borderline nodes: 144, overall activation: 5805.417, activation diff: 396.148, ratio: 0.068
#   pulse 10: activated nodes: 11444, borderline nodes: 114, overall activation: 6041.941, activation diff: 236.524, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 6041.9
#   number of spread. activ. pulses: 10
#   running time: 1439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99644077   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9804747   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9795798   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9601306   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9016285   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8682205   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7958262   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7951244   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79398686   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7939637   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7937811   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7937242   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.7936835   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.79362106   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7934424   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.79342204   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.79332936   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7933244   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   19   0.7929332   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   20   0.7928879   REFERENCES:SIMLOC
