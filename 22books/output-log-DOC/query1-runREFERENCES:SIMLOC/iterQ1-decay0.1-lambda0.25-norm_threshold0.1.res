###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 880.389, activation diff: 876.563, ratio: 0.996
#   pulse 3: activated nodes: 9816, borderline nodes: 3919, overall activation: 1507.736, activation diff: 868.516, ratio: 0.576
#   pulse 4: activated nodes: 11322, borderline nodes: 2890, overall activation: 5159.208, activation diff: 3653.503, ratio: 0.708
#   pulse 5: activated nodes: 11426, borderline nodes: 364, overall activation: 7104.257, activation diff: 1945.051, ratio: 0.274
#   pulse 6: activated nodes: 11448, borderline nodes: 59, overall activation: 7892.713, activation diff: 788.456, ratio: 0.100
#   pulse 7: activated nodes: 11454, borderline nodes: 16, overall activation: 8181.941, activation diff: 289.228, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8181.9
#   number of spread. activ. pulses: 7
#   running time: 1307

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985225   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9878974   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98768246   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714927   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9231699   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8993336   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8985402   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8982602   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.8978857   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   10   0.89783496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.8977836   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.89776176   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.8977048   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.8976703   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.89759684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.89759475   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.89759266   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8975909   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.897549   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.89751166   REFERENCES:SIMLOC
