###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 411.574, activation diff: 414.240, ratio: 1.006
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 476.397, activation diff: 407.639, ratio: 0.856
#   pulse 4: activated nodes: 11099, borderline nodes: 6380, overall activation: 2487.243, activation diff: 2048.605, ratio: 0.824
#   pulse 5: activated nodes: 11340, borderline nodes: 1867, overall activation: 4008.197, activation diff: 1521.655, ratio: 0.380
#   pulse 6: activated nodes: 11414, borderline nodes: 734, overall activation: 4974.871, activation diff: 966.674, ratio: 0.194
#   pulse 7: activated nodes: 11429, borderline nodes: 244, overall activation: 5384.742, activation diff: 409.871, ratio: 0.076
#   pulse 8: activated nodes: 11434, borderline nodes: 170, overall activation: 5541.107, activation diff: 156.365, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 5541.1
#   number of spread. activ. pulses: 8
#   running time: 1339

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99901855   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98657984   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9865725   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.967175   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9092653   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88091147   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7494879   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74898356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.74898136   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.748976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.74888486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7488447   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.74881625   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.74880135   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7487659   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7487353   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.7487335   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.74872875   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.74871016   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   20   0.74864775   REFERENCES:SIMLOC
