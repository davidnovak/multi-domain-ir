###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 438.253, activation diff: 440.920, ratio: 1.006
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 525.995, activation diff: 452.610, ratio: 0.860
#   pulse 4: activated nodes: 11101, borderline nodes: 6350, overall activation: 2856.873, activation diff: 2374.092, ratio: 0.831
#   pulse 5: activated nodes: 11352, borderline nodes: 1730, overall activation: 4602.141, activation diff: 1746.106, ratio: 0.379
#   pulse 6: activated nodes: 11417, borderline nodes: 576, overall activation: 5672.675, activation diff: 1070.535, ratio: 0.189
#   pulse 7: activated nodes: 11438, borderline nodes: 167, overall activation: 6120.757, activation diff: 448.081, ratio: 0.073
#   pulse 8: activated nodes: 11444, borderline nodes: 108, overall activation: 6291.865, activation diff: 171.108, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 6291.9
#   number of spread. activ. pulses: 8
#   running time: 1317

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99902457   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9866365   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9866291   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9671945   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9093185   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88106036   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79948133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.79916304   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.7991252   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7991105   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.79907507   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.7990738   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.79905856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7990553   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.79903686   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.79902685   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7990215   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.79900014   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   19   0.7989811   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.79892766   REFERENCES:SIMLOC
