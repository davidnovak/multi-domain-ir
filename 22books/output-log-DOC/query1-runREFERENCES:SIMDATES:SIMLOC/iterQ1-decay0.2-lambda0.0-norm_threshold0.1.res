###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1389.784, activation diff: 1415.136, ratio: 1.018
#   pulse 3: activated nodes: 9963, borderline nodes: 3894, overall activation: 1185.520, activation diff: 2344.433, ratio: 1.978
#   pulse 4: activated nodes: 11395, borderline nodes: 2499, overall activation: 5781.928, activation diff: 5115.416, ratio: 0.885
#   pulse 5: activated nodes: 11442, borderline nodes: 259, overall activation: 6359.502, activation diff: 1027.786, ratio: 0.162
#   pulse 6: activated nodes: 11455, borderline nodes: 59, overall activation: 6772.498, activation diff: 417.213, ratio: 0.062
#   pulse 7: activated nodes: 11460, borderline nodes: 25, overall activation: 6816.155, activation diff: 44.135, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 6816.2
#   number of spread. activ. pulses: 7
#   running time: 1356

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99963564   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99016535   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.973923   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.926508   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79999346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.79997057   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7999447   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.79992914   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.7999212   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   12   0.7999208   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.79991907   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.7999179   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.79991233   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7999062   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.79989076   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.7998843   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.79987663   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7998652   REFERENCES:SIMDATES:SIMLOC
