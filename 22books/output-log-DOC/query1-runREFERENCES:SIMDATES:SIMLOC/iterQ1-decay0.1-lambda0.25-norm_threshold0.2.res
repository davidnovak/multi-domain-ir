###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 667.097, activation diff: 665.987, ratio: 0.998
#   pulse 3: activated nodes: 9306, borderline nodes: 4486, overall activation: 1013.963, activation diff: 701.413, ratio: 0.692
#   pulse 4: activated nodes: 11297, borderline nodes: 4758, overall activation: 4482.718, activation diff: 3480.042, ratio: 0.776
#   pulse 5: activated nodes: 11407, borderline nodes: 875, overall activation: 6624.064, activation diff: 2141.413, ratio: 0.323
#   pulse 6: activated nodes: 11446, borderline nodes: 163, overall activation: 7618.466, activation diff: 994.402, ratio: 0.131
#   pulse 7: activated nodes: 11455, borderline nodes: 58, overall activation: 8007.325, activation diff: 388.858, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 8007.3
#   number of spread. activ. pulses: 7
#   running time: 1360

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980396   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9862945   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9859868   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96823645   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9150283   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8982699   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8979303   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.8975506   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.89748436   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   10   0.89747775   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.8974501   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.89733636   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.89729   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   14   0.89727426   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.8972452   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.89723325   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.8972245   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   18   0.89716375   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_26   19   0.89715755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.8971561   REFERENCES:SIMDATES:SIMLOC
