###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 941.223, activation diff: 932.421, ratio: 0.991
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1708.107, activation diff: 814.141, ratio: 0.477
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 4295.612, activation diff: 2587.505, ratio: 0.602
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5531.910, activation diff: 1236.298, ratio: 0.223
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6007.134, activation diff: 475.224, ratio: 0.079
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 6178.015, activation diff: 170.881, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6178.0
#   number of spread. activ. pulses: 7
#   running time: 1310

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99886143   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98943937   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98927975   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743723   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93043435   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9088589   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7491363   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7484916   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74839175   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.74838775   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7483155   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.74818915   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.74814534   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7481103   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.7480593   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.74801505   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   17   0.74798733   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   18   0.7479588   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_267   19   0.7479503   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7479424   REFERENCES:SIMDATES:SIMLOC
