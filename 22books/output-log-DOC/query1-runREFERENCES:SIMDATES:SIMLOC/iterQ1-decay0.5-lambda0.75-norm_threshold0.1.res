###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 52.093, activation diff: 41.020, ratio: 0.787
#   pulse 3: activated nodes: 9081, borderline nodes: 5011, overall activation: 130.228, activation diff: 78.575, ratio: 0.603
#   pulse 4: activated nodes: 10544, borderline nodes: 5239, overall activation: 290.482, activation diff: 160.279, ratio: 0.552
#   pulse 5: activated nodes: 10803, borderline nodes: 4343, overall activation: 521.561, activation diff: 231.078, ratio: 0.443
#   pulse 6: activated nodes: 10914, borderline nodes: 3892, overall activation: 798.833, activation diff: 277.272, ratio: 0.347
#   pulse 7: activated nodes: 10943, borderline nodes: 3662, overall activation: 1095.735, activation diff: 296.902, ratio: 0.271
#   pulse 8: activated nodes: 10959, borderline nodes: 3584, overall activation: 1388.959, activation diff: 293.224, ratio: 0.211
#   pulse 9: activated nodes: 10961, borderline nodes: 3545, overall activation: 1658.145, activation diff: 269.186, ratio: 0.162
#   pulse 10: activated nodes: 10965, borderline nodes: 3531, overall activation: 1892.160, activation diff: 234.015, ratio: 0.124
#   pulse 11: activated nodes: 10968, borderline nodes: 3523, overall activation: 2089.103, activation diff: 196.943, ratio: 0.094
#   pulse 12: activated nodes: 10968, borderline nodes: 3523, overall activation: 2251.810, activation diff: 162.706, ratio: 0.072
#   pulse 13: activated nodes: 10969, borderline nodes: 3522, overall activation: 2384.653, activation diff: 132.844, ratio: 0.056
#   pulse 14: activated nodes: 10969, borderline nodes: 3521, overall activation: 2492.210, activation diff: 107.556, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 2492.2
#   number of spread. activ. pulses: 14
#   running time: 1635

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.983634   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9580942   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9561135   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9361486   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8816876   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84554136   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47971264   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47596666   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4754864   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.47458765   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4739734   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.47179133   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.47030002   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.46959496   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4687906   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.4683241   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.4677294   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.46762836   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.46733513   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.46722382   REFERENCES:SIMDATES:SIMLOC
