###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 702.568, activation diff: 727.689, ratio: 1.036
#   pulse 3: activated nodes: 9409, borderline nodes: 4160, overall activation: 335.176, activation diff: 996.639, ratio: 2.973
#   pulse 4: activated nodes: 10776, borderline nodes: 4651, overall activation: 2207.497, activation diff: 2132.368, ratio: 0.966
#   pulse 5: activated nodes: 10943, borderline nodes: 3646, overall activation: 1953.939, activation diff: 1084.239, ratio: 0.555
#   pulse 6: activated nodes: 10959, borderline nodes: 3553, overall activation: 2773.142, activation diff: 835.742, ratio: 0.301
#   pulse 7: activated nodes: 10966, borderline nodes: 3531, overall activation: 2817.581, activation diff: 49.563, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10966
#   final overall activation: 2817.6
#   number of spread. activ. pulses: 7
#   running time: 1308

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99954563   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98913664   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98849475   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9712196   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9188442   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8940376   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4998554   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49902174   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4982867   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.49775207   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4973583   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4973558   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49726337   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.49649605   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49635586   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.49579197   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   17   0.49578783   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.49568877   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49565968   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.49556205   REFERENCES:SIMDATES:SIMLOC
