###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 21.820, activation diff: 12.323, ratio: 0.565
#   pulse 3: activated nodes: 7901, borderline nodes: 6875, overall activation: 61.195, activation diff: 40.124, ratio: 0.656
#   pulse 4: activated nodes: 9004, borderline nodes: 6854, overall activation: 165.330, activation diff: 104.307, ratio: 0.631
#   pulse 5: activated nodes: 10474, borderline nodes: 6814, overall activation: 373.246, activation diff: 207.917, ratio: 0.557
#   pulse 6: activated nodes: 11006, borderline nodes: 5588, overall activation: 718.060, activation diff: 344.815, ratio: 0.480
#   pulse 7: activated nodes: 11218, borderline nodes: 4112, overall activation: 1199.058, activation diff: 480.997, ratio: 0.401
#   pulse 8: activated nodes: 11325, borderline nodes: 2475, overall activation: 1773.075, activation diff: 574.017, ratio: 0.324
#   pulse 9: activated nodes: 11386, borderline nodes: 1387, overall activation: 2378.151, activation diff: 605.076, ratio: 0.254
#   pulse 10: activated nodes: 11409, borderline nodes: 832, overall activation: 2949.752, activation diff: 571.601, ratio: 0.194
#   pulse 11: activated nodes: 11422, borderline nodes: 535, overall activation: 3453.442, activation diff: 503.691, ratio: 0.146
#   pulse 12: activated nodes: 11428, borderline nodes: 353, overall activation: 3882.376, activation diff: 428.934, ratio: 0.110
#   pulse 13: activated nodes: 11433, borderline nodes: 260, overall activation: 4240.047, activation diff: 357.671, ratio: 0.084
#   pulse 14: activated nodes: 11438, borderline nodes: 205, overall activation: 4534.101, activation diff: 294.054, ratio: 0.065
#   pulse 15: activated nodes: 11445, borderline nodes: 180, overall activation: 4773.558, activation diff: 239.457, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 4773.6
#   number of spread. activ. pulses: 15
#   running time: 1660

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.982662   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95169663   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9439368   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.92741823   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8663919   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8162953   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.72301185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7211968   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7190951   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7180705   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.71632767   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7154454   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7151097   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.71503526   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.7148079   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7138423   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.71374404   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7133278   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.71295536   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   20   0.71287054   REFERENCES:SIMDATES:SIMLOC
