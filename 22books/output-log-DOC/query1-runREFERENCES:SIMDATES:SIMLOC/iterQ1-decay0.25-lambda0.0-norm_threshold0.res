###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1577.038, activation diff: 1601.345, ratio: 1.015
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1637.414, activation diff: 2389.371, ratio: 1.459
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5658.215, activation diff: 4249.381, ratio: 0.751
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6110.721, activation diff: 522.832, ratio: 0.086
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6246.907, activation diff: 136.731, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6246.9
#   number of spread. activ. pulses: 6
#   running time: 1322

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996669   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9910973   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9763753   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9332683   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124048   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7499901   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74992996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7499072   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.74988776   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74987364   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.74985564   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.74984324   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.7498342   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7498176   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.7498091   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.74979323   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.7497703   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.749766   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7497378   REFERENCES:SIMDATES:SIMLOC
