###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 446.559, activation diff: 426.730, ratio: 0.956
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1070.599, activation diff: 624.040, ratio: 0.583
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2614.748, activation diff: 1544.149, ratio: 0.591
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3982.450, activation diff: 1367.702, ratio: 0.343
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 4906.777, activation diff: 924.328, ratio: 0.188
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 5476.361, activation diff: 569.583, ratio: 0.104
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 5814.037, activation diff: 337.676, ratio: 0.058
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 6010.352, activation diff: 196.315, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6010.4
#   number of spread. activ. pulses: 9
#   running time: 1454

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996004   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9837983   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9832432   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9672925   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9221983   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89760613   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7445433   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7433454   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7428104   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.74276006   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7421173   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7419236   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.74192023   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.74187195   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.7418643   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.74181163   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.7417064   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.74146897   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   19   0.74138904   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   20   0.7413858   REFERENCES:SIMDATES:SIMLOC
