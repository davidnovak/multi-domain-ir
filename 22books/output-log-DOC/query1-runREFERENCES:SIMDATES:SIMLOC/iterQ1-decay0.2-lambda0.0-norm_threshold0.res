###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1681.501, activation diff: 1705.807, ratio: 1.014
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1838.585, activation diff: 2620.825, ratio: 1.425
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 6329.729, activation diff: 4736.589, ratio: 0.748
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6845.829, activation diff: 586.174, ratio: 0.086
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6994.393, activation diff: 149.168, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6994.4
#   number of spread. activ. pulses: 6
#   running time: 1578

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99966866   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99109745   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9910974   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9763753   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93326837   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9124048   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999941   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999735   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.79994994   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.79993594   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   11   0.79992914   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.79992884   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.7999286   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.79992574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.7999209   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7999135   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7999011   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.79989445   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.79988515   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.799878   REFERENCES:SIMDATES:SIMLOC
