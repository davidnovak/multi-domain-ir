###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 22.362, activation diff: 12.865, ratio: 0.575
#   pulse 3: activated nodes: 7901, borderline nodes: 6875, overall activation: 64.283, activation diff: 42.669, ratio: 0.664
#   pulse 4: activated nodes: 9032, borderline nodes: 6878, overall activation: 177.784, activation diff: 113.669, ratio: 0.639
#   pulse 5: activated nodes: 10517, borderline nodes: 6805, overall activation: 410.069, activation diff: 232.284, ratio: 0.566
#   pulse 6: activated nodes: 11027, borderline nodes: 5487, overall activation: 805.579, activation diff: 395.510, ratio: 0.491
#   pulse 7: activated nodes: 11239, borderline nodes: 3841, overall activation: 1365.894, activation diff: 560.315, ratio: 0.410
#   pulse 8: activated nodes: 11337, borderline nodes: 2108, overall activation: 2037.883, activation diff: 671.990, ratio: 0.330
#   pulse 9: activated nodes: 11401, borderline nodes: 1177, overall activation: 2735.088, activation diff: 697.205, ratio: 0.255
#   pulse 10: activated nodes: 11416, borderline nodes: 666, overall activation: 3383.283, activation diff: 648.195, ratio: 0.192
#   pulse 11: activated nodes: 11425, borderline nodes: 429, overall activation: 3950.301, activation diff: 567.017, ratio: 0.144
#   pulse 12: activated nodes: 11433, borderline nodes: 268, overall activation: 4430.757, activation diff: 480.456, ratio: 0.108
#   pulse 13: activated nodes: 11444, borderline nodes: 191, overall activation: 4829.837, activation diff: 399.080, ratio: 0.083
#   pulse 14: activated nodes: 11446, borderline nodes: 145, overall activation: 5157.127, activation diff: 327.290, ratio: 0.063
#   pulse 15: activated nodes: 11451, borderline nodes: 130, overall activation: 5423.266, activation diff: 266.139, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5423.3
#   number of spread. activ. pulses: 15
#   running time: 1652

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9827918   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95229036   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9446709   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9278581   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.86684114   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8172116   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7716045   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7702776   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7677761   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.7666658   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.76493853   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7645233   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7644105   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.76373196   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7634981   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.76309204   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.76299006   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7628094   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   19   0.76241064   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7622257   REFERENCES:SIMDATES:SIMLOC
