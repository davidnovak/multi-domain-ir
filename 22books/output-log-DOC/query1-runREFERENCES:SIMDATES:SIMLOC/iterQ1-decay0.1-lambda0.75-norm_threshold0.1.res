###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 80.858, activation diff: 69.785, ratio: 0.863
#   pulse 3: activated nodes: 9081, borderline nodes: 5011, overall activation: 233.596, activation diff: 153.178, ratio: 0.656
#   pulse 4: activated nodes: 11102, borderline nodes: 5421, overall activation: 667.184, activation diff: 433.605, ratio: 0.650
#   pulse 5: activated nodes: 11318, borderline nodes: 2454, overall activation: 1427.845, activation diff: 760.661, ratio: 0.533
#   pulse 6: activated nodes: 11410, borderline nodes: 981, overall activation: 2398.167, activation diff: 970.322, ratio: 0.405
#   pulse 7: activated nodes: 11435, borderline nodes: 348, overall activation: 3391.457, activation diff: 993.290, ratio: 0.293
#   pulse 8: activated nodes: 11451, borderline nodes: 138, overall activation: 4294.237, activation diff: 902.780, ratio: 0.210
#   pulse 9: activated nodes: 11454, borderline nodes: 71, overall activation: 5070.384, activation diff: 776.147, ratio: 0.153
#   pulse 10: activated nodes: 11455, borderline nodes: 53, overall activation: 5718.609, activation diff: 648.225, ratio: 0.113
#   pulse 11: activated nodes: 11456, borderline nodes: 35, overall activation: 6250.759, activation diff: 532.150, ratio: 0.085
#   pulse 12: activated nodes: 11458, borderline nodes: 21, overall activation: 6682.622, activation diff: 431.863, ratio: 0.065
#   pulse 13: activated nodes: 11460, borderline nodes: 17, overall activation: 7030.271, activation diff: 347.649, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 7030.3
#   number of spread. activ. pulses: 13
#   running time: 1562

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9790081   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95189434   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.94936335   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9279872   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.87283826   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8560939   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.85405755   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8508098   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8500728   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.8495531   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8486808   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.8478923   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.8477027   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.84742785   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.847404   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.8470891   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.8469193   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   18   0.84686774   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.8466201   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   20   0.8464692   REFERENCES:SIMDATES:SIMLOC
