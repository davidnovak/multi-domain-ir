###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 492.028, activation diff: 493.861, ratio: 1.004
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 637.742, activation diff: 548.932, ratio: 0.861
#   pulse 4: activated nodes: 11164, borderline nodes: 6334, overall activation: 3713.132, activation diff: 3112.899, ratio: 0.838
#   pulse 5: activated nodes: 11363, borderline nodes: 1504, overall activation: 5940.947, activation diff: 2229.726, ratio: 0.375
#   pulse 6: activated nodes: 11431, borderline nodes: 386, overall activation: 7220.688, activation diff: 1279.742, ratio: 0.177
#   pulse 7: activated nodes: 11452, borderline nodes: 95, overall activation: 7749.213, activation diff: 528.525, ratio: 0.068
#   pulse 8: activated nodes: 11455, borderline nodes: 58, overall activation: 7953.592, activation diff: 204.378, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7953.6
#   number of spread. activ. pulses: 8
#   running time: 1376

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990345   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9869116   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9867784   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96722215   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9094266   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8994629   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8992746   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.8992612   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8992341   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   10   0.89921683   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.8991923   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.89919215   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.89918995   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.8991821   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.8991795   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.89917946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.8991758   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.899172   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.89914787   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   20   0.89913064   REFERENCES:SIMDATES:SIMLOC
