###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 474.823, activation diff: 454.995, ratio: 0.958
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1163.744, activation diff: 688.920, ratio: 0.592
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2925.588, activation diff: 1761.845, ratio: 0.602
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4467.870, activation diff: 1542.282, ratio: 0.345
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5503.273, activation diff: 1035.402, ratio: 0.188
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6139.058, activation diff: 635.785, ratio: 0.104
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 6515.045, activation diff: 375.987, ratio: 0.058
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6733.128, activation diff: 218.082, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6733.1
#   number of spread. activ. pulses: 9
#   running time: 1466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960084   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9838388   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98325217   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9673159   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92224693   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89764893   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7942046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.793212   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7925544   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7923827   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7918505   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.79174817   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.79169667   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.79165196   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.79162014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.79158795   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.7914238   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   18   0.791311   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.79128224   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   20   0.79120773   REFERENCES:SIMDATES:SIMLOC
