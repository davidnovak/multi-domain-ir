###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1258.948, activation diff: 1284.069, ratio: 1.020
#   pulse 3: activated nodes: 9409, borderline nodes: 4160, overall activation: 1074.260, activation diff: 2251.005, ratio: 2.095
#   pulse 4: activated nodes: 11343, borderline nodes: 3809, overall activation: 6583.799, activation diff: 6248.651, ratio: 0.949
#   pulse 5: activated nodes: 11421, borderline nodes: 482, overall activation: 7000.115, activation diff: 2009.502, ratio: 0.287
#   pulse 6: activated nodes: 11450, borderline nodes: 84, overall activation: 8130.022, activation diff: 1154.374, ratio: 0.142
#   pulse 7: activated nodes: 11458, borderline nodes: 29, overall activation: 8219.737, activation diff: 93.120, ratio: 0.011

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 8219.7
#   number of spread. activ. pulses: 7
#   running time: 1447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99959874   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9891367   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121996   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9190915   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8999974   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.8999954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.89999145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.89998394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.899983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.89998233   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.89998   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.8999777   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   14   0.8999772   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8999767   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.8999755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.8999736   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.8999723   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   19   0.8999721   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.89996934   REFERENCES:SIMDATES:SIMLOC
