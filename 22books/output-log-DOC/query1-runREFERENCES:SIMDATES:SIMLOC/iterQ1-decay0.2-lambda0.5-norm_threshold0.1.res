###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 336.649, activation diff: 318.360, ratio: 0.946
#   pulse 3: activated nodes: 9496, borderline nodes: 4063, overall activation: 788.968, activation diff: 452.323, ratio: 0.573
#   pulse 4: activated nodes: 11318, borderline nodes: 4062, overall activation: 2299.853, activation diff: 1510.886, ratio: 0.657
#   pulse 5: activated nodes: 11413, borderline nodes: 741, overall activation: 3869.396, activation diff: 1569.542, ratio: 0.406
#   pulse 6: activated nodes: 11446, borderline nodes: 201, overall activation: 5010.511, activation diff: 1141.115, ratio: 0.228
#   pulse 7: activated nodes: 11454, borderline nodes: 77, overall activation: 5734.279, activation diff: 723.768, ratio: 0.126
#   pulse 8: activated nodes: 11456, borderline nodes: 47, overall activation: 6170.112, activation diff: 435.833, ratio: 0.071
#   pulse 9: activated nodes: 11458, borderline nodes: 34, overall activation: 6426.396, activation diff: 256.285, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 6426.4
#   number of spread. activ. pulses: 9
#   running time: 1561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99539506   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9814083   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98101616   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9634279   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9139477   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8865926   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79353595   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7925796   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7916304   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.79149103   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7909181   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.7908262   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.79078555   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.79075027   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.790727   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   16   0.79064536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.7903659   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.790349   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.7903223   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   20   0.79027176   REFERENCES:SIMDATES:SIMLOC
