###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 140.605, activation diff: 127.083, ratio: 0.904
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 304.938, activation diff: 164.687, ratio: 0.540
#   pulse 4: activated nodes: 10780, borderline nodes: 7396, overall activation: 1078.644, activation diff: 773.707, ratio: 0.717
#   pulse 5: activated nodes: 11196, borderline nodes: 3787, overall activation: 2164.732, activation diff: 1086.088, ratio: 0.502
#   pulse 6: activated nodes: 11347, borderline nodes: 1828, overall activation: 3329.194, activation diff: 1164.462, ratio: 0.350
#   pulse 7: activated nodes: 11417, borderline nodes: 669, overall activation: 4224.855, activation diff: 895.660, ratio: 0.212
#   pulse 8: activated nodes: 11428, borderline nodes: 329, overall activation: 4809.380, activation diff: 584.526, ratio: 0.122
#   pulse 9: activated nodes: 11439, borderline nodes: 215, overall activation: 5168.498, activation diff: 359.117, ratio: 0.069
#   pulse 10: activated nodes: 11442, borderline nodes: 177, overall activation: 5383.171, activation diff: 214.674, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5383.2
#   number of spread. activ. pulses: 10
#   running time: 1488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99642694   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9803839   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97976345   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9600425   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9015197   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.86827886   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7461413   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7451549   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.74464536   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7441441   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.74371886   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7436745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.7435975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.743595   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.74355495   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.74345356   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.743377   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.7432928   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.74328685   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.74321115   REFERENCES:SIMDATES:SIMLOC
