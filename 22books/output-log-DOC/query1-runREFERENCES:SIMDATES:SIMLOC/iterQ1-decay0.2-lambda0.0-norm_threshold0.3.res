###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 882.674, activation diff: 907.242, ratio: 1.028
#   pulse 3: activated nodes: 9182, borderline nodes: 4784, overall activation: 629.256, activation diff: 1483.146, ratio: 2.357
#   pulse 4: activated nodes: 11279, borderline nodes: 5296, overall activation: 4793.453, activation diff: 4761.372, ratio: 0.993
#   pulse 5: activated nodes: 11398, borderline nodes: 1010, overall activation: 4648.569, activation diff: 2409.501, ratio: 0.518
#   pulse 6: activated nodes: 11440, borderline nodes: 254, overall activation: 6316.603, activation diff: 1734.687, ratio: 0.275
#   pulse 7: activated nodes: 11454, borderline nodes: 111, overall activation: 6437.687, activation diff: 133.419, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6437.7
#   number of spread. activ. pulses: 7
#   running time: 1323

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995451   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879788   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682412   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91095996   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88354325   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7999919   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7999632   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7999318   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.79991335   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.79989886   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.799898   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.79989666   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.79988927   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7998814   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.799879   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7998656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.79984975   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.7998388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7998345   REFERENCES:SIMDATES:SIMLOC
