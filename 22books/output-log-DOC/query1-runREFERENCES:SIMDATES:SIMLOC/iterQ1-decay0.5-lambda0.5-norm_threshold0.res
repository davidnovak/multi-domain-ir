###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 305.235, activation diff: 285.407, ratio: 0.935
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 639.247, activation diff: 334.012, ratio: 0.523
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 1301.473, activation diff: 662.226, ratio: 0.509
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1909.176, activation diff: 607.703, ratio: 0.318
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 2336.274, activation diff: 427.098, ratio: 0.183
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2603.752, activation diff: 267.478, ratio: 0.103
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2763.679, activation diff: 159.927, ratio: 0.058
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2857.041, activation diff: 93.362, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2857.0
#   number of spread. activ. pulses: 9
#   running time: 1415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9959656   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98339105   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9831325   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9670857   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92170846   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8971264   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49609774   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4940076   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49400115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49342832   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49301505   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.49160725   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.49122986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.49097574   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.49077952   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.4905674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.49006206   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.49003395   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.48967618   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.48945934   REFERENCES:SIMDATES:SIMLOC
