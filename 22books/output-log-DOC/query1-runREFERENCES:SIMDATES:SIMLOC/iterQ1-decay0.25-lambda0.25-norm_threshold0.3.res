###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 411.991, activation diff: 413.823, ratio: 1.004
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 489.147, activation diff: 415.346, ratio: 0.849
#   pulse 4: activated nodes: 11150, borderline nodes: 6431, overall activation: 2539.476, activation diff: 2077.323, ratio: 0.818
#   pulse 5: activated nodes: 11343, borderline nodes: 1856, overall activation: 4083.309, activation diff: 1544.272, ratio: 0.378
#   pulse 6: activated nodes: 11421, borderline nodes: 727, overall activation: 5047.370, activation diff: 964.062, ratio: 0.191
#   pulse 7: activated nodes: 11435, borderline nodes: 235, overall activation: 5454.955, activation diff: 407.585, ratio: 0.075
#   pulse 8: activated nodes: 11445, borderline nodes: 159, overall activation: 5611.031, activation diff: 156.076, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 5611.0
#   number of spread. activ. pulses: 8
#   running time: 1447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99902   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98680246   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98664546   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9671762   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9092894   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8811977   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7495385   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7492272   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7491778   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.74912703   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74911505   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.74909556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7490556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.74898994   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7489767   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.7489673   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7488978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7488875   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7488853   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.74886274   REFERENCES:SIMDATES:SIMLOC
