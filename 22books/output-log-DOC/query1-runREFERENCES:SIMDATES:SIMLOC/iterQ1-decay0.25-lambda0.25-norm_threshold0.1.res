###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 737.284, activation diff: 732.279, ratio: 0.993
#   pulse 3: activated nodes: 9876, borderline nodes: 3964, overall activation: 1173.051, activation diff: 627.708, ratio: 0.535
#   pulse 4: activated nodes: 11357, borderline nodes: 3082, overall activation: 3719.856, activation diff: 2547.303, ratio: 0.685
#   pulse 5: activated nodes: 11427, borderline nodes: 458, overall activation: 5126.553, activation diff: 1406.697, ratio: 0.274
#   pulse 6: activated nodes: 11452, borderline nodes: 127, overall activation: 5713.741, activation diff: 587.188, ratio: 0.103
#   pulse 7: activated nodes: 11456, borderline nodes: 60, overall activation: 5932.806, activation diff: 219.065, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 5932.8
#   number of spread. activ. pulses: 7
#   running time: 1358

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99851435   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98789746   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98785543   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714752   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9230989   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89945376   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74885523   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.74813557   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7480937   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.74804   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.74792564   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.74782336   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7477983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7477529   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.7477033   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7476493   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.74761426   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   18   0.7476031   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.7475757   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.74757564   REFERENCES:SIMDATES:SIMLOC
