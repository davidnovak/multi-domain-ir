###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1562.468, activation diff: 1587.819, ratio: 1.016
#   pulse 3: activated nodes: 9963, borderline nodes: 3894, overall activation: 1476.675, activation diff: 2760.844, ratio: 1.870
#   pulse 4: activated nodes: 11395, borderline nodes: 2450, overall activation: 7143.005, activation diff: 6257.630, ratio: 0.876
#   pulse 5: activated nodes: 11445, borderline nodes: 226, overall activation: 7914.665, activation diff: 1190.385, ratio: 0.150
#   pulse 6: activated nodes: 11456, borderline nodes: 40, overall activation: 8349.928, activation diff: 439.056, ratio: 0.053
#   pulse 7: activated nodes: 11461, borderline nodes: 14, overall activation: 8399.343, activation diff: 49.949, ratio: 0.006

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11461
#   final overall activation: 8399.3
#   number of spread. activ. pulses: 7
#   running time: 2347

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99963856   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99016577   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9901657   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.973923   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92650807   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90363616   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89999765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.8999958   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.8999925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.8999857   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   11   0.89998466   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.89998406   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.899982   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.8999815   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.89997935   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.89997923   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.8999779   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.89997613   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   19   0.8999759   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.8999754   REFERENCES:SIMDATES:SIMLOC
