###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 165.559, activation diff: 152.037, ratio: 0.918
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 377.256, activation diff: 212.090, ratio: 0.562
#   pulse 4: activated nodes: 10820, borderline nodes: 7363, overall activation: 1523.728, activation diff: 1146.472, ratio: 0.752
#   pulse 5: activated nodes: 11245, borderline nodes: 3095, overall activation: 3197.575, activation diff: 1673.848, ratio: 0.523
#   pulse 6: activated nodes: 11395, borderline nodes: 1282, overall activation: 4890.586, activation diff: 1693.011, ratio: 0.346
#   pulse 7: activated nodes: 11432, borderline nodes: 377, overall activation: 6118.410, activation diff: 1227.823, ratio: 0.201
#   pulse 8: activated nodes: 11450, borderline nodes: 139, overall activation: 6902.945, activation diff: 784.535, ratio: 0.114
#   pulse 9: activated nodes: 11453, borderline nodes: 76, overall activation: 7381.502, activation diff: 478.558, ratio: 0.065
#   pulse 10: activated nodes: 11455, borderline nodes: 56, overall activation: 7666.926, activation diff: 285.424, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7666.9
#   number of spread. activ. pulses: 10
#   running time: 1412

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996467   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98082435   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98022366   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96028084   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9018376   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89547485   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8951109   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.894052   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   9   0.89393175   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.89383173   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.89369154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8936131   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.89357847   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.8935567   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.89346755   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   16   0.8934262   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.89338607   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.893312   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.8933017   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.8932258   REFERENCES:SIMDATES:SIMLOC
