###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 634.331, activation diff: 625.529, ratio: 0.986
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 955.429, activation diff: 358.833, ratio: 0.376
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 2077.666, activation diff: 1122.237, ratio: 0.540
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 2640.329, activation diff: 562.663, ratio: 0.213
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 2859.636, activation diff: 219.307, ratio: 0.077
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2938.557, activation diff: 78.921, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2938.6
#   number of spread. activ. pulses: 7
#   running time: 1335

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99884117   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9892714   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9892317   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743347   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9302466   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9088206   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49927264   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4981894   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49771124   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49700996   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49691147   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.4968787   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.4963007   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.49591973   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.49537522   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.49532017   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.49509108   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.49503195   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.495011   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.49500066   REFERENCES:SIMDATES:SIMLOC
