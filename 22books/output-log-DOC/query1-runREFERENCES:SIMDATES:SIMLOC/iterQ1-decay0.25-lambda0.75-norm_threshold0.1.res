###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 70.071, activation diff: 58.998, ratio: 0.842
#   pulse 3: activated nodes: 9081, borderline nodes: 5011, overall activation: 193.149, activation diff: 123.518, ratio: 0.639
#   pulse 4: activated nodes: 11087, borderline nodes: 5484, overall activation: 501.405, activation diff: 308.275, ratio: 0.615
#   pulse 5: activated nodes: 11288, borderline nodes: 3021, overall activation: 1007.535, activation diff: 506.129, ratio: 0.502
#   pulse 6: activated nodes: 11397, borderline nodes: 1283, overall activation: 1658.272, activation diff: 650.737, ratio: 0.392
#   pulse 7: activated nodes: 11419, borderline nodes: 550, overall activation: 2346.160, activation diff: 687.888, ratio: 0.293
#   pulse 8: activated nodes: 11436, borderline nodes: 298, overall activation: 2986.451, activation diff: 640.290, ratio: 0.214
#   pulse 9: activated nodes: 11446, borderline nodes: 174, overall activation: 3544.263, activation diff: 557.812, ratio: 0.157
#   pulse 10: activated nodes: 11449, borderline nodes: 135, overall activation: 4014.023, activation diff: 469.760, ratio: 0.117
#   pulse 11: activated nodes: 11450, borderline nodes: 112, overall activation: 4401.701, activation diff: 387.679, ratio: 0.088
#   pulse 12: activated nodes: 11450, borderline nodes: 102, overall activation: 4717.563, activation diff: 315.861, ratio: 0.067
#   pulse 13: activated nodes: 11451, borderline nodes: 95, overall activation: 4972.696, activation diff: 255.133, ratio: 0.051
#   pulse 14: activated nodes: 11451, borderline nodes: 88, overall activation: 5177.512, activation diff: 204.817, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5177.5
#   number of spread. activ. pulses: 14
#   running time: 1625

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9839968   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9602047   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.958243   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9378269   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88370913   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8486004   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7218818   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.71937436   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7177409   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.71736145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7166009   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7152704   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7146231   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7146227   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.71448994   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.7140983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.714018   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7138512   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.71360284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7135962   REFERENCES:SIMDATES:SIMLOC
