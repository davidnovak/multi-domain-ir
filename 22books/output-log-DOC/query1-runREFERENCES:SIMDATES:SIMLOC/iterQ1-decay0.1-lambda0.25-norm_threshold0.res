###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1125.358, activation diff: 1116.556, ratio: 0.992
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2216.988, activation diff: 1143.816, ratio: 0.516
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5885.602, activation diff: 3668.614, ratio: 0.623
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 7584.461, activation diff: 1698.859, ratio: 0.224
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8227.120, activation diff: 642.659, ratio: 0.078
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8455.055, activation diff: 227.935, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8455.1
#   number of spread. activ. pulses: 7
#   running time: 1330

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99886703   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98948956   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98928034   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743793   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.93045807   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90886134   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8990009   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8985741   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.8983468   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.89833087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.89828384   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.8982283   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.89814055   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.8980781   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.8980713   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.8980522   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.89801884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_267   18   0.8979745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.8979677   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.8979528   REFERENCES:SIMDATES:SIMLOC
