###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1050.306, activation diff: 1075.427, ratio: 1.024
#   pulse 3: activated nodes: 9409, borderline nodes: 4160, overall activation: 760.118, activation diff: 1745.579, ratio: 2.296
#   pulse 4: activated nodes: 11330, borderline nodes: 4003, overall activation: 4659.185, activation diff: 4468.945, ratio: 0.959
#   pulse 5: activated nodes: 11415, borderline nodes: 645, overall activation: 4738.147, activation diff: 1683.610, ratio: 0.355
#   pulse 6: activated nodes: 11447, borderline nodes: 175, overall activation: 5784.688, activation diff: 1078.794, ratio: 0.186
#   pulse 7: activated nodes: 11454, borderline nodes: 96, overall activation: 5858.592, activation diff: 78.928, ratio: 0.013

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 5858.6
#   number of spread. activ. pulses: 7
#   running time: 1329

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99958986   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9891371   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98912674   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97121996   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9190911   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89403844   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74998784   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.74991286   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.749886   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.74986184   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7498455   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.749805   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.7498015   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.7497916   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7497742   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.7497661   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.7497413   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.74971336   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.7497081   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7496786   REFERENCES:SIMDATES:SIMLOC
