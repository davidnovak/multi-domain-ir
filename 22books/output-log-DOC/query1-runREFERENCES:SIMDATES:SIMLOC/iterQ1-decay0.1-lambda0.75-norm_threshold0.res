###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 147.512, activation diff: 135.705, ratio: 0.920
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 419.846, activation diff: 272.661, ratio: 0.649
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1113.600, activation diff: 693.754, ratio: 0.623
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2113.198, activation diff: 999.598, ratio: 0.473
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3175.674, activation diff: 1062.476, ratio: 0.335
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4154.767, activation diff: 979.093, ratio: 0.236
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5001.543, activation diff: 846.776, ratio: 0.169
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5710.352, activation diff: 708.809, ratio: 0.124
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 6292.284, activation diff: 581.932, ratio: 0.092
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 6764.033, activation diff: 471.748, ratio: 0.070
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 7143.135, activation diff: 379.102, ratio: 0.053
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 7445.891, activation diff: 302.755, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7445.9
#   number of spread. activ. pulses: 13
#   running time: 1631

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9813316   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95821744   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95686066   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93603563   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8849598   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8588353   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.8566601   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.85407984   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.85402435   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   10   0.8528364   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.85270727   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.85242754   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.8523414   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.8521593   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.85196537   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.85144085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.85132617   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   18   0.85129327   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.851264   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.8512043   REFERENCES:SIMDATES:SIMLOC
