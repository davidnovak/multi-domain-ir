###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 149.624, activation diff: 133.527, ratio: 0.892
#   pulse 3: activated nodes: 9035, borderline nodes: 5131, overall activation: 299.062, activation diff: 149.551, ratio: 0.500
#   pulse 4: activated nodes: 10550, borderline nodes: 6086, overall activation: 767.537, activation diff: 468.475, ratio: 0.610
#   pulse 5: activated nodes: 10836, borderline nodes: 4210, overall activation: 1308.096, activation diff: 540.560, ratio: 0.413
#   pulse 6: activated nodes: 10928, borderline nodes: 3737, overall activation: 1811.379, activation diff: 503.283, ratio: 0.278
#   pulse 7: activated nodes: 10958, borderline nodes: 3598, overall activation: 2198.798, activation diff: 387.418, ratio: 0.176
#   pulse 8: activated nodes: 10961, borderline nodes: 3551, overall activation: 2453.441, activation diff: 254.644, ratio: 0.104
#   pulse 9: activated nodes: 10965, borderline nodes: 3536, overall activation: 2610.529, activation diff: 157.088, ratio: 0.060
#   pulse 10: activated nodes: 10967, borderline nodes: 3528, overall activation: 2704.922, activation diff: 94.393, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 2704.9
#   number of spread. activ. pulses: 10
#   running time: 1442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969779   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9827626   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9823946   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96420634   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91039395   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88142014   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49744096   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49555284   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4951966   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49448642   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49409765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.49280933   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.49259502   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.4918787   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.49184924   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.49184775   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.4909628   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.49092248   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49031818   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.49023658   REFERENCES:SIMDATES:SIMLOC
