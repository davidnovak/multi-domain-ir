###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 133.043, activation diff: 121.235, ratio: 0.911
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 365.624, activation diff: 232.909, ratio: 0.637
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 913.849, activation diff: 548.225, ratio: 0.600
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1701.066, activation diff: 787.216, ratio: 0.463
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 2552.304, activation diff: 851.238, ratio: 0.334
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 3346.680, activation diff: 794.376, ratio: 0.237
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 4039.354, activation diff: 692.674, ratio: 0.171
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 4622.428, activation diff: 583.074, ratio: 0.126
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 5103.300, activation diff: 480.872, ratio: 0.094
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 5494.721, activation diff: 391.420, ratio: 0.071
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 5810.466, activation diff: 315.745, ratio: 0.054
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 6063.515, activation diff: 253.049, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6063.5
#   number of spread. activ. pulses: 13
#   running time: 1647

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98127174   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9579204   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9565784   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.93576133   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88465244   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8523353   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76301515   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76031107   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7583063   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.75824785   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.75705594   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7561184   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7559749   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.7559406   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   15   0.7552643   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.75523764   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.7551563   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.75508267   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.75484073   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7547228   REFERENCES:SIMDATES:SIMLOC
