###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 558.327, activation diff: 557.216, ratio: 0.998
#   pulse 3: activated nodes: 9306, borderline nodes: 4486, overall activation: 777.223, activation diff: 518.823, ratio: 0.668
#   pulse 4: activated nodes: 11281, borderline nodes: 4911, overall activation: 3134.511, activation diff: 2366.371, ratio: 0.755
#   pulse 5: activated nodes: 11397, borderline nodes: 1072, overall activation: 4665.146, activation diff: 1530.648, ratio: 0.328
#   pulse 6: activated nodes: 11433, borderline nodes: 291, overall activation: 5404.220, activation diff: 739.075, ratio: 0.137
#   pulse 7: activated nodes: 11448, borderline nodes: 128, overall activation: 5694.586, activation diff: 290.366, ratio: 0.051
#   pulse 8: activated nodes: 11450, borderline nodes: 95, overall activation: 5803.469, activation diff: 108.883, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 5803.5
#   number of spread. activ. pulses: 8
#   running time: 1350

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991959   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9883716   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9882128   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9703928   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9178687   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89256084   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7496252   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.74933684   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7492957   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7492395   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.74923307   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   12   0.74921274   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7491892   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.74913335   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.7491277   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7491257   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.74908483   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.749068   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.74905866   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7490454   REFERENCES:SIMDATES:SIMLOC
