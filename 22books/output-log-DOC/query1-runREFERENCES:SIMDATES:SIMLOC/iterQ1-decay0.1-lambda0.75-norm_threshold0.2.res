###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 42.956, activation diff: 32.653, ratio: 0.760
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 128.527, activation diff: 86.153, ratio: 0.670
#   pulse 4: activated nodes: 10495, borderline nodes: 6926, overall activation: 377.091, activation diff: 248.610, ratio: 0.659
#   pulse 5: activated nodes: 11056, borderline nodes: 5051, overall activation: 867.517, activation diff: 490.426, ratio: 0.565
#   pulse 6: activated nodes: 11289, borderline nodes: 3034, overall activation: 1634.755, activation diff: 767.238, ratio: 0.469
#   pulse 7: activated nodes: 11394, borderline nodes: 1333, overall activation: 2564.369, activation diff: 929.614, ratio: 0.363
#   pulse 8: activated nodes: 11420, borderline nodes: 576, overall activation: 3501.676, activation diff: 937.307, ratio: 0.268
#   pulse 9: activated nodes: 11439, borderline nodes: 288, overall activation: 4350.482, activation diff: 848.806, ratio: 0.195
#   pulse 10: activated nodes: 11450, borderline nodes: 145, overall activation: 5080.552, activation diff: 730.070, ratio: 0.144
#   pulse 11: activated nodes: 11453, borderline nodes: 91, overall activation: 5691.459, activation diff: 610.907, ratio: 0.107
#   pulse 12: activated nodes: 11455, borderline nodes: 68, overall activation: 6194.314, activation diff: 502.855, ratio: 0.081
#   pulse 13: activated nodes: 11455, borderline nodes: 54, overall activation: 6603.705, activation diff: 409.390, ratio: 0.062
#   pulse 14: activated nodes: 11455, borderline nodes: 40, overall activation: 6934.389, activation diff: 330.684, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6934.4
#   number of spread. activ. pulses: 14
#   running time: 1715

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98161876   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9536234   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.94891834   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.92941135   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8714647   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8637048   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.86241007   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8594997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.85821044   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8575221   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.857059   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.8567045   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.85587895   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.8555649   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.8555063   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.85547596   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.85536325   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.8550231   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.8546953   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.8544507   REFERENCES:SIMDATES:SIMLOC
