###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 992.266, activation diff: 1016.834, ratio: 1.025
#   pulse 3: activated nodes: 9182, borderline nodes: 4784, overall activation: 788.786, activation diff: 1748.513, ratio: 2.217
#   pulse 4: activated nodes: 11286, borderline nodes: 5211, overall activation: 6042.498, activation diff: 5960.044, ratio: 0.986
#   pulse 5: activated nodes: 11401, borderline nodes: 898, overall activation: 6122.435, activation diff: 2704.766, ratio: 0.442
#   pulse 6: activated nodes: 11443, borderline nodes: 166, overall activation: 7907.527, activation diff: 1839.744, ratio: 0.233
#   pulse 7: activated nodes: 11454, borderline nodes: 56, overall activation: 8042.839, activation diff: 144.590, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8042.8
#   number of spread. activ. pulses: 7
#   running time: 1343

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995535   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9880015   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9879989   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9682412   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91096103   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.8999971   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.89999485   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.8999902   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.89998186   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.89998114   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.8999804   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.8999779   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.89997476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.89997363   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.89997286   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.89997256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.8999708   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.89996934   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   19   0.8999686   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.8999624   REFERENCES:SIMDATES:SIMLOC
