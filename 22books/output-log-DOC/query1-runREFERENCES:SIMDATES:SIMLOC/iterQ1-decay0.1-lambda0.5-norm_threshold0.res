###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 531.353, activation diff: 511.525, ratio: 0.963
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1356.166, activation diff: 824.813, ratio: 0.608
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3584.102, activation diff: 2227.936, ratio: 0.622
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5490.948, activation diff: 1906.845, ratio: 0.347
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6754.754, activation diff: 1263.806, ratio: 0.187
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7524.676, activation diff: 769.922, ratio: 0.102
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7976.707, activation diff: 452.032, ratio: 0.057
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 8237.145, activation diff: 260.437, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8237.1
#   number of spread. activ. pulses: 9
#   running time: 1437

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960153   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.983904   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98326427   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96735394   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.92232096   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8977097   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8935202   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8927361   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.89201564   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8915758   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8913161   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.891101   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.8910722   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.8910571   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.89105046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   16   0.89099765   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   17   0.89091635   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.8908964   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.89085317   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.89079094   REFERENCES:SIMDATES:SIMLOC
