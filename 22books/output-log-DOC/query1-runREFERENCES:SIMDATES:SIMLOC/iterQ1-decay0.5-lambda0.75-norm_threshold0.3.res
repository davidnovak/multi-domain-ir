###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 19.110, activation diff: 9.613, ratio: 0.503
#   pulse 3: activated nodes: 7901, borderline nodes: 6875, overall activation: 45.858, activation diff: 27.497, ratio: 0.600
#   pulse 4: activated nodes: 8852, borderline nodes: 6712, overall activation: 108.180, activation diff: 62.518, ratio: 0.578
#   pulse 5: activated nodes: 9546, borderline nodes: 6219, overall activation: 218.649, activation diff: 110.481, ratio: 0.505
#   pulse 6: activated nodes: 10274, borderline nodes: 5586, overall activation: 377.849, activation diff: 159.200, ratio: 0.421
#   pulse 7: activated nodes: 10662, borderline nodes: 5046, overall activation: 581.505, activation diff: 203.655, ratio: 0.350
#   pulse 8: activated nodes: 10772, borderline nodes: 4512, overall activation: 811.360, activation diff: 229.856, ratio: 0.283
#   pulse 9: activated nodes: 10857, borderline nodes: 4105, overall activation: 1051.244, activation diff: 239.883, ratio: 0.228
#   pulse 10: activated nodes: 10912, borderline nodes: 3828, overall activation: 1292.714, activation diff: 241.470, ratio: 0.187
#   pulse 11: activated nodes: 10928, borderline nodes: 3707, overall activation: 1525.974, activation diff: 233.260, ratio: 0.153
#   pulse 12: activated nodes: 10942, borderline nodes: 3655, overall activation: 1739.170, activation diff: 213.196, ratio: 0.123
#   pulse 13: activated nodes: 10951, borderline nodes: 3615, overall activation: 1924.917, activation diff: 185.747, ratio: 0.096
#   pulse 14: activated nodes: 10958, borderline nodes: 3596, overall activation: 2081.717, activation diff: 156.799, ratio: 0.075
#   pulse 15: activated nodes: 10959, borderline nodes: 3577, overall activation: 2211.774, activation diff: 130.057, ratio: 0.059

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10959
#   final overall activation: 2211.8
#   number of spread. activ. pulses: 15
#   running time: 1688

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9817129   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9468197   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9381903   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9240325   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.86257076   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.80915576   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47986403   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4758712   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.47563052   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.47324893   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.47235465   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.47175094   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.46886104   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.46786794   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.46716434   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.46684158   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.46683913   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.466708   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.465703   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.4655118   REFERENCES:SIMDATES:SIMLOC
