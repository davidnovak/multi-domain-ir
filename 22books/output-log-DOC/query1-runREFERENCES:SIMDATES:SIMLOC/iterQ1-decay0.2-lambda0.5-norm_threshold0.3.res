###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 148.923, activation diff: 135.401, ratio: 0.909
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 328.472, activation diff: 179.916, ratio: 0.548
#   pulse 4: activated nodes: 10790, borderline nodes: 7387, overall activation: 1214.088, activation diff: 885.617, ratio: 0.729
#   pulse 5: activated nodes: 11212, borderline nodes: 3533, overall activation: 2480.247, activation diff: 1266.159, ratio: 0.510
#   pulse 6: activated nodes: 11360, borderline nodes: 1571, overall activation: 3816.948, activation diff: 1336.701, ratio: 0.350
#   pulse 7: activated nodes: 11420, borderline nodes: 536, overall activation: 4821.598, activation diff: 1004.650, ratio: 0.208
#   pulse 8: activated nodes: 11441, borderline nodes: 242, overall activation: 5471.964, activation diff: 650.366, ratio: 0.119
#   pulse 9: activated nodes: 11449, borderline nodes: 141, overall activation: 5870.344, activation diff: 398.380, ratio: 0.068
#   pulse 10: activated nodes: 11453, borderline nodes: 105, overall activation: 6108.669, activation diff: 238.325, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6108.7
#   number of spread. activ. pulses: 10
#   running time: 1539

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9964421   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98055756   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9799361   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9601321   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.90164083   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8685651   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7959256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7952312   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79447484   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.79405296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.7938808   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.79379386   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.79372627   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.7936857   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.793684   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.7934518   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.79344374   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.7934315   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   19   0.79339975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   20   0.79339576   REFERENCES:SIMDATES:SIMLOC
