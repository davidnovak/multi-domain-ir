###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 99.016, activation diff: 85.494, ratio: 0.863
#   pulse 3: activated nodes: 8623, borderline nodes: 5872, overall activation: 196.163, activation diff: 97.435, ratio: 0.497
#   pulse 4: activated nodes: 10035, borderline nodes: 6857, overall activation: 558.333, activation diff: 362.170, ratio: 0.649
#   pulse 5: activated nodes: 10616, borderline nodes: 4828, overall activation: 1016.628, activation diff: 458.295, ratio: 0.451
#   pulse 6: activated nodes: 10829, borderline nodes: 4271, overall activation: 1489.659, activation diff: 473.031, ratio: 0.318
#   pulse 7: activated nodes: 10919, borderline nodes: 3737, overall activation: 1910.816, activation diff: 421.157, ratio: 0.220
#   pulse 8: activated nodes: 10946, borderline nodes: 3632, overall activation: 2229.073, activation diff: 318.257, ratio: 0.143
#   pulse 9: activated nodes: 10959, borderline nodes: 3580, overall activation: 2437.122, activation diff: 208.049, ratio: 0.085
#   pulse 10: activated nodes: 10961, borderline nodes: 3556, overall activation: 2566.167, activation diff: 129.045, ratio: 0.050
#   pulse 11: activated nodes: 10965, borderline nodes: 3550, overall activation: 2644.644, activation diff: 78.477, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10965
#   final overall activation: 2644.6
#   number of spread. activ. pulses: 11
#   running time: 1522

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978939   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9829935   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9827965   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96346235   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9051399   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.87402093   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49841195   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49675077   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49623802   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49537122   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49507076   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.49407423   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.4937576   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.49315453   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.49290532   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.49266702   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.4919724   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.4918696   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.49149877   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.4913509   REFERENCES:SIMDATES:SIMLOC
