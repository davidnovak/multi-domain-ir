###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 30.503, activation diff: 20.201, ratio: 0.662
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 77.481, activation diff: 47.560, ratio: 0.614
#   pulse 4: activated nodes: 9537, borderline nodes: 6105, overall activation: 183.210, activation diff: 105.801, ratio: 0.577
#   pulse 5: activated nodes: 10364, borderline nodes: 5304, overall activation: 350.105, activation diff: 166.895, ratio: 0.477
#   pulse 6: activated nodes: 10745, borderline nodes: 4709, overall activation: 571.412, activation diff: 221.307, ratio: 0.387
#   pulse 7: activated nodes: 10841, borderline nodes: 4235, overall activation: 825.635, activation diff: 254.222, ratio: 0.308
#   pulse 8: activated nodes: 10912, borderline nodes: 3842, overall activation: 1093.462, activation diff: 267.827, ratio: 0.245
#   pulse 9: activated nodes: 10939, borderline nodes: 3685, overall activation: 1360.839, activation diff: 267.377, ratio: 0.196
#   pulse 10: activated nodes: 10948, borderline nodes: 3622, overall activation: 1611.814, activation diff: 250.975, ratio: 0.156
#   pulse 11: activated nodes: 10959, borderline nodes: 3585, overall activation: 1834.186, activation diff: 222.373, ratio: 0.121
#   pulse 12: activated nodes: 10959, borderline nodes: 3560, overall activation: 2023.565, activation diff: 189.379, ratio: 0.094
#   pulse 13: activated nodes: 10962, borderline nodes: 3548, overall activation: 2181.247, activation diff: 157.682, ratio: 0.072
#   pulse 14: activated nodes: 10965, borderline nodes: 3539, overall activation: 2310.830, activation diff: 129.583, ratio: 0.056
#   pulse 15: activated nodes: 10966, borderline nodes: 3533, overall activation: 2416.390, activation diff: 105.560, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10966
#   final overall activation: 2416.4
#   number of spread. activ. pulses: 15
#   running time: 1615

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98536825   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95814705   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95438826   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9360888   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.87844396   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8373842   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4828412   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47932872   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4788382   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.47747672   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.47718832   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.47520792   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.47358978   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.47277063   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.47194338   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.47143158   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.47106004   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.47090128   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.47060293   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.4700913   REFERENCES:SIMDATES:SIMLOC
