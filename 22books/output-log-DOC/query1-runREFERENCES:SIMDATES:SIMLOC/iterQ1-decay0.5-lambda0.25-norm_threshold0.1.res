###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 497.377, activation diff: 492.372, ratio: 0.990
#   pulse 3: activated nodes: 9876, borderline nodes: 3964, overall activation: 660.897, activation diff: 300.287, ratio: 0.454
#   pulse 4: activated nodes: 10863, borderline nodes: 4017, overall activation: 1774.148, activation diff: 1113.293, ratio: 0.628
#   pulse 5: activated nodes: 10952, borderline nodes: 3584, overall activation: 2435.786, activation diff: 661.638, ratio: 0.272
#   pulse 6: activated nodes: 10966, borderline nodes: 3526, overall activation: 2731.705, activation diff: 295.919, ratio: 0.108
#   pulse 7: activated nodes: 10969, borderline nodes: 3521, overall activation: 2845.633, activation diff: 113.929, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 2845.6
#   number of spread. activ. pulses: 7
#   running time: 1312

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.998484   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98785466   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98743707   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97139174   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9226723   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89925647   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49907714   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4978633   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49730143   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49657562   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49643642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.4963444   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49564472   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4953489   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.49473107   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   16   0.49450138   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   17   0.49436826   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   18   0.49432623   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.49420488   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   20   0.4941926   REFERENCES:SIMDATES:SIMLOC
