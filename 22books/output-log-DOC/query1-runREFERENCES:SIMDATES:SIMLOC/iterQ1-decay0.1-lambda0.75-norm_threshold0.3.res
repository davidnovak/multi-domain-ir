###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 23.446, activation diff: 13.949, ratio: 0.595
#   pulse 3: activated nodes: 7901, borderline nodes: 6875, overall activation: 70.490, activation diff: 47.793, ratio: 0.678
#   pulse 4: activated nodes: 9067, borderline nodes: 6911, overall activation: 203.910, activation diff: 133.580, ratio: 0.655
#   pulse 5: activated nodes: 10548, borderline nodes: 6706, overall activation: 490.875, activation diff: 286.965, ratio: 0.585
#   pulse 6: activated nodes: 11069, borderline nodes: 5240, overall activation: 1005.234, activation diff: 514.359, ratio: 0.512
#   pulse 7: activated nodes: 11275, borderline nodes: 3292, overall activation: 1755.161, activation diff: 749.927, ratio: 0.427
#   pulse 8: activated nodes: 11371, borderline nodes: 1647, overall activation: 2639.551, activation diff: 884.390, ratio: 0.335
#   pulse 9: activated nodes: 11409, borderline nodes: 824, overall activation: 3529.099, activation diff: 889.548, ratio: 0.252
#   pulse 10: activated nodes: 11424, borderline nodes: 459, overall activation: 4336.774, activation diff: 807.675, ratio: 0.186
#   pulse 11: activated nodes: 11441, borderline nodes: 243, overall activation: 5034.267, activation diff: 697.493, ratio: 0.139
#   pulse 12: activated nodes: 11447, borderline nodes: 154, overall activation: 5620.179, activation diff: 585.912, ratio: 0.104
#   pulse 13: activated nodes: 11451, borderline nodes: 108, overall activation: 6104.411, activation diff: 484.232, ratio: 0.079
#   pulse 14: activated nodes: 11452, borderline nodes: 86, overall activation: 6500.304, activation diff: 395.893, ratio: 0.061
#   pulse 15: activated nodes: 11455, borderline nodes: 65, overall activation: 6821.478, activation diff: 321.174, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6821.5
#   number of spread. activ. pulses: 15
#   running time: 1577

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.983016   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9532736   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9459117   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9286097   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.8687854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.8681112   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   7   0.86758417   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.86514235   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.86392415   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   10   0.8627026   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8624142   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.86207485   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.8613845   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   14   0.8613665   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.86108077   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.8610097   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   17   0.8610062   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.86051756   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.8604963   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.86030877   REFERENCES:SIMDATES:SIMLOC
