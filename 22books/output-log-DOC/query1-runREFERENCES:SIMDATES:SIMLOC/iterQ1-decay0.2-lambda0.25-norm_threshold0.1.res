###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 785.266, activation diff: 780.260, ratio: 0.994
#   pulse 3: activated nodes: 9876, borderline nodes: 3964, overall activation: 1286.496, activation diff: 703.549, ratio: 0.547
#   pulse 4: activated nodes: 11360, borderline nodes: 3038, overall activation: 4194.655, activation diff: 2908.693, ratio: 0.693
#   pulse 5: activated nodes: 11429, borderline nodes: 413, overall activation: 5776.257, activation diff: 1581.608, ratio: 0.274
#   pulse 6: activated nodes: 11452, borderline nodes: 89, overall activation: 6432.294, activation diff: 656.037, ratio: 0.102
#   pulse 7: activated nodes: 11457, borderline nodes: 40, overall activation: 6677.690, activation diff: 245.396, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6677.7
#   number of spread. activ. pulses: 7
#   running time: 1383

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985176   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9878996   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9878919   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714824   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9231313   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8994618   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79879   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.798218   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79807305   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.79806846   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.797889   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.7978637   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.79781795   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.79773855   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7977128   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.7977072   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.79768074   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   18   0.79768014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   19   0.79765815   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   20   0.79760134   REFERENCES:SIMDATES:SIMLOC
