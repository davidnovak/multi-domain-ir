###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 377.042, activation diff: 375.932, ratio: 0.997
#   pulse 3: activated nodes: 9306, borderline nodes: 4486, overall activation: 436.639, activation diff: 264.845, ratio: 0.607
#   pulse 4: activated nodes: 10707, borderline nodes: 5195, overall activation: 1473.131, activation diff: 1038.305, ratio: 0.705
#   pulse 5: activated nodes: 10930, borderline nodes: 3772, overall activation: 2164.098, activation diff: 690.967, ratio: 0.319
#   pulse 6: activated nodes: 10957, borderline nodes: 3602, overall activation: 2563.262, activation diff: 399.164, ratio: 0.156
#   pulse 7: activated nodes: 10963, borderline nodes: 3538, overall activation: 2730.883, activation diff: 167.621, ratio: 0.061
#   pulse 8: activated nodes: 10967, borderline nodes: 3527, overall activation: 2797.081, activation diff: 66.198, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 2797.1
#   number of spread. activ. pulses: 8
#   running time: 1341

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991597   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98827744   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98782164   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97032607   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91757697   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8922188   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4996016   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4986332   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49791378   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.49718654   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4970234   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49696892   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49664748   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4960373   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49543303   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4953198   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.495153   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.4950528   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   19   0.4949984   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.494709   REFERENCES:SIMDATES:SIMLOC
