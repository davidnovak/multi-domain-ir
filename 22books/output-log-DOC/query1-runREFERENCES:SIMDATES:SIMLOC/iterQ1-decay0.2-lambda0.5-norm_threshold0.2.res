###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 228.493, activation diff: 212.396, ratio: 0.930
#   pulse 3: activated nodes: 9035, borderline nodes: 5131, overall activation: 515.945, activation diff: 287.624, ratio: 0.557
#   pulse 4: activated nodes: 11132, borderline nodes: 6203, overall activation: 1728.050, activation diff: 1212.104, ratio: 0.701
#   pulse 5: activated nodes: 11350, borderline nodes: 1887, overall activation: 3221.005, activation diff: 1492.955, ratio: 0.464
#   pulse 6: activated nodes: 11418, borderline nodes: 647, overall activation: 4480.734, activation diff: 1259.729, ratio: 0.281
#   pulse 7: activated nodes: 11443, borderline nodes: 208, overall activation: 5327.353, activation diff: 846.619, ratio: 0.159
#   pulse 8: activated nodes: 11453, borderline nodes: 106, overall activation: 5850.950, activation diff: 523.597, ratio: 0.089
#   pulse 9: activated nodes: 11454, borderline nodes: 72, overall activation: 6163.861, activation diff: 312.911, ratio: 0.051
#   pulse 10: activated nodes: 11456, borderline nodes: 58, overall activation: 6347.818, activation diff: 183.957, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 6347.8
#   number of spread. activ. pulses: 10
#   running time: 1592

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970584   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98343027   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9833634   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9646498   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9112141   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8828192   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7963915   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79573184   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79519093   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7948154   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.7948084   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.79471624   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.79468334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.7946488   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.79452884   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.7945103   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.7944371   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.7944048   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   19   0.7943781   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.79436934   REFERENCES:SIMDATES:SIMLOC
