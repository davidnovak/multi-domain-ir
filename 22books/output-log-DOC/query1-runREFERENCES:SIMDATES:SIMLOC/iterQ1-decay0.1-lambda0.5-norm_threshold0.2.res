###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 254.782, activation diff: 238.686, ratio: 0.937
#   pulse 3: activated nodes: 9035, borderline nodes: 5131, overall activation: 595.563, activation diff: 340.971, ratio: 0.573
#   pulse 4: activated nodes: 11146, borderline nodes: 6148, overall activation: 2170.186, activation diff: 1574.623, ratio: 0.726
#   pulse 5: activated nodes: 11366, borderline nodes: 1668, overall activation: 4087.350, activation diff: 1917.164, ratio: 0.469
#   pulse 6: activated nodes: 11430, borderline nodes: 476, overall activation: 5650.123, activation diff: 1562.773, ratio: 0.277
#   pulse 7: activated nodes: 11450, borderline nodes: 134, overall activation: 6683.958, activation diff: 1033.835, ratio: 0.155
#   pulse 8: activated nodes: 11455, borderline nodes: 66, overall activation: 7319.749, activation diff: 635.791, ratio: 0.087
#   pulse 9: activated nodes: 11456, borderline nodes: 39, overall activation: 7698.498, activation diff: 378.749, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 7698.5
#   number of spread. activ. pulses: 9
#   running time: 1427

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99457484   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97847164   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97815996   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.95894134   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9048046   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89194137   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89132655   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.889884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.8895421   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   10   0.88938046   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.88921344   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8890091   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.88896745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.8889668   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.88888913   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.8888639   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.8887259   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.8887187   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.8887186   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.8886415   REFERENCES:SIMDATES:SIMLOC
