###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 316.885, activation diff: 298.596, ratio: 0.942
#   pulse 3: activated nodes: 9496, borderline nodes: 4063, overall activation: 727.950, activation diff: 411.069, ratio: 0.565
#   pulse 4: activated nodes: 11314, borderline nodes: 4121, overall activation: 2042.311, activation diff: 1314.361, ratio: 0.644
#   pulse 5: activated nodes: 11404, borderline nodes: 811, overall activation: 3421.593, activation diff: 1379.282, ratio: 0.403
#   pulse 6: activated nodes: 11437, borderline nodes: 251, overall activation: 4434.959, activation diff: 1013.366, ratio: 0.228
#   pulse 7: activated nodes: 11449, borderline nodes: 111, overall activation: 5080.750, activation diff: 645.791, ratio: 0.127
#   pulse 8: activated nodes: 11451, borderline nodes: 78, overall activation: 5470.041, activation diff: 389.291, ratio: 0.071
#   pulse 9: activated nodes: 11451, borderline nodes: 57, overall activation: 5698.977, activation diff: 228.936, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5699.0
#   number of spread. activ. pulses: 9
#   running time: 1468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99538755   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98133695   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9809876   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9633855   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9138645   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88648105   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74391556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7427138   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.74190867   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7419045   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.74117804   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7409824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.74095356   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   14   0.7409477   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.74090815   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.740868   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.7407718   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.74053574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.74043256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   20   0.74042034   REFERENCES:SIMDATES:SIMLOC
