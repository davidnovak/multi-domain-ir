###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1002.602, activation diff: 993.800, ratio: 0.991
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1873.414, activation diff: 919.754, ratio: 0.491
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 4808.513, activation diff: 2935.099, ratio: 0.610
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6196.489, activation diff: 1387.976, ratio: 0.224
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6727.538, activation diff: 531.049, ratio: 0.079
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6918.052, activation diff: 190.514, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6918.1
#   number of spread. activ. pulses: 7
#   running time: 1366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988636   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9894587   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9892801   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9743753   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9304452   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90886015   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79909194   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7985104   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.79844975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7984201   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.79827535   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   12   0.79824775   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.79816574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7981278   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.79807836   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.7980437   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   17   0.79804087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.79802954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.7980253   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.79802334   REFERENCES:SIMDATES:SIMLOC
