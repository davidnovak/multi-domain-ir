###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 438.670, activation diff: 440.503, ratio: 1.004
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 537.012, activation diff: 458.280, ratio: 0.853
#   pulse 4: activated nodes: 11155, borderline nodes: 6404, overall activation: 2904.719, activation diff: 2398.091, ratio: 0.826
#   pulse 5: activated nodes: 11353, borderline nodes: 1728, overall activation: 4671.277, activation diff: 1767.104, ratio: 0.378
#   pulse 6: activated nodes: 11423, borderline nodes: 579, overall activation: 5738.853, activation diff: 1067.576, ratio: 0.186
#   pulse 7: activated nodes: 11447, borderline nodes: 167, overall activation: 6185.918, activation diff: 447.065, ratio: 0.072
#   pulse 8: activated nodes: 11453, borderline nodes: 101, overall activation: 6358.049, activation diff: 172.132, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6358.0
#   number of spread. activ. pulses: 8
#   running time: 1429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990258   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.98684454   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98670137   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9671953   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9093412   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8813159   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7995155   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7992467   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7992185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7992108   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7991802   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   12   0.7991729   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7991515   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.7991214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.7991189   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7991109   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7990817   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.7990769   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.79907346   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   20   0.79906666   REFERENCES:SIMDATES:SIMLOC
