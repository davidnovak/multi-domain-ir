###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 39.842, activation diff: 29.540, ratio: 0.741
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 115.365, activation diff: 76.104, ratio: 0.660
#   pulse 4: activated nodes: 10428, borderline nodes: 6880, overall activation: 321.488, activation diff: 206.174, ratio: 0.641
#   pulse 5: activated nodes: 11031, borderline nodes: 5247, overall activation: 706.841, activation diff: 385.353, ratio: 0.545
#   pulse 6: activated nodes: 11264, borderline nodes: 3478, overall activation: 1287.496, activation diff: 580.655, ratio: 0.451
#   pulse 7: activated nodes: 11369, borderline nodes: 1661, overall activation: 1999.884, activation diff: 712.388, ratio: 0.356
#   pulse 8: activated nodes: 11409, borderline nodes: 812, overall activation: 2737.499, activation diff: 737.615, ratio: 0.269
#   pulse 9: activated nodes: 11428, borderline nodes: 443, overall activation: 3417.888, activation diff: 680.389, ratio: 0.199
#   pulse 10: activated nodes: 11437, borderline nodes: 233, overall activation: 4009.316, activation diff: 591.428, ratio: 0.148
#   pulse 11: activated nodes: 11446, borderline nodes: 161, overall activation: 4507.472, activation diff: 498.156, ratio: 0.111
#   pulse 12: activated nodes: 11453, borderline nodes: 124, overall activation: 4919.130, activation diff: 411.658, ratio: 0.084
#   pulse 13: activated nodes: 11453, borderline nodes: 93, overall activation: 5255.239, activation diff: 336.109, ratio: 0.064
#   pulse 14: activated nodes: 11454, borderline nodes: 79, overall activation: 5527.415, activation diff: 272.176, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 5527.4
#   number of spread. activ. pulses: 14
#   running time: 1677

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9814594   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95290565   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9480496   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.92881286   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8708637   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.82807183   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7671017   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76519966   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7628635   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.76169264   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7609715   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.75980926   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.75911885   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.75886214   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7587406   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.75815886   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.75790375   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.75789833   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.75758123   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   20   0.75748116   REFERENCES:SIMDATES:SIMLOC
