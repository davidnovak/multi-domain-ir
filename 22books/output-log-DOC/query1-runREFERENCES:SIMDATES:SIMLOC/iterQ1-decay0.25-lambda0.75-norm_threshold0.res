###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 125.808, activation diff: 114.000, ratio: 0.906
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 339.311, activation diff: 213.831, ratio: 0.630
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 822.009, activation diff: 482.697, ratio: 0.587
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1510.290, activation diff: 688.282, ratio: 0.456
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 2261.511, activation diff: 751.220, ratio: 0.332
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 2967.304, activation diff: 705.794, ratio: 0.238
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 3585.511, activation diff: 618.206, ratio: 0.172
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 4107.514, activation diff: 522.004, ratio: 0.127
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 4538.950, activation diff: 431.436, ratio: 0.095
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 4890.735, activation diff: 351.785, ratio: 0.072
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 5174.942, activation diff: 284.207, ratio: 0.055
#   pulse 13: activated nodes: 11464, borderline nodes: 21, overall activation: 5403.041, activation diff: 228.099, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5403.0
#   number of spread. activ. pulses: 13
#   running time: 1468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812366   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.95773834   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9564075   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9355958   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88446385   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.85203874   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7150895   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.71197677   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7103772   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.71036446   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7092587   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.70785   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.70765114   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.7075482   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7073638   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.70713043   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   17   0.70702404   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.7065467   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.70654225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   20   0.7064254   REFERENCES:SIMDATES:SIMLOC
