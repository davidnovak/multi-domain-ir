###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 215.348, activation diff: 199.251, ratio: 0.925
#   pulse 3: activated nodes: 9035, borderline nodes: 5131, overall activation: 477.427, activation diff: 262.240, ratio: 0.549
#   pulse 4: activated nodes: 11123, borderline nodes: 6232, overall activation: 1530.244, activation diff: 1052.817, ratio: 0.688
#   pulse 5: activated nodes: 11344, borderline nodes: 2029, overall activation: 2826.106, activation diff: 1295.863, ratio: 0.459
#   pulse 6: activated nodes: 11416, borderline nodes: 759, overall activation: 3941.449, activation diff: 1115.342, ratio: 0.283
#   pulse 7: activated nodes: 11431, borderline nodes: 274, overall activation: 4697.565, activation diff: 756.117, ratio: 0.161
#   pulse 8: activated nodes: 11445, borderline nodes: 161, overall activation: 5166.819, activation diff: 469.253, ratio: 0.091
#   pulse 9: activated nodes: 11446, borderline nodes: 131, overall activation: 5447.189, activation diff: 280.370, ratio: 0.051
#   pulse 10: activated nodes: 11448, borderline nodes: 114, overall activation: 5611.799, activation diff: 164.610, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5611.8
#   number of spread. activ. pulses: 10
#   running time: 1441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970498   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9833473   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9833033   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96460414   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.91113687   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8826653   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7465963   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7456838   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7453792   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.74489653   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.74484515   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7447096   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.7446585   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   14   0.74463654   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.7445427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   16   0.74450755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.74443203   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.7444212   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.7443967   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.74435   REFERENCES:SIMDATES:SIMLOC
