###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 278.595, activation diff: 280.427, ratio: 1.007
#   pulse 3: activated nodes: 8986, borderline nodes: 5239, overall activation: 276.765, activation diff: 226.375, ratio: 0.818
#   pulse 4: activated nodes: 10517, borderline nodes: 6300, overall activation: 1163.733, activation diff: 898.478, ratio: 0.772
#   pulse 5: activated nodes: 10823, borderline nodes: 4117, overall activation: 1795.931, activation diff: 632.308, ratio: 0.352
#   pulse 6: activated nodes: 10921, borderline nodes: 3755, overall activation: 2313.063, activation diff: 517.131, ratio: 0.224
#   pulse 7: activated nodes: 10958, borderline nodes: 3598, overall activation: 2575.860, activation diff: 262.798, ratio: 0.102
#   pulse 8: activated nodes: 10961, borderline nodes: 3555, overall activation: 2685.362, activation diff: 109.502, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10961
#   final overall activation: 2685.4
#   number of spread. activ. pulses: 8
#   running time: 1366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9989639   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9864365   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98555565   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9669635   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9088006   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88030034   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49951816   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49840832   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4975862   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49665076   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.49663383   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49656653   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49590495   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.49552155   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.49471906   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   16   0.49433133   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.49426427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.49413514   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.49409422   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.49408582   REFERENCES:SIMDATES:SIMLOC
