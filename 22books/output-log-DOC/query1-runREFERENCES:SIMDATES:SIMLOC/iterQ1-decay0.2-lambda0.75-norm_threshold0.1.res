###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 73.667, activation diff: 62.594, ratio: 0.850
#   pulse 3: activated nodes: 9081, borderline nodes: 5011, overall activation: 206.410, activation diff: 133.183, ratio: 0.645
#   pulse 4: activated nodes: 11093, borderline nodes: 5468, overall activation: 552.929, activation diff: 346.537, ratio: 0.627
#   pulse 5: activated nodes: 11299, borderline nodes: 2778, overall activation: 1136.009, activation diff: 583.080, ratio: 0.513
#   pulse 6: activated nodes: 11400, borderline nodes: 1166, overall activation: 1887.932, activation diff: 751.923, ratio: 0.398
#   pulse 7: activated nodes: 11429, borderline nodes: 480, overall activation: 2673.713, activation diff: 785.781, ratio: 0.294
#   pulse 8: activated nodes: 11440, borderline nodes: 205, overall activation: 3398.684, activation diff: 724.971, ratio: 0.213
#   pulse 9: activated nodes: 11453, borderline nodes: 122, overall activation: 4027.387, activation diff: 628.702, ratio: 0.156
#   pulse 10: activated nodes: 11454, borderline nodes: 79, overall activation: 4555.232, activation diff: 527.845, ratio: 0.116
#   pulse 11: activated nodes: 11454, borderline nodes: 63, overall activation: 4990.058, activation diff: 434.826, ratio: 0.087
#   pulse 12: activated nodes: 11456, borderline nodes: 51, overall activation: 5343.966, activation diff: 353.908, ratio: 0.066
#   pulse 13: activated nodes: 11456, borderline nodes: 37, overall activation: 5629.644, activation diff: 285.677, ratio: 0.051
#   pulse 14: activated nodes: 11457, borderline nodes: 34, overall activation: 5858.866, activation diff: 229.223, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 5858.9
#   number of spread. activ. pulses: 14
#   running time: 1619

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9840451   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.96045685   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9585221   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9380431   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.883945   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8489878   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7702824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7681816   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.7661257   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7657764   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7649103   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7640497   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7632849   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.7632753   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.7630231   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.76298124   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.762967   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.7628494   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.76241606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7623802   REFERENCES:SIMDATES:SIMLOC
