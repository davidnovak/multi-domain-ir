###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 376.177, activation diff: 357.887, ratio: 0.951
#   pulse 3: activated nodes: 9496, borderline nodes: 4063, overall activation: 915.120, activation diff: 538.949, ratio: 0.589
#   pulse 4: activated nodes: 11322, borderline nodes: 3956, overall activation: 2861.202, activation diff: 1946.082, ratio: 0.680
#   pulse 5: activated nodes: 11422, borderline nodes: 647, overall activation: 4833.104, activation diff: 1971.902, ratio: 0.408
#   pulse 6: activated nodes: 11449, borderline nodes: 136, overall activation: 6239.887, activation diff: 1406.783, ratio: 0.225
#   pulse 7: activated nodes: 11455, borderline nodes: 52, overall activation: 7124.889, activation diff: 885.002, ratio: 0.124
#   pulse 8: activated nodes: 11459, borderline nodes: 22, overall activation: 7655.388, activation diff: 530.498, ratio: 0.069
#   pulse 9: activated nodes: 11460, borderline nodes: 13, overall activation: 7965.800, activation diff: 310.413, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 7965.8
#   number of spread. activ. pulses: 9
#   running time: 1367

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.995407   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98151946   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9810574   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9634968   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9140785   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.89276433   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89207417   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.8910426   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.89059794   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   10   0.89032847   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.8902981   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8901261   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   13   0.8901046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   14   0.89005744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.89000416   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.88997746   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.8898733   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.8898185   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.8898103   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   20   0.88968533   REFERENCES:SIMDATES:SIMLOC
