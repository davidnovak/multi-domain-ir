###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 38.286, activation diff: 27.983, ratio: 0.731
#   pulse 3: activated nodes: 8447, borderline nodes: 6341, overall activation: 108.888, activation diff: 71.183, ratio: 0.654
#   pulse 4: activated nodes: 10403, borderline nodes: 6873, overall activation: 295.636, activation diff: 186.803, ratio: 0.632
#   pulse 5: activated nodes: 10983, borderline nodes: 5319, overall activation: 635.375, activation diff: 339.739, ratio: 0.535
#   pulse 6: activated nodes: 11245, borderline nodes: 3725, overall activation: 1136.743, activation diff: 501.368, ratio: 0.441
#   pulse 7: activated nodes: 11365, borderline nodes: 1886, overall activation: 1748.689, activation diff: 611.946, ratio: 0.350
#   pulse 8: activated nodes: 11407, borderline nodes: 970, overall activation: 2391.201, activation diff: 642.511, ratio: 0.269
#   pulse 9: activated nodes: 11421, borderline nodes: 532, overall activation: 2990.917, activation diff: 599.716, ratio: 0.201
#   pulse 10: activated nodes: 11429, borderline nodes: 308, overall activation: 3515.132, activation diff: 524.215, ratio: 0.149
#   pulse 11: activated nodes: 11437, borderline nodes: 212, overall activation: 3958.475, activation diff: 443.343, ratio: 0.112
#   pulse 12: activated nodes: 11442, borderline nodes: 175, overall activation: 4325.859, activation diff: 367.385, ratio: 0.085
#   pulse 13: activated nodes: 11445, borderline nodes: 155, overall activation: 4626.288, activation diff: 300.429, ratio: 0.065
#   pulse 14: activated nodes: 11447, borderline nodes: 142, overall activation: 4869.794, activation diff: 243.506, ratio: 0.050
#   pulse 15: activated nodes: 11447, borderline nodes: 125, overall activation: 5065.934, activation diff: 196.140, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 5065.9
#   number of spread. activ. pulses: 15
#   running time: 1526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9858851   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.96102417   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95736074   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9381609   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.88093096   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84154075   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7265961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7246349   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.72308856   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.7219478   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.7218945   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.7204588   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.719864   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.7197516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7194649   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7190074   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.7188949   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.71882486   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.7186589   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.7184011   REFERENCES:SIMDATES:SIMLOC
