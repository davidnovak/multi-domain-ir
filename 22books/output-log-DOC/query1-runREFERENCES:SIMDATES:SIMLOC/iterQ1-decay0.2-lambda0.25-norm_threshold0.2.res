###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 594.584, activation diff: 593.473, ratio: 0.998
#   pulse 3: activated nodes: 9306, borderline nodes: 4486, overall activation: 853.619, activation diff: 577.349, ratio: 0.676
#   pulse 4: activated nodes: 11289, borderline nodes: 4868, overall activation: 3560.250, activation diff: 2716.446, ratio: 0.763
#   pulse 5: activated nodes: 11399, borderline nodes: 1005, overall activation: 5288.144, activation diff: 1727.913, ratio: 0.327
#   pulse 6: activated nodes: 11440, borderline nodes: 241, overall activation: 6111.101, activation diff: 822.956, ratio: 0.135
#   pulse 7: activated nodes: 11453, borderline nodes: 85, overall activation: 6434.043, activation diff: 322.943, ratio: 0.050
#   pulse 8: activated nodes: 11456, borderline nodes: 51, overall activation: 6556.023, activation diff: 121.980, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 6556.0
#   number of spread. activ. pulses: 8
#   running time: 1405

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991994   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9883791   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.988235   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9703983   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9178952   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8925882   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79960716   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7993529   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7993388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.79931736   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.79928064   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.799259   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7992387   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   14   0.7992334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7992331   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.79922783   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.7992049   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.7991992   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.7991988   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7991978   REFERENCES:SIMDATES:SIMLOC
