###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 89.634, activation diff: 77.826, ratio: 0.868
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 216.399, activation diff: 127.092, ratio: 0.587
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 444.591, activation diff: 228.196, ratio: 0.513
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 741.544, activation diff: 296.953, ratio: 0.400
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1067.875, activation diff: 326.331, ratio: 0.306
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1387.442, activation diff: 319.567, ratio: 0.230
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 1675.938, activation diff: 288.495, ratio: 0.172
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 1923.871, activation diff: 247.933, ratio: 0.129
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 2131.145, activation diff: 207.275, ratio: 0.097
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 2301.551, activation diff: 170.406, ratio: 0.074
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 2440.076, activation diff: 138.524, ratio: 0.057
#   pulse 13: activated nodes: 10971, borderline nodes: 3512, overall activation: 2551.780, activation diff: 111.705, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2551.8
#   number of spread. activ. pulses: 13
#   running time: 1502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98097104   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9561841   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.95501226   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9342636   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.8828323   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.849723   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47521275   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.47114855   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.47071517   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.46999958   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.46926695   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.46683043   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.4653727   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.46482003   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.46395034   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.46362188   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.4632524   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.46279946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.46278203   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.46246845   REFERENCES:SIMDATES:SIMLOC
