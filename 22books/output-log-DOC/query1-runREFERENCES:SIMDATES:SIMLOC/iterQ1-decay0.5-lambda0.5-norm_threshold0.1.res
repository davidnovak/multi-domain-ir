###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 218.066, activation diff: 199.777, ratio: 0.916
#   pulse 3: activated nodes: 9496, borderline nodes: 4063, overall activation: 445.568, activation diff: 227.505, ratio: 0.511
#   pulse 4: activated nodes: 10792, borderline nodes: 4510, overall activation: 1021.985, activation diff: 576.417, ratio: 0.564
#   pulse 5: activated nodes: 10939, borderline nodes: 3685, overall activation: 1620.795, activation diff: 598.810, ratio: 0.369
#   pulse 6: activated nodes: 10959, borderline nodes: 3558, overall activation: 2104.433, activation diff: 483.638, ratio: 0.230
#   pulse 7: activated nodes: 10966, borderline nodes: 3526, overall activation: 2428.013, activation diff: 323.580, ratio: 0.133
#   pulse 8: activated nodes: 10969, borderline nodes: 3522, overall activation: 2627.928, activation diff: 199.915, ratio: 0.076
#   pulse 9: activated nodes: 10969, borderline nodes: 3520, overall activation: 2747.356, activation diff: 119.428, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 2747.4
#   number of spread. activ. pulses: 9
#   running time: 1499

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99532413   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9806762   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.98058116   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96301126   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9130714   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.885411   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4956296   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4932138   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49319977   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49266014   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49210948   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.49021408   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.49017   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.49000084   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.48959577   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.48902386   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.48882243   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.4885059   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.48819792   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.48797458   REFERENCES:SIMDATES:SIMLOC
