###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 881.229, activation diff: 876.223, ratio: 0.994
#   pulse 3: activated nodes: 9876, borderline nodes: 3964, overall activation: 1523.190, activation diff: 864.446, ratio: 0.568
#   pulse 4: activated nodes: 11363, borderline nodes: 2931, overall activation: 5203.177, activation diff: 3680.597, ratio: 0.707
#   pulse 5: activated nodes: 11435, borderline nodes: 353, overall activation: 7147.237, activation diff: 1944.060, ratio: 0.272
#   pulse 6: activated nodes: 11454, borderline nodes: 60, overall activation: 7942.868, activation diff: 795.631, ratio: 0.100
#   pulse 7: activated nodes: 11460, borderline nodes: 20, overall activation: 8238.680, activation diff: 295.812, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8238.7
#   number of spread. activ. pulses: 7
#   running time: 1331

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985225   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98795146   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98790216   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9714927   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9231742   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8994701   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8986553   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89827263   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.8979621   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.8979063   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   11   0.89785516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.897833   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   13   0.8977686   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.89770484   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   15   0.8976731   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.89761674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.89760685   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.8976001   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   19   0.897596   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.897591   REFERENCES:SIMDATES:SIMLOC
