###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 307.427, activation diff: 289.450, ratio: 0.942
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 622.679, activation diff: 315.269, ratio: 0.506
#   pulse 4: activated nodes: 10536, borderline nodes: 3964, overall activation: 1515.776, activation diff: 893.097, ratio: 0.589
#   pulse 5: activated nodes: 11189, borderline nodes: 2083, overall activation: 2399.755, activation diff: 883.979, ratio: 0.368
#   pulse 6: activated nodes: 11238, borderline nodes: 818, overall activation: 3085.502, activation diff: 685.747, ratio: 0.222
#   pulse 7: activated nodes: 11260, borderline nodes: 582, overall activation: 3556.665, activation diff: 471.163, ratio: 0.132
#   pulse 8: activated nodes: 11273, borderline nodes: 538, overall activation: 3861.325, activation diff: 304.660, ratio: 0.079
#   pulse 9: activated nodes: 11273, borderline nodes: 526, overall activation: 4051.911, activation diff: 190.586, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11273
#   final overall activation: 4051.9
#   number of spread. activ. pulses: 9
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99533063   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9806497   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9792328   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9615642   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91373503   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.885   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7434741   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74165463   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74074996   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.74004614   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.7399015   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.73965335   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.7395879   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.73936576   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.7391945   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.73898554   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   17   0.7389759   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   18   0.7387072   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   19   0.7386718   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.7384932   REFERENCES
