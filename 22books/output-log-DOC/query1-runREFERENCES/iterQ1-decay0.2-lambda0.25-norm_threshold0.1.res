###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 754.661, activation diff: 754.212, ratio: 0.999
#   pulse 3: activated nodes: 8645, borderline nodes: 3407, overall activation: 976.693, activation diff: 574.593, ratio: 0.588
#   pulse 4: activated nodes: 10770, borderline nodes: 3079, overall activation: 2918.097, activation diff: 1972.266, ratio: 0.676
#   pulse 5: activated nodes: 11233, borderline nodes: 1474, overall activation: 4135.287, activation diff: 1217.393, ratio: 0.294
#   pulse 6: activated nodes: 11295, borderline nodes: 383, overall activation: 4732.826, activation diff: 597.539, ratio: 0.126
#   pulse 7: activated nodes: 11317, borderline nodes: 203, overall activation: 4989.820, activation diff: 256.993, ratio: 0.052
#   pulse 8: activated nodes: 11324, borderline nodes: 146, overall activation: 5095.293, activation diff: 105.473, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 5095.3
#   number of spread. activ. pulses: 8
#   running time: 697

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993253   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9894937   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98802555   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9728557   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92549634   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90220624   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7995853   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.7991797   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.7991754   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.79916716   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.7991669   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.7991252   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.79911184   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   14   0.79909396   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.79905355   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   16   0.7990499   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   17   0.7990435   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.79900694   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.7989675   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.7989299   REFERENCES
