###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 39.552, activation diff: 29.250, ratio: 0.740
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 111.704, activation diff: 72.753, ratio: 0.651
#   pulse 4: activated nodes: 9143, borderline nodes: 5869, overall activation: 296.450, activation diff: 184.810, ratio: 0.623
#   pulse 5: activated nodes: 10184, borderline nodes: 4894, overall activation: 609.546, activation diff: 313.095, ratio: 0.514
#   pulse 6: activated nodes: 10749, borderline nodes: 3981, overall activation: 1032.140, activation diff: 422.594, ratio: 0.409
#   pulse 7: activated nodes: 11021, borderline nodes: 2885, overall activation: 1509.624, activation diff: 477.484, ratio: 0.316
#   pulse 8: activated nodes: 11155, borderline nodes: 2078, overall activation: 1992.184, activation diff: 482.560, ratio: 0.242
#   pulse 9: activated nodes: 11212, borderline nodes: 1404, overall activation: 2447.493, activation diff: 455.309, ratio: 0.186
#   pulse 10: activated nodes: 11230, borderline nodes: 974, overall activation: 2858.400, activation diff: 410.906, ratio: 0.144
#   pulse 11: activated nodes: 11270, borderline nodes: 777, overall activation: 3218.341, activation diff: 359.942, ratio: 0.112
#   pulse 12: activated nodes: 11283, borderline nodes: 655, overall activation: 3527.161, activation diff: 308.819, ratio: 0.088
#   pulse 13: activated nodes: 11290, borderline nodes: 592, overall activation: 3788.216, activation diff: 261.056, ratio: 0.069
#   pulse 14: activated nodes: 11295, borderline nodes: 522, overall activation: 4006.523, activation diff: 218.307, ratio: 0.054
#   pulse 15: activated nodes: 11307, borderline nodes: 483, overall activation: 4187.669, activation diff: 181.146, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11307
#   final overall activation: 4187.7
#   number of spread. activ. pulses: 15
#   running time: 548

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98590183   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9583898   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.95693046   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9353713   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8810621   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.83856916   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.77435786   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.77252483   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7696461   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   10   0.76877046   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.76834726   REFERENCES
1   Q1   essentialsinmod01howegoog_474   12   0.7667978   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.76667297   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7663716   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.7656684   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.7655472   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.7655029   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   18   0.7652248   REFERENCES
1   Q1   essentialsinmod01howegoog_471   19   0.76490796   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.7647483   REFERENCES
