###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 211.961, activation diff: 196.589, ratio: 0.927
#   pulse 3: activated nodes: 7946, borderline nodes: 4186, overall activation: 428.325, activation diff: 216.857, ratio: 0.506
#   pulse 4: activated nodes: 9961, borderline nodes: 5349, overall activation: 1209.023, activation diff: 780.698, ratio: 0.646
#   pulse 5: activated nodes: 10956, borderline nodes: 3274, overall activation: 2046.500, activation diff: 837.477, ratio: 0.409
#   pulse 6: activated nodes: 11158, borderline nodes: 1905, overall activation: 2751.742, activation diff: 705.242, ratio: 0.256
#   pulse 7: activated nodes: 11221, borderline nodes: 1044, overall activation: 3261.282, activation diff: 509.541, ratio: 0.156
#   pulse 8: activated nodes: 11231, borderline nodes: 762, overall activation: 3601.598, activation diff: 340.316, ratio: 0.094
#   pulse 9: activated nodes: 11246, borderline nodes: 661, overall activation: 3819.194, activation diff: 217.596, ratio: 0.057
#   pulse 10: activated nodes: 11247, borderline nodes: 635, overall activation: 3954.896, activation diff: 135.702, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11247
#   final overall activation: 3954.9
#   number of spread. activ. pulses: 10
#   running time: 447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99700785   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98303765   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98119164   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9631976   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91104364   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.88128316   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7463006   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7446753   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74436414   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.74378055   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7436166   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.74351716   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.7433107   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.7431198   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.7431121   REFERENCES
1   Q1   europesincenapol00leveuoft_16   16   0.74306756   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.74304384   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   18   0.7430187   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   19   0.74295926   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.7427685   REFERENCES
