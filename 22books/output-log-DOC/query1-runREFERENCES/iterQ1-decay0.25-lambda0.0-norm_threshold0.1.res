###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1244.666, activation diff: 1276.166, ratio: 1.025
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 604.523, activation diff: 1849.190, ratio: 3.059
#   pulse 4: activated nodes: 10803, borderline nodes: 2677, overall activation: 3170.675, activation diff: 3775.198, ratio: 1.191
#   pulse 5: activated nodes: 11255, borderline nodes: 1036, overall activation: 1054.327, activation diff: 4225.002, ratio: 4.007
#   pulse 6: activated nodes: 11288, borderline nodes: 531, overall activation: 3269.235, activation diff: 4323.562, ratio: 1.322
#   pulse 7: activated nodes: 11304, borderline nodes: 472, overall activation: 1082.998, activation diff: 4352.233, ratio: 4.019
#   pulse 8: activated nodes: 11304, borderline nodes: 472, overall activation: 3275.341, activation diff: 4358.339, ratio: 1.331
#   pulse 9: activated nodes: 11304, borderline nodes: 472, overall activation: 1085.455, activation diff: 4360.796, ratio: 4.017
#   pulse 10: activated nodes: 11304, borderline nodes: 472, overall activation: 3275.947, activation diff: 4361.402, ratio: 1.331
#   pulse 11: activated nodes: 11304, borderline nodes: 472, overall activation: 1085.755, activation diff: 4361.702, ratio: 4.017
#   pulse 12: activated nodes: 11304, borderline nodes: 472, overall activation: 3276.021, activation diff: 4361.776, ratio: 1.331
#   pulse 13: activated nodes: 11304, borderline nodes: 472, overall activation: 1085.798, activation diff: 4361.819, ratio: 4.017
#   pulse 14: activated nodes: 11304, borderline nodes: 472, overall activation: 3276.032, activation diff: 4361.830, ratio: 1.331
#   pulse 15: activated nodes: 11304, borderline nodes: 472, overall activation: 1085.805, activation diff: 4361.837, ratio: 4.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 1085.8
#   number of spread. activ. pulses: 15
#   running time: 544

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
