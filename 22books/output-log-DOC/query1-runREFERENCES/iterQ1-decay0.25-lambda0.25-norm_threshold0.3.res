###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 406.092, activation diff: 411.297, ratio: 1.013
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 398.913, activation diff: 384.751, ratio: 0.964
#   pulse 4: activated nodes: 9881, borderline nodes: 5289, overall activation: 1873.129, activation diff: 1563.188, ratio: 0.835
#   pulse 5: activated nodes: 10989, borderline nodes: 3255, overall activation: 2763.665, activation diff: 896.293, ratio: 0.324
#   pulse 6: activated nodes: 11116, borderline nodes: 1703, overall activation: 3423.922, activation diff: 660.256, ratio: 0.193
#   pulse 7: activated nodes: 11212, borderline nodes: 1074, overall activation: 3751.197, activation diff: 327.276, ratio: 0.087
#   pulse 8: activated nodes: 11230, borderline nodes: 805, overall activation: 3893.462, activation diff: 142.265, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11230
#   final overall activation: 3893.5
#   number of spread. activ. pulses: 8
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9989819   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98637474   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9842047   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9660344   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9091409   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8791783   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7493756   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.7484775   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74845797   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7482867   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.74821734   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.7481917   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.7480155   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   14   0.7480085   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   15   0.747954   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   16   0.74791145   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.74789625   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.74786705   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.7478661   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.747677   REFERENCES
