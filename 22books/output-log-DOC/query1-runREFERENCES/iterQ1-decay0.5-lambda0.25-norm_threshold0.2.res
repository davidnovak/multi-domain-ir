###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 367.255, activation diff: 370.575, ratio: 1.009
#   pulse 3: activated nodes: 8274, borderline nodes: 3743, overall activation: 273.221, activation diff: 169.796, ratio: 0.621
#   pulse 4: activated nodes: 8274, borderline nodes: 3743, overall activation: 838.426, activation diff: 566.490, ratio: 0.676
#   pulse 5: activated nodes: 8654, borderline nodes: 3404, overall activation: 1101.989, activation diff: 263.612, ratio: 0.239
#   pulse 6: activated nodes: 8654, borderline nodes: 3403, overall activation: 1205.767, activation diff: 103.778, ratio: 0.086
#   pulse 7: activated nodes: 8660, borderline nodes: 3406, overall activation: 1241.217, activation diff: 35.450, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 1241.2
#   number of spread. activ. pulses: 7
#   running time: 419

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99785066   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98506147   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9813585   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96359074   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91331553   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8831551   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.49064693   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49044204   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49024087   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.4900006   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.4855904   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.4817875   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.47704136   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.47668248   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4766617   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.47474068   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.47273678   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.47169274   REFERENCES
1   Q1   essentialsinmod01howegoog_446   19   0.4710335   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.47068247   REFERENCES
