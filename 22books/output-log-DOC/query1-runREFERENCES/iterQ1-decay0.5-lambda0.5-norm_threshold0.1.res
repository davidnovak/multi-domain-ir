###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 211.424, activation diff: 193.447, ratio: 0.915
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 357.149, activation diff: 145.737, ratio: 0.408
#   pulse 4: activated nodes: 8452, borderline nodes: 3468, overall activation: 662.171, activation diff: 305.023, ratio: 0.461
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 915.913, activation diff: 253.742, ratio: 0.277
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1089.523, activation diff: 173.610, ratio: 0.159
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1198.672, activation diff: 109.149, ratio: 0.091
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1264.190, activation diff: 65.518, ratio: 0.052
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1302.380, activation diff: 38.191, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1302.4
#   number of spread. activ. pulses: 9
#   running time: 402

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9952551   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9802611   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.97785056   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9593834   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91265583   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8827326   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4872781   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.48712927   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.48660883   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.486503   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48244137   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47881407   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.47415274   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4735908   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.47358185   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.47159785   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.46900824   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.46826833   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.46790388   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.46761727   REFERENCES
