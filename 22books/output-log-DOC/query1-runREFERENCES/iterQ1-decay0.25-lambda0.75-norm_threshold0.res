###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 118.605, activation diff: 106.797, ratio: 0.900
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 295.601, activation diff: 177.342, ratio: 0.600
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 635.914, activation diff: 340.317, ratio: 0.535
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1074.951, activation diff: 439.038, ratio: 0.408
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 1545.951, activation diff: 471.000, ratio: 0.305
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2001.444, activation diff: 455.492, ratio: 0.228
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 2416.128, activation diff: 414.685, ratio: 0.172
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 2779.828, activation diff: 363.700, ratio: 0.131
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3091.121, activation diff: 311.292, ratio: 0.101
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 3353.130, activation diff: 262.010, ratio: 0.078
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 3571.035, activation diff: 217.905, ratio: 0.061
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 3750.664, activation diff: 179.629, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 3750.7
#   number of spread. activ. pulses: 13
#   running time: 524

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98116726   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.95592743   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9552514   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9324746   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.88419914   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8491839   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.71379375   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.71004725   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7074814   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.70729244   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.706347   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.7046133   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.70451444   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.7040043   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   15   0.7036793   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.7035772   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7033707   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   18   0.7029435   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   19   0.7026675   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.7022716   REFERENCES
