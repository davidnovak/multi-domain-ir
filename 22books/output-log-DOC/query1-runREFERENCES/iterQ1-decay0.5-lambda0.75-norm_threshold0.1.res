###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 51.346, activation diff: 40.273, ratio: 0.784
#   pulse 3: activated nodes: 8020, borderline nodes: 4132, overall activation: 120.919, activation diff: 70.032, ratio: 0.579
#   pulse 4: activated nodes: 8303, borderline nodes: 3739, overall activation: 240.421, activation diff: 119.534, ratio: 0.497
#   pulse 5: activated nodes: 8625, borderline nodes: 3414, overall activation: 382.414, activation diff: 141.992, ratio: 0.371
#   pulse 6: activated nodes: 8659, borderline nodes: 3407, overall activation: 526.689, activation diff: 144.276, ratio: 0.274
#   pulse 7: activated nodes: 8660, borderline nodes: 3406, overall activation: 661.674, activation diff: 134.985, ratio: 0.204
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 781.994, activation diff: 120.320, ratio: 0.154
#   pulse 9: activated nodes: 8660, borderline nodes: 3406, overall activation: 885.970, activation diff: 103.976, ratio: 0.117
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 973.933, activation diff: 87.963, ratio: 0.090
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1047.216, activation diff: 73.282, ratio: 0.070
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1107.563, activation diff: 60.347, ratio: 0.054
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 1156.809, activation diff: 49.247, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1156.8
#   number of spread. activ. pulses: 13
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97826624   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9448048   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9443393   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.91825014   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.86884713   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.82321954   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.46073624   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.46000212   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.4599652   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.45930806   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.45573136   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.45087975   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.44530618   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.44442427   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4441409   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.44080245   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.43806812   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.43737936   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.43733752   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.43657154   REFERENCES
