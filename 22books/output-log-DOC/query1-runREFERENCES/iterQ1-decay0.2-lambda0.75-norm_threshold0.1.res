###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 72.611, activation diff: 61.538, ratio: 0.847
#   pulse 3: activated nodes: 8020, borderline nodes: 4132, overall activation: 194.652, activation diff: 122.499, ratio: 0.629
#   pulse 4: activated nodes: 10205, borderline nodes: 5039, overall activation: 481.284, activation diff: 286.658, ratio: 0.596
#   pulse 5: activated nodes: 10876, borderline nodes: 3620, overall activation: 908.286, activation diff: 427.002, ratio: 0.470
#   pulse 6: activated nodes: 11136, borderline nodes: 2320, overall activation: 1416.019, activation diff: 507.733, ratio: 0.359
#   pulse 7: activated nodes: 11220, borderline nodes: 1332, overall activation: 1940.212, activation diff: 524.193, ratio: 0.270
#   pulse 8: activated nodes: 11265, borderline nodes: 796, overall activation: 2438.022, activation diff: 497.810, ratio: 0.204
#   pulse 9: activated nodes: 11282, borderline nodes: 589, overall activation: 2887.152, activation diff: 449.130, ratio: 0.156
#   pulse 10: activated nodes: 11302, borderline nodes: 488, overall activation: 3279.364, activation diff: 392.212, ratio: 0.120
#   pulse 11: activated nodes: 11314, borderline nodes: 384, overall activation: 3614.524, activation diff: 335.161, ratio: 0.093
#   pulse 12: activated nodes: 11314, borderline nodes: 295, overall activation: 3896.681, activation diff: 282.157, ratio: 0.072
#   pulse 13: activated nodes: 11314, borderline nodes: 246, overall activation: 4131.639, activation diff: 234.958, ratio: 0.057
#   pulse 14: activated nodes: 11318, borderline nodes: 204, overall activation: 4325.711, activation diff: 194.072, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 4325.7
#   number of spread. activ. pulses: 14
#   running time: 526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.983993   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9579817   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.95788467   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93512714   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.88378584   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84607565   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7693774   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.76697415   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.76392114   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.76389456   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.76271003   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.76154447   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7613917   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.7612264   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   15   0.7610551   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   16   0.76054955   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.7601894   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.76017463   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   19   0.760026   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.75986713   REFERENCES
