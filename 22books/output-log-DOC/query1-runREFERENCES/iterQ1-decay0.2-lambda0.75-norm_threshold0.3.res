###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 22.152, activation diff: 12.655, ratio: 0.571
#   pulse 3: activated nodes: 6260, borderline nodes: 5234, overall activation: 62.719, activation diff: 41.336, ratio: 0.659
#   pulse 4: activated nodes: 7653, borderline nodes: 5538, overall activation: 169.326, activation diff: 106.800, ratio: 0.631
#   pulse 5: activated nodes: 9169, borderline nodes: 5621, overall activation: 372.800, activation diff: 203.485, ratio: 0.546
#   pulse 6: activated nodes: 10057, borderline nodes: 5039, overall activation: 686.105, activation diff: 313.305, ratio: 0.457
#   pulse 7: activated nodes: 10637, borderline nodes: 4224, overall activation: 1087.454, activation diff: 401.349, ratio: 0.369
#   pulse 8: activated nodes: 10908, borderline nodes: 3263, overall activation: 1531.418, activation diff: 443.964, ratio: 0.290
#   pulse 9: activated nodes: 11061, borderline nodes: 2579, overall activation: 1976.482, activation diff: 445.064, ratio: 0.225
#   pulse 10: activated nodes: 11150, borderline nodes: 1946, overall activation: 2396.129, activation diff: 419.647, ratio: 0.175
#   pulse 11: activated nodes: 11200, borderline nodes: 1497, overall activation: 2775.919, activation diff: 379.791, ratio: 0.137
#   pulse 12: activated nodes: 11220, borderline nodes: 1156, overall activation: 3110.218, activation diff: 334.299, ratio: 0.107
#   pulse 13: activated nodes: 11250, borderline nodes: 951, overall activation: 3398.587, activation diff: 288.369, ratio: 0.085
#   pulse 14: activated nodes: 11254, borderline nodes: 829, overall activation: 3643.741, activation diff: 245.154, ratio: 0.067
#   pulse 15: activated nodes: 11277, borderline nodes: 742, overall activation: 3849.915, activation diff: 206.174, ratio: 0.054

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11277
#   final overall activation: 3849.9
#   number of spread. activ. pulses: 15
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9827055   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.94835645   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.94297206   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.92348623   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8665943   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.811546   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7702794   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.76873755   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.76516426   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.7640558   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.76235044   REFERENCES
1   Q1   essentialsinmod01howegoog_474   12   0.76115775   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7608133   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.76064956   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.76031864   REFERENCES
1   Q1   europesincenapol00leveuoft_353   16   0.7597399   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.759581   REFERENCES
1   Q1   essentialsinmod01howegoog_471   18   0.7593453   REFERENCES
1   Q1   essentialsinmod01howegoog_14   19   0.75907713   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7581947   REFERENCES
