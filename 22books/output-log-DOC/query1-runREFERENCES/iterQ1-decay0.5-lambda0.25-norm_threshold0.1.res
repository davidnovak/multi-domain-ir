###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 477.338, activation diff: 476.890, ratio: 0.999
#   pulse 3: activated nodes: 8645, borderline nodes: 3407, overall activation: 404.906, activation diff: 148.352, ratio: 0.366
#   pulse 4: activated nodes: 8645, borderline nodes: 3407, overall activation: 962.690, activation diff: 557.866, ratio: 0.579
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1216.017, activation diff: 253.329, ratio: 0.208
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1307.945, activation diff: 91.928, ratio: 0.070
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1338.090, activation diff: 30.146, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1338.1
#   number of spread. activ. pulses: 7
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9983895   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.987489   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98471457   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96849585   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9222185   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89730155   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4917649   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.4915254   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49136505   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49114496   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48705366   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.4836126   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.47930452   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.47896892   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.47896817   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.47723424   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.4757681   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.47450143   REFERENCES
1   Q1   essentialsinmod01howegoog_446   19   0.47387165   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.47352672   REFERENCES
