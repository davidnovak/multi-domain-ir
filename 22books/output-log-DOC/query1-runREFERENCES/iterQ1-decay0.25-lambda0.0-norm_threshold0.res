###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1462.769, activation diff: 1496.499, ratio: 1.023
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 730.006, activation diff: 2192.775, ratio: 3.004
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3297.136, activation diff: 4027.141, ratio: 1.221
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1118.742, activation diff: 4415.878, ratio: 3.947
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 3367.406, activation diff: 4486.149, ratio: 1.332
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 1138.393, activation diff: 4505.799, ratio: 3.958
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3371.156, activation diff: 4509.549, ratio: 1.338
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1139.798, activation diff: 4510.955, ratio: 3.958
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3371.448, activation diff: 4511.246, ratio: 1.338
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1139.940, activation diff: 4511.387, ratio: 3.958
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 3371.481, activation diff: 4511.420, ratio: 1.338
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1139.957, activation diff: 4511.438, ratio: 3.958
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 3371.486, activation diff: 4511.443, ratio: 1.338
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1139.959, activation diff: 4511.445, ratio: 3.958

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 1140.0
#   number of spread. activ. pulses: 15
#   running time: 550

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
