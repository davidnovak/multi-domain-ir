###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1019.951, activation diff: 1049.116, ratio: 1.029
#   pulse 3: activated nodes: 8396, borderline nodes: 3551, overall activation: 491.092, activation diff: 1511.043, ratio: 3.077
#   pulse 4: activated nodes: 10500, borderline nodes: 3599, overall activation: 3023.807, activation diff: 3514.899, ratio: 1.162
#   pulse 5: activated nodes: 11201, borderline nodes: 1886, overall activation: 976.102, activation diff: 3999.909, ratio: 4.098
#   pulse 6: activated nodes: 11242, borderline nodes: 660, overall activation: 3160.876, activation diff: 4136.977, ratio: 1.309
#   pulse 7: activated nodes: 11272, borderline nodes: 582, overall activation: 1013.896, activation diff: 4174.772, ratio: 4.118
#   pulse 8: activated nodes: 11273, borderline nodes: 557, overall activation: 3168.106, activation diff: 4182.002, ratio: 1.320
#   pulse 9: activated nodes: 11273, borderline nodes: 551, overall activation: 1017.455, activation diff: 4185.562, ratio: 4.114
#   pulse 10: activated nodes: 11273, borderline nodes: 551, overall activation: 3168.939, activation diff: 4186.394, ratio: 1.321
#   pulse 11: activated nodes: 11273, borderline nodes: 551, overall activation: 1018.000, activation diff: 4186.938, ratio: 4.113
#   pulse 12: activated nodes: 11273, borderline nodes: 551, overall activation: 3169.076, activation diff: 4187.076, ratio: 1.321
#   pulse 13: activated nodes: 11273, borderline nodes: 551, overall activation: 1018.100, activation diff: 4187.176, ratio: 4.113
#   pulse 14: activated nodes: 11273, borderline nodes: 551, overall activation: 3169.102, activation diff: 4187.202, ratio: 1.321
#   pulse 15: activated nodes: 11273, borderline nodes: 551, overall activation: 1018.120, activation diff: 4187.221, ratio: 4.113

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11273
#   final overall activation: 1018.1
#   number of spread. activ. pulses: 15
#   running time: 650

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
