###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 813.679, activation diff: 840.411, ratio: 1.033
#   pulse 3: activated nodes: 8128, borderline nodes: 3940, overall activation: 394.338, activation diff: 1208.017, ratio: 3.063
#   pulse 4: activated nodes: 10191, borderline nodes: 4514, overall activation: 2852.540, activation diff: 3246.878, ratio: 1.138
#   pulse 5: activated nodes: 11144, borderline nodes: 2481, overall activation: 894.356, activation diff: 3746.897, ratio: 4.189
#   pulse 6: activated nodes: 11219, borderline nodes: 895, overall activation: 3046.626, activation diff: 3940.983, ratio: 1.294
#   pulse 7: activated nodes: 11251, borderline nodes: 756, overall activation: 943.231, activation diff: 3989.857, ratio: 4.230
#   pulse 8: activated nodes: 11251, borderline nodes: 685, overall activation: 3056.671, activation diff: 3999.902, ratio: 1.309
#   pulse 9: activated nodes: 11251, borderline nodes: 674, overall activation: 947.933, activation diff: 4004.604, ratio: 4.225
#   pulse 10: activated nodes: 11251, borderline nodes: 669, overall activation: 3057.944, activation diff: 4005.877, ratio: 1.310
#   pulse 11: activated nodes: 11257, borderline nodes: 672, overall activation: 948.859, activation diff: 4006.803, ratio: 4.223
#   pulse 12: activated nodes: 11257, borderline nodes: 672, overall activation: 3058.237, activation diff: 4007.096, ratio: 1.310
#   pulse 13: activated nodes: 11257, borderline nodes: 672, overall activation: 949.106, activation diff: 4007.342, ratio: 4.222
#   pulse 14: activated nodes: 11257, borderline nodes: 672, overall activation: 3058.317, activation diff: 4007.422, ratio: 1.310
#   pulse 15: activated nodes: 11257, borderline nodes: 672, overall activation: 949.175, activation diff: 4007.492, ratio: 4.222

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11257
#   final overall activation: 949.2
#   number of spread. activ. pulses: 15
#   running time: 683

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
