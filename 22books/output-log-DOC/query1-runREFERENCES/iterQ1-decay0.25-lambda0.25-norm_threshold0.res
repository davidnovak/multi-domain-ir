###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 876.587, activation diff: 872.381, ratio: 0.995
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1213.196, activation diff: 514.776, ratio: 0.424
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2857.547, activation diff: 1644.350, ratio: 0.575
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3809.661, activation diff: 952.114, ratio: 0.250
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4230.959, activation diff: 421.298, ratio: 0.100
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 4401.127, activation diff: 170.168, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4401.1
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99879926   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9890361   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98776436   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9734907   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9303092   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9083048   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7487478   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.7477185   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7475847   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7475361   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.7474131   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.747348   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.7473428   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   14   0.7472706   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   15   0.7472032   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.74710023   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   17   0.74705374   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.74696887   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.74689686   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   20   0.7468951   REFERENCES
