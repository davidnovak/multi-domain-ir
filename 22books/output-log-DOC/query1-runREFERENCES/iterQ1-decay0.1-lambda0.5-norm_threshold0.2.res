###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 250.910, activation diff: 235.538, ratio: 0.939
#   pulse 3: activated nodes: 7946, borderline nodes: 4186, overall activation: 549.738, activation diff: 299.411, ratio: 0.545
#   pulse 4: activated nodes: 9978, borderline nodes: 5270, overall activation: 1797.262, activation diff: 1247.524, ratio: 0.694
#   pulse 5: activated nodes: 11070, borderline nodes: 2986, overall activation: 3188.135, activation diff: 1390.872, ratio: 0.436
#   pulse 6: activated nodes: 11220, borderline nodes: 1251, overall activation: 4369.662, activation diff: 1181.528, ratio: 0.270
#   pulse 7: activated nodes: 11293, borderline nodes: 589, overall activation: 5216.446, activation diff: 846.784, ratio: 0.162
#   pulse 8: activated nodes: 11312, borderline nodes: 354, overall activation: 5777.719, activation diff: 561.272, ratio: 0.097
#   pulse 9: activated nodes: 11316, borderline nodes: 211, overall activation: 6135.417, activation diff: 357.698, ratio: 0.058
#   pulse 10: activated nodes: 11323, borderline nodes: 150, overall activation: 6358.018, activation diff: 222.601, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 6358.0
#   number of spread. activ. pulses: 10
#   running time: 469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970327   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9832227   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9816096   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9636115   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9112669   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8958045   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8954023   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.8945457   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   9   0.8944843   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.89426726   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.89423287   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.8942285   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.8940565   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   14   0.8940377   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   15   0.89401996   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.89401734   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   17   0.89397275   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.89386135   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   19   0.89385223   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.8938283   REFERENCES
