###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1087.610, activation diff: 1116.775, ratio: 1.027
#   pulse 3: activated nodes: 8396, borderline nodes: 3551, overall activation: 596.960, activation diff: 1684.570, ratio: 2.822
#   pulse 4: activated nodes: 10503, borderline nodes: 3533, overall activation: 3503.756, activation diff: 4100.716, ratio: 1.170
#   pulse 5: activated nodes: 11210, borderline nodes: 1798, overall activation: 1231.580, activation diff: 4735.336, ratio: 3.845
#   pulse 6: activated nodes: 11289, borderline nodes: 443, overall activation: 3678.347, activation diff: 4909.927, ratio: 1.335
#   pulse 7: activated nodes: 11313, borderline nodes: 377, overall activation: 1280.892, activation diff: 4959.239, ratio: 3.872
#   pulse 8: activated nodes: 11315, borderline nodes: 314, overall activation: 3688.263, activation diff: 4969.156, ratio: 1.347
#   pulse 9: activated nodes: 11319, borderline nodes: 306, overall activation: 1285.387, activation diff: 4973.650, ratio: 3.869
#   pulse 10: activated nodes: 11319, borderline nodes: 297, overall activation: 3689.292, activation diff: 4974.679, ratio: 1.348
#   pulse 11: activated nodes: 11319, borderline nodes: 297, overall activation: 1285.931, activation diff: 4975.223, ratio: 3.869
#   pulse 12: activated nodes: 11319, borderline nodes: 294, overall activation: 3689.451, activation diff: 4975.382, ratio: 1.349
#   pulse 13: activated nodes: 11319, borderline nodes: 292, overall activation: 1286.017, activation diff: 4975.468, ratio: 3.869
#   pulse 14: activated nodes: 11319, borderline nodes: 292, overall activation: 3689.492, activation diff: 4975.508, ratio: 1.349
#   pulse 15: activated nodes: 11319, borderline nodes: 292, overall activation: 1286.035, activation diff: 4975.527, ratio: 3.869

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 1286.0
#   number of spread. activ. pulses: 15
#   running time: 546

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
