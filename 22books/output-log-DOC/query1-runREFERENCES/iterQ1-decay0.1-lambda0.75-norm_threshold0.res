###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 138.917, activation diff: 127.109, ratio: 0.915
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 370.496, activation diff: 231.925, ratio: 0.626
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 893.043, activation diff: 522.549, ratio: 0.585
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1604.001, activation diff: 710.958, ratio: 0.443
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2378.345, activation diff: 774.344, ratio: 0.326
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 3127.099, activation diff: 748.754, ratio: 0.239
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 3805.541, activation diff: 678.443, ratio: 0.178
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 4397.161, activation diff: 591.620, ratio: 0.135
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 4900.568, activation diff: 503.406, ratio: 0.103
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 5321.808, activation diff: 421.240, ratio: 0.079
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 5670.144, activation diff: 348.336, ratio: 0.061
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 5955.722, activation diff: 285.577, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5955.7
#   number of spread. activ. pulses: 13
#   running time: 517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812676   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9564452   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.95588756   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93349034   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8847727   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8582547   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8561561   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.8532266   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.8527192   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.8515575   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   11   0.85137886   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.8513695   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   13   0.85129064   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.85101074   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   15   0.8509489   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   16   0.85048544   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   17   0.85031676   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.85026497   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.85011274   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.8499403   REFERENCES
