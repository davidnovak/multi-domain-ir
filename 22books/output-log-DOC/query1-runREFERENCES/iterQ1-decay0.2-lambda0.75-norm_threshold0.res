###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 125.375, activation diff: 113.568, ratio: 0.906
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 319.969, activation diff: 194.938, ratio: 0.609
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 715.665, activation diff: 395.700, ratio: 0.553
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1237.181, activation diff: 521.516, ratio: 0.422
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 1800.992, activation diff: 563.811, ratio: 0.313
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 2347.103, activation diff: 546.111, ratio: 0.233
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 2843.953, activation diff: 496.849, ratio: 0.175
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 3279.122, activation diff: 435.170, ratio: 0.133
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 3651.040, activation diff: 371.918, ratio: 0.102
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 3963.639, activation diff: 312.598, ratio: 0.079
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 4223.266, activation diff: 259.627, ratio: 0.061
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 4436.996, activation diff: 213.730, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 4437.0
#   number of spread. activ. pulses: 13
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9812046   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9561233   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.95549035   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9328681   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8844207   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8496765   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.76205397   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.75907284   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7563822   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7558665   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.7547909   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.75401527   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.75399566   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.7535062   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   15   0.7534247   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.7530465   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.7526407   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   18   0.7524644   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   19   0.7523291   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.7523036   REFERENCES
