###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1327.295, activation diff: 1358.794, ratio: 1.024
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 736.065, activation diff: 2063.360, ratio: 2.803
#   pulse 4: activated nodes: 10803, borderline nodes: 2618, overall activation: 3667.522, activation diff: 4403.587, ratio: 1.201
#   pulse 5: activated nodes: 11263, borderline nodes: 977, overall activation: 1323.613, activation diff: 4991.135, ratio: 3.771
#   pulse 6: activated nodes: 11316, borderline nodes: 204, overall activation: 3797.060, activation diff: 5120.673, ratio: 1.349
#   pulse 7: activated nodes: 11325, borderline nodes: 144, overall activation: 1360.948, activation diff: 5158.007, ratio: 3.790
#   pulse 8: activated nodes: 11326, borderline nodes: 111, overall activation: 3804.555, activation diff: 5165.503, ratio: 1.358
#   pulse 9: activated nodes: 11326, borderline nodes: 110, overall activation: 1363.975, activation diff: 5168.530, ratio: 3.789
#   pulse 10: activated nodes: 11326, borderline nodes: 105, overall activation: 3805.279, activation diff: 5169.254, ratio: 1.358
#   pulse 11: activated nodes: 11326, borderline nodes: 104, overall activation: 1364.323, activation diff: 5169.602, ratio: 3.789
#   pulse 12: activated nodes: 11326, borderline nodes: 103, overall activation: 3805.407, activation diff: 5169.730, ratio: 1.359
#   pulse 13: activated nodes: 11326, borderline nodes: 103, overall activation: 1364.378, activation diff: 5169.785, ratio: 3.789
#   pulse 14: activated nodes: 11326, borderline nodes: 103, overall activation: 3805.440, activation diff: 5169.819, ratio: 1.359
#   pulse 15: activated nodes: 11326, borderline nodes: 103, overall activation: 1364.390, activation diff: 5169.831, ratio: 3.789

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 1364.4
#   number of spread. activ. pulses: 15
#   running time: 584

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
