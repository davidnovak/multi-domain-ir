###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 544.789, activation diff: 548.109, ratio: 1.006
#   pulse 3: activated nodes: 8274, borderline nodes: 3743, overall activation: 597.223, activation diff: 447.211, ratio: 0.749
#   pulse 4: activated nodes: 10345, borderline nodes: 4317, overall activation: 2182.172, activation diff: 1650.211, ratio: 0.756
#   pulse 5: activated nodes: 11153, borderline nodes: 2528, overall activation: 3166.565, activation diff: 986.248, ratio: 0.311
#   pulse 6: activated nodes: 11219, borderline nodes: 950, overall activation: 3733.126, activation diff: 566.560, ratio: 0.152
#   pulse 7: activated nodes: 11251, borderline nodes: 667, overall activation: 3992.905, activation diff: 259.780, ratio: 0.065
#   pulse 8: activated nodes: 11258, borderline nodes: 598, overall activation: 4101.871, activation diff: 108.966, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11258
#   final overall activation: 4101.9
#   number of spread. activ. pulses: 8
#   running time: 481

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991596   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9882029   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98625237   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9696603   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9177608   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89170647   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7494689   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.7487421   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7486798   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7485962   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.74853826   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.74852556   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.74841374   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   14   0.7483749   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   15   0.74833864   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   16   0.7483214   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.748284   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.7481825   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   19   0.7481695   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.74806   REFERENCES
