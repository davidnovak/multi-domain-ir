###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 417.754, activation diff: 397.926, ratio: 0.953
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 859.181, activation diff: 441.427, ratio: 0.514
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1833.023, activation diff: 973.842, ratio: 0.531
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2727.700, activation diff: 894.677, ratio: 0.328
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 3382.034, activation diff: 654.334, ratio: 0.193
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 3815.781, activation diff: 433.747, ratio: 0.114
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4089.706, activation diff: 273.925, ratio: 0.067
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 4258.160, activation diff: 168.453, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4258.2
#   number of spread. activ. pulses: 9
#   running time: 446

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99595845   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98296326   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98199844   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96591544   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92207396   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89656544   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7440839   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74237037   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7416895   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7411454   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.7409231   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.7407405   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.7405609   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.74037457   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.7403462   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7401433   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   17   0.7400752   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   18   0.7398484   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   19   0.7396468   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.7396381   REFERENCES
