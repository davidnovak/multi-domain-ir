###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 708.440, activation diff: 707.992, ratio: 0.999
#   pulse 3: activated nodes: 8645, borderline nodes: 3407, overall activation: 869.977, activation diff: 492.135, ratio: 0.566
#   pulse 4: activated nodes: 10768, borderline nodes: 3114, overall activation: 2508.796, activation diff: 1663.697, ratio: 0.663
#   pulse 5: activated nodes: 11226, borderline nodes: 1523, overall activation: 3512.951, activation diff: 1004.263, ratio: 0.286
#   pulse 6: activated nodes: 11275, borderline nodes: 587, overall activation: 4003.375, activation diff: 490.424, ratio: 0.123
#   pulse 7: activated nodes: 11299, borderline nodes: 494, overall activation: 4213.756, activation diff: 210.380, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11299
#   final overall activation: 4213.8
#   number of spread. activ. pulses: 7
#   running time: 413

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9984314   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9876009   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9857992   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9701441   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92290795   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8985288   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7484764   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.7473395   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.74721515   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7471477   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.74703866   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.74693245   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.7469044   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   14   0.7468604   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.7468377   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.746661   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.7465774   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   18   0.74656975   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.7464628   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   20   0.74645466   REFERENCES
