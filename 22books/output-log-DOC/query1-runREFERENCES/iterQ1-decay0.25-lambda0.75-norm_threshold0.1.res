###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 69.067, activation diff: 57.994, ratio: 0.840
#   pulse 3: activated nodes: 8020, borderline nodes: 4132, overall activation: 181.579, activation diff: 112.970, ratio: 0.622
#   pulse 4: activated nodes: 10194, borderline nodes: 5051, overall activation: 432.775, activation diff: 251.222, ratio: 0.580
#   pulse 5: activated nodes: 10814, borderline nodes: 3802, overall activation: 795.447, activation diff: 362.672, ratio: 0.456
#   pulse 6: activated nodes: 11112, borderline nodes: 2493, overall activation: 1220.293, activation diff: 424.846, ratio: 0.348
#   pulse 7: activated nodes: 11206, borderline nodes: 1544, overall activation: 1656.674, activation diff: 436.381, ratio: 0.263
#   pulse 8: activated nodes: 11229, borderline nodes: 941, overall activation: 2070.918, activation diff: 414.245, ratio: 0.200
#   pulse 9: activated nodes: 11269, borderline nodes: 682, overall activation: 2445.230, activation diff: 374.311, ratio: 0.153
#   pulse 10: activated nodes: 11286, borderline nodes: 614, overall activation: 2772.733, activation diff: 327.504, ratio: 0.118
#   pulse 11: activated nodes: 11289, borderline nodes: 575, overall activation: 3053.101, activation diff: 280.368, ratio: 0.092
#   pulse 12: activated nodes: 11293, borderline nodes: 547, overall activation: 3289.457, activation diff: 236.356, ratio: 0.072
#   pulse 13: activated nodes: 11299, borderline nodes: 546, overall activation: 3486.504, activation diff: 197.047, ratio: 0.057
#   pulse 14: activated nodes: 11299, borderline nodes: 516, overall activation: 3649.423, activation diff: 162.920, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11299
#   final overall activation: 3649.4
#   number of spread. activ. pulses: 14
#   running time: 537

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9839423   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.95766914   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.95755494   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9346444   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.88351566   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.84545666   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.72064626   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7174578   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7147998   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   10   0.71454346   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.7137288   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.71157295   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.71132815   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   14   0.7111727   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.71100926   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.7109835   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   17   0.7103196   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.70967567   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   19   0.7094904   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   20   0.7094444   REFERENCES
