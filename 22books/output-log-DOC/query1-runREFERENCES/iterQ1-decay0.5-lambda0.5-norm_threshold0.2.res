###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 147.045, activation diff: 131.673, ratio: 0.895
#   pulse 3: activated nodes: 7946, borderline nodes: 4186, overall activation: 253.450, activation diff: 106.748, ratio: 0.421
#   pulse 4: activated nodes: 7983, borderline nodes: 4181, overall activation: 539.018, activation diff: 285.568, ratio: 0.530
#   pulse 5: activated nodes: 8611, borderline nodes: 3439, overall activation: 793.318, activation diff: 254.300, ratio: 0.321
#   pulse 6: activated nodes: 8653, borderline nodes: 3409, overall activation: 974.293, activation diff: 180.975, ratio: 0.186
#   pulse 7: activated nodes: 8654, borderline nodes: 3403, overall activation: 1090.601, activation diff: 116.308, ratio: 0.107
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 1161.419, activation diff: 70.818, ratio: 0.061
#   pulse 9: activated nodes: 8660, borderline nodes: 3406, overall activation: 1203.127, activation diff: 41.708, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 1203.1
#   number of spread. activ. pulses: 9
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99434197   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.97614723   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9733489   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95309967   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.90264916   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.86652714   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4855948   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.4855724   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.48491365   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.48485258   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48054755   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47651768   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.47138166   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.47077978   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4707251   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.46848685   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.4650694   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.46473476   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.46448275   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.46423203   REFERENCES
