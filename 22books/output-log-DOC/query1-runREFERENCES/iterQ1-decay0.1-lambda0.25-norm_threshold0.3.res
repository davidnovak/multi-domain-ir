###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 485.289, activation diff: 490.494, ratio: 1.011
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 562.411, activation diff: 546.918, ratio: 0.972
#   pulse 4: activated nodes: 9886, borderline nodes: 5183, overall activation: 2883.811, activation diff: 2472.653, ratio: 0.857
#   pulse 5: activated nodes: 11081, borderline nodes: 2960, overall activation: 4407.454, activation diff: 1542.230, ratio: 0.350
#   pulse 6: activated nodes: 11242, borderline nodes: 1021, overall activation: 5521.217, activation diff: 1113.785, ratio: 0.202
#   pulse 7: activated nodes: 11298, borderline nodes: 608, overall activation: 6069.256, activation diff: 548.039, ratio: 0.090
#   pulse 8: activated nodes: 11312, borderline nodes: 380, overall activation: 6309.134, activation diff: 239.878, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11312
#   final overall activation: 6309.1
#   number of spread. activ. pulses: 8
#   running time: 632

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9990006   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9865402   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98462653   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96629804   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.90928876   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8993863   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.89917636   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.89914715   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8991388   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.89912635   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.899117   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   12   0.899105   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.89910436   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   14   0.8990998   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.89908713   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.89908504   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.89908165   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   18   0.8990801   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   19   0.8990755   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   20   0.89905703   REFERENCES
