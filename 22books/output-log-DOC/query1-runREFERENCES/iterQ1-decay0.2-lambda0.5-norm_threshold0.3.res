###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 147.262, activation diff: 134.778, ratio: 0.915
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 302.698, activation diff: 156.272, ratio: 0.516
#   pulse 4: activated nodes: 9211, borderline nodes: 5918, overall activation: 1032.828, activation diff: 730.130, ratio: 0.707
#   pulse 5: activated nodes: 10643, borderline nodes: 4061, overall activation: 1926.310, activation diff: 893.482, ratio: 0.464
#   pulse 6: activated nodes: 11032, borderline nodes: 2720, overall activation: 2792.249, activation diff: 865.939, ratio: 0.310
#   pulse 7: activated nodes: 11195, borderline nodes: 1618, overall activation: 3465.339, activation diff: 673.090, ratio: 0.194
#   pulse 8: activated nodes: 11223, borderline nodes: 964, overall activation: 3936.136, activation diff: 470.797, ratio: 0.120
#   pulse 9: activated nodes: 11272, borderline nodes: 742, overall activation: 4247.309, activation diff: 311.173, ratio: 0.073
#   pulse 10: activated nodes: 11280, borderline nodes: 632, overall activation: 4446.645, activation diff: 199.336, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11280
#   final overall activation: 4446.6
#   number of spread. activ. pulses: 10
#   running time: 488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99639034   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9793997   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.97803843   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95845616   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9015368   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8662658   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7956505   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79457235   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7937126   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.7930494   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.7929863   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.79293966   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.7929288   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.7926519   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.79255545   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   16   0.7922812   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   17   0.7922758   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.79227364   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   19   0.792198   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   20   0.79213333   REFERENCES
