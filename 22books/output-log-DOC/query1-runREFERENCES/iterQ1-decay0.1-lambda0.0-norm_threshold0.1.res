###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1492.552, activation diff: 1524.051, ratio: 1.021
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1018.247, activation diff: 2510.798, ratio: 2.466
#   pulse 4: activated nodes: 10803, borderline nodes: 2584, overall activation: 4746.877, activation diff: 5765.123, ratio: 1.215
#   pulse 5: activated nodes: 11265, borderline nodes: 931, overall activation: 1929.393, activation diff: 6676.269, ratio: 3.460
#   pulse 6: activated nodes: 11326, borderline nodes: 107, overall activation: 4943.414, activation diff: 6872.807, ratio: 1.390
#   pulse 7: activated nodes: 11334, borderline nodes: 42, overall activation: 1986.199, activation diff: 6929.613, ratio: 3.489
#   pulse 8: activated nodes: 11335, borderline nodes: 18, overall activation: 4955.872, activation diff: 6942.071, ratio: 1.401
#   pulse 9: activated nodes: 11335, borderline nodes: 13, overall activation: 1991.360, activation diff: 6947.232, ratio: 3.489
#   pulse 10: activated nodes: 11336, borderline nodes: 11, overall activation: 4957.588, activation diff: 6948.948, ratio: 1.402
#   pulse 11: activated nodes: 11336, borderline nodes: 11, overall activation: 1992.122, activation diff: 6949.710, ratio: 3.489
#   pulse 12: activated nodes: 11336, borderline nodes: 11, overall activation: 4957.891, activation diff: 6950.013, ratio: 1.402
#   pulse 13: activated nodes: 11336, borderline nodes: 11, overall activation: 1992.271, activation diff: 6950.162, ratio: 3.489
#   pulse 14: activated nodes: 11336, borderline nodes: 11, overall activation: 4957.957, activation diff: 6950.227, ratio: 1.402
#   pulse 15: activated nodes: 11340, borderline nodes: 14, overall activation: 1992.306, activation diff: 6950.262, ratio: 3.489

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 1992.3
#   number of spread. activ. pulses: 15
#   running time: 1561

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
