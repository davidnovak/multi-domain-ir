###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 21.610, activation diff: 12.113, ratio: 0.561
#   pulse 3: activated nodes: 6260, borderline nodes: 5234, overall activation: 59.683, activation diff: 38.842, ratio: 0.651
#   pulse 4: activated nodes: 7640, borderline nodes: 5527, overall activation: 157.080, activation diff: 97.595, ratio: 0.621
#   pulse 5: activated nodes: 9125, borderline nodes: 5663, overall activation: 336.625, activation diff: 179.558, ratio: 0.533
#   pulse 6: activated nodes: 10009, borderline nodes: 5124, overall activation: 603.781, activation diff: 267.156, ratio: 0.442
#   pulse 7: activated nodes: 10562, borderline nodes: 4407, overall activation: 939.237, activation diff: 335.455, ratio: 0.357
#   pulse 8: activated nodes: 10812, borderline nodes: 3576, overall activation: 1307.202, activation diff: 367.965, ratio: 0.281
#   pulse 9: activated nodes: 10992, borderline nodes: 2839, overall activation: 1675.128, activation diff: 367.927, ratio: 0.220
#   pulse 10: activated nodes: 11086, borderline nodes: 2281, overall activation: 2021.850, activation diff: 346.722, ratio: 0.171
#   pulse 11: activated nodes: 11161, borderline nodes: 1762, overall activation: 2336.044, activation diff: 314.194, ratio: 0.134
#   pulse 12: activated nodes: 11188, borderline nodes: 1445, overall activation: 2613.020, activation diff: 276.976, ratio: 0.106
#   pulse 13: activated nodes: 11212, borderline nodes: 1175, overall activation: 2852.500, activation diff: 239.480, ratio: 0.084
#   pulse 14: activated nodes: 11218, borderline nodes: 979, overall activation: 3056.526, activation diff: 204.026, ratio: 0.067
#   pulse 15: activated nodes: 11248, borderline nodes: 910, overall activation: 3228.429, activation diff: 171.903, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11248
#   final overall activation: 3228.4
#   number of spread. activ. pulses: 15
#   running time: 546

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9825693   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.94761187   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.94208646   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.92259955   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.86608756   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.81012785   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7213032   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7188245   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.7156621   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.7146857   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.71254313   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.711106   REFERENCES
1   Q1   essentialsinmod01howegoog_474   13   0.7103561   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.7098442   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   15   0.70936275   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   16   0.70898914   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.708597   REFERENCES
1   Q1   essentialsinmod01howegoog_471   18   0.7080368   REFERENCES
1   Q1   essentialsinmod01howegoog_14   19   0.707806   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7069409   REFERENCES
