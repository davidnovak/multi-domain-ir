###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 975.443, activation diff: 1002.175, ratio: 1.027
#   pulse 3: activated nodes: 8128, borderline nodes: 3940, overall activation: 658.798, activation diff: 1634.241, ratio: 2.481
#   pulse 4: activated nodes: 10203, borderline nodes: 4379, overall activation: 4327.717, activation diff: 4986.515, ratio: 1.152
#   pulse 5: activated nodes: 11169, borderline nodes: 2296, overall activation: 1685.755, activation diff: 6013.472, ratio: 3.567
#   pulse 6: activated nodes: 11279, borderline nodes: 484, overall activation: 4681.872, activation diff: 6367.628, ratio: 1.360
#   pulse 7: activated nodes: 11312, borderline nodes: 343, overall activation: 1783.980, activation diff: 6465.852, ratio: 3.624
#   pulse 8: activated nodes: 11316, borderline nodes: 216, overall activation: 4703.716, activation diff: 6487.696, ratio: 1.379
#   pulse 9: activated nodes: 11318, borderline nodes: 201, overall activation: 1793.418, activation diff: 6497.134, ratio: 3.623
#   pulse 10: activated nodes: 11319, borderline nodes: 190, overall activation: 4706.984, activation diff: 6500.402, ratio: 1.381
#   pulse 11: activated nodes: 11319, borderline nodes: 186, overall activation: 1794.886, activation diff: 6501.870, ratio: 3.622
#   pulse 12: activated nodes: 11319, borderline nodes: 185, overall activation: 4707.829, activation diff: 6502.716, ratio: 1.381
#   pulse 13: activated nodes: 11320, borderline nodes: 185, overall activation: 1795.207, activation diff: 6503.036, ratio: 3.622
#   pulse 14: activated nodes: 11320, borderline nodes: 185, overall activation: 4708.073, activation diff: 6503.280, ratio: 1.381
#   pulse 15: activated nodes: 11320, borderline nodes: 185, overall activation: 1795.293, activation diff: 6503.366, ratio: 3.622

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 1795.3
#   number of spread. activ. pulses: 15
#   running time: 626

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
