###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 42.657, activation diff: 32.354, ratio: 0.758
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 124.814, activation diff: 82.759, ratio: 0.663
#   pulse 4: activated nodes: 9155, borderline nodes: 5857, overall activation: 353.207, activation diff: 228.450, ratio: 0.647
#   pulse 5: activated nodes: 10271, borderline nodes: 4743, overall activation: 766.885, activation diff: 413.678, ratio: 0.539
#   pulse 6: activated nodes: 10858, borderline nodes: 3646, overall activation: 1347.876, activation diff: 580.991, ratio: 0.431
#   pulse 7: activated nodes: 11110, borderline nodes: 2541, overall activation: 2011.320, activation diff: 663.444, ratio: 0.330
#   pulse 8: activated nodes: 11203, borderline nodes: 1631, overall activation: 2681.765, activation diff: 670.444, ratio: 0.250
#   pulse 9: activated nodes: 11232, borderline nodes: 996, overall activation: 3311.152, activation diff: 629.388, ratio: 0.190
#   pulse 10: activated nodes: 11280, borderline nodes: 704, overall activation: 3875.565, activation diff: 564.412, ratio: 0.146
#   pulse 11: activated nodes: 11301, borderline nodes: 511, overall activation: 4366.983, activation diff: 491.418, ratio: 0.113
#   pulse 12: activated nodes: 11308, borderline nodes: 420, overall activation: 4786.559, activation diff: 419.576, ratio: 0.088
#   pulse 13: activated nodes: 11313, borderline nodes: 306, overall activation: 5139.939, activation diff: 353.380, ratio: 0.069
#   pulse 14: activated nodes: 11314, borderline nodes: 246, overall activation: 5434.464, activation diff: 294.525, ratio: 0.054
#   pulse 15: activated nodes: 11316, borderline nodes: 205, overall activation: 5677.956, activation diff: 243.493, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 5678.0
#   number of spread. activ. pulses: 15
#   running time: 550

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98602927   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9591381   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.95769894   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93631536   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8815987   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8721844   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8712885   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.86830413   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8672384   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.866974   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.8665332   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.86607397   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.8660636   REFERENCES
1   Q1   europesincenapol00leveuoft_353   14   0.8655689   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   15   0.8653782   REFERENCES
1   Q1   essentialsinmod01howegoog_471   16   0.8652276   REFERENCES
1   Q1   essentialsinmod01howegoog_14   17   0.86514616   REFERENCES
1   Q1   europesincenapol00leveuoft_17   18   0.8645751   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.864528   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.864346   REFERENCES
