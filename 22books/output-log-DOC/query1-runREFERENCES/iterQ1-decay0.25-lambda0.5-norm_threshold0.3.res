###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 138.991, activation diff: 126.507, ratio: 0.910
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 278.641, activation diff: 140.447, ratio: 0.504
#   pulse 4: activated nodes: 9207, borderline nodes: 5930, overall activation: 902.132, activation diff: 623.491, ratio: 0.691
#   pulse 5: activated nodes: 10551, borderline nodes: 4230, overall activation: 1647.825, activation diff: 745.694, ratio: 0.453
#   pulse 6: activated nodes: 10964, borderline nodes: 2979, overall activation: 2363.596, activation diff: 715.770, ratio: 0.303
#   pulse 7: activated nodes: 11141, borderline nodes: 1944, overall activation: 2919.887, activation diff: 556.292, ratio: 0.191
#   pulse 8: activated nodes: 11209, borderline nodes: 1227, overall activation: 3310.650, activation diff: 390.763, ratio: 0.118
#   pulse 9: activated nodes: 11222, borderline nodes: 908, overall activation: 3569.997, activation diff: 259.347, ratio: 0.073
#   pulse 10: activated nodes: 11258, borderline nodes: 778, overall activation: 3736.633, activation diff: 166.636, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11258
#   final overall activation: 3736.6
#   number of spread. activ. pulses: 10
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99637365   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9792017   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.977759   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95817685   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9013984   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8658118   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.74574685   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7439746   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.74330604   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7422168   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.74218214   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7420256   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.74201953   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.74186534   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.74156713   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   16   0.74147606   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.7411132   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   18   0.74109674   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.74102086   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.74093723   REFERENCES
