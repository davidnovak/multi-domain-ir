###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 365.029, activation diff: 347.053, ratio: 0.951
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 805.010, activation diff: 440.002, ratio: 0.547
#   pulse 4: activated nodes: 10547, borderline nodes: 3819, overall activation: 2245.762, activation diff: 1440.752, ratio: 0.642
#   pulse 5: activated nodes: 11203, borderline nodes: 1855, overall activation: 3717.570, activation diff: 1471.808, ratio: 0.396
#   pulse 6: activated nodes: 11287, borderline nodes: 490, overall activation: 4858.873, activation diff: 1141.303, ratio: 0.235
#   pulse 7: activated nodes: 11315, borderline nodes: 234, overall activation: 5638.492, activation diff: 779.619, ratio: 0.138
#   pulse 8: activated nodes: 11329, borderline nodes: 75, overall activation: 6140.142, activation diff: 501.650, ratio: 0.082
#   pulse 9: activated nodes: 11332, borderline nodes: 51, overall activation: 6452.463, activation diff: 312.321, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 6452.5
#   number of spread. activ. pulses: 9
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99535227   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98073435   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.97956777   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96200466   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9139749   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.89243174   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.891814   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.89042485   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8902607   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.8901061   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.8898356   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.88977236   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   13   0.88961875   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   14   0.8894964   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   15   0.88948226   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   16   0.8894253   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.88940156   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   18   0.88937247   REFERENCES
1   Q1   europesincenapol00leveuoft_353   19   0.88925445   REFERENCES
1   Q1   essentialsinmod01howegoog_471   20   0.8892181   REFERENCES
