###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1559.929, activation diff: 1593.659, ratio: 1.022
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 887.976, activation diff: 2447.905, ratio: 2.757
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3807.296, activation diff: 4695.272, ratio: 1.233
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1401.626, activation diff: 5208.922, ratio: 3.716
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3901.498, activation diff: 5303.124, ratio: 1.359
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 1427.442, activation diff: 5328.940, ratio: 3.733
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 3906.121, activation diff: 5333.563, ratio: 1.365
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 1429.238, activation diff: 5335.359, ratio: 3.733
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 3906.553, activation diff: 5335.791, ratio: 1.366
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 1429.422, activation diff: 5335.975, ratio: 3.733
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 3906.620, activation diff: 5336.042, ratio: 1.366
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 1429.447, activation diff: 5336.068, ratio: 3.733
#   pulse 14: activated nodes: 11335, borderline nodes: 30, overall activation: 3906.634, activation diff: 5336.081, ratio: 1.366
#   pulse 15: activated nodes: 11335, borderline nodes: 30, overall activation: 1429.452, activation diff: 5336.086, ratio: 3.733

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 1429.5
#   number of spread. activ. pulses: 15
#   running time: 637

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
