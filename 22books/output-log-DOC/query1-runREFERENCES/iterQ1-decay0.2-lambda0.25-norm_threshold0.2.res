###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 580.295, activation diff: 583.615, ratio: 1.006
#   pulse 3: activated nodes: 8274, borderline nodes: 3743, overall activation: 672.926, activation diff: 513.597, ratio: 0.763
#   pulse 4: activated nodes: 10349, borderline nodes: 4273, overall activation: 2540.430, activation diff: 1948.517, ratio: 0.767
#   pulse 5: activated nodes: 11165, borderline nodes: 2436, overall activation: 3733.916, activation diff: 1196.845, ratio: 0.321
#   pulse 6: activated nodes: 11260, borderline nodes: 779, overall activation: 4423.714, activation diff: 689.798, ratio: 0.156
#   pulse 7: activated nodes: 11294, borderline nodes: 534, overall activation: 4739.745, activation diff: 316.031, ratio: 0.067
#   pulse 8: activated nodes: 11314, borderline nodes: 416, overall activation: 4872.709, activation diff: 132.964, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11314
#   final overall activation: 4872.7
#   number of spread. activ. pulses: 8
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991643   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9882223   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98635817   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9697389   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.917795   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89181495   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7994984   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.79908013   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.79904866   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7990451   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.799035   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.7989874   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.7989673   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   14   0.79896516   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   15   0.7989405   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.79890966   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.7988992   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   18   0.79887974   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.7988229   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.79881203   REFERENCES
