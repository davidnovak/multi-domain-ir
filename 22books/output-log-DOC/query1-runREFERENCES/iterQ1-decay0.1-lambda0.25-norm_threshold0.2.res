###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 651.309, activation diff: 654.629, ratio: 1.005
#   pulse 3: activated nodes: 8274, borderline nodes: 3743, overall activation: 833.942, activation diff: 655.978, ratio: 0.787
#   pulse 4: activated nodes: 10356, borderline nodes: 4182, overall activation: 3339.763, activation diff: 2620.670, ratio: 0.785
#   pulse 5: activated nodes: 11176, borderline nodes: 2305, overall activation: 5006.023, activation diff: 1672.140, ratio: 0.334
#   pulse 6: activated nodes: 11278, borderline nodes: 521, overall activation: 5964.367, activation diff: 958.344, ratio: 0.161
#   pulse 7: activated nodes: 11314, borderline nodes: 273, overall activation: 6402.122, activation diff: 437.755, ratio: 0.068
#   pulse 8: activated nodes: 11320, borderline nodes: 141, overall activation: 6587.297, activation diff: 185.175, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 6587.3
#   number of spread. activ. pulses: 8
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9991714   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9882519   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98654485   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96984446   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9178467   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.89947426   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.8992697   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.89925396   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8992449   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8992416   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.8992336   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   12   0.8992144   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.89919895   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.8991922   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.899192   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   16   0.89919055   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.8991841   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   18   0.8991606   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   19   0.89915   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   20   0.8991411   REFERENCES
