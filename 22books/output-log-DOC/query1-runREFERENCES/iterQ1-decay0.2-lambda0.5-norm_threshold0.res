###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 444.168, activation diff: 424.340, ratio: 0.955
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 943.500, activation diff: 499.331, ratio: 0.529
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2106.004, activation diff: 1162.504, ratio: 0.552
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3185.962, activation diff: 1079.958, ratio: 0.339
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3976.452, activation diff: 790.489, ratio: 0.199
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 4499.956, activation diff: 523.505, ratio: 0.116
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4830.235, activation diff: 330.279, ratio: 0.068
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 5033.007, activation diff: 202.771, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5033.0
#   number of spread. activ. pulses: 9
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99596345   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98297405   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9820851   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96603894   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9221338   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89667284   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7937974   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7926177   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7916943   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7913511   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.7910379   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.79102   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7909622   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   14   0.79078794   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.7904723   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7903854   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7903836   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   18   0.79030895   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   19   0.79030585   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   20   0.7902396   REFERENCES
