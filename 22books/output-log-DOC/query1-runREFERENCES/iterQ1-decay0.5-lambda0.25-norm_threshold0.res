###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 590.393, activation diff: 586.187, ratio: 0.993
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 570.426, activation diff: 98.990, ratio: 0.174
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1088.807, activation diff: 518.381, ratio: 0.476
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1320.584, activation diff: 231.777, ratio: 0.176
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1403.014, activation diff: 82.430, ratio: 0.059
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1429.716, activation diff: 26.702, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1429.7
#   number of spread. activ. pulses: 7
#   running time: 418

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99877065   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.989015   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98711944   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9723612   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.929917   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9078619   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.49266863   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.49245307   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.4923231   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49212247   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48836654   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.48524433   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4813375   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4810338   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4810338   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.4794639   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.47824815   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.4769939   REFERENCES
1   Q1   essentialsinmod01howegoog_446   19   0.4764244   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.47608912   REFERENCES
