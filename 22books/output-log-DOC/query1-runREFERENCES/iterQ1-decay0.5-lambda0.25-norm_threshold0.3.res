###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 274.098, activation diff: 279.302, ratio: 1.019
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 180.340, activation diff: 168.398, ratio: 0.934
#   pulse 4: activated nodes: 7878, borderline nodes: 4240, overall activation: 718.329, activation diff: 544.278, ratio: 0.758
#   pulse 5: activated nodes: 8510, borderline nodes: 3435, overall activation: 972.859, activation diff: 256.948, ratio: 0.264
#   pulse 6: activated nodes: 8610, borderline nodes: 3431, overall activation: 1093.572, activation diff: 120.713, ratio: 0.110
#   pulse 7: activated nodes: 8631, borderline nodes: 3402, overall activation: 1138.132, activation diff: 44.559, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8631
#   final overall activation: 1138.1
#   number of spread. activ. pulses: 7
#   running time: 380

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972574   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9803413   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9767803   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95711327   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.902922   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8640338   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4891644   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.48913065   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.48887712   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.48872027   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.48399633   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47970653   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.47449988   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4741913   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4740368   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.47182557   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.46881   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.4683809   REFERENCES
1   Q1   essentialsinmod01howegoog_446   19   0.46783435   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.46745214   REFERENCES
