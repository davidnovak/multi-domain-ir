###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 79.700, activation diff: 68.626, ratio: 0.861
#   pulse 3: activated nodes: 8020, borderline nodes: 4132, overall activation: 221.715, activation diff: 142.474, ratio: 0.643
#   pulse 4: activated nodes: 10213, borderline nodes: 4996, overall activation: 590.903, activation diff: 369.211, ratio: 0.625
#   pulse 5: activated nodes: 10978, borderline nodes: 3342, overall activation: 1168.868, activation diff: 577.966, ratio: 0.494
#   pulse 6: activated nodes: 11176, borderline nodes: 2078, overall activation: 1868.584, activation diff: 699.715, ratio: 0.374
#   pulse 7: activated nodes: 11231, borderline nodes: 1011, overall activation: 2592.030, activation diff: 723.447, ratio: 0.279
#   pulse 8: activated nodes: 11287, borderline nodes: 568, overall activation: 3276.150, activation diff: 684.119, ratio: 0.209
#   pulse 9: activated nodes: 11304, borderline nodes: 344, overall activation: 3890.215, activation diff: 614.065, ratio: 0.158
#   pulse 10: activated nodes: 11315, borderline nodes: 215, overall activation: 4424.123, activation diff: 533.909, ratio: 0.121
#   pulse 11: activated nodes: 11320, borderline nodes: 143, overall activation: 4878.634, activation diff: 454.510, ratio: 0.093
#   pulse 12: activated nodes: 11328, borderline nodes: 94, overall activation: 5259.834, activation diff: 381.200, ratio: 0.072
#   pulse 13: activated nodes: 11331, borderline nodes: 65, overall activation: 5576.051, activation diff: 316.217, ratio: 0.057
#   pulse 14: activated nodes: 11331, borderline nodes: 53, overall activation: 5836.201, activation diff: 260.150, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5836.2
#   number of spread. activ. pulses: 14
#   running time: 499

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98407835   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.95850766   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9584346   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93589526   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8842161   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8665644   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.8650425   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.8617578   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8617445   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.86052775   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   11   0.8602122   REFERENCES
1   Q1   essentialsinmod01howegoog_474   12   0.8602098   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.8599047   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.85947335   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   15   0.85931766   REFERENCES
1   Q1   europesincenapol00leveuoft_353   16   0.85920995   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   17   0.85908866   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   18   0.8590167   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   19   0.8588951   REFERENCES
1   Q1   essentialsinmod01howegoog_471   20   0.8587502   REFERENCES
