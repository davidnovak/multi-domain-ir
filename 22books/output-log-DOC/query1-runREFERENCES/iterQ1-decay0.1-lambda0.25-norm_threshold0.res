###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1048.303, activation diff: 1044.098, ratio: 0.996
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1658.460, activation diff: 823.849, ratio: 0.497
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4327.856, activation diff: 2669.397, ratio: 0.617
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5945.421, activation diff: 1617.565, ratio: 0.272
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 6665.055, activation diff: 719.634, ratio: 0.108
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 6954.623, activation diff: 289.568, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6954.6
#   number of spread. activ. pulses: 7
#   running time: 559

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.998806   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.98903817   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9879621   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97367966   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9303608   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9083586   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.89859504   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.8983188   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.89796793   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   10   0.8979213   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.8978883   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.89782786   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   13   0.89779437   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.8977756   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   15   0.8977555   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   16   0.8976929   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   17   0.89768064   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.89767265   REFERENCES
1   Q1   essentialsinmod01howegoog_474   19   0.89762664   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   20   0.897623   REFERENCES
