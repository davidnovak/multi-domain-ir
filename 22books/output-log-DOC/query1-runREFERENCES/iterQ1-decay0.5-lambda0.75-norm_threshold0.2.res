###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 30.238, activation diff: 19.935, ratio: 0.659
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 74.680, activation diff: 45.043, ratio: 0.603
#   pulse 4: activated nodes: 7675, borderline nodes: 4565, overall activation: 163.810, activation diff: 89.217, ratio: 0.545
#   pulse 5: activated nodes: 8180, borderline nodes: 3828, overall activation: 282.726, activation diff: 118.916, ratio: 0.421
#   pulse 6: activated nodes: 8430, borderline nodes: 3549, overall activation: 413.039, activation diff: 130.313, ratio: 0.315
#   pulse 7: activated nodes: 8574, borderline nodes: 3468, overall activation: 541.248, activation diff: 128.209, ratio: 0.237
#   pulse 8: activated nodes: 8629, borderline nodes: 3417, overall activation: 659.530, activation diff: 118.281, ratio: 0.179
#   pulse 9: activated nodes: 8651, borderline nodes: 3406, overall activation: 764.243, activation diff: 104.713, ratio: 0.137
#   pulse 10: activated nodes: 8654, borderline nodes: 3404, overall activation: 854.416, activation diff: 90.173, ratio: 0.106
#   pulse 11: activated nodes: 8656, borderline nodes: 3403, overall activation: 930.570, activation diff: 76.154, ratio: 0.082
#   pulse 12: activated nodes: 8660, borderline nodes: 3406, overall activation: 993.967, activation diff: 63.396, ratio: 0.064
#   pulse 13: activated nodes: 8660, borderline nodes: 3406, overall activation: 1046.163, activation diff: 52.196, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 1046.2
#   number of spread. activ. pulses: 13
#   running time: 472

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9743429   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9325436   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9270006   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.90377164   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8527933   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.79290503   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.45655888   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.45577413   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.45496246   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.4544616   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.45169237   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.44637752   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.44017676   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4392094   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4386661   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.4342986   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.43266436   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.4318574   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.43122652   REFERENCES
1   Q1   politicalsketche00retsrich_153   20   0.4306074   REFERENCES
