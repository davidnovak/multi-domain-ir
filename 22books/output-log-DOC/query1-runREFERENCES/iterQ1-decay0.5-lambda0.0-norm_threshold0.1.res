###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 831.524, activation diff: 863.023, ratio: 1.038
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.436, activation diff: 892.960, ratio: 14.535
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.132, activation diff: 1350.568, ratio: 1.048
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.923, activation diff: 1351.055, ratio: 21.818
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.549, activation diff: 1351.471, ratio: 1.048
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.942, activation diff: 1351.491, ratio: 21.819
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.570, activation diff: 1351.512, ratio: 1.048
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.943, activation diff: 1351.513, ratio: 21.819
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.571, activation diff: 1351.514, ratio: 1.048
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.943, activation diff: 1351.514, ratio: 21.819
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.571, activation diff: 1351.515, ratio: 1.048
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.943, activation diff: 1351.515, ratio: 21.819
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.571, activation diff: 1351.515, ratio: 1.048
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.943, activation diff: 1351.515, ratio: 21.819

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 61.9
#   number of spread. activ. pulses: 15
#   running time: 504

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
