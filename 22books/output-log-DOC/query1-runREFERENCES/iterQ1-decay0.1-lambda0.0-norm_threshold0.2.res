###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1222.927, activation diff: 1252.092, ratio: 1.024
#   pulse 3: activated nodes: 8396, borderline nodes: 3551, overall activation: 825.847, activation diff: 2048.775, ratio: 2.481
#   pulse 4: activated nodes: 10509, borderline nodes: 3437, overall activation: 4555.399, activation diff: 5381.246, ratio: 1.181
#   pulse 5: activated nodes: 11222, borderline nodes: 1681, overall activation: 1810.973, activation diff: 6366.372, ratio: 3.515
#   pulse 6: activated nodes: 11300, borderline nodes: 239, overall activation: 4815.399, activation diff: 6626.372, ratio: 1.376
#   pulse 7: activated nodes: 11325, borderline nodes: 141, overall activation: 1886.293, activation diff: 6701.692, ratio: 3.553
#   pulse 8: activated nodes: 11327, borderline nodes: 95, overall activation: 4831.782, activation diff: 6718.075, ratio: 1.390
#   pulse 9: activated nodes: 11327, borderline nodes: 93, overall activation: 1893.322, activation diff: 6725.104, ratio: 3.552
#   pulse 10: activated nodes: 11327, borderline nodes: 90, overall activation: 4834.302, activation diff: 6727.623, ratio: 1.392
#   pulse 11: activated nodes: 11327, borderline nodes: 89, overall activation: 1894.384, activation diff: 6728.686, ratio: 3.552
#   pulse 12: activated nodes: 11327, borderline nodes: 88, overall activation: 4834.818, activation diff: 6729.202, ratio: 1.392
#   pulse 13: activated nodes: 11327, borderline nodes: 88, overall activation: 1894.603, activation diff: 6729.421, ratio: 3.552
#   pulse 14: activated nodes: 11327, borderline nodes: 88, overall activation: 4834.943, activation diff: 6729.546, ratio: 1.392
#   pulse 15: activated nodes: 11327, borderline nodes: 88, overall activation: 1894.657, activation diff: 6729.601, ratio: 3.552

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327
#   final overall activation: 1894.7
#   number of spread. activ. pulses: 15
#   running time: 695

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
