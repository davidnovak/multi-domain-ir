###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 163.802, activation diff: 151.318, ratio: 0.924
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 353.403, activation diff: 190.513, ratio: 0.539
#   pulse 4: activated nodes: 9237, borderline nodes: 5897, overall activation: 1336.810, activation diff: 983.407, ratio: 0.736
#   pulse 5: activated nodes: 10773, borderline nodes: 3771, overall activation: 2574.969, activation diff: 1238.159, ratio: 0.481
#   pulse 6: activated nodes: 11122, borderline nodes: 2292, overall activation: 3781.381, activation diff: 1206.412, ratio: 0.319
#   pulse 7: activated nodes: 11220, borderline nodes: 1202, overall activation: 4711.741, activation diff: 930.359, ratio: 0.197
#   pulse 8: activated nodes: 11283, borderline nodes: 689, overall activation: 5355.530, activation diff: 643.789, ratio: 0.120
#   pulse 9: activated nodes: 11302, borderline nodes: 469, overall activation: 5777.463, activation diff: 421.934, ratio: 0.073
#   pulse 10: activated nodes: 11312, borderline nodes: 362, overall activation: 6046.433, activation diff: 268.970, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11312
#   final overall activation: 6046.4
#   number of spread. activ. pulses: 10
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99641716   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9797299   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.978485   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.95884377   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.90175056   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   6   0.8952882   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   7   0.89489925   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.89377046   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   9   0.8937346   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   10   0.8933629   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.8933228   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.8932434   REFERENCES
1   Q1   essentialsinmod01howegoog_474   13   0.89322984   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.89317966   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   15   0.89314246   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   16   0.89306843   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   17   0.89300793   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   18   0.89300054   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.89289165   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.89280975   REFERENCES
