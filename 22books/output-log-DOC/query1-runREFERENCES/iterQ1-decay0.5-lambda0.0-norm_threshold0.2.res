###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 681.658, activation diff: 710.823, ratio: 1.043
#   pulse 3: activated nodes: 8396, borderline nodes: 3551, overall activation: 60.960, activation diff: 742.618, ratio: 12.182
#   pulse 4: activated nodes: 8396, borderline nodes: 3551, overall activation: 1195.022, activation diff: 1255.982, ratio: 1.051
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 61.741, activation diff: 1256.762, ratio: 20.355
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1195.808, activation diff: 1257.548, ratio: 1.052
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.789, activation diff: 1257.596, ratio: 20.353
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1195.871, activation diff: 1257.660, ratio: 1.052
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.793, activation diff: 1257.665, ratio: 20.353
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1195.878, activation diff: 1257.672, ratio: 1.052
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.794, activation diff: 1257.672, ratio: 20.353
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1195.879, activation diff: 1257.673, ratio: 1.052
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.794, activation diff: 1257.673, ratio: 20.353
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1195.879, activation diff: 1257.673, ratio: 1.052
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.794, activation diff: 1257.673, ratio: 20.353

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 61.8
#   number of spread. activ. pulses: 15
#   running time: 521

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
