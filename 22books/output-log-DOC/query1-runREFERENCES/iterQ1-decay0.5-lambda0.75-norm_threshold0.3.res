###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 18.900, activation diff: 9.403, ratio: 0.498
#   pulse 3: activated nodes: 6260, borderline nodes: 5234, overall activation: 44.684, activation diff: 26.553, ratio: 0.594
#   pulse 4: activated nodes: 6988, borderline nodes: 4894, overall activation: 102.309, activation diff: 57.855, ratio: 0.565
#   pulse 5: activated nodes: 7566, borderline nodes: 4472, overall activation: 191.622, activation diff: 89.337, ratio: 0.466
#   pulse 6: activated nodes: 7970, borderline nodes: 4025, overall activation: 300.484, activation diff: 108.862, ratio: 0.362
#   pulse 7: activated nodes: 8217, borderline nodes: 3734, overall activation: 415.653, activation diff: 115.170, ratio: 0.277
#   pulse 8: activated nodes: 8355, borderline nodes: 3564, overall activation: 527.675, activation diff: 112.022, ratio: 0.212
#   pulse 9: activated nodes: 8456, borderline nodes: 3489, overall activation: 630.832, activation diff: 103.157, ratio: 0.164
#   pulse 10: activated nodes: 8532, borderline nodes: 3452, overall activation: 722.332, activation diff: 91.500, ratio: 0.127
#   pulse 11: activated nodes: 8568, borderline nodes: 3433, overall activation: 801.394, activation diff: 79.062, ratio: 0.099
#   pulse 12: activated nodes: 8607, borderline nodes: 3419, overall activation: 868.410, activation diff: 67.016, ratio: 0.077
#   pulse 13: activated nodes: 8627, borderline nodes: 3410, overall activation: 924.391, activation diff: 55.981, ratio: 0.061
#   pulse 14: activated nodes: 8628, borderline nodes: 3407, overall activation: 970.626, activation diff: 46.235, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8628
#   final overall activation: 970.6
#   number of spread. activ. pulses: 14
#   running time: 483

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9756659   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9292592   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9183217   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.90056103   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.84850657   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.77546716   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4599523   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.4590264   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.45778865   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.45752883   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.45531815   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.44986278   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4431423   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4421744   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.44156307   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.43632072   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.43546847   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.43467364   REFERENCES
1   Q1   politicalsketche00retsrich_153   19   0.43382984   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.43381342   REFERENCES
