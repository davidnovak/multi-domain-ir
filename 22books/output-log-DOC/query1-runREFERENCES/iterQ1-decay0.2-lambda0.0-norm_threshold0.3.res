###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 867.601, activation diff: 894.332, ratio: 1.031
#   pulse 3: activated nodes: 8128, borderline nodes: 3940, overall activation: 477.653, activation diff: 1345.254, ratio: 2.816
#   pulse 4: activated nodes: 10196, borderline nodes: 4461, overall activation: 3311.517, activation diff: 3789.170, ratio: 1.144
#   pulse 5: activated nodes: 11156, borderline nodes: 2412, overall activation: 1135.815, activation diff: 4447.332, ratio: 3.916
#   pulse 6: activated nodes: 11261, borderline nodes: 721, overall activation: 3555.982, activation diff: 4691.797, ratio: 1.319
#   pulse 7: activated nodes: 11293, borderline nodes: 590, overall activation: 1199.559, activation diff: 4755.541, ratio: 3.964
#   pulse 8: activated nodes: 11303, borderline nodes: 494, overall activation: 3570.172, activation diff: 4769.730, ratio: 1.336
#   pulse 9: activated nodes: 11309, borderline nodes: 495, overall activation: 1206.103, activation diff: 4776.274, ratio: 3.960
#   pulse 10: activated nodes: 11309, borderline nodes: 484, overall activation: 3571.849, activation diff: 4777.952, ratio: 1.338
#   pulse 11: activated nodes: 11309, borderline nodes: 484, overall activation: 1206.993, activation diff: 4778.843, ratio: 3.959
#   pulse 12: activated nodes: 11310, borderline nodes: 484, overall activation: 3572.077, activation diff: 4779.070, ratio: 1.338
#   pulse 13: activated nodes: 11310, borderline nodes: 484, overall activation: 1207.127, activation diff: 4779.204, ratio: 3.959
#   pulse 14: activated nodes: 11310, borderline nodes: 484, overall activation: 3572.111, activation diff: 4779.238, ratio: 1.338
#   pulse 15: activated nodes: 11310, borderline nodes: 484, overall activation: 1207.149, activation diff: 4779.260, ratio: 3.959

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11310
#   final overall activation: 1207.1
#   number of spread. activ. pulses: 15
#   running time: 505

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
