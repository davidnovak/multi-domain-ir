###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 496.997, activation diff: 477.169, ratio: 0.960
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1118.749, activation diff: 621.752, ratio: 0.556
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2705.782, activation diff: 1587.033, ratio: 0.587
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4199.494, activation diff: 1493.712, ratio: 0.356
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5289.761, activation diff: 1090.267, ratio: 0.206
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 6008.174, activation diff: 718.413, ratio: 0.120
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 6458.517, activation diff: 450.343, ratio: 0.070
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 6733.323, activation diff: 274.807, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6733.3
#   number of spread. activ. pulses: 9
#   running time: 455

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99597156   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9829886   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9822367   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96622443   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92222315   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.89682865   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8931089   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89245504   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.89142954   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   10   0.89115167   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   11   0.8908756   REFERENCES
1   Q1   essentialsinmod01howegoog_474   12   0.89077675   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.89065444   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   14   0.89047426   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   15   0.89041626   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   16   0.8903997   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.89032006   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   18   0.8902998   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   19   0.8902558   REFERENCES
1   Q1   europesincenapol00leveuoft_353   20   0.8902379   REFERENCES
