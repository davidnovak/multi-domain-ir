###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 933.825, activation diff: 929.620, ratio: 0.995
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1357.280, activation diff: 613.463, ratio: 0.452
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3320.510, activation diff: 1963.230, ratio: 0.591
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4478.827, activation diff: 1158.316, ratio: 0.259
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4993.636, activation diff: 514.810, ratio: 0.103
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 5201.868, activation diff: 208.231, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5201.9
#   number of spread. activ. pulses: 7
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.998802   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9890371   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9878346   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.973568   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9303317   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.90832925   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7987208   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7980184   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7979418   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.79782426   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.79777527   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.7977601   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.7976425   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   14   0.7976345   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.79757655   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.797529   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.7974686   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7974429   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.797428   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   20   0.79737186   REFERENCES
