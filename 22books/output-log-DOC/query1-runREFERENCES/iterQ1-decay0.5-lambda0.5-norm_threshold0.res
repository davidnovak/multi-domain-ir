###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 285.682, activation diff: 265.854, ratio: 0.931
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 476.002, activation diff: 190.320, ratio: 0.400
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 785.963, activation diff: 309.960, ratio: 0.394
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1032.474, activation diff: 246.511, ratio: 0.239
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1197.554, activation diff: 165.080, ratio: 0.138
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1300.050, activation diff: 102.496, ratio: 0.079
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1361.057, activation diff: 61.007, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1361.1
#   number of spread. activ. pulses: 8
#   running time: 381

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99223137   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.97511977   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.97383124   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9548598   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.91163296   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8813964   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.48381925   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.48371488   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.48290282   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.48289472   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.4791956   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47575927   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.4711902   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4704588   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4704563   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.46844906   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.46525586   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   18   0.4650857   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.46499085   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.46487242   REFERENCES
