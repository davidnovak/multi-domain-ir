###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 224.944, activation diff: 209.572, ratio: 0.932
#   pulse 3: activated nodes: 7946, borderline nodes: 4186, overall activation: 467.540, activation diff: 243.119, ratio: 0.520
#   pulse 4: activated nodes: 9965, borderline nodes: 5320, overall activation: 1388.679, activation diff: 921.139, ratio: 0.663
#   pulse 5: activated nodes: 11006, borderline nodes: 3129, overall activation: 2394.640, activation diff: 1005.961, ratio: 0.420
#   pulse 6: activated nodes: 11195, borderline nodes: 1646, overall activation: 3247.623, activation diff: 852.984, ratio: 0.263
#   pulse 7: activated nodes: 11236, borderline nodes: 808, overall activation: 3863.227, activation diff: 615.604, ratio: 0.159
#   pulse 8: activated nodes: 11287, borderline nodes: 606, overall activation: 4273.027, activation diff: 409.800, ratio: 0.096
#   pulse 9: activated nodes: 11296, borderline nodes: 483, overall activation: 4534.440, activation diff: 261.413, ratio: 0.058
#   pulse 10: activated nodes: 11310, borderline nodes: 432, overall activation: 4697.250, activation diff: 162.810, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11310
#   final overall activation: 4697.2
#   number of spread. activ. pulses: 10
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9970175   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9831096   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.98135054   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.96336913   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9111315   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8815449   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79617876   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7951721   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.79463685   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.79424334   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.79420197   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.7941162   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.7940581   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   14   0.7938086   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.7937869   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7937619   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.7936804   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7936327   REFERENCES
1   Q1   europesincenapol00leveuoft_16   19   0.79353064   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.79348004   REFERENCES
