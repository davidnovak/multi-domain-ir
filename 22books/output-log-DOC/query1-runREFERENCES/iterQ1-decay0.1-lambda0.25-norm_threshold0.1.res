###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 847.102, activation diff: 846.653, ratio: 0.999
#   pulse 3: activated nodes: 8645, borderline nodes: 3407, overall activation: 1201.873, activation diff: 751.257, ratio: 0.625
#   pulse 4: activated nodes: 10773, borderline nodes: 2982, overall activation: 3819.605, activation diff: 2661.528, ratio: 0.697
#   pulse 5: activated nodes: 11240, borderline nodes: 1355, overall activation: 5513.909, activation diff: 1694.762, ratio: 0.307
#   pulse 6: activated nodes: 11309, borderline nodes: 184, overall activation: 6346.710, activation diff: 832.801, ratio: 0.131
#   pulse 7: activated nodes: 11331, borderline nodes: 65, overall activation: 6702.668, activation diff: 355.958, ratio: 0.053
#   pulse 8: activated nodes: 11334, borderline nodes: 38, overall activation: 6846.747, activation diff: 144.079, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 6846.7
#   number of spread. activ. pulses: 8
#   running time: 601

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993299   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9894968   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9881701   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.97293013   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.92551947   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9022505   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.89956695   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.8993513   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8993511   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.89934707   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.899347   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.8993342   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.8993082   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.8992952   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.8992897   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.8992832   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.89927536   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.89927125   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.8992537   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   20   0.89925104   REFERENCES
