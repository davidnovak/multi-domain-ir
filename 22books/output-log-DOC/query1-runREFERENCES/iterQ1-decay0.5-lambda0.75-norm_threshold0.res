###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 84.752, activation diff: 72.944, ratio: 0.861
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 183.658, activation diff: 99.251, ratio: 0.540
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 326.683, activation diff: 143.036, ratio: 0.438
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 483.188, activation diff: 156.506, ratio: 0.324
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 634.926, activation diff: 151.738, ratio: 0.239
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 772.821, activation diff: 137.895, ratio: 0.178
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 893.351, activation diff: 120.530, ratio: 0.135
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 996.047, activation diff: 102.696, ratio: 0.103
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1082.000, activation diff: 85.953, ratio: 0.079
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1153.003, activation diff: 71.003, ratio: 0.062
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1211.070, activation diff: 58.067, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1211.1
#   number of spread. activ. pulses: 12
#   running time: 453

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9746954   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.94291764   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9423352   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.91570425   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8685235   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8272251   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.45450166   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.45403114   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.45355082   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.45309746   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.4493651   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.44477367   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.43940407   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.43838274   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.43826535   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.43524492   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.43237284   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   18   0.43184245   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.43160814   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.43093634   REFERENCES
