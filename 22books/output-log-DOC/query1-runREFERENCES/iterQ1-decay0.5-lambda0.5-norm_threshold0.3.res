###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 97.640, activation diff: 85.156, ratio: 0.872
#   pulse 3: activated nodes: 7364, borderline nodes: 4655, overall activation: 172.076, activation diff: 75.041, ratio: 0.436
#   pulse 4: activated nodes: 7461, borderline nodes: 4615, overall activation: 421.508, activation diff: 249.431, ratio: 0.592
#   pulse 5: activated nodes: 8274, borderline nodes: 3645, overall activation: 663.639, activation diff: 242.131, ratio: 0.365
#   pulse 6: activated nodes: 8487, borderline nodes: 3478, overall activation: 848.713, activation diff: 185.074, ratio: 0.218
#   pulse 7: activated nodes: 8601, borderline nodes: 3428, overall activation: 972.962, activation diff: 124.250, ratio: 0.128
#   pulse 8: activated nodes: 8628, borderline nodes: 3408, overall activation: 1050.717, activation diff: 77.755, ratio: 0.074
#   pulse 9: activated nodes: 8636, borderline nodes: 3404, overall activation: 1097.377, activation diff: 46.660, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8636
#   final overall activation: 1097.4
#   number of spread. activ. pulses: 9
#   running time: 397

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9930397   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9678936   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.96685195   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.94468516   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.89088774   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8440263   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.48363578   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.48325777   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.4828754   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.48273984   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.47840077   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.47385365   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.46822262   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.46763778   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.46738923   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.46478632   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   17   0.46052724   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.4605168   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.46041757   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.46010637   REFERENCES
