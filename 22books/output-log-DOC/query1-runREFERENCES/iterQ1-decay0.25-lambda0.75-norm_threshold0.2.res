###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 38.000, activation diff: 27.697, ratio: 0.729
#   pulse 3: activated nodes: 7013, borderline nodes: 4933, overall activation: 105.302, activation diff: 67.904, ratio: 0.645
#   pulse 4: activated nodes: 9112, borderline nodes: 5853, overall activation: 270.772, activation diff: 165.537, ratio: 0.611
#   pulse 5: activated nodes: 10145, borderline nodes: 5010, overall activation: 541.383, activation diff: 270.612, ratio: 0.500
#   pulse 6: activated nodes: 10688, borderline nodes: 4164, overall activation: 897.327, activation diff: 355.944, ratio: 0.397
#   pulse 7: activated nodes: 10974, borderline nodes: 3124, overall activation: 1295.071, activation diff: 397.743, ratio: 0.307
#   pulse 8: activated nodes: 11114, borderline nodes: 2314, overall activation: 1695.322, activation diff: 400.251, ratio: 0.236
#   pulse 9: activated nodes: 11188, borderline nodes: 1641, overall activation: 2072.710, activation diff: 377.388, ratio: 0.182
#   pulse 10: activated nodes: 11220, borderline nodes: 1190, overall activation: 2413.770, activation diff: 341.060, ratio: 0.141
#   pulse 11: activated nodes: 11226, borderline nodes: 908, overall activation: 2713.120, activation diff: 299.350, ratio: 0.110
#   pulse 12: activated nodes: 11270, borderline nodes: 781, overall activation: 2970.562, activation diff: 257.442, ratio: 0.087
#   pulse 13: activated nodes: 11278, borderline nodes: 690, overall activation: 3188.697, activation diff: 218.136, ratio: 0.068
#   pulse 14: activated nodes: 11281, borderline nodes: 650, overall activation: 3371.477, activation diff: 182.780, ratio: 0.054
#   pulse 15: activated nodes: 11286, borderline nodes: 605, overall activation: 3523.349, activation diff: 151.871, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11286
#   final overall activation: 3523.3
#   number of spread. activ. pulses: 15
#   running time: 551

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98582715   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9579417   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.95647174   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.93478084   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.8807285   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.83773625   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.72532195   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.72265285   REFERENCES
1   Q1   europesincenapol00leveuoft_16   9   0.72013736   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   10   0.71906155   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   11   0.7189519   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.7163901   REFERENCES
1   Q1   essentialsinmod01howegoog_474   13   0.71615946   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.71604514   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   15   0.71594006   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   16   0.71501815   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.7145425   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.7144423   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   19   0.7142133   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.71386045   REFERENCES
