###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 23.236, activation diff: 13.739, ratio: 0.591
#   pulse 3: activated nodes: 6260, borderline nodes: 5234, overall activation: 68.837, activation diff: 46.370, ratio: 0.674
#   pulse 4: activated nodes: 7663, borderline nodes: 5545, overall activation: 195.389, activation diff: 126.736, ratio: 0.649
#   pulse 5: activated nodes: 9229, borderline nodes: 5572, overall activation: 454.294, activation diff: 258.912, ratio: 0.570
#   pulse 6: activated nodes: 10190, borderline nodes: 4842, overall activation: 878.696, activation diff: 424.402, ratio: 0.483
#   pulse 7: activated nodes: 10748, borderline nodes: 3810, overall activation: 1438.411, activation diff: 559.716, ratio: 0.389
#   pulse 8: activated nodes: 11027, borderline nodes: 2827, overall activation: 2060.470, activation diff: 622.058, ratio: 0.302
#   pulse 9: activated nodes: 11158, borderline nodes: 2088, overall activation: 2682.979, activation diff: 622.510, ratio: 0.232
#   pulse 10: activated nodes: 11208, borderline nodes: 1465, overall activation: 3266.522, activation diff: 583.543, ratio: 0.179
#   pulse 11: activated nodes: 11225, borderline nodes: 1038, overall activation: 3791.122, activation diff: 524.599, ratio: 0.138
#   pulse 12: activated nodes: 11275, borderline nodes: 820, overall activation: 4249.650, activation diff: 458.528, ratio: 0.108
#   pulse 13: activated nodes: 11287, borderline nodes: 645, overall activation: 4642.862, activation diff: 393.212, ratio: 0.085
#   pulse 14: activated nodes: 11300, borderline nodes: 526, overall activation: 4975.499, activation diff: 332.637, ratio: 0.067
#   pulse 15: activated nodes: 11308, borderline nodes: 464, overall activation: 5254.260, activation diff: 278.761, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11308
#   final overall activation: 5254.3
#   number of spread. activ. pulses: 15
#   running time: 529

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98293936   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.94958436   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.94445163   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9249111   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.8679193   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   6   0.86747664   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   7   0.8674123   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.86379963   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8626309   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   10   0.8613524   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.86102813   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   12   0.8608041   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.8603399   REFERENCES
1   Q1   europesincenapol00leveuoft_353   14   0.8599341   REFERENCES
1   Q1   essentialsinmod01howegoog_471   15   0.8598704   REFERENCES
1   Q1   essentialsinmod01howegoog_14   16   0.85973716   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.85888886   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.8588622   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.85882026   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   20   0.8587465   REFERENCES
