###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 432.491, activation diff: 437.696, ratio: 1.012
#   pulse 3: activated nodes: 7878, borderline nodes: 4240, overall activation: 450.966, activation diff: 436.361, ratio: 0.968
#   pulse 4: activated nodes: 9884, borderline nodes: 5250, overall activation: 2182.951, activation diff: 1840.720, ratio: 0.843
#   pulse 5: activated nodes: 11031, borderline nodes: 3132, overall activation: 3264.961, activation diff: 1090.835, ratio: 0.334
#   pulse 6: activated nodes: 11168, borderline nodes: 1377, overall activation: 4068.250, activation diff: 803.289, ratio: 0.197
#   pulse 7: activated nodes: 11239, borderline nodes: 842, overall activation: 4465.780, activation diff: 397.530, ratio: 0.089
#   pulse 8: activated nodes: 11285, borderline nodes: 620, overall activation: 4637.915, activation diff: 172.135, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11285
#   final overall activation: 4637.9
#   number of spread. activ. pulses: 8
#   running time: 433

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99898934   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9864374   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9843662   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9661506   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.90919757   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.879388   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79941213   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.79895836   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.7988793   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.79887056   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.79885286   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.7988141   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.79877406   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   14   0.7987306   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.7987069   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   16   0.798699   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.7986891   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   18   0.79864633   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.79860854   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   20   0.79859054   REFERENCES
