###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 326.628, activation diff: 308.651, ratio: 0.945
#   pulse 3: activated nodes: 8452, borderline nodes: 3468, overall activation: 681.717, activation diff: 355.108, ratio: 0.521
#   pulse 4: activated nodes: 10538, borderline nodes: 3912, overall activation: 1741.632, activation diff: 1059.914, ratio: 0.609
#   pulse 5: activated nodes: 11196, borderline nodes: 2003, overall activation: 2806.505, activation diff: 1064.874, ratio: 0.379
#   pulse 6: activated nodes: 11265, borderline nodes: 707, overall activation: 3634.221, activation diff: 827.715, ratio: 0.228
#   pulse 7: activated nodes: 11303, borderline nodes: 435, overall activation: 4202.094, activation diff: 567.874, ratio: 0.135
#   pulse 8: activated nodes: 11314, borderline nodes: 290, overall activation: 4568.767, activation diff: 366.673, ratio: 0.080
#   pulse 9: activated nodes: 11321, borderline nodes: 206, overall activation: 4797.981, activation diff: 229.213, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 4798.0
#   number of spread. activ. pulses: 9
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99533904   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9806844   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9793581   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9617407   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9138298   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.8852249   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79317284   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.791954   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.79080105   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7902945   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.7901238   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.79009473   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.7900522   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   14   0.7899435   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.78944504   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7893841   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   17   0.78932136   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.78931636   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   19   0.7891867   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   20   0.78917825   REFERENCES
