###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.132, activation diff: 10.132, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 340.383, activation diff: 331.297, ratio: 0.973
#   pulse 3: activated nodes: 8832, borderline nodes: 4237, overall activation: 618.411, activation diff: 279.060, ratio: 0.451
#   pulse 4: activated nodes: 11231, borderline nodes: 5251, overall activation: 2412.332, activation diff: 1793.921, ratio: 0.744
#   pulse 5: activated nodes: 11385, borderline nodes: 1234, overall activation: 4075.755, activation diff: 1663.423, ratio: 0.408
#   pulse 6: activated nodes: 11438, borderline nodes: 297, overall activation: 5397.623, activation diff: 1321.867, ratio: 0.245
#   pulse 7: activated nodes: 11451, borderline nodes: 79, overall activation: 6272.657, activation diff: 875.034, ratio: 0.139
#   pulse 8: activated nodes: 11455, borderline nodes: 41, overall activation: 6801.437, activation diff: 528.780, ratio: 0.078
#   pulse 9: activated nodes: 11456, borderline nodes: 27, overall activation: 7108.649, activation diff: 307.212, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 7108.6
#   number of spread. activ. pulses: 9
#   running time: 1340

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99580556   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99451745   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99365175   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9933388   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9923378   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98839664   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79343724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7933707   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.79317164   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7930844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7930405   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.79296005   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7926575   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79265654   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.79259324   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.79256934   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7925631   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.79251564   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.7925105   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_383   20   0.79250246   REFERENCES:SIMDATES:SIMLOC
