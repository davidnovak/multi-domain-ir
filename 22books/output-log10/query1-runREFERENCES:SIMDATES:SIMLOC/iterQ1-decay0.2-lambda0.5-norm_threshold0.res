###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.589, activation diff: 11.589, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 504.710, activation diff: 493.120, ratio: 0.977
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1004.708, activation diff: 499.998, ratio: 0.498
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3061.235, activation diff: 2056.528, ratio: 0.672
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4833.111, activation diff: 1771.876, ratio: 0.367
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6058.470, activation diff: 1225.359, ratio: 0.202
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6804.897, activation diff: 746.427, ratio: 0.110
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 7236.945, activation diff: 432.048, ratio: 0.060
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 7481.816, activation diff: 244.872, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7481.8
#   number of spread. activ. pulses: 9
#   running time: 1302

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969183   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957586   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.995173   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947459   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99357545   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99057114   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79464114   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7945168   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7942239   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7941997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7941128   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7939303   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7938577   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.79380536   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7937894   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.7937702   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.79370826   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.793699   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.79366124   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.7935946   REFERENCES:SIMDATES:SIMLOC
