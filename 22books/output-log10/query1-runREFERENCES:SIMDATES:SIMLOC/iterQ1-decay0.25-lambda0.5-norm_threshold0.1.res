###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.132, activation diff: 10.132, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 319.745, activation diff: 310.658, ratio: 0.972
#   pulse 3: activated nodes: 8832, borderline nodes: 4237, overall activation: 572.459, activation diff: 253.683, ratio: 0.443
#   pulse 4: activated nodes: 11228, borderline nodes: 5296, overall activation: 2153.232, activation diff: 1580.772, ratio: 0.734
#   pulse 5: activated nodes: 11357, borderline nodes: 1345, overall activation: 3603.213, activation diff: 1449.981, ratio: 0.402
#   pulse 6: activated nodes: 11428, borderline nodes: 373, overall activation: 4764.118, activation diff: 1160.905, ratio: 0.244
#   pulse 7: activated nodes: 11445, borderline nodes: 118, overall activation: 5544.579, activation diff: 780.461, ratio: 0.141
#   pulse 8: activated nodes: 11449, borderline nodes: 78, overall activation: 6022.433, activation diff: 477.855, ratio: 0.079
#   pulse 9: activated nodes: 11450, borderline nodes: 77, overall activation: 6302.546, activation diff: 280.113, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 6302.5
#   number of spread. activ. pulses: 9
#   running time: 1366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99580544   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9945128   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99364394   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9933263   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9923179   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9883154   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7438473   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74378335   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7435927   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7435138   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74347425   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7433781   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7431091   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.743109   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7430456   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.7430258   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.74302036   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7429497   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.74294883   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.74294263   REFERENCES:SIMDATES:SIMLOC
