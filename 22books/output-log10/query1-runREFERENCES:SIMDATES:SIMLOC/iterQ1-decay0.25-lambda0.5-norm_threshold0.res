###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.589, activation diff: 11.589, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 473.961, activation diff: 462.371, ratio: 0.976
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 927.820, activation diff: 453.859, ratio: 0.489
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2743.496, activation diff: 1815.676, ratio: 0.662
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4311.451, activation diff: 1567.955, ratio: 0.364
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 5411.136, activation diff: 1099.685, ratio: 0.203
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 6089.287, activation diff: 678.151, ratio: 0.111
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 6485.228, activation diff: 395.941, ratio: 0.061
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 6711.226, activation diff: 225.998, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6711.2
#   number of spread. activ. pulses: 9
#   running time: 1306

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99691826   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957568   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9951714   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947401   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99356234   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99053025   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7449716   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74485755   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74458075   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.74455976   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74447817   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7442918   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.744233   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7441897   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7441745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.74415374   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.7440952   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.74408615   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.74404955   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.743971   REFERENCES:SIMDATES:SIMLOC
