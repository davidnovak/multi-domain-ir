###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.384, activation diff: 17.384, ratio: 1.209
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 995.371, activation diff: 992.396, ratio: 0.997
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1326.527, activation diff: 591.969, ratio: 0.446
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 4469.145, activation diff: 3142.618, ratio: 0.703
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6015.227, activation diff: 1546.082, ratio: 0.257
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6648.036, activation diff: 632.809, ratio: 0.095
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 6876.971, activation diff: 228.935, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6877.0
#   number of spread. activ. pulses: 7
#   running time: 1195

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99926615   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9989531   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9987637   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99863577   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99767977   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.996423   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7491088   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7489854   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74890643   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7488748   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7488728   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7488711   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.7487987   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.74879694   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.7487956   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.7487785   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.74874777   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7487356   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7487279   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.74871886   REFERENCES:SIMDATES:SIMLOC
