###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.642, activation diff: 8.642, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 199.593, activation diff: 193.622, ratio: 0.970
#   pulse 3: activated nodes: 8632, borderline nodes: 5390, overall activation: 323.479, activation diff: 127.644, ratio: 0.395
#   pulse 4: activated nodes: 10888, borderline nodes: 7103, overall activation: 1673.211, activation diff: 1349.732, ratio: 0.807
#   pulse 5: activated nodes: 11161, borderline nodes: 3391, overall activation: 2970.674, activation diff: 1297.463, ratio: 0.437
#   pulse 6: activated nodes: 11366, borderline nodes: 1446, overall activation: 4199.091, activation diff: 1228.417, ratio: 0.293
#   pulse 7: activated nodes: 11422, borderline nodes: 375, overall activation: 5121.716, activation diff: 922.626, ratio: 0.180
#   pulse 8: activated nodes: 11431, borderline nodes: 199, overall activation: 5729.579, activation diff: 607.863, ratio: 0.106
#   pulse 9: activated nodes: 11437, borderline nodes: 142, overall activation: 6100.128, activation diff: 370.549, ratio: 0.061
#   pulse 10: activated nodes: 11440, borderline nodes: 121, overall activation: 6318.358, activation diff: 218.230, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6318.4
#   number of spread. activ. pulses: 10
#   running time: 1477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9968963   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99630904   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9956033   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99556184   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9947686   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9912704   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74621147   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74618703   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7461325   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7461275   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.74607193   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7460639   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74592674   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7459133   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.74588203   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.745831   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7458135   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7458043   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.74579984   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.7457981   REFERENCES:SIMDATES:SIMLOC
