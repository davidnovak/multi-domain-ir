###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.691, activation diff: 10.691, ratio: 1.390
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 221.236, activation diff: 222.679, ratio: 1.007
#   pulse 3: activated nodes: 8533, borderline nodes: 5695, overall activation: 122.421, activation diff: 208.690, ratio: 1.705
#   pulse 4: activated nodes: 10006, borderline nodes: 6924, overall activation: 1450.992, activation diff: 1371.575, ratio: 0.945
#   pulse 5: activated nodes: 10395, borderline nodes: 4760, overall activation: 1920.846, activation diff: 521.125, ratio: 0.271
#   pulse 6: activated nodes: 10828, borderline nodes: 4499, overall activation: 2563.624, activation diff: 642.793, ratio: 0.251
#   pulse 7: activated nodes: 10921, borderline nodes: 3756, overall activation: 2925.821, activation diff: 362.197, ratio: 0.124
#   pulse 8: activated nodes: 10943, borderline nodes: 3629, overall activation: 3134.960, activation diff: 209.139, ratio: 0.067
#   pulse 9: activated nodes: 10952, borderline nodes: 3586, overall activation: 3245.614, activation diff: 110.654, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10952
#   final overall activation: 3245.6
#   number of spread. activ. pulses: 9
#   running time: 1414

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998235   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997791   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996538   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99955994   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99852043   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99713236   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49989745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49989706   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.49989223   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.49988616   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49988377   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49988315   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.49988312   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.49988288   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.49988008   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49988005   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.49987927   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.49987864   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.49987832   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49987763   REFERENCES:SIMDATES:SIMLOC
