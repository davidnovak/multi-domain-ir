###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.795, activation diff: 5.795, ratio: 0.659
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 90.391, activation diff: 82.476, ratio: 0.912
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 207.405, activation diff: 117.183, ratio: 0.565
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 491.141, activation diff: 283.737, ratio: 0.578
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 851.824, activation diff: 360.683, ratio: 0.423
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1231.958, activation diff: 380.134, ratio: 0.309
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1593.346, activation diff: 361.388, ratio: 0.227
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 1917.801, activation diff: 324.454, ratio: 0.169
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2199.444, activation diff: 281.643, ratio: 0.128
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 2438.191, activation diff: 238.747, ratio: 0.098
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 2636.908, activation diff: 198.717, ratio: 0.075
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 2799.994, activation diff: 163.086, ratio: 0.058
#   pulse 13: activated nodes: 10971, borderline nodes: 3512, overall activation: 2932.425, activation diff: 132.431, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2932.4
#   number of spread. activ. pulses: 13
#   running time: 1448

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9851476   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98034745   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9781232   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97659254   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97401166   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96358234   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47813112   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.47794995   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.47741693   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.47728738   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.47715688   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.4770419   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.4767848   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47646722   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   15   0.47642463   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.47640613   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.47636214   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.47631538   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.4762795   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   20   0.4762273   REFERENCES:SIMDATES:SIMLOC
