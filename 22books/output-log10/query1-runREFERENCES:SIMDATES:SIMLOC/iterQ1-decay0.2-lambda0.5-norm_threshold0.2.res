###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.642, activation diff: 8.642, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 212.397, activation diff: 206.425, ratio: 0.972
#   pulse 3: activated nodes: 8632, borderline nodes: 5390, overall activation: 348.533, activation diff: 140.127, ratio: 0.402
#   pulse 4: activated nodes: 10908, borderline nodes: 7099, overall activation: 1869.359, activation diff: 1520.827, ratio: 0.814
#   pulse 5: activated nodes: 11184, borderline nodes: 3259, overall activation: 3345.975, activation diff: 1476.616, ratio: 0.441
#   pulse 6: activated nodes: 11377, borderline nodes: 1247, overall activation: 4760.099, activation diff: 1414.124, ratio: 0.297
#   pulse 7: activated nodes: 11429, borderline nodes: 284, overall activation: 5805.941, activation diff: 1045.842, ratio: 0.180
#   pulse 8: activated nodes: 11445, borderline nodes: 108, overall activation: 6481.802, activation diff: 675.862, ratio: 0.104
#   pulse 9: activated nodes: 11451, borderline nodes: 62, overall activation: 6888.043, activation diff: 406.241, ratio: 0.059
#   pulse 10: activated nodes: 11452, borderline nodes: 51, overall activation: 7124.961, activation diff: 236.918, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7125.0
#   number of spread. activ. pulses: 10
#   running time: 1299

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9968966   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99631774   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99562085   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9955946   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99478805   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9913576   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7959596   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7959349   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7958812   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.79587114   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.79582405   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.7958132   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7956622   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7956512   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.7956163   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.7955803   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.79554224   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   18   0.7955403   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.79553014   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7955288   REFERENCES:SIMDATES:SIMLOC
