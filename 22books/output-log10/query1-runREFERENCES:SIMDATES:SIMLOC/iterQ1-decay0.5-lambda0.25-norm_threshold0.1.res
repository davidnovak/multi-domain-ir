###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.198, activation diff: 15.198, ratio: 1.246
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 493.452, activation diff: 494.255, ratio: 1.002
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 430.100, activation diff: 309.922, ratio: 0.721
#   pulse 4: activated nodes: 10757, borderline nodes: 4912, overall activation: 1916.104, activation diff: 1490.943, ratio: 0.778
#   pulse 5: activated nodes: 10930, borderline nodes: 3764, overall activation: 2625.104, activation diff: 709.000, ratio: 0.270
#   pulse 6: activated nodes: 10957, borderline nodes: 3561, overall activation: 3046.475, activation diff: 421.371, ratio: 0.138
#   pulse 7: activated nodes: 10962, borderline nodes: 3529, overall activation: 3257.045, activation diff: 210.569, ratio: 0.065
#   pulse 8: activated nodes: 10968, borderline nodes: 3522, overall activation: 3351.486, activation diff: 94.441, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 3351.5
#   number of spread. activ. pulses: 8
#   running time: 1386

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996271   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9995664   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.999498   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99942434   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848783   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974108   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49975318   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.4997403   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.4997264   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.49971497   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.499712   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.49970996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49970895   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.49969465   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.49969348   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.49969083   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.49968255   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   18   0.49968022   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   19   0.49967846   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.499677   REFERENCES:SIMDATES:SIMLOC
