###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.198, activation diff: 15.198, ratio: 1.246
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 881.560, activation diff: 882.363, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 988.808, activation diff: 776.483, ratio: 0.785
#   pulse 4: activated nodes: 11316, borderline nodes: 4174, overall activation: 5183.083, activation diff: 4219.042, ratio: 0.814
#   pulse 5: activated nodes: 11420, borderline nodes: 552, overall activation: 7508.452, activation diff: 2325.642, ratio: 0.310
#   pulse 6: activated nodes: 11450, borderline nodes: 75, overall activation: 8613.539, activation diff: 1105.087, ratio: 0.128
#   pulse 7: activated nodes: 11458, borderline nodes: 22, overall activation: 9023.084, activation diff: 409.545, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 9023.1
#   number of spread. activ. pulses: 7
#   running time: 1163

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99850845   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99830836   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9980234   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99798936   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99707425   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9955587   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8982234   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8981372   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8981342   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.8980802   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.89806205   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.8980497   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.89800495   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   14   0.8979836   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.8979744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.89797133   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.8979368   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.89792967   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.8979184   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   20   0.8979182   REFERENCES:SIMDATES:SIMLOC
