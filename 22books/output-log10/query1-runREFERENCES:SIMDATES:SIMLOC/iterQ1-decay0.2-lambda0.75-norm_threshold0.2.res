###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.321, activation diff: 4.321, ratio: 0.590
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 34.641, activation diff: 28.779, ratio: 0.831
#   pulse 3: activated nodes: 8253, borderline nodes: 6245, overall activation: 92.553, activation diff: 58.580, ratio: 0.633
#   pulse 4: activated nodes: 9645, borderline nodes: 6891, overall activation: 346.846, activation diff: 254.306, ratio: 0.733
#   pulse 5: activated nodes: 10586, borderline nodes: 5543, overall activation: 816.808, activation diff: 469.962, ratio: 0.575
#   pulse 6: activated nodes: 11162, borderline nodes: 4415, overall activation: 1506.034, activation diff: 689.225, ratio: 0.458
#   pulse 7: activated nodes: 11305, borderline nodes: 2509, overall activation: 2299.023, activation diff: 792.990, ratio: 0.345
#   pulse 8: activated nodes: 11399, borderline nodes: 1088, overall activation: 3111.461, activation diff: 812.437, ratio: 0.261
#   pulse 9: activated nodes: 11422, borderline nodes: 500, overall activation: 3876.727, activation diff: 765.266, ratio: 0.197
#   pulse 10: activated nodes: 11436, borderline nodes: 275, overall activation: 4555.289, activation diff: 678.563, ratio: 0.149
#   pulse 11: activated nodes: 11442, borderline nodes: 143, overall activation: 5133.206, activation diff: 577.916, ratio: 0.113
#   pulse 12: activated nodes: 11444, borderline nodes: 100, overall activation: 5611.790, activation diff: 478.584, ratio: 0.085
#   pulse 13: activated nodes: 11448, borderline nodes: 68, overall activation: 6000.990, activation diff: 389.200, ratio: 0.065
#   pulse 14: activated nodes: 11453, borderline nodes: 50, overall activation: 6313.901, activation diff: 312.911, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 6313.9
#   number of spread. activ. pulses: 14
#   running time: 1373

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9829161   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9774663   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9742519   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.97385883   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9696864   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95702946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7685896   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7684718   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.76833385   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7682198   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7679922   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   12   0.7673758   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.7673492   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7672653   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7671893   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.7670571   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.76694906   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.76675457   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.76663804   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.76661944   REFERENCES:SIMDATES:SIMLOC
