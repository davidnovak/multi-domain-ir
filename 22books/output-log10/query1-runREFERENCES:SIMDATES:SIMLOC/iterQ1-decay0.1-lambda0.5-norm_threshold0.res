###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.589, activation diff: 11.589, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 566.207, activation diff: 554.618, ratio: 0.980
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1163.067, activation diff: 596.860, ratio: 0.513
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3736.531, activation diff: 2573.464, ratio: 0.689
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5926.519, activation diff: 2189.988, ratio: 0.370
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 7400.180, activation diff: 1473.661, ratio: 0.199
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 8278.255, activation diff: 878.075, ratio: 0.106
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 8778.294, activation diff: 500.039, ratio: 0.057
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 9058.115, activation diff: 279.821, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 9058.1
#   number of spread. activ. pulses: 9
#   running time: 1408

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969184   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957615   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99517524   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947553   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99359643   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99063754   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8939819   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89383554   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8935108   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8934802   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.893382   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.893208   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.893108   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.89303696   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.89301956   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.89300257   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.89293593   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.89292663   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.8928813   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.89284   REFERENCES:SIMDATES:SIMLOC
