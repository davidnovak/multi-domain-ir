###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.564, activation diff: 3.564, ratio: 0.543
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 13.975, activation diff: 9.275, ratio: 0.664
#   pulse 3: activated nodes: 7794, borderline nodes: 6930, overall activation: 40.476, activation diff: 27.696, ratio: 0.684
#   pulse 4: activated nodes: 8056, borderline nodes: 6493, overall activation: 152.460, activation diff: 112.339, ratio: 0.737
#   pulse 5: activated nodes: 9231, borderline nodes: 6450, overall activation: 435.263, activation diff: 282.839, ratio: 0.650
#   pulse 6: activated nodes: 10566, borderline nodes: 6167, overall activation: 989.146, activation diff: 553.883, ratio: 0.560
#   pulse 7: activated nodes: 11126, borderline nodes: 4642, overall activation: 1803.171, activation diff: 814.025, ratio: 0.451
#   pulse 8: activated nodes: 11278, borderline nodes: 2866, overall activation: 2755.049, activation diff: 951.878, ratio: 0.346
#   pulse 9: activated nodes: 11354, borderline nodes: 1416, overall activation: 3749.912, activation diff: 994.863, ratio: 0.265
#   pulse 10: activated nodes: 11413, borderline nodes: 649, overall activation: 4692.985, activation diff: 943.073, ratio: 0.201
#   pulse 11: activated nodes: 11436, borderline nodes: 320, overall activation: 5530.935, activation diff: 837.951, ratio: 0.152
#   pulse 12: activated nodes: 11441, borderline nodes: 160, overall activation: 6244.990, activation diff: 714.055, ratio: 0.114
#   pulse 13: activated nodes: 11444, borderline nodes: 98, overall activation: 6836.067, activation diff: 591.077, ratio: 0.086
#   pulse 14: activated nodes: 11448, borderline nodes: 63, overall activation: 7316.572, activation diff: 480.505, ratio: 0.066
#   pulse 15: activated nodes: 11451, borderline nodes: 45, overall activation: 7702.731, activation diff: 386.159, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 7702.7
#   number of spread. activ. pulses: 15
#   running time: 1488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98031133   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9746502   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9740814   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97209024   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9608273   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9511581   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.8678955   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.8675359   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.86752224   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.86746204   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.86745536   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.8671702   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.8669699   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.86672413   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.8663798   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   16   0.8663481   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   17   0.8663417   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   18   0.8662596   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8660466   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.86603355   REFERENCES:SIMDATES:SIMLOC
