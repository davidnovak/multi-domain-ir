###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.127, activation diff: 7.127, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 108.552, activation diff: 104.699, ratio: 0.965
#   pulse 3: activated nodes: 8322, borderline nodes: 6135, overall activation: 155.250, activation diff: 53.097, ratio: 0.342
#   pulse 4: activated nodes: 10242, borderline nodes: 7861, overall activation: 1181.933, activation diff: 1026.816, ratio: 0.869
#   pulse 5: activated nodes: 10681, borderline nodes: 4633, overall activation: 2243.286, activation diff: 1061.353, ratio: 0.473
#   pulse 6: activated nodes: 11285, borderline nodes: 3495, overall activation: 3518.403, activation diff: 1275.116, ratio: 0.362
#   pulse 7: activated nodes: 11400, borderline nodes: 989, overall activation: 4576.552, activation diff: 1058.149, ratio: 0.231
#   pulse 8: activated nodes: 11422, borderline nodes: 431, overall activation: 5337.623, activation diff: 761.071, ratio: 0.143
#   pulse 9: activated nodes: 11430, borderline nodes: 241, overall activation: 5832.231, activation diff: 494.608, ratio: 0.085
#   pulse 10: activated nodes: 11434, borderline nodes: 186, overall activation: 6135.159, activation diff: 302.928, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 6135.2
#   number of spread. activ. pulses: 10
#   running time: 1447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99566734   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99490666   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.994068   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9935491   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9920224   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9875704   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.74545383   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7454339   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.74538386   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7453793   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.745374   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7453276   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7452493   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.74523103   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.7451683   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.74514776   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.74514043   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.74509585   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   19   0.74508756   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.7450774   REFERENCES:SIMDATES:SIMLOC
