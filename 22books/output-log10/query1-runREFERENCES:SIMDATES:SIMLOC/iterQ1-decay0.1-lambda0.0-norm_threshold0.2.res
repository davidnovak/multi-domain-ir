###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.285, activation diff: 17.285, ratio: 1.532
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1134.505, activation diff: 1145.790, ratio: 1.010
#   pulse 3: activated nodes: 8762, borderline nodes: 4759, overall activation: 608.026, activation diff: 1742.531, ratio: 2.866
#   pulse 4: activated nodes: 11281, borderline nodes: 5570, overall activation: 6514.476, activation diff: 6797.408, ratio: 1.043
#   pulse 5: activated nodes: 11406, borderline nodes: 842, overall activation: 5680.791, activation diff: 4481.378, ratio: 0.789
#   pulse 6: activated nodes: 11445, borderline nodes: 115, overall activation: 8891.463, activation diff: 3347.040, ratio: 0.376
#   pulse 7: activated nodes: 11457, borderline nodes: 30, overall activation: 9111.544, activation diff: 239.391, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 9111.5
#   number of spread. activ. pulses: 7
#   running time: 1312

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999902   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999085   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99894136   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99798554   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   7   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   8   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   14   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   16   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   18   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   19   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   20   0.9   REFERENCES:SIMDATES:SIMLOC
