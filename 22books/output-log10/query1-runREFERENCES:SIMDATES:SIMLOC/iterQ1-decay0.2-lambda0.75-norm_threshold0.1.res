###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.066, activation diff: 5.066, ratio: 0.628
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 74.415, activation diff: 67.503, ratio: 0.907
#   pulse 3: activated nodes: 8689, borderline nodes: 5079, overall activation: 187.957, activation diff: 113.909, ratio: 0.606
#   pulse 4: activated nodes: 10851, borderline nodes: 6294, overall activation: 629.608, activation diff: 441.651, ratio: 0.701
#   pulse 5: activated nodes: 11191, borderline nodes: 3798, overall activation: 1311.095, activation diff: 681.488, ratio: 0.520
#   pulse 6: activated nodes: 11341, borderline nodes: 1891, overall activation: 2146.277, activation diff: 835.182, ratio: 0.389
#   pulse 7: activated nodes: 11415, borderline nodes: 601, overall activation: 3013.570, activation diff: 867.293, ratio: 0.288
#   pulse 8: activated nodes: 11435, borderline nodes: 249, overall activation: 3827.418, activation diff: 813.848, ratio: 0.213
#   pulse 9: activated nodes: 11445, borderline nodes: 110, overall activation: 4542.442, activation diff: 715.024, ratio: 0.157
#   pulse 10: activated nodes: 11452, borderline nodes: 63, overall activation: 5144.824, activation diff: 602.382, ratio: 0.117
#   pulse 11: activated nodes: 11454, borderline nodes: 49, overall activation: 5638.724, activation diff: 493.900, ratio: 0.088
#   pulse 12: activated nodes: 11455, borderline nodes: 42, overall activation: 6036.962, activation diff: 398.238, ratio: 0.066
#   pulse 13: activated nodes: 11455, borderline nodes: 34, overall activation: 6354.730, activation diff: 317.768, ratio: 0.050
#   pulse 14: activated nodes: 11456, borderline nodes: 28, overall activation: 6606.554, activation diff: 251.824, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 6606.6
#   number of spread. activ. pulses: 14
#   running time: 1545

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98666686   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98266345   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97968626   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9796632   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9779423   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96789247   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7720749   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.77185744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.77156323   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7711587   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.77087146   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.77052516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7703995   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7703801   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.77019215   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.77014846   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7700103   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.7700046   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7699907   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7698308   REFERENCES:SIMDATES:SIMLOC
