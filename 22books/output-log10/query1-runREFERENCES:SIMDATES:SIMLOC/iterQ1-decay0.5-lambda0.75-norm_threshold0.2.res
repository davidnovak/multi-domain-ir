###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.321, activation diff: 4.321, ratio: 0.590
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 24.534, activation diff: 18.673, ratio: 0.761
#   pulse 3: activated nodes: 8253, borderline nodes: 6245, overall activation: 60.783, activation diff: 36.916, ratio: 0.607
#   pulse 4: activated nodes: 9101, borderline nodes: 6378, overall activation: 188.534, activation diff: 127.802, ratio: 0.678
#   pulse 5: activated nodes: 9831, borderline nodes: 5489, overall activation: 409.935, activation diff: 221.400, ratio: 0.540
#   pulse 6: activated nodes: 10552, borderline nodes: 5241, overall activation: 702.742, activation diff: 292.807, ratio: 0.417
#   pulse 7: activated nodes: 10736, borderline nodes: 4821, overall activation: 1024.215, activation diff: 321.473, ratio: 0.314
#   pulse 8: activated nodes: 10825, borderline nodes: 4488, overall activation: 1342.726, activation diff: 318.511, ratio: 0.237
#   pulse 9: activated nodes: 10858, borderline nodes: 4115, overall activation: 1642.140, activation diff: 299.414, ratio: 0.182
#   pulse 10: activated nodes: 10917, borderline nodes: 3801, overall activation: 1913.229, activation diff: 271.089, ratio: 0.142
#   pulse 11: activated nodes: 10938, borderline nodes: 3680, overall activation: 2152.612, activation diff: 239.384, ratio: 0.111
#   pulse 12: activated nodes: 10943, borderline nodes: 3624, overall activation: 2360.934, activation diff: 208.322, ratio: 0.088
#   pulse 13: activated nodes: 10953, borderline nodes: 3586, overall activation: 2540.066, activation diff: 179.132, ratio: 0.071
#   pulse 14: activated nodes: 10958, borderline nodes: 3566, overall activation: 2692.207, activation diff: 152.140, ratio: 0.057
#   pulse 15: activated nodes: 10959, borderline nodes: 3556, overall activation: 2819.913, activation diff: 127.706, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10959
#   final overall activation: 2819.9
#   number of spread. activ. pulses: 15
#   running time: 1573

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98661244   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9819102   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.97915405   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.97879153   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.97441363   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9628978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.48461956   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.48461545   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.48450089   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.48444405   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   11   0.48443142   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   12   0.4843282   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.48401344   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   14   0.48382688   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.48382267   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.48381016   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   17   0.48375392   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.48372012   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.48371804   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   20   0.4836532   REFERENCES:SIMDATES:SIMLOC
