###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.066, activation diff: 5.066, ratio: 0.628
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 82.570, activation diff: 75.658, ratio: 0.916
#   pulse 3: activated nodes: 8689, borderline nodes: 5079, overall activation: 211.948, activation diff: 129.745, ratio: 0.612
#   pulse 4: activated nodes: 10895, borderline nodes: 6288, overall activation: 756.433, activation diff: 544.486, ratio: 0.720
#   pulse 5: activated nodes: 11203, borderline nodes: 3508, overall activation: 1616.358, activation diff: 859.924, ratio: 0.532
#   pulse 6: activated nodes: 11361, borderline nodes: 1557, overall activation: 2686.194, activation diff: 1069.836, ratio: 0.398
#   pulse 7: activated nodes: 11427, borderline nodes: 414, overall activation: 3795.065, activation diff: 1108.871, ratio: 0.292
#   pulse 8: activated nodes: 11443, borderline nodes: 160, overall activation: 4818.821, activation diff: 1023.756, ratio: 0.212
#   pulse 9: activated nodes: 11452, borderline nodes: 68, overall activation: 5703.787, activation diff: 884.966, ratio: 0.155
#   pulse 10: activated nodes: 11455, borderline nodes: 35, overall activation: 6438.352, activation diff: 734.566, ratio: 0.114
#   pulse 11: activated nodes: 11457, borderline nodes: 26, overall activation: 7033.651, activation diff: 595.298, ratio: 0.085
#   pulse 12: activated nodes: 11458, borderline nodes: 18, overall activation: 7509.248, activation diff: 475.598, ratio: 0.063
#   pulse 13: activated nodes: 11458, borderline nodes: 15, overall activation: 7885.821, activation diff: 376.572, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 7885.8
#   number of spread. activ. pulses: 13
#   running time: 1375

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98226976   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9770812   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97338736   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97320855   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97126055   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9588184   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.85833955   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8579642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8575947   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8568758   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.85647273   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.855998   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.85591614   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8559109   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.85562146   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.8555236   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8553467   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.85523325   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.8552288   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.8550769   REFERENCES:SIMDATES:SIMLOC
