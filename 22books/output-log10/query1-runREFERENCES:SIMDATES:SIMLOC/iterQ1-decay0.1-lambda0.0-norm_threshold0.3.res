###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.255, activation diff: 14.255, ratio: 1.727
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 794.726, activation diff: 802.981, ratio: 1.010
#   pulse 3: activated nodes: 8646, borderline nodes: 5432, overall activation: 403.824, activation diff: 1198.550, ratio: 2.968
#   pulse 4: activated nodes: 11170, borderline nodes: 6887, overall activation: 5889.500, activation diff: 6170.452, ratio: 1.048
#   pulse 5: activated nodes: 11341, borderline nodes: 1727, overall activation: 3412.398, activation diff: 6100.225, ratio: 1.788
#   pulse 6: activated nodes: 11427, borderline nodes: 317, overall activation: 8460.144, activation diff: 5538.592, ratio: 0.655
#   pulse 7: activated nodes: 11452, borderline nodes: 62, overall activation: 8938.883, activation diff: 560.857, ratio: 0.063
#   pulse 8: activated nodes: 11454, borderline nodes: 27, overall activation: 9079.860, activation diff: 143.319, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 9079.9
#   number of spread. activ. pulses: 8
#   running time: 1225

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999894   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998989   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.998838   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777436   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   7   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   8   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_514   14   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   15   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   16   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   17   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   19   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   20   0.9   REFERENCES:SIMDATES:SIMLOC
