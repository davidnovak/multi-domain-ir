###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.179, activation diff: 23.179, ratio: 1.349
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1101.880, activation diff: 1116.924, ratio: 1.014
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 396.565, activation diff: 1317.638, ratio: 3.323
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 2888.809, activation diff: 2702.974, ratio: 0.936
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 3142.413, activation diff: 496.026, ratio: 0.158
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 3404.814, activation diff: 263.149, ratio: 0.077
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 3441.023, activation diff: 36.287, ratio: 0.011

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 3441.0
#   number of spread. activ. pulses: 7
#   running time: 1245

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999912   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992436   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9991133   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9983468   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49999994   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4999997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.4999995   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   11   0.49999925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999925   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49999923   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4999986   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49999836   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4999978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   17   0.49999774   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   18   0.49999768   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.4999976   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.49999738   REFERENCES:SIMDATES:SIMLOC
