###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.642, activation diff: 8.642, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 238.003, activation diff: 232.032, ratio: 0.975
#   pulse 3: activated nodes: 8632, borderline nodes: 5390, overall activation: 400.385, activation diff: 166.841, ratio: 0.417
#   pulse 4: activated nodes: 10970, borderline nodes: 7104, overall activation: 2303.465, activation diff: 1903.080, ratio: 0.826
#   pulse 5: activated nodes: 11219, borderline nodes: 2960, overall activation: 4184.053, activation diff: 1880.588, ratio: 0.449
#   pulse 6: activated nodes: 11409, borderline nodes: 944, overall activation: 5986.533, activation diff: 1802.480, ratio: 0.301
#   pulse 7: activated nodes: 11442, borderline nodes: 185, overall activation: 7271.517, activation diff: 1284.984, ratio: 0.177
#   pulse 8: activated nodes: 11452, borderline nodes: 58, overall activation: 8074.875, activation diff: 803.357, ratio: 0.099
#   pulse 9: activated nodes: 11455, borderline nodes: 29, overall activation: 8546.980, activation diff: 472.105, ratio: 0.055
#   pulse 10: activated nodes: 11456, borderline nodes: 21, overall activation: 8817.691, activation diff: 270.711, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 8817.7
#   number of spread. activ. pulses: 10
#   running time: 1379

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996897   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9963315   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99564886   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9956442   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9948182   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99150425   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89545524   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89543015   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8953792   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.895357   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.8953446   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.895296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8951309   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8951226   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.8950835   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.89507747   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   17   0.89505076   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.8950278   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   19   0.8950232   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.89501727   REFERENCES:SIMDATES:SIMLOC
