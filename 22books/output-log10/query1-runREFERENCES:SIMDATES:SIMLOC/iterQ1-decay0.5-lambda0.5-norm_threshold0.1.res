###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.132, activation diff: 10.132, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 216.553, activation diff: 207.466, ratio: 0.958
#   pulse 3: activated nodes: 8832, borderline nodes: 4237, overall activation: 358.954, activation diff: 143.047, ratio: 0.399
#   pulse 4: activated nodes: 10611, borderline nodes: 5479, overall activation: 1139.385, activation diff: 780.431, ratio: 0.685
#   pulse 5: activated nodes: 10845, borderline nodes: 4255, overall activation: 1814.259, activation diff: 674.874, ratio: 0.372
#   pulse 6: activated nodes: 10932, borderline nodes: 3668, overall activation: 2345.779, activation diff: 531.521, ratio: 0.227
#   pulse 7: activated nodes: 10958, borderline nodes: 3548, overall activation: 2727.366, activation diff: 381.587, ratio: 0.140
#   pulse 8: activated nodes: 10962, borderline nodes: 3534, overall activation: 2987.153, activation diff: 259.786, ratio: 0.087
#   pulse 9: activated nodes: 10965, borderline nodes: 3525, overall activation: 3155.210, activation diff: 168.058, ratio: 0.053
#   pulse 10: activated nodes: 10968, borderline nodes: 3522, overall activation: 3259.997, activation diff: 104.786, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 3260.0
#   number of spread. activ. pulses: 10
#   running time: 1488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99790215   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9972287   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9967746   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99655735   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9955373   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9928415   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49794728   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49792215   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49783152   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.4978212   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49780723   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.49768975   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.49767607   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.4976722   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   15   0.4976232   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.49762285   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.4976153   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.4976092   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.49759388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.49758202   REFERENCES:SIMDATES:SIMLOC
