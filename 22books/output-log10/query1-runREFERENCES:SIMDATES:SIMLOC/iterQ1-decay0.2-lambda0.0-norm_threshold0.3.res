###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.255, activation diff: 14.255, ratio: 1.727
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 706.940, activation diff: 715.194, ratio: 1.012
#   pulse 3: activated nodes: 8646, borderline nodes: 5432, overall activation: 319.494, activation diff: 1026.433, ratio: 3.213
#   pulse 4: activated nodes: 11157, borderline nodes: 6944, overall activation: 4759.643, activation diff: 5004.461, ratio: 1.051
#   pulse 5: activated nodes: 11300, borderline nodes: 1981, overall activation: 2074.941, activation diff: 5411.729, ratio: 2.608
#   pulse 6: activated nodes: 11422, borderline nodes: 486, overall activation: 6627.684, activation diff: 5217.050, ratio: 0.787
#   pulse 7: activated nodes: 11448, borderline nodes: 139, overall activation: 7145.930, activation diff: 666.173, ratio: 0.093
#   pulse 8: activated nodes: 11450, borderline nodes: 77, overall activation: 7335.452, activation diff: 193.769, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7335.5
#   number of spread. activ. pulses: 8
#   running time: 1269

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.999989   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998989   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883544   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777424   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   9   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   10   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   12   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   14   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_501   15   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   16   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_505   17   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   19   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.8   REFERENCES:SIMDATES:SIMLOC
