###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.264, activation diff: 20.264, ratio: 1.421
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1371.645, activation diff: 1385.643, ratio: 1.010
#   pulse 3: activated nodes: 9157, borderline nodes: 3896, overall activation: 658.611, activation diff: 2026.843, ratio: 3.077
#   pulse 4: activated nodes: 11332, borderline nodes: 3653, overall activation: 5698.311, activation diff: 5888.078, ratio: 1.033
#   pulse 5: activated nodes: 11430, borderline nodes: 383, overall activation: 5381.747, activation diff: 3035.471, ratio: 0.564
#   pulse 6: activated nodes: 11451, borderline nodes: 64, overall activation: 7383.173, activation diff: 2067.148, ratio: 0.280
#   pulse 7: activated nodes: 11460, borderline nodes: 26, overall activation: 7507.104, activation diff: 132.851, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 7507.1
#   number of spread. activ. pulses: 7
#   running time: 1227

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999912   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999172   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99904305   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817705   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   8   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_514   9   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   10   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   11   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   12   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   13   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_505   15   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   17   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.8   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_510   20   0.8   REFERENCES:SIMDATES:SIMLOC
