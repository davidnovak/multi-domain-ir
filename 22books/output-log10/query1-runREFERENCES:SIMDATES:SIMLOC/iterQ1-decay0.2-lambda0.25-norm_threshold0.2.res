###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.964, activation diff: 12.964, ratio: 1.301
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 545.230, activation diff: 547.339, ratio: 1.004
#   pulse 3: activated nodes: 8727, borderline nodes: 5091, overall activation: 472.067, activation diff: 534.515, ratio: 1.132
#   pulse 4: activated nodes: 11193, borderline nodes: 6411, overall activation: 3596.209, activation diff: 3201.121, ratio: 0.890
#   pulse 5: activated nodes: 11320, borderline nodes: 1712, overall activation: 5325.114, activation diff: 1739.119, ratio: 0.327
#   pulse 6: activated nodes: 11425, borderline nodes: 412, overall activation: 6568.795, activation diff: 1243.683, ratio: 0.189
#   pulse 7: activated nodes: 11449, borderline nodes: 95, overall activation: 7115.786, activation diff: 546.991, ratio: 0.077
#   pulse 8: activated nodes: 11452, borderline nodes: 44, overall activation: 7325.707, activation diff: 209.921, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7325.7
#   number of spread. activ. pulses: 8
#   running time: 1181

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993803   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.999314   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992328   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982698   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99699074   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7994571   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7994563   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7994465   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7994279   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.79942757   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.79942524   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.79942334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7994133   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7994124   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   16   0.79941016   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.7994052   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.79939973   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.7993991   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   20   0.7993956   REFERENCES:SIMDATES:SIMLOC
