###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.264, activation diff: 20.264, ratio: 1.421
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1542.395, activation diff: 1556.394, ratio: 1.009
#   pulse 3: activated nodes: 9157, borderline nodes: 3896, overall activation: 838.496, activation diff: 2376.882, ratio: 2.835
#   pulse 4: activated nodes: 11332, borderline nodes: 3565, overall activation: 7077.452, activation diff: 7252.997, ratio: 1.025
#   pulse 5: activated nodes: 11435, borderline nodes: 327, overall activation: 7154.969, activation diff: 3234.837, ratio: 0.452
#   pulse 6: activated nodes: 11454, borderline nodes: 52, overall activation: 9108.896, activation diff: 2003.503, ratio: 0.220
#   pulse 7: activated nodes: 11460, borderline nodes: 18, overall activation: 9224.414, activation diff: 122.256, ratio: 0.013

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 9224.4
#   number of spread. activ. pulses: 7
#   running time: 2348

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999136   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991727   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990477   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981774   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   7   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   8   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_800   10   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   14   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   15   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   16   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   17   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   18   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_546   19   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.9   REFERENCES:SIMDATES:SIMLOC
