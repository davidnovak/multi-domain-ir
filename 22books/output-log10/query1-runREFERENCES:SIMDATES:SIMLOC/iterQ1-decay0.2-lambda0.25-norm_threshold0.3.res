###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.691, activation diff: 10.691, ratio: 1.390
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 351.069, activation diff: 352.512, ratio: 1.004
#   pulse 3: activated nodes: 8533, borderline nodes: 5695, overall activation: 237.864, activation diff: 376.478, ratio: 1.583
#   pulse 4: activated nodes: 10884, borderline nodes: 7474, overall activation: 3050.455, activation diff: 2910.932, ratio: 0.954
#   pulse 5: activated nodes: 11127, borderline nodes: 3181, overall activation: 4334.909, activation diff: 1397.554, ratio: 0.322
#   pulse 6: activated nodes: 11367, borderline nodes: 1394, overall activation: 6007.230, activation diff: 1672.370, ratio: 0.278
#   pulse 7: activated nodes: 11431, borderline nodes: 279, overall activation: 6829.477, activation diff: 822.248, ratio: 0.120
#   pulse 8: activated nodes: 11444, borderline nodes: 104, overall activation: 7167.292, activation diff: 337.815, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 7167.3
#   number of spread. activ. pulses: 8
#   running time: 1371

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99929464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992178   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99904424   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9989612   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979458   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9960832   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.79935133   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79934734   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.7993473   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7993289   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.79932547   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.799322   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.79931754   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   14   0.79931736   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.79931223   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.7993079   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.79929996   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.7992997   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.7992958   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.7992956   REFERENCES:SIMDATES:SIMLOC
