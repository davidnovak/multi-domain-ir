###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.642, activation diff: 8.642, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 135.577, activation diff: 129.605, ratio: 0.956
#   pulse 3: activated nodes: 8632, borderline nodes: 5390, overall activation: 207.116, activation diff: 74.130, ratio: 0.358
#   pulse 4: activated nodes: 10084, borderline nodes: 6600, overall activation: 898.220, activation diff: 691.104, ratio: 0.769
#   pulse 5: activated nodes: 10535, borderline nodes: 4811, overall activation: 1524.263, activation diff: 626.043, ratio: 0.411
#   pulse 6: activated nodes: 10839, borderline nodes: 4420, overall activation: 2075.468, activation diff: 551.205, ratio: 0.266
#   pulse 7: activated nodes: 10924, borderline nodes: 3765, overall activation: 2493.717, activation diff: 418.249, ratio: 0.168
#   pulse 8: activated nodes: 10946, borderline nodes: 3610, overall activation: 2796.370, activation diff: 302.654, ratio: 0.108
#   pulse 9: activated nodes: 10958, borderline nodes: 3560, overall activation: 3007.386, activation diff: 211.015, ratio: 0.070
#   pulse 10: activated nodes: 10960, borderline nodes: 3543, overall activation: 3147.761, activation diff: 140.376, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10960
#   final overall activation: 3147.8
#   number of spread. activ. pulses: 10
#   running time: 1662

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9968934   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99622965   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9954459   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99522763   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9945712   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9905491   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4974624   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49744508   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49739346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.4973895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.49736708   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.49727145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49723312   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.4972024   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.49718773   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.4971754   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.49716157   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.49715272   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.497113   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.49708807   REFERENCES:SIMDATES:SIMLOC
