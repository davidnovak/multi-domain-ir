###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.964, activation diff: 12.964, ratio: 1.301
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 511.539, activation diff: 513.649, ratio: 1.004
#   pulse 3: activated nodes: 8727, borderline nodes: 5091, overall activation: 428.865, activation diff: 487.229, ratio: 1.136
#   pulse 4: activated nodes: 11190, borderline nodes: 6447, overall activation: 3200.918, activation diff: 2841.723, ratio: 0.888
#   pulse 5: activated nodes: 11315, borderline nodes: 1845, overall activation: 4687.975, activation diff: 1495.564, ratio: 0.319
#   pulse 6: activated nodes: 11415, borderline nodes: 511, overall activation: 5793.741, activation diff: 1105.773, ratio: 0.191
#   pulse 7: activated nodes: 11440, borderline nodes: 163, overall activation: 6298.561, activation diff: 504.819, ratio: 0.080
#   pulse 8: activated nodes: 11447, borderline nodes: 99, overall activation: 6497.310, activation diff: 198.750, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6497.3
#   number of spread. activ. pulses: 8
#   running time: 1256

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99937946   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99931264   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992307   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982637   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9969699   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.749491   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74949014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7494811   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7494627   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.74946237   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7494603   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.74945676   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.74944973   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.74944836   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   16   0.74944466   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.7494404   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.7494317   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.74942935   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.7494293   REFERENCES:SIMDATES:SIMLOC
