###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.255, activation diff: 14.255, ratio: 1.727
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 663.046, activation diff: 671.301, ratio: 1.012
#   pulse 3: activated nodes: 8646, borderline nodes: 5432, overall activation: 280.784, activation diff: 943.830, ratio: 3.361
#   pulse 4: activated nodes: 11146, borderline nodes: 6970, overall activation: 4238.368, activation diff: 4462.009, ratio: 1.053
#   pulse 5: activated nodes: 11284, borderline nodes: 2139, overall activation: 1590.794, activation diff: 4938.597, ratio: 3.104
#   pulse 6: activated nodes: 11411, borderline nodes: 618, overall activation: 5771.540, activation diff: 4892.822, ratio: 0.848
#   pulse 7: activated nodes: 11436, borderline nodes: 230, overall activation: 6269.989, activation diff: 706.914, ratio: 0.113
#   pulse 8: activated nodes: 11440, borderline nodes: 174, overall activation: 6494.524, activation diff: 229.316, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6494.5
#   number of spread. activ. pulses: 8
#   running time: 1251

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998844   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989885   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883264   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777406   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.75   REFERENCES:SIMDATES:SIMLOC
