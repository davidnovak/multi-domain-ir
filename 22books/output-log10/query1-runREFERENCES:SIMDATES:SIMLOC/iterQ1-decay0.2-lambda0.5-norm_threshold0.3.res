###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.127, activation diff: 7.127, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 115.421, activation diff: 111.569, ratio: 0.967
#   pulse 3: activated nodes: 8322, borderline nodes: 6135, overall activation: 166.402, activation diff: 57.729, ratio: 0.347
#   pulse 4: activated nodes: 10248, borderline nodes: 7860, overall activation: 1312.303, activation diff: 1146.024, ratio: 0.873
#   pulse 5: activated nodes: 10705, borderline nodes: 4547, overall activation: 2507.691, activation diff: 1195.388, ratio: 0.477
#   pulse 6: activated nodes: 11302, borderline nodes: 3250, overall activation: 3979.248, activation diff: 1471.557, ratio: 0.370
#   pulse 7: activated nodes: 11407, borderline nodes: 817, overall activation: 5198.347, activation diff: 1219.099, ratio: 0.235
#   pulse 8: activated nodes: 11432, borderline nodes: 324, overall activation: 6057.139, activation diff: 858.793, ratio: 0.142
#   pulse 9: activated nodes: 11440, borderline nodes: 153, overall activation: 6603.383, activation diff: 546.243, ratio: 0.083
#   pulse 10: activated nodes: 11444, borderline nodes: 96, overall activation: 6933.454, activation diff: 330.071, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 6933.5
#   number of spread. activ. pulses: 10
#   running time: 1401

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99567014   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.994949   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99412966   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9935952   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9923005   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98780954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7951591   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.79513717   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.7950895   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.79507923   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7950789   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7950479   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7949538   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79493105   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.79488105   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7948405   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7948321   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.79479647   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   19   0.7947921   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.79478174   REFERENCES:SIMDATES:SIMLOC
