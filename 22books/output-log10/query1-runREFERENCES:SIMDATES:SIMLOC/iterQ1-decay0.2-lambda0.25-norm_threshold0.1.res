###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.198, activation diff: 15.198, ratio: 1.246
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 784.533, activation diff: 785.336, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 833.922, activation diff: 644.723, ratio: 0.773
#   pulse 4: activated nodes: 11313, borderline nodes: 4244, overall activation: 4176.878, activation diff: 3365.339, ratio: 0.806
#   pulse 5: activated nodes: 11414, borderline nodes: 661, overall activation: 6033.034, activation diff: 1856.317, ratio: 0.308
#   pulse 6: activated nodes: 11445, borderline nodes: 99, overall activation: 6967.478, activation diff: 934.445, ratio: 0.134
#   pulse 7: activated nodes: 11455, borderline nodes: 33, overall activation: 7328.876, activation diff: 361.398, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7328.9
#   number of spread. activ. pulses: 7
#   running time: 1206

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99850845   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99830735   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.998023   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9979856   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99705946   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99551445   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7984207   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7983441   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.798337   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.79827726   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.79827434   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.79826397   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7982241   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79819566   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   15   0.79818064   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   16   0.7981699   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7981628   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.79815155   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.79812276   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.7981155   REFERENCES:SIMDATES:SIMLOC
