###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.321, activation diff: 4.321, ratio: 0.590
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 38.009, activation diff: 32.148, ratio: 0.846
#   pulse 3: activated nodes: 8253, borderline nodes: 6245, overall activation: 103.254, activation diff: 65.912, ratio: 0.638
#   pulse 4: activated nodes: 9724, borderline nodes: 6953, overall activation: 408.507, activation diff: 305.260, ratio: 0.747
#   pulse 5: activated nodes: 10701, borderline nodes: 5439, overall activation: 988.598, activation diff: 580.091, ratio: 0.587
#   pulse 6: activated nodes: 11202, borderline nodes: 4135, overall activation: 1865.696, activation diff: 877.097, ratio: 0.470
#   pulse 7: activated nodes: 11331, borderline nodes: 2108, overall activation: 2886.143, activation diff: 1020.448, ratio: 0.354
#   pulse 8: activated nodes: 11407, borderline nodes: 791, overall activation: 3931.088, activation diff: 1044.944, ratio: 0.266
#   pulse 9: activated nodes: 11435, borderline nodes: 315, overall activation: 4897.711, activation diff: 966.623, ratio: 0.197
#   pulse 10: activated nodes: 11443, borderline nodes: 132, overall activation: 5739.568, activation diff: 841.857, ratio: 0.147
#   pulse 11: activated nodes: 11447, borderline nodes: 76, overall activation: 6444.142, activation diff: 704.575, ratio: 0.109
#   pulse 12: activated nodes: 11452, borderline nodes: 46, overall activation: 7019.455, activation diff: 575.313, ratio: 0.082
#   pulse 13: activated nodes: 11453, borderline nodes: 35, overall activation: 7482.177, activation diff: 462.722, ratio: 0.062
#   pulse 14: activated nodes: 11454, borderline nodes: 29, overall activation: 7850.738, activation diff: 368.562, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 7850.7
#   number of spread. activ. pulses: 14
#   running time: 1523

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9830599   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9777904   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9746504   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9741669   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9704675   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9581337   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.86499226   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.86490476   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.8647518   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8644764   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8642182   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.86358315   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8635827   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.86348236   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   15   0.8633981   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.8632678   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8632562   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.8629381   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8628735   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.8627266   REFERENCES:SIMDATES:SIMLOC
