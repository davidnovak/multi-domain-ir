###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.564, activation diff: 3.564, ratio: 0.543
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 12.680, activation diff: 7.981, ratio: 0.629
#   pulse 3: activated nodes: 7794, borderline nodes: 6930, overall activation: 34.677, activation diff: 23.215, ratio: 0.669
#   pulse 4: activated nodes: 8051, borderline nodes: 6489, overall activation: 118.765, activation diff: 84.487, ratio: 0.711
#   pulse 5: activated nodes: 9143, borderline nodes: 6484, overall activation: 329.621, activation diff: 210.911, ratio: 0.640
#   pulse 6: activated nodes: 10483, borderline nodes: 6386, overall activation: 719.095, activation diff: 389.475, ratio: 0.542
#   pulse 7: activated nodes: 11066, borderline nodes: 5108, overall activation: 1275.595, activation diff: 556.500, ratio: 0.436
#   pulse 8: activated nodes: 11223, borderline nodes: 3580, overall activation: 1922.190, activation diff: 646.595, ratio: 0.336
#   pulse 9: activated nodes: 11321, borderline nodes: 2221, overall activation: 2589.862, activation diff: 667.672, ratio: 0.258
#   pulse 10: activated nodes: 11391, borderline nodes: 1162, overall activation: 3235.687, activation diff: 645.825, ratio: 0.200
#   pulse 11: activated nodes: 11411, borderline nodes: 670, overall activation: 3826.446, activation diff: 590.759, ratio: 0.154
#   pulse 12: activated nodes: 11426, borderline nodes: 430, overall activation: 4344.710, activation diff: 518.264, ratio: 0.119
#   pulse 13: activated nodes: 11427, borderline nodes: 271, overall activation: 4785.878, activation diff: 441.168, ratio: 0.092
#   pulse 14: activated nodes: 11436, borderline nodes: 212, overall activation: 5152.721, activation diff: 366.843, ratio: 0.071
#   pulse 15: activated nodes: 11438, borderline nodes: 176, overall activation: 5452.765, activation diff: 300.043, ratio: 0.055

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 5452.8
#   number of spread. activ. pulses: 15
#   running time: 1564

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97954047   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9735037   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9730791   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9707608   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9581293   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94819224   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.7227212   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.72226924   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7222444   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.7221055   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.72207713   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.7218402   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.7216504   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7214121   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   15   0.7212085   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.7211152   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   17   0.7210234   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   18   0.7209922   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   19   0.7208898   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   20   0.72084385   REFERENCES:SIMDATES:SIMLOC
