###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.285, activation diff: 17.285, ratio: 1.532
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 632.588, activation diff: 643.872, ratio: 1.018
#   pulse 3: activated nodes: 8762, borderline nodes: 4759, overall activation: 174.488, activation diff: 807.076, ratio: 4.625
#   pulse 4: activated nodes: 10611, borderline nodes: 6007, overall activation: 2438.451, activation diff: 2568.141, ratio: 1.053
#   pulse 5: activated nodes: 10893, borderline nodes: 4031, overall activation: 683.430, activation diff: 2595.302, ratio: 3.797
#   pulse 6: activated nodes: 10933, borderline nodes: 3634, overall activation: 2999.856, activation diff: 2553.110, ratio: 0.851
#   pulse 7: activated nodes: 10959, borderline nodes: 3560, overall activation: 3172.350, activation diff: 331.047, ratio: 0.104
#   pulse 8: activated nodes: 10960, borderline nodes: 3546, overall activation: 3339.548, activation diff: 168.061, ratio: 0.050
#   pulse 9: activated nodes: 10960, borderline nodes: 3537, overall activation: 3371.294, activation diff: 31.885, ratio: 0.009

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10960
#   final overall activation: 3371.3
#   number of spread. activ. pulses: 9
#   running time: 1270

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998856   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999075   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989087   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9979805   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4999999   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4999996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.49999937   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49999908   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4999983   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49999797   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4999973   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   17   0.4999972   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   18   0.49999714   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.49999678   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.49999675   REFERENCES:SIMDATES:SIMLOC
