###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.127, activation diff: 7.127, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 74.206, activation diff: 70.353, ratio: 0.948
#   pulse 3: activated nodes: 8322, borderline nodes: 6135, overall activation: 102.650, activation diff: 33.129, ratio: 0.323
#   pulse 4: activated nodes: 9291, borderline nodes: 7027, overall activation: 650.043, activation diff: 547.587, ratio: 0.842
#   pulse 5: activated nodes: 9917, borderline nodes: 4937, overall activation: 1186.487, activation diff: 536.444, ratio: 0.452
#   pulse 6: activated nodes: 10717, borderline nodes: 5017, overall activation: 1760.453, activation diff: 573.966, ratio: 0.326
#   pulse 7: activated nodes: 10838, borderline nodes: 4388, overall activation: 2219.764, activation diff: 459.312, ratio: 0.207
#   pulse 8: activated nodes: 10913, borderline nodes: 3865, overall activation: 2565.667, activation diff: 345.902, ratio: 0.135
#   pulse 9: activated nodes: 10937, borderline nodes: 3691, overall activation: 2818.482, activation diff: 252.816, ratio: 0.090
#   pulse 10: activated nodes: 10946, borderline nodes: 3629, overall activation: 2998.538, activation diff: 180.056, ratio: 0.060
#   pulse 11: activated nodes: 10951, borderline nodes: 3595, overall activation: 3121.731, activation diff: 123.193, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10951
#   final overall activation: 3121.7
#   number of spread. activ. pulses: 11
#   running time: 1437

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99781823   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99726266   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99672824   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9959173   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9948522   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9915434   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.49845183   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.4984475   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49842662   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.49841014   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.49840534   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.4983508   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.49832356   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.49832183   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.49830964   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.49829793   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49828035   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.49826494   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.49824798   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.49824247   REFERENCES:SIMDATES:SIMLOC
