###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.691, activation diff: 10.691, ratio: 1.390
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 329.431, activation diff: 330.873, ratio: 1.004
#   pulse 3: activated nodes: 8533, borderline nodes: 5695, overall activation: 215.810, activation diff: 345.700, ratio: 1.602
#   pulse 4: activated nodes: 10862, borderline nodes: 7484, overall activation: 2725.479, activation diff: 2598.258, ratio: 0.953
#   pulse 5: activated nodes: 11100, borderline nodes: 3317, overall activation: 3833.517, activation diff: 1208.922, ratio: 0.315
#   pulse 6: activated nodes: 11353, borderline nodes: 1615, overall activation: 5286.086, activation diff: 1452.642, ratio: 0.275
#   pulse 7: activated nodes: 11423, borderline nodes: 375, overall activation: 6030.019, activation diff: 743.933, ratio: 0.123
#   pulse 8: activated nodes: 11430, borderline nodes: 202, overall activation: 6346.112, activation diff: 316.093, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11430
#   final overall activation: 6346.1
#   number of spread. activ. pulses: 8
#   running time: 1348

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992946   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992132   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9990347   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99890804   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979318   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9960087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.74939156   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.749388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7493879   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7493695   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.7493641   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7493624   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74935955   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   14   0.74935824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.74935293   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.74934673   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.7493403   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   18   0.74933827   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.74933606   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   20   0.749333   REFERENCES:SIMDATES:SIMLOC
