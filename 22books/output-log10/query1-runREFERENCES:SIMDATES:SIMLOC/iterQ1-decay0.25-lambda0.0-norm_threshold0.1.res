###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.264, activation diff: 20.264, ratio: 1.421
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1286.270, activation diff: 1300.268, ratio: 1.011
#   pulse 3: activated nodes: 9157, borderline nodes: 3896, overall activation: 574.792, activation diff: 1857.938, ratio: 3.232
#   pulse 4: activated nodes: 11327, borderline nodes: 3697, overall activation: 5049.731, activation diff: 5237.888, ratio: 1.037
#   pulse 5: activated nodes: 11425, borderline nodes: 420, overall activation: 4542.683, activation diff: 2921.793, ratio: 0.643
#   pulse 6: activated nodes: 11449, borderline nodes: 90, overall activation: 6552.802, activation diff: 2087.161, ratio: 0.319
#   pulse 7: activated nodes: 11456, borderline nodes: 52, overall activation: 6680.050, activation diff: 136.521, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 6680.0
#   number of spread. activ. pulses: 7
#   running time: 1200

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.999991   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991715   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990368   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.75   REFERENCES:SIMDATES:SIMLOC
