###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.564, activation diff: 3.564, ratio: 0.543
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 13.112, activation diff: 8.412, ratio: 0.642
#   pulse 3: activated nodes: 7794, borderline nodes: 6930, overall activation: 36.610, activation diff: 24.709, ratio: 0.675
#   pulse 4: activated nodes: 8051, borderline nodes: 6489, overall activation: 129.686, activation diff: 93.460, ratio: 0.721
#   pulse 5: activated nodes: 9175, borderline nodes: 6477, overall activation: 363.367, activation diff: 233.730, ratio: 0.643
#   pulse 6: activated nodes: 10508, borderline nodes: 6333, overall activation: 803.122, activation diff: 439.755, ratio: 0.548
#   pulse 7: activated nodes: 11086, borderline nodes: 4944, overall activation: 1438.389, activation diff: 635.267, ratio: 0.442
#   pulse 8: activated nodes: 11238, borderline nodes: 3317, overall activation: 2178.363, activation diff: 739.974, ratio: 0.340
#   pulse 9: activated nodes: 11334, borderline nodes: 1950, overall activation: 2947.288, activation diff: 768.925, ratio: 0.261
#   pulse 10: activated nodes: 11401, borderline nodes: 976, overall activation: 3689.886, activation diff: 742.598, ratio: 0.201
#   pulse 11: activated nodes: 11421, borderline nodes: 527, overall activation: 4362.897, activation diff: 673.012, ratio: 0.154
#   pulse 12: activated nodes: 11429, borderline nodes: 318, overall activation: 4947.671, activation diff: 584.774, ratio: 0.118
#   pulse 13: activated nodes: 11439, borderline nodes: 197, overall activation: 5440.404, activation diff: 492.733, ratio: 0.091
#   pulse 14: activated nodes: 11442, borderline nodes: 145, overall activation: 5846.522, activation diff: 406.118, ratio: 0.069
#   pulse 15: activated nodes: 11443, borderline nodes: 105, overall activation: 6176.497, activation diff: 329.975, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 6176.5
#   number of spread. activ. pulses: 15
#   running time: 1588

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9798229   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9739264   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9734501   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97125244   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9591337   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9493009   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.7711077   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.7706847   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.77066356   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.77054846   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.770529   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.7702769   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.7700816   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7698437   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   15   0.76957923   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.76952946   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   17   0.7694571   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   18   0.76941115   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   19   0.7692254   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.7692152   REFERENCES:SIMDATES:SIMLOC
