###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.066, activation diff: 5.066, ratio: 0.628
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 70.338, activation diff: 63.426, ratio: 0.902
#   pulse 3: activated nodes: 8689, borderline nodes: 5079, overall activation: 176.147, activation diff: 106.177, ratio: 0.603
#   pulse 4: activated nodes: 10755, borderline nodes: 6218, overall activation: 571.913, activation diff: 395.766, ratio: 0.692
#   pulse 5: activated nodes: 11184, borderline nodes: 3960, overall activation: 1173.937, activation diff: 602.024, ratio: 0.513
#   pulse 6: activated nodes: 11335, borderline nodes: 2050, overall activation: 1904.282, activation diff: 730.345, ratio: 0.384
#   pulse 7: activated nodes: 11411, borderline nodes: 684, overall activation: 2659.547, activation diff: 755.265, ratio: 0.284
#   pulse 8: activated nodes: 11428, borderline nodes: 322, overall activation: 3371.675, activation diff: 712.128, ratio: 0.211
#   pulse 9: activated nodes: 11439, borderline nodes: 190, overall activation: 4002.328, activation diff: 630.654, ratio: 0.158
#   pulse 10: activated nodes: 11444, borderline nodes: 134, overall activation: 4537.803, activation diff: 535.475, ratio: 0.118
#   pulse 11: activated nodes: 11448, borderline nodes: 104, overall activation: 4979.780, activation diff: 441.977, ratio: 0.089
#   pulse 12: activated nodes: 11449, borderline nodes: 96, overall activation: 5337.989, activation diff: 358.209, ratio: 0.067
#   pulse 13: activated nodes: 11450, borderline nodes: 95, overall activation: 5624.974, activation diff: 286.985, ratio: 0.051
#   pulse 14: activated nodes: 11450, borderline nodes: 93, overall activation: 5853.187, activation diff: 228.213, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 5853.2
#   number of spread. activ. pulses: 14
#   running time: 1540

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9866456   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9825805   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9795429   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9794837   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9778402   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9675524   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7237346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7235511   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.723244   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7229165   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7226424   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7222901   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7221358   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7221081   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7219332   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.7219195   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.72179806   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.7217935   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.72176534   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7216489   REFERENCES:SIMDATES:SIMLOC
