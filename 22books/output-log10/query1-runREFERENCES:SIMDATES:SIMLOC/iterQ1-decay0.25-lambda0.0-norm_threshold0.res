###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.179, activation diff: 23.179, ratio: 1.349
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1649.439, activation diff: 1664.483, ratio: 1.009
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 971.121, activation diff: 2302.383, ratio: 2.371
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5846.909, activation diff: 5372.169, ratio: 0.919
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6650.240, activation diff: 1015.589, ratio: 0.153
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6955.384, activation diff: 306.626, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6955.4
#   number of spread. activ. pulses: 6
#   running time: 1333

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999917   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.999925   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99913645   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835056   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   20   0.75   REFERENCES:SIMDATES:SIMLOC
