###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.066, activation diff: 5.066, ratio: 0.628
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 49.952, activation diff: 43.040, ratio: 0.862
#   pulse 3: activated nodes: 8689, borderline nodes: 5079, overall activation: 118.875, activation diff: 69.291, ratio: 0.583
#   pulse 4: activated nodes: 10101, borderline nodes: 5778, overall activation: 331.561, activation diff: 212.686, ratio: 0.641
#   pulse 5: activated nodes: 10660, borderline nodes: 4909, overall activation: 632.604, activation diff: 301.043, ratio: 0.476
#   pulse 6: activated nodes: 10809, borderline nodes: 4547, overall activation: 976.059, activation diff: 343.455, ratio: 0.352
#   pulse 7: activated nodes: 10870, borderline nodes: 4027, overall activation: 1322.096, activation diff: 346.037, ratio: 0.262
#   pulse 8: activated nodes: 10932, borderline nodes: 3691, overall activation: 1647.595, activation diff: 325.499, ratio: 0.198
#   pulse 9: activated nodes: 10949, borderline nodes: 3584, overall activation: 1940.406, activation diff: 292.811, ratio: 0.151
#   pulse 10: activated nodes: 10958, borderline nodes: 3549, overall activation: 2196.901, activation diff: 256.495, ratio: 0.117
#   pulse 11: activated nodes: 10960, borderline nodes: 3538, overall activation: 2417.474, activation diff: 220.573, ratio: 0.091
#   pulse 12: activated nodes: 10962, borderline nodes: 3535, overall activation: 2604.130, activation diff: 186.656, ratio: 0.072
#   pulse 13: activated nodes: 10962, borderline nodes: 3529, overall activation: 2759.869, activation diff: 155.739, ratio: 0.056
#   pulse 14: activated nodes: 10964, borderline nodes: 3526, overall activation: 2888.299, activation diff: 128.431, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10964
#   final overall activation: 2888.3
#   number of spread. activ. pulses: 14
#   running time: 1376

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98647505   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9819768   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.97867346   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9780208   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97706676   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96494234   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48206592   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.48204544   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.4817257   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.48163623   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.48153406   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.48114246   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   13   0.48096415   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.48088884   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.48086032   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.4808432   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.4808063   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.48080596   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.48079208   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.4806903   REFERENCES:SIMDATES:SIMLOC
