###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.255, activation diff: 14.255, ratio: 1.727
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 443.579, activation diff: 451.834, ratio: 1.019
#   pulse 3: activated nodes: 8646, borderline nodes: 5432, overall activation: 124.875, activation diff: 568.455, ratio: 4.552
#   pulse 4: activated nodes: 10415, borderline nodes: 6800, overall activation: 2279.043, activation diff: 2392.646, ratio: 1.050
#   pulse 5: activated nodes: 10721, borderline nodes: 4594, overall activation: 403.337, activation diff: 2609.846, ratio: 6.471
#   pulse 6: activated nodes: 10870, borderline nodes: 4204, overall activation: 2741.187, activation diff: 2783.732, ratio: 1.016
#   pulse 7: activated nodes: 10940, borderline nodes: 3699, overall activation: 2003.282, activation diff: 1440.067, ratio: 0.719
#   pulse 8: activated nodes: 10943, borderline nodes: 3666, overall activation: 3203.551, activation diff: 1241.675, ratio: 0.388
#   pulse 9: activated nodes: 10951, borderline nodes: 3614, overall activation: 3291.280, activation diff: 96.931, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10951
#   final overall activation: 3291.3
#   number of spread. activ. pulses: 9
#   running time: 1226

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99998945   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.999982   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998969   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.998652   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.997725   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.49999988   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49999943   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.49999928   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4999989   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.49999884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.49999842   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4999979   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.49999744   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.49999675   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.49999675   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.4999965   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.49999598   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.49999577   REFERENCES:SIMDATES:SIMLOC
