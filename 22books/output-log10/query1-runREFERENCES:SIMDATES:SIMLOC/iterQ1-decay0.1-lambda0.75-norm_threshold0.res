###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.795, activation diff: 5.795, ratio: 0.659
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 154.201, activation diff: 146.286, ratio: 0.949
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 390.299, activation diff: 236.267, ratio: 0.605
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1198.321, activation diff: 808.022, ratio: 0.674
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2312.353, activation diff: 1114.032, ratio: 0.482
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3520.262, activation diff: 1207.909, ratio: 0.343
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4645.087, activation diff: 1124.824, ratio: 0.242
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5614.724, activation diff: 969.637, ratio: 0.173
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 6415.342, activation diff: 800.618, ratio: 0.125
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 7060.812, activation diff: 645.470, ratio: 0.091
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 7574.003, activation diff: 513.191, ratio: 0.068
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 7978.539, activation diff: 404.535, ratio: 0.051
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 8295.686, activation diff: 317.147, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8295.7
#   number of spread. activ. pulses: 13
#   running time: 1466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98523754   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9808792   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9792559   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97750115   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9748803   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9661522   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8612827   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8608441   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8601812   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.85968643   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8593366   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.8592699   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.8588749   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8588586   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.8586975   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.85864615   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8584557   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.8583828   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.8583008   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.85815424   REFERENCES:SIMDATES:SIMLOC
