###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.127, activation diff: 7.127, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 129.160, activation diff: 125.307, ratio: 0.970
#   pulse 3: activated nodes: 8322, borderline nodes: 6135, overall activation: 189.402, activation diff: 67.697, ratio: 0.357
#   pulse 4: activated nodes: 10295, borderline nodes: 7873, overall activation: 1600.351, activation diff: 1411.063, ratio: 0.882
#   pulse 5: activated nodes: 10742, borderline nodes: 4334, overall activation: 3095.722, activation diff: 1495.371, ratio: 0.483
#   pulse 6: activated nodes: 11321, borderline nodes: 2877, overall activation: 5000.238, activation diff: 1904.516, ratio: 0.381
#   pulse 7: activated nodes: 11414, borderline nodes: 573, overall activation: 6541.184, activation diff: 1540.946, ratio: 0.236
#   pulse 8: activated nodes: 11442, borderline nodes: 200, overall activation: 7584.579, activation diff: 1043.395, ratio: 0.138
#   pulse 9: activated nodes: 11448, borderline nodes: 72, overall activation: 8226.544, activation diff: 641.965, ratio: 0.078
#   pulse 10: activated nodes: 11453, borderline nodes: 37, overall activation: 8605.206, activation diff: 378.662, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 8605.2
#   number of spread. activ. pulses: 10
#   running time: 1282

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99567425   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9950173   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99423075   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99366754   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9927486   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98819995   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.894566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.894542   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.89449495   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.8944894   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.89448935   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.89447904   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8943568   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.89432824   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.8943051   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   16   0.89426947   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8942214   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.8942156   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   19   0.8942015   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.89419436   REFERENCES:SIMDATES:SIMLOC
