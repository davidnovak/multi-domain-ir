###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.564, activation diff: 3.564, ratio: 0.543
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 10.522, activation diff: 5.822, ratio: 0.553
#   pulse 3: activated nodes: 7794, borderline nodes: 6930, overall activation: 24.990, activation diff: 15.732, ratio: 0.630
#   pulse 4: activated nodes: 8051, borderline nodes: 6489, overall activation: 69.242, activation diff: 44.768, ratio: 0.647
#   pulse 5: activated nodes: 8933, borderline nodes: 6493, overall activation: 180.780, activation diff: 111.664, ratio: 0.618
#   pulse 6: activated nodes: 9550, borderline nodes: 6027, overall activation: 375.737, activation diff: 194.963, ratio: 0.519
#   pulse 7: activated nodes: 10171, borderline nodes: 5486, overall activation: 638.049, activation diff: 262.312, ratio: 0.411
#   pulse 8: activated nodes: 10606, borderline nodes: 5147, overall activation: 934.541, activation diff: 296.492, ratio: 0.317
#   pulse 9: activated nodes: 10738, borderline nodes: 4803, overall activation: 1234.916, activation diff: 300.375, ratio: 0.243
#   pulse 10: activated nodes: 10813, borderline nodes: 4525, overall activation: 1520.647, activation diff: 285.730, ratio: 0.188
#   pulse 11: activated nodes: 10846, borderline nodes: 4261, overall activation: 1783.424, activation diff: 262.778, ratio: 0.147
#   pulse 12: activated nodes: 10879, borderline nodes: 3957, overall activation: 2018.711, activation diff: 235.287, ratio: 0.117
#   pulse 13: activated nodes: 10921, borderline nodes: 3794, overall activation: 2225.721, activation diff: 207.010, ratio: 0.093
#   pulse 14: activated nodes: 10930, borderline nodes: 3718, overall activation: 2406.234, activation diff: 180.512, ratio: 0.075
#   pulse 15: activated nodes: 10941, borderline nodes: 3676, overall activation: 2562.480, activation diff: 156.247, ratio: 0.061

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10941
#   final overall activation: 2562.5
#   number of spread. activ. pulses: 15
#   running time: 1433

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9774778   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9703423   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.97029024   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.96709013   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9500699   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9393832   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.48089716   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.48032874   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.4802862   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.4800431   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.47989744   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.4798215   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.47963166   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   14   0.47954798   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   15   0.479438   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.47935393   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_148   17   0.47922269   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   18   0.4791757   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_242   19   0.4790675   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   20   0.4790331   REFERENCES:SIMDATES:SIMLOC
