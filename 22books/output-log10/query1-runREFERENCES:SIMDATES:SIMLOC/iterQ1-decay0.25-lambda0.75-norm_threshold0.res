###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.795, activation diff: 5.795, ratio: 0.659
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 130.272, activation diff: 122.357, ratio: 0.939
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 319.028, activation diff: 188.925, ratio: 0.592
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 891.068, activation diff: 572.040, ratio: 0.642
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1667.446, activation diff: 776.378, ratio: 0.466
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 2515.432, activation diff: 847.985, ratio: 0.337
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 3324.805, activation diff: 809.373, ratio: 0.243
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 4039.001, activation diff: 714.197, ratio: 0.177
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 4640.891, activation diff: 601.889, ratio: 0.130
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 5134.038, activation diff: 493.147, ratio: 0.096
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 5531.238, activation diff: 397.200, ratio: 0.072
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 5847.766, activation diff: 316.528, ratio: 0.054
#   pulse 13: activated nodes: 11464, borderline nodes: 21, overall activation: 6098.258, activation diff: 250.492, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6098.3
#   number of spread. activ. pulses: 13
#   running time: 1374

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98521566   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9807372   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9789659   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9772552   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97465366   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9654951   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7176208   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7172517   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.71664125   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7163286   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7159989   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.71592414   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.71557295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7154452   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.7152806   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7152789   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.71510166   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.71506214   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.7149914   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7148874   REFERENCES:SIMDATES:SIMLOC
