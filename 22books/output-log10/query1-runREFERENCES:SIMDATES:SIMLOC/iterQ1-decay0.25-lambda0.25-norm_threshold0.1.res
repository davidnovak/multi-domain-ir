###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.198, activation diff: 15.198, ratio: 1.246
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 736.020, activation diff: 736.822, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 760.018, activation diff: 582.362, ratio: 0.766
#   pulse 4: activated nodes: 11307, borderline nodes: 4283, overall activation: 3705.339, activation diff: 2966.061, ratio: 0.800
#   pulse 5: activated nodes: 11410, borderline nodes: 719, overall activation: 5329.930, activation diff: 1624.681, ratio: 0.305
#   pulse 6: activated nodes: 11442, borderline nodes: 148, overall activation: 6169.427, activation diff: 839.497, ratio: 0.136
#   pulse 7: activated nodes: 11454, borderline nodes: 60, overall activation: 6501.776, activation diff: 332.349, ratio: 0.051
#   pulse 8: activated nodes: 11457, borderline nodes: 53, overall activation: 6625.688, activation diff: 123.911, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6625.7
#   number of spread. activ. pulses: 8
#   running time: 1276

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9996271   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9995696   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9994991   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994324   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99853826   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9974858   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74962986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7496116   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7495962   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7495818   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7495767   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7495761   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   13   0.7495743   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.74956393   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7495536   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.7495501   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.74955004   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.7495416   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.74954087   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.74953973   REFERENCES:SIMDATES:SIMLOC
