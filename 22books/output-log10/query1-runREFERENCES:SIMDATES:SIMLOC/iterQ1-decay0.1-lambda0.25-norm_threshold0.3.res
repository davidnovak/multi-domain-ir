###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.691, activation diff: 10.691, ratio: 1.390
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 394.347, activation diff: 395.790, ratio: 1.004
#   pulse 3: activated nodes: 8533, borderline nodes: 5695, overall activation: 285.208, activation diff: 441.272, ratio: 1.547
#   pulse 4: activated nodes: 10915, borderline nodes: 7454, overall activation: 3762.231, activation diff: 3595.063, ratio: 0.956
#   pulse 5: activated nodes: 11169, borderline nodes: 2918, overall activation: 5486.779, activation diff: 1844.094, ratio: 0.336
#   pulse 6: activated nodes: 11414, borderline nodes: 971, overall activation: 7566.160, activation diff: 2079.396, ratio: 0.275
#   pulse 7: activated nodes: 11444, borderline nodes: 173, overall activation: 8518.647, activation diff: 952.487, ratio: 0.112
#   pulse 8: activated nodes: 11452, borderline nodes: 52, overall activation: 8889.439, activation diff: 370.791, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 8889.4
#   number of spread. activ. pulses: 8
#   running time: 1223

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99929464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992244   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9990587   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99903584   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979668   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9962026   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.89927065   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89926577   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.89926565   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.8992466   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.89924574   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.89924026   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   13   0.89923555   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89923286   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.8992287   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   16   0.8992281   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.8992274   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   18   0.8992207   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   19   0.8992205   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.8992189   REFERENCES:SIMDATES:SIMLOC
