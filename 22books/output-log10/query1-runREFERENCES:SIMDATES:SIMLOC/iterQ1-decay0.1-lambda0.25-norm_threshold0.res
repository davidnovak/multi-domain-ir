###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.384, activation diff: 17.384, ratio: 1.209
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1192.137, activation diff: 1189.162, ratio: 0.998
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1716.027, activation diff: 825.376, ratio: 0.481
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 6103.450, activation diff: 4387.422, ratio: 0.719
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 8185.185, activation diff: 2081.735, ratio: 0.254
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 8985.710, activation diff: 800.525, ratio: 0.089
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 9262.095, activation diff: 276.385, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 9262.1
#   number of spread. activ. pulses: 7
#   running time: 1240

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99926615   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9989538   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99876374   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9986383   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9976931   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964429   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8989473   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8987868   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8986949   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.898663   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.898657   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.8986521   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.8985909   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8985648   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.89856017   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.8985405   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   17   0.8985277   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.89851296   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.89851004   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.8984802   REFERENCES:SIMDATES:SIMLOC
