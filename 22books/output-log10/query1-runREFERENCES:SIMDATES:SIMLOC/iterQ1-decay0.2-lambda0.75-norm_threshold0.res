###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.795, activation diff: 5.795, ratio: 0.659
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 138.249, activation diff: 130.334, ratio: 0.943
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 342.439, activation diff: 204.360, ratio: 0.597
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 987.946, activation diff: 645.508, ratio: 0.653
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1870.187, activation diff: 882.240, ratio: 0.472
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 2834.155, activation diff: 963.968, ratio: 0.340
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 3747.024, activation diff: 912.869, ratio: 0.244
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 4546.240, activation diff: 799.216, ratio: 0.176
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5214.956, activation diff: 668.716, ratio: 0.128
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 5759.689, activation diff: 544.733, ratio: 0.095
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 6196.438, activation diff: 436.749, ratio: 0.070
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 6543.165, activation diff: 346.727, ratio: 0.053
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 6816.655, activation diff: 273.490, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6816.7
#   number of spread. activ. pulses: 13
#   running time: 1478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9852239   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9807898   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9790745   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9773458   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97473806   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9657396   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76551145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7651154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7644938   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.76411384   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7637757   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7637052   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7633383   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.76324904   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.76308525   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.7630667   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7628877   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.762831   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.76271063   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.7626623   REFERENCES:SIMDATES:SIMLOC
