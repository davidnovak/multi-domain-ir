###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.964, activation diff: 12.964, ratio: 1.301
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 612.610, activation diff: 614.720, ratio: 1.003
#   pulse 3: activated nodes: 8727, borderline nodes: 5091, overall activation: 563.583, activation diff: 634.199, ratio: 1.125
#   pulse 4: activated nodes: 11211, borderline nodes: 6338, overall activation: 4458.060, activation diff: 3985.307, ratio: 0.894
#   pulse 5: activated nodes: 11364, borderline nodes: 1488, overall activation: 6704.700, activation diff: 2259.206, ratio: 0.337
#   pulse 6: activated nodes: 11436, borderline nodes: 264, overall activation: 8207.451, activation diff: 1502.750, ratio: 0.183
#   pulse 7: activated nodes: 11452, borderline nodes: 50, overall activation: 8822.915, activation diff: 615.464, ratio: 0.070
#   pulse 8: activated nodes: 11456, borderline nodes: 24, overall activation: 9049.218, activation diff: 226.303, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 9049.2
#   number of spread. activ. pulses: 8
#   running time: 1245

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994464   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99938154   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9993159   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99923605   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982787   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99702555   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8993891   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89938843   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8993772   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.89935917   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89935684   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.89935476   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.8993546   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.89934176   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8993402   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.89933956   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   17   0.89933443   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   18   0.8993338   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   19   0.8993335   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.8993335   REFERENCES:SIMDATES:SIMLOC
