###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.321, activation diff: 4.321, ratio: 0.590
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 32.956, activation diff: 27.095, ratio: 0.822
#   pulse 3: activated nodes: 8253, borderline nodes: 6245, overall activation: 87.231, activation diff: 54.942, ratio: 0.630
#   pulse 4: activated nodes: 9554, borderline nodes: 6806, overall activation: 317.901, activation diff: 230.687, ratio: 0.726
#   pulse 5: activated nodes: 10568, borderline nodes: 5654, overall activation: 738.511, activation diff: 420.610, ratio: 0.570
#   pulse 6: activated nodes: 11153, borderline nodes: 4571, overall activation: 1345.082, activation diff: 606.571, ratio: 0.451
#   pulse 7: activated nodes: 11290, borderline nodes: 2718, overall activation: 2038.580, activation diff: 693.498, ratio: 0.340
#   pulse 8: activated nodes: 11365, borderline nodes: 1290, overall activation: 2745.859, activation diff: 707.279, ratio: 0.258
#   pulse 9: activated nodes: 11413, borderline nodes: 614, overall activation: 3414.570, activation diff: 668.711, ratio: 0.196
#   pulse 10: activated nodes: 11426, borderline nodes: 350, overall activation: 4012.548, activation diff: 597.977, ratio: 0.149
#   pulse 11: activated nodes: 11437, borderline nodes: 220, overall activation: 4526.434, activation diff: 513.887, ratio: 0.114
#   pulse 12: activated nodes: 11440, borderline nodes: 162, overall activation: 4955.450, activation diff: 429.015, ratio: 0.087
#   pulse 13: activated nodes: 11443, borderline nodes: 139, overall activation: 5306.540, activation diff: 351.090, ratio: 0.066
#   pulse 14: activated nodes: 11444, borderline nodes: 116, overall activation: 5590.150, activation diff: 283.610, ratio: 0.051
#   pulse 15: activated nodes: 11446, borderline nodes: 105, overall activation: 5817.340, activation diff: 227.190, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 5817.3
#   number of spread. activ. pulses: 15
#   running time: 1472

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98712254   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98295355   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9804769   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9799034   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9769212   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.966581   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7277959   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72769654   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.72760105   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7275609   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7273939   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.72689927   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_153   13   0.7268865   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7268345   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7267889   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.7266837   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.72660184   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.72647166   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.72639513   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7263821   REFERENCES:SIMDATES:SIMLOC
