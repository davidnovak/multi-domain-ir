###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.964, activation diff: 12.964, ratio: 1.301
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 343.088, activation diff: 345.198, ratio: 1.006
#   pulse 3: activated nodes: 8727, borderline nodes: 5091, overall activation: 240.667, activation diff: 278.611, ratio: 1.158
#   pulse 4: activated nodes: 10521, borderline nodes: 6430, overall activation: 1684.329, activation diff: 1474.248, ratio: 0.875
#   pulse 5: activated nodes: 10765, borderline nodes: 4509, overall activation: 2310.323, activation diff: 630.380, ratio: 0.273
#   pulse 6: activated nodes: 10881, borderline nodes: 3913, overall activation: 2816.085, activation diff: 505.762, ratio: 0.180
#   pulse 7: activated nodes: 10949, borderline nodes: 3580, overall activation: 3104.593, activation diff: 288.509, ratio: 0.093
#   pulse 8: activated nodes: 10960, borderline nodes: 3543, overall activation: 3256.602, activation diff: 152.009, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10960
#   final overall activation: 3256.6
#   number of spread. activ. pulses: 8
#   running time: 1448

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99944633   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99937105   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99929154   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.999207   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99818224   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99678814   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4996606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.4996591   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.4996488   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49963662   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.49963236   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49963126   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.49962366   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.49962017   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.49961358   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49961135   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.4996092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.49960542   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.4996006   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.4995998   REFERENCES:SIMDATES:SIMLOC
