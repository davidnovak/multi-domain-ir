###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.777, activation diff: 20.777, ratio: 1.406
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1061.285, activation diff: 1076.063, ratio: 1.014
#   pulse 3: activated nodes: 8173, borderline nodes: 4071, overall activation: 717.407, activation diff: 1778.692, ratio: 2.479
#   pulse 4: activated nodes: 10289, borderline nodes: 3637, overall activation: 4591.582, activation diff: 5308.989, ratio: 1.156
#   pulse 5: activated nodes: 11227, borderline nodes: 1348, overall activation: 1908.762, activation diff: 6500.344, ratio: 3.406
#   pulse 6: activated nodes: 11309, borderline nodes: 135, overall activation: 4762.592, activation diff: 6671.354, ratio: 1.401
#   pulse 7: activated nodes: 11328, borderline nodes: 97, overall activation: 1941.811, activation diff: 6704.403, ratio: 3.453
#   pulse 8: activated nodes: 11330, borderline nodes: 69, overall activation: 4767.503, activation diff: 6709.314, ratio: 1.407
#   pulse 9: activated nodes: 11330, borderline nodes: 69, overall activation: 1943.303, activation diff: 6710.806, ratio: 3.453
#   pulse 10: activated nodes: 11330, borderline nodes: 69, overall activation: 4767.905, activation diff: 6711.208, ratio: 1.408
#   pulse 11: activated nodes: 11330, borderline nodes: 69, overall activation: 1943.452, activation diff: 6711.357, ratio: 3.453
#   pulse 12: activated nodes: 11330, borderline nodes: 69, overall activation: 4767.962, activation diff: 6711.414, ratio: 1.408
#   pulse 13: activated nodes: 11330, borderline nodes: 69, overall activation: 1943.474, activation diff: 6711.436, ratio: 3.453
#   pulse 14: activated nodes: 11330, borderline nodes: 69, overall activation: 4767.973, activation diff: 6711.447, ratio: 1.408
#   pulse 15: activated nodes: 11330, borderline nodes: 69, overall activation: 1943.478, activation diff: 6711.451, ratio: 3.453

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 1943.5
#   number of spread. activ. pulses: 15
#   running time: 624

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
