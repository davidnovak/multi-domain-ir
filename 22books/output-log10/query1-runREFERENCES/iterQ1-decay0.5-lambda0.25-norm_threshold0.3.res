###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.377, activation diff: 13.377, ratio: 1.289
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 239.501, activation diff: 242.039, ratio: 1.011
#   pulse 3: activated nodes: 7560, borderline nodes: 4598, overall activation: 109.004, activation diff: 199.989, ratio: 1.835
#   pulse 4: activated nodes: 7560, borderline nodes: 4598, overall activation: 1265.061, activation diff: 1176.710, ratio: 0.930
#   pulse 5: activated nodes: 8634, borderline nodes: 3435, overall activation: 1582.600, activation diff: 363.538, ratio: 0.230
#   pulse 6: activated nodes: 8634, borderline nodes: 3435, overall activation: 1874.256, activation diff: 291.656, ratio: 0.156
#   pulse 7: activated nodes: 8660, borderline nodes: 3406, overall activation: 1987.789, activation diff: 113.534, ratio: 0.057
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2025.934, activation diff: 38.144, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2025.9
#   number of spread. activ. pulses: 8
#   running time: 394

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999382   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993065   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991007   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9985175   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.997971   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9957195   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49959388   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.4995796   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49955684   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49954638   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.4995441   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.4995325   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.49946073   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4994539   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.49944177   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.49939755   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.4993402   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   18   0.4993378   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.499331   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.4993126   REFERENCES
