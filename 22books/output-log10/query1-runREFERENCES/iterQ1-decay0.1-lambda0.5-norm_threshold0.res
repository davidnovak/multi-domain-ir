###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.229, activation diff: 13.229, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 576.878, activation diff: 563.648, ratio: 0.977
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1276.366, activation diff: 699.488, ratio: 0.548
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3656.940, activation diff: 2380.575, ratio: 0.651
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5754.797, activation diff: 2097.857, ratio: 0.365
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 7121.584, activation diff: 1366.786, ratio: 0.192
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 7927.376, activation diff: 805.792, ratio: 0.102
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 8383.850, activation diff: 456.474, ratio: 0.054
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 8637.958, activation diff: 254.108, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 8638.0
#   number of spread. activ. pulses: 9
#   running time: 492

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972838   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9963722   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99548936   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9953548   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9940558   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9913086   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89410543   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89401996   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.89366347   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8935783   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.8935148   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.8933475   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.8933233   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   14   0.89328754   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.8932827   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.8931689   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   17   0.89315605   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8931041   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.89295316   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.89292663   REFERENCES
