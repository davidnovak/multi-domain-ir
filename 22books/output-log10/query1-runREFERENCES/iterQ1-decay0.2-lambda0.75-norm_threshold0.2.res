###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.194, activation diff: 5.194, ratio: 0.634
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 38.611, activation diff: 31.699, ratio: 0.821
#   pulse 3: activated nodes: 6984, borderline nodes: 4853, overall activation: 101.321, activation diff: 63.196, ratio: 0.624
#   pulse 4: activated nodes: 8513, borderline nodes: 5629, overall activation: 368.123, activation diff: 266.803, ratio: 0.725
#   pulse 5: activated nodes: 10024, borderline nodes: 4828, overall activation: 850.667, activation diff: 482.543, ratio: 0.567
#   pulse 6: activated nodes: 10778, borderline nodes: 3295, overall activation: 1548.976, activation diff: 698.309, ratio: 0.451
#   pulse 7: activated nodes: 11144, borderline nodes: 1572, overall activation: 2335.421, activation diff: 786.445, ratio: 0.337
#   pulse 8: activated nodes: 11229, borderline nodes: 774, overall activation: 3098.691, activation diff: 763.271, ratio: 0.246
#   pulse 9: activated nodes: 11253, borderline nodes: 425, overall activation: 3780.658, activation diff: 681.967, ratio: 0.180
#   pulse 10: activated nodes: 11300, borderline nodes: 267, overall activation: 4362.023, activation diff: 581.365, ratio: 0.133
#   pulse 11: activated nodes: 11317, borderline nodes: 156, overall activation: 4843.989, activation diff: 481.965, ratio: 0.099
#   pulse 12: activated nodes: 11320, borderline nodes: 111, overall activation: 5236.709, activation diff: 392.721, ratio: 0.075
#   pulse 13: activated nodes: 11326, borderline nodes: 65, overall activation: 5553.085, activation diff: 316.376, ratio: 0.057
#   pulse 14: activated nodes: 11328, borderline nodes: 61, overall activation: 5805.931, activation diff: 252.846, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5805.9
#   number of spread. activ. pulses: 14
#   running time: 554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.985335   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98124903   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97845304   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9761094   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.97208756   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.962144   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.76963854   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.76916337   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7690335   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.7687943   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.76868385   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.76791453   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.76779366   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.767713   REFERENCES
1   Q1   politicalsketche00retsrich_153   15   0.76764333   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.76763576   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.76752615   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7674885   REFERENCES
1   Q1   politicalsketche00retsrich_156   19   0.76731175   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   20   0.767218   REFERENCES
