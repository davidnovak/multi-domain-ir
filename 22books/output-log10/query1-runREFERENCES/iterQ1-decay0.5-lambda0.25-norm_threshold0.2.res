###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.583, activation diff: 15.583, ratio: 1.238
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 363.368, activation diff: 366.350, ratio: 1.008
#   pulse 3: activated nodes: 8057, borderline nodes: 4312, overall activation: 201.194, activation diff: 241.812, ratio: 1.202
#   pulse 4: activated nodes: 8057, borderline nodes: 4312, overall activation: 1402.034, activation diff: 1209.377, ratio: 0.863
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 1802.503, activation diff: 408.242, ratio: 0.226
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1993.119, activation diff: 190.615, ratio: 0.096
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2059.659, activation diff: 66.540, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2059.7
#   number of spread. activ. pulses: 7
#   running time: 381

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99810183   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99791646   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9974807   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99728906   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99644613   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99393845   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49869007   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.49865696   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4986201   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.49860916   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49851304   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   12   0.49850044   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.49849936   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.498495   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.4984517   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.49841946   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.49837977   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.49837738   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49836528   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.49834594   REFERENCES
