###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 20.459, activation diff: 26.459, ratio: 1.293
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1111.519, activation diff: 1131.978, ratio: 1.018
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.405, activation diff: 1173.924, ratio: 18.811
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.673, activation diff: 2175.079, ratio: 1.030
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.756, activation diff: 2175.429, ratio: 34.665
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2112.996, activation diff: 2175.752, ratio: 1.030
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.760, activation diff: 2175.756, ratio: 34.668
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2113.000, activation diff: 2175.760, ratio: 1.030
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.760, activation diff: 2175.760, ratio: 34.668
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 2113.000, activation diff: 2175.760, ratio: 1.030
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.760, activation diff: 2175.760, ratio: 34.668
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 2113.000, activation diff: 2175.760, ratio: 1.030
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.760, activation diff: 2175.760, ratio: 34.668
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 2113.000, activation diff: 2175.760, ratio: 1.030
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.760, activation diff: 2175.760, ratio: 34.668

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 62.8
#   number of spread. activ. pulses: 15
#   running time: 604

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
