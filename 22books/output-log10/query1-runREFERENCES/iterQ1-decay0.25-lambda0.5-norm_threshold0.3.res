###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.918, activation diff: 8.918, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 120.287, activation diff: 115.008, ratio: 0.956
#   pulse 3: activated nodes: 7117, borderline nodes: 4806, overall activation: 173.381, activation diff: 59.861, ratio: 0.345
#   pulse 4: activated nodes: 8655, borderline nodes: 6024, overall activation: 1184.154, activation diff: 1010.816, ratio: 0.854
#   pulse 5: activated nodes: 10278, borderline nodes: 4101, overall activation: 2292.463, activation diff: 1108.308, ratio: 0.483
#   pulse 6: activated nodes: 10926, borderline nodes: 2023, overall activation: 3469.800, activation diff: 1177.337, ratio: 0.339
#   pulse 7: activated nodes: 11220, borderline nodes: 838, overall activation: 4336.722, activation diff: 866.923, ratio: 0.200
#   pulse 8: activated nodes: 11238, borderline nodes: 609, overall activation: 4890.587, activation diff: 553.865, ratio: 0.113
#   pulse 9: activated nodes: 11273, borderline nodes: 608, overall activation: 5223.166, activation diff: 332.579, ratio: 0.064
#   pulse 10: activated nodes: 11280, borderline nodes: 605, overall activation: 5417.284, activation diff: 194.118, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11280
#   final overall activation: 5417.3
#   number of spread. activ. pulses: 10
#   running time: 470

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99652404   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9960735   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99529445   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9942067   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.99322045   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98940873   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.74569845   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74559236   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.74555993   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.74554193   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.745464   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.74538165   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.7453555   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7453005   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.74526846   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.7452637   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.745257   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.7452202   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   19   0.7451767   REFERENCES
1   Q1   europesincenapol00leveuoft_35   20   0.74516636   REFERENCES
