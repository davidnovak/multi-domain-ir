###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.583, activation diff: 15.583, ratio: 1.238
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 541.377, activation diff: 544.359, ratio: 1.006
#   pulse 3: activated nodes: 8057, borderline nodes: 4312, overall activation: 503.194, activation diff: 566.184, ratio: 1.125
#   pulse 4: activated nodes: 10124, borderline nodes: 4329, overall activation: 2988.365, activation diff: 2668.913, ratio: 0.893
#   pulse 5: activated nodes: 11161, borderline nodes: 1930, overall activation: 4493.034, activation diff: 1522.459, ratio: 0.339
#   pulse 6: activated nodes: 11228, borderline nodes: 740, overall activation: 5271.195, activation diff: 778.252, ratio: 0.148
#   pulse 7: activated nodes: 11264, borderline nodes: 569, overall activation: 5569.483, activation diff: 298.288, ratio: 0.054
#   pulse 8: activated nodes: 11264, borderline nodes: 568, overall activation: 5672.714, activation diff: 103.231, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11264
#   final overall activation: 5672.7
#   number of spread. activ. pulses: 8
#   running time: 471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999474   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9993309   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99930817   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9983326   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99705863   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.74951357   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7495039   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.74949354   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.74947333   REFERENCES
1   Q1   politicalsketche00retsrich_96   11   0.749463   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   12   0.74946284   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.74946153   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.7494603   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.74944997   REFERENCES
1   Q1   politicalsketche00retsrich_154   16   0.74944395   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.74944246   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.74944186   REFERENCES
1   Q1   essentialsinmod01howegoog_283   19   0.74943495   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   20   0.74943376   REFERENCES
