###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.656, activation diff: 23.656, ratio: 1.340
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1596.165, activation diff: 1613.821, ratio: 1.011
#   pulse 3: activated nodes: 8627, borderline nodes: 3418, overall activation: 1374.708, activation diff: 2970.873, ratio: 2.161
#   pulse 4: activated nodes: 10760, borderline nodes: 2410, overall activation: 5859.804, activation diff: 7234.512, ratio: 1.235
#   pulse 5: activated nodes: 11262, borderline nodes: 740, overall activation: 2833.750, activation diff: 8693.554, ratio: 3.068
#   pulse 6: activated nodes: 11329, borderline nodes: 76, overall activation: 6029.746, activation diff: 8863.496, ratio: 1.470
#   pulse 7: activated nodes: 11339, borderline nodes: 12, overall activation: 2871.730, activation diff: 8901.476, ratio: 3.100
#   pulse 8: activated nodes: 11341, borderline nodes: 2, overall activation: 6038.058, activation diff: 8909.789, ratio: 1.476
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 2876.376, activation diff: 8914.434, ratio: 3.099
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 6039.311, activation diff: 8915.687, ratio: 1.476
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 2877.208, activation diff: 8916.519, ratio: 3.099
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 6039.573, activation diff: 8916.781, ratio: 1.476
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 2877.346, activation diff: 8916.919, ratio: 3.099
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 6039.634, activation diff: 8916.980, ratio: 1.476
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 2877.373, activation diff: 8917.007, ratio: 3.099

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 2877.4
#   number of spread. activ. pulses: 15
#   running time: 1681

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
