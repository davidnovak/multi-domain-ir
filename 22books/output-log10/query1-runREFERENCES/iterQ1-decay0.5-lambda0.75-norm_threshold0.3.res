###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.459, activation diff: 4.459, ratio: 0.598
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 12.776, activation diff: 6.956, ratio: 0.544
#   pulse 3: activated nodes: 6159, borderline nodes: 5206, overall activation: 29.522, activation diff: 17.629, ratio: 0.597
#   pulse 4: activated nodes: 6688, borderline nodes: 5015, overall activation: 83.091, activation diff: 53.831, ratio: 0.648
#   pulse 5: activated nodes: 7318, borderline nodes: 4694, overall activation: 205.061, activation diff: 122.009, ratio: 0.595
#   pulse 6: activated nodes: 7840, borderline nodes: 4217, overall activation: 392.146, activation diff: 187.084, ratio: 0.477
#   pulse 7: activated nodes: 8260, borderline nodes: 3793, overall activation: 613.407, activation diff: 221.262, ratio: 0.361
#   pulse 8: activated nodes: 8484, borderline nodes: 3553, overall activation: 837.823, activation diff: 224.416, ratio: 0.268
#   pulse 9: activated nodes: 8599, borderline nodes: 3455, overall activation: 1046.822, activation diff: 208.998, ratio: 0.200
#   pulse 10: activated nodes: 8645, borderline nodes: 3417, overall activation: 1231.130, activation diff: 184.308, ratio: 0.150
#   pulse 11: activated nodes: 8656, borderline nodes: 3404, overall activation: 1388.003, activation diff: 156.873, ratio: 0.113
#   pulse 12: activated nodes: 8660, borderline nodes: 3406, overall activation: 1518.489, activation diff: 130.486, ratio: 0.086
#   pulse 13: activated nodes: 8660, borderline nodes: 3406, overall activation: 1625.367, activation diff: 106.878, ratio: 0.066
#   pulse 14: activated nodes: 8660, borderline nodes: 3406, overall activation: 1711.963, activation diff: 86.597, ratio: 0.051
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 1781.569, activation diff: 69.605, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1781.6
#   number of spread. activ. pulses: 15
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98270315   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97783166   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9747634   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.97449404   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9564095   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9494643   REFERENCES
1   Q1   politicalsketche00retsrich_153   7   0.48156342   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.48142433   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.48117954   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.48106968   REFERENCES
1   Q1   essentialsinmod01howegoog_293   11   0.48036996   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.48035818   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   13   0.4803404   REFERENCES
1   Q1   shorthistoryofmo00haslrich_242   14   0.48019707   REFERENCES
1   Q1   essentialsinmod01howegoog_37   15   0.48017848   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.48010266   REFERENCES
1   Q1   politicalsketche00retsrich_156   17   0.4800831   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   18   0.48002845   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.47977152   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.4796593   REFERENCES
