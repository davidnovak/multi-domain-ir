###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.844, activation diff: 19.844, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 676.885, activation diff: 674.874, ratio: 0.997
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 594.406, activation diff: 162.408, ratio: 0.273
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1631.793, activation diff: 1037.387, ratio: 0.636
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 2006.353, activation diff: 374.560, ratio: 0.187
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2125.314, activation diff: 118.961, ratio: 0.056
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2161.122, activation diff: 35.808, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2161.1
#   number of spread. activ. pulses: 7
#   running time: 381

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993978   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99907184   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987413   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.998731   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977919   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964337   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49933237   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.4993164   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4992351   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.49923027   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.49920037   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.49919462   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.49913868   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49910218   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.49907288   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.49905875   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   17   0.4989971   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.4989936   REFERENCES
1   Q1   politicalsketche00retsrich_153   19   0.49898207   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.49895784   REFERENCES
