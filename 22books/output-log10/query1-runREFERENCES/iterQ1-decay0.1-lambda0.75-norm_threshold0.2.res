###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.194, activation diff: 5.194, ratio: 0.634
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 42.302, activation diff: 35.390, ratio: 0.837
#   pulse 3: activated nodes: 6984, borderline nodes: 4853, overall activation: 113.187, activation diff: 71.371, ratio: 0.631
#   pulse 4: activated nodes: 8634, borderline nodes: 5721, overall activation: 439.137, activation diff: 325.950, ratio: 0.742
#   pulse 5: activated nodes: 10146, borderline nodes: 4583, overall activation: 1062.917, activation diff: 623.780, ratio: 0.587
#   pulse 6: activated nodes: 10865, borderline nodes: 2948, overall activation: 2005.249, activation diff: 942.332, ratio: 0.470
#   pulse 7: activated nodes: 11187, borderline nodes: 1281, overall activation: 3070.312, activation diff: 1065.063, ratio: 0.347
#   pulse 8: activated nodes: 11246, borderline nodes: 489, overall activation: 4097.059, activation diff: 1026.748, ratio: 0.251
#   pulse 9: activated nodes: 11306, borderline nodes: 214, overall activation: 5007.176, activation diff: 910.117, ratio: 0.182
#   pulse 10: activated nodes: 11321, borderline nodes: 117, overall activation: 5777.573, activation diff: 770.397, ratio: 0.133
#   pulse 11: activated nodes: 11329, borderline nodes: 67, overall activation: 6412.639, activation diff: 635.066, ratio: 0.099
#   pulse 12: activated nodes: 11332, borderline nodes: 37, overall activation: 6927.321, activation diff: 514.682, ratio: 0.074
#   pulse 13: activated nodes: 11334, borderline nodes: 23, overall activation: 7339.676, activation diff: 412.355, ratio: 0.056
#   pulse 14: activated nodes: 11334, borderline nodes: 15, overall activation: 7667.445, activation diff: 327.769, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7667.4
#   number of spread. activ. pulses: 14
#   running time: 572

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9854178   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98145956   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9787253   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.97632873   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.97278273   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.963019   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.86618394   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8657402   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8655989   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.8651259   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.86500627   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.8643919   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.86417794   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.8641289   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.86405176   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.8640125   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.8637906   REFERENCES
1   Q1   politicalsketche00retsrich_153   18   0.86367273   REFERENCES
1   Q1   politicalsketche00retsrich_156   19   0.86366963   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   20   0.8635106   REFERENCES
