###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.918, activation diff: 8.918, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 127.820, activation diff: 122.541, ratio: 0.959
#   pulse 3: activated nodes: 7117, borderline nodes: 4806, overall activation: 187.397, activation diff: 66.750, ratio: 0.356
#   pulse 4: activated nodes: 8668, borderline nodes: 6005, overall activation: 1334.950, activation diff: 1147.599, ratio: 0.860
#   pulse 5: activated nodes: 10358, borderline nodes: 3983, overall activation: 2638.666, activation diff: 1303.716, ratio: 0.494
#   pulse 6: activated nodes: 10987, borderline nodes: 1744, overall activation: 4035.524, activation diff: 1396.858, ratio: 0.346
#   pulse 7: activated nodes: 11236, borderline nodes: 699, overall activation: 5058.556, activation diff: 1023.032, ratio: 0.202
#   pulse 8: activated nodes: 11287, borderline nodes: 327, overall activation: 5708.352, activation diff: 649.796, ratio: 0.114
#   pulse 9: activated nodes: 11310, borderline nodes: 201, overall activation: 6098.076, activation diff: 389.723, ratio: 0.064
#   pulse 10: activated nodes: 11317, borderline nodes: 130, overall activation: 6326.038, activation diff: 227.962, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 6326.0
#   number of spread. activ. pulses: 10
#   running time: 472

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99652493   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9960916   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99532264   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.99423116   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9934167   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98957765   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7954208   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7953106   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.7952777   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.79526126   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7951722   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.79511905   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.79510266   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7950208   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   15   0.7949812   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.794979   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7949761   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.794975   REFERENCES
1   Q1   europesincenapol00leveuoft_35   19   0.794947   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7949407   REFERENCES
