###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.914, activation diff: 5.914, ratio: 0.663
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 75.144, activation diff: 67.227, ratio: 0.895
#   pulse 3: activated nodes: 7996, borderline nodes: 4261, overall activation: 186.451, activation diff: 111.562, ratio: 0.598
#   pulse 4: activated nodes: 9988, borderline nodes: 5093, overall activation: 577.441, activation diff: 390.990, ratio: 0.677
#   pulse 5: activated nodes: 10879, borderline nodes: 3139, overall activation: 1168.501, activation diff: 591.060, ratio: 0.506
#   pulse 6: activated nodes: 11159, borderline nodes: 1282, overall activation: 1863.209, activation diff: 694.708, ratio: 0.373
#   pulse 7: activated nodes: 11238, borderline nodes: 680, overall activation: 2546.824, activation diff: 683.615, ratio: 0.268
#   pulse 8: activated nodes: 11282, borderline nodes: 569, overall activation: 3160.136, activation diff: 613.312, ratio: 0.194
#   pulse 9: activated nodes: 11299, borderline nodes: 550, overall activation: 3683.161, activation diff: 523.025, ratio: 0.142
#   pulse 10: activated nodes: 11300, borderline nodes: 537, overall activation: 4116.135, activation diff: 432.974, ratio: 0.105
#   pulse 11: activated nodes: 11302, borderline nodes: 528, overall activation: 4468.065, activation diff: 351.929, ratio: 0.079
#   pulse 12: activated nodes: 11303, borderline nodes: 503, overall activation: 4750.703, activation diff: 282.638, ratio: 0.059
#   pulse 13: activated nodes: 11303, borderline nodes: 503, overall activation: 4975.838, activation diff: 225.136, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 4975.8
#   number of spread. activ. pulses: 13
#   running time: 539

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9841912   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97986716   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9764688   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9742207   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9730249   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9617675   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.71547246   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7154348   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7146458   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.714187   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.7141509   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7138262   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.71344376   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7132677   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7129183   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.71288514   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.7128223   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.71281844   REFERENCES
1   Q1   essentialsinmod01howegoog_293   19   0.71270066   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7124858   REFERENCES
