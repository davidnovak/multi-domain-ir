###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.918, activation diff: 8.918, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 142.886, activation diff: 137.608, ratio: 0.963
#   pulse 3: activated nodes: 7117, borderline nodes: 4806, overall activation: 217.298, activation diff: 82.397, ratio: 0.379
#   pulse 4: activated nodes: 8682, borderline nodes: 5965, overall activation: 1681.259, activation diff: 1464.003, ratio: 0.871
#   pulse 5: activated nodes: 10432, borderline nodes: 3759, overall activation: 3442.477, activation diff: 1761.218, ratio: 0.512
#   pulse 6: activated nodes: 11047, borderline nodes: 1419, overall activation: 5325.902, activation diff: 1883.426, ratio: 0.354
#   pulse 7: activated nodes: 11258, borderline nodes: 432, overall activation: 6689.815, activation diff: 1363.912, ratio: 0.204
#   pulse 8: activated nodes: 11319, borderline nodes: 131, overall activation: 7549.553, activation diff: 859.738, ratio: 0.114
#   pulse 9: activated nodes: 11330, borderline nodes: 54, overall activation: 8062.199, activation diff: 512.646, ratio: 0.064
#   pulse 10: activated nodes: 11334, borderline nodes: 23, overall activation: 8360.117, activation diff: 297.918, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 8360.1
#   number of spread. activ. pulses: 10
#   running time: 470

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99652624   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99612045   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9953673   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9942708   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9937264   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9898565   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8948624   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89473903   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.8947028   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.8947004   REFERENCES
1   Q1   essentialsinmod01howegoog_293   11   0.89463246   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   12   0.89458835   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   13   0.8945537   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.89453036   REFERENCES
1   Q1   europesincenapol00leveuoft_35   15   0.8944503   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.8944435   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.8944082   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8943918   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.8943817   REFERENCES
1   Q1   politicalsketche00retsrich_156   20   0.8943799   REFERENCES
