###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.194, activation diff: 5.194, ratio: 0.634
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 27.539, activation diff: 20.628, ratio: 0.749
#   pulse 3: activated nodes: 6984, borderline nodes: 4853, overall activation: 66.703, activation diff: 39.650, ratio: 0.594
#   pulse 4: activated nodes: 7462, borderline nodes: 4658, overall activation: 197.898, activation diff: 131.206, ratio: 0.663
#   pulse 5: activated nodes: 8166, borderline nodes: 3860, overall activation: 402.299, activation diff: 204.401, ratio: 0.508
#   pulse 6: activated nodes: 8464, borderline nodes: 3579, overall activation: 642.443, activation diff: 240.143, ratio: 0.374
#   pulse 7: activated nodes: 8605, borderline nodes: 3449, overall activation: 881.713, activation diff: 239.270, ratio: 0.271
#   pulse 8: activated nodes: 8660, borderline nodes: 3407, overall activation: 1099.804, activation diff: 218.091, ratio: 0.198
#   pulse 9: activated nodes: 8660, borderline nodes: 3406, overall activation: 1288.629, activation diff: 188.825, ratio: 0.147
#   pulse 10: activated nodes: 8660, borderline nodes: 3406, overall activation: 1447.162, activation diff: 158.533, ratio: 0.110
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1577.697, activation diff: 130.534, ratio: 0.083
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1683.775, activation diff: 106.078, ratio: 0.063
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 1769.178, activation diff: 85.403, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1769.2
#   number of spread. activ. pulses: 13
#   running time: 469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97985137   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9736558   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9695458   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9671302   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9583044   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9447722   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4734627   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.47320843   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4730865   REFERENCES
1   Q1   politicalsketche00retsrich_153   10   0.4727519   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.47261927   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.47214258   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   13   0.47174457   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.47172064   REFERENCES
1   Q1   europesincenapol00leveuoft_16   15   0.4716692   REFERENCES
1   Q1   essentialsinmod01howegoog_293   16   0.4715232   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.47131413   REFERENCES
1   Q1   essentialsinmod01howegoog_37   18   0.47128975   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   19   0.47128513   REFERENCES
1   Q1   politicalsketche00retsrich_156   20   0.47119528   REFERENCES
