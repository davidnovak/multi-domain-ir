###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.835, activation diff: 17.835, ratio: 1.507
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 849.642, activation diff: 861.477, ratio: 1.014
#   pulse 3: activated nodes: 7820, borderline nodes: 4517, overall activation: 661.147, activation diff: 1510.789, ratio: 2.285
#   pulse 4: activated nodes: 9884, borderline nodes: 4684, overall activation: 5547.878, activation diff: 6209.025, ratio: 1.119
#   pulse 5: activated nodes: 11167, borderline nodes: 2020, overall activation: 2699.141, activation diff: 8247.019, ratio: 3.055
#   pulse 6: activated nodes: 11300, borderline nodes: 212, overall activation: 5964.668, activation diff: 8663.809, ratio: 1.453
#   pulse 7: activated nodes: 11332, borderline nodes: 45, overall activation: 2791.602, activation diff: 8756.270, ratio: 3.137
#   pulse 8: activated nodes: 11335, borderline nodes: 10, overall activation: 5984.014, activation diff: 8775.617, ratio: 1.467
#   pulse 9: activated nodes: 11339, borderline nodes: 8, overall activation: 2800.314, activation diff: 8784.328, ratio: 3.137
#   pulse 10: activated nodes: 11341, borderline nodes: 2, overall activation: 5986.748, activation diff: 8787.062, ratio: 1.468
#   pulse 11: activated nodes: 11341, borderline nodes: 2, overall activation: 2802.643, activation diff: 8789.391, ratio: 3.136
#   pulse 12: activated nodes: 11341, borderline nodes: 2, overall activation: 5987.470, activation diff: 8790.113, ratio: 1.468
#   pulse 13: activated nodes: 11341, borderline nodes: 2, overall activation: 2803.371, activation diff: 8790.841, ratio: 3.136
#   pulse 14: activated nodes: 11341, borderline nodes: 2, overall activation: 5987.722, activation diff: 8791.093, ratio: 1.468
#   pulse 15: activated nodes: 11341, borderline nodes: 2, overall activation: 2803.584, activation diff: 8791.306, ratio: 3.136

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 2803.6
#   number of spread. activ. pulses: 15
#   running time: 605

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
