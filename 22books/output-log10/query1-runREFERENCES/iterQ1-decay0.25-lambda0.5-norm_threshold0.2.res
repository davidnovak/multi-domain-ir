###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.389, activation diff: 10.389, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 214.128, activation diff: 206.539, ratio: 0.965
#   pulse 3: activated nodes: 7802, borderline nodes: 4480, overall activation: 351.438, activation diff: 141.380, ratio: 0.402
#   pulse 4: activated nodes: 9734, borderline nodes: 5330, overall activation: 1642.244, activation diff: 1290.806, ratio: 0.786
#   pulse 5: activated nodes: 10991, borderline nodes: 2935, overall activation: 2959.726, activation diff: 1317.483, ratio: 0.445
#   pulse 6: activated nodes: 11192, borderline nodes: 1063, overall activation: 4025.865, activation diff: 1066.139, ratio: 0.265
#   pulse 7: activated nodes: 11251, borderline nodes: 652, overall activation: 4724.195, activation diff: 698.330, ratio: 0.148
#   pulse 8: activated nodes: 11261, borderline nodes: 630, overall activation: 5144.045, activation diff: 419.850, ratio: 0.082
#   pulse 9: activated nodes: 11264, borderline nodes: 623, overall activation: 5387.008, activation diff: 242.963, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11264
#   final overall activation: 5387.0
#   number of spread. activ. pulses: 9
#   running time: 434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99509966   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9941276   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9929173   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99172705   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99158835   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9867095   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7428433   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74268883   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7425823   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.74250734   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.74232364   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.742274   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   13   0.74223053   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7421201   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.7420045   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.7419556   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.74191403   REFERENCES
1   Q1   politicalsketche00retsrich_153   18   0.74187773   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.7418387   REFERENCES
1   Q1   europesincenapol00leveuoft_35   20   0.7418343   REFERENCES
