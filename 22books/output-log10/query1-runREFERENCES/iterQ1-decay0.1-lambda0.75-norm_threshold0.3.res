###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.459, activation diff: 4.459, ratio: 0.598
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 16.881, activation diff: 11.061, ratio: 0.655
#   pulse 3: activated nodes: 6159, borderline nodes: 5206, overall activation: 47.136, activation diff: 31.114, ratio: 0.660
#   pulse 4: activated nodes: 6711, borderline nodes: 5035, overall activation: 179.879, activation diff: 132.936, ratio: 0.739
#   pulse 5: activated nodes: 8245, borderline nodes: 5263, overall activation: 497.660, activation diff: 317.781, ratio: 0.639
#   pulse 6: activated nodes: 9826, borderline nodes: 5027, overall activation: 1110.562, activation diff: 612.901, ratio: 0.552
#   pulse 7: activated nodes: 10787, borderline nodes: 3394, overall activation: 2002.223, activation diff: 891.662, ratio: 0.445
#   pulse 8: activated nodes: 11132, borderline nodes: 1704, overall activation: 3018.924, activation diff: 1016.701, ratio: 0.337
#   pulse 9: activated nodes: 11228, borderline nodes: 743, overall activation: 4012.479, activation diff: 993.555, ratio: 0.248
#   pulse 10: activated nodes: 11255, borderline nodes: 327, overall activation: 4904.033, activation diff: 891.553, ratio: 0.182
#   pulse 11: activated nodes: 11308, borderline nodes: 186, overall activation: 5666.438, activation diff: 762.405, ratio: 0.135
#   pulse 12: activated nodes: 11321, borderline nodes: 116, overall activation: 6300.034, activation diff: 633.597, ratio: 0.101
#   pulse 13: activated nodes: 11326, borderline nodes: 71, overall activation: 6817.367, activation diff: 517.333, ratio: 0.076
#   pulse 14: activated nodes: 11331, borderline nodes: 52, overall activation: 7234.629, activation diff: 417.262, ratio: 0.058
#   pulse 15: activated nodes: 11332, borderline nodes: 29, overall activation: 7568.313, activation diff: 333.684, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 7568.3
#   number of spread. activ. pulses: 15
#   running time: 612

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9844208   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98041844   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9779618   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9771966   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.96526   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9583221   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.86927456   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.8692291   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8690015   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8688291   REFERENCES
1   Q1   politicalsketche00retsrich_153   11   0.86880577   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.8687732   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   13   0.86825895   REFERENCES
1   Q1   politicalsketche00retsrich_156   14   0.86824036   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.8681559   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.86787   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.86764383   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8675569   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.86744344   REFERENCES
1   Q1   ouroldworldbackg00bearrich_310   20   0.86737156   REFERENCES
