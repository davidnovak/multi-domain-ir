###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.459, activation diff: 4.459, ratio: 0.598
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 15.342, activation diff: 9.522, ratio: 0.621
#   pulse 3: activated nodes: 6159, borderline nodes: 5206, overall activation: 40.540, activation diff: 26.062, ratio: 0.643
#   pulse 4: activated nodes: 6693, borderline nodes: 5019, overall activation: 140.837, activation diff: 100.511, ratio: 0.714
#   pulse 5: activated nodes: 8155, borderline nodes: 5301, overall activation: 372.722, activation diff: 231.889, ratio: 0.622
#   pulse 6: activated nodes: 9626, borderline nodes: 5243, overall activation: 775.325, activation diff: 402.603, ratio: 0.519
#   pulse 7: activated nodes: 10522, borderline nodes: 4001, overall activation: 1328.836, activation diff: 553.511, ratio: 0.417
#   pulse 8: activated nodes: 10964, borderline nodes: 2398, overall activation: 1955.601, activation diff: 626.765, ratio: 0.320
#   pulse 9: activated nodes: 11156, borderline nodes: 1324, overall activation: 2575.771, activation diff: 620.170, ratio: 0.241
#   pulse 10: activated nodes: 11225, borderline nodes: 795, overall activation: 3140.730, activation diff: 564.959, ratio: 0.180
#   pulse 11: activated nodes: 11239, borderline nodes: 602, overall activation: 3630.292, activation diff: 489.562, ratio: 0.135
#   pulse 12: activated nodes: 11254, borderline nodes: 538, overall activation: 4041.546, activation diff: 411.254, ratio: 0.102
#   pulse 13: activated nodes: 11287, borderline nodes: 549, overall activation: 4380.144, activation diff: 338.598, ratio: 0.077
#   pulse 14: activated nodes: 11288, borderline nodes: 534, overall activation: 4655.241, activation diff: 275.097, ratio: 0.059
#   pulse 15: activated nodes: 11296, borderline nodes: 531, overall activation: 4876.685, activation diff: 221.444, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11296
#   final overall activation: 4876.7
#   number of spread. activ. pulses: 15
#   running time: 559

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98395   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97972393   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97711295   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9764827   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.96304226   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9560952   REFERENCES
1   Q1   politicalsketche00retsrich_154   7   0.72374135   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.7236459   REFERENCES
1   Q1   politicalsketche00retsrich_153   9   0.723552   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7233905   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7233642   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.7230814   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   13   0.7227025   REFERENCES
1   Q1   politicalsketche00retsrich_156   14   0.7226629   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7225793   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7223698   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.7222008   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7221679   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.7220921   REFERENCES
1   Q1   essentialsinmod01howegoog_37   20   0.72196996   REFERENCES
