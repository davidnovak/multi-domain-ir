###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.742, activation diff: 17.742, ratio: 1.204
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 816.615, activation diff: 818.193, ratio: 1.002
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 997.611, activation diff: 824.736, ratio: 0.827
#   pulse 4: activated nodes: 10652, borderline nodes: 2698, overall activation: 3852.050, activation diff: 3003.760, ratio: 0.780
#   pulse 5: activated nodes: 11243, borderline nodes: 995, overall activation: 5647.491, activation diff: 1797.735, ratio: 0.318
#   pulse 6: activated nodes: 11317, borderline nodes: 114, overall activation: 6385.214, activation diff: 737.730, ratio: 0.116
#   pulse 7: activated nodes: 11331, borderline nodes: 83, overall activation: 6646.436, activation diff: 261.221, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 6646.4
#   number of spread. activ. pulses: 7
#   running time: 430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988029   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.998538   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9981869   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99800646   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99725085   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99567544   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7984278   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7983828   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7983271   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.79831684   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.79827565   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   12   0.7982464   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.7982275   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.79822373   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.79820305   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7981666   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.7981477   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.7981398   REFERENCES
1   Q1   europesincenapol00leveuoft_35   19   0.7981385   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.798136   REFERENCES
