###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.583, activation diff: 15.583, ratio: 1.238
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 648.182, activation diff: 651.164, ratio: 1.005
#   pulse 3: activated nodes: 8057, borderline nodes: 4312, overall activation: 748.570, activation diff: 824.983, ratio: 1.102
#   pulse 4: activated nodes: 10139, borderline nodes: 4204, overall activation: 4324.624, activation diff: 3900.380, ratio: 0.902
#   pulse 5: activated nodes: 11185, borderline nodes: 1730, overall activation: 6828.527, activation diff: 2557.810, ratio: 0.375
#   pulse 6: activated nodes: 11299, borderline nodes: 199, overall activation: 8089.477, activation diff: 1262.112, ratio: 0.156
#   pulse 7: activated nodes: 11330, borderline nodes: 51, overall activation: 8569.933, activation diff: 480.464, ratio: 0.056
#   pulse 8: activated nodes: 11335, borderline nodes: 12, overall activation: 8742.929, activation diff: 172.996, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 8742.9
#   number of spread. activ. pulses: 8
#   running time: 484

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99947476   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9993335   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9993107   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9983386   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9971012   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89941776   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89940464   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8993924   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.8993754   REFERENCES
1   Q1   politicalsketche00retsrich_96   11   0.8993678   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   12   0.8993653   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8993599   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.89935774   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   15   0.89935553   REFERENCES
1   Q1   politicalsketche00retsrich_154   16   0.899351   REFERENCES
1   Q1   essentialsinmod01howegoog_283   17   0.8993494   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   18   0.89934754   REFERENCES
1   Q1   europesincenapol00leveuoft_35   19   0.89934665   REFERENCES
1   Q1   essentialsinmod01howegoog_319   20   0.89934015   REFERENCES
