###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.229, activation diff: 13.229, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 326.868, activation diff: 313.639, ratio: 0.960
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 517.068, activation diff: 190.200, ratio: 0.368
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1111.268, activation diff: 594.200, ratio: 0.535
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1547.963, activation diff: 436.696, ratio: 0.282
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1818.659, activation diff: 270.696, ratio: 0.149
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1976.618, activation diff: 157.958, ratio: 0.080
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2066.123, activation diff: 89.506, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2066.1
#   number of spread. activ. pulses: 8
#   running time: 389

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9945674   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9927345   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99099934   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9906893   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9889412   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9839146   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49339658   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.4932925   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.49291342   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.492836   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.4925478   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.49251413   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.49251074   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49226874   REFERENCES
1   Q1   politicalsketche00retsrich_153   15   0.49218136   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.4920777   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.492067   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.49203086   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.49197087   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49194813   REFERENCES
