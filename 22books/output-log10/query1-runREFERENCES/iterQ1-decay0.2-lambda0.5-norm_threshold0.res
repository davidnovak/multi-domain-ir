###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.229, activation diff: 13.229, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 514.375, activation diff: 501.146, ratio: 0.974
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1063.852, activation diff: 549.476, ratio: 0.516
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2870.009, activation diff: 1806.157, ratio: 0.629
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4427.388, activation diff: 1557.380, ratio: 0.352
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5447.943, activation diff: 1020.554, ratio: 0.187
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 6055.085, activation diff: 607.142, ratio: 0.100
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 6402.521, activation diff: 347.436, ratio: 0.054
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 6597.502, activation diff: 194.981, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 6597.5
#   number of spread. activ. pulses: 9
#   running time: 492

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972838   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9963708   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99548465   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9953532   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9940482   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9912756   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.79475665   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7946844   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7943516   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7942821   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7942344   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.7940817   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.794062   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.79394984   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   15   0.79394794   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.79391086   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   17   0.79390365   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7938431   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.79371357   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.79368484   REFERENCES
