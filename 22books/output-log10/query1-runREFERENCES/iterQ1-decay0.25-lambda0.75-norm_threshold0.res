###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.615, activation diff: 6.615, ratio: 0.688
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 133.811, activation diff: 124.940, ratio: 0.934
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 329.002, activation diff: 195.296, ratio: 0.594
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 861.595, activation diff: 532.594, ratio: 0.618
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1563.518, activation diff: 701.923, ratio: 0.449
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2292.526, activation diff: 729.008, ratio: 0.318
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2960.685, activation diff: 668.159, ratio: 0.226
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 3535.268, activation diff: 574.583, ratio: 0.163
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 4012.243, activation diff: 476.975, ratio: 0.119
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4399.938, activation diff: 387.695, ratio: 0.088
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 4710.877, activation diff: 310.939, ratio: 0.066
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4958.040, activation diff: 247.162, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4958.0
#   number of spread. activ. pulses: 12
#   running time: 542

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98207784   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9771159   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97316647   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9730514   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.96874946   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9584253   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.707191   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.7070412   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7058334   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.70536697   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.70527565   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.70525306   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.70504177   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7042629   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.70400906   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.70376337   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.7037338   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.70365536   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.70359755   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.703472   REFERENCES
