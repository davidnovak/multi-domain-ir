###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.615, activation diff: 6.615, ratio: 0.688
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 141.937, activation diff: 133.067, ratio: 0.938
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 356.491, activation diff: 214.659, ratio: 0.602
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 973.330, activation diff: 616.838, ratio: 0.634
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1799.578, activation diff: 826.248, ratio: 0.459
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2661.325, activation diff: 861.747, ratio: 0.324
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3450.544, activation diff: 789.220, ratio: 0.229
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 4127.930, activation diff: 677.385, ratio: 0.164
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 4689.162, activation diff: 561.232, ratio: 0.120
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 5144.571, activation diff: 455.409, ratio: 0.089
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 5509.265, activation diff: 364.694, ratio: 0.066
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 5798.735, activation diff: 289.471, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 5798.7
#   number of spread. activ. pulses: 12
#   running time: 503

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98208433   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97716117   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9732468   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9731753   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9688256   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9586835   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7544438   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.75427157   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7530382   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7524468   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.7523987   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.7523446   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7521521   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7514506   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.751173   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7508894   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.7508292   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.7507886   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7507811   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.75061244   REFERENCES
