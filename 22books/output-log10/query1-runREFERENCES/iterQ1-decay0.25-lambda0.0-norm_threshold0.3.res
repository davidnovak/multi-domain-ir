###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.835, activation diff: 17.835, ratio: 1.507
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 708.886, activation diff: 720.721, ratio: 1.017
#   pulse 3: activated nodes: 7820, borderline nodes: 4517, overall activation: 378.795, activation diff: 1087.680, ratio: 2.871
#   pulse 4: activated nodes: 9868, borderline nodes: 4775, overall activation: 3914.348, activation diff: 4293.142, ratio: 1.097
#   pulse 5: activated nodes: 11137, borderline nodes: 2186, overall activation: 1461.271, activation diff: 5375.619, ratio: 3.679
#   pulse 6: activated nodes: 11217, borderline nodes: 801, overall activation: 4149.446, activation diff: 5610.717, ratio: 1.352
#   pulse 7: activated nodes: 11258, borderline nodes: 620, overall activation: 1502.345, activation diff: 5651.791, ratio: 3.762
#   pulse 8: activated nodes: 11258, borderline nodes: 620, overall activation: 4154.493, activation diff: 5656.838, ratio: 1.362
#   pulse 9: activated nodes: 11258, borderline nodes: 620, overall activation: 1504.101, activation diff: 5658.593, ratio: 3.762
#   pulse 10: activated nodes: 11258, borderline nodes: 620, overall activation: 4154.842, activation diff: 5658.943, ratio: 1.362
#   pulse 11: activated nodes: 11258, borderline nodes: 620, overall activation: 1504.235, activation diff: 5659.077, ratio: 3.762
#   pulse 12: activated nodes: 11258, borderline nodes: 620, overall activation: 4154.885, activation diff: 5659.120, ratio: 1.362
#   pulse 13: activated nodes: 11258, borderline nodes: 620, overall activation: 1504.250, activation diff: 5659.136, ratio: 3.762
#   pulse 14: activated nodes: 11258, borderline nodes: 620, overall activation: 4154.894, activation diff: 5659.144, ratio: 1.362
#   pulse 15: activated nodes: 11258, borderline nodes: 620, overall activation: 1504.253, activation diff: 5659.147, ratio: 3.762

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11258
#   final overall activation: 1504.3
#   number of spread. activ. pulses: 15
#   running time: 636

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
