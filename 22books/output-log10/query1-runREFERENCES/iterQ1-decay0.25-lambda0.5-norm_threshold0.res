###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.229, activation diff: 13.229, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 483.124, activation diff: 469.895, ratio: 0.973
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 962.850, activation diff: 479.726, ratio: 0.498
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2507.031, activation diff: 1544.182, ratio: 0.616
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3818.255, activation diff: 1311.224, ratio: 0.343
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4677.204, activation diff: 858.948, ratio: 0.184
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 5188.771, activation diff: 511.568, ratio: 0.099
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 5481.731, activation diff: 292.960, ratio: 0.053
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 5646.381, activation diff: 164.650, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5646.4
#   number of spread. activ. pulses: 9
#   running time: 457

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972838   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99636996   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9954817   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99535227   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9940432   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99125373   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7450816   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74501646   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.74469614   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.74463433   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.74459326   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.74444807   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.7444306   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.74427724   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.74427706   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   16   0.74427253   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   17   0.7442655   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7442135   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7440865   REFERENCES
1   Q1   politicalsketche00retsrich_159   20   0.7440649   REFERENCES
