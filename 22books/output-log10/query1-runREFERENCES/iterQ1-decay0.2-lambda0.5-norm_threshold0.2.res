###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.389, activation diff: 10.389, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 227.774, activation diff: 220.185, ratio: 0.967
#   pulse 3: activated nodes: 7802, borderline nodes: 4480, overall activation: 384.612, activation diff: 161.170, ratio: 0.419
#   pulse 4: activated nodes: 9742, borderline nodes: 5274, overall activation: 1870.384, activation diff: 1485.772, ratio: 0.794
#   pulse 5: activated nodes: 11017, borderline nodes: 2851, overall activation: 3429.078, activation diff: 1558.694, ratio: 0.455
#   pulse 6: activated nodes: 11209, borderline nodes: 819, overall activation: 4693.518, activation diff: 1264.440, ratio: 0.269
#   pulse 7: activated nodes: 11297, borderline nodes: 287, overall activation: 5519.758, activation diff: 826.240, ratio: 0.150
#   pulse 8: activated nodes: 11319, borderline nodes: 133, overall activation: 6016.172, activation diff: 496.414, ratio: 0.083
#   pulse 9: activated nodes: 11327, borderline nodes: 106, overall activation: 6303.683, activation diff: 287.511, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327
#   final overall activation: 6303.7
#   number of spread. activ. pulses: 9
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9950999   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99413484   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99293196   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9917686   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99160683   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9868333   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7923734   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.792204   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7921029   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7920143   REFERENCES
1   Q1   essentialsinmod01howegoog_293   11   0.79183877   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   12   0.79182506   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.79182076   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7916218   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.79150355   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.7914412   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.79141015   REFERENCES
1   Q1   europesincenapol00leveuoft_35   18   0.79138947   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   19   0.791361   REFERENCES
1   Q1   shorthistoryofmo00haslrich_134   20   0.79133976   REFERENCES
