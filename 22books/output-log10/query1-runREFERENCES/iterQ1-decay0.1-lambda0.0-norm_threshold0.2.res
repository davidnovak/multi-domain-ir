###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.777, activation diff: 20.777, ratio: 1.406
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1193.264, activation diff: 1208.041, ratio: 1.012
#   pulse 3: activated nodes: 8173, borderline nodes: 4071, overall activation: 1012.975, activation diff: 2206.239, ratio: 2.178
#   pulse 4: activated nodes: 10290, borderline nodes: 3598, overall activation: 5745.211, activation diff: 6758.186, ratio: 1.176
#   pulse 5: activated nodes: 11236, borderline nodes: 1300, overall activation: 2776.536, activation diff: 8521.747, ratio: 3.069
#   pulse 6: activated nodes: 11317, borderline nodes: 117, overall activation: 5996.722, activation diff: 8773.258, ratio: 1.463
#   pulse 7: activated nodes: 11337, borderline nodes: 30, overall activation: 2832.546, activation diff: 8829.268, ratio: 3.117
#   pulse 8: activated nodes: 11341, borderline nodes: 8, overall activation: 6008.164, activation diff: 8840.709, ratio: 1.471
#   pulse 9: activated nodes: 11341, borderline nodes: 6, overall activation: 2838.941, activation diff: 8847.104, ratio: 3.116
#   pulse 10: activated nodes: 11341, borderline nodes: 2, overall activation: 6009.950, activation diff: 8848.891, ratio: 1.472
#   pulse 11: activated nodes: 11341, borderline nodes: 1, overall activation: 2840.493, activation diff: 8850.443, ratio: 3.116
#   pulse 12: activated nodes: 11341, borderline nodes: 1, overall activation: 6010.365, activation diff: 8850.858, ratio: 1.473
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 2840.831, activation diff: 8851.196, ratio: 3.116
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 6010.479, activation diff: 8851.310, ratio: 1.473
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 2840.905, activation diff: 8851.384, ratio: 3.116

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 2840.9
#   number of spread. activ. pulses: 15
#   running time: 680

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
