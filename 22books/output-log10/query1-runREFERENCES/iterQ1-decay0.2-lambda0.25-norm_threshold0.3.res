###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.377, activation diff: 13.377, ratio: 1.289
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 379.555, activation diff: 382.094, ratio: 1.007
#   pulse 3: activated nodes: 7560, borderline nodes: 4598, overall activation: 294.310, activation diff: 441.147, ratio: 1.499
#   pulse 4: activated nodes: 9498, borderline nodes: 5357, overall activation: 2978.530, activation diff: 2866.573, ratio: 0.962
#   pulse 5: activated nodes: 10999, borderline nodes: 2911, overall activation: 4535.666, activation diff: 1706.601, ratio: 0.376
#   pulse 6: activated nodes: 11210, borderline nodes: 699, overall activation: 5769.163, activation diff: 1239.230, ratio: 0.215
#   pulse 7: activated nodes: 11276, borderline nodes: 308, overall activation: 6304.156, activation diff: 534.994, ratio: 0.085
#   pulse 8: activated nodes: 11321, borderline nodes: 139, overall activation: 6497.959, activation diff: 193.802, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 6498.0
#   number of spread. activ. pulses: 8
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993822   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993329   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991509   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99902916   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99803996   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9962762   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7993779   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.799367   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.799362   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.7993431   REFERENCES
1   Q1   politicalsketche00retsrich_96   11   0.7993331   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   12   0.7993299   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.79932976   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7993256   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.79932153   REFERENCES
1   Q1   essentialsinmod01howegoog_283   16   0.799315   REFERENCES
1   Q1   politicalsketche00retsrich_154   17   0.7993077   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   18   0.7993054   REFERENCES
1   Q1   europesincenapol00leveuoft_16   19   0.7993033   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   20   0.7993022   REFERENCES
