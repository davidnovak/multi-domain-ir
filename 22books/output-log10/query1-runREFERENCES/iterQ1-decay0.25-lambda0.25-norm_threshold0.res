###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.844, activation diff: 19.844, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1009.120, activation diff: 1007.109, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1386.610, activation diff: 744.528, ratio: 0.537
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3801.763, activation diff: 2415.152, ratio: 0.635
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5126.588, activation diff: 1324.825, ratio: 0.258
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 5610.027, activation diff: 483.439, ratio: 0.086
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 5771.593, activation diff: 161.566, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 5771.6
#   number of spread. activ. pulses: 7
#   running time: 419

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993978   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99907297   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9987475   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9987315   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978074   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964844   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7490144   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74899065   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.74887717   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.74887   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.7488652   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.7488165   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.7488104   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7487738   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.74877375   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   16   0.7487254   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.7487154   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.7487148   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.7487123   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7487048   REFERENCES
