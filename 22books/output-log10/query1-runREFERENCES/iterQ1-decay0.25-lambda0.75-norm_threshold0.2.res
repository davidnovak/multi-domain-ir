###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.194, activation diff: 5.194, ratio: 0.634
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 36.766, activation diff: 29.854, ratio: 0.812
#   pulse 3: activated nodes: 6984, borderline nodes: 4853, overall activation: 95.465, activation diff: 59.185, ratio: 0.620
#   pulse 4: activated nodes: 8501, borderline nodes: 5628, overall activation: 335.899, activation diff: 240.434, ratio: 0.716
#   pulse 5: activated nodes: 9999, borderline nodes: 4903, overall activation: 758.198, activation diff: 422.300, ratio: 0.557
#   pulse 6: activated nodes: 10733, borderline nodes: 3503, overall activation: 1352.209, activation diff: 594.011, ratio: 0.439
#   pulse 7: activated nodes: 11097, borderline nodes: 1770, overall activation: 2015.713, activation diff: 663.504, ratio: 0.329
#   pulse 8: activated nodes: 11218, borderline nodes: 859, overall activation: 2659.988, activation diff: 644.275, ratio: 0.242
#   pulse 9: activated nodes: 11241, borderline nodes: 614, overall activation: 3237.140, activation diff: 577.153, ratio: 0.178
#   pulse 10: activated nodes: 11284, borderline nodes: 560, overall activation: 3730.526, activation diff: 493.386, ratio: 0.132
#   pulse 11: activated nodes: 11292, borderline nodes: 541, overall activation: 4140.503, activation diff: 409.977, ratio: 0.099
#   pulse 12: activated nodes: 11300, borderline nodes: 535, overall activation: 4475.072, activation diff: 334.570, ratio: 0.075
#   pulse 13: activated nodes: 11301, borderline nodes: 531, overall activation: 4744.845, activation diff: 269.773, ratio: 0.057
#   pulse 14: activated nodes: 11303, borderline nodes: 501, overall activation: 4960.647, activation diff: 215.802, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 4960.6
#   number of spread. activ. pulses: 14
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9852853   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9811274   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97829485   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9759799   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9716854   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9616324   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.72137   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.72088313   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.72075266   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.72063327   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.72052747   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7196755   REFERENCES
1   Q1   politicalsketche00retsrich_153   13   0.71962357   REFERENCES
1   Q1   europesincenapol00leveuoft_16   14   0.719607   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.7195071   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.71942604   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.719346   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.71928096   REFERENCES
1   Q1   politicalsketche00retsrich_156   19   0.7191352   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   20   0.7190781   REFERENCES
