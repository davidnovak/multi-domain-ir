###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.742, activation diff: 17.742, ratio: 1.204
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 917.518, activation diff: 919.095, ratio: 1.002
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 1267.106, activation diff: 1073.562, ratio: 0.847
#   pulse 4: activated nodes: 10654, borderline nodes: 2665, overall activation: 4905.958, activation diff: 3848.327, ratio: 0.784
#   pulse 5: activated nodes: 11253, borderline nodes: 955, overall activation: 7365.924, activation diff: 2464.265, ratio: 0.335
#   pulse 6: activated nodes: 11327, borderline nodes: 99, overall activation: 8367.106, activation diff: 1001.226, ratio: 0.120
#   pulse 7: activated nodes: 11339, borderline nodes: 29, overall activation: 8717.857, activation diff: 350.751, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 8717.9
#   number of spread. activ. pulses: 7
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988029   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9985384   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9981886   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9980068   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972551   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99570084   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89823216   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89818054   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.89812934   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.89810634   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   11   0.89808667   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.8980666   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.8980664   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.898005   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.8979843   REFERENCES
1   Q1   europesincenapol00leveuoft_35   16   0.89794886   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.89794683   REFERENCES
1   Q1   shorthistoryofmo00haslrich_134   18   0.89792496   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.89792395   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.8979175   REFERENCES
