###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.377, activation diff: 13.377, ratio: 1.289
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 426.240, activation diff: 428.778, ratio: 1.006
#   pulse 3: activated nodes: 7560, borderline nodes: 4598, overall activation: 382.871, activation diff: 548.326, ratio: 1.432
#   pulse 4: activated nodes: 9539, borderline nodes: 5301, overall activation: 3770.444, activation diff: 3652.608, ratio: 0.969
#   pulse 5: activated nodes: 11047, borderline nodes: 2800, overall activation: 5975.378, activation diff: 2423.902, ratio: 0.406
#   pulse 6: activated nodes: 11257, borderline nodes: 519, overall activation: 7616.588, activation diff: 1657.364, ratio: 0.218
#   pulse 7: activated nodes: 11322, borderline nodes: 155, overall activation: 8337.487, activation diff: 720.910, ratio: 0.086
#   pulse 8: activated nodes: 11333, borderline nodes: 57, overall activation: 8607.833, activation diff: 270.346, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 8607.8
#   number of spread. activ. pulses: 8
#   running time: 478

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993822   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999336   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99915767   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99908465   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9980507   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.996367   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8993012   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.8992879   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8992823   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.89926535   REFERENCES
1   Q1   politicalsketche00retsrich_96   11   0.8992582   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   12   0.89925236   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.8992494   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.89924777   REFERENCES
1   Q1   essentialsinmod01howegoog_283   15   0.89924645   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.8992442   REFERENCES
1   Q1   europesincenapol00leveuoft_35   17   0.8992382   REFERENCES
1   Q1   essentialsinmod01howegoog_319   18   0.89923716   REFERENCES
1   Q1   politicalsketche00retsrich_154   19   0.89923495   REFERENCES
1   Q1   frenchschoolsth02instgoog_135   20   0.8992245   REFERENCES
