###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.835, activation diff: 17.835, ratio: 1.507
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 755.804, activation diff: 767.640, ratio: 1.016
#   pulse 3: activated nodes: 7820, borderline nodes: 4517, overall activation: 466.166, activation diff: 1221.970, ratio: 2.621
#   pulse 4: activated nodes: 9869, borderline nodes: 4741, overall activation: 4433.511, activation diff: 4899.677, ratio: 1.105
#   pulse 5: activated nodes: 11145, borderline nodes: 2103, overall activation: 1836.121, activation diff: 6269.632, ratio: 3.415
#   pulse 6: activated nodes: 11285, borderline nodes: 271, overall activation: 4729.316, activation diff: 6565.437, ratio: 1.388
#   pulse 7: activated nodes: 11317, borderline nodes: 143, overall activation: 1892.412, activation diff: 6621.729, ratio: 3.499
#   pulse 8: activated nodes: 11324, borderline nodes: 82, overall activation: 4737.732, activation diff: 6630.145, ratio: 1.399
#   pulse 9: activated nodes: 11324, borderline nodes: 82, overall activation: 1894.894, activation diff: 6632.626, ratio: 3.500
#   pulse 10: activated nodes: 11324, borderline nodes: 78, overall activation: 4738.429, activation diff: 6633.323, ratio: 1.400
#   pulse 11: activated nodes: 11324, borderline nodes: 78, overall activation: 1895.144, activation diff: 6633.573, ratio: 3.500
#   pulse 12: activated nodes: 11324, borderline nodes: 78, overall activation: 4738.544, activation diff: 6633.688, ratio: 1.400
#   pulse 13: activated nodes: 11324, borderline nodes: 78, overall activation: 1895.186, activation diff: 6633.730, ratio: 3.500
#   pulse 14: activated nodes: 11324, borderline nodes: 78, overall activation: 4738.570, activation diff: 6633.756, ratio: 1.400
#   pulse 15: activated nodes: 11324, borderline nodes: 78, overall activation: 1895.195, activation diff: 6633.765, ratio: 3.500

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 1895.2
#   number of spread. activ. pulses: 15
#   running time: 534

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
