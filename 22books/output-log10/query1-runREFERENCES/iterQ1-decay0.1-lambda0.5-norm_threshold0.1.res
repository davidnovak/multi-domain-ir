###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.828, activation diff: 11.828, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 400.618, activation diff: 390.053, ratio: 0.974
#   pulse 3: activated nodes: 8340, borderline nodes: 3672, overall activation: 816.278, activation diff: 416.966, ratio: 0.511
#   pulse 4: activated nodes: 10447, borderline nodes: 3288, overall activation: 3042.410, activation diff: 2226.133, ratio: 0.732
#   pulse 5: activated nodes: 11222, borderline nodes: 1305, overall activation: 5218.810, activation diff: 2176.399, ratio: 0.417
#   pulse 6: activated nodes: 11307, borderline nodes: 150, overall activation: 6739.442, activation diff: 1520.633, ratio: 0.226
#   pulse 7: activated nodes: 11331, borderline nodes: 46, overall activation: 7666.975, activation diff: 927.533, ratio: 0.121
#   pulse 8: activated nodes: 11335, borderline nodes: 11, overall activation: 8204.241, activation diff: 537.266, ratio: 0.065
#   pulse 9: activated nodes: 11339, borderline nodes: 9, overall activation: 8508.143, activation diff: 303.902, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 8508.1
#   number of spread. activ. pulses: 9
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965278   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99548376   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9944328   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9939529   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99300563   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98961914   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.89296615   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89283293   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.89256024   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.8924094   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.8923732   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   12   0.89228106   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.8922604   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.8920139   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.8919792   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   16   0.891904   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.89189076   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.8918687   REFERENCES
1   Q1   shorthistoryofmo00haslrich_134   19   0.89184934   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   20   0.891816   REFERENCES
