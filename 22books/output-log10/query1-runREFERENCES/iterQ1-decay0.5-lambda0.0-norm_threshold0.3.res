###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.835, activation diff: 17.835, ratio: 1.507
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 474.292, activation diff: 486.128, ratio: 1.025
#   pulse 3: activated nodes: 7820, borderline nodes: 4517, overall activation: 57.828, activation diff: 532.120, ratio: 9.202
#   pulse 4: activated nodes: 7820, borderline nodes: 4517, overall activation: 1955.945, activation diff: 2013.773, ratio: 1.030
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 62.576, activation diff: 2018.521, ratio: 32.257
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 1980.330, activation diff: 2042.906, ratio: 1.032
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.644, activation diff: 2042.974, ratio: 32.613
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1980.455, activation diff: 2043.099, ratio: 1.032
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.646, activation diff: 2043.102, ratio: 32.613
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1980.460, activation diff: 2043.106, ratio: 1.032
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.647, activation diff: 2043.106, ratio: 32.613
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1980.460, activation diff: 2043.106, ratio: 1.032
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.647, activation diff: 2043.106, ratio: 32.613
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1980.460, activation diff: 2043.106, ratio: 1.032
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.647, activation diff: 2043.106, ratio: 32.613

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 62.6
#   number of spread. activ. pulses: 15
#   running time: 532

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
