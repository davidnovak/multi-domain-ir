###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.615, activation diff: 6.615, ratio: 0.688
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 158.191, activation diff: 149.320, ratio: 0.944
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 413.716, activation diff: 255.630, ratio: 0.618
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1223.245, activation diff: 809.529, ratio: 0.662
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2332.134, activation diff: 1108.889, ratio: 0.475
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3489.425, activation diff: 1157.291, ratio: 0.332
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4542.966, activation diff: 1053.542, ratio: 0.232
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5441.235, activation diff: 898.269, ratio: 0.165
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 6180.819, activation diff: 739.584, ratio: 0.120
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 6777.305, activation diff: 596.486, ratio: 0.088
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 7252.165, activation diff: 474.861, ratio: 0.065
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 7627.009, activation diff: 374.844, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7627.0
#   number of spread. activ. pulses: 12
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98209494   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97723746   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9733827   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.97338027   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.96895343   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95911765   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8488991   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.8487261   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.8473867   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.84663934   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.8466133   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.8465414   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.8463768   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.84578454   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.84548116   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.84516823   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   17   0.84511125   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.84510803   REFERENCES
1   Q1   essentialsinmod01howegoog_293   19   0.8449034   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.8448284   REFERENCES
