###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.918, activation diff: 8.918, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 82.621, activation diff: 77.342, ratio: 0.936
#   pulse 3: activated nodes: 7117, borderline nodes: 4806, overall activation: 111.575, activation diff: 33.710, ratio: 0.302
#   pulse 4: activated nodes: 7117, borderline nodes: 4806, overall activation: 617.081, activation diff: 505.556, ratio: 0.819
#   pulse 5: activated nodes: 8417, borderline nodes: 3620, overall activation: 1060.760, activation diff: 443.679, ratio: 0.418
#   pulse 6: activated nodes: 8517, borderline nodes: 3556, overall activation: 1433.482, activation diff: 372.723, ratio: 0.260
#   pulse 7: activated nodes: 8654, borderline nodes: 3404, overall activation: 1684.149, activation diff: 250.667, ratio: 0.149
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 1837.724, activation diff: 153.575, ratio: 0.084
#   pulse 9: activated nodes: 8660, borderline nodes: 3406, overall activation: 1927.693, activation diff: 89.968, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 1927.7
#   number of spread. activ. pulses: 9
#   running time: 439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99302727   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99183816   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99021506   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9892558   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9829549   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9784882   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49412733   REFERENCES
1   Q1   politicalsketche00retsrich_154   8   0.49398652   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49389246   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.49375188   REFERENCES
1   Q1   politicalsketche00retsrich_153   11   0.49373743   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.49370468   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.4936071   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   14   0.49343452   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.49337673   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.49333316   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.49325508   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   18   0.4932486   REFERENCES
1   Q1   politicalsketche00retsrich_90   19   0.4932402   REFERENCES
1   Q1   europesincenapol00leveuoft_16   20   0.49323595   REFERENCES
