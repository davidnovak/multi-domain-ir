###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.389, activation diff: 10.389, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 145.898, activation diff: 138.309, ratio: 0.948
#   pulse 3: activated nodes: 7802, borderline nodes: 4480, overall activation: 210.034, activation diff: 66.896, ratio: 0.319
#   pulse 4: activated nodes: 7802, borderline nodes: 4480, overall activation: 797.771, activation diff: 587.737, ratio: 0.737
#   pulse 5: activated nodes: 8637, borderline nodes: 3429, overall activation: 1274.537, activation diff: 476.766, ratio: 0.374
#   pulse 6: activated nodes: 8653, borderline nodes: 3419, overall activation: 1608.311, activation diff: 333.774, ratio: 0.208
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1814.606, activation diff: 206.295, ratio: 0.114
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1935.585, activation diff: 120.979, ratio: 0.063
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 2004.572, activation diff: 68.987, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2004.6
#   number of spread. activ. pulses: 9
#   running time: 402

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99509746   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99405754   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9927772   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.991418   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.9912833   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9856975   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4951684   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.49500912   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.4949887   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.49484307   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.4947152   REFERENCES
1   Q1   politicalsketche00retsrich_153   12   0.49458513   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.49454218   REFERENCES
1   Q1   essentialsinmod01howegoog_293   14   0.49438554   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   15   0.49438128   REFERENCES
1   Q1   europesincenapol00leveuoft_16   16   0.49436966   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.49436635   REFERENCES
1   Q1   politicalsketche00retsrich_159   18   0.49435323   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.4943332   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49430627   REFERENCES
