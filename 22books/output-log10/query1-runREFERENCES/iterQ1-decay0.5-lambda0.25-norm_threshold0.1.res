###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.742, activation diff: 17.742, ratio: 1.204
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 513.908, activation diff: 515.485, ratio: 1.003
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 353.471, activation diff: 242.604, ratio: 0.686
#   pulse 4: activated nodes: 8528, borderline nodes: 3466, overall activation: 1513.792, activation diff: 1160.900, ratio: 0.767
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1927.594, activation diff: 413.802, ratio: 0.215
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 2070.429, activation diff: 142.834, ratio: 0.069
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 2115.285, activation diff: 44.856, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2115.3
#   number of spread. activ. pulses: 7
#   running time: 388

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988029   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99853504   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9981739   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99800295   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99721646   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.995494   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.49900472   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.49897623   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.4989266   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.4989072   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49883005   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.49881834   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.49881202   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49878436   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.49874592   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.49873334   REFERENCES
1   Q1   politicalsketche00retsrich_153   17   0.49868703   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.4986843   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.4986791   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.49863797   REFERENCES
