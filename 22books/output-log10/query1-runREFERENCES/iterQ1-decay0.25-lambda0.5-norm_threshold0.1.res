###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.828, activation diff: 11.828, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 335.836, activation diff: 325.271, ratio: 0.969
#   pulse 3: activated nodes: 8340, borderline nodes: 3672, overall activation: 618.670, activation diff: 283.922, ratio: 0.459
#   pulse 4: activated nodes: 10437, borderline nodes: 3390, overall activation: 2085.976, activation diff: 1467.306, ratio: 0.703
#   pulse 5: activated nodes: 11205, borderline nodes: 1477, overall activation: 3451.086, activation diff: 1365.110, ratio: 0.396
#   pulse 6: activated nodes: 11245, borderline nodes: 611, overall activation: 4409.158, activation diff: 958.072, ratio: 0.217
#   pulse 7: activated nodes: 11270, borderline nodes: 531, overall activation: 4999.104, activation diff: 589.946, ratio: 0.118
#   pulse 8: activated nodes: 11270, borderline nodes: 522, overall activation: 5343.060, activation diff: 343.956, ratio: 0.064
#   pulse 9: activated nodes: 11270, borderline nodes: 522, overall activation: 5538.455, activation diff: 195.396, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11270
#   final overall activation: 5538.5
#   number of spread. activ. pulses: 9
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965277   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954784   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9944179   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99394053   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9929822   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9894864   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7441293   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7440269   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7437715   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.74367034   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7436271   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.74340117   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   13   0.74339056   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.74331146   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.743274   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.7432297   REFERENCES
1   Q1   europesincenapol00leveuoft_16   17   0.7432152   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7432065   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.74310476   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.74305135   REFERENCES
