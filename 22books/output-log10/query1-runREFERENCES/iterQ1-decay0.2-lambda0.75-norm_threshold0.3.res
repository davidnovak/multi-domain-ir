###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.459, activation diff: 4.459, ratio: 0.598
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 15.855, activation diff: 10.035, ratio: 0.633
#   pulse 3: activated nodes: 6159, borderline nodes: 5206, overall activation: 42.739, activation diff: 27.746, ratio: 0.649
#   pulse 4: activated nodes: 6707, borderline nodes: 5032, overall activation: 153.517, activation diff: 110.985, ratio: 0.723
#   pulse 5: activated nodes: 8186, borderline nodes: 5289, overall activation: 412.070, activation diff: 258.553, ratio: 0.627
#   pulse 6: activated nodes: 9684, borderline nodes: 5168, overall activation: 876.534, activation diff: 464.464, ratio: 0.530
#   pulse 7: activated nodes: 10636, borderline nodes: 3786, overall activation: 1530.277, activation diff: 653.743, ratio: 0.427
#   pulse 8: activated nodes: 11039, borderline nodes: 2108, overall activation: 2275.455, activation diff: 745.178, ratio: 0.327
#   pulse 9: activated nodes: 11192, borderline nodes: 1083, overall activation: 3011.755, activation diff: 736.299, ratio: 0.244
#   pulse 10: activated nodes: 11234, borderline nodes: 646, overall activation: 3679.588, activation diff: 667.833, ratio: 0.181
#   pulse 11: activated nodes: 11252, borderline nodes: 408, overall activation: 4255.744, activation diff: 576.156, ratio: 0.135
#   pulse 12: activated nodes: 11297, borderline nodes: 289, overall activation: 4737.927, activation diff: 482.183, ratio: 0.102
#   pulse 13: activated nodes: 11310, borderline nodes: 208, overall activation: 5133.814, activation diff: 395.887, ratio: 0.077
#   pulse 14: activated nodes: 11317, borderline nodes: 158, overall activation: 5454.765, activation diff: 320.950, ratio: 0.059
#   pulse 15: activated nodes: 11321, borderline nodes: 97, overall activation: 5712.837, activation diff: 258.072, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5712.8
#   number of spread. activ. pulses: 15
#   running time: 542

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9841225   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97997946   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9774265   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.9767468   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.96387285   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.956927   REFERENCES
1   Q1   politicalsketche00retsrich_154   7   0.7722311   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.7721827   REFERENCES
1   Q1   politicalsketche00retsrich_153   9   0.77196646   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.77190775   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7718612   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.77164334   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   13   0.7712198   REFERENCES
1   Q1   politicalsketche00retsrich_156   14   0.77118915   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.7711068   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.77087384   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7706203   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.77061176   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.7706074   REFERENCES
1   Q1   essentialsinmod01howegoog_37   20   0.77038014   REFERENCES
