###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.828, activation diff: 11.828, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 357.430, activation diff: 346.865, ratio: 0.970
#   pulse 3: activated nodes: 8340, borderline nodes: 3672, overall activation: 681.851, activation diff: 325.581, ratio: 0.477
#   pulse 4: activated nodes: 10443, borderline nodes: 3354, overall activation: 2384.477, activation diff: 1702.626, ratio: 0.714
#   pulse 5: activated nodes: 11208, borderline nodes: 1403, overall activation: 4000.852, activation diff: 1616.375, ratio: 0.404
#   pulse 6: activated nodes: 11295, borderline nodes: 216, overall activation: 5137.137, activation diff: 1136.285, ratio: 0.221
#   pulse 7: activated nodes: 11325, borderline nodes: 108, overall activation: 5835.697, activation diff: 698.560, ratio: 0.120
#   pulse 8: activated nodes: 11329, borderline nodes: 74, overall activation: 6242.485, activation diff: 406.788, ratio: 0.065
#   pulse 9: activated nodes: 11330, borderline nodes: 63, overall activation: 6473.481, activation diff: 230.996, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 6473.5
#   number of spread. activ. pulses: 9
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99652773   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954804   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9944236   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99394554   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99299127   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98953766   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7937422   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.79362905   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7933674   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.79325104   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.79320884   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   12   0.79303074   REFERENCES
1   Q1   essentialsinmod01howegoog_293   13   0.7930255   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.7928831   REFERENCES
1   Q1   politicalsketche00retsrich_96   15   0.7928462   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.792784   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7927716   REFERENCES
1   Q1   europesincenapol00leveuoft_16   18   0.79276717   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7926715   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7926037   REFERENCES
