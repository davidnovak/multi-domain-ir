###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.914, activation diff: 5.914, ratio: 0.663
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 88.069, activation diff: 80.152, ratio: 0.910
#   pulse 3: activated nodes: 7996, borderline nodes: 4261, overall activation: 227.194, activation diff: 139.381, ratio: 0.613
#   pulse 4: activated nodes: 10064, borderline nodes: 5034, overall activation: 795.317, activation diff: 568.123, ratio: 0.714
#   pulse 5: activated nodes: 10996, borderline nodes: 2674, overall activation: 1714.694, activation diff: 919.377, ratio: 0.536
#   pulse 6: activated nodes: 11205, borderline nodes: 931, overall activation: 2819.912, activation diff: 1105.218, ratio: 0.392
#   pulse 7: activated nodes: 11262, borderline nodes: 270, overall activation: 3904.700, activation diff: 1084.788, ratio: 0.278
#   pulse 8: activated nodes: 11322, borderline nodes: 121, overall activation: 4869.823, activation diff: 965.123, ratio: 0.198
#   pulse 9: activated nodes: 11330, borderline nodes: 84, overall activation: 5685.737, activation diff: 815.914, ratio: 0.144
#   pulse 10: activated nodes: 11332, borderline nodes: 28, overall activation: 6355.870, activation diff: 670.133, ratio: 0.105
#   pulse 11: activated nodes: 11335, borderline nodes: 15, overall activation: 6896.576, activation diff: 540.706, ratio: 0.078
#   pulse 12: activated nodes: 11335, borderline nodes: 7, overall activation: 7327.863, activation diff: 431.287, ratio: 0.059
#   pulse 13: activated nodes: 11340, borderline nodes: 9, overall activation: 7669.229, activation diff: 341.366, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 7669.2
#   number of spread. activ. pulses: 13
#   running time: 513

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98423505   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98006266   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97676635   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.97486603   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97328055   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9627276   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8590126   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.85892546   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.858107   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.85726017   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.8572451   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.8569973   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.8566427   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   14   0.85655224   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.8561988   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.85617423   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.8561466   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.85594565   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.8558577   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.8558551   REFERENCES
