###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.844, activation diff: 19.844, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1208.461, activation diff: 1206.450, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1958.956, activation diff: 1190.823, ratio: 0.608
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 5582.082, activation diff: 3623.126, ratio: 0.649
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 7759.821, activation diff: 2177.739, ratio: 0.281
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 8555.056, activation diff: 795.234, ratio: 0.093
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 8818.967, activation diff: 263.911, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 8819.0
#   number of spread. activ. pulses: 7
#   running time: 473

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993978   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9990733   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99874884   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9987315   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978099   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964927   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8988187   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89878875   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.8986686   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8986529   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.8986384   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.89858127   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.8985729   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   14   0.8985636   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.8985502   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.8985392   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   17   0.8985353   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8984842   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.8984767   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.89844906   REFERENCES
