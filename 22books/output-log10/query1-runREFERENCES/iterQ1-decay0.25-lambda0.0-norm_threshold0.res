###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 20.459, activation diff: 26.459, ratio: 1.293
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1664.383, activation diff: 1684.842, ratio: 1.012
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1004.899, activation diff: 2669.282, ratio: 2.656
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4180.211, activation diff: 5185.110, ratio: 1.240
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1606.340, activation diff: 5786.551, ratio: 3.602
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4233.448, activation diff: 5839.788, ratio: 1.379
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 1614.831, activation diff: 5848.279, ratio: 3.622
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4235.612, activation diff: 5850.443, ratio: 1.381
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1615.105, activation diff: 5850.717, ratio: 3.622
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 4235.738, activation diff: 5850.843, ratio: 1.381
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1615.124, activation diff: 5850.862, ratio: 3.623
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 4235.753, activation diff: 5850.877, ratio: 1.381
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1615.126, activation diff: 5850.879, ratio: 3.623
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 4235.755, activation diff: 5850.882, ratio: 1.381
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1615.127, activation diff: 5850.882, ratio: 3.623

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 1615.1
#   number of spread. activ. pulses: 15
#   running time: 616

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
