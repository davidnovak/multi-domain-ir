###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.583, activation diff: 15.583, ratio: 1.238
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 576.978, activation diff: 579.961, ratio: 1.005
#   pulse 3: activated nodes: 8057, borderline nodes: 4312, overall activation: 580.060, activation diff: 647.524, ratio: 1.116
#   pulse 4: activated nodes: 10130, borderline nodes: 4285, overall activation: 3408.859, activation diff: 3056.884, ratio: 0.897
#   pulse 5: activated nodes: 11169, borderline nodes: 1846, overall activation: 5216.675, activation diff: 1837.136, ratio: 0.352
#   pulse 6: activated nodes: 11285, borderline nodes: 269, overall activation: 6151.754, activation diff: 935.358, ratio: 0.152
#   pulse 7: activated nodes: 11319, borderline nodes: 137, overall activation: 6511.069, activation diff: 359.315, ratio: 0.055
#   pulse 8: activated nodes: 11329, borderline nodes: 80, overall activation: 6636.801, activation diff: 125.732, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 6636.8
#   number of spread. activ. pulses: 8
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9994744   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99933195   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9993092   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99833494   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9970747   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.79948187   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.79947084   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.79946   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.7994422   REFERENCES
1   Q1   politicalsketche00retsrich_96   11   0.799433   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   12   0.7994303   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   13   0.799428   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   14   0.79942626   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.799423   REFERENCES
1   Q1   politicalsketche00retsrich_154   16   0.79941285   REFERENCES
1   Q1   essentialsinmod01howegoog_283   17   0.7994116   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   18   0.79940605   REFERENCES
1   Q1   europesincenapol00leveuoft_16   19   0.799405   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   20   0.799401   REFERENCES
