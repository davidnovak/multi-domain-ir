###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.914, activation diff: 5.914, ratio: 0.663
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 53.602, activation diff: 45.686, ratio: 0.852
#   pulse 3: activated nodes: 7996, borderline nodes: 4261, overall activation: 124.230, activation diff: 70.883, ratio: 0.571
#   pulse 4: activated nodes: 8186, borderline nodes: 3984, overall activation: 319.111, activation diff: 194.881, ratio: 0.611
#   pulse 5: activated nodes: 8625, borderline nodes: 3424, overall activation: 569.984, activation diff: 250.873, ratio: 0.440
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 828.029, activation diff: 258.045, ratio: 0.312
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1065.074, activation diff: 237.045, ratio: 0.223
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1270.460, activation diff: 205.386, ratio: 0.162
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1442.655, activation diff: 172.196, ratio: 0.119
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1584.141, activation diff: 141.485, ratio: 0.089
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1698.853, activation diff: 114.713, ratio: 0.068
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1790.993, activation diff: 92.140, ratio: 0.051
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 1864.496, activation diff: 73.502, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1864.5
#   number of spread. activ. pulses: 13
#   running time: 455

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9840567   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9793353   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9756707   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.972474   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97231257   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9589925   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.47639576   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.47592705   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47579822   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.47576332   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.4752229   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.47502536   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.4750064   REFERENCES
1   Q1   politicalsketche00retsrich_153   14   0.4748759   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   15   0.4747053   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.4744248   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.47436762   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.4740907   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.47403145   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.47400674   REFERENCES
