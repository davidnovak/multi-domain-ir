###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.777, activation diff: 20.777, ratio: 1.406
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 995.296, activation diff: 1010.074, ratio: 1.015
#   pulse 3: activated nodes: 8173, borderline nodes: 4071, overall activation: 582.424, activation diff: 1577.720, ratio: 2.709
#   pulse 4: activated nodes: 10287, borderline nodes: 3666, overall activation: 4046.584, activation diff: 4629.008, ratio: 1.144
#   pulse 5: activated nodes: 11223, borderline nodes: 1391, overall activation: 1525.445, activation diff: 5572.028, ratio: 3.653
#   pulse 6: activated nodes: 11243, borderline nodes: 605, overall activation: 4179.541, activation diff: 5704.985, ratio: 1.365
#   pulse 7: activated nodes: 11264, borderline nodes: 550, overall activation: 1549.421, activation diff: 5728.962, ratio: 3.697
#   pulse 8: activated nodes: 11264, borderline nodes: 550, overall activation: 4182.715, activation diff: 5732.136, ratio: 1.370
#   pulse 9: activated nodes: 11264, borderline nodes: 550, overall activation: 1550.319, activation diff: 5733.034, ratio: 3.698
#   pulse 10: activated nodes: 11264, borderline nodes: 550, overall activation: 4182.907, activation diff: 5733.225, ratio: 1.371
#   pulse 11: activated nodes: 11264, borderline nodes: 550, overall activation: 1550.378, activation diff: 5733.285, ratio: 3.698
#   pulse 12: activated nodes: 11264, borderline nodes: 550, overall activation: 4182.929, activation diff: 5733.307, ratio: 1.371
#   pulse 13: activated nodes: 11264, borderline nodes: 550, overall activation: 1550.385, activation diff: 5733.314, ratio: 3.698
#   pulse 14: activated nodes: 11264, borderline nodes: 550, overall activation: 4182.933, activation diff: 5733.318, ratio: 1.371
#   pulse 15: activated nodes: 11264, borderline nodes: 550, overall activation: 1550.386, activation diff: 5733.319, ratio: 3.698

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11264
#   final overall activation: 1550.4
#   number of spread. activ. pulses: 15
#   running time: 615

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
