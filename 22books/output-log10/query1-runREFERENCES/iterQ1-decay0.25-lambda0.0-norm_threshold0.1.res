###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.656, activation diff: 23.656, ratio: 1.340
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1331.082, activation diff: 1348.738, ratio: 1.013
#   pulse 3: activated nodes: 8627, borderline nodes: 3418, overall activation: 797.868, activation diff: 2128.950, ratio: 2.668
#   pulse 4: activated nodes: 10760, borderline nodes: 2437, overall activation: 4126.427, activation diff: 4924.295, ratio: 1.193
#   pulse 5: activated nodes: 11260, borderline nodes: 769, overall activation: 1580.907, activation diff: 5707.335, ratio: 3.610
#   pulse 6: activated nodes: 11298, borderline nodes: 518, overall activation: 4211.254, activation diff: 5792.162, ratio: 1.375
#   pulse 7: activated nodes: 11304, borderline nodes: 475, overall activation: 1596.621, activation diff: 5807.875, ratio: 3.638
#   pulse 8: activated nodes: 11304, borderline nodes: 475, overall activation: 4215.150, activation diff: 5811.771, ratio: 1.379
#   pulse 9: activated nodes: 11304, borderline nodes: 475, overall activation: 1597.189, activation diff: 5812.340, ratio: 3.639
#   pulse 10: activated nodes: 11304, borderline nodes: 475, overall activation: 4215.407, activation diff: 5812.596, ratio: 1.379
#   pulse 11: activated nodes: 11304, borderline nodes: 475, overall activation: 1597.232, activation diff: 5812.639, ratio: 3.639
#   pulse 12: activated nodes: 11304, borderline nodes: 475, overall activation: 4215.442, activation diff: 5812.674, ratio: 1.379
#   pulse 13: activated nodes: 11304, borderline nodes: 475, overall activation: 1597.238, activation diff: 5812.680, ratio: 3.639
#   pulse 14: activated nodes: 11304, borderline nodes: 475, overall activation: 4215.449, activation diff: 5812.686, ratio: 1.379
#   pulse 15: activated nodes: 11304, borderline nodes: 475, overall activation: 1597.239, activation diff: 5812.688, ratio: 3.639

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 1597.2
#   number of spread. activ. pulses: 15
#   running time: 553

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
