###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.828, activation diff: 11.828, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 227.867, activation diff: 217.302, ratio: 0.954
#   pulse 3: activated nodes: 8340, borderline nodes: 3672, overall activation: 346.881, activation diff: 119.740, ratio: 0.345
#   pulse 4: activated nodes: 8340, borderline nodes: 3672, overall activation: 961.134, activation diff: 614.252, ratio: 0.639
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1430.854, activation diff: 469.720, ratio: 0.328
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1730.202, activation diff: 299.348, ratio: 0.173
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1907.393, activation diff: 177.192, ratio: 0.093
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 2008.720, activation diff: 101.327, ratio: 0.050
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 2065.515, activation diff: 56.795, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 2065.5
#   number of spread. activ. pulses: 9
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99652725   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99545836   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.994363   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99388397   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9928962   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98903716   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.4960482   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.4959613   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.49580124   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.49572515   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.49554992   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.49544424   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.4954301   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49539813   REFERENCES
1   Q1   politicalsketche00retsrich_153   15   0.49534047   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.49526206   REFERENCES
1   Q1   politicalsketche00retsrich_159   17   0.49526012   REFERENCES
1   Q1   essentialsinmod01howegoog_293   18   0.4952095   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   19   0.49520856   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.4951849   REFERENCES
