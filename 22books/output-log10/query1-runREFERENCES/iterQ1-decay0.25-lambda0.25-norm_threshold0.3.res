###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.377, activation diff: 13.377, ratio: 1.289
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 356.213, activation diff: 358.751, ratio: 1.007
#   pulse 3: activated nodes: 7560, borderline nodes: 4598, overall activation: 254.929, activation diff: 392.458, ratio: 1.539
#   pulse 4: activated nodes: 9495, borderline nodes: 5401, overall activation: 2619.853, activation diff: 2510.981, ratio: 0.958
#   pulse 5: activated nodes: 10971, borderline nodes: 3003, overall activation: 3901.712, activation diff: 1397.832, ratio: 0.358
#   pulse 6: activated nodes: 11157, borderline nodes: 1128, overall activation: 4937.174, activation diff: 1039.464, ratio: 0.211
#   pulse 7: activated nodes: 11242, borderline nodes: 691, overall activation: 5386.483, activation diff: 449.309, ratio: 0.083
#   pulse 8: activated nodes: 11248, borderline nodes: 679, overall activation: 5547.018, activation diff: 160.535, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11248
#   final overall activation: 5547.0
#   number of spread. activ. pulses: 8
#   running time: 447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99938214   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99933076   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99914646   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99898994   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99803334   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99622   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7494158   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.7494064   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7494015   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   10   0.74937904   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.7493689   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   12   0.7493677   REFERENCES
1   Q1   politicalsketche00retsrich_96   13   0.749367   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.7493603   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7493569   REFERENCES
1   Q1   europesincenapol00leveuoft_16   16   0.7493464   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   17   0.7493445   REFERENCES
1   Q1   politicalsketche00retsrich_154   18   0.7493443   REFERENCES
1   Q1   shorthistoryofmo00haslrich_77   19   0.74934185   REFERENCES
1   Q1   essentialsinmod01howegoog_283   20   0.74933934   REFERENCES
