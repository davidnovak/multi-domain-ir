###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.389, activation diff: 10.389, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 255.066, activation diff: 247.477, ratio: 0.970
#   pulse 3: activated nodes: 7802, borderline nodes: 4480, overall activation: 455.889, activation diff: 205.680, ratio: 0.451
#   pulse 4: activated nodes: 9784, borderline nodes: 5216, overall activation: 2384.753, activation diff: 1928.863, ratio: 0.809
#   pulse 5: activated nodes: 11062, borderline nodes: 2733, overall activation: 4489.554, activation diff: 2104.801, ratio: 0.469
#   pulse 6: activated nodes: 11235, borderline nodes: 545, overall activation: 6185.857, activation diff: 1696.303, ratio: 0.274
#   pulse 7: activated nodes: 11321, borderline nodes: 155, overall activation: 7285.255, activation diff: 1099.398, ratio: 0.151
#   pulse 8: activated nodes: 11331, borderline nodes: 76, overall activation: 7944.030, activation diff: 658.775, ratio: 0.083
#   pulse 9: activated nodes: 11335, borderline nodes: 14, overall activation: 8325.390, activation diff: 381.360, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 8325.4
#   number of spread. activ. pulses: 9
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9951002   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9941462   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99295497   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9918306   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99163646   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9870397   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.8914308   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.89123136   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.89114505   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.89102185   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   11   0.8909763   REFERENCES
1   Q1   essentialsinmod01howegoog_293   12   0.89095426   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.89081573   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.89060843   REFERENCES
1   Q1   shorthistoryofmo00haslrich_134   15   0.890496   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.89048696   REFERENCES
1   Q1   ouroldworldbackg00bearrich_383   17   0.8904772   REFERENCES
1   Q1   europesincenapol00leveuoft_35   18   0.8904439   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.89041483   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.8903812   REFERENCES
