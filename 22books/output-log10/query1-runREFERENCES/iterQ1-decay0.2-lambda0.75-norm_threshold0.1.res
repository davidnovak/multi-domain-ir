###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.914, activation diff: 5.914, ratio: 0.663
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 79.452, activation diff: 71.535, ratio: 0.900
#   pulse 3: activated nodes: 7996, borderline nodes: 4261, overall activation: 199.731, activation diff: 120.534, ratio: 0.603
#   pulse 4: activated nodes: 10003, borderline nodes: 5052, overall activation: 643.945, activation diff: 444.214, ratio: 0.690
#   pulse 5: activated nodes: 10934, borderline nodes: 2991, overall activation: 1333.754, activation diff: 689.809, ratio: 0.517
#   pulse 6: activated nodes: 11192, borderline nodes: 1167, overall activation: 2154.059, activation diff: 820.304, ratio: 0.381
#   pulse 7: activated nodes: 11244, borderline nodes: 533, overall activation: 2962.350, activation diff: 808.292, ratio: 0.273
#   pulse 8: activated nodes: 11301, borderline nodes: 253, overall activation: 3686.422, activation diff: 724.071, ratio: 0.196
#   pulse 9: activated nodes: 11320, borderline nodes: 125, overall activation: 4302.631, activation diff: 616.209, ratio: 0.143
#   pulse 10: activated nodes: 11328, borderline nodes: 93, overall activation: 4811.908, activation diff: 509.277, ratio: 0.106
#   pulse 11: activated nodes: 11332, borderline nodes: 56, overall activation: 5225.288, activation diff: 413.380, ratio: 0.079
#   pulse 12: activated nodes: 11332, borderline nodes: 43, overall activation: 5556.911, activation diff: 331.623, ratio: 0.060
#   pulse 13: activated nodes: 11333, borderline nodes: 37, overall activation: 5820.813, activation diff: 263.902, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11333
#   final overall activation: 5820.8
#   number of spread. activ. pulses: 13
#   running time: 531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9842077   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9799392   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9765781   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9744581   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9731193   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9621249   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.76330364   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.7632909   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.76247865   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7618753   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.76184416   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7615502   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7611438   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   14   0.76106536   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7606827   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.7606153   REFERENCES
1   Q1   politicalsketche00retsrich_96   17   0.7606   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7605378   REFERENCES
1   Q1   essentialsinmod01howegoog_293   19   0.7604453   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7602836   REFERENCES
