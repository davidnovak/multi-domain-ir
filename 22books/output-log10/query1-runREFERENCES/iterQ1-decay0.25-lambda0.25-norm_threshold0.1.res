###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.742, activation diff: 17.742, ratio: 1.204
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 766.164, activation diff: 767.742, ratio: 1.002
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 872.187, activation diff: 709.646, ratio: 0.814
#   pulse 4: activated nodes: 10650, borderline nodes: 2719, overall activation: 3362.325, activation diff: 2610.972, ratio: 0.777
#   pulse 5: activated nodes: 11235, borderline nodes: 1017, overall activation: 4860.394, activation diff: 1499.448, ratio: 0.309
#   pulse 6: activated nodes: 11260, borderline nodes: 526, overall activation: 5473.033, activation diff: 612.639, ratio: 0.112
#   pulse 7: activated nodes: 11280, borderline nodes: 493, overall activation: 5689.677, activation diff: 216.645, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11280
#   final overall activation: 5689.7
#   number of spread. activ. pulses: 7
#   running time: 417

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9988029   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9985378   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99818575   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.99800617   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.997248   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9956587   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.7485254   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.74848384   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.74842614   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   10   0.7484218   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7483804   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.74833083   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   13   0.7483151   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.74830973   REFERENCES
1   Q1   essentialsinmod01howegoog_293   15   0.7483014   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.74827194   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.7482612   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   18   0.74825406   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   19   0.7482461   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   20   0.74824256   REFERENCES
