###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.615, activation diff: 6.615, ratio: 0.688
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 93.177, activation diff: 84.306, ratio: 0.905
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 203.269, activation diff: 110.197, ratio: 0.542
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 437.915, activation diff: 234.646, ratio: 0.536
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 708.717, activation diff: 270.802, ratio: 0.382
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 968.624, activation diff: 259.908, ratio: 0.268
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1198.069, activation diff: 229.445, ratio: 0.192
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1392.126, activation diff: 194.057, ratio: 0.139
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1552.251, activation diff: 160.125, ratio: 0.103
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1682.333, activation diff: 130.082, ratio: 0.077
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1786.898, activation diff: 104.565, ratio: 0.059
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1870.319, activation diff: 83.421, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1870.3
#   number of spread. activ. pulses: 12
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9820235   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.97677064   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9725617   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.972068   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9681628   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95637494   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.47087458   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.47051868   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47001192   REFERENCES
1   Q1   politicalsketche00retsrich_154   10   0.4698804   REFERENCES
1   Q1   europesincenapol00leveuoft_16   11   0.46964744   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.46953413   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.46909726   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.46875173   REFERENCES
1   Q1   politicalsketche00retsrich_153   15   0.46855193   REFERENCES
1   Q1   politicalsketche00retsrich_159   16   0.468401   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.4682304   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.46803695   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.46792647   REFERENCES
1   Q1   essentialsinmod01howegoog_293   20   0.46790373   REFERENCES
