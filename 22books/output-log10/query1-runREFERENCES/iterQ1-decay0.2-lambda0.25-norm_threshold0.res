###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.844, activation diff: 19.844, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1075.567, activation diff: 1073.556, ratio: 0.998
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1570.348, activation diff: 886.248, ratio: 0.564
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4370.449, activation diff: 2800.101, ratio: 0.641
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5961.981, activation diff: 1591.533, ratio: 0.267
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 6547.050, activation diff: 585.069, ratio: 0.089
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 6742.955, activation diff: 195.905, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 6743.0
#   number of spread. activ. pulses: 7
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993978   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999073   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.998748   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9987315   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99780846   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.996488   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.79894924   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   8   0.79892343   REFERENCES
1   Q1   politicalsketche00retsrich_154   9   0.7988075   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.79879755   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.79878974   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   12   0.79873824   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.7987313   REFERENCES
1   Q1   politicalsketche00retsrich_96   14   0.7986971   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   15   0.79869556   REFERENCES
1   Q1   shorthistoryofmo00haslrich_136   16   0.79867786   REFERENCES
1   Q1   essentialsinmod01howegoog_293   17   0.7986639   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   18   0.79863846   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   19   0.79863596   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   20   0.7986204   REFERENCES
