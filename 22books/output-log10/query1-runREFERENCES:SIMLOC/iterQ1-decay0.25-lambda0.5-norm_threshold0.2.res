###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.728, activation diff: 8.728, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 200.184, activation diff: 194.224, ratio: 0.970
#   pulse 3: activated nodes: 8632, borderline nodes: 5384, overall activation: 323.454, activation diff: 127.253, ratio: 0.393
#   pulse 4: activated nodes: 10874, borderline nodes: 6975, overall activation: 1662.781, activation diff: 1339.326, ratio: 0.805
#   pulse 5: activated nodes: 11163, borderline nodes: 3249, overall activation: 2973.000, activation diff: 1310.219, ratio: 0.441
#   pulse 6: activated nodes: 11362, borderline nodes: 1429, overall activation: 4208.664, activation diff: 1235.664, ratio: 0.294
#   pulse 7: activated nodes: 11416, borderline nodes: 365, overall activation: 5133.558, activation diff: 924.893, ratio: 0.180
#   pulse 8: activated nodes: 11425, borderline nodes: 191, overall activation: 5741.265, activation diff: 607.707, ratio: 0.106
#   pulse 9: activated nodes: 11431, borderline nodes: 137, overall activation: 6110.822, activation diff: 369.557, ratio: 0.060
#   pulse 10: activated nodes: 11434, borderline nodes: 120, overall activation: 6327.925, activation diff: 217.103, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 6327.9
#   number of spread. activ. pulses: 10
#   running time: 1310

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99692035   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9963267   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9956167   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99561155   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99478406   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9913056   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7462269   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7461796   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74613273   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.7461326   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.74607193   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.746027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74592686   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7459053   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.7458789   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.745831   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7458135   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.7458043   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   19   0.7457799   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.7457574   REFERENCES:SIMLOC
