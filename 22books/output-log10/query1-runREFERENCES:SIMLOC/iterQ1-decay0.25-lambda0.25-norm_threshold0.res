###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.505, activation diff: 17.505, ratio: 1.207
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 996.216, activation diff: 993.586, ratio: 0.997
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1340.651, activation diff: 621.596, ratio: 0.464
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4464.617, activation diff: 3123.967, ratio: 0.700
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6034.716, activation diff: 1570.099, ratio: 0.260
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 6669.444, activation diff: 634.728, ratio: 0.095
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6896.302, activation diff: 226.858, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6896.3
#   number of spread. activ. pulses: 7
#   running time: 1245

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99894136   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99869215   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99863935   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99768394   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9963913   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74904346   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7489528   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74890643   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.74887615   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7488728   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7488526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74879694   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.74877846   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.74877405   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.74876064   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.74872416   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.7487227   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   19   0.74871874   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.74870265   REFERENCES:SIMLOC
