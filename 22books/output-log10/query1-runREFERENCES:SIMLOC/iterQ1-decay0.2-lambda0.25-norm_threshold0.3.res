###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.822, activation diff: 10.822, ratio: 1.384
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 352.190, activation diff: 353.704, ratio: 1.004
#   pulse 3: activated nodes: 8533, borderline nodes: 5690, overall activation: 241.559, activation diff: 380.582, ratio: 1.576
#   pulse 4: activated nodes: 10842, borderline nodes: 7306, overall activation: 3031.415, activation diff: 2904.197, ratio: 0.958
#   pulse 5: activated nodes: 11140, borderline nodes: 3055, overall activation: 4362.265, activation diff: 1461.121, ratio: 0.335
#   pulse 6: activated nodes: 11364, borderline nodes: 1355, overall activation: 6044.964, activation diff: 1682.811, ratio: 0.278
#   pulse 7: activated nodes: 11425, borderline nodes: 255, overall activation: 6867.885, activation diff: 822.922, ratio: 0.120
#   pulse 8: activated nodes: 11439, borderline nodes: 88, overall activation: 7204.378, activation diff: 336.493, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 7204.4
#   number of spread. activ. pulses: 8
#   running time: 1301

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99929726   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992183   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9990454   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99888486   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979459   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.996024   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.79935217   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7993513   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7993479   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7993289   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.7993231   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.79932296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7993175   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   14   0.79931736   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7993119   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.79930466   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.7992997   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.79929656   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.7992959   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.79929566   REFERENCES:SIMLOC
