###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.215, activation diff: 10.215, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 217.035, activation diff: 207.958, ratio: 0.958
#   pulse 3: activated nodes: 8834, borderline nodes: 4236, overall activation: 352.070, activation diff: 135.700, ratio: 0.385
#   pulse 4: activated nodes: 10525, borderline nodes: 5455, overall activation: 1108.668, activation diff: 756.598, ratio: 0.682
#   pulse 5: activated nodes: 10768, borderline nodes: 4242, overall activation: 1763.133, activation diff: 654.465, ratio: 0.371
#   pulse 6: activated nodes: 10860, borderline nodes: 3666, overall activation: 2284.805, activation diff: 521.672, ratio: 0.228
#   pulse 7: activated nodes: 10886, borderline nodes: 3543, overall activation: 2663.830, activation diff: 379.025, ratio: 0.142
#   pulse 8: activated nodes: 10890, borderline nodes: 3528, overall activation: 2924.291, activation diff: 260.460, ratio: 0.089
#   pulse 9: activated nodes: 10892, borderline nodes: 3519, overall activation: 3093.994, activation diff: 169.703, ratio: 0.055
#   pulse 10: activated nodes: 10895, borderline nodes: 3516, overall activation: 3200.379, activation diff: 106.385, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 3200.4
#   number of spread. activ. pulses: 10
#   running time: 1353

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9979159   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9972385   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9967921   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9965669   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.995548   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9928536   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49794728   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.4979118   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49783152   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.4978212   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.4977976   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.4976729   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49767223   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   14   0.49762288   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.497612   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49760902   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   17   0.49760714   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.49758643   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.49757683   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.4975556   REFERENCES:SIMLOC
