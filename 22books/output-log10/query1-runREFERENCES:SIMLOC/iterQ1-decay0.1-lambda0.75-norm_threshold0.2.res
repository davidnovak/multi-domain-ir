###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.364, activation diff: 4.364, ratio: 0.593
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 38.159, activation diff: 32.245, ratio: 0.845
#   pulse 3: activated nodes: 8256, borderline nodes: 6241, overall activation: 103.542, activation diff: 66.042, ratio: 0.638
#   pulse 4: activated nodes: 9722, borderline nodes: 6943, overall activation: 409.671, activation diff: 306.136, ratio: 0.747
#   pulse 5: activated nodes: 10699, borderline nodes: 5407, overall activation: 995.690, activation diff: 586.019, ratio: 0.589
#   pulse 6: activated nodes: 11197, borderline nodes: 3953, overall activation: 1886.238, activation diff: 890.548, ratio: 0.472
#   pulse 7: activated nodes: 11330, borderline nodes: 2006, overall activation: 2924.519, activation diff: 1038.281, ratio: 0.355
#   pulse 8: activated nodes: 11406, borderline nodes: 750, overall activation: 3985.573, activation diff: 1061.054, ratio: 0.266
#   pulse 9: activated nodes: 11430, borderline nodes: 292, overall activation: 4964.335, activation diff: 978.762, ratio: 0.197
#   pulse 10: activated nodes: 11438, borderline nodes: 117, overall activation: 5814.341, activation diff: 850.006, ratio: 0.146
#   pulse 11: activated nodes: 11443, borderline nodes: 66, overall activation: 6523.588, activation diff: 709.247, ratio: 0.109
#   pulse 12: activated nodes: 11448, borderline nodes: 36, overall activation: 7100.865, activation diff: 577.276, ratio: 0.081
#   pulse 13: activated nodes: 11449, borderline nodes: 28, overall activation: 7563.653, activation diff: 462.788, ratio: 0.061
#   pulse 14: activated nodes: 11450, borderline nodes: 24, overall activation: 7931.067, activation diff: 367.414, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7931.1
#   number of spread. activ. pulses: 14
#   running time: 1763

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9831468   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97790194   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.97473186   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9742577   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9709418   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9583525   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.8650083   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.86493695   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.86489534   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8644821   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.86422306   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.8635815   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.86355954   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8634895   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   15   0.8634001   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.86327124   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.86326677   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.86294484   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.8628701   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   20   0.862723   REFERENCES:SIMLOC
