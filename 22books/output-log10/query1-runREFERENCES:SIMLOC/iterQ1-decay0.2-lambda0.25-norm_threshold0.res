###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.505, activation diff: 17.505, ratio: 1.207
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1061.868, activation diff: 1059.238, ratio: 0.998
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1476.874, activation diff: 707.509, ratio: 0.479
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5006.436, activation diff: 3529.561, ratio: 0.705
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6762.441, activation diff: 1756.005, ratio: 0.260
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7453.717, activation diff: 691.276, ratio: 0.093
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 7695.758, activation diff: 242.041, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7695.8
#   number of spread. activ. pulses: 7
#   running time: 1299

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99894166   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9986923   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9986404   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9976894   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9963997   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7989825   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.798884   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.79883575   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.79880345   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.79879916   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7987817   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.79871947   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.79869896   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   15   0.7986951   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.7986941   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   17   0.7986551   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.79865277   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7986436   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.7986207   REFERENCES:SIMLOC
