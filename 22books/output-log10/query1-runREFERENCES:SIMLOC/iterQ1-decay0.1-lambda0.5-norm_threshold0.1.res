###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.215, activation diff: 10.215, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 382.502, activation diff: 373.425, ratio: 0.976
#   pulse 3: activated nodes: 8834, borderline nodes: 4236, overall activation: 726.770, activation diff: 345.463, ratio: 0.475
#   pulse 4: activated nodes: 11186, borderline nodes: 5008, overall activation: 2995.859, activation diff: 2269.089, ratio: 0.757
#   pulse 5: activated nodes: 11391, borderline nodes: 1007, overall activation: 5158.375, activation diff: 2162.516, ratio: 0.419
#   pulse 6: activated nodes: 11436, borderline nodes: 188, overall activation: 6820.037, activation diff: 1661.662, ratio: 0.244
#   pulse 7: activated nodes: 11450, borderline nodes: 37, overall activation: 7877.612, activation diff: 1057.575, ratio: 0.134
#   pulse 8: activated nodes: 11451, borderline nodes: 19, overall activation: 8496.713, activation diff: 619.101, ratio: 0.073
#   pulse 9: activated nodes: 11454, borderline nodes: 9, overall activation: 8847.355, activation diff: 350.641, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 8847.4
#   number of spread. activ. pulses: 9
#   running time: 1343

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99583316   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99454355   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9936917   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9933768   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99239147   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9885576   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8926357   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89252853   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8923303   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8922261   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8921828   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.8920958   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.89174914   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.8917414   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.8916818   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   16   0.89165115   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.8916501   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   18   0.891634   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   19   0.89163136   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.8916073   REFERENCES:SIMLOC
