###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.728, activation diff: 8.728, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 213.024, activation diff: 207.063, ratio: 0.972
#   pulse 3: activated nodes: 8632, borderline nodes: 5384, overall activation: 349.723, activation diff: 140.930, ratio: 0.403
#   pulse 4: activated nodes: 10885, borderline nodes: 6949, overall activation: 1863.739, activation diff: 1514.016, ratio: 0.812
#   pulse 5: activated nodes: 11188, borderline nodes: 3111, overall activation: 3364.055, activation diff: 1500.316, ratio: 0.446
#   pulse 6: activated nodes: 11372, borderline nodes: 1217, overall activation: 4791.844, activation diff: 1427.790, ratio: 0.298
#   pulse 7: activated nodes: 11427, borderline nodes: 263, overall activation: 5842.311, activation diff: 1050.466, ratio: 0.180
#   pulse 8: activated nodes: 11438, borderline nodes: 94, overall activation: 6518.490, activation diff: 676.180, ratio: 0.104
#   pulse 9: activated nodes: 11446, borderline nodes: 52, overall activation: 6923.348, activation diff: 404.857, ratio: 0.058
#   pulse 10: activated nodes: 11447, borderline nodes: 45, overall activation: 7158.487, activation diff: 235.139, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 7158.5
#   number of spread. activ. pulses: 10
#   running time: 1368

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969206   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99633527   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9956423   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9956341   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99480325   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9913931   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7959765   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7959278   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.79588133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7958771   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.7958132   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7957946   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7956623   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7956463   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.79561293   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.7955804   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.79554224   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.79553026   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   19   0.7955116   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7955076   REFERENCES:SIMLOC
