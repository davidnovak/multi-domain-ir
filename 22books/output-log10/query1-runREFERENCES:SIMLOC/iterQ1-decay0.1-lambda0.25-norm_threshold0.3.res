###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.822, activation diff: 10.822, ratio: 1.384
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 395.600, activation diff: 397.114, ratio: 1.004
#   pulse 3: activated nodes: 8533, borderline nodes: 5690, overall activation: 295.981, activation diff: 452.512, ratio: 1.529
#   pulse 4: activated nodes: 10860, borderline nodes: 7260, overall activation: 3755.587, activation diff: 3609.092, ratio: 0.961
#   pulse 5: activated nodes: 11173, borderline nodes: 2771, overall activation: 5547.342, activation diff: 1945.751, ratio: 0.351
#   pulse 6: activated nodes: 11409, borderline nodes: 938, overall activation: 7648.184, activation diff: 2101.304, ratio: 0.275
#   pulse 7: activated nodes: 11440, borderline nodes: 153, overall activation: 8600.533, activation diff: 952.349, ratio: 0.111
#   pulse 8: activated nodes: 11448, borderline nodes: 41, overall activation: 8966.238, activation diff: 365.705, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 8966.2
#   number of spread. activ. pulses: 8
#   running time: 1294

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992973   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99922526   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9990598   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99897957   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99796665   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9961511   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.89927185   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.8992702   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8992664   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.8992466   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   11   0.8992452   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.8992413   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   13   0.8992356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89923286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.89922917   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.89922714   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   17   0.899227   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.8992214   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   19   0.8992207   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.899219   REFERENCES:SIMLOC
