###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.430, activation diff: 20.430, ratio: 1.416
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1545.148, activation diff: 1559.362, ratio: 1.009
#   pulse 3: activated nodes: 9154, borderline nodes: 3894, overall activation: 926.649, activation diff: 2467.868, ratio: 2.663
#   pulse 4: activated nodes: 11288, borderline nodes: 3491, overall activation: 7019.423, activation diff: 7381.310, ratio: 1.052
#   pulse 5: activated nodes: 11427, borderline nodes: 318, overall activation: 7024.610, activation diff: 3612.420, ratio: 0.514
#   pulse 6: activated nodes: 11447, borderline nodes: 43, overall activation: 9155.327, activation diff: 2224.146, ratio: 0.243
#   pulse 7: activated nodes: 11453, borderline nodes: 14, overall activation: 9276.751, activation diff: 132.761, ratio: 0.014

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 9276.8
#   number of spread. activ. pulses: 7
#   running time: 2348

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999136   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991727   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9990471   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981774   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   14   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   15   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   16   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   17   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   18   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   19   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_546   20   0.9   REFERENCES:SIMLOC
