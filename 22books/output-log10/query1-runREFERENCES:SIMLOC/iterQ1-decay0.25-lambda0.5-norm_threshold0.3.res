###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.214, activation diff: 7.214, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 108.963, activation diff: 105.034, ratio: 0.964
#   pulse 3: activated nodes: 8324, borderline nodes: 6133, overall activation: 155.609, activation diff: 53.077, ratio: 0.341
#   pulse 4: activated nodes: 10238, borderline nodes: 7841, overall activation: 1181.493, activation diff: 1026.016, ratio: 0.868
#   pulse 5: activated nodes: 10686, borderline nodes: 4630, overall activation: 2257.733, activation diff: 1076.240, ratio: 0.477
#   pulse 6: activated nodes: 11276, borderline nodes: 3381, overall activation: 3535.767, activation diff: 1278.033, ratio: 0.361
#   pulse 7: activated nodes: 11397, borderline nodes: 960, overall activation: 4594.845, activation diff: 1059.078, ratio: 0.230
#   pulse 8: activated nodes: 11416, borderline nodes: 418, overall activation: 5354.147, activation diff: 759.302, ratio: 0.142
#   pulse 9: activated nodes: 11421, borderline nodes: 231, overall activation: 5846.407, activation diff: 492.261, ratio: 0.084
#   pulse 10: activated nodes: 11425, borderline nodes: 175, overall activation: 6147.141, activation diff: 300.733, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11425
#   final overall activation: 6147.1
#   number of spread. activ. pulses: 10
#   running time: 1373

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9956969   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.994948   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99409753   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.99357986   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.99226964   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9876478   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7454599   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7454419   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.74542224   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.74537975   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7453746   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.74529934   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7452401   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7452317   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.74516857   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.74514806   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7451409   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.7451093   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   19   0.745073   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.74505454   REFERENCES:SIMLOC
