###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.108, activation diff: 5.108, ratio: 0.630
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 74.631, activation diff: 67.668, ratio: 0.907
#   pulse 3: activated nodes: 8690, borderline nodes: 5068, overall activation: 188.213, activation diff: 113.944, ratio: 0.605
#   pulse 4: activated nodes: 10859, borderline nodes: 6235, overall activation: 630.030, activation diff: 441.817, ratio: 0.701
#   pulse 5: activated nodes: 11186, borderline nodes: 3644, overall activation: 1314.287, activation diff: 684.257, ratio: 0.521
#   pulse 6: activated nodes: 11345, borderline nodes: 1817, overall activation: 2154.748, activation diff: 840.461, ratio: 0.390
#   pulse 7: activated nodes: 11411, borderline nodes: 571, overall activation: 3028.806, activation diff: 874.057, ratio: 0.289
#   pulse 8: activated nodes: 11434, borderline nodes: 230, overall activation: 3848.694, activation diff: 819.888, ratio: 0.213
#   pulse 9: activated nodes: 11439, borderline nodes: 97, overall activation: 4568.309, activation diff: 719.615, ratio: 0.158
#   pulse 10: activated nodes: 11448, borderline nodes: 51, overall activation: 5173.826, activation diff: 605.518, ratio: 0.117
#   pulse 11: activated nodes: 11450, borderline nodes: 42, overall activation: 5669.614, activation diff: 495.787, ratio: 0.087
#   pulse 12: activated nodes: 11451, borderline nodes: 37, overall activation: 6068.764, activation diff: 399.151, ratio: 0.066
#   pulse 13: activated nodes: 11451, borderline nodes: 30, overall activation: 6386.743, activation diff: 317.978, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 6386.7
#   number of spread. activ. pulses: 13
#   running time: 1432

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98229474   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9769869   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9732638   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9729939   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9711125   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95823187   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.762826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.76245797   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.76209366   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7615598   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7611862   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7606783   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.76053566   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7604647   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7602606   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.76015127   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.7600367   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.7600292   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.7599984   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.75979763   REFERENCES:SIMLOC
