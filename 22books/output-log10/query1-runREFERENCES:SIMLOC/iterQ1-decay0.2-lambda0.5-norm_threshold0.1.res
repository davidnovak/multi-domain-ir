###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.215, activation diff: 10.215, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 341.135, activation diff: 332.058, ratio: 0.973
#   pulse 3: activated nodes: 8834, borderline nodes: 4236, overall activation: 624.806, activation diff: 284.734, ratio: 0.456
#   pulse 4: activated nodes: 11180, borderline nodes: 5066, overall activation: 2412.232, activation diff: 1787.425, ratio: 0.741
#   pulse 5: activated nodes: 11383, borderline nodes: 1148, overall activation: 4096.932, activation diff: 1684.701, ratio: 0.411
#   pulse 6: activated nodes: 11432, borderline nodes: 272, overall activation: 5429.420, activation diff: 1332.487, ratio: 0.245
#   pulse 7: activated nodes: 11447, borderline nodes: 64, overall activation: 6307.207, activation diff: 877.787, ratio: 0.139
#   pulse 8: activated nodes: 11450, borderline nodes: 40, overall activation: 6835.114, activation diff: 527.907, ratio: 0.077
#   pulse 9: activated nodes: 11450, borderline nodes: 27, overall activation: 7140.277, activation diff: 305.164, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 7140.3
#   number of spread. activ. pulses: 9
#   running time: 1252

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99583304   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9945364   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99368095   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99335694   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9923593   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98842275   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7934538   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.79335463   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.79317164   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7930844   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.79305017   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.79291695   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7926566   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.7926471   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7925866   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.7925631   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.7925242   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7925105   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   19   0.79250157   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.79248524   REFERENCES:SIMLOC
