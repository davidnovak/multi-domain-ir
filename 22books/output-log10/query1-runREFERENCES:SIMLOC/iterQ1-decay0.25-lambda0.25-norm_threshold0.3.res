###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.822, activation diff: 10.822, ratio: 1.384
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 330.485, activation diff: 331.999, ratio: 1.005
#   pulse 3: activated nodes: 8533, borderline nodes: 5690, overall activation: 216.669, activation diff: 346.940, ratio: 1.601
#   pulse 4: activated nodes: 10828, borderline nodes: 7331, overall activation: 2702.161, activation diff: 2583.776, ratio: 0.956
#   pulse 5: activated nodes: 11115, borderline nodes: 3199, overall activation: 3846.207, activation diff: 1254.859, ratio: 0.326
#   pulse 6: activated nodes: 11348, borderline nodes: 1607, overall activation: 5301.402, activation diff: 1455.330, ratio: 0.275
#   pulse 7: activated nodes: 11417, borderline nodes: 370, overall activation: 6044.382, activation diff: 742.980, ratio: 0.123
#   pulse 8: activated nodes: 11424, borderline nodes: 196, overall activation: 6359.152, activation diff: 314.770, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11424
#   final overall activation: 6359.2
#   number of spread. activ. pulses: 8
#   running time: 1365

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99929726   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9992136   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99903595   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9988196   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99793196   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.995945   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7493921   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.7493917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74938846   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7493695   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.74936324   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   12   0.7493598   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7493594   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   14   0.7493583   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7493514   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   16   0.74934065   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.7493403   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   18   0.74933827   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.7493361   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   20   0.74933237   REFERENCES:SIMLOC
