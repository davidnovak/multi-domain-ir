###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.607, activation diff: 3.607, ratio: 0.546
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 14.069, activation diff: 9.316, ratio: 0.662
#   pulse 3: activated nodes: 7789, borderline nodes: 6924, overall activation: 40.646, activation diff: 27.753, ratio: 0.683
#   pulse 4: activated nodes: 8063, borderline nodes: 6494, overall activation: 153.064, activation diff: 112.752, ratio: 0.737
#   pulse 5: activated nodes: 9231, borderline nodes: 6449, overall activation: 436.906, activation diff: 283.873, ratio: 0.650
#   pulse 6: activated nodes: 10563, borderline nodes: 6146, overall activation: 995.535, activation diff: 558.630, ratio: 0.561
#   pulse 7: activated nodes: 11124, borderline nodes: 4474, overall activation: 1822.777, activation diff: 827.241, ratio: 0.454
#   pulse 8: activated nodes: 11280, borderline nodes: 2698, overall activation: 2793.480, activation diff: 970.704, ratio: 0.347
#   pulse 9: activated nodes: 11359, borderline nodes: 1337, overall activation: 3806.347, activation diff: 1012.867, ratio: 0.266
#   pulse 10: activated nodes: 11409, borderline nodes: 611, overall activation: 4763.418, activation diff: 957.070, ratio: 0.201
#   pulse 11: activated nodes: 11431, borderline nodes: 294, overall activation: 5611.219, activation diff: 847.801, ratio: 0.151
#   pulse 12: activated nodes: 11435, borderline nodes: 143, overall activation: 6331.468, activation diff: 720.249, ratio: 0.114
#   pulse 13: activated nodes: 11439, borderline nodes: 82, overall activation: 6925.775, activation diff: 594.308, ratio: 0.086
#   pulse 14: activated nodes: 11444, borderline nodes: 51, overall activation: 7407.318, activation diff: 481.542, ratio: 0.065
#   pulse 15: activated nodes: 11447, borderline nodes: 38, overall activation: 7793.022, activation diff: 385.705, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 7793.0
#   number of spread. activ. pulses: 15
#   running time: 1463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9804604   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9748179   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9742062   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9722182   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9615675   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95151025   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.86791056   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.8675546   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8675411   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.8675127   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.86745405   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.8671873   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.8670113   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8667376   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   15   0.86647224   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.86640096   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   17   0.8663889   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   18   0.8663594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.8660719   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.866042   REFERENCES:SIMLOC
