###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.835, activation diff: 5.835, ratio: 0.660
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 154.410, activation diff: 146.446, ratio: 0.948
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 393.250, activation diff: 239.006, ratio: 0.608
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1205.928, activation diff: 812.679, ratio: 0.674
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2333.503, activation diff: 1127.575, ratio: 0.483
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3557.941, activation diff: 1224.438, ratio: 0.344
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 4696.989, activation diff: 1139.049, ratio: 0.243
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 5676.768, activation diff: 979.779, ratio: 0.173
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 6483.554, activation diff: 806.786, ratio: 0.124
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 7132.078, activation diff: 648.524, ratio: 0.091
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 7646.143, activation diff: 514.065, ratio: 0.067
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 8050.141, activation diff: 403.998, ratio: 0.050
#   pulse 13: activated nodes: 11457, borderline nodes: 0, overall activation: 8365.908, activation diff: 315.767, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8365.9
#   number of spread. activ. pulses: 13
#   running time: 1517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98528576   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9809445   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9794476   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9775392   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9749317   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96626186   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.86132884   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.86083645   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.86019564   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.85968685   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8593371   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.85926306   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.85886455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8588593   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.85867953   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.85865915   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.85846055   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.8583412   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.8582965   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   20   0.85815454   REFERENCES:SIMLOC
