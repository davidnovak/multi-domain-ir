###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.835, activation diff: 5.835, ratio: 0.660
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 90.528, activation diff: 82.564, ratio: 0.912
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 205.896, activation diff: 115.534, ratio: 0.561
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 483.094, activation diff: 277.198, ratio: 0.574
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 832.339, activation diff: 349.245, ratio: 0.420
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1199.954, activation diff: 367.615, ratio: 0.306
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1551.228, activation diff: 351.274, ratio: 0.226
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 1868.604, activation diff: 317.376, ratio: 0.170
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2145.713, activation diff: 277.109, ratio: 0.129
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 2381.793, activation diff: 236.080, ratio: 0.099
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 2579.121, activation diff: 197.328, ratio: 0.077
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 2741.645, activation diff: 162.524, ratio: 0.059
#   pulse 13: activated nodes: 10899, borderline nodes: 3506, overall activation: 2874.021, activation diff: 132.376, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2874.0
#   number of spread. activ. pulses: 13
#   running time: 1384

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9851973   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9804149   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97833157   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97663665   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9740702   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9636699   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4779908   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.4778761   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.47741735   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.4772047   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.47715718   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.4770041   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.47677788   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.47646758   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   15   0.47642517   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.47641402   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.47634807   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   18   0.47622734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.4761519   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.47611642   REFERENCES:SIMLOC
