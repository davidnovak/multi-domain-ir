###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.429, activation diff: 14.429, ratio: 1.712
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 444.916, activation diff: 453.345, ratio: 1.019
#   pulse 3: activated nodes: 8651, borderline nodes: 5429, overall activation: 105.384, activation diff: 550.300, ratio: 5.222
#   pulse 4: activated nodes: 10317, borderline nodes: 6760, overall activation: 2242.016, activation diff: 2336.945, ratio: 1.042
#   pulse 5: activated nodes: 10636, borderline nodes: 4570, overall activation: 366.980, activation diff: 2537.911, ratio: 6.916
#   pulse 6: activated nodes: 10794, borderline nodes: 4196, overall activation: 2704.028, activation diff: 2721.518, ratio: 1.006
#   pulse 7: activated nodes: 10867, borderline nodes: 3694, overall activation: 1912.947, activation diff: 1431.908, ratio: 0.749
#   pulse 8: activated nodes: 10871, borderline nodes: 3663, overall activation: 3137.576, activation diff: 1268.193, ratio: 0.404
#   pulse 9: activated nodes: 10879, borderline nodes: 3611, overall activation: 3227.096, activation diff: 100.846, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10879
#   final overall activation: 3227.1
#   number of spread. activ. pulses: 9
#   running time: 1471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999894   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998146   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998968   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9986127   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99771166   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4999998   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4999992   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4999988   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.49999788   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   11   0.49999675   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999672   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.49999672   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4999957   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999458   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999413   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999294   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.49999243   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.49999055   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49998987   REFERENCES:SIMLOC
