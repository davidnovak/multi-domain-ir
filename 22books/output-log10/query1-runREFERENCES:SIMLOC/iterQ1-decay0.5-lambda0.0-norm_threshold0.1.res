###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.430, activation diff: 20.430, ratio: 1.416
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 860.917, activation diff: 875.132, ratio: 1.017
#   pulse 3: activated nodes: 9154, borderline nodes: 3894, overall activation: 196.165, activation diff: 1055.328, ratio: 5.380
#   pulse 4: activated nodes: 10731, borderline nodes: 4538, overall activation: 2569.253, activation diff: 2655.052, ratio: 1.033
#   pulse 5: activated nodes: 10878, borderline nodes: 3639, overall activation: 1290.881, activation diff: 2135.710, ratio: 1.654
#   pulse 6: activated nodes: 10887, borderline nodes: 3535, overall activation: 3180.829, activation diff: 1968.025, ratio: 0.619
#   pulse 7: activated nodes: 10896, borderline nodes: 3516, overall activation: 3301.298, activation diff: 140.998, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 3301.3
#   number of spread. activ. pulses: 7
#   running time: 1878

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999005   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998236   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999139   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9986256   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99797094   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4999998   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49999928   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49999875   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4999978   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   11   0.49999702   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.49999693   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.49999693   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49999562   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.49999514   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4999949   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999362   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.49999362   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4999917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49999067   REFERENCES:SIMLOC
