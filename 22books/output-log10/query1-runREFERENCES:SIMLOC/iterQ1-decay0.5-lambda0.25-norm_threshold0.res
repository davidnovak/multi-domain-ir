###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.505, activation diff: 17.505, ratio: 1.207
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 667.956, activation diff: 665.326, ratio: 0.996
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 727.248, activation diff: 254.448, ratio: 0.350
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2161.905, activation diff: 1434.657, ratio: 0.664
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2845.646, activation diff: 683.741, ratio: 0.240
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 3170.461, activation diff: 324.816, ratio: 0.102
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 3307.651, activation diff: 137.190, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3307.7
#   number of spread. activ. pulses: 7
#   running time: 1209

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9992714   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99893856   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9986917   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99862933   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99762267   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99629426   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49935317   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.4992972   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49926296   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49924362   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.4992329   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.49921376   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49918222   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.499179   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.49917674   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.4991192   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.499119   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.49910915   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.49908158   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.49908072   REFERENCES:SIMLOC
