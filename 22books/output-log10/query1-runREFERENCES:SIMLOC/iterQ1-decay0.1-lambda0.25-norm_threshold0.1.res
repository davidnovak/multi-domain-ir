###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.323, activation diff: 15.323, ratio: 1.243
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 883.200, activation diff: 884.296, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 1031.002, activation diff: 826.590, ratio: 0.802
#   pulse 4: activated nodes: 11266, borderline nodes: 4092, overall activation: 5198.163, activation diff: 4226.652, ratio: 0.813
#   pulse 5: activated nodes: 11416, borderline nodes: 528, overall activation: 7584.679, activation diff: 2387.045, ratio: 0.315
#   pulse 6: activated nodes: 11444, borderline nodes: 68, overall activation: 8687.438, activation diff: 1102.760, ratio: 0.127
#   pulse 7: activated nodes: 11451, borderline nodes: 18, overall activation: 9086.507, activation diff: 399.069, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9086.5
#   number of spread. activ. pulses: 7
#   running time: 1304

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985195   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99829555   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9979946   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9979418   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9970796   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9955287   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8981805   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.8981342   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8981023   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.89806545   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.8980497   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.8980495   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   13   0.89798355   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.8979795   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.89797133   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.8979701   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.89793634   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.8979184   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   19   0.8978951   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.89788526   REFERENCES:SIMLOC
