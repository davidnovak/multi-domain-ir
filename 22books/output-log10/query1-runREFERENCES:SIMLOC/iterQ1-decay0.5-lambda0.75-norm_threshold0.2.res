###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.364, activation diff: 4.364, ratio: 0.593
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 24.643, activation diff: 18.729, ratio: 0.760
#   pulse 3: activated nodes: 8256, borderline nodes: 6241, overall activation: 60.953, activation diff: 36.969, ratio: 0.607
#   pulse 4: activated nodes: 9101, borderline nodes: 6371, overall activation: 188.674, activation diff: 127.769, ratio: 0.677
#   pulse 5: activated nodes: 9692, borderline nodes: 5378, overall activation: 407.436, activation diff: 218.761, ratio: 0.537
#   pulse 6: activated nodes: 10454, borderline nodes: 5209, overall activation: 691.499, activation diff: 284.064, ratio: 0.411
#   pulse 7: activated nodes: 10649, borderline nodes: 4809, overall activation: 999.950, activation diff: 308.451, ratio: 0.308
#   pulse 8: activated nodes: 10743, borderline nodes: 4477, overall activation: 1305.625, activation diff: 305.674, ratio: 0.234
#   pulse 9: activated nodes: 10781, borderline nodes: 4116, overall activation: 1594.742, activation diff: 289.117, ratio: 0.181
#   pulse 10: activated nodes: 10845, borderline nodes: 3803, overall activation: 1858.431, activation diff: 263.688, ratio: 0.142
#   pulse 11: activated nodes: 10864, borderline nodes: 3681, overall activation: 2092.843, activation diff: 234.412, ratio: 0.112
#   pulse 12: activated nodes: 10871, borderline nodes: 3622, overall activation: 2298.069, activation diff: 205.226, ratio: 0.089
#   pulse 13: activated nodes: 10881, borderline nodes: 3582, overall activation: 2475.499, activation diff: 177.430, ratio: 0.072
#   pulse 14: activated nodes: 10882, borderline nodes: 3560, overall activation: 2626.925, activation diff: 151.426, ratio: 0.058
#   pulse 15: activated nodes: 10887, borderline nodes: 3552, overall activation: 2754.575, activation diff: 127.650, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10887
#   final overall activation: 2754.6
#   number of spread. activ. pulses: 15
#   running time: 1485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98668855   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98200345   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9792261   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9788689   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.974812   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.963053   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   7   0.4846191   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.48457506   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.48450422   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.48441654   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.4844155   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   12   0.48433048   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.48387173   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   14   0.48382977   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.48382652   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   16   0.48375773   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.48372227   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.48370805   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.48367977   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.48360977   REFERENCES:SIMLOC
