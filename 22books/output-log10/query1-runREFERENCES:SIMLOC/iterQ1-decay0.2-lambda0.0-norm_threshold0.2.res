###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.456, activation diff: 17.456, ratio: 1.524
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1011.683, activation diff: 1023.139, ratio: 1.011
#   pulse 3: activated nodes: 8764, borderline nodes: 4752, overall activation: 519.577, activation diff: 1531.260, ratio: 2.947
#   pulse 4: activated nodes: 11189, borderline nodes: 5487, overall activation: 5197.990, activation diff: 5522.572, ratio: 1.062
#   pulse 5: activated nodes: 11395, borderline nodes: 886, overall activation: 3832.744, activation diff: 4425.457, ratio: 1.155
#   pulse 6: activated nodes: 11433, borderline nodes: 165, overall activation: 7128.456, activation diff: 3586.480, ratio: 0.503
#   pulse 7: activated nodes: 11448, borderline nodes: 52, overall activation: 7398.262, activation diff: 304.283, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 7398.3
#   number of spread. activ. pulses: 7
#   running time: 1304

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999892   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990845   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9988953   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9979812   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.8   REFERENCES:SIMLOC
