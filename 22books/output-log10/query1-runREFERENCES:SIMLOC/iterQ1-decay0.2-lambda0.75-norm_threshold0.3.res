###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.607, activation diff: 3.607, ratio: 0.546
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 13.203, activation diff: 8.449, ratio: 0.640
#   pulse 3: activated nodes: 7789, borderline nodes: 6924, overall activation: 36.770, activation diff: 24.759, ratio: 0.673
#   pulse 4: activated nodes: 8058, borderline nodes: 6490, overall activation: 130.205, activation diff: 93.800, ratio: 0.720
#   pulse 5: activated nodes: 9177, borderline nodes: 6476, overall activation: 364.445, activation diff: 234.285, ratio: 0.643
#   pulse 6: activated nodes: 10513, borderline nodes: 6322, overall activation: 805.324, activation diff: 440.879, ratio: 0.547
#   pulse 7: activated nodes: 11092, borderline nodes: 4811, overall activation: 1444.286, activation diff: 638.962, ratio: 0.442
#   pulse 8: activated nodes: 11233, borderline nodes: 3128, overall activation: 2191.348, activation diff: 747.062, ratio: 0.341
#   pulse 9: activated nodes: 11333, borderline nodes: 1836, overall activation: 2968.801, activation diff: 777.452, ratio: 0.262
#   pulse 10: activated nodes: 11400, borderline nodes: 926, overall activation: 3718.786, activation diff: 749.985, ratio: 0.202
#   pulse 11: activated nodes: 11417, borderline nodes: 498, overall activation: 4397.379, activation diff: 678.593, ratio: 0.154
#   pulse 12: activated nodes: 11428, borderline nodes: 294, overall activation: 4985.995, activation diff: 588.616, ratio: 0.118
#   pulse 13: activated nodes: 11433, borderline nodes: 174, overall activation: 5481.174, activation diff: 495.180, ratio: 0.090
#   pulse 14: activated nodes: 11436, borderline nodes: 123, overall activation: 5888.650, activation diff: 407.476, ratio: 0.069
#   pulse 15: activated nodes: 11439, borderline nodes: 85, overall activation: 6219.137, activation diff: 330.487, ratio: 0.053

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 6219.1
#   number of spread. activ. pulses: 15
#   running time: 1523

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97997844   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9741014   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.97358054   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97138697   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.95990765   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9496649   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.77112186   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.7707025   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7706813   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7705775   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.77050674   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.7702932   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.77009946   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.76983964   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   15   0.76961523   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   16   0.7695858   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   17   0.769549   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   18   0.76947355   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   19   0.7692423   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.76923054   REFERENCES:SIMLOC
