###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.670, activation diff: 11.670, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 474.463, activation diff: 462.793, ratio: 0.975
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 932.406, activation diff: 457.942, ratio: 0.491
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2740.902, activation diff: 1808.496, ratio: 0.660
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4322.099, activation diff: 1581.197, ratio: 0.366
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 5429.707, activation diff: 1107.608, ratio: 0.204
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6110.590, activation diff: 680.883, ratio: 0.111
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 6506.513, activation diff: 395.923, ratio: 0.061
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 6731.507, activation diff: 224.993, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6731.5
#   number of spread. activ. pulses: 9
#   running time: 1319

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969325   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957665   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9951788   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.994753   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99357843   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99053586   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7449449   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74483645   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7445808   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.74455976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.7444842   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.74424535   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7442232   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7441891   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.74415565   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.74415374   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.74408615   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.7440559   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.74404025   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.7439418   REFERENCES:SIMLOC
