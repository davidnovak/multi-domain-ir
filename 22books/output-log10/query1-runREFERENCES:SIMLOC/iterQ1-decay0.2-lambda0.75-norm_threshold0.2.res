###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.364, activation diff: 4.364, ratio: 0.593
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 34.780, activation diff: 28.866, ratio: 0.830
#   pulse 3: activated nodes: 8256, borderline nodes: 6241, overall activation: 92.802, activation diff: 58.681, ratio: 0.632
#   pulse 4: activated nodes: 9647, borderline nodes: 6884, overall activation: 347.518, activation diff: 254.727, ratio: 0.733
#   pulse 5: activated nodes: 10584, borderline nodes: 5524, overall activation: 819.247, activation diff: 471.729, ratio: 0.576
#   pulse 6: activated nodes: 11154, borderline nodes: 4216, overall activation: 1512.481, activation diff: 693.234, ratio: 0.458
#   pulse 7: activated nodes: 11301, borderline nodes: 2386, overall activation: 2312.391, activation diff: 799.910, ratio: 0.346
#   pulse 8: activated nodes: 11396, borderline nodes: 1044, overall activation: 3132.398, activation diff: 820.008, ratio: 0.262
#   pulse 9: activated nodes: 11417, borderline nodes: 466, overall activation: 3903.945, activation diff: 771.547, ratio: 0.198
#   pulse 10: activated nodes: 11431, borderline nodes: 253, overall activation: 4586.994, activation diff: 683.049, ratio: 0.149
#   pulse 11: activated nodes: 11436, borderline nodes: 127, overall activation: 5167.815, activation diff: 580.820, ratio: 0.112
#   pulse 12: activated nodes: 11439, borderline nodes: 82, overall activation: 5648.010, activation diff: 480.195, ratio: 0.085
#   pulse 13: activated nodes: 11444, borderline nodes: 60, overall activation: 6037.808, activation diff: 389.798, ratio: 0.065
#   pulse 14: activated nodes: 11449, borderline nodes: 46, overall activation: 6350.580, activation diff: 312.772, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6350.6
#   number of spread. activ. pulses: 14
#   running time: 1561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9830054   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97757995   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.974336   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9739519   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9701711   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9572502   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.7685853   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7684964   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.76844084   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.76822543   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.76799697   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   12   0.76737815   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.76729333   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.76724505   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.767196   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.76706064   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7669571   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.7667612   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   19   0.7666398   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.7666185   REFERENCES:SIMLOC
