###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.214, activation diff: 7.214, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 115.854, activation diff: 111.925, ratio: 0.966
#   pulse 3: activated nodes: 8324, borderline nodes: 6133, overall activation: 166.974, activation diff: 57.902, ratio: 0.347
#   pulse 4: activated nodes: 10246, borderline nodes: 7837, overall activation: 1313.104, activation diff: 1146.253, ratio: 0.873
#   pulse 5: activated nodes: 10707, borderline nodes: 4548, overall activation: 2533.078, activation diff: 1219.974, ratio: 0.482
#   pulse 6: activated nodes: 11291, borderline nodes: 3134, overall activation: 4015.437, activation diff: 1482.358, ratio: 0.369
#   pulse 7: activated nodes: 11402, borderline nodes: 793, overall activation: 5238.541, activation diff: 1223.105, ratio: 0.233
#   pulse 8: activated nodes: 11427, borderline nodes: 298, overall activation: 6095.896, activation diff: 857.354, ratio: 0.141
#   pulse 9: activated nodes: 11435, borderline nodes: 137, overall activation: 6639.172, activation diff: 543.276, ratio: 0.082
#   pulse 10: activated nodes: 11440, borderline nodes: 90, overall activation: 6966.044, activation diff: 326.873, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6966.0
#   number of spread. activ. pulses: 10
#   running time: 1357

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9956996   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99498963   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9941576   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9936248   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9925359   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98788655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7951661   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7951471   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.7951323   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7950797   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.79507935   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.795028   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7949482   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79493165   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.79488134   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7948411   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.7948326   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.7948103   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   19   0.7947841   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   20   0.7947764   REFERENCES:SIMLOC
