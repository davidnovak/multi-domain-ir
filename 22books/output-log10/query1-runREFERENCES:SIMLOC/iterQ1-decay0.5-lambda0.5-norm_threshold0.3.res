###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.214, activation diff: 7.214, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 74.507, activation diff: 70.579, ratio: 0.947
#   pulse 3: activated nodes: 8324, borderline nodes: 6133, overall activation: 102.669, activation diff: 32.861, ratio: 0.320
#   pulse 4: activated nodes: 9168, borderline nodes: 6921, overall activation: 648.508, activation diff: 546.031, ratio: 0.842
#   pulse 5: activated nodes: 9796, borderline nodes: 4841, overall activation: 1170.333, activation diff: 521.826, ratio: 0.446
#   pulse 6: activated nodes: 10636, borderline nodes: 5002, overall activation: 1716.168, activation diff: 545.835, ratio: 0.318
#   pulse 7: activated nodes: 10759, borderline nodes: 4377, overall activation: 2160.193, activation diff: 444.024, ratio: 0.206
#   pulse 8: activated nodes: 10838, borderline nodes: 3867, overall activation: 2499.916, activation diff: 339.724, ratio: 0.136
#   pulse 9: activated nodes: 10864, borderline nodes: 3691, overall activation: 2751.126, activation diff: 251.210, ratio: 0.091
#   pulse 10: activated nodes: 10874, borderline nodes: 3624, overall activation: 2931.714, activation diff: 180.588, ratio: 0.062
#   pulse 11: activated nodes: 10878, borderline nodes: 3591, overall activation: 3056.188, activation diff: 124.474, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10878
#   final overall activation: 3056.2
#   number of spread. activ. pulses: 11
#   running time: 1462

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9978336   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99728644   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99674773   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9959356   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9950135   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9915838   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.49844816   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49843848   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.49842685   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   10   0.498416   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.49840564   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49835116   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.4983195   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   14   0.4982983   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.49828625   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49826694   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.49826187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.49824825   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.49823743   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   20   0.49823448   REFERENCES:SIMLOC
