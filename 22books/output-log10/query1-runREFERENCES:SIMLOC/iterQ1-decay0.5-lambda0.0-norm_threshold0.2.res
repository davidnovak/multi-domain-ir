###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.456, activation diff: 17.456, ratio: 1.524
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 634.256, activation diff: 645.712, ratio: 1.018
#   pulse 3: activated nodes: 8764, borderline nodes: 4752, overall activation: 147.662, activation diff: 781.919, ratio: 5.295
#   pulse 4: activated nodes: 10527, borderline nodes: 5979, overall activation: 2402.240, activation diff: 2510.256, ratio: 1.045
#   pulse 5: activated nodes: 10820, borderline nodes: 4020, overall activation: 624.079, activation diff: 2553.224, ratio: 4.091
#   pulse 6: activated nodes: 10861, borderline nodes: 3632, overall activation: 2938.216, activation diff: 2548.553, ratio: 0.867
#   pulse 7: activated nodes: 10887, borderline nodes: 3556, overall activation: 3071.505, activation diff: 365.291, ratio: 0.119
#   pulse 8: activated nodes: 10888, borderline nodes: 3540, overall activation: 3277.908, activation diff: 207.945, ratio: 0.063
#   pulse 9: activated nodes: 10888, borderline nodes: 3532, overall activation: 3311.862, activation diff: 34.191, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10888
#   final overall activation: 3311.9
#   number of spread. activ. pulses: 9
#   running time: 1332

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998844   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999075   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99890655   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99798024   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999982   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49999946   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49999905   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4999983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4999973   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4999973   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999714   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49999672   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.4999957   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4999953   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.49999446   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.4999932   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.49999237   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.49999136   REFERENCES:SIMLOC
