###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.092, activation diff: 13.092, ratio: 1.297
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 512.944, activation diff: 515.151, ratio: 1.004
#   pulse 3: activated nodes: 8729, borderline nodes: 5086, overall activation: 437.338, activation diff: 496.199, ratio: 1.135
#   pulse 4: activated nodes: 11120, borderline nodes: 6222, overall activation: 3171.669, activation diff: 2829.823, ratio: 0.892
#   pulse 5: activated nodes: 11312, borderline nodes: 1686, overall activation: 4696.967, activation diff: 1535.292, ratio: 0.327
#   pulse 6: activated nodes: 11407, borderline nodes: 507, overall activation: 5808.735, activation diff: 1111.774, ratio: 0.191
#   pulse 7: activated nodes: 11429, borderline nodes: 158, overall activation: 6313.214, activation diff: 504.479, ratio: 0.080
#   pulse 8: activated nodes: 11436, borderline nodes: 104, overall activation: 6510.940, activation diff: 197.726, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 6510.9
#   number of spread. activ. pulses: 8
#   running time: 1315

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994484   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993815   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9993128   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992324   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982655   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9969501   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74949217   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7494911   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74948186   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.7494627   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.74946237   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.74946153   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7494532   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7494497   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7494482   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   16   0.74944466   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.74943733   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.74943286   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.7494293   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.74942493   REFERENCES:SIMLOC
