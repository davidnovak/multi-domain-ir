###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.728, activation diff: 8.728, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 238.703, activation diff: 232.742, ratio: 0.975
#   pulse 3: activated nodes: 8632, borderline nodes: 5384, overall activation: 404.743, activation diff: 170.768, ratio: 0.422
#   pulse 4: activated nodes: 10941, borderline nodes: 6913, overall activation: 2310.168, activation diff: 1905.425, ratio: 0.825
#   pulse 5: activated nodes: 11219, borderline nodes: 2800, overall activation: 4238.060, activation diff: 1927.892, ratio: 0.455
#   pulse 6: activated nodes: 11409, borderline nodes: 912, overall activation: 6064.504, activation diff: 1826.445, ratio: 0.301
#   pulse 7: activated nodes: 11436, borderline nodes: 164, overall activation: 7356.080, activation diff: 1291.575, ratio: 0.176
#   pulse 8: activated nodes: 11448, borderline nodes: 49, overall activation: 8157.317, activation diff: 801.238, ratio: 0.098
#   pulse 9: activated nodes: 11451, borderline nodes: 25, overall activation: 8624.430, activation diff: 467.113, ratio: 0.054
#   pulse 10: activated nodes: 11451, borderline nodes: 16, overall activation: 8890.120, activation diff: 265.690, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 8890.1
#   number of spread. activ. pulses: 10
#   running time: 1507

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99692094   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99634874   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99568856   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9956616   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9948329   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9915395   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89547455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89542353   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8953792   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.89536405   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.89532787   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.895296   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.89513093   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8951212   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.89507955   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.8950775   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   17   0.89503515   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   18   0.89501774   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   19   0.89501554   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.89501256   REFERENCES:SIMLOC
