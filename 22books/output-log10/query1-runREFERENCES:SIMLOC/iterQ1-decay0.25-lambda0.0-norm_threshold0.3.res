###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.429, activation diff: 14.429, ratio: 1.712
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 665.033, activation diff: 673.462, ratio: 1.013
#   pulse 3: activated nodes: 8651, borderline nodes: 5429, overall activation: 292.651, activation diff: 957.684, ratio: 3.272
#   pulse 4: activated nodes: 11070, borderline nodes: 6752, overall activation: 4203.022, activation diff: 4444.952, ratio: 1.058
#   pulse 5: activated nodes: 11287, borderline nodes: 1990, overall activation: 1628.153, activation diff: 5007.756, ratio: 3.076
#   pulse 6: activated nodes: 11403, borderline nodes: 602, overall activation: 5694.299, activation diff: 4991.851, ratio: 0.877
#   pulse 7: activated nodes: 11429, borderline nodes: 237, overall activation: 6261.905, activation diff: 844.304, ratio: 0.135
#   pulse 8: activated nodes: 11430, borderline nodes: 166, overall activation: 6512.083, activation diff: 260.567, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11430
#   final overall activation: 6512.1
#   number of spread. activ. pulses: 8
#   running time: 1379

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998844   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989885   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9988325   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777406   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.75   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.75   REFERENCES:SIMLOC
