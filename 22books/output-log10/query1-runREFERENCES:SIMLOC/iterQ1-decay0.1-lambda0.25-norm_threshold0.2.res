###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.092, activation diff: 13.092, ratio: 1.297
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 614.289, activation diff: 616.495, ratio: 1.004
#   pulse 3: activated nodes: 8729, borderline nodes: 5086, overall activation: 593.235, activation diff: 664.449, ratio: 1.120
#   pulse 4: activated nodes: 11136, borderline nodes: 6117, overall activation: 4455.449, activation diff: 4007.119, ratio: 0.899
#   pulse 5: activated nodes: 11360, borderline nodes: 1336, overall activation: 6779.327, activation diff: 2338.533, ratio: 0.345
#   pulse 6: activated nodes: 11431, borderline nodes: 251, overall activation: 8288.906, activation diff: 1509.579, ratio: 0.182
#   pulse 7: activated nodes: 11448, borderline nodes: 44, overall activation: 8896.333, activation diff: 607.427, ratio: 0.068
#   pulse 8: activated nodes: 11451, borderline nodes: 19, overall activation: 9115.603, activation diff: 219.270, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9115.6
#   number of spread. activ. pulses: 8
#   running time: 1379

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994484   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993836   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9993168   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992377   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99828064   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9970105   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8993906   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89939   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.8993783   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.89935917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89935684   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.899356   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8993543   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.89934176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.89934075   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.8993402   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   17   0.89933485   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   18   0.89933443   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   19   0.8993342   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.8993331   REFERENCES:SIMLOC
