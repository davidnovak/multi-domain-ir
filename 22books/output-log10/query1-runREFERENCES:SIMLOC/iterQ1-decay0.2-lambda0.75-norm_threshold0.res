###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.835, activation diff: 5.835, ratio: 0.660
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 138.439, activation diff: 130.476, ratio: 0.942
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 343.826, activation diff: 205.553, ratio: 0.598
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 989.160, activation diff: 645.334, ratio: 0.652
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1875.508, activation diff: 886.347, ratio: 0.473
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 2847.117, activation diff: 971.609, ratio: 0.341
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 3768.061, activation diff: 920.944, ratio: 0.244
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 4574.009, activation diff: 805.947, ratio: 0.176
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 5247.610, activation diff: 673.602, ratio: 0.128
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 5795.484, activation diff: 547.874, ratio: 0.095
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 6233.977, activation diff: 438.493, ratio: 0.070
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 6581.431, activation diff: 347.454, ratio: 0.053
#   pulse 13: activated nodes: 11457, borderline nodes: 2, overall activation: 6854.958, activation diff: 273.528, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6855.0
#   number of spread. activ. pulses: 13
#   running time: 1587

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9852724   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9808557   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97927016   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9773852   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9747912   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96584827   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.76553404   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.76509076   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7644985   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.76411414   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7637761   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.76369   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.76332897   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.76324975   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.76307863   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.76303834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.76288724   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.7627454   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   19   0.762711   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.762626   REFERENCES:SIMLOC
