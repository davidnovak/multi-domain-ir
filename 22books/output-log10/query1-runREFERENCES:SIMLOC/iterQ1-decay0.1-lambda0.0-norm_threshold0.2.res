###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.456, activation diff: 17.456, ratio: 1.524
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1137.492, activation diff: 1148.947, ratio: 1.010
#   pulse 3: activated nodes: 8764, borderline nodes: 4752, overall activation: 679.136, activation diff: 1816.628, ratio: 2.675
#   pulse 4: activated nodes: 11201, borderline nodes: 5412, overall activation: 6485.857, activation diff: 6884.188, ratio: 1.061
#   pulse 5: activated nodes: 11408, borderline nodes: 786, overall activation: 5561.981, activation diff: 4842.856, ratio: 0.871
#   pulse 6: activated nodes: 11439, borderline nodes: 105, overall activation: 8930.309, activation diff: 3599.633, ratio: 0.403
#   pulse 7: activated nodes: 11451, borderline nodes: 25, overall activation: 9172.090, activation diff: 270.760, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9172.1
#   number of spread. activ. pulses: 7
#   running time: 1467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999017   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999085   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9989361   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99798536   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   7   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   14   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   15   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   16   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   18   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   19   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_540   20   0.9   REFERENCES:SIMLOC
