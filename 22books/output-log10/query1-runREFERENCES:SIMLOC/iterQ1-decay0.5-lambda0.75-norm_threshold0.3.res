###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.607, activation diff: 3.607, ratio: 0.546
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 10.602, activation diff: 5.849, ratio: 0.552
#   pulse 3: activated nodes: 7789, borderline nodes: 6924, overall activation: 25.116, activation diff: 15.763, ratio: 0.628
#   pulse 4: activated nodes: 8058, borderline nodes: 6490, overall activation: 69.549, activation diff: 44.928, ratio: 0.646
#   pulse 5: activated nodes: 8928, borderline nodes: 6491, overall activation: 181.248, activation diff: 111.820, ratio: 0.617
#   pulse 6: activated nodes: 9448, borderline nodes: 5936, overall activation: 374.741, activation diff: 193.498, ratio: 0.516
#   pulse 7: activated nodes: 10054, borderline nodes: 5426, overall activation: 630.483, activation diff: 255.742, ratio: 0.406
#   pulse 8: activated nodes: 10514, borderline nodes: 5142, overall activation: 914.788, activation diff: 284.306, ratio: 0.311
#   pulse 9: activated nodes: 10657, borderline nodes: 4788, overall activation: 1201.478, activation diff: 286.689, ratio: 0.239
#   pulse 10: activated nodes: 10730, borderline nodes: 4516, overall activation: 1475.367, activation diff: 273.889, ratio: 0.186
#   pulse 11: activated nodes: 10767, borderline nodes: 4268, overall activation: 1729.145, activation diff: 253.778, ratio: 0.147
#   pulse 12: activated nodes: 10803, borderline nodes: 3971, overall activation: 1958.083, activation diff: 228.939, ratio: 0.117
#   pulse 13: activated nodes: 10847, borderline nodes: 3798, overall activation: 2160.868, activation diff: 202.784, ratio: 0.094
#   pulse 14: activated nodes: 10857, borderline nodes: 3721, overall activation: 2338.752, activation diff: 177.884, ratio: 0.076
#   pulse 15: activated nodes: 10863, borderline nodes: 3676, overall activation: 2493.583, activation diff: 154.831, ratio: 0.062

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10863
#   final overall activation: 2493.6
#   number of spread. activ. pulses: 15
#   running time: 1548

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9776677   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97055185   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.9704469   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9672556   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.951028   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.93977493   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.4809081   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.48034292   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.48030037   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   10   0.4798899   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.47988242   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.4798344   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.4795819   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   14   0.47956356   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   15   0.4794504   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.47920993   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   17   0.479191   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_242   18   0.47908106   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   19   0.47904587   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   20   0.4790451   REFERENCES:SIMLOC
