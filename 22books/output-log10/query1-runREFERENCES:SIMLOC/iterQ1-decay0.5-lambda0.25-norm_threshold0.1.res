###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.323, activation diff: 15.323, ratio: 1.243
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 494.332, activation diff: 495.428, ratio: 1.002
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 409.663, activation diff: 293.947, ratio: 0.718
#   pulse 4: activated nodes: 10672, borderline nodes: 4891, overall activation: 1864.361, activation diff: 1459.874, ratio: 0.783
#   pulse 5: activated nodes: 10858, borderline nodes: 3757, overall activation: 2560.259, activation diff: 695.897, ratio: 0.272
#   pulse 6: activated nodes: 10885, borderline nodes: 3555, overall activation: 2983.336, activation diff: 423.077, ratio: 0.142
#   pulse 7: activated nodes: 10890, borderline nodes: 3524, overall activation: 3197.052, activation diff: 213.716, ratio: 0.067
#   pulse 8: activated nodes: 10896, borderline nodes: 3516, overall activation: 3293.604, activation diff: 96.552, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 3293.6
#   number of spread. activ. pulses: 8
#   running time: 1267

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99962986   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99956316   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9994775   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994256   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99848855   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99739754   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49974698   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49973363   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.4997205   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.499712   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.49971145   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.49970996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.4997089   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.4996916   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.49969062   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.49968928   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.499681   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   18   0.49968022   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.4996755   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.49966222   REFERENCES:SIMLOC
