###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.670, activation diff: 11.670, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 320.558, activation diff: 308.887, ratio: 0.964
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 558.052, activation diff: 237.494, ratio: 0.426
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1366.815, activation diff: 808.763, ratio: 0.592
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2046.713, activation diff: 679.898, ratio: 0.332
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 2543.226, activation diff: 496.513, ratio: 0.195
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 2875.953, activation diff: 332.727, ratio: 0.116
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 3086.033, activation diff: 210.080, ratio: 0.068
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 3213.793, activation diff: 127.760, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3213.8
#   number of spread. activ. pulses: 9
#   running time: 1252

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969322   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99575067   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99516237   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99470097   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99346006   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9901933   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49661618   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49654388   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49636966   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49636418   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.49627957   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.49611157   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.49610618   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.49608964   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.49606103   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.49603164   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   17   0.49601978   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.49601254   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.49593326   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.49589074   REFERENCES:SIMLOC
