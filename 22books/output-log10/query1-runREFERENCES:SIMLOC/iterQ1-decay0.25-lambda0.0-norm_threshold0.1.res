###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.430, activation diff: 20.430, ratio: 1.416
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1288.562, activation diff: 1302.776, ratio: 1.011
#   pulse 3: activated nodes: 9154, borderline nodes: 3894, overall activation: 612.934, activation diff: 1898.447, ratio: 3.097
#   pulse 4: activated nodes: 11282, borderline nodes: 3614, overall activation: 4999.091, activation diff: 5264.011, ratio: 1.053
#   pulse 5: activated nodes: 11420, borderline nodes: 390, overall activation: 4448.713, activation diff: 3115.298, ratio: 0.700
#   pulse 6: activated nodes: 11434, borderline nodes: 100, overall activation: 6547.974, activation diff: 2218.998, ratio: 0.339
#   pulse 7: activated nodes: 11442, borderline nodes: 67, overall activation: 6685.871, activation diff: 150.414, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 6685.9
#   number of spread. activ. pulses: 7
#   running time: 1270

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.999991   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991715   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99903184   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981763   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   7   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   11   0.75   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   20   0.75   REFERENCES:SIMLOC
