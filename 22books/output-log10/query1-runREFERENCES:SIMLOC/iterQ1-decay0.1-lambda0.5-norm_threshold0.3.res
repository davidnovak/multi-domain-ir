###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.214, activation diff: 7.214, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 129.636, activation diff: 125.707, ratio: 0.970
#   pulse 3: activated nodes: 8324, borderline nodes: 6133, overall activation: 190.553, activation diff: 68.411, ratio: 0.359
#   pulse 4: activated nodes: 10285, borderline nodes: 7849, overall activation: 1605.441, activation diff: 1415.000, ratio: 0.881
#   pulse 5: activated nodes: 10756, borderline nodes: 4317, overall activation: 3147.887, activation diff: 1542.447, ratio: 0.490
#   pulse 6: activated nodes: 11308, borderline nodes: 2777, overall activation: 5080.888, activation diff: 1933.000, ratio: 0.380
#   pulse 7: activated nodes: 11411, borderline nodes: 541, overall activation: 6633.102, activation diff: 1552.215, ratio: 0.234
#   pulse 8: activated nodes: 11436, borderline nodes: 180, overall activation: 7676.437, activation diff: 1043.334, ratio: 0.136
#   pulse 9: activated nodes: 11445, borderline nodes: 61, overall activation: 8313.384, activation diff: 636.948, ratio: 0.077
#   pulse 10: activated nodes: 11449, borderline nodes: 30, overall activation: 8686.011, activation diff: 372.627, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 8686.0
#   number of spread. activ. pulses: 10
#   running time: 1277

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99570346   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99505675   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99425626   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.993695   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.9929631   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98827696   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.89457446   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8945553   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.8945451   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.89449   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.8944838   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   12   0.8944794   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.8943555   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.8943287   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   15   0.89430535   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_12   16   0.8942722   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.89422256   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.8942158   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.894209   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_60   20   0.89419115   REFERENCES:SIMLOC
