###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.430, activation diff: 20.430, ratio: 1.416
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1374.091, activation diff: 1388.305, ratio: 1.010
#   pulse 3: activated nodes: 9154, borderline nodes: 3894, overall activation: 712.942, activation diff: 2083.696, ratio: 2.923
#   pulse 4: activated nodes: 11288, borderline nodes: 3578, overall activation: 5648.387, activation diff: 5946.414, ratio: 1.053
#   pulse 5: activated nodes: 11426, borderline nodes: 359, overall activation: 5279.593, activation diff: 3287.144, ratio: 0.623
#   pulse 6: activated nodes: 11445, borderline nodes: 54, overall activation: 7398.236, activation diff: 2229.906, ratio: 0.301
#   pulse 7: activated nodes: 11453, borderline nodes: 30, overall activation: 7531.578, activation diff: 146.068, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 7531.6
#   number of spread. activ. pulses: 7
#   running time: 1299

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999912   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999172   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99904054   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.998177   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   20   0.8   REFERENCES:SIMLOC
