###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.670, activation diff: 11.670, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 566.807, activation diff: 555.136, ratio: 0.979
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1181.256, activation diff: 614.449, ratio: 0.520
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3767.104, activation diff: 2585.849, ratio: 0.686
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5989.761, activation diff: 2222.656, ratio: 0.371
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7474.702, activation diff: 1484.941, ratio: 0.199
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8351.934, activation diff: 877.232, ratio: 0.105
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 8847.114, activation diff: 495.180, ratio: 0.056
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 9121.769, activation diff: 274.655, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 9121.8
#   number of spread. activ. pulses: 9
#   running time: 1424

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99693257   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99577105   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9951823   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947681   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9936124   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9906421   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8939425   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.89381063   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.8935109   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.8934802   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.89338946   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.89318025   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.8930963   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.8930367   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.89300257   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.8929971   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.89292663   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.89288884   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.892879   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.89283776   REFERENCES:SIMLOC
