###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.340, activation diff: 23.340, ratio: 1.346
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1102.517, activation diff: 1118.355, ratio: 1.014
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 352.536, activation diff: 1300.736, ratio: 3.690
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2840.561, activation diff: 2681.389, ratio: 0.944
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 3037.909, activation diff: 527.806, ratio: 0.174
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 3348.328, activation diff: 311.589, ratio: 0.093
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 3386.380, activation diff: 38.189, ratio: 0.011

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3386.4
#   number of spread. activ. pulses: 7
#   running time: 1641

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999911   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999243   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99911183   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9983466   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49999985   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.49999958   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.49999923   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4999986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4999978   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4999978   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.49999768   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.49999732   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.4999965   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49999624   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4999955   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.49999443   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4999939   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.499993   REFERENCES:SIMLOC
