###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.092, activation diff: 13.092, ratio: 1.297
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 344.037, activation diff: 346.244, ratio: 1.006
#   pulse 3: activated nodes: 8729, borderline nodes: 5086, overall activation: 227.297, activation diff: 265.571, ratio: 1.168
#   pulse 4: activated nodes: 10433, borderline nodes: 6400, overall activation: 1642.481, activation diff: 1440.060, ratio: 0.877
#   pulse 5: activated nodes: 10682, borderline nodes: 4491, overall activation: 2248.106, activation diff: 610.754, ratio: 0.272
#   pulse 6: activated nodes: 10805, borderline nodes: 3914, overall activation: 2748.542, activation diff: 500.436, ratio: 0.182
#   pulse 7: activated nodes: 10877, borderline nodes: 3579, overall activation: 3038.901, activation diff: 290.359, ratio: 0.096
#   pulse 8: activated nodes: 10888, borderline nodes: 3537, overall activation: 3194.069, activation diff: 155.168, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10888
#   final overall activation: 3194.1
#   number of spread. activ. pulses: 8
#   running time: 1253

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994484   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993728   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99928725   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992087   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99818325   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9967537   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49966097   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49965775   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.499642   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.49963662   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.49963316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49963126   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.4996202   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.49962017   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.49961323   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.49961135   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.4996093   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.49960542   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.49959993   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.49959195   REFERENCES:SIMLOC
