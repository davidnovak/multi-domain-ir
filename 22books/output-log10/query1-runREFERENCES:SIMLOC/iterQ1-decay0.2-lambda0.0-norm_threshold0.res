###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.340, activation diff: 23.340, ratio: 1.346
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1760.156, activation diff: 1775.993, ratio: 1.009
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1141.563, activation diff: 2601.954, ratio: 2.279
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 6452.655, activation diff: 6100.213, ratio: 0.945
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 7424.191, activation diff: 1286.492, ratio: 0.173
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7776.267, activation diff: 358.944, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7776.3
#   number of spread. activ. pulses: 6
#   running time: 1264

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999183   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999251   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99913764   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9983506   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_532   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_503   20   0.8   REFERENCES:SIMLOC
