###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.670, activation diff: 11.670, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 505.244, activation diff: 493.574, ratio: 0.977
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1013.509, activation diff: 508.265, ratio: 0.501
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3069.035, activation diff: 2055.526, ratio: 0.670
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4860.843, activation diff: 1791.809, ratio: 0.369
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6095.923, activation diff: 1235.080, ratio: 0.203
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 6844.541, activation diff: 748.618, ratio: 0.109
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 7275.378, activation diff: 430.837, ratio: 0.059
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 7518.117, activation diff: 242.740, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 7518.1
#   number of spread. activ. pulses: 9
#   running time: 1335

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9969325   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99576825   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99518025   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9947587   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9935914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9905764   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7946105   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7944946   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.7942239   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7941997   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.79411936   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7938908   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.7938473   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.7938049   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.79377025   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.79376936   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.793699   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   18   0.79366636   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7936553   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   20   0.7935734   REFERENCES:SIMLOC
