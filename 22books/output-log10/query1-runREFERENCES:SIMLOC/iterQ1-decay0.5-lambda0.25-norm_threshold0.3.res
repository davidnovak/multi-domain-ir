###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.822, activation diff: 10.822, ratio: 1.384
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 221.959, activation diff: 223.473, ratio: 1.007
#   pulse 3: activated nodes: 8533, borderline nodes: 5690, overall activation: 116.115, activation diff: 202.625, ratio: 1.745
#   pulse 4: activated nodes: 9907, borderline nodes: 6881, overall activation: 1426.069, activation diff: 1345.870, ratio: 0.944
#   pulse 5: activated nodes: 10349, borderline nodes: 4773, overall activation: 1875.091, activation diff: 500.490, ratio: 0.267
#   pulse 6: activated nodes: 10751, borderline nodes: 4501, overall activation: 2496.306, activation diff: 621.231, ratio: 0.249
#   pulse 7: activated nodes: 10849, borderline nodes: 3760, overall activation: 2855.806, activation diff: 359.500, ratio: 0.126
#   pulse 8: activated nodes: 10871, borderline nodes: 3626, overall activation: 3067.505, activation diff: 211.699, ratio: 0.069
#   pulse 9: activated nodes: 10880, borderline nodes: 3584, overall activation: 3181.070, activation diff: 113.565, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10880
#   final overall activation: 3181.1
#   number of spread. activ. pulses: 9
#   running time: 1601

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99982417   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997788   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99965435   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99951386   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99851996   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9971068   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49989718   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49989486   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4998858   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.4998845   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49988374   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.4998832   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.49988315   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   14   0.49988008   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   15   0.49988008   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.49987927   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.4998787   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.49987826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.49987766   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.49987248   REFERENCES:SIMLOC
