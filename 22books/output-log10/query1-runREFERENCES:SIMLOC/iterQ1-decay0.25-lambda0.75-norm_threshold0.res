###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.835, activation diff: 5.835, ratio: 0.660
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 130.454, activation diff: 122.491, ratio: 0.939
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 319.743, activation diff: 189.455, ratio: 0.593
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 889.689, activation diff: 569.946, ratio: 0.641
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1666.119, activation diff: 776.430, ratio: 0.466
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 2517.462, activation diff: 851.342, ratio: 0.338
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 3331.641, activation diff: 814.179, ratio: 0.244
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 4050.420, activation diff: 718.780, ratio: 0.177
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 4655.993, activation diff: 605.572, ratio: 0.130
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 5151.779, activation diff: 495.786, ratio: 0.096
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 5550.676, activation diff: 398.898, ratio: 0.072
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 5868.157, activation diff: 317.481, ratio: 0.054
#   pulse 13: activated nodes: 11455, borderline nodes: 19, overall activation: 6119.059, activation diff: 250.902, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6119.1
#   number of spread. activ. pulses: 13
#   running time: 1519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9852643   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9808034   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9791636   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.97729534   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9747077   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9656025   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7176264   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.7172184   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7166388   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.716329   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7159993   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.71590483   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.7155641   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.71544594   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.7152921   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7152143   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.715098   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.71499175   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   19   0.7149553   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7148714   REFERENCES:SIMLOC
