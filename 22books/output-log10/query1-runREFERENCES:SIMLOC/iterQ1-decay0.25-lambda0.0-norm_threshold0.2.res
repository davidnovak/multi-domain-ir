###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.456, activation diff: 17.456, ratio: 1.524
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 948.779, activation diff: 960.234, ratio: 1.012
#   pulse 3: activated nodes: 8764, borderline nodes: 4752, overall activation: 445.859, activation diff: 1394.638, ratio: 3.128
#   pulse 4: activated nodes: 11179, borderline nodes: 5521, overall activation: 4595.760, activation diff: 4883.377, ratio: 1.063
#   pulse 5: activated nodes: 11382, borderline nodes: 949, overall activation: 3060.936, activation diff: 4168.390, ratio: 1.362
#   pulse 6: activated nodes: 11420, borderline nodes: 264, overall activation: 6255.170, activation diff: 3514.315, ratio: 0.562
#   pulse 7: activated nodes: 11434, borderline nodes: 122, overall activation: 6537.411, activation diff: 319.073, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 6537.4
#   number of spread. activ. pulses: 7
#   running time: 1351

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999046   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99998784   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999083   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9988422   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99797153   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.75   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   8   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   10   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   12   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.75   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.75   REFERENCES:SIMLOC
