###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.429, activation diff: 14.429, ratio: 1.712
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 709.056, activation diff: 717.485, ratio: 1.012
#   pulse 3: activated nodes: 8651, borderline nodes: 5429, overall activation: 340.612, activation diff: 1049.668, ratio: 3.082
#   pulse 4: activated nodes: 11080, borderline nodes: 6730, overall activation: 4730.912, activation diff: 5006.027, ratio: 1.058
#   pulse 5: activated nodes: 11301, borderline nodes: 1849, overall activation: 2109.290, activation diff: 5532.541, ratio: 2.623
#   pulse 6: activated nodes: 11416, borderline nodes: 470, overall activation: 6569.209, activation diff: 5353.413, ratio: 0.815
#   pulse 7: activated nodes: 11443, borderline nodes: 124, overall activation: 7174.661, activation diff: 794.239, ratio: 0.111
#   pulse 8: activated nodes: 11445, borderline nodes: 72, overall activation: 7373.618, activation diff: 209.219, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 7373.6
#   number of spread. activ. pulses: 8
#   running time: 1256

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999889   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998989   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9988353   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777424   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   7   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   8   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_517   9   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   13   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   16   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_535   18   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMLOC
