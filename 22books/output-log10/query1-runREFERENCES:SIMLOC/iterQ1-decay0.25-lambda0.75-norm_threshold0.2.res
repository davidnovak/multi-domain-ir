###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.364, activation diff: 4.364, ratio: 0.593
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 33.091, activation diff: 27.176, ratio: 0.821
#   pulse 3: activated nodes: 8256, borderline nodes: 6241, overall activation: 87.463, activation diff: 55.032, ratio: 0.629
#   pulse 4: activated nodes: 9566, borderline nodes: 6807, overall activation: 318.391, activation diff: 230.942, ratio: 0.725
#   pulse 5: activated nodes: 10570, borderline nodes: 5641, overall activation: 739.219, activation diff: 420.828, ratio: 0.569
#   pulse 6: activated nodes: 11146, borderline nodes: 4383, overall activation: 1345.954, activation diff: 606.735, ratio: 0.451
#   pulse 7: activated nodes: 11291, borderline nodes: 2593, overall activation: 2041.235, activation diff: 695.281, ratio: 0.341
#   pulse 8: activated nodes: 11365, borderline nodes: 1239, overall activation: 2751.531, activation diff: 710.295, ratio: 0.258
#   pulse 9: activated nodes: 11409, borderline nodes: 594, overall activation: 3423.146, activation diff: 671.615, ratio: 0.196
#   pulse 10: activated nodes: 11420, borderline nodes: 334, overall activation: 4023.307, activation diff: 600.162, ratio: 0.149
#   pulse 11: activated nodes: 11424, borderline nodes: 212, overall activation: 4538.612, activation diff: 515.304, ratio: 0.114
#   pulse 12: activated nodes: 11427, borderline nodes: 157, overall activation: 4968.394, activation diff: 429.782, ratio: 0.087
#   pulse 13: activated nodes: 11431, borderline nodes: 128, overall activation: 5319.740, activation diff: 351.345, ratio: 0.066
#   pulse 14: activated nodes: 11432, borderline nodes: 115, overall activation: 5603.210, activation diff: 283.470, ratio: 0.051
#   pulse 15: activated nodes: 11436, borderline nodes: 106, overall activation: 5829.985, activation diff: 226.775, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 5830.0
#   number of spread. activ. pulses: 15
#   running time: 1463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9871905   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98303974   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9805412   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.9799744   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.97728914   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9667473   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.72778517   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72771144   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   9   0.7276677   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.72756493   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7273975   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   12   0.72688836   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   13   0.72683775   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7268086   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   15   0.7267934   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   16   0.72668636   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7266064   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.72647643   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   19   0.72638136   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.7263751   REFERENCES:SIMLOC
