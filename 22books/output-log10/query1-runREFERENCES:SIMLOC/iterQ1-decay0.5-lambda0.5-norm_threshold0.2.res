###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.728, activation diff: 8.728, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 135.987, activation diff: 130.027, ratio: 0.956
#   pulse 3: activated nodes: 8632, borderline nodes: 5384, overall activation: 204.205, activation diff: 70.959, ratio: 0.347
#   pulse 4: activated nodes: 9976, borderline nodes: 6547, overall activation: 881.664, activation diff: 677.459, ratio: 0.768
#   pulse 5: activated nodes: 10450, borderline nodes: 4790, overall activation: 1485.953, activation diff: 604.289, ratio: 0.407
#   pulse 6: activated nodes: 10760, borderline nodes: 4416, overall activation: 2018.839, activation diff: 532.886, ratio: 0.264
#   pulse 7: activated nodes: 10851, borderline nodes: 3769, overall activation: 2429.299, activation diff: 410.460, ratio: 0.169
#   pulse 8: activated nodes: 10872, borderline nodes: 3604, overall activation: 2730.025, activation diff: 300.726, ratio: 0.110
#   pulse 9: activated nodes: 10886, borderline nodes: 3556, overall activation: 2941.859, activation diff: 211.835, ratio: 0.072
#   pulse 10: activated nodes: 10888, borderline nodes: 3537, overall activation: 3083.934, activation diff: 142.074, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10888
#   final overall activation: 3083.9
#   number of spread. activ. pulses: 10
#   running time: 1289

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99691755   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9962482   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99546134   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99529314   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9945886   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9905758   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49746037   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.49743336   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.49739358   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.49737927   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.4973672   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.4972332   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.49720055   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   14   0.49718684   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   15   0.49717548   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   16   0.49714792   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.49714732   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.49712265   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.497113   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   20   0.4970852   REFERENCES:SIMLOC
