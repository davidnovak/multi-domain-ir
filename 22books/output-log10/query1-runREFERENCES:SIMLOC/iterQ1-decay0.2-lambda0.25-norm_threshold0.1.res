###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.323, activation diff: 15.323, ratio: 1.243
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 785.983, activation diff: 787.079, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 856.573, activation diff: 674.423, ratio: 0.787
#   pulse 4: activated nodes: 11262, borderline nodes: 4159, overall activation: 4166.862, activation diff: 3355.255, ratio: 0.805
#   pulse 5: activated nodes: 11409, borderline nodes: 628, overall activation: 6063.879, activation diff: 1897.267, ratio: 0.313
#   pulse 6: activated nodes: 11440, borderline nodes: 91, overall activation: 7000.340, activation diff: 936.460, ratio: 0.134
#   pulse 7: activated nodes: 11451, borderline nodes: 36, overall activation: 7357.266, activation diff: 356.927, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 7357.3
#   number of spread. activ. pulses: 7
#   running time: 1188

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9985195   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9982945   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99799085   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99794126   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9970648   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99548256   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.79838264   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   8   0.798337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7983128   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.79828024   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.79826397   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.7982371   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.79820144   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.79819566   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   15   0.79816985   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_136   16   0.7981694   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7981608   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.79812276   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   19   0.79810226   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_187   20   0.79809815   REFERENCES:SIMLOC
