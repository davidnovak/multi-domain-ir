###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.092, activation diff: 13.092, ratio: 1.297
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 546.726, activation diff: 548.932, ratio: 1.004
#   pulse 3: activated nodes: 8729, borderline nodes: 5086, overall activation: 487.013, activation diff: 549.992, ratio: 1.129
#   pulse 4: activated nodes: 11124, borderline nodes: 6184, overall activation: 3575.167, activation diff: 3199.655, ratio: 0.895
#   pulse 5: activated nodes: 11319, borderline nodes: 1554, overall activation: 5354.838, activation diff: 1791.600, ratio: 0.335
#   pulse 6: activated nodes: 11419, borderline nodes: 380, overall activation: 6606.139, activation diff: 1251.302, ratio: 0.189
#   pulse 7: activated nodes: 11444, borderline nodes: 82, overall activation: 7151.540, activation diff: 545.401, ratio: 0.076
#   pulse 8: activated nodes: 11447, borderline nodes: 43, overall activation: 7358.694, activation diff: 207.154, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 7358.7
#   number of spread. activ. pulses: 8
#   running time: 1285

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9994484   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9993823   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9993145   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99923456   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9982717   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99697274   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7994584   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.79945743   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.7994474   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7994279   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.79942757   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.79942656   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7994214   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.7994133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7994131   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   16   0.79941016   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.79940414   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.79940104   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   19   0.79939634   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_34   20   0.7993956   REFERENCES:SIMLOC
