###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.108, activation diff: 5.108, ratio: 0.630
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 50.106, activation diff: 43.144, ratio: 0.861
#   pulse 3: activated nodes: 8690, borderline nodes: 5068, overall activation: 118.646, activation diff: 68.901, ratio: 0.581
#   pulse 4: activated nodes: 9985, borderline nodes: 5720, overall activation: 328.662, activation diff: 210.017, ratio: 0.639
#   pulse 5: activated nodes: 10574, borderline nodes: 4891, overall activation: 621.739, activation diff: 293.076, ratio: 0.471
#   pulse 6: activated nodes: 10727, borderline nodes: 4534, overall activation: 952.548, activation diff: 330.810, ratio: 0.347
#   pulse 7: activated nodes: 10795, borderline nodes: 4025, overall activation: 1286.043, activation diff: 333.495, ratio: 0.259
#   pulse 8: activated nodes: 10860, borderline nodes: 3691, overall activation: 1601.650, activation diff: 315.607, ratio: 0.197
#   pulse 9: activated nodes: 10877, borderline nodes: 3582, overall activation: 1887.515, activation diff: 285.865, ratio: 0.151
#   pulse 10: activated nodes: 10886, borderline nodes: 3544, overall activation: 2139.508, activation diff: 251.993, ratio: 0.118
#   pulse 11: activated nodes: 10888, borderline nodes: 3533, overall activation: 2357.419, activation diff: 217.911, ratio: 0.092
#   pulse 12: activated nodes: 10890, borderline nodes: 3529, overall activation: 2542.718, activation diff: 185.299, ratio: 0.073
#   pulse 13: activated nodes: 10890, borderline nodes: 3523, overall activation: 2697.971, activation diff: 155.253, ratio: 0.058
#   pulse 14: activated nodes: 10891, borderline nodes: 3520, overall activation: 2826.461, activation diff: 128.490, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10891
#   final overall activation: 2826.5
#   number of spread. activ. pulses: 14
#   running time: 1550

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98653233   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9820517   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9787261   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9782926   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.977131   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96504855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   7   0.4819854   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.48197353   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   9   0.48172694   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.48157853   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.48153496   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.4811024   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   13   0.48096448   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.4808899   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   15   0.48084474   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   16   0.48080105   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.48077923   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   18   0.48069486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   19   0.48067182   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.48063928   REFERENCES:SIMLOC
