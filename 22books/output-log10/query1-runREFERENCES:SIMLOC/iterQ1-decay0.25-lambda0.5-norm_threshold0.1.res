###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.215, activation diff: 10.215, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 320.452, activation diff: 311.375, ratio: 0.972
#   pulse 3: activated nodes: 8834, borderline nodes: 4236, overall activation: 575.760, activation diff: 256.305, ratio: 0.445
#   pulse 4: activated nodes: 11177, borderline nodes: 5097, overall activation: 2144.485, activation diff: 1568.725, ratio: 0.732
#   pulse 5: activated nodes: 11356, borderline nodes: 1253, overall activation: 3606.505, activation diff: 1462.020, ratio: 0.405
#   pulse 6: activated nodes: 11420, borderline nodes: 365, overall activation: 4774.327, activation diff: 1167.822, ratio: 0.245
#   pulse 7: activated nodes: 11432, borderline nodes: 123, overall activation: 5556.678, activation diff: 782.351, ratio: 0.141
#   pulse 8: activated nodes: 11437, borderline nodes: 86, overall activation: 6034.113, activation diff: 477.435, ratio: 0.079
#   pulse 9: activated nodes: 11438, borderline nodes: 85, overall activation: 6313.010, activation diff: 278.897, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11438
#   final overall activation: 6313.0
#   number of spread. activ. pulses: 9
#   running time: 1300

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9958329   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99453187   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99367374   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99334455   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9923394   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9883422   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7438626   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.74376726   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.74359274   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7435138   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.74348295   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   12   0.74332666   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74310917   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   14   0.74309933   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.7430351   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   16   0.74302036   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   17   0.74298346   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   18   0.7429497   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   19   0.74294263   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_134   20   0.7429267   REFERENCES:SIMLOC
