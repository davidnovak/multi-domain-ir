###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.323, activation diff: 15.323, ratio: 1.243
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 737.374, activation diff: 738.470, ratio: 1.001
#   pulse 3: activated nodes: 9060, borderline nodes: 3997, overall activation: 773.723, activation diff: 602.685, ratio: 0.779
#   pulse 4: activated nodes: 11256, borderline nodes: 4192, overall activation: 3684.793, activation diff: 2948.912, ratio: 0.800
#   pulse 5: activated nodes: 11405, borderline nodes: 674, overall activation: 5340.497, activation diff: 1655.861, ratio: 0.310
#   pulse 6: activated nodes: 11428, borderline nodes: 154, overall activation: 6182.924, activation diff: 842.427, ratio: 0.136
#   pulse 7: activated nodes: 11441, borderline nodes: 76, overall activation: 6513.109, activation diff: 330.185, ratio: 0.051
#   pulse 8: activated nodes: 11442, borderline nodes: 69, overall activation: 6634.925, activation diff: 121.816, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 6634.9
#   number of spread. activ. pulses: 8
#   running time: 1259

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99962986   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9995664   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99947864   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994337   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99853957   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99747705   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7496209   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.749604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.74959683   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   10   0.7495767   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.74957657   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7495761   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   13   0.7495743   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7495609   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.74955356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.7495499   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_283   17   0.7495478   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_330   18   0.7495391   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.749537   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_319   20   0.7495359   REFERENCES:SIMLOC
