###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 6.607, activation diff: 3.607, ratio: 0.546
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 12.769, activation diff: 8.016, ratio: 0.628
#   pulse 3: activated nodes: 7789, borderline nodes: 6924, overall activation: 34.831, activation diff: 23.262, ratio: 0.668
#   pulse 4: activated nodes: 8058, borderline nodes: 6490, overall activation: 119.244, activation diff: 84.794, ratio: 0.711
#   pulse 5: activated nodes: 9143, borderline nodes: 6478, overall activation: 330.505, activation diff: 211.312, ratio: 0.639
#   pulse 6: activated nodes: 10485, borderline nodes: 6370, overall activation: 719.900, activation diff: 389.396, ratio: 0.541
#   pulse 7: activated nodes: 11074, borderline nodes: 4989, overall activation: 1276.559, activation diff: 556.659, ratio: 0.436
#   pulse 8: activated nodes: 11216, borderline nodes: 3394, overall activation: 1925.041, activation diff: 648.483, ratio: 0.337
#   pulse 9: activated nodes: 11318, borderline nodes: 2104, overall activation: 2596.429, activation diff: 671.388, ratio: 0.259
#   pulse 10: activated nodes: 11369, borderline nodes: 1091, overall activation: 3245.992, activation diff: 649.562, ratio: 0.200
#   pulse 11: activated nodes: 11406, borderline nodes: 642, overall activation: 3839.751, activation diff: 593.760, ratio: 0.155
#   pulse 12: activated nodes: 11422, borderline nodes: 411, overall activation: 4360.144, activation diff: 520.393, ratio: 0.119
#   pulse 13: activated nodes: 11425, borderline nodes: 258, overall activation: 4802.685, activation diff: 442.541, ratio: 0.092
#   pulse 14: activated nodes: 11428, borderline nodes: 196, overall activation: 5170.315, activation diff: 367.630, ratio: 0.071
#   pulse 15: activated nodes: 11429, borderline nodes: 164, overall activation: 5470.690, activation diff: 300.374, ratio: 0.055

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11429
#   final overall activation: 5470.7
#   number of spread. activ. pulses: 15
#   running time: 1610

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9797001   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9736829   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   3   0.97321266   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9708993   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.958922   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94856256   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_153   7   0.7227349   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   8   0.7222863   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   9   0.72226167   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.7221209   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   11   0.7220459   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_156   12   0.72185594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.7216566   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7213964   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_711   15   0.72124183   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   16   0.7211466   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   17   0.72113425   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_310   18   0.72103924   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_37   19   0.7209058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.72082007   REFERENCES:SIMLOC
