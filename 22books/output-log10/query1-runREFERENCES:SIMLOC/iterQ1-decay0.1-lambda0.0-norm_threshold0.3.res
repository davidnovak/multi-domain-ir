###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.429, activation diff: 14.429, ratio: 1.712
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 797.103, activation diff: 805.532, ratio: 1.011
#   pulse 3: activated nodes: 8651, borderline nodes: 5429, overall activation: 445.807, activation diff: 1242.910, ratio: 2.788
#   pulse 4: activated nodes: 11091, borderline nodes: 6672, overall activation: 5875.072, activation diff: 6217.570, ratio: 1.058
#   pulse 5: activated nodes: 11342, borderline nodes: 1585, overall activation: 3386.706, activation diff: 6379.815, ratio: 1.884
#   pulse 6: activated nodes: 11423, borderline nodes: 302, overall activation: 8441.474, activation diff: 5794.044, ratio: 0.686
#   pulse 7: activated nodes: 11449, borderline nodes: 55, overall activation: 9006.826, activation diff: 676.796, ratio: 0.075
#   pulse 8: activated nodes: 11451, borderline nodes: 21, overall activation: 9148.880, activation diff: 148.973, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 9148.9
#   number of spread. activ. pulses: 8
#   running time: 1292

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999894   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998989   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99883795   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777436   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   7   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   9   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_71   10   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   11   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   12   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   13   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_514   14   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_560   15   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   16   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_579   17   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_576   19   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_541   20   0.9   REFERENCES:SIMLOC
