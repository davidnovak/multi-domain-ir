###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.108, activation diff: 5.108, ratio: 0.630
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 82.806, activation diff: 75.843, ratio: 0.916
#   pulse 3: activated nodes: 8690, borderline nodes: 5068, overall activation: 212.633, activation diff: 130.189, ratio: 0.612
#   pulse 4: activated nodes: 10898, borderline nodes: 6211, overall activation: 759.972, activation diff: 547.339, ratio: 0.720
#   pulse 5: activated nodes: 11211, borderline nodes: 3354, overall activation: 1630.932, activation diff: 870.960, ratio: 0.534
#   pulse 6: activated nodes: 11356, borderline nodes: 1494, overall activation: 2716.847, activation diff: 1085.915, ratio: 0.400
#   pulse 7: activated nodes: 11426, borderline nodes: 393, overall activation: 3841.414, activation diff: 1124.567, ratio: 0.293
#   pulse 8: activated nodes: 11437, borderline nodes: 140, overall activation: 4877.414, activation diff: 1036.000, ratio: 0.212
#   pulse 9: activated nodes: 11447, borderline nodes: 57, overall activation: 5770.720, activation diff: 893.305, ratio: 0.155
#   pulse 10: activated nodes: 11451, borderline nodes: 29, overall activation: 6510.130, activation diff: 739.411, ratio: 0.114
#   pulse 11: activated nodes: 11451, borderline nodes: 21, overall activation: 7107.588, activation diff: 597.458, ratio: 0.084
#   pulse 12: activated nodes: 11451, borderline nodes: 13, overall activation: 7583.485, activation diff: 475.897, ratio: 0.063
#   pulse 13: activated nodes: 11452, borderline nodes: 10, overall activation: 7959.169, activation diff: 375.684, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 7959.2
#   number of spread. activ. pulses: 13
#   running time: 1590

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9823412   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.97717613   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9737238   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9732691   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97133684   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95899177   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.85844874   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.8579686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.857617   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.85687757   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.8564743   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.855989   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.85591894   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.8558959   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8556313   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.85548645   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8553705   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   18   0.8552306   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   19   0.8552248   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.85510266   REFERENCES:SIMLOC
