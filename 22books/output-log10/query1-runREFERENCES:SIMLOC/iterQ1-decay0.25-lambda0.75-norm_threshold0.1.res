###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.108, activation diff: 5.108, ratio: 0.630
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 70.544, activation diff: 63.581, ratio: 0.901
#   pulse 3: activated nodes: 8690, borderline nodes: 5068, overall activation: 176.242, activation diff: 106.061, ratio: 0.602
#   pulse 4: activated nodes: 10757, borderline nodes: 6158, overall activation: 571.205, activation diff: 394.963, ratio: 0.691
#   pulse 5: activated nodes: 11182, borderline nodes: 3810, overall activation: 1172.740, activation diff: 601.534, ratio: 0.513
#   pulse 6: activated nodes: 11335, borderline nodes: 1965, overall activation: 1903.607, activation diff: 730.868, ratio: 0.384
#   pulse 7: activated nodes: 11407, borderline nodes: 662, overall activation: 2661.200, activation diff: 757.593, ratio: 0.285
#   pulse 8: activated nodes: 11422, borderline nodes: 315, overall activation: 3375.970, activation diff: 714.770, ratio: 0.212
#   pulse 9: activated nodes: 11430, borderline nodes: 178, overall activation: 4008.782, activation diff: 632.812, ratio: 0.158
#   pulse 10: activated nodes: 11434, borderline nodes: 125, overall activation: 4545.754, activation diff: 536.973, ratio: 0.118
#   pulse 11: activated nodes: 11440, borderline nodes: 98, overall activation: 4988.613, activation diff: 442.858, ratio: 0.089
#   pulse 12: activated nodes: 11441, borderline nodes: 92, overall activation: 5347.194, activation diff: 358.581, ratio: 0.067
#   pulse 13: activated nodes: 11442, borderline nodes: 91, overall activation: 5634.170, activation diff: 286.977, ratio: 0.051
#   pulse 14: activated nodes: 11442, borderline nodes: 88, overall activation: 5862.113, activation diff: 227.942, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 5862.1
#   number of spread. activ. pulses: 14
#   running time: 1535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98670006   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9826529   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.97974384   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9795909   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97790015   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9676797   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7237585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   8   0.72352827   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   9   0.723245   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.7229179   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7226435   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.72226894   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.7221376   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.7220603   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7219341   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_293   16   0.72183657   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_234   17   0.721795   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   18   0.72179234   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.721781   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_159   20   0.7216363   REFERENCES:SIMLOC
