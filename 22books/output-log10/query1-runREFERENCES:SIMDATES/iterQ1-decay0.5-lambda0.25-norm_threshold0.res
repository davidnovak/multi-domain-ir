###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.723, activation diff: 19.723, ratio: 1.179
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 676.366, activation diff: 674.010, ratio: 0.997
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 620.378, activation diff: 178.963, ratio: 0.288
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1709.032, activation diff: 1088.654, ratio: 0.637
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2095.229, activation diff: 386.197, ratio: 0.184
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2217.686, activation diff: 122.458, ratio: 0.055
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2254.445, activation diff: 36.758, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2254.4
#   number of spread. activ. pulses: 7
#   running time: 402

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993937   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99908245   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.998801   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99873817   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977881   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99646705   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49936557   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.4993587   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4992351   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49923027   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49921447   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.4991998   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49919814   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.49910533   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.49910218   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.49907207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.4990682   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.499052   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.4990424   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   20   0.49903333   REFERENCES:SIMDATES
