###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.606, activation diff: 20.606, ratio: 1.411
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1058.659, activation diff: 1073.266, ratio: 1.014
#   pulse 3: activated nodes: 8171, borderline nodes: 4081, overall activation: 672.454, activation diff: 1731.113, ratio: 2.574
#   pulse 4: activated nodes: 10366, borderline nodes: 3798, overall activation: 4619.510, activation diff: 5265.170, ratio: 1.140
#   pulse 5: activated nodes: 11225, borderline nodes: 1420, overall activation: 1928.531, activation diff: 6320.306, ratio: 3.277
#   pulse 6: activated nodes: 11314, borderline nodes: 149, overall activation: 5054.388, activation diff: 6232.609, ratio: 1.233
#   pulse 7: activated nodes: 11334, borderline nodes: 88, overall activation: 6037.560, activation diff: 2198.029, ratio: 0.364
#   pulse 8: activated nodes: 11337, borderline nodes: 58, overall activation: 6583.515, activation diff: 701.920, ratio: 0.107
#   pulse 9: activated nodes: 11337, borderline nodes: 58, overall activation: 6659.082, activation diff: 97.126, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 6659.1
#   number of spread. activ. pulses: 9
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999005   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990827   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99894935   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99798596   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_579   13   0.8   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_535   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMDATES
