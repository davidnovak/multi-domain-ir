###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.617, activation diff: 17.617, ratio: 1.205
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 815.181, activation diff: 816.465, ratio: 1.002
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 973.210, activation diff: 793.259, ratio: 0.815
#   pulse 4: activated nodes: 10701, borderline nodes: 2786, overall activation: 3855.217, activation diff: 3003.964, ratio: 0.779
#   pulse 5: activated nodes: 11240, borderline nodes: 1030, overall activation: 5608.040, activation diff: 1754.775, ratio: 0.313
#   pulse 6: activated nodes: 11316, borderline nodes: 118, overall activation: 6341.842, activation diff: 733.809, ratio: 0.116
#   pulse 7: activated nodes: 11335, borderline nodes: 73, overall activation: 6606.807, activation diff: 264.964, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 6606.8
#   number of spread. activ. pulses: 7
#   running time: 439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9987939   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99854994   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9981822   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99808705   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972455   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9957066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.798458   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7984215   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7983271   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7983139   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.79827565   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7982702   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.798263   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7982258   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.79822373   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.79816973   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.7981561   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   18   0.7981476   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.7981447   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   20   0.7981399   REFERENCES:SIMDATES
