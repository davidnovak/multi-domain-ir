###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.246, activation diff: 13.246, ratio: 1.293
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 424.955, activation diff: 427.422, ratio: 1.006
#   pulse 3: activated nodes: 7559, borderline nodes: 4599, overall activation: 368.526, activation diff: 533.505, ratio: 1.448
#   pulse 4: activated nodes: 9604, borderline nodes: 5524, overall activation: 3774.606, activation diff: 3632.693, ratio: 0.962
#   pulse 5: activated nodes: 11043, borderline nodes: 2958, overall activation: 5915.035, activation diff: 2321.027, ratio: 0.392
#   pulse 6: activated nodes: 11261, borderline nodes: 544, overall activation: 7533.580, activation diff: 1631.869, ratio: 0.217
#   pulse 7: activated nodes: 11323, borderline nodes: 170, overall activation: 8250.509, activation diff: 716.933, ratio: 0.087
#   pulse 8: activated nodes: 11336, borderline nodes: 36, overall activation: 8529.426, activation diff: 278.917, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 8529.4
#   number of spread. activ. pulses: 8
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99937993   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993347   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99915683   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9991162   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9980507   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9964097   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.89930016   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8992834   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.8992816   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.89926535   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   11   0.8992572   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.8992531   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.89924884   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.89924777   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   15   0.8992468   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.8992442   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   17   0.89924073   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_319   18   0.8992386   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   19   0.8992349   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   20   0.8992268   REFERENCES:SIMDATES
