###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.246, activation diff: 13.246, ratio: 1.293
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 378.407, activation diff: 380.874, ratio: 1.007
#   pulse 3: activated nodes: 7559, borderline nodes: 4599, overall activation: 288.638, activation diff: 435.057, ratio: 1.507
#   pulse 4: activated nodes: 9559, borderline nodes: 5561, overall activation: 2993.620, activation diff: 2866.824, ratio: 0.958
#   pulse 5: activated nodes: 10995, borderline nodes: 3051, overall activation: 4507.927, activation diff: 1642.084, ratio: 0.364
#   pulse 6: activated nodes: 11242, borderline nodes: 755, overall activation: 5724.077, activation diff: 1221.148, ratio: 0.213
#   pulse 7: activated nodes: 11308, borderline nodes: 360, overall activation: 6253.096, activation diff: 529.020, ratio: 0.085
#   pulse 8: activated nodes: 11323, borderline nodes: 152, overall activation: 6451.321, activation diff: 198.225, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 6451.3
#   number of spread. activ. pulses: 8
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99937993   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993317   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99915   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9990731   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99803984   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99632525   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7993773   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.79936314   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.79936147   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.79934305   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   11   0.7993322   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   12   0.79933035   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.7993299   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7993291   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   15   0.79932153   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   16   0.7993208   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.7993077   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_77   18   0.7993053   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   19   0.7993036   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   20   0.79930335   REFERENCES:SIMDATES
