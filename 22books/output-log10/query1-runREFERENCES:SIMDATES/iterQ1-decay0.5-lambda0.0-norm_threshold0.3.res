###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.661, activation diff: 17.661, ratio: 1.515
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 472.969, activation diff: 484.630, ratio: 1.025
#   pulse 3: activated nodes: 7814, borderline nodes: 4516, overall activation: 78.923, activation diff: 551.893, ratio: 6.993
#   pulse 4: activated nodes: 8085, borderline nodes: 4721, overall activation: 2017.900, activation diff: 2095.770, ratio: 1.039
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 95.432, activation diff: 2112.225, ratio: 22.133
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2057.242, activation diff: 2149.409, ratio: 1.045
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 95.801, activation diff: 2149.225, ratio: 22.434
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2057.902, activation diff: 2148.791, ratio: 1.044
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 96.994, activation diff: 2147.603, ratio: 22.142
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 2059.580, activation diff: 2145.930, ratio: 1.042
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 110.118, activation diff: 2132.807, ratio: 19.368
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 2070.983, activation diff: 2121.405, ratio: 1.024
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 567.484, activation diff: 1664.043, ratio: 2.932
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 2141.491, activation diff: 1593.537, ratio: 0.744
#   pulse 15: activated nodes: 8935, borderline nodes: 3614, overall activation: 2132.767, activation diff: 28.304, ratio: 0.013

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2132.8
#   number of spread. activ. pulses: 15
#   running time: 534

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99999994   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999838   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99994534   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996959   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9984967   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939347   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49999905   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49999484   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49998686   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49998224   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49997833   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49996158   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49995047   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49994522   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.4999273   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   17   0.49992216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   18   0.49990916   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.4999073   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_177   20   0.49989775   REFERENCES:SIMDATES
