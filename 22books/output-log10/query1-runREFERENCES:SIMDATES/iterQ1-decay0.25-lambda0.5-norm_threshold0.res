###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.149, activation diff: 13.149, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 482.626, activation diff: 469.478, ratio: 0.973
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 957.846, activation diff: 475.219, ratio: 0.496
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2524.193, activation diff: 1566.348, ratio: 0.621
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3837.973, activation diff: 1313.780, ratio: 0.342
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4700.822, activation diff: 862.849, ratio: 0.184
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5217.229, activation diff: 516.407, ratio: 0.099
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 5514.690, activation diff: 297.461, ratio: 0.054
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 5682.911, activation diff: 168.221, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5682.9
#   number of spread. activ. pulses: 9
#   running time: 476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972739   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9963627   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9954715   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9953454   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9940288   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9912491   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74510145   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74504364   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.74469614   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.74463433   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.74458766   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7444655   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.7444314   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.74433446   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   15   0.74430466   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.74428725   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.74427253   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7442135   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.744104   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.7441001   REFERENCES:SIMDATES
