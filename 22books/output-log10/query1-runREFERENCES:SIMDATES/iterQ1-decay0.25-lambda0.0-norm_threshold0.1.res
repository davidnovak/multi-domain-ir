###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.489, activation diff: 23.489, ratio: 1.343
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1328.822, activation diff: 1346.262, ratio: 1.013
#   pulse 3: activated nodes: 8630, borderline nodes: 3420, overall activation: 757.859, activation diff: 2086.619, ratio: 2.753
#   pulse 4: activated nodes: 10802, borderline nodes: 2511, overall activation: 4164.367, activation diff: 4881.630, ratio: 1.172
#   pulse 5: activated nodes: 11264, borderline nodes: 797, overall activation: 1718.158, activation diff: 5455.925, ratio: 3.175
#   pulse 6: activated nodes: 11314, borderline nodes: 503, overall activation: 4592.167, activation diff: 5198.827, ratio: 1.132
#   pulse 7: activated nodes: 11319, borderline nodes: 454, overall activation: 5497.658, activation diff: 1458.586, ratio: 0.265
#   pulse 8: activated nodes: 11319, borderline nodes: 454, overall activation: 5751.897, activation diff: 322.112, ratio: 0.056
#   pulse 9: activated nodes: 11321, borderline nodes: 454, overall activation: 5783.019, activation diff: 46.017, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5783.0
#   number of spread. activ. pulses: 9
#   running time: 498

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.999991   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999917   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990493   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981774   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
