###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.661, activation diff: 17.661, ratio: 1.515
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 753.705, activation diff: 765.366, ratio: 1.015
#   pulse 3: activated nodes: 7814, borderline nodes: 4516, overall activation: 440.422, activation diff: 1194.127, ratio: 2.711
#   pulse 4: activated nodes: 9940, borderline nodes: 4937, overall activation: 4453.605, activation diff: 4883.063, ratio: 1.096
#   pulse 5: activated nodes: 11147, borderline nodes: 2230, overall activation: 1759.835, activation diff: 6184.040, ratio: 3.514
#   pulse 6: activated nodes: 11290, borderline nodes: 292, overall activation: 4799.893, activation diff: 6432.686, ratio: 1.340
#   pulse 7: activated nodes: 11322, borderline nodes: 149, overall activation: 3114.175, activation diff: 5193.490, ratio: 1.668
#   pulse 8: activated nodes: 11331, borderline nodes: 81, overall activation: 5732.249, activation diff: 4281.144, ratio: 0.747
#   pulse 9: activated nodes: 11331, borderline nodes: 76, overall activation: 6462.560, activation diff: 954.070, ratio: 0.148
#   pulse 10: activated nodes: 11331, borderline nodes: 72, overall activation: 6559.067, activation diff: 144.331, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 6559.1
#   number of spread. activ. pulses: 10
#   running time: 485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.999989   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998987   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.998839   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777436   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.8   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_535   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   20   0.8   REFERENCES:SIMDATES
