###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.303, activation diff: 10.303, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 213.535, activation diff: 205.935, ratio: 0.964
#   pulse 3: activated nodes: 7800, borderline nodes: 4486, overall activation: 351.112, activation diff: 141.419, ratio: 0.403
#   pulse 4: activated nodes: 9757, borderline nodes: 5487, overall activation: 1650.907, activation diff: 1299.795, ratio: 0.787
#   pulse 5: activated nodes: 10989, borderline nodes: 3085, overall activation: 2953.901, activation diff: 1302.994, ratio: 0.441
#   pulse 6: activated nodes: 11196, borderline nodes: 1080, overall activation: 4008.109, activation diff: 1054.207, ratio: 0.263
#   pulse 7: activated nodes: 11287, borderline nodes: 687, overall activation: 4700.123, activation diff: 692.014, ratio: 0.147
#   pulse 8: activated nodes: 11296, borderline nodes: 664, overall activation: 5117.944, activation diff: 417.821, ratio: 0.082
#   pulse 9: activated nodes: 11297, borderline nodes: 628, overall activation: 5361.511, activation diff: 243.567, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11297
#   final overall activation: 5361.5
#   number of spread. activ. pulses: 9
#   running time: 495

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99506384   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9941014   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99289644   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99164224   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99156135   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98665285   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74285764   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74265933   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7425822   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74249744   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.7423395   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.7423236   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.7422973   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7421199   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.74201095   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.7419555   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   17   0.741933   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.74193156   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.74187773   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   20   0.74187565   REFERENCES:SIMDATES
