###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.455, activation diff: 15.455, ratio: 1.241
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 575.491, activation diff: 578.377, ratio: 1.005
#   pulse 3: activated nodes: 8057, borderline nodes: 4319, overall activation: 562.820, activation diff: 629.754, ratio: 1.119
#   pulse 4: activated nodes: 10199, borderline nodes: 4504, overall activation: 3422.667, activation diff: 3051.433, ratio: 0.892
#   pulse 5: activated nodes: 11170, borderline nodes: 1992, overall activation: 5181.338, activation diff: 1785.121, ratio: 0.345
#   pulse 6: activated nodes: 11290, borderline nodes: 287, overall activation: 6104.789, activation diff: 923.722, ratio: 0.151
#   pulse 7: activated nodes: 11323, borderline nodes: 142, overall activation: 6463.909, activation diff: 359.120, ratio: 0.056
#   pulse 8: activated nodes: 11335, borderline nodes: 75, overall activation: 6592.366, activation diff: 128.457, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 6592.4
#   number of spread. activ. pulses: 8
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995237   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9994726   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99933124   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99930763   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99833316   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99709034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79948086   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.79946965   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7994591   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.7994422   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   11   0.79943174   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.7994303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.7994275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.79942644   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.79942596   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   16   0.7994151   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.79941285   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.79940593   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.799405   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   20   0.7994027   REFERENCES:SIMDATES
