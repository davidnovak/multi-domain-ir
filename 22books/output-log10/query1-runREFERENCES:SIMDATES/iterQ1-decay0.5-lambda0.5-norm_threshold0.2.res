###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.303, activation diff: 10.303, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 145.487, activation diff: 137.887, ratio: 0.948
#   pulse 3: activated nodes: 7800, borderline nodes: 4486, overall activation: 213.322, activation diff: 70.441, ratio: 0.330
#   pulse 4: activated nodes: 8070, borderline nodes: 4692, overall activation: 817.342, activation diff: 604.020, ratio: 0.739
#   pulse 5: activated nodes: 8918, borderline nodes: 3635, overall activation: 1317.440, activation diff: 500.098, ratio: 0.380
#   pulse 6: activated nodes: 8929, borderline nodes: 3625, overall activation: 1676.698, activation diff: 359.259, ratio: 0.214
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1898.964, activation diff: 222.266, ratio: 0.117
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2028.827, activation diff: 129.863, ratio: 0.064
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2102.556, activation diff: 73.729, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2102.6
#   number of spread. activ. pulses: 9
#   running time: 434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9950615   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99403065   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99275434   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.991387   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.99117446   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9856483   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49520046   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.4950609   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.49498856   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.49484292   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.49482027   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   12   0.49458513   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.4945768   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49454212   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   15   0.49438423   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   16   0.49438107   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.49438065   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.49438065   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.49435413   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.49433887   REFERENCES:SIMDATES
