###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.830, activation diff: 8.830, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 142.391, activation diff: 137.178, ratio: 0.963
#   pulse 3: activated nodes: 7110, borderline nodes: 4805, overall activation: 215.897, activation diff: 81.432, ratio: 0.377
#   pulse 4: activated nodes: 8688, borderline nodes: 6006, overall activation: 1675.779, activation diff: 1459.928, ratio: 0.871
#   pulse 5: activated nodes: 10420, borderline nodes: 3792, overall activation: 3387.757, activation diff: 1711.978, ratio: 0.505
#   pulse 6: activated nodes: 11058, borderline nodes: 1492, overall activation: 5242.089, activation diff: 1854.332, ratio: 0.354
#   pulse 7: activated nodes: 11261, borderline nodes: 465, overall activation: 6593.789, activation diff: 1351.700, ratio: 0.205
#   pulse 8: activated nodes: 11324, borderline nodes: 150, overall activation: 7453.440, activation diff: 859.651, ratio: 0.115
#   pulse 9: activated nodes: 11334, borderline nodes: 65, overall activation: 7971.625, activation diff: 518.185, ratio: 0.065
#   pulse 10: activated nodes: 11337, borderline nodes: 33, overall activation: 8275.905, activation diff: 304.281, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 8275.9
#   number of spread. activ. pulses: 10
#   running time: 492

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965025   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99609065   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9953505   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9942486   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.99354976   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9897946   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8948497   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8947308   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.89470017   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.89465487   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.89463925   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.8945881   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.8945698   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8945301   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   15   0.89445645   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.894445   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.894395   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.89439166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.8943808   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   20   0.8943798   REFERENCES:SIMDATES
