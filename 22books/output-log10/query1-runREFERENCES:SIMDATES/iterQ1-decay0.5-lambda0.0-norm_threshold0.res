###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 20.297, activation diff: 26.297, ratio: 1.296
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1110.901, activation diff: 1130.566, ratio: 1.018
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 106.766, activation diff: 1191.129, ratio: 11.156
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 2194.518, activation diff: 2232.821, ratio: 1.017
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1219.800, activation diff: 1122.679, ratio: 0.920
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2268.130, activation diff: 1050.851, ratio: 0.463
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2268.367, activation diff: 2.334, ratio: 0.001

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2268.4
#   number of spread. activ. pulses: 7
#   running time: 494

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999884   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99991024   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9991099   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99826396   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4999994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4999968   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4999927   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49999055   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.49998748   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49997163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49996394   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49996382   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.49995488   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.49994853   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.4999434   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.4999361   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.4999351   REFERENCES:SIMDATES
