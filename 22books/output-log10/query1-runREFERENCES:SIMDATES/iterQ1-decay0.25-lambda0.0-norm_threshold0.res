###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 20.297, activation diff: 26.297, ratio: 1.296
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1663.301, activation diff: 1682.966, ratio: 1.012
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 983.209, activation diff: 2600.948, ratio: 2.645
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4346.648, activation diff: 5084.442, ratio: 1.170
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4485.957, activation diff: 2792.204, ratio: 0.622
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 5682.430, activation diff: 1574.003, ratio: 0.277
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5872.740, activation diff: 223.797, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5872.7
#   number of spread. activ. pulses: 7
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999917   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99992454   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99913913   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99835026   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
