###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.872, activation diff: 5.872, ratio: 0.662
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 74.938, activation diff: 67.071, ratio: 0.895
#   pulse 3: activated nodes: 7994, borderline nodes: 4268, overall activation: 186.342, activation diff: 111.664, ratio: 0.599
#   pulse 4: activated nodes: 9987, borderline nodes: 5167, overall activation: 577.971, activation diff: 391.629, ratio: 0.678
#   pulse 5: activated nodes: 10887, borderline nodes: 3287, overall activation: 1168.958, activation diff: 590.986, ratio: 0.506
#   pulse 6: activated nodes: 11159, borderline nodes: 1354, overall activation: 1861.662, activation diff: 692.704, ratio: 0.372
#   pulse 7: activated nodes: 11242, borderline nodes: 702, overall activation: 2540.901, activation diff: 679.239, ratio: 0.267
#   pulse 8: activated nodes: 11293, borderline nodes: 586, overall activation: 3149.308, activation diff: 608.407, ratio: 0.193
#   pulse 9: activated nodes: 11306, borderline nodes: 558, overall activation: 3668.019, activation diff: 518.712, ratio: 0.141
#   pulse 10: activated nodes: 11313, borderline nodes: 548, overall activation: 4097.645, activation diff: 429.626, ratio: 0.105
#   pulse 11: activated nodes: 11313, borderline nodes: 512, overall activation: 4447.189, activation diff: 349.544, ratio: 0.079
#   pulse 12: activated nodes: 11313, borderline nodes: 510, overall activation: 4728.257, activation diff: 281.067, ratio: 0.059
#   pulse 13: activated nodes: 11313, borderline nodes: 509, overall activation: 4952.460, activation diff: 224.204, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11313
#   final overall activation: 4952.5
#   number of spread. activ. pulses: 13
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98413265   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97979015   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.976419   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9738995   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9729541   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9616153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.71550304   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.71540684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7146479   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7141855   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.71414965   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7138524   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7134511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7132657   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.712991   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.7128836   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.7128204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.71281934   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.71280223   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.7125233   REFERENCES:SIMDATES
