###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.303, activation diff: 10.303, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 227.145, activation diff: 219.544, ratio: 0.967
#   pulse 3: activated nodes: 7800, borderline nodes: 4486, overall activation: 382.817, activation diff: 159.762, ratio: 0.417
#   pulse 4: activated nodes: 9770, borderline nodes: 5457, overall activation: 1873.684, activation diff: 1490.867, ratio: 0.796
#   pulse 5: activated nodes: 11016, borderline nodes: 3010, overall activation: 3406.857, activation diff: 1533.173, ratio: 0.450
#   pulse 6: activated nodes: 11214, borderline nodes: 850, overall activation: 4654.022, activation diff: 1247.164, ratio: 0.268
#   pulse 7: activated nodes: 11302, borderline nodes: 310, overall activation: 5472.576, activation diff: 818.554, ratio: 0.150
#   pulse 8: activated nodes: 11322, borderline nodes: 148, overall activation: 5967.936, activation diff: 495.360, ratio: 0.083
#   pulse 9: activated nodes: 11329, borderline nodes: 86, overall activation: 6258.011, activation diff: 290.075, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 6258.0
#   number of spread. activ. pulses: 9
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.995064   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9941089   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99291134   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9916867   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99158037   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.986777   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79238665   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7921716   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7921028   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7920028   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.7918891   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   12   0.7918694   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.79182065   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7916217   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.79151046   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   16   0.7914704   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7914412   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.7914202   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   19   0.791414   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_134   20   0.7913391   REFERENCES:SIMDATES
