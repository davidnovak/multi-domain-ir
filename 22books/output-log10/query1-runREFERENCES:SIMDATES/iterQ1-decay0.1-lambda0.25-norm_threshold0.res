###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.723, activation diff: 19.723, ratio: 1.179
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1207.443, activation diff: 1205.087, ratio: 0.998
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1912.313, activation diff: 1122.774, ratio: 0.587
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 5568.190, activation diff: 3655.877, ratio: 0.657
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 7712.159, activation diff: 2143.970, ratio: 0.278
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 8514.175, activation diff: 802.016, ratio: 0.094
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 8787.247, activation diff: 273.072, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 8787.2
#   number of spread. activ. pulses: 7
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993937   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9990839   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99880147   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99874556   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978057   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99652374   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.89888155   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.8988571   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.8986686   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8986529   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.8986368   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.8986066   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.8985805   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.89857304   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   15   0.8985679   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.89856297   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.8985353   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.89848983   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8984842   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.8984779   REFERENCES:SIMDATES
