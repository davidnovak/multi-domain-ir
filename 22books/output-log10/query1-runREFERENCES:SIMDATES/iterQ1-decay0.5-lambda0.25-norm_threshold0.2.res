###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.455, activation diff: 15.455, ratio: 1.241
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 362.426, activation diff: 365.312, ratio: 1.008
#   pulse 3: activated nodes: 8057, borderline nodes: 4319, overall activation: 215.425, activation diff: 255.715, ratio: 1.187
#   pulse 4: activated nodes: 8328, borderline nodes: 4524, overall activation: 1459.139, activation diff: 1258.081, ratio: 0.862
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1876.282, activation diff: 424.133, ratio: 0.226
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2086.598, activation diff: 210.316, ratio: 0.101
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2159.903, activation diff: 73.305, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2159.9
#   number of spread. activ. pulses: 7
#   running time: 403

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980948   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9979099   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99747473   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99729824   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9964391   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9940429   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4986969   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49867326   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.49862006   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.49860913   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.49858752   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   12   0.49850044   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.4984999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.49849898   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.49845174   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.49844873   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.49843818   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.49843666   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.49841645   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   20   0.49840382   REFERENCES:SIMDATES
