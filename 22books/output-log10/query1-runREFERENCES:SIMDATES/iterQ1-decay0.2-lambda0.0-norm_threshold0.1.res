###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.489, activation diff: 23.489, ratio: 1.343
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1417.031, activation diff: 1434.471, ratio: 1.012
#   pulse 3: activated nodes: 8630, borderline nodes: 3420, overall activation: 924.065, activation diff: 2341.033, ratio: 2.533
#   pulse 4: activated nodes: 10802, borderline nodes: 2500, overall activation: 4728.415, activation diff: 5595.849, ratio: 1.183
#   pulse 5: activated nodes: 11264, borderline nodes: 779, overall activation: 2172.800, activation diff: 6292.949, ratio: 2.896
#   pulse 6: activated nodes: 11333, borderline nodes: 91, overall activation: 5348.146, activation diff: 5904.736, ratio: 1.104
#   pulse 7: activated nodes: 11340, borderline nodes: 36, overall activation: 6459.556, activation diff: 1675.272, ratio: 0.259
#   pulse 8: activated nodes: 11341, borderline nodes: 27, overall activation: 6719.825, activation diff: 335.665, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 6719.8
#   number of spread. activ. pulses: 8
#   running time: 529

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999106   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999171   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990494   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99817747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_579   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   14   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_535   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_532   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMDATES
