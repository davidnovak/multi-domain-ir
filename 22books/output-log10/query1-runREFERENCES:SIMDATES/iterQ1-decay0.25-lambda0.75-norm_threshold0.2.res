###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.152, activation diff: 5.152, ratio: 0.632
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 36.627, activation diff: 29.768, ratio: 0.813
#   pulse 3: activated nodes: 6984, borderline nodes: 4856, overall activation: 95.227, activation diff: 59.094, ratio: 0.621
#   pulse 4: activated nodes: 8491, borderline nodes: 5626, overall activation: 335.489, activation diff: 240.261, ratio: 0.716
#   pulse 5: activated nodes: 9930, borderline nodes: 4858, overall activation: 757.462, activation diff: 421.974, ratio: 0.557
#   pulse 6: activated nodes: 10740, borderline nodes: 3694, overall activation: 1350.947, activation diff: 593.484, ratio: 0.439
#   pulse 7: activated nodes: 11100, borderline nodes: 1882, overall activation: 2011.960, activation diff: 661.014, ratio: 0.329
#   pulse 8: activated nodes: 11218, borderline nodes: 909, overall activation: 2652.215, activation diff: 640.255, ratio: 0.241
#   pulse 9: activated nodes: 11246, borderline nodes: 630, overall activation: 3225.317, activation diff: 573.101, ratio: 0.178
#   pulse 10: activated nodes: 11294, borderline nodes: 574, overall activation: 3715.407, activation diff: 490.091, ratio: 0.132
#   pulse 11: activated nodes: 11305, borderline nodes: 551, overall activation: 4123.020, activation diff: 407.612, ratio: 0.099
#   pulse 12: activated nodes: 11313, borderline nodes: 543, overall activation: 4456.068, activation diff: 333.049, ratio: 0.075
#   pulse 13: activated nodes: 11313, borderline nodes: 541, overall activation: 4725.038, activation diff: 268.970, ratio: 0.057
#   pulse 14: activated nodes: 11313, borderline nodes: 508, overall activation: 4940.556, activation diff: 215.518, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11313
#   final overall activation: 4940.6
#   number of spread. activ. pulses: 14
#   running time: 561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98521525   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98103803   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9782311   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.97589874   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.97123367   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96143883   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.721385   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7208007   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7207371   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.72062963   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.72052294   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.7196705   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.71963155   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.7196218   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.7195954   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7194685   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7193408   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.71927714   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.71913254   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.71908104   REFERENCES:SIMDATES
