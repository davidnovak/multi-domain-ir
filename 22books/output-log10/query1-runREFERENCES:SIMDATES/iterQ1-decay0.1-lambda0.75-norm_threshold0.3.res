###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.415, activation diff: 4.415, ratio: 0.595
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 16.781, activation diff: 11.014, ratio: 0.656
#   pulse 3: activated nodes: 6150, borderline nodes: 5201, overall activation: 46.949, activation diff: 31.048, ratio: 0.661
#   pulse 4: activated nodes: 6707, borderline nodes: 5035, overall activation: 179.209, activation diff: 132.469, ratio: 0.739
#   pulse 5: activated nodes: 8242, borderline nodes: 5266, overall activation: 495.785, activation diff: 316.576, ratio: 0.639
#   pulse 6: activated nodes: 9828, borderline nodes: 5070, overall activation: 1102.668, activation diff: 606.883, ratio: 0.550
#   pulse 7: activated nodes: 10793, borderline nodes: 3575, overall activation: 1979.466, activation diff: 876.798, ratio: 0.443
#   pulse 8: activated nodes: 11129, borderline nodes: 1850, overall activation: 2976.684, activation diff: 997.218, ratio: 0.335
#   pulse 9: activated nodes: 11227, borderline nodes: 806, overall activation: 3952.500, activation diff: 975.816, ratio: 0.247
#   pulse 10: activated nodes: 11259, borderline nodes: 359, overall activation: 4830.388, activation diff: 877.889, ratio: 0.182
#   pulse 11: activated nodes: 11311, borderline nodes: 209, overall activation: 5583.219, activation diff: 752.831, ratio: 0.135
#   pulse 12: activated nodes: 11326, borderline nodes: 135, overall activation: 6210.872, activation diff: 627.652, ratio: 0.101
#   pulse 13: activated nodes: 11330, borderline nodes: 87, overall activation: 6725.187, activation diff: 514.315, ratio: 0.076
#   pulse 14: activated nodes: 11333, borderline nodes: 57, overall activation: 7141.582, activation diff: 416.395, ratio: 0.058
#   pulse 15: activated nodes: 11335, borderline nodes: 37, overall activation: 7475.800, activation diff: 334.219, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 7475.8
#   number of spread. activ. pulses: 15
#   running time: 556

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98431337   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98029447   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97787064   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9770947   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.96459067   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9580227   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8692354   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   8   0.86921406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.86895216   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8688125   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   11   0.8687935   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.8687827   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   13   0.8682266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8681393   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.86806405   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.8678605   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.86762357   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.86753845   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.86740774   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_310   20   0.86735594   REFERENCES:SIMDATES
