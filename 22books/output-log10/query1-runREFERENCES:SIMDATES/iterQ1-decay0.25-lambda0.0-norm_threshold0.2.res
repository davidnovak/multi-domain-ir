###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.606, activation diff: 20.606, ratio: 1.411
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 992.833, activation diff: 1007.440, ratio: 1.015
#   pulse 3: activated nodes: 8171, borderline nodes: 4081, overall activation: 552.152, activation diff: 1544.985, ratio: 2.798
#   pulse 4: activated nodes: 10363, borderline nodes: 3836, overall activation: 4076.528, activation diff: 4608.641, ratio: 1.131
#   pulse 5: activated nodes: 11220, borderline nodes: 1473, overall activation: 1523.678, activation diff: 5457.831, ratio: 3.582
#   pulse 6: activated nodes: 11266, borderline nodes: 603, overall activation: 4367.994, activation diff: 5434.001, ratio: 1.244
#   pulse 7: activated nodes: 11286, borderline nodes: 545, overall activation: 4968.011, activation diff: 2041.068, ratio: 0.411
#   pulse 8: activated nodes: 11288, borderline nodes: 540, overall activation: 5605.067, activation diff: 821.993, ratio: 0.147
#   pulse 9: activated nodes: 11288, borderline nodes: 538, overall activation: 5695.326, activation diff: 109.423, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11288
#   final overall activation: 5695.3
#   number of spread. activ. pulses: 9
#   running time: 597

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999081   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99894905   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9979857   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.75   REFERENCES:SIMDATES
