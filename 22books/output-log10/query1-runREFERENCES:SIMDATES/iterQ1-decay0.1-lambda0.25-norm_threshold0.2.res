###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.455, activation diff: 15.455, ratio: 1.241
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 646.513, activation diff: 649.399, ratio: 1.004
#   pulse 3: activated nodes: 8057, borderline nodes: 4319, overall activation: 715.406, activation diff: 791.222, ratio: 1.106
#   pulse 4: activated nodes: 10212, borderline nodes: 4407, overall activation: 4322.658, activation diff: 3874.755, ratio: 0.896
#   pulse 5: activated nodes: 11187, borderline nodes: 1858, overall activation: 6750.806, activation diff: 2478.854, ratio: 0.367
#   pulse 6: activated nodes: 11302, borderline nodes: 207, overall activation: 8004.288, activation diff: 1254.580, ratio: 0.157
#   pulse 7: activated nodes: 11334, borderline nodes: 58, overall activation: 8491.604, activation diff: 487.322, ratio: 0.057
#   pulse 8: activated nodes: 11340, borderline nodes: 18, overall activation: 8670.854, activation diff: 179.250, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11340
#   final overall activation: 8670.9
#   number of spread. activ. pulses: 8
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995237   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99947304   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99933255   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9993092   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9983368   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99711436   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8994163   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8994033   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.8993914   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.8993754   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   11   0.8993663   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.8993653   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8993605   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.8993566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   15   0.89935553   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   16   0.8993525   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.899351   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   18   0.89934886   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   19   0.8993487   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_319   20   0.8993413   REFERENCES:SIMDATES
