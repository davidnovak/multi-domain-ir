###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.830, activation diff: 8.830, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 119.860, activation diff: 114.647, ratio: 0.957
#   pulse 3: activated nodes: 7110, borderline nodes: 4805, overall activation: 173.067, activation diff: 59.930, ratio: 0.346
#   pulse 4: activated nodes: 8652, borderline nodes: 6042, overall activation: 1185.019, activation diff: 1011.995, ratio: 0.854
#   pulse 5: activated nodes: 10274, borderline nodes: 4107, overall activation: 2277.919, activation diff: 1092.900, ratio: 0.480
#   pulse 6: activated nodes: 10941, borderline nodes: 2127, overall activation: 3448.483, activation diff: 1170.564, ratio: 0.339
#   pulse 7: activated nodes: 11221, borderline nodes: 875, overall activation: 4310.754, activation diff: 862.271, ratio: 0.200
#   pulse 8: activated nodes: 11268, borderline nodes: 649, overall activation: 4863.592, activation diff: 552.837, ratio: 0.114
#   pulse 9: activated nodes: 11288, borderline nodes: 623, overall activation: 5196.989, activation diff: 333.398, ratio: 0.064
#   pulse 10: activated nodes: 11295, borderline nodes: 622, overall activation: 5392.693, activation diff: 195.704, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11295
#   final overall activation: 5392.7
#   number of spread. activ. pulses: 10
#   running time: 501

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99650013   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9960427   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99527586   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9941821   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.9930171   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98934585   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7456912   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7455868   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7455416   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.74552274   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7454637   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7453964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.74538124   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7453134   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.7452682   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.74526364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.74524933   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.74524486   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.7452166   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   20   0.74520636   REFERENCES:SIMDATES
