###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.415, activation diff: 4.415, ratio: 0.595
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 15.248, activation diff: 9.481, ratio: 0.622
#   pulse 3: activated nodes: 6150, borderline nodes: 5201, overall activation: 40.372, activation diff: 26.006, ratio: 0.644
#   pulse 4: activated nodes: 6693, borderline nodes: 5022, overall activation: 140.318, activation diff: 100.178, ratio: 0.714
#   pulse 5: activated nodes: 8154, borderline nodes: 5304, overall activation: 371.917, activation diff: 231.607, ratio: 0.623
#   pulse 6: activated nodes: 9627, borderline nodes: 5262, overall activation: 774.642, activation diff: 402.726, ratio: 0.520
#   pulse 7: activated nodes: 10534, borderline nodes: 4161, overall activation: 1327.831, activation diff: 553.189, ratio: 0.417
#   pulse 8: activated nodes: 10975, borderline nodes: 2578, overall activation: 1952.418, activation diff: 624.586, ratio: 0.320
#   pulse 9: activated nodes: 11159, borderline nodes: 1438, overall activation: 2568.631, activation diff: 616.213, ratio: 0.240
#   pulse 10: activated nodes: 11227, borderline nodes: 842, overall activation: 3129.524, activation diff: 560.893, ratio: 0.179
#   pulse 11: activated nodes: 11241, borderline nodes: 614, overall activation: 3615.739, activation diff: 486.215, ratio: 0.134
#   pulse 12: activated nodes: 11284, borderline nodes: 578, overall activation: 4024.554, activation diff: 408.815, ratio: 0.102
#   pulse 13: activated nodes: 11297, borderline nodes: 555, overall activation: 4361.573, activation diff: 337.018, ratio: 0.077
#   pulse 14: activated nodes: 11298, borderline nodes: 542, overall activation: 4635.767, activation diff: 274.194, ratio: 0.059
#   pulse 15: activated nodes: 11306, borderline nodes: 537, overall activation: 4856.820, activation diff: 221.054, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11306
#   final overall activation: 4856.8
#   number of spread. activ. pulses: 15
#   running time: 628

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9838354   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9795925   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97701514   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9763737   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.96232414   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95578134   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   7   0.7237273   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.723639   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   9   0.7235406   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.723376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.72332716   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7231551   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   13   0.72265   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.72256386   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.72256184   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.72239494   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   17   0.7221714   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.72215104   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7220757   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_37   20   0.72195625   REFERENCES:SIMDATES
