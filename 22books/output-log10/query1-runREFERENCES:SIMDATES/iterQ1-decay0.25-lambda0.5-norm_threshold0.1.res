###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.745, activation diff: 11.745, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 335.133, activation diff: 324.555, ratio: 0.968
#   pulse 3: activated nodes: 8336, borderline nodes: 3670, overall activation: 614.870, activation diff: 280.801, ratio: 0.457
#   pulse 4: activated nodes: 10478, borderline nodes: 3570, overall activation: 2091.054, activation diff: 1476.183, ratio: 0.706
#   pulse 5: activated nodes: 11205, borderline nodes: 1562, overall activation: 3441.536, activation diff: 1350.482, ratio: 0.392
#   pulse 6: activated nodes: 11261, borderline nodes: 623, overall activation: 4389.366, activation diff: 947.830, ratio: 0.216
#   pulse 7: activated nodes: 11289, borderline nodes: 529, overall activation: 4975.025, activation diff: 585.659, ratio: 0.118
#   pulse 8: activated nodes: 11289, borderline nodes: 515, overall activation: 5318.161, activation diff: 343.136, ratio: 0.065
#   pulse 9: activated nodes: 11289, borderline nodes: 514, overall activation: 5514.229, activation diff: 196.068, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11289
#   final overall activation: 5514.2
#   number of spread. activ. pulses: 9
#   running time: 466

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99650824   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954643   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99440324   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9939132   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99296296   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9894644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7441449   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74401253   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7437715   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74366224   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7436271   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.74346423   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.7434392   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7433114   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.74328345   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.7432505   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.7432164   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.74320644   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7431202   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   20   0.7430873   REFERENCES:SIMDATES
