###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.661, activation diff: 17.661, ratio: 1.515
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 706.916, activation diff: 718.577, ratio: 1.016
#   pulse 3: activated nodes: 7814, borderline nodes: 4516, overall activation: 363.489, activation diff: 1070.404, ratio: 2.945
#   pulse 4: activated nodes: 9937, borderline nodes: 4985, overall activation: 3938.714, activation diff: 4294.397, ratio: 1.090
#   pulse 5: activated nodes: 11137, borderline nodes: 2324, overall activation: 1398.900, activation diff: 5322.558, ratio: 3.805
#   pulse 6: activated nodes: 11235, borderline nodes: 809, overall activation: 4192.544, activation diff: 5537.900, ratio: 1.321
#   pulse 7: activated nodes: 11275, borderline nodes: 632, overall activation: 1648.589, activation diff: 5370.501, ratio: 3.258
#   pulse 8: activated nodes: 11278, borderline nodes: 628, overall activation: 4464.069, activation diff: 5109.558, ratio: 1.145
#   pulse 9: activated nodes: 11278, borderline nodes: 625, overall activation: 5271.144, activation diff: 1493.559, ratio: 0.283
#   pulse 10: activated nodes: 11278, borderline nodes: 625, overall activation: 5573.847, activation diff: 398.341, ratio: 0.071
#   pulse 11: activated nodes: 11278, borderline nodes: 623, overall activation: 5619.756, activation diff: 58.205, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11278
#   final overall activation: 5619.8
#   number of spread. activ. pulses: 11
#   running time: 728

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998885   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9998985   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99883884   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9977743   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   7   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   8   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   14   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   18   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   19   0.75   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.74999994   REFERENCES:SIMDATES
