###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.489, activation diff: 23.489, ratio: 1.343
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 887.775, activation diff: 905.215, ratio: 1.020
#   pulse 3: activated nodes: 8630, borderline nodes: 3420, overall activation: 92.679, activation diff: 980.396, ratio: 10.578
#   pulse 4: activated nodes: 8898, borderline nodes: 3623, overall activation: 2140.111, activation diff: 2221.848, ratio: 1.038
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 132.154, activation diff: 2188.215, ratio: 16.558
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2180.380, activation diff: 2152.578, ratio: 0.987
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1807.536, activation diff: 477.240, ratio: 0.264
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2232.247, activation diff: 425.398, ratio: 0.191
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2232.242, activation diff: 0.728, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2232.2
#   number of spread. activ. pulses: 9
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999914   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999894   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999122   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9990387   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9981634   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49999934   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49999654   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49999207   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49998972   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.4999864   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.49996865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.49996015   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49996015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   16   0.499952   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.49994314   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   18   0.49993753   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   19   0.49992988   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   20   0.49992967   REFERENCES:SIMDATES
