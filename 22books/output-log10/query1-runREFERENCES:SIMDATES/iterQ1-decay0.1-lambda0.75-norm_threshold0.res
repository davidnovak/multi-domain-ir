###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.574, activation diff: 6.574, ratio: 0.687
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 157.983, activation diff: 149.160, ratio: 0.944
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 410.606, activation diff: 252.731, ratio: 0.616
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1216.752, activation diff: 806.146, ratio: 0.663
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2314.758, activation diff: 1098.005, ratio: 0.474
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3459.410, activation diff: 1144.652, ratio: 0.331
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4502.753, activation diff: 1043.343, ratio: 0.232
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 5394.473, activation diff: 891.719, ratio: 0.165
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 6130.854, activation diff: 736.381, ratio: 0.120
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 6726.646, activation diff: 595.792, ratio: 0.089
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 7202.480, activation diff: 475.834, ratio: 0.066
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 7579.295, activation diff: 376.815, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7579.3
#   number of spread. activ. pulses: 12
#   running time: 521

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98204255   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97716594   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.973341   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9731381   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9688915   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.958985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8488381   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.8487345   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.847368   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.84664696   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.84661293   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.8465409   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.8463895   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8457838   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.8454646   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.8451925   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.8451133   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.84510505   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.84495705   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.84482807   REFERENCES:SIMDATES
