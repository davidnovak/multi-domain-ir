###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.246, activation diff: 13.246, ratio: 1.293
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 355.133, activation diff: 357.600, ratio: 1.007
#   pulse 3: activated nodes: 7559, borderline nodes: 4599, overall activation: 252.782, activation diff: 389.920, ratio: 1.543
#   pulse 4: activated nodes: 9542, borderline nodes: 5584, overall activation: 2639.221, activation diff: 2519.354, ratio: 0.955
#   pulse 5: activated nodes: 10962, borderline nodes: 3128, overall activation: 3888.673, activation diff: 1351.215, ratio: 0.347
#   pulse 6: activated nodes: 11168, borderline nodes: 1136, overall activation: 4911.485, activation diff: 1026.210, ratio: 0.209
#   pulse 7: activated nodes: 11279, borderline nodes: 729, overall activation: 5355.403, activation diff: 443.917, ratio: 0.083
#   pulse 8: activated nodes: 11285, borderline nodes: 713, overall activation: 5515.987, activation diff: 160.585, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11285
#   final overall activation: 5516.0
#   number of spread. activ. pulses: 8
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993799   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9993297   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9991455   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99904174   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99803305   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9962727   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74941576   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7494029   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7494011   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.74937904   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   11   0.7493697   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7493694   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   13   0.7493663   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7493642   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   15   0.7493603   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   16   0.749353   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.7493465   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.74934435   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   19   0.7493442   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_77   20   0.7493418   REFERENCES:SIMDATES
