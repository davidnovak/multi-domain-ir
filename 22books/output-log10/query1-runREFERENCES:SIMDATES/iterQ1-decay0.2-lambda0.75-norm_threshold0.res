###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.574, activation diff: 6.574, ratio: 0.687
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 141.747, activation diff: 132.925, ratio: 0.938
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 355.019, activation diff: 213.379, ratio: 0.601
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 973.980, activation diff: 618.961, ratio: 0.635
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1800.579, activation diff: 826.599, ratio: 0.459
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2661.084, activation diff: 860.505, ratio: 0.323
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3448.965, activation diff: 787.881, ratio: 0.228
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 4125.863, activation diff: 676.898, ratio: 0.164
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 4687.613, activation diff: 561.750, ratio: 0.120
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 5144.363, activation diff: 456.751, ratio: 0.089
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 5510.947, activation diff: 366.584, ratio: 0.067
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 5802.599, activation diff: 291.652, ratio: 0.050
#   pulse 13: activated nodes: 11342, borderline nodes: 14, overall activation: 6033.219, activation diff: 230.620, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6033.2
#   number of spread. activ. pulses: 13
#   running time: 539

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98652387   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9828128   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97987014   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.97969216   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97625804   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9683058   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7658114   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.7657272   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7647755   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.76432323   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.764313   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.7642379   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.76412237   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7635861   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.76336557   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7632151   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.76312685   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.76312256   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.76305026   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   20   0.76301354   REFERENCES:SIMDATES
