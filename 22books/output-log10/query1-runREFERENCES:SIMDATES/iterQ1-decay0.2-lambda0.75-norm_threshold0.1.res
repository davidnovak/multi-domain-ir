###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.872, activation diff: 5.872, ratio: 0.662
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 79.236, activation diff: 71.369, ratio: 0.901
#   pulse 3: activated nodes: 7994, borderline nodes: 4268, overall activation: 199.433, activation diff: 120.458, ratio: 0.604
#   pulse 4: activated nodes: 10001, borderline nodes: 5125, overall activation: 643.243, activation diff: 443.810, ratio: 0.690
#   pulse 5: activated nodes: 10935, borderline nodes: 3130, overall activation: 1329.614, activation diff: 686.370, ratio: 0.516
#   pulse 6: activated nodes: 11188, borderline nodes: 1236, overall activation: 2143.226, activation diff: 813.612, ratio: 0.380
#   pulse 7: activated nodes: 11248, borderline nodes: 559, overall activation: 2943.030, activation diff: 799.804, ratio: 0.272
#   pulse 8: activated nodes: 11306, borderline nodes: 279, overall activation: 3659.305, activation diff: 716.275, ratio: 0.196
#   pulse 9: activated nodes: 11324, borderline nodes: 139, overall activation: 4269.379, activation diff: 610.073, ratio: 0.143
#   pulse 10: activated nodes: 11330, borderline nodes: 79, overall activation: 4774.298, activation diff: 504.919, ratio: 0.106
#   pulse 11: activated nodes: 11334, borderline nodes: 58, overall activation: 5184.859, activation diff: 410.561, ratio: 0.079
#   pulse 12: activated nodes: 11335, borderline nodes: 43, overall activation: 5514.869, activation diff: 330.010, ratio: 0.060
#   pulse 13: activated nodes: 11337, borderline nodes: 37, overall activation: 5778.031, activation diff: 263.162, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11337
#   final overall activation: 5778.0
#   number of spread. activ. pulses: 13
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9841496   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9798627   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97652924   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9741404   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97304964   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9619719   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.76330876   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7632457   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7624711   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7618737   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.76184297   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.7615696   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.7611514   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7610633   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.76073235   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.7606117   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.7605791   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7605363   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.76053536   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.76029587   REFERENCES:SIMDATES
