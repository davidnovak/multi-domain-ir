###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.152, activation diff: 5.152, ratio: 0.632
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 42.147, activation diff: 35.288, ratio: 0.837
#   pulse 3: activated nodes: 6984, borderline nodes: 4856, overall activation: 112.882, activation diff: 71.230, ratio: 0.631
#   pulse 4: activated nodes: 8612, borderline nodes: 5710, overall activation: 437.872, activation diff: 324.989, ratio: 0.742
#   pulse 5: activated nodes: 10142, borderline nodes: 4618, overall activation: 1054.904, activation diff: 617.033, ratio: 0.585
#   pulse 6: activated nodes: 10871, borderline nodes: 3152, overall activation: 1982.852, activation diff: 927.948, ratio: 0.468
#   pulse 7: activated nodes: 11188, borderline nodes: 1374, overall activation: 3029.687, activation diff: 1046.835, ratio: 0.346
#   pulse 8: activated nodes: 11245, borderline nodes: 526, overall activation: 4040.246, activation diff: 1010.559, ratio: 0.250
#   pulse 9: activated nodes: 11311, borderline nodes: 240, overall activation: 4938.197, activation diff: 897.951, ratio: 0.182
#   pulse 10: activated nodes: 11326, borderline nodes: 131, overall activation: 5700.461, activation diff: 762.264, ratio: 0.134
#   pulse 11: activated nodes: 11332, borderline nodes: 77, overall activation: 6330.899, activation diff: 630.438, ratio: 0.100
#   pulse 12: activated nodes: 11335, borderline nodes: 43, overall activation: 6843.632, activation diff: 512.733, ratio: 0.075
#   pulse 13: activated nodes: 11337, borderline nodes: 30, overall activation: 7255.879, activation diff: 412.246, ratio: 0.057
#   pulse 14: activated nodes: 11338, borderline nodes: 21, overall activation: 7584.710, activation diff: 328.831, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11338
#   final overall activation: 7584.7
#   number of spread. activ. pulses: 14
#   running time: 558

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98535   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9813728   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97866464   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.97625065   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.9723467   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96282864   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.86616963   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.86560535   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.86556923   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.8651222   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.8650015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.8643862   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.8641812   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.86415166   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.86405504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.864004   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.8637853   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   18   0.8636713   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.8636669   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.8635127   REFERENCES:SIMDATES
