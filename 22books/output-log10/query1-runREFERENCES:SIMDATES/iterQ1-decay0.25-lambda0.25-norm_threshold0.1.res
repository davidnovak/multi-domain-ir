###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.617, activation diff: 17.617, ratio: 1.205
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 764.824, activation diff: 766.108, ratio: 1.002
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 857.156, activation diff: 687.973, ratio: 0.803
#   pulse 4: activated nodes: 10701, borderline nodes: 2799, overall activation: 3375.528, activation diff: 2618.438, ratio: 0.776
#   pulse 5: activated nodes: 11240, borderline nodes: 1052, overall activation: 4840.497, activation diff: 1466.120, ratio: 0.303
#   pulse 6: activated nodes: 11279, borderline nodes: 523, overall activation: 5447.402, activation diff: 606.905, ratio: 0.111
#   pulse 7: activated nodes: 11298, borderline nodes: 478, overall activation: 5665.093, activation diff: 217.691, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11298
#   final overall activation: 5665.1
#   number of spread. activ. pulses: 7
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9987939   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9985497   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9981811   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9980868   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972427   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99569094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.74855393   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.74852014   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.74842614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.74841917   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7483804   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7483531   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.7483419   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7483311   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.74833083   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7482783   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   17   0.74827063   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.74825954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7482545   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.74824256   REFERENCES:SIMDATES
