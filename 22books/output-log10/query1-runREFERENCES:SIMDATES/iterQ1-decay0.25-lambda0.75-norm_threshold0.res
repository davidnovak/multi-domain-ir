###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.574, activation diff: 6.574, ratio: 0.687
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 133.629, activation diff: 124.807, ratio: 0.934
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 328.234, activation diff: 194.712, ratio: 0.593
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 864.954, activation diff: 536.720, ratio: 0.621
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1571.578, activation diff: 706.624, ratio: 0.450
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2304.197, activation diff: 732.619, ratio: 0.318
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2975.000, activation diff: 670.804, ratio: 0.225
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 3551.986, activation diff: 576.986, ratio: 0.162
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 4031.415, activation diff: 479.428, ratio: 0.119
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 4421.643, activation diff: 390.228, ratio: 0.088
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 4735.130, activation diff: 313.487, ratio: 0.066
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 4984.768, activation diff: 249.638, ratio: 0.050
#   pulse 13: activated nodes: 11328, borderline nodes: 416, overall activation: 5182.349, activation diff: 197.581, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5182.3
#   number of spread. activ. pulses: 13
#   running time: 509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98651886   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98277867   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97980905   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.97959733   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9761992   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96810997   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.71789193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.71781445   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.7168807   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.71651316   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7164755   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.71641874   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.716289   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7156942   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.71549135   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7153692   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.71528614   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.71525735   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   19   0.7152463   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   20   0.71517956   REFERENCES:SIMDATES
