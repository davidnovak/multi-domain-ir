###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.606, activation diff: 20.606, ratio: 1.411
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 663.703, activation diff: 678.309, ratio: 1.022
#   pulse 3: activated nodes: 8171, borderline nodes: 4081, overall activation: 88.838, activation diff: 752.541, ratio: 8.471
#   pulse 4: activated nodes: 8442, borderline nodes: 4286, overall activation: 2092.279, activation diff: 2175.499, ratio: 1.040
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 101.289, activation diff: 2176.426, ratio: 21.487
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2108.772, activation diff: 2176.258, ratio: 1.032
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 264.761, activation diff: 2012.856, ratio: 7.603
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2168.534, activation diff: 1953.155, ratio: 0.901
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2114.049, activation diff: 103.900, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2114.0
#   number of spread. activ. pulses: 9
#   running time: 600

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9999993   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999331   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9996294   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9984861   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972047   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98202133   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49999955   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.49999878   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49998373   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.499962   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.4999558   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.49994126   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.49992585   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   14   0.49992415   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   15   0.49992394   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.4999066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   17   0.499891   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_177   18   0.49988678   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   19   0.49986437   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.4998548   REFERENCES:SIMDATES
