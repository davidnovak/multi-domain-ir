###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.617, activation diff: 17.617, ratio: 1.205
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 915.895, activation diff: 917.180, ratio: 1.001
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 1222.303, activation diff: 1020.817, ratio: 0.835
#   pulse 4: activated nodes: 10704, borderline nodes: 2746, overall activation: 4888.316, activation diff: 3833.288, ratio: 0.784
#   pulse 5: activated nodes: 11257, borderline nodes: 982, overall activation: 7286.347, activation diff: 2401.630, ratio: 0.330
#   pulse 6: activated nodes: 11333, borderline nodes: 110, overall activation: 8288.985, activation diff: 1002.674, ratio: 0.121
#   pulse 7: activated nodes: 11346, borderline nodes: 33, overall activation: 8649.502, activation diff: 360.517, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11346
#   final overall activation: 8649.5
#   number of spread. activ. pulses: 7
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9987939   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9985503   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99818397   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99808735   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972496   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9957304   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8982656   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.89822406   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.8981294   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.89810306   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.8980982   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   12   0.89809155   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.8980664   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.8980099   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.898005   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   16   0.89795095   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.89794743   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.89792645   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_134   19   0.897925   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.89792395   REFERENCES:SIMDATES
