###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.723, activation diff: 19.723, ratio: 1.179
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1008.289, activation diff: 1005.933, ratio: 0.998
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1371.559, activation diff: 713.024, ratio: 0.520
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3840.066, activation diff: 2468.507, ratio: 0.643
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5156.362, activation diff: 1316.296, ratio: 0.255
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 5644.829, activation diff: 488.467, ratio: 0.087
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5811.068, activation diff: 166.239, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5811.1
#   number of spread. activ. pulses: 7
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993937   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9990836   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9988014   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99874425   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9978033   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9965158   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74906075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.74904567   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.74887717   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.74887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7488638   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7488376   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.7488105   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7487936   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7487738   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   16   0.7487625   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.74874854   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.7487219   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7487215   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.748717   REFERENCES:SIMDATES
