###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.415, activation diff: 4.415, ratio: 0.595
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 12.693, activation diff: 6.926, ratio: 0.546
#   pulse 3: activated nodes: 6150, borderline nodes: 5201, overall activation: 29.386, activation diff: 17.592, ratio: 0.599
#   pulse 4: activated nodes: 6688, borderline nodes: 5018, overall activation: 82.761, activation diff: 53.653, ratio: 0.648
#   pulse 5: activated nodes: 7316, borderline nodes: 4694, overall activation: 204.716, activation diff: 121.996, ratio: 0.596
#   pulse 6: activated nodes: 8019, borderline nodes: 4382, overall activation: 393.958, activation diff: 189.242, ratio: 0.480
#   pulse 7: activated nodes: 8529, borderline nodes: 3998, overall activation: 622.910, activation diff: 228.952, ratio: 0.368
#   pulse 8: activated nodes: 8772, borderline nodes: 3749, overall activation: 860.783, activation diff: 237.873, ratio: 0.276
#   pulse 9: activated nodes: 8883, borderline nodes: 3651, overall activation: 1085.413, activation diff: 224.630, ratio: 0.207
#   pulse 10: activated nodes: 8922, borderline nodes: 3621, overall activation: 1284.360, activation diff: 198.947, ratio: 0.155
#   pulse 11: activated nodes: 8931, borderline nodes: 3611, overall activation: 1453.632, activation diff: 169.271, ratio: 0.116
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1594.109, activation diff: 140.477, ratio: 0.088
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1708.824, activation diff: 114.715, ratio: 0.067
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 1801.469, activation diff: 92.645, ratio: 0.051
#   pulse 15: activated nodes: 8935, borderline nodes: 3614, overall activation: 1875.694, activation diff: 74.225, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1875.7
#   number of spread. activ. pulses: 15
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9825692   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97768044   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97464705   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9743667   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.95556   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94912875   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   7   0.48155427   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   8   0.48141277   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.48116738   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.48113796   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   11   0.48061964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.48052424   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   13   0.4803274   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_242   14   0.48018512   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_37   15   0.48016742   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   16   0.48012096   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.48008886   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   18   0.48007217   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.48005414   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.47989956   REFERENCES:SIMDATES
