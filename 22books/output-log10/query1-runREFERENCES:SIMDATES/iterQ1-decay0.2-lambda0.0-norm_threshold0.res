###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 20.297, activation diff: 26.297, ratio: 1.296
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1773.781, activation diff: 1793.446, ratio: 1.011
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1195.062, activation diff: 2918.978, ratio: 2.443
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4930.679, activation diff: 5820.556, ratio: 1.180
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5343.844, activation diff: 3221.130, ratio: 0.603
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6628.250, activation diff: 1691.149, ratio: 0.255
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 6835.601, activation diff: 248.704, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6835.6
#   number of spread. activ. pulses: 7
#   running time: 538

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999922   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999918   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999248   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99913955   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9983506   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   7   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   8   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   9   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   10   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   11   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   12   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_579   13   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   14   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_597   15   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   16   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_535   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_532   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_506   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   20   0.8   REFERENCES:SIMDATES
