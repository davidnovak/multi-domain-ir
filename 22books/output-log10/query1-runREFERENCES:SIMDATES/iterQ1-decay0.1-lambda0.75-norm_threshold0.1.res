###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.872, activation diff: 5.872, ratio: 0.662
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 87.832, activation diff: 79.965, ratio: 0.910
#   pulse 3: activated nodes: 7994, borderline nodes: 4268, overall activation: 226.402, activation diff: 138.830, ratio: 0.613
#   pulse 4: activated nodes: 10060, borderline nodes: 5110, overall activation: 791.284, activation diff: 564.883, ratio: 0.714
#   pulse 5: activated nodes: 11006, borderline nodes: 2838, overall activation: 1698.745, activation diff: 907.461, ratio: 0.534
#   pulse 6: activated nodes: 11209, borderline nodes: 989, overall activation: 2786.954, activation diff: 1088.209, ratio: 0.390
#   pulse 7: activated nodes: 11287, borderline nodes: 316, overall activation: 3855.231, activation diff: 1068.277, ratio: 0.277
#   pulse 8: activated nodes: 11327, borderline nodes: 141, overall activation: 4807.451, activation diff: 952.220, ratio: 0.198
#   pulse 9: activated nodes: 11333, borderline nodes: 96, overall activation: 5614.928, activation diff: 807.476, ratio: 0.144
#   pulse 10: activated nodes: 11336, borderline nodes: 35, overall activation: 6280.591, activation diff: 665.663, ratio: 0.106
#   pulse 11: activated nodes: 11340, borderline nodes: 21, overall activation: 6819.773, activation diff: 539.181, ratio: 0.079
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 7251.410, activation diff: 431.637, ratio: 0.060
#   pulse 13: activated nodes: 11342, borderline nodes: 11, overall activation: 7594.220, activation diff: 342.810, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 7594.2
#   number of spread. activ. pulses: 13
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9841774   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97998697   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.976719   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9745549   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9732131   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9625736   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8589205   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8589071   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.8580859   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.85725874   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   11   0.8572438   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.8570047   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.85664046   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.8565604   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.85621434   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   16   0.8561653   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.85612434   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.85598326   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.8558538   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.85583353   REFERENCES:SIMDATES
