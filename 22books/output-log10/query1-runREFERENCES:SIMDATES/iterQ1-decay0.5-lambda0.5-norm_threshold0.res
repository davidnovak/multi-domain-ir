###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.149, activation diff: 13.149, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 326.531, activation diff: 313.383, ratio: 0.960
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 528.088, activation diff: 201.557, ratio: 0.382
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1156.293, activation diff: 628.204, ratio: 0.543
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1616.168, activation diff: 459.875, ratio: 0.285
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1899.824, activation diff: 283.656, ratio: 0.149
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2064.307, activation diff: 164.483, ratio: 0.080
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2156.972, activation diff: 92.665, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2157.0
#   number of spread. activ. pulses: 8
#   running time: 409

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9945476   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99271965   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9909787   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9906739   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.988912   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98390365   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49344325   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49336958   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.4929134   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.492836   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.4926314   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.49257112   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49252748   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49226874   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.4922448   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   16   0.49218136   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.49211964   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.49209166   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   19   0.49200034   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.49197084   REFERENCES:SIMDATES
