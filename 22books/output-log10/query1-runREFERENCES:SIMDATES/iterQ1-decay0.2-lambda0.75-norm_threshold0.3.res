###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 7.415, activation diff: 4.415, ratio: 0.595
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 15.759, activation diff: 9.992, ratio: 0.634
#   pulse 3: activated nodes: 6150, borderline nodes: 5201, overall activation: 42.565, activation diff: 27.687, ratio: 0.650
#   pulse 4: activated nodes: 6707, borderline nodes: 5035, overall activation: 152.953, activation diff: 110.612, ratio: 0.723
#   pulse 5: activated nodes: 8184, borderline nodes: 5291, overall activation: 411.008, activation diff: 258.056, ratio: 0.628
#   pulse 6: activated nodes: 9686, borderline nodes: 5199, overall activation: 874.052, activation diff: 463.044, ratio: 0.530
#   pulse 7: activated nodes: 10635, borderline nodes: 3951, overall activation: 1523.401, activation diff: 649.349, ratio: 0.426
#   pulse 8: activated nodes: 11044, borderline nodes: 2273, overall activation: 2260.803, activation diff: 737.403, ratio: 0.326
#   pulse 9: activated nodes: 11193, borderline nodes: 1186, overall activation: 2988.145, activation diff: 727.341, ratio: 0.243
#   pulse 10: activated nodes: 11235, borderline nodes: 675, overall activation: 3648.161, activation diff: 660.016, ratio: 0.181
#   pulse 11: activated nodes: 11258, borderline nodes: 435, overall activation: 4218.330, activation diff: 570.169, ratio: 0.135
#   pulse 12: activated nodes: 11305, borderline nodes: 317, overall activation: 4696.295, activation diff: 477.965, ratio: 0.102
#   pulse 13: activated nodes: 11310, borderline nodes: 236, overall activation: 5089.437, activation diff: 393.142, ratio: 0.077
#   pulse 14: activated nodes: 11321, borderline nodes: 180, overall activation: 5408.866, activation diff: 319.429, ratio: 0.059
#   pulse 15: activated nodes: 11324, borderline nodes: 116, overall activation: 5666.304, activation diff: 257.437, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11324
#   final overall activation: 5666.3
#   number of spread. activ. pulses: 15
#   running time: 561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98401046   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9798507   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9773313   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9766405   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.9631731   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9566177   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   7   0.77221674   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.77216476   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   9   0.77195483   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.7718653   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.7718456   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.7716921   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   13   0.77117586   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7710909   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   15   0.7710593   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7708836   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7706028   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   18   0.77059317   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.77057564   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_37   20   0.77036595   REFERENCES:SIMDATES
