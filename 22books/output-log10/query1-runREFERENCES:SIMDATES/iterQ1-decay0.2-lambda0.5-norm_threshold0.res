###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.149, activation diff: 13.149, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 513.845, activation diff: 500.697, ratio: 0.974
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1054.466, activation diff: 540.621, ratio: 0.513
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2875.235, activation diff: 1820.769, ratio: 0.633
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4426.787, activation diff: 1551.551, ratio: 0.350
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 5448.083, activation diff: 1021.296, ratio: 0.187
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 6059.577, activation diff: 611.494, ratio: 0.101
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 6412.035, activation diff: 352.459, ratio: 0.055
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 6611.362, activation diff: 199.326, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6611.4
#   number of spread. activ. pulses: 9
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9972739   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99636364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9954744   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99534655   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9940338   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9912713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79477715   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7947155   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7943516   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.7942821   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7942281   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7941002   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.7940625   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.79399514   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   15   0.79397345   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.7939216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.79390365   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7938431   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.7937265   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.79372144   REFERENCES:SIMDATES
