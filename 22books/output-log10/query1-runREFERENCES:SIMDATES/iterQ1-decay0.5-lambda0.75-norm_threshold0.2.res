###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.152, activation diff: 5.152, ratio: 0.632
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 27.428, activation diff: 20.569, ratio: 0.750
#   pulse 3: activated nodes: 6984, borderline nodes: 4856, overall activation: 66.532, activation diff: 39.598, ratio: 0.595
#   pulse 4: activated nodes: 7476, borderline nodes: 4676, overall activation: 197.869, activation diff: 131.351, ratio: 0.664
#   pulse 5: activated nodes: 8402, borderline nodes: 4062, overall activation: 405.278, activation diff: 207.409, ratio: 0.512
#   pulse 6: activated nodes: 8733, borderline nodes: 3781, overall activation: 654.747, activation diff: 249.469, ratio: 0.381
#   pulse 7: activated nodes: 8887, borderline nodes: 3644, overall activation: 908.263, activation diff: 253.516, ratio: 0.279
#   pulse 8: activated nodes: 8935, borderline nodes: 3615, overall activation: 1141.504, activation diff: 233.241, ratio: 0.204
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1343.885, activation diff: 202.381, ratio: 0.151
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1513.606, activation diff: 169.722, ratio: 0.112
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1653.005, activation diff: 139.398, ratio: 0.084
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1765.948, activation diff: 112.943, ratio: 0.064
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1856.594, activation diff: 90.646, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1856.6
#   number of spread. activ. pulses: 13
#   running time: 477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97974896   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.97352827   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9694507   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.96701163   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.95765334   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.94452524   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4735639   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   8   0.4732036   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47308117   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.47278967   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   11   0.47274873   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.47235042   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.47180843   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   14   0.47175285   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   15   0.47173882   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.47171462   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   17   0.4715279   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.4713468   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.47133523   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.47129124   REFERENCES:SIMDATES
