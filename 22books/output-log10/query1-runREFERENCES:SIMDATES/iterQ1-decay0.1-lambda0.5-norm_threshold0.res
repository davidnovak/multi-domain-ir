###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.149, activation diff: 13.149, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 576.283, activation diff: 563.135, ratio: 0.977
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1257.239, activation diff: 680.956, ratio: 0.542
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3634.305, activation diff: 2377.065, ratio: 0.654
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5707.513, activation diff: 2073.208, ratio: 0.363
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 7068.670, activation diff: 1361.157, ratio: 0.193
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7878.493, activation diff: 809.824, ratio: 0.103
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 8341.561, activation diff: 463.068, ratio: 0.056
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 8601.774, activation diff: 260.213, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 8601.8
#   number of spread. activ. pulses: 9
#   running time: 469

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.997274   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9963651   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9954791   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9953482   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99404156   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9913049   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.894128   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8940601   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.89366347   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.8935783   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.8935076   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.8933683   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.8933234   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.8933109   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   15   0.893296   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.8931809   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   17   0.89315605   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.8931041   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.8929734   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.89295566   REFERENCES:SIMDATES
