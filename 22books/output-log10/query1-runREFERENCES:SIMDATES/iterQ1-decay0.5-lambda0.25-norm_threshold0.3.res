###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.246, activation diff: 13.246, ratio: 1.293
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 238.762, activation diff: 241.229, ratio: 1.010
#   pulse 3: activated nodes: 7559, borderline nodes: 4599, overall activation: 116.320, activation diff: 207.057, ratio: 1.780
#   pulse 4: activated nodes: 7829, borderline nodes: 4806, overall activation: 1298.989, activation diff: 1211.134, ratio: 0.932
#   pulse 5: activated nodes: 8911, borderline nodes: 3643, overall activation: 1632.480, activation diff: 380.731, ratio: 0.233
#   pulse 6: activated nodes: 8912, borderline nodes: 3641, overall activation: 1964.824, activation diff: 332.343, ratio: 0.169
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2091.028, activation diff: 126.205, ratio: 0.060
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2132.777, activation diff: 41.749, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2132.8
#   number of spread. activ. pulses: 8
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99937975   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99930644   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99909925   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9986408   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9979695   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9958005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49960375   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.4995975   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4995568   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4995513   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49954414   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.4995398   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   13   0.49953246   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   14   0.49951068   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   15   0.49947   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.4994623   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.49945378   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.49944293   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.49943653   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.49942687   REFERENCES:SIMDATES
