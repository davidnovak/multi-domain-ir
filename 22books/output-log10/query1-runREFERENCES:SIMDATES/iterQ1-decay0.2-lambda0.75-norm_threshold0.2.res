###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.152, activation diff: 5.152, ratio: 0.632
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 38.467, activation diff: 31.608, ratio: 0.822
#   pulse 3: activated nodes: 6984, borderline nodes: 4856, overall activation: 101.065, activation diff: 63.092, ratio: 0.624
#   pulse 4: activated nodes: 8510, borderline nodes: 5632, overall activation: 367.502, activation diff: 266.437, ratio: 0.725
#   pulse 5: activated nodes: 10024, borderline nodes: 4858, overall activation: 847.997, activation diff: 480.494, ratio: 0.567
#   pulse 6: activated nodes: 10788, borderline nodes: 3498, overall activation: 1541.924, activation diff: 693.927, ratio: 0.450
#   pulse 7: activated nodes: 11144, borderline nodes: 1684, overall activation: 2321.014, activation diff: 779.091, ratio: 0.336
#   pulse 8: activated nodes: 11232, borderline nodes: 810, overall activation: 3076.268, activation diff: 755.254, ratio: 0.246
#   pulse 9: activated nodes: 11258, borderline nodes: 457, overall activation: 3751.437, activation diff: 675.169, ratio: 0.180
#   pulse 10: activated nodes: 11306, borderline nodes: 296, overall activation: 4327.755, activation diff: 576.318, ratio: 0.133
#   pulse 11: activated nodes: 11321, borderline nodes: 176, overall activation: 4806.315, activation diff: 478.561, ratio: 0.100
#   pulse 12: activated nodes: 11324, borderline nodes: 131, overall activation: 5197.048, activation diff: 390.733, ratio: 0.075
#   pulse 13: activated nodes: 11329, borderline nodes: 76, overall activation: 5512.512, activation diff: 315.463, ratio: 0.057
#   pulse 14: activated nodes: 11331, borderline nodes: 60, overall activation: 5765.233, activation diff: 252.721, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5765.2
#   number of spread. activ. pulses: 14
#   running time: 558

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98526585   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98116064   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97839046   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.97602946   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.97164136   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96195143   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7696434   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7690622   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.76901275   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.76879036   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.76867926   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.7679093   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.767811   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.7677767   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.76766175   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   16   0.76764166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.76751965   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.7674834   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_156   19   0.76730907   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.7672206   REFERENCES:SIMDATES
