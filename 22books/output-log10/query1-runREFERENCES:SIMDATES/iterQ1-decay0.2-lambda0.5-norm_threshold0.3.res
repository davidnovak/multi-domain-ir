###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.830, activation diff: 8.830, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 127.370, activation diff: 122.158, ratio: 0.959
#   pulse 3: activated nodes: 7110, borderline nodes: 4805, overall activation: 186.806, activation diff: 66.557, ratio: 0.356
#   pulse 4: activated nodes: 8672, borderline nodes: 6034, overall activation: 1334.234, activation diff: 1147.473, ratio: 0.860
#   pulse 5: activated nodes: 10355, borderline nodes: 4008, overall activation: 2612.286, activation diff: 1278.052, ratio: 0.489
#   pulse 6: activated nodes: 11000, borderline nodes: 1841, overall activation: 3994.017, activation diff: 1381.730, ratio: 0.346
#   pulse 7: activated nodes: 11238, borderline nodes: 721, overall activation: 5009.080, activation diff: 1015.063, ratio: 0.203
#   pulse 8: activated nodes: 11296, borderline nodes: 359, overall activation: 5657.145, activation diff: 648.065, ratio: 0.115
#   pulse 9: activated nodes: 11314, borderline nodes: 222, overall activation: 6048.254, activation diff: 391.109, ratio: 0.065
#   pulse 10: activated nodes: 11319, borderline nodes: 146, overall activation: 6278.655, activation diff: 230.401, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 6278.7
#   number of spread. activ. pulses: 10
#   running time: 487

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9965011   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9960612   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9953047   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9942075   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.99322355   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9895152   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.79541147   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.79530394   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7952609   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   10   0.79523623   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.79517186   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.795146   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7951023   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   14   0.7950298   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.795028   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.79497594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.7949743   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   18   0.7949705   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.79496646   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.79493344   REFERENCES:SIMDATES
