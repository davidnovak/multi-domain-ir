###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.745, activation diff: 11.745, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 227.388, activation diff: 216.809, ratio: 0.953
#   pulse 3: activated nodes: 8336, borderline nodes: 3670, overall activation: 354.122, activation diff: 127.443, ratio: 0.360
#   pulse 4: activated nodes: 8607, borderline nodes: 3874, overall activation: 996.660, activation diff: 642.539, ratio: 0.645
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1491.298, activation diff: 494.638, ratio: 0.332
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1808.139, activation diff: 316.841, ratio: 0.175
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1995.156, activation diff: 187.016, ratio: 0.094
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2101.650, activation diff: 106.495, ratio: 0.051
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2161.097, activation diff: 59.446, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2161.1
#   number of spread. activ. pulses: 9
#   running time: 432

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99650776   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99544406   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99434805   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.993853   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99287593   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98901534   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49607652   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49599767   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.4958012   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.49572515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.4956501   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.4954582   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49544084   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4953981   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.49539208   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   16   0.49534047   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.49528852   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.49526846   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.49520856   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   20   0.49520132   REFERENCES:SIMDATES
