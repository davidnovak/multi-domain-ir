###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.574, activation diff: 6.574, ratio: 0.687
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 93.040, activation diff: 84.217, ratio: 0.905
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 204.838, activation diff: 111.906, ratio: 0.546
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 446.374, activation diff: 241.536, ratio: 0.541
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 729.346, activation diff: 282.973, ratio: 0.388
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1003.724, activation diff: 274.377, ratio: 0.273
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1246.567, activation diff: 242.843, ratio: 0.195
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1451.710, activation diff: 205.143, ratio: 0.141
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1620.535, activation diff: 168.825, ratio: 0.104
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1757.256, activation diff: 136.721, ratio: 0.078
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1866.805, activation diff: 109.550, ratio: 0.059
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1953.930, activation diff: 87.124, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1953.9
#   number of spread. activ. pulses: 12
#   running time: 583

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9819699   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9766972   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9725142   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.97180545   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9680914   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95626515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.4710057   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.4709018   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4700115   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.46988   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.46971917   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.46954292   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4693995   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   14   0.4687512   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   15   0.46855187   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.4684203   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.46825826   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   18   0.4682301   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.46817133   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.46812597   REFERENCES:SIMDATES
