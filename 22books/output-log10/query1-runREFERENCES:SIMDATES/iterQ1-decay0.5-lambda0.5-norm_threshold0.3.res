###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.830, activation diff: 8.830, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 82.309, activation diff: 77.096, ratio: 0.937
#   pulse 3: activated nodes: 7110, borderline nodes: 4805, overall activation: 111.706, activation diff: 34.126, ratio: 0.306
#   pulse 4: activated nodes: 7300, borderline nodes: 4969, overall activation: 619.884, activation diff: 508.229, ratio: 0.820
#   pulse 5: activated nodes: 8635, borderline nodes: 3800, overall activation: 1078.735, activation diff: 458.850, ratio: 0.425
#   pulse 6: activated nodes: 8789, borderline nodes: 3749, overall activation: 1486.402, activation diff: 407.667, ratio: 0.274
#   pulse 7: activated nodes: 8929, borderline nodes: 3611, overall activation: 1761.744, activation diff: 275.342, ratio: 0.156
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1929.607, activation diff: 167.863, ratio: 0.087
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 2027.335, activation diff: 97.728, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2027.3
#   number of spread. activ. pulses: 9
#   running time: 431

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99297863   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99177015   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9901665   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.9891971   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.98241276   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9783515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49414676   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   8   0.49398565   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.4938916   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.4938097   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   11   0.4937756   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   12   0.4937374   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   13   0.4936062   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   14   0.49355525   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   15   0.4934336   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   16   0.4934132   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.49337196   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.49332553   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.49326062   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   20   0.49324757   REFERENCES:SIMDATES
