###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.745, activation diff: 11.745, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 399.781, activation diff: 389.202, ratio: 0.974
#   pulse 3: activated nodes: 8336, borderline nodes: 3670, overall activation: 801.613, activation diff: 403.110, ratio: 0.503
#   pulse 4: activated nodes: 10502, borderline nodes: 3443, overall activation: 3019.286, activation diff: 2217.673, ratio: 0.735
#   pulse 5: activated nodes: 11218, borderline nodes: 1370, overall activation: 5154.387, activation diff: 2135.101, ratio: 0.414
#   pulse 6: activated nodes: 11312, borderline nodes: 163, overall activation: 6658.473, activation diff: 1504.086, ratio: 0.226
#   pulse 7: activated nodes: 11335, borderline nodes: 54, overall activation: 7584.682, activation diff: 926.209, ratio: 0.122
#   pulse 8: activated nodes: 11342, borderline nodes: 17, overall activation: 8126.638, activation diff: 541.956, ratio: 0.067
#   pulse 9: activated nodes: 11347, borderline nodes: 14, overall activation: 8436.247, activation diff: 309.609, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11347
#   final overall activation: 8436.2
#   number of spread. activ. pulses: 9
#   running time: 487

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99650836   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954698   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99441826   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99392664   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9929867   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9895991   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.8929813   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.89281535   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.8925602   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8923987   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.8923732   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   12   0.89229167   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   13   0.89228964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8920139   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.89199054   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.8919156   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.891904   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   18   0.8918785   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.89186895   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_134   20   0.89184916   REFERENCES:SIMDATES
