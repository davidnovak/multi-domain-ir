###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.723, activation diff: 19.723, ratio: 1.179
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1074.674, activation diff: 1072.318, ratio: 0.998
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1545.190, activation diff: 843.044, ratio: 0.546
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 4392.116, activation diff: 2846.926, ratio: 0.648
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 5967.344, activation diff: 1575.228, ratio: 0.264
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 6557.999, activation diff: 590.654, ratio: 0.090
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 6760.103, activation diff: 202.104, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 6760.1
#   number of spread. activ. pulses: 7
#   running time: 434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9993937   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99908364   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99880147   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9987448   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99780434   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99651927   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7990008   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.79898274   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7988075   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.79879755   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.79878825   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7987608   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.79873145   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.7987183   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   15   0.7987037   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   16   0.79869556   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   17   0.79869217   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.7986418   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.79864097   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   20   0.7986389   REFERENCES:SIMDATES
