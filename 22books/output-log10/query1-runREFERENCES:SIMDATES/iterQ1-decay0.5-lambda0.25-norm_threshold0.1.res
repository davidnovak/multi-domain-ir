###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.617, activation diff: 17.617, ratio: 1.205
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 513.039, activation diff: 514.323, ratio: 1.003
#   pulse 3: activated nodes: 8528, borderline nodes: 3466, overall activation: 374.455, activation diff: 259.111, ratio: 0.692
#   pulse 4: activated nodes: 8799, borderline nodes: 3671, overall activation: 1585.122, activation diff: 1211.316, ratio: 0.764
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2013.526, activation diff: 428.405, ratio: 0.213
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2164.669, activation diff: 151.143, ratio: 0.070
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 2212.351, activation diff: 47.682, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2212.4
#   number of spread. activ. pulses: 7
#   running time: 398

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99879384   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9985471   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99816936   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99808395   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972116   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9955381   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.49903175   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.49901322   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.49892664   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.4989072   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.49889728   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.49883467   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.49881607   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   14   0.49878436   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.49878138   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.49874872   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.49874777   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.49874234   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   19   0.49873692   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.49873328   REFERENCES:SIMDATES
