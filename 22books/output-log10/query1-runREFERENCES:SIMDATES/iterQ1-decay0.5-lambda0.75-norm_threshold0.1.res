###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.872, activation diff: 5.872, ratio: 0.662
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 53.448, activation diff: 45.581, ratio: 0.853
#   pulse 3: activated nodes: 7994, borderline nodes: 4268, overall activation: 124.513, activation diff: 71.325, ratio: 0.573
#   pulse 4: activated nodes: 8452, borderline nodes: 4192, overall activation: 322.343, activation diff: 197.830, ratio: 0.614
#   pulse 5: activated nodes: 8908, borderline nodes: 3627, overall activation: 581.547, activation diff: 259.204, ratio: 0.446
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 853.154, activation diff: 271.607, ratio: 0.318
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1104.924, activation diff: 251.770, ratio: 0.228
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1323.507, activation diff: 218.583, ratio: 0.165
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1506.543, activation diff: 183.036, ratio: 0.121
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1656.556, activation diff: 150.014, ratio: 0.091
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1777.821, activation diff: 121.265, ratio: 0.068
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1874.925, activation diff: 97.104, ratio: 0.052
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 1952.153, activation diff: 77.228, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1952.2
#   number of spread. activ. pulses: 13
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9839959   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.979256   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.97561604   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.97223556   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.972137   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9588617   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.47650114   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.47619402   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.47579673   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   10   0.47576237   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.47529107   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.47526282   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.47501338   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   14   0.47487563   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.47470364   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   16   0.47442362   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   17   0.47439012   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.47438508   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   19   0.47433907   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.4742585   REFERENCES:SIMDATES
