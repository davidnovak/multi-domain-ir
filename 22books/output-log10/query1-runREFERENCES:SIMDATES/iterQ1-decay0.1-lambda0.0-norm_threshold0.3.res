###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.661, activation diff: 17.661, ratio: 1.515
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 847.283, activation diff: 858.944, ratio: 1.014
#   pulse 3: activated nodes: 7814, borderline nodes: 4516, overall activation: 612.127, activation diff: 1459.410, ratio: 2.384
#   pulse 4: activated nodes: 9960, borderline nodes: 4880, overall activation: 5560.617, activation diff: 6150.581, ratio: 1.106
#   pulse 5: activated nodes: 11168, borderline nodes: 2139, overall activation: 2632.418, activation diff: 8056.673, ratio: 3.061
#   pulse 6: activated nodes: 11305, borderline nodes: 223, overall activation: 6232.382, activation diff: 8227.438, ratio: 1.320
#   pulse 7: activated nodes: 11335, borderline nodes: 52, overall activation: 7380.290, activation diff: 3678.833, ratio: 0.498
#   pulse 8: activated nodes: 11339, borderline nodes: 18, overall activation: 8479.141, activation diff: 1508.549, ratio: 0.178
#   pulse 9: activated nodes: 11343, borderline nodes: 13, overall activation: 8668.341, activation diff: 264.131, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11343
#   final overall activation: 8668.3
#   number of spread. activ. pulses: 9
#   running time: 528

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999895   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99998903   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99989873   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.998839   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99777436   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   7   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   8   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   10   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   11   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   12   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   13   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   15   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   16   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_517   17   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   18   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_514   20   0.9   REFERENCES:SIMDATES
