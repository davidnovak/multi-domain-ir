###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.303, activation diff: 10.303, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 254.364, activation diff: 246.764, ratio: 0.970
#   pulse 3: activated nodes: 7800, borderline nodes: 4486, overall activation: 450.338, activation diff: 200.558, ratio: 0.445
#   pulse 4: activated nodes: 9824, borderline nodes: 5434, overall activation: 2375.318, activation diff: 1924.980, ratio: 0.810
#   pulse 5: activated nodes: 11064, borderline nodes: 2871, overall activation: 4431.437, activation diff: 2056.119, ratio: 0.464
#   pulse 6: activated nodes: 11262, borderline nodes: 597, overall activation: 6102.728, activation diff: 1671.292, ratio: 0.274
#   pulse 7: activated nodes: 11325, borderline nodes: 175, overall activation: 7194.550, activation diff: 1091.821, ratio: 0.152
#   pulse 8: activated nodes: 11335, borderline nodes: 79, overall activation: 7856.118, activation diff: 661.569, ratio: 0.084
#   pulse 9: activated nodes: 11339, borderline nodes: 24, overall activation: 8243.112, activation diff: 386.993, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 8243.1
#   number of spread. activ. pulses: 9
#   running time: 483

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9950644   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9941205   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9929346   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.9917536   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99161077   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9869847   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.89144313   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.8911943   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.891145   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.8910083   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   11   0.89099133   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.89098203   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.8908156   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.8906083   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   15   0.8905431   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_134   16   0.8904954   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   17   0.89049494   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_35   18   0.89045066   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.89041483   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.8903838   REFERENCES:SIMDATES
