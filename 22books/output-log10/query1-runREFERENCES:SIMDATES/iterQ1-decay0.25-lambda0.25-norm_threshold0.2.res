###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.455, activation diff: 15.455, ratio: 1.241
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 539.980, activation diff: 542.866, ratio: 1.005
#   pulse 3: activated nodes: 8057, borderline nodes: 4319, overall activation: 493.045, activation diff: 555.538, ratio: 1.127
#   pulse 4: activated nodes: 10193, borderline nodes: 4552, overall activation: 3009.369, activation diff: 2672.597, ratio: 0.888
#   pulse 5: activated nodes: 11163, borderline nodes: 2082, overall activation: 4477.239, activation diff: 1483.442, ratio: 0.331
#   pulse 6: activated nodes: 11244, borderline nodes: 750, overall activation: 5243.460, activation diff: 766.315, ratio: 0.146
#   pulse 7: activated nodes: 11282, borderline nodes: 580, overall activation: 5539.406, activation diff: 295.947, ratio: 0.053
#   pulse 8: activated nodes: 11283, borderline nodes: 566, overall activation: 5643.329, activation diff: 103.923, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11283
#   final overall activation: 5643.3
#   number of spread. activ. pulses: 8
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9995237   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9994723   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9993303   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9993066   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99833083   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9970757   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7495129   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.7495028   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.74949276   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.74947333   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.74946284   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   12   0.74946225   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   13   0.7494618   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.7494607   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.7494562   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_283   16   0.74944466   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   17   0.74944395   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.74944234   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   19   0.7494419   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   20   0.7494369   REFERENCES:SIMDATES
