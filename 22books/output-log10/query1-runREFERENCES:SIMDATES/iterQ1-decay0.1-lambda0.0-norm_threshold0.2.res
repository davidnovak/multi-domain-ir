###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.606, activation diff: 20.606, ratio: 1.411
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1190.312, activation diff: 1204.918, ratio: 1.012
#   pulse 3: activated nodes: 8171, borderline nodes: 4081, overall activation: 936.597, activation diff: 2126.909, ratio: 2.271
#   pulse 4: activated nodes: 10371, borderline nodes: 3757, overall activation: 5771.787, activation diff: 6660.471, ratio: 1.154
#   pulse 5: activated nodes: 11238, borderline nodes: 1360, overall activation: 2908.746, activation diff: 8139.349, ratio: 2.798
#   pulse 6: activated nodes: 11323, borderline nodes: 127, overall activation: 6632.540, activation diff: 7789.899, ratio: 1.174
#   pulse 7: activated nodes: 11343, borderline nodes: 34, overall activation: 8240.650, activation diff: 2594.695, ratio: 0.315
#   pulse 8: activated nodes: 11346, borderline nodes: 12, overall activation: 8689.930, activation diff: 604.149, ratio: 0.070
#   pulse 9: activated nodes: 11346, borderline nodes: 10, overall activation: 8764.354, activation diff: 107.852, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11346
#   final overall activation: 8764.4
#   number of spread. activ. pulses: 9
#   running time: 627

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999905   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999017   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99990845   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9989495   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99798596   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   7   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   8   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   10   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   11   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   12   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   13   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   15   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_510   16   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_517   17   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_516   18   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_514   20   0.9   REFERENCES:SIMDATES
