###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.745, activation diff: 11.745, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 356.683, activation diff: 346.104, ratio: 0.970
#   pulse 3: activated nodes: 8336, borderline nodes: 3670, overall activation: 674.739, activation diff: 319.192, ratio: 0.473
#   pulse 4: activated nodes: 10489, borderline nodes: 3519, overall activation: 2380.818, activation diff: 1706.078, ratio: 0.717
#   pulse 5: activated nodes: 11210, borderline nodes: 1482, overall activation: 3973.742, activation diff: 1592.925, ratio: 0.401
#   pulse 6: activated nodes: 11300, borderline nodes: 235, overall activation: 5097.072, activation diff: 1123.330, ratio: 0.220
#   pulse 7: activated nodes: 11329, borderline nodes: 118, overall activation: 5791.583, activation diff: 694.512, ratio: 0.120
#   pulse 8: activated nodes: 11336, borderline nodes: 67, overall activation: 6198.923, activation diff: 407.340, ratio: 0.066
#   pulse 9: activated nodes: 11338, borderline nodes: 57, overall activation: 6432.087, activation diff: 233.164, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11338
#   final overall activation: 6432.1
#   number of spread. activ. pulses: 9
#   running time: 454

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99650824   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9954665   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.9944091   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.99391866   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99297214   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9895163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.7937573   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   8   0.79361355   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   9   0.7933673   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   10   0.79324186   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.79320884   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   12   0.79307467   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_136   13   0.79306257   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.79288304   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.7928563   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.79280615   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.7927716   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   18   0.7927678   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_383   19   0.7926848   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.79268026   REFERENCES:SIMDATES
