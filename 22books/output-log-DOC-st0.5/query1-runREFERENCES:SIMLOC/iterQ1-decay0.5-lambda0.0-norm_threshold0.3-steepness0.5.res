###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 155.464, activation diff: 170.394, ratio: 1.096
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 64.939, activation diff: 219.792, ratio: 3.385
#   pulse 4: activated nodes: 10196, borderline nodes: 6801, overall activation: 737.506, activation diff: 792.303, ratio: 1.074
#   pulse 5: activated nodes: 10570, borderline nodes: 4508, overall activation: 267.454, activation diff: 933.261, ratio: 3.489
#   pulse 6: activated nodes: 10759, borderline nodes: 4360, overall activation: 1171.744, activation diff: 1146.919, ratio: 0.979
#   pulse 7: activated nodes: 10857, borderline nodes: 3760, overall activation: 770.040, activation diff: 910.733, ratio: 1.183
#   pulse 8: activated nodes: 10866, borderline nodes: 3702, overall activation: 1564.215, activation diff: 895.106, ratio: 0.572
#   pulse 9: activated nodes: 10876, borderline nodes: 3645, overall activation: 1652.071, activation diff: 221.218, ratio: 0.134
#   pulse 10: activated nodes: 10878, borderline nodes: 3622, overall activation: 1789.745, activation diff: 142.508, ratio: 0.080
#   pulse 11: activated nodes: 10878, borderline nodes: 3595, overall activation: 1819.858, activation diff: 33.035, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10878
#   final overall activation: 1819.9
#   number of spread. activ. pulses: 11
#   running time: 1292

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9652252   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85391235   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8330503   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77376276   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6319747   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.59988856   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46194524   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4523991   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.44626236   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43868172   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.43423682   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.43421257   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.43392673   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4307582   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.42861187   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.42675364   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.4241568   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.4224592   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42004207   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.41790187   REFERENCES:SIMLOC
