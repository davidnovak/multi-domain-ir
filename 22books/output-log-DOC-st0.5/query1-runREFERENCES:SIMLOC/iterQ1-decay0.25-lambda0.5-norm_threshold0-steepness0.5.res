###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 154.709, activation diff: 143.550, ratio: 0.928
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 327.626, activation diff: 173.567, ratio: 0.530
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 943.095, activation diff: 615.474, ratio: 0.653
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 1771.343, activation diff: 828.249, ratio: 0.468
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 2627.952, activation diff: 856.609, ratio: 0.326
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 3319.874, activation diff: 691.922, ratio: 0.208
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 3807.623, activation diff: 487.749, ratio: 0.128
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 4129.140, activation diff: 321.517, ratio: 0.078
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 4333.561, activation diff: 204.421, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 4333.6
#   number of spread. activ. pulses: 10
#   running time: 1282

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96085626   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8522553   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8456856   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7817478   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.73382974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7190563   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.7163236   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7142836   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7129686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7125976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.71249175   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.7123025   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7122965   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7116173   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   15   0.7096456   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.7091284   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.70885134   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.7085043   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.70686495   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.70551026   REFERENCES:SIMLOC
