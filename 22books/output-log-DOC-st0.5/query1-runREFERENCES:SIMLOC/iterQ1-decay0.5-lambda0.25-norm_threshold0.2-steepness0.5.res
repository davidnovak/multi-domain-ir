###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 107.915, activation diff: 108.616, ratio: 1.007
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 95.767, activation diff: 86.539, ratio: 0.904
#   pulse 4: activated nodes: 10318, borderline nodes: 6441, overall activation: 483.483, activation diff: 399.188, ratio: 0.826
#   pulse 5: activated nodes: 10600, borderline nodes: 4551, overall activation: 750.423, activation diff: 270.439, ratio: 0.360
#   pulse 6: activated nodes: 10758, borderline nodes: 4310, overall activation: 1106.179, activation diff: 355.756, ratio: 0.322
#   pulse 7: activated nodes: 10864, borderline nodes: 3708, overall activation: 1381.270, activation diff: 275.091, ratio: 0.199
#   pulse 8: activated nodes: 10876, borderline nodes: 3611, overall activation: 1599.992, activation diff: 218.722, ratio: 0.137
#   pulse 9: activated nodes: 10886, borderline nodes: 3569, overall activation: 1746.987, activation diff: 146.995, ratio: 0.084
#   pulse 10: activated nodes: 10889, borderline nodes: 3553, overall activation: 1830.593, activation diff: 83.606, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10889
#   final overall activation: 1830.6
#   number of spread. activ. pulses: 10
#   running time: 1386

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9665537   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8579272   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8357192   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7808306   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6466918   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6120282   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46321356   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45323488   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.44797212   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.44087455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4369696   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4363602   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4352181   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.4303645   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4295591   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.42779177   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.42634526   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.42534316   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42119676   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.42046365   REFERENCES:SIMLOC
