###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 112.563, activation diff: 114.650, ratio: 1.019
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 100.309, activation diff: 124.423, ratio: 1.240
#   pulse 4: activated nodes: 10634, borderline nodes: 7587, overall activation: 811.887, activation diff: 750.720, ratio: 0.925
#   pulse 5: activated nodes: 10987, borderline nodes: 4131, overall activation: 1352.757, activation diff: 573.641, ratio: 0.424
#   pulse 6: activated nodes: 11303, borderline nodes: 3265, overall activation: 2608.623, activation diff: 1255.888, ratio: 0.481
#   pulse 7: activated nodes: 11393, borderline nodes: 1031, overall activation: 3613.207, activation diff: 1004.584, ratio: 0.278
#   pulse 8: activated nodes: 11417, borderline nodes: 504, overall activation: 4229.789, activation diff: 616.581, ratio: 0.146
#   pulse 9: activated nodes: 11427, borderline nodes: 259, overall activation: 4528.931, activation diff: 299.142, ratio: 0.066
#   pulse 10: activated nodes: 11433, borderline nodes: 192, overall activation: 4664.412, activation diff: 135.481, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11433
#   final overall activation: 4664.4
#   number of spread. activ. pulses: 10
#   running time: 1437

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9676011   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8537319   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84767056   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7939195   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.788512   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   6   0.78316045   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.7825514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.78254896   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7821727   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7817865   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   11   0.78176564   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7816443   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7814385   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.77983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   15   0.7797585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.7796201   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.7792468   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   18   0.7784499   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7781848   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.77696157   REFERENCES:SIMLOC
