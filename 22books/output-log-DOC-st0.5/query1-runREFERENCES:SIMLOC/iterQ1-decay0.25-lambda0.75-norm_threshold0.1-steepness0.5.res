###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 23.500, activation diff: 16.479, ratio: 0.701
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 53.069, activation diff: 30.849, ratio: 0.581
#   pulse 4: activated nodes: 10525, borderline nodes: 6553, overall activation: 124.296, activation diff: 71.925, ratio: 0.579
#   pulse 5: activated nodes: 11027, borderline nodes: 5303, overall activation: 251.178, activation diff: 127.173, ratio: 0.506
#   pulse 6: activated nodes: 11209, borderline nodes: 4078, overall activation: 446.562, activation diff: 195.431, ratio: 0.438
#   pulse 7: activated nodes: 11317, borderline nodes: 2614, overall activation: 715.818, activation diff: 269.256, ratio: 0.376
#   pulse 8: activated nodes: 11389, borderline nodes: 1415, overall activation: 1045.913, activation diff: 330.095, ratio: 0.316
#   pulse 9: activated nodes: 11405, borderline nodes: 847, overall activation: 1417.314, activation diff: 371.401, ratio: 0.262
#   pulse 10: activated nodes: 11419, borderline nodes: 512, overall activation: 1803.747, activation diff: 386.432, ratio: 0.214
#   pulse 11: activated nodes: 11428, borderline nodes: 363, overall activation: 2180.523, activation diff: 376.776, ratio: 0.173
#   pulse 12: activated nodes: 11431, borderline nodes: 241, overall activation: 2529.412, activation diff: 348.889, ratio: 0.138
#   pulse 13: activated nodes: 11434, borderline nodes: 189, overall activation: 2839.826, activation diff: 310.415, ratio: 0.109
#   pulse 14: activated nodes: 11436, borderline nodes: 162, overall activation: 3108.615, activation diff: 268.789, ratio: 0.086
#   pulse 15: activated nodes: 11439, borderline nodes: 147, overall activation: 3337.252, activation diff: 228.636, ratio: 0.069

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 3337.3
#   number of spread. activ. pulses: 15
#   running time: 1591

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91235626   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.76091534   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.7584588   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7041582   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.6858939   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.65991443   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.6513102   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.6495301   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   9   0.6469157   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.644126   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.642599   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   12   0.64236045   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.6404842   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.6401604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.6382973   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.63776165   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   17   0.6373892   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   18   0.6371201   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   19   0.635732   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.63438576   REFERENCES:SIMLOC
