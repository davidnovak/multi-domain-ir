###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 247.481, activation diff: 262.411, ratio: 1.060
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 160.902, activation diff: 407.096, ratio: 2.530
#   pulse 4: activated nodes: 10983, borderline nodes: 7172, overall activation: 1861.447, activation diff: 1978.963, ratio: 1.063
#   pulse 5: activated nodes: 11261, borderline nodes: 2625, overall activation: 1152.861, activation diff: 2625.133, ratio: 2.277
#   pulse 6: activated nodes: 11359, borderline nodes: 1326, overall activation: 3700.068, activation diff: 3274.198, ratio: 0.885
#   pulse 7: activated nodes: 11416, borderline nodes: 489, overall activation: 3715.499, activation diff: 1458.502, ratio: 0.393
#   pulse 8: activated nodes: 11432, borderline nodes: 260, overall activation: 4599.734, activation diff: 999.857, ratio: 0.217
#   pulse 9: activated nodes: 11436, borderline nodes: 190, overall activation: 4711.168, activation diff: 147.299, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 4711.2
#   number of spread. activ. pulses: 9
#   running time: 1327

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9677225   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8557578   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84944636   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79425085   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.78980726   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   6   0.78417575   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.78324336   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7831635   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   9   0.7830988   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.78301024   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.78245515   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7821689   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7819175   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.7809152   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.7805577   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.78031486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.78005546   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.77965945   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7791647   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.77882427   REFERENCES:SIMLOC
