###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 427.134, activation diff: 443.980, ratio: 1.039
#   pulse 3: activated nodes: 9505, borderline nodes: 3995, overall activation: 311.975, activation diff: 718.529, ratio: 2.303
#   pulse 4: activated nodes: 11268, borderline nodes: 3957, overall activation: 2491.915, activation diff: 2559.923, ratio: 1.027
#   pulse 5: activated nodes: 11403, borderline nodes: 846, overall activation: 2176.393, activation diff: 2292.733, ratio: 1.053
#   pulse 6: activated nodes: 11432, borderline nodes: 254, overall activation: 4052.177, activation diff: 2127.477, ratio: 0.525
#   pulse 7: activated nodes: 11441, borderline nodes: 116, overall activation: 4287.975, activation diff: 348.907, ratio: 0.081
#   pulse 8: activated nodes: 11441, borderline nodes: 103, overall activation: 4415.001, activation diff: 136.050, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11441
#   final overall activation: 4415.0
#   number of spread. activ. pulses: 8
#   running time: 1269

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9715392   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8686424   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8670025   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7937904   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7429235   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.735109   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.7311974   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7301762   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.72934276   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7293353   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7285826   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.7284602   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.72824246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.7262949   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   15   0.7259461   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.7253038   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7250824   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.72439855   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   19   0.7229266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.722334   REFERENCES:SIMLOC
