###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 125.950, activation diff: 128.038, ratio: 1.017
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 119.420, activation diff: 146.945, ratio: 1.230
#   pulse 4: activated nodes: 10663, borderline nodes: 7594, overall activation: 1035.227, activation diff: 964.700, ratio: 0.932
#   pulse 5: activated nodes: 11028, borderline nodes: 3742, overall activation: 1783.093, activation diff: 794.630, ratio: 0.446
#   pulse 6: activated nodes: 11322, borderline nodes: 2651, overall activation: 3546.137, activation diff: 1763.070, ratio: 0.497
#   pulse 7: activated nodes: 11408, borderline nodes: 800, overall activation: 4817.356, activation diff: 1271.219, ratio: 0.264
#   pulse 8: activated nodes: 11430, borderline nodes: 324, overall activation: 5518.763, activation diff: 701.407, ratio: 0.127
#   pulse 9: activated nodes: 11443, borderline nodes: 151, overall activation: 5840.924, activation diff: 322.161, ratio: 0.055
#   pulse 10: activated nodes: 11446, borderline nodes: 100, overall activation: 5981.960, activation diff: 141.036, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 5982.0
#   number of spread. activ. pulses: 10
#   running time: 1604

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96817416   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89679635   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89559495   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89434624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89228845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.89195454   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.8916599   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.8915439   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   9   0.89147943   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.8914577   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8914172   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89106596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.8909501   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.8903719   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.8898508   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   16   0.88955   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.88938856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.8891821   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.88913983   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.88903403   REFERENCES:SIMLOC
