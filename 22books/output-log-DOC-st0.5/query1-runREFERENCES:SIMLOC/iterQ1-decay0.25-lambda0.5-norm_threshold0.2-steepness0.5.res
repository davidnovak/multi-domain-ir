###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 58.636, activation diff: 50.363, ratio: 0.859
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 106.879, activation diff: 49.402, ratio: 0.462
#   pulse 4: activated nodes: 10598, borderline nodes: 7481, overall activation: 383.044, activation diff: 276.285, ratio: 0.721
#   pulse 5: activated nodes: 10975, borderline nodes: 4698, overall activation: 804.002, activation diff: 420.958, ratio: 0.524
#   pulse 6: activated nodes: 11278, borderline nodes: 3491, overall activation: 1428.610, activation diff: 624.608, ratio: 0.437
#   pulse 7: activated nodes: 11379, borderline nodes: 1438, overall activation: 2127.060, activation diff: 698.450, ratio: 0.328
#   pulse 8: activated nodes: 11407, borderline nodes: 740, overall activation: 2777.072, activation diff: 650.012, ratio: 0.234
#   pulse 9: activated nodes: 11420, borderline nodes: 428, overall activation: 3287.211, activation diff: 510.139, ratio: 0.155
#   pulse 10: activated nodes: 11429, borderline nodes: 266, overall activation: 3643.042, activation diff: 355.831, ratio: 0.098
#   pulse 11: activated nodes: 11432, borderline nodes: 203, overall activation: 3877.523, activation diff: 234.481, ratio: 0.060
#   pulse 12: activated nodes: 11433, borderline nodes: 178, overall activation: 4027.858, activation diff: 150.335, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11433
#   final overall activation: 4027.9
#   number of spread. activ. pulses: 12
#   running time: 1345

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96355045   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.84655046   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8391907   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7722017   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7378757   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.72303015   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.72014076   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7185681   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.71722066   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.7166651   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.71658456   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.7163131   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.71629244   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7151019   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.71284056   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.71239156   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   17   0.71218956   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.712016   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7105472   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.70905125   REFERENCES:SIMLOC
