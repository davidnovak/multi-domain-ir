###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 13.288, activation diff: 6.718, ratio: 0.506
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 27.373, activation diff: 15.449, ratio: 0.564
#   pulse 4: activated nodes: 8803, borderline nodes: 6572, overall activation: 58.522, activation diff: 32.019, ratio: 0.547
#   pulse 5: activated nodes: 10038, borderline nodes: 6947, overall activation: 122.369, activation diff: 64.304, ratio: 0.525
#   pulse 6: activated nodes: 10743, borderline nodes: 6221, overall activation: 230.277, activation diff: 108.084, ratio: 0.469
#   pulse 7: activated nodes: 11068, borderline nodes: 5224, overall activation: 392.532, activation diff: 162.278, ratio: 0.413
#   pulse 8: activated nodes: 11205, borderline nodes: 4143, overall activation: 617.655, activation diff: 225.123, ratio: 0.364
#   pulse 9: activated nodes: 11290, borderline nodes: 2901, overall activation: 899.664, activation diff: 282.009, ratio: 0.313
#   pulse 10: activated nodes: 11364, borderline nodes: 1816, overall activation: 1224.761, activation diff: 325.097, ratio: 0.265
#   pulse 11: activated nodes: 11395, borderline nodes: 1242, overall activation: 1576.562, activation diff: 351.801, ratio: 0.223
#   pulse 12: activated nodes: 11401, borderline nodes: 850, overall activation: 1934.098, activation diff: 357.536, ratio: 0.185
#   pulse 13: activated nodes: 11412, borderline nodes: 608, overall activation: 2278.365, activation diff: 344.266, ratio: 0.151
#   pulse 14: activated nodes: 11420, borderline nodes: 471, overall activation: 2595.065, activation diff: 316.701, ratio: 0.122
#   pulse 15: activated nodes: 11423, borderline nodes: 348, overall activation: 2875.967, activation diff: 280.902, ratio: 0.098

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11423
#   final overall activation: 2876.0
#   number of spread. activ. pulses: 15
#   running time: 1434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.88918066   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.71685773   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.6994885   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.6657816   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.66571957   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.6415486   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.62069017   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.6205714   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.6201695   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   10   0.61599994   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.61585534   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.6155744   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.6092118   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.60742104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   15   0.60495996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.6044152   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.6039058   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   18   0.598868   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   19   0.5987208   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.59834987   REFERENCES:SIMLOC
