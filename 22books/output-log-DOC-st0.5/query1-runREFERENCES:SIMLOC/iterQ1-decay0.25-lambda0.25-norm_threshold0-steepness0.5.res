###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 328.575, activation diff: 325.352, ratio: 0.990
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 525.109, activation diff: 282.736, ratio: 0.538
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1947.699, activation diff: 1422.625, ratio: 0.730
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3141.930, activation diff: 1194.231, ratio: 0.380
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 3963.333, activation diff: 821.403, ratio: 0.207
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 4361.190, activation diff: 397.857, ratio: 0.091
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 4534.911, activation diff: 173.721, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 4534.9
#   number of spread. activ. pulses: 8
#   running time: 1288

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9706727   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8700614   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.86509025   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.79901254   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7419183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7327362   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.72847176   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.72832406   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7273436   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7268243   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.72622293   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.72616434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7246537   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.72416246   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7239365   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.723402   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.72261935   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.7209929   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.72044176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7198223   REFERENCES:SIMLOC
