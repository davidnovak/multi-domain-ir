###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 349.806, activation diff: 346.582, ratio: 0.991
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 579.627, activation diff: 320.687, ratio: 0.553
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2231.505, activation diff: 1651.907, ratio: 0.740
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3609.854, activation diff: 1378.350, ratio: 0.382
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 4527.218, activation diff: 917.364, ratio: 0.203
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 4961.663, activation diff: 434.445, ratio: 0.088
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 5147.832, activation diff: 186.169, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 5147.8
#   number of spread. activ. pulses: 8
#   running time: 1386

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9709568   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.870353   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8664995   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7993093   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7939677   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.78918576   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.7847923   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7837236   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.78346264   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.7834515   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7833754   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7829081   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7828766   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.78278553   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.78135926   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.78133684   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.78117526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.78077716   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7800206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.77981704   REFERENCES:SIMLOC
