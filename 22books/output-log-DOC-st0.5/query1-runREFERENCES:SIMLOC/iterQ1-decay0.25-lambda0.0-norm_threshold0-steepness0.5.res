###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 566.028, activation diff: 583.270, ratio: 1.030
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 529.612, activation diff: 953.953, ratio: 1.801
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 3098.813, activation diff: 2904.298, ratio: 0.937
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 3544.548, activation diff: 1382.252, ratio: 0.390
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 4480.361, activation diff: 989.850, ratio: 0.221
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 4605.881, activation diff: 138.666, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 4605.9
#   number of spread. activ. pulses: 7
#   running time: 1204

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9723982   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8745824   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8694993   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.802839   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7431717   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7355489   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.73157483   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7304943   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7297785   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.72952   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7289605   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7289245   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.72749543   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.7271071   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   15   0.7261845   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.72608733   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7256921   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7249787   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7232646   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.72315055   REFERENCES:SIMLOC
