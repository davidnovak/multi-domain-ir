###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 678.468, activation diff: 695.710, ratio: 1.025
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 760.681, activation diff: 1259.496, ratio: 1.656
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4534.402, activation diff: 4238.699, ratio: 0.935
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 5253.719, activation diff: 1831.919, ratio: 0.349
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 6392.159, activation diff: 1199.687, ratio: 0.188
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 6537.265, activation diff: 157.626, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6537.3
#   number of spread. activ. pulses: 7
#   running time: 2294

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9732042   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89752996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8969484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8959764   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89418644   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.8940076   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.8935154   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.893395   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.89334464   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.89330906   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   11   0.89330375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.89291924   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8929165   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.8924983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.89236325   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.8922978   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.89175975   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   18   0.89166313   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.89160043   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   20   0.8915852   REFERENCES:SIMLOC
