###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 511.943, activation diff: 528.789, ratio: 1.033
#   pulse 3: activated nodes: 9505, borderline nodes: 3995, overall activation: 455.472, activation diff: 942.038, ratio: 2.068
#   pulse 4: activated nodes: 11280, borderline nodes: 3803, overall activation: 3739.688, activation diff: 3818.000, ratio: 1.021
#   pulse 5: activated nodes: 11413, borderline nodes: 701, overall activation: 3513.624, activation diff: 3144.663, ratio: 0.895
#   pulse 6: activated nodes: 11442, borderline nodes: 134, overall activation: 5960.561, activation diff: 2749.290, ratio: 0.461
#   pulse 7: activated nodes: 11450, borderline nodes: 41, overall activation: 6252.299, activation diff: 392.893, ratio: 0.063
#   pulse 8: activated nodes: 11451, borderline nodes: 21, overall activation: 6370.888, activation diff: 132.265, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 6370.9
#   number of spread. activ. pulses: 8
#   running time: 1535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9720586   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.897414   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89684653   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8957899   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   5   0.89412993   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   6   0.89399904   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.8932972   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8932677   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.89315027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.8931239   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   11   0.8929624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89285266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.89277375   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.89249426   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.8920444   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.89192325   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   17   0.891709   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_788   18   0.891496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.89147145   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.89142054   REFERENCES:SIMLOC
