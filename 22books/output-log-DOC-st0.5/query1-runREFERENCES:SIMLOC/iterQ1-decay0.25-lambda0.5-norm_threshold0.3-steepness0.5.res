###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 33.745, activation diff: 27.108, ratio: 0.803
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 56.649, activation diff: 24.842, ratio: 0.439
#   pulse 4: activated nodes: 9587, borderline nodes: 7704, overall activation: 224.495, activation diff: 168.480, ratio: 0.750
#   pulse 5: activated nodes: 10395, borderline nodes: 5974, overall activation: 489.807, activation diff: 265.328, ratio: 0.542
#   pulse 6: activated nodes: 11100, borderline nodes: 5247, overall activation: 948.634, activation diff: 458.827, ratio: 0.484
#   pulse 7: activated nodes: 11264, borderline nodes: 3338, overall activation: 1538.973, activation diff: 590.339, ratio: 0.384
#   pulse 8: activated nodes: 11365, borderline nodes: 1759, overall activation: 2177.692, activation diff: 638.719, ratio: 0.293
#   pulse 9: activated nodes: 11399, borderline nodes: 989, overall activation: 2764.701, activation diff: 587.009, ratio: 0.212
#   pulse 10: activated nodes: 11412, borderline nodes: 637, overall activation: 3228.064, activation diff: 463.364, ratio: 0.144
#   pulse 11: activated nodes: 11418, borderline nodes: 428, overall activation: 3554.655, activation diff: 326.591, ratio: 0.092
#   pulse 12: activated nodes: 11424, borderline nodes: 331, overall activation: 3772.458, activation diff: 217.803, ratio: 0.058
#   pulse 13: activated nodes: 11428, borderline nodes: 275, overall activation: 3914.077, activation diff: 141.619, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11428
#   final overall activation: 3914.1
#   number of spread. activ. pulses: 13
#   running time: 1604

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9628502   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.84086597   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.83243084   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7641996   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7383766   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7231661   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.72021204   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.71840453   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.717193   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7165434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.71616995   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.7160219   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.7151798   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.71488416   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7125976   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.71223176   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7115588   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.711518   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.71030444   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.70871276   REFERENCES:SIMLOC
