###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 101.638, activation diff: 91.792, ratio: 0.903
#   pulse 3: activated nodes: 9187, borderline nodes: 4792, overall activation: 201.499, activation diff: 100.751, ratio: 0.500
#   pulse 4: activated nodes: 11094, borderline nodes: 5819, overall activation: 677.253, activation diff: 475.798, ratio: 0.703
#   pulse 5: activated nodes: 11300, borderline nodes: 2398, overall activation: 1396.527, activation diff: 719.274, ratio: 0.515
#   pulse 6: activated nodes: 11404, borderline nodes: 1047, overall activation: 2310.628, activation diff: 914.101, ratio: 0.396
#   pulse 7: activated nodes: 11429, borderline nodes: 389, overall activation: 3160.976, activation diff: 850.348, ratio: 0.269
#   pulse 8: activated nodes: 11441, borderline nodes: 174, overall activation: 3817.755, activation diff: 656.780, ratio: 0.172
#   pulse 9: activated nodes: 11447, borderline nodes: 102, overall activation: 4269.235, activation diff: 451.480, ratio: 0.106
#   pulse 10: activated nodes: 11448, borderline nodes: 67, overall activation: 4562.544, activation diff: 293.309, ratio: 0.064
#   pulse 11: activated nodes: 11448, borderline nodes: 62, overall activation: 4747.430, activation diff: 184.886, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 4747.4
#   number of spread. activ. pulses: 11
#   running time: 1367

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9635841   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8512512   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8459462   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.78891206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7797229   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   6   0.7791622   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.7746633   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.77453226   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.77434635   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7736731   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.7735729   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.77309155   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7729837   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.77199996   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7717319   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.771274   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.77127063   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.77103007   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7696474   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.76922023   REFERENCES:SIMLOC
