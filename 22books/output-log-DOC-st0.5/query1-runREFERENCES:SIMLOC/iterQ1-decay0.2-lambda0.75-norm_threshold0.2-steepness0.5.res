###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 13.533, activation diff: 6.962, ratio: 0.514
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 28.552, activation diff: 16.383, ratio: 0.574
#   pulse 4: activated nodes: 8848, borderline nodes: 6616, overall activation: 62.598, activation diff: 34.911, ratio: 0.558
#   pulse 5: activated nodes: 10066, borderline nodes: 6953, overall activation: 133.451, activation diff: 71.301, ratio: 0.534
#   pulse 6: activated nodes: 10755, borderline nodes: 6163, overall activation: 255.601, activation diff: 122.315, ratio: 0.479
#   pulse 7: activated nodes: 11090, borderline nodes: 5059, overall activation: 443.694, activation diff: 188.110, ratio: 0.424
#   pulse 8: activated nodes: 11224, borderline nodes: 3867, overall activation: 710.115, activation diff: 266.420, ratio: 0.375
#   pulse 9: activated nodes: 11306, borderline nodes: 2580, overall activation: 1048.808, activation diff: 338.693, ratio: 0.323
#   pulse 10: activated nodes: 11375, borderline nodes: 1556, overall activation: 1443.354, activation diff: 394.546, ratio: 0.273
#   pulse 11: activated nodes: 11399, borderline nodes: 1035, overall activation: 1866.051, activation diff: 422.697, ratio: 0.227
#   pulse 12: activated nodes: 11409, borderline nodes: 700, overall activation: 2288.112, activation diff: 422.061, ratio: 0.184
#   pulse 13: activated nodes: 11417, borderline nodes: 482, overall activation: 2687.317, activation diff: 399.206, ratio: 0.149
#   pulse 14: activated nodes: 11425, borderline nodes: 338, overall activation: 3048.296, activation diff: 360.979, ratio: 0.118
#   pulse 15: activated nodes: 11428, borderline nodes: 252, overall activation: 3364.397, activation diff: 316.100, ratio: 0.094

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11428
#   final overall activation: 3364.4
#   number of spread. activ. pulses: 15
#   running time: 1546

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89191854   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.72256577   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   3   0.7190266   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.707038   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.6967919   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.6783842   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   7   0.67808044   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.6778782   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.6700594   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   10   0.67001224   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.6697333   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.6689327   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.66818124   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.66807723   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.6675647   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   16   0.6656907   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   17   0.6622434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.66137546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.661314   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.6605692   REFERENCES:SIMLOC
