###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 278.154, activation diff: 293.084, ratio: 1.054
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 204.016, activation diff: 480.511, ratio: 2.355
#   pulse 4: activated nodes: 10995, borderline nodes: 7151, overall activation: 2444.649, activation diff: 2584.743, ratio: 1.057
#   pulse 5: activated nodes: 11282, borderline nodes: 2353, overall activation: 1691.451, activation diff: 3423.557, ratio: 2.024
#   pulse 6: activated nodes: 11406, borderline nodes: 988, overall activation: 4947.924, activation diff: 4098.542, ratio: 0.828
#   pulse 7: activated nodes: 11432, borderline nodes: 332, overall activation: 5237.665, activation diff: 1370.167, ratio: 0.262
#   pulse 8: activated nodes: 11442, borderline nodes: 143, overall activation: 5951.506, activation diff: 796.212, ratio: 0.134
#   pulse 9: activated nodes: 11446, borderline nodes: 94, overall activation: 6048.027, activation diff: 119.850, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 6048.0
#   number of spread. activ. pulses: 9
#   running time: 1333

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96878034   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.897113   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8964496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89518887   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8932525   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.89298415   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.8924638   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.89231604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.89225584   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.892225   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.89218646   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.891762   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.891758   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.89109546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.8910587   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.8909099   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8904067   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.89026725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   19   0.8902257   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.89018494   REFERENCES:SIMLOC
