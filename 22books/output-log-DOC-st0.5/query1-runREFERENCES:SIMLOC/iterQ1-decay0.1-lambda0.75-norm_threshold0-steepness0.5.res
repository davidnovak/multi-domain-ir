###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 53.801, activation diff: 46.337, ratio: 0.861
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 131.048, activation diff: 78.443, ratio: 0.599
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 327.050, activation diff: 196.536, ratio: 0.601
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 672.558, activation diff: 345.649, ratio: 0.514
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 1182.527, activation diff: 509.980, ratio: 0.431
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 1798.828, activation diff: 616.300, ratio: 0.343
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 2451.141, activation diff: 652.314, ratio: 0.266
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 3083.928, activation diff: 632.787, ratio: 0.205
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 3661.163, activation diff: 577.235, ratio: 0.158
#   pulse 11: activated nodes: 11457, borderline nodes: 0, overall activation: 4167.728, activation diff: 506.565, ratio: 0.122
#   pulse 12: activated nodes: 11457, borderline nodes: 0, overall activation: 4601.435, activation diff: 433.707, ratio: 0.094
#   pulse 13: activated nodes: 11457, borderline nodes: 0, overall activation: 4966.607, activation diff: 365.172, ratio: 0.074
#   pulse 14: activated nodes: 11457, borderline nodes: 0, overall activation: 5270.483, activation diff: 303.877, ratio: 0.058
#   pulse 15: activated nodes: 11457, borderline nodes: 0, overall activation: 5521.224, activation diff: 250.741, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 5521.2
#   number of spread. activ. pulses: 15
#   running time: 1579

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9292907   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.85191   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8402267   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.83741814   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.8372209   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   6   0.83546317   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   7   0.8344374   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.833896   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.8337407   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.8337042   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.833159   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   12   0.83265024   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   13   0.8326029   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.83183277   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.83119965   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.83089983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   17   0.8293887   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8293287   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.82895106   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.82867664   REFERENCES:SIMLOC
