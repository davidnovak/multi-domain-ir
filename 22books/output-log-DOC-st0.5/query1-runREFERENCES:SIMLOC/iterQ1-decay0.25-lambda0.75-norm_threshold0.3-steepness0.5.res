###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 9.359, activation diff: 3.241, ratio: 0.346
#   pulse 3: activated nodes: 7552, borderline nodes: 7231, overall activation: 14.517, activation diff: 6.609, ratio: 0.455
#   pulse 4: activated nodes: 7938, borderline nodes: 6772, overall activation: 24.136, activation diff: 10.810, ratio: 0.448
#   pulse 5: activated nodes: 8553, borderline nodes: 6989, overall activation: 48.249, activation diff: 24.999, ratio: 0.518
#   pulse 6: activated nodes: 9132, borderline nodes: 6737, overall activation: 95.021, activation diff: 47.241, ratio: 0.497
#   pulse 7: activated nodes: 10275, borderline nodes: 6957, overall activation: 173.145, activation diff: 78.275, ratio: 0.452
#   pulse 8: activated nodes: 10759, borderline nodes: 6206, overall activation: 290.593, activation diff: 117.495, ratio: 0.404
#   pulse 9: activated nodes: 11043, borderline nodes: 5398, overall activation: 455.907, activation diff: 165.314, ratio: 0.363
#   pulse 10: activated nodes: 11186, borderline nodes: 4460, overall activation: 675.650, activation diff: 219.744, ratio: 0.325
#   pulse 11: activated nodes: 11267, borderline nodes: 3374, overall activation: 942.117, activation diff: 266.467, ratio: 0.283
#   pulse 12: activated nodes: 11317, borderline nodes: 2356, overall activation: 1244.155, activation diff: 302.038, ratio: 0.243
#   pulse 13: activated nodes: 11368, borderline nodes: 1655, overall activation: 1569.473, activation diff: 325.318, ratio: 0.207
#   pulse 14: activated nodes: 11388, borderline nodes: 1255, overall activation: 1900.854, activation diff: 331.381, ratio: 0.174
#   pulse 15: activated nodes: 11398, borderline nodes: 921, overall activation: 2222.305, activation diff: 321.451, ratio: 0.145

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11398
#   final overall activation: 2222.3
#   number of spread. activ. pulses: 15
#   running time: 1671

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8448969   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.6464336   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   3   0.6257387   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   4   0.60807574   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.6023474   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   6   0.59093827   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   7   0.5819005   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   8   0.5725275   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   9   0.57074845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.5686289   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.56721157   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.5650375   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.5648187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.5588681   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   15   0.55738544   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   16   0.55730677   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.55567324   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.5545049   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   19   0.5540917   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   20   0.55195487   REFERENCES:SIMLOC
