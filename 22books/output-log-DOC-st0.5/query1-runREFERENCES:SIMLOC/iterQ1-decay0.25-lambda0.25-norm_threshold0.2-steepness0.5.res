###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 158.469, activation diff: 159.170, ratio: 1.004
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 167.754, activation diff: 155.957, ratio: 0.930
#   pulse 4: activated nodes: 11007, borderline nodes: 6726, overall activation: 1007.700, activation diff: 868.149, ratio: 0.862
#   pulse 5: activated nodes: 11248, borderline nodes: 2910, overall activation: 1730.555, activation diff: 727.718, ratio: 0.421
#   pulse 6: activated nodes: 11350, borderline nodes: 1677, overall activation: 2815.177, activation diff: 1084.623, ratio: 0.385
#   pulse 7: activated nodes: 11415, borderline nodes: 562, overall activation: 3559.909, activation diff: 744.732, ratio: 0.209
#   pulse 8: activated nodes: 11426, borderline nodes: 279, overall activation: 3957.540, activation diff: 397.631, ratio: 0.100
#   pulse 9: activated nodes: 11430, borderline nodes: 189, overall activation: 4141.081, activation diff: 183.541, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11430
#   final overall activation: 4141.1
#   number of spread. activ. pulses: 9
#   running time: 1338

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96812624   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8576026   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8490338   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.78105056   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.74150443   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.73053616   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.72631025   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   8   0.72551686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.72519803   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7247486   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7239344   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7237741   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.7219267   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.72105485   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.7208771   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.7203582   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7197833   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7177063   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.71638393   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.7163274   REFERENCES:SIMLOC
