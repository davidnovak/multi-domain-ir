###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 105.869, activation diff: 107.957, ratio: 1.020
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 91.484, activation diff: 113.886, ratio: 1.245
#   pulse 4: activated nodes: 10624, borderline nodes: 7593, overall activation: 716.397, activation diff: 659.559, ratio: 0.921
#   pulse 5: activated nodes: 10952, borderline nodes: 4327, overall activation: 1176.692, activation diff: 487.555, ratio: 0.414
#   pulse 6: activated nodes: 11291, borderline nodes: 3577, overall activation: 2214.323, activation diff: 1037.643, ratio: 0.469
#   pulse 7: activated nodes: 11382, borderline nodes: 1206, overall activation: 3072.420, activation diff: 858.097, ratio: 0.279
#   pulse 8: activated nodes: 11412, borderline nodes: 634, overall activation: 3636.822, activation diff: 564.402, ratio: 0.155
#   pulse 9: activated nodes: 11420, borderline nodes: 362, overall activation: 3921.163, activation diff: 284.341, ratio: 0.073
#   pulse 10: activated nodes: 11423, borderline nodes: 266, overall activation: 4053.128, activation diff: 131.965, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11423
#   final overall activation: 4053.1
#   number of spread. activ. pulses: 10
#   running time: 1354

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9672355   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85346055   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84524536   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.772615   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7416037   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7311205   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.7263242   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   8   0.72586393   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7252643   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.72500575   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.72423875   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.72394305   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.72177595   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.72153455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   15   0.7206578   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.72055584   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7198088   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7176202   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.71699214   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   20   0.71640503   REFERENCES:SIMLOC
