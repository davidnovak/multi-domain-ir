###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 24.379, activation diff: 17.359, ratio: 0.712
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 56.006, activation diff: 32.907, ratio: 0.588
#   pulse 4: activated nodes: 10530, borderline nodes: 6546, overall activation: 134.525, activation diff: 79.211, ratio: 0.589
#   pulse 5: activated nodes: 11041, borderline nodes: 5235, overall activation: 277.426, activation diff: 143.184, ratio: 0.516
#   pulse 6: activated nodes: 11218, borderline nodes: 3910, overall activation: 503.517, activation diff: 226.132, ratio: 0.449
#   pulse 7: activated nodes: 11324, borderline nodes: 2372, overall activation: 821.568, activation diff: 318.051, ratio: 0.387
#   pulse 8: activated nodes: 11393, borderline nodes: 1244, overall activation: 1217.438, activation diff: 395.870, ratio: 0.325
#   pulse 9: activated nodes: 11412, borderline nodes: 704, overall activation: 1661.728, activation diff: 444.290, ratio: 0.267
#   pulse 10: activated nodes: 11425, borderline nodes: 417, overall activation: 2117.240, activation diff: 455.512, ratio: 0.215
#   pulse 11: activated nodes: 11430, borderline nodes: 248, overall activation: 2555.040, activation diff: 437.800, ratio: 0.171
#   pulse 12: activated nodes: 11440, borderline nodes: 176, overall activation: 2954.652, activation diff: 399.613, ratio: 0.135
#   pulse 13: activated nodes: 11444, borderline nodes: 140, overall activation: 3306.328, activation diff: 351.676, ratio: 0.106
#   pulse 14: activated nodes: 11447, borderline nodes: 109, overall activation: 3608.561, activation diff: 302.232, ratio: 0.084
#   pulse 15: activated nodes: 11447, borderline nodes: 92, overall activation: 3864.170, activation diff: 255.610, ratio: 0.066

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 3864.2
#   number of spread. activ. pulses: 15
#   running time: 1510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9140922   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.76521766   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.7626569   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7385052   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.71504587   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   6   0.7074901   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.70735425   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   8   0.70688444   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7067986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.7052908   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7033845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.7007526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.69881374   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   14   0.697749   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.6976794   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.69720006   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.69618005   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.6950274   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.6942148   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.69368064   REFERENCES:SIMLOC
