###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 285.785, activation diff: 302.631, ratio: 1.059
#   pulse 3: activated nodes: 9505, borderline nodes: 3995, overall activation: 127.451, activation diff: 400.515, ratio: 3.143
#   pulse 4: activated nodes: 10705, borderline nodes: 4541, overall activation: 1090.850, activation diff: 1126.333, ratio: 1.033
#   pulse 5: activated nodes: 10867, borderline nodes: 3698, overall activation: 689.632, activation diff: 1021.009, ratio: 1.481
#   pulse 6: activated nodes: 10886, borderline nodes: 3573, overall activation: 1661.347, activation diff: 1073.312, ratio: 0.646
#   pulse 7: activated nodes: 10894, borderline nodes: 3533, overall activation: 1785.465, activation diff: 254.962, ratio: 0.143
#   pulse 8: activated nodes: 10895, borderline nodes: 3525, overall activation: 1937.116, activation diff: 154.905, ratio: 0.080
#   pulse 9: activated nodes: 10897, borderline nodes: 3518, overall activation: 1967.257, activation diff: 31.871, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 1967.3
#   number of spread. activ. pulses: 9
#   running time: 1255

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9692997   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8674899   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8530612   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.79317904   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6641453   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6314661   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46586713   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45740607   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.45165586   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4447908   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44050246   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.44048193   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4401228   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.43750376   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.43523458   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4336467   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.43144315   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.42934585   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42758784   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.4253654   REFERENCES:SIMLOC
