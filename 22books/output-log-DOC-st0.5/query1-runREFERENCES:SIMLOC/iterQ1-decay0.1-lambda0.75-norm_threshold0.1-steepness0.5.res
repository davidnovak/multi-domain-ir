###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 26.138, activation diff: 19.117, ratio: 0.731
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 61.956, activation diff: 37.098, ratio: 0.599
#   pulse 4: activated nodes: 10558, borderline nodes: 6536, overall activation: 156.244, activation diff: 94.969, ratio: 0.608
#   pulse 5: activated nodes: 11065, borderline nodes: 5019, overall activation: 335.566, activation diff: 179.593, ratio: 0.535
#   pulse 6: activated nodes: 11250, borderline nodes: 3529, overall activation: 635.260, activation diff: 299.724, ratio: 0.472
#   pulse 7: activated nodes: 11365, borderline nodes: 1890, overall activation: 1073.302, activation diff: 438.042, ratio: 0.408
#   pulse 8: activated nodes: 11406, borderline nodes: 986, overall activation: 1623.887, activation diff: 550.585, ratio: 0.339
#   pulse 9: activated nodes: 11422, borderline nodes: 492, overall activation: 2225.335, activation diff: 601.448, ratio: 0.270
#   pulse 10: activated nodes: 11437, borderline nodes: 279, overall activation: 2827.510, activation diff: 602.175, ratio: 0.213
#   pulse 11: activated nodes: 11443, borderline nodes: 153, overall activation: 3391.362, activation diff: 563.852, ratio: 0.166
#   pulse 12: activated nodes: 11446, borderline nodes: 104, overall activation: 3894.620, activation diff: 503.259, ratio: 0.129
#   pulse 13: activated nodes: 11447, borderline nodes: 75, overall activation: 4330.314, activation diff: 435.693, ratio: 0.101
#   pulse 14: activated nodes: 11448, borderline nodes: 59, overall activation: 4700.099, activation diff: 369.785, ratio: 0.079
#   pulse 15: activated nodes: 11448, borderline nodes: 51, overall activation: 5009.663, activation diff: 309.565, ratio: 0.062

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5009.7
#   number of spread. activ. pulses: 15
#   running time: 1437

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91695327   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.841509   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.8231881   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   4   0.8212497   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.8200195   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.81733185   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   7   0.8169626   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.81690323   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.81630933   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8162552   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.81596756   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.8136337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   13   0.81227416   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   14   0.8121233   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.8111316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.8109194   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.8100548   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   18   0.80904526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.8087639   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.8083036   REFERENCES:SIMLOC
