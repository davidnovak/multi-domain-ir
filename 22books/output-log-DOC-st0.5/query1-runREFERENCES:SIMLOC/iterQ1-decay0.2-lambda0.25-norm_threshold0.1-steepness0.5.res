###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 245.174, activation diff: 243.902, ratio: 0.995
#   pulse 3: activated nodes: 9362, borderline nodes: 4294, overall activation: 328.705, activation diff: 232.414, ratio: 0.707
#   pulse 4: activated nodes: 11228, borderline nodes: 4767, overall activation: 1631.319, activation diff: 1313.586, ratio: 0.805
#   pulse 5: activated nodes: 11384, borderline nodes: 1304, overall activation: 2818.853, activation diff: 1187.608, ratio: 0.421
#   pulse 6: activated nodes: 11423, borderline nodes: 399, overall activation: 3954.899, activation diff: 1136.046, ratio: 0.287
#   pulse 7: activated nodes: 11445, borderline nodes: 130, overall activation: 4570.599, activation diff: 615.700, ratio: 0.135
#   pulse 8: activated nodes: 11448, borderline nodes: 68, overall activation: 4850.884, activation diff: 280.286, ratio: 0.058
#   pulse 9: activated nodes: 11448, borderline nodes: 54, overall activation: 4970.879, activation diff: 119.995, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 4970.9
#   number of spread. activ. pulses: 9
#   running time: 1307

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97073156   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8660964   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.86281395   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7944127   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.79212713   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.78972274   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.7851927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7842419   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.78411555   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.78398234   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.7838522   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.78365326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7834298   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.78337085   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7818202   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.78154224   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.7815019   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.78099644   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.7803031   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.78021145   REFERENCES:SIMLOC
