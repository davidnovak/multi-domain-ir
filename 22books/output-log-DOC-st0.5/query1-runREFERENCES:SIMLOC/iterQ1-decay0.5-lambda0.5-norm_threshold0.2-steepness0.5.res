###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 42.411, activation diff: 34.138, ratio: 0.805
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 71.737, activation diff: 30.597, ratio: 0.427
#   pulse 4: activated nodes: 9651, borderline nodes: 6744, overall activation: 212.580, activation diff: 141.070, ratio: 0.664
#   pulse 5: activated nodes: 10262, borderline nodes: 5091, overall activation: 397.954, activation diff: 185.374, ratio: 0.466
#   pulse 6: activated nodes: 10666, borderline nodes: 4831, overall activation: 627.856, activation diff: 229.902, ratio: 0.366
#   pulse 7: activated nodes: 10769, borderline nodes: 4191, overall activation: 863.488, activation diff: 235.631, ratio: 0.273
#   pulse 8: activated nodes: 10845, borderline nodes: 3792, overall activation: 1082.085, activation diff: 218.597, ratio: 0.202
#   pulse 9: activated nodes: 10871, borderline nodes: 3674, overall activation: 1279.069, activation diff: 196.984, ratio: 0.154
#   pulse 10: activated nodes: 10876, borderline nodes: 3615, overall activation: 1450.350, activation diff: 171.281, ratio: 0.118
#   pulse 11: activated nodes: 10878, borderline nodes: 3583, overall activation: 1588.745, activation diff: 138.395, ratio: 0.087
#   pulse 12: activated nodes: 10886, borderline nodes: 3566, overall activation: 1692.701, activation diff: 103.956, ratio: 0.061
#   pulse 13: activated nodes: 10889, borderline nodes: 3554, overall activation: 1766.514, activation diff: 73.813, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10889
#   final overall activation: 1766.5
#   number of spread. activ. pulses: 13
#   running time: 1440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96233356   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8449304   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8245666   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7718252   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6395238   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.599926   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4597777   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.44976878   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.44413298   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43712628   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4343579   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.43233266   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.42899954   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.4260404   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.4240219   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.42250717   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.42210495   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.41975063   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.41723484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.41656095   REFERENCES:SIMLOC
