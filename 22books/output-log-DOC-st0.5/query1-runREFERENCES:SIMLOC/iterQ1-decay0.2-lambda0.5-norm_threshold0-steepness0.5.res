###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 164.193, activation diff: 153.034, ratio: 0.932
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 355.960, activation diff: 192.414, ratio: 0.541
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1067.503, activation diff: 711.548, ratio: 0.667
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2038.461, activation diff: 970.958, ratio: 0.476
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3025.718, activation diff: 987.257, ratio: 0.326
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 3807.595, activation diff: 781.878, ratio: 0.205
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 4350.799, activation diff: 543.204, ratio: 0.125
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 4704.900, activation diff: 354.100, ratio: 0.075
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 4928.037, activation diff: 223.137, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 4928.0
#   number of spread. activ. pulses: 10
#   running time: 1364

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9613431   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85312784   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84771895   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.78611195   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.7825655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7768948   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.77154183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.77142423   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.7713504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7706841   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.77025616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.7701187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.76977384   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.76877683   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.76860094   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.76853025   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.7683364   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.768013   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.76690364   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7661315   REFERENCES:SIMLOC
