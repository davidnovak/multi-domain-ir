###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 107.290, activation diff: 96.130, ratio: 0.896
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 199.130, activation diff: 92.500, ratio: 0.465
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 463.061, activation diff: 263.940, ratio: 0.570
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 776.414, activation diff: 313.353, ratio: 0.404
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1087.001, activation diff: 310.586, ratio: 0.286
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1360.040, activation diff: 273.039, ratio: 0.201
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 1578.079, activation diff: 218.039, ratio: 0.138
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 1738.163, activation diff: 160.084, ratio: 0.092
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 1848.576, activation diff: 110.413, ratio: 0.060
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 1921.872, activation diff: 73.296, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 1921.9
#   number of spread. activ. pulses: 11
#   running time: 1340

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96306765   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85577625   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84109044   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7855214   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6645702   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6265085   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46092644   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4523279   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4464361   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43997347   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.437787   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.43533128   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.43268102   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.42972022   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.42953405   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.42657638   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.42590123   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.42466205   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.421969   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.42135012   REFERENCES:SIMLOC
