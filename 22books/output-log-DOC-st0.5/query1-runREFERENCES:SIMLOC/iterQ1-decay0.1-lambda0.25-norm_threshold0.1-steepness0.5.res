###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 274.762, activation diff: 273.490, ratio: 0.995
#   pulse 3: activated nodes: 9362, borderline nodes: 4294, overall activation: 394.193, activation diff: 285.901, ratio: 0.725
#   pulse 4: activated nodes: 11235, borderline nodes: 4666, overall activation: 2118.203, activation diff: 1737.496, ratio: 0.820
#   pulse 5: activated nodes: 11390, borderline nodes: 1120, overall activation: 3705.621, activation diff: 1587.510, ratio: 0.428
#   pulse 6: activated nodes: 11438, borderline nodes: 276, overall activation: 5128.502, activation diff: 1422.881, ratio: 0.277
#   pulse 7: activated nodes: 11448, borderline nodes: 74, overall activation: 5854.556, activation diff: 726.054, ratio: 0.124
#   pulse 8: activated nodes: 11449, borderline nodes: 41, overall activation: 6171.649, activation diff: 317.093, ratio: 0.051
#   pulse 9: activated nodes: 11452, borderline nodes: 19, overall activation: 6304.849, activation diff: 133.200, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6304.8
#   number of spread. activ. pulses: 9
#   running time: 1278

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9711708   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8970251   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89608413   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8950345   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8930214   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.8928119   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.89239764   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   8   0.8922577   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.8922403   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8922162   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8922043   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8918637   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.89171624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.8912413   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.8910027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   16   0.89057595   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8903774   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.8902321   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   19   0.89014983   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.89010906   REFERENCES:SIMLOC
