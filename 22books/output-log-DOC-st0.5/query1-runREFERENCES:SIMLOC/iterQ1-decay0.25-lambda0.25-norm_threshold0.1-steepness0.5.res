###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 230.380, activation diff: 229.108, ratio: 0.994
#   pulse 3: activated nodes: 9362, borderline nodes: 4294, overall activation: 297.962, activation diff: 207.602, ratio: 0.697
#   pulse 4: activated nodes: 11222, borderline nodes: 4813, overall activation: 1418.164, activation diff: 1129.885, ratio: 0.797
#   pulse 5: activated nodes: 11381, borderline nodes: 1399, overall activation: 2421.716, activation diff: 1003.584, ratio: 0.414
#   pulse 6: activated nodes: 11423, borderline nodes: 490, overall activation: 3415.816, activation diff: 994.101, ratio: 0.291
#   pulse 7: activated nodes: 11436, borderline nodes: 183, overall activation: 3975.000, activation diff: 559.184, ratio: 0.141
#   pulse 8: activated nodes: 11439, borderline nodes: 124, overall activation: 4235.866, activation diff: 260.866, ratio: 0.062
#   pulse 9: activated nodes: 11440, borderline nodes: 101, overall activation: 4349.994, activation diff: 114.128, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 4350.0
#   number of spread. activ. pulses: 9
#   running time: 1402

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.970461   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86594397   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8613415   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.79194444   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7423221   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7332226   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.7288478   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7286686   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7277219   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.7273685   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.72666436   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7265171   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7253415   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.7243559   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.7240569   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.72328734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.72291446   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   18   0.7217769   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7204159   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7200186   REFERENCES:SIMLOC
