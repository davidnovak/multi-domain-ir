###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 67.729, activation diff: 57.882, ratio: 0.855
#   pulse 3: activated nodes: 9187, borderline nodes: 4792, overall activation: 119.390, activation diff: 52.553, ratio: 0.440
#   pulse 4: activated nodes: 10493, borderline nodes: 5695, overall activation: 316.387, activation diff: 197.064, ratio: 0.623
#   pulse 5: activated nodes: 10703, borderline nodes: 4341, overall activation: 563.666, activation diff: 247.279, ratio: 0.439
#   pulse 6: activated nodes: 10812, borderline nodes: 3889, overall activation: 836.998, activation diff: 273.332, ratio: 0.327
#   pulse 7: activated nodes: 10872, borderline nodes: 3650, overall activation: 1095.814, activation diff: 258.815, ratio: 0.236
#   pulse 8: activated nodes: 10886, borderline nodes: 3576, overall activation: 1326.209, activation diff: 230.395, ratio: 0.174
#   pulse 9: activated nodes: 10889, borderline nodes: 3536, overall activation: 1518.725, activation diff: 192.516, ratio: 0.127
#   pulse 10: activated nodes: 10893, borderline nodes: 3526, overall activation: 1667.181, activation diff: 148.456, ratio: 0.089
#   pulse 11: activated nodes: 10894, borderline nodes: 3522, overall activation: 1773.928, activation diff: 106.747, ratio: 0.060
#   pulse 12: activated nodes: 10896, borderline nodes: 3519, overall activation: 1847.050, activation diff: 73.123, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 1847.1
#   number of spread. activ. pulses: 12
#   running time: 1436

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96324295   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8512852   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8339821   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77972883   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6531013   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.61450946   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46079022   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45151818   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.44578028   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43904862   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.43649903   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.4343223   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.43130767   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.4283728   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.42722854   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.42507237   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.42448175   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.42244676   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.4200925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.41941738   REFERENCES:SIMLOC
