###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 183.161, activation diff: 172.001, ratio: 0.939
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 415.119, activation diff: 232.603, ratio: 0.560
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1345.778, activation diff: 930.663, ratio: 0.692
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2630.027, activation diff: 1284.249, ratio: 0.488
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 3892.615, activation diff: 1262.588, ratio: 0.324
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 4857.581, activation diff: 964.967, ratio: 0.199
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 5511.375, activation diff: 653.794, ratio: 0.119
#   pulse 9: activated nodes: 11457, borderline nodes: 0, overall activation: 5930.123, activation diff: 418.748, ratio: 0.071
#   pulse 10: activated nodes: 11457, borderline nodes: 0, overall activation: 6190.888, activation diff: 260.765, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6190.9
#   number of spread. activ. pulses: 10
#   running time: 1352

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9621265   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8889537   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.88504183   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8836006   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   5   0.8810098   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   6   0.8808237   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   7   0.880725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8806019   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.88049716   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.88027   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.88019747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.87958974   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.87929153   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.8792858   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.8785484   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.8777429   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.877725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8773259   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.8768914   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.8767067   REFERENCES:SIMLOC
