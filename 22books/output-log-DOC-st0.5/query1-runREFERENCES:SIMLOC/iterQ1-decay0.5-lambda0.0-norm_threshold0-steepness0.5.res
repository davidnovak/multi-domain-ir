###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 378.628, activation diff: 395.870, ratio: 1.046
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 221.192, activation diff: 514.325, ratio: 2.325
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1335.976, activation diff: 1246.346, ratio: 0.933
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1394.422, activation diff: 594.948, ratio: 0.427
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1912.499, activation diff: 532.977, ratio: 0.279
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1998.531, activation diff: 93.972, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 1998.5
#   number of spread. activ. pulses: 7
#   running time: 1214

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96861494   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8679394   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8502779   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.80159503   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.66983837   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6415579   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46600187   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45776847   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4520952   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.44557822   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4421601   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.44191653   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.44120806   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.43775034   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.43640673   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.43594763   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.43293983   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.43232903   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42871   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.42794302   REFERENCES:SIMLOC
