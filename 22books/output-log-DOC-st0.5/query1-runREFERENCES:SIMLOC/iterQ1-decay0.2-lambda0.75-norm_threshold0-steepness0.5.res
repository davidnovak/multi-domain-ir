###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 49.045, activation diff: 41.580, ratio: 0.848
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 115.696, activation diff: 67.848, ratio: 0.586
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 272.839, activation diff: 157.690, ratio: 0.578
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 536.757, activation diff: 264.075, ratio: 0.492
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 914.588, activation diff: 377.844, ratio: 0.413
#   pulse 7: activated nodes: 11457, borderline nodes: 2, overall activation: 1378.730, activation diff: 464.143, ratio: 0.337
#   pulse 8: activated nodes: 11457, borderline nodes: 2, overall activation: 1878.480, activation diff: 499.750, ratio: 0.266
#   pulse 9: activated nodes: 11457, borderline nodes: 2, overall activation: 2373.048, activation diff: 494.568, ratio: 0.208
#   pulse 10: activated nodes: 11457, borderline nodes: 2, overall activation: 2833.051, activation diff: 460.003, ratio: 0.162
#   pulse 11: activated nodes: 11457, borderline nodes: 2, overall activation: 3242.906, activation diff: 409.856, ratio: 0.126
#   pulse 12: activated nodes: 11457, borderline nodes: 2, overall activation: 3597.980, activation diff: 355.074, ratio: 0.099
#   pulse 13: activated nodes: 11457, borderline nodes: 2, overall activation: 3899.806, activation diff: 301.826, ratio: 0.077
#   pulse 14: activated nodes: 11457, borderline nodes: 2, overall activation: 4152.931, activation diff: 253.125, ratio: 0.061
#   pulse 15: activated nodes: 11457, borderline nodes: 2, overall activation: 4363.118, activation diff: 210.186, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 4363.1
#   number of spread. activ. pulses: 15
#   running time: 1570

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.92738366   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.79763615   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.79451466   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.749579   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.7320043   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7292199   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.7260223   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7258333   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   9   0.7253546   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.72347736   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7234106   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7218192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.72122884   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.7207911   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.72021496   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.72007084   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   17   0.7196133   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.7163985   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.71471816   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.7143713   REFERENCES:SIMLOC
