###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 34.775, activation diff: 27.310, ratio: 0.785
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 72.845, activation diff: 39.266, ratio: 0.539
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 144.011, activation diff: 71.762, ratio: 0.498
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 246.132, activation diff: 102.338, ratio: 0.416
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 374.817, activation diff: 128.710, ratio: 0.343
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 520.891, activation diff: 146.074, ratio: 0.280
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 675.399, activation diff: 154.508, ratio: 0.229
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 831.705, activation diff: 156.306, ratio: 0.188
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 984.678, activation diff: 152.973, ratio: 0.155
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 1130.041, activation diff: 145.362, ratio: 0.129
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 1264.468, activation diff: 134.427, ratio: 0.106
#   pulse 13: activated nodes: 10899, borderline nodes: 3506, overall activation: 1385.774, activation diff: 121.306, ratio: 0.088
#   pulse 14: activated nodes: 10899, borderline nodes: 3506, overall activation: 1492.928, activation diff: 107.154, ratio: 0.072
#   pulse 15: activated nodes: 10899, borderline nodes: 3506, overall activation: 1585.917, activation diff: 92.988, ratio: 0.059

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 1585.9
#   number of spread. activ. pulses: 15
#   running time: 1565

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9164633   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.77011794   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.76673245   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7130113   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6083392   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.55066454   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.42141026   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.41404602   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4056899   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.40230525   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.39783597   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.3926493   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.38887316   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.38792497   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.38585097   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   16   0.3845184   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   17   0.38409624   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   18   0.38320023   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.38253653   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.38247883   REFERENCES:SIMLOC
