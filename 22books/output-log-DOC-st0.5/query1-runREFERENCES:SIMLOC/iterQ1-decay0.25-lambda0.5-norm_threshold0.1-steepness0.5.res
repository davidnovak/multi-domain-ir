###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 95.987, activation diff: 86.140, ratio: 0.897
#   pulse 3: activated nodes: 9187, borderline nodes: 4792, overall activation: 186.770, activation diff: 91.674, ratio: 0.491
#   pulse 4: activated nodes: 11085, borderline nodes: 5836, overall activation: 602.177, activation diff: 415.455, ratio: 0.690
#   pulse 5: activated nodes: 11292, borderline nodes: 2626, overall activation: 1212.690, activation diff: 610.513, ratio: 0.503
#   pulse 6: activated nodes: 11398, borderline nodes: 1171, overall activation: 1981.188, activation diff: 768.498, ratio: 0.388
#   pulse 7: activated nodes: 11423, borderline nodes: 458, overall activation: 2714.212, activation diff: 733.024, ratio: 0.270
#   pulse 8: activated nodes: 11432, borderline nodes: 240, overall activation: 3295.426, activation diff: 581.214, ratio: 0.176
#   pulse 9: activated nodes: 11438, borderline nodes: 156, overall activation: 3702.123, activation diff: 406.697, ratio: 0.110
#   pulse 10: activated nodes: 11439, borderline nodes: 131, overall activation: 3969.561, activation diff: 267.437, ratio: 0.067
#   pulse 11: activated nodes: 11439, borderline nodes: 109, overall activation: 4139.898, activation diff: 170.337, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 4139.9
#   number of spread. activ. pulses: 11
#   running time: 1388

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.963074   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85036814   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8435997   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7784035   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.73653567   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7216797   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.7189948   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   8   0.7172842   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7158129   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   10   0.7152973   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   11   0.7152585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7152332   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   13   0.7151675   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.71415114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   15   0.71186805   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.71175337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.7116692   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.71102417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.70950496   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   20   0.7080929   REFERENCES:SIMLOC
