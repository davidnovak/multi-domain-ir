###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 232.145, activation diff: 247.075, ratio: 1.064
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 141.316, activation diff: 372.335, ratio: 2.635
#   pulse 4: activated nodes: 10974, borderline nodes: 7183, overall activation: 1611.825, activation diff: 1718.181, ratio: 1.066
#   pulse 5: activated nodes: 11249, borderline nodes: 2823, overall activation: 925.460, activation diff: 2250.333, ratio: 2.432
#   pulse 6: activated nodes: 11350, borderline nodes: 1618, overall activation: 3145.418, activation diff: 2864.808, ratio: 0.911
#   pulse 7: activated nodes: 11412, borderline nodes: 624, overall activation: 3035.104, activation diff: 1453.802, ratio: 0.479
#   pulse 8: activated nodes: 11422, borderline nodes: 365, overall activation: 3972.170, activation diff: 1066.274, ratio: 0.268
#   pulse 9: activated nodes: 11426, borderline nodes: 272, overall activation: 4087.962, activation diff: 161.927, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11426
#   final overall activation: 4088.0
#   number of spread. activ. pulses: 9
#   running time: 1313

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96679807   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.855394   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8451623   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77451557   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7418337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7327199   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.72735626   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.72637594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.72524846   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7251537   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.725099   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7245246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.72165   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.72152555   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.72132903   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.7207603   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   17   0.7201593   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7190286   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7188425   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.718295   REFERENCES:SIMLOC
