###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 168.579, activation diff: 169.281, ratio: 1.004
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 184.752, activation diff: 172.410, ratio: 0.933
#   pulse 4: activated nodes: 11015, borderline nodes: 6716, overall activation: 1154.418, activation diff: 1001.979, ratio: 0.868
#   pulse 5: activated nodes: 11255, borderline nodes: 2702, overall activation: 2022.469, activation diff: 873.587, ratio: 0.432
#   pulse 6: activated nodes: 11358, borderline nodes: 1411, overall activation: 3299.545, activation diff: 1277.077, ratio: 0.387
#   pulse 7: activated nodes: 11420, borderline nodes: 461, overall activation: 4137.960, activation diff: 838.415, ratio: 0.203
#   pulse 8: activated nodes: 11436, borderline nodes: 200, overall activation: 4565.578, activation diff: 427.618, ratio: 0.094
#   pulse 9: activated nodes: 11444, borderline nodes: 129, overall activation: 4757.735, activation diff: 192.158, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 4757.7
#   number of spread. activ. pulses: 9
#   running time: 1321

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9684912   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8578479   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8514857   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7937701   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.78781754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   6   0.78261894   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.7822942   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7820863   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.78183866   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.7815259   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.78139734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.78131956   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   13   0.78127354   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.7805946   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.77967024   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.77952737   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.7790169   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7789388   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.77860016   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7778102   REFERENCES:SIMLOC
