###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 9.446, activation diff: 3.328, ratio: 0.352
#   pulse 3: activated nodes: 7552, borderline nodes: 7231, overall activation: 15.685, activation diff: 7.690, ratio: 0.490
#   pulse 4: activated nodes: 7938, borderline nodes: 6772, overall activation: 27.706, activation diff: 13.192, ratio: 0.476
#   pulse 5: activated nodes: 8656, borderline nodes: 7072, overall activation: 59.790, activation diff: 32.923, ratio: 0.551
#   pulse 6: activated nodes: 9558, borderline nodes: 7011, overall activation: 123.709, activation diff: 64.319, ratio: 0.520
#   pulse 7: activated nodes: 10399, borderline nodes: 6826, overall activation: 235.997, activation diff: 112.419, ratio: 0.476
#   pulse 8: activated nodes: 10846, borderline nodes: 5904, overall activation: 415.908, activation diff: 179.929, ratio: 0.433
#   pulse 9: activated nodes: 11134, borderline nodes: 4792, overall activation: 687.705, activation diff: 271.797, ratio: 0.395
#   pulse 10: activated nodes: 11252, borderline nodes: 3508, overall activation: 1064.733, activation diff: 377.028, ratio: 0.354
#   pulse 11: activated nodes: 11313, borderline nodes: 2288, overall activation: 1536.489, activation diff: 471.755, ratio: 0.307
#   pulse 12: activated nodes: 11377, borderline nodes: 1441, overall activation: 2061.817, activation diff: 525.328, ratio: 0.255
#   pulse 13: activated nodes: 11396, borderline nodes: 946, overall activation: 2599.957, activation diff: 538.140, ratio: 0.207
#   pulse 14: activated nodes: 11411, borderline nodes: 657, overall activation: 3119.589, activation diff: 519.632, ratio: 0.167
#   pulse 15: activated nodes: 11418, borderline nodes: 484, overall activation: 3596.122, activation diff: 476.533, ratio: 0.133

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11418
#   final overall activation: 3596.1
#   number of spread. activ. pulses: 15
#   running time: 1450

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8575086   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.78656995   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.7722417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   4   0.74370104   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   5   0.7395701   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   6   0.73769987   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   7   0.7363666   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   8   0.732582   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   9   0.7307547   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   10   0.72928846   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.728643   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7283112   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7270626   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   14   0.7234261   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.7234217   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   16   0.72118115   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   17   0.7202208   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   18   0.7198576   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.7174274   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   20   0.7171343   REFERENCES:SIMLOC
