###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 25.396, activation diff: 18.759, ratio: 0.739
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 39.924, activation diff: 16.626, ratio: 0.416
#   pulse 4: activated nodes: 8884, borderline nodes: 7055, overall activation: 128.760, activation diff: 89.640, ratio: 0.696
#   pulse 5: activated nodes: 9562, borderline nodes: 5719, overall activation: 253.934, activation diff: 125.247, ratio: 0.493
#   pulse 6: activated nodes: 10298, borderline nodes: 5602, overall activation: 428.477, activation diff: 174.543, ratio: 0.407
#   pulse 7: activated nodes: 10628, borderline nodes: 4991, overall activation: 631.173, activation diff: 202.695, ratio: 0.321
#   pulse 8: activated nodes: 10727, borderline nodes: 4473, overall activation: 835.690, activation diff: 204.518, ratio: 0.245
#   pulse 9: activated nodes: 10797, borderline nodes: 4004, overall activation: 1024.064, activation diff: 188.374, ratio: 0.184
#   pulse 10: activated nodes: 10845, borderline nodes: 3786, overall activation: 1195.014, activation diff: 170.950, ratio: 0.143
#   pulse 11: activated nodes: 10862, borderline nodes: 3706, overall activation: 1349.611, activation diff: 154.596, ratio: 0.115
#   pulse 12: activated nodes: 10870, borderline nodes: 3656, overall activation: 1482.326, activation diff: 132.715, ratio: 0.090
#   pulse 13: activated nodes: 10876, borderline nodes: 3630, overall activation: 1588.355, activation diff: 106.029, ratio: 0.067
#   pulse 14: activated nodes: 10876, borderline nodes: 3610, overall activation: 1667.854, activation diff: 79.499, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10876
#   final overall activation: 1667.9
#   number of spread. activ. pulses: 14
#   running time: 1487

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9604417   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8362951   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8120603   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.76213115   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6238171   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5827035   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.45797956   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.44711956   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4416227   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43426585   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4314948   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.42941654   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.425398   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.4228888   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.4190938   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.4189517   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.41892594   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.4153955   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.41344512   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.4130352   REFERENCES:SIMLOC
