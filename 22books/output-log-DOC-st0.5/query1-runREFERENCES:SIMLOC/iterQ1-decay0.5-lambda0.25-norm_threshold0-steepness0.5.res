###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 222.424, activation diff: 219.200, ratio: 0.986
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 285.971, activation diff: 124.743, ratio: 0.436
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 865.517, activation diff: 579.609, ratio: 0.670
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1302.088, activation diff: 436.571, ratio: 0.335
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1644.496, activation diff: 342.408, ratio: 0.208
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1851.061, activation diff: 206.564, ratio: 0.112
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 1957.796, activation diff: 106.736, ratio: 0.055
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2008.964, activation diff: 51.167, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2009.0
#   number of spread. activ. pulses: 9
#   running time: 1273

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9703642   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.87159514   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.85832983   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7999143   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6774245   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6434605   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46689302   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45852524   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4531539   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4467297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44288343   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.44238448   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.44163215   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.43879598   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.43607906   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.43601143   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.43356496   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.43186206   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42902356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.42796952   REFERENCES:SIMLOC
