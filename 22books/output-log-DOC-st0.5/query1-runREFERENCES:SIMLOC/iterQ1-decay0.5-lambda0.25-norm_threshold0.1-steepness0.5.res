###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 156.410, activation diff: 155.138, ratio: 0.992
#   pulse 3: activated nodes: 9362, borderline nodes: 4294, overall activation: 165.653, activation diff: 104.278, ratio: 0.629
#   pulse 4: activated nodes: 10633, borderline nodes: 5034, overall activation: 650.418, activation diff: 487.867, ratio: 0.750
#   pulse 5: activated nodes: 10844, borderline nodes: 3855, overall activation: 1012.056, activation diff: 361.660, ratio: 0.357
#   pulse 6: activated nodes: 10873, borderline nodes: 3657, overall activation: 1371.347, activation diff: 359.291, ratio: 0.262
#   pulse 7: activated nodes: 10889, borderline nodes: 3547, overall activation: 1635.649, activation diff: 264.303, ratio: 0.162
#   pulse 8: activated nodes: 10893, borderline nodes: 3525, overall activation: 1804.708, activation diff: 169.059, ratio: 0.094
#   pulse 9: activated nodes: 10896, borderline nodes: 3520, overall activation: 1897.033, activation diff: 92.325, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 1897.0
#   number of spread. activ. pulses: 9
#   running time: 1368

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9677454   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8626889   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84330535   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7888981   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.66033304   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6258342   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46447936   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45509678   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4498272   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.4431334   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.43952125   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.43863684   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.4374001   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.433078   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.4322895   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.4300852   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.42921007   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.42818046   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.4238455   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.4237109   REFERENCES:SIMLOC
