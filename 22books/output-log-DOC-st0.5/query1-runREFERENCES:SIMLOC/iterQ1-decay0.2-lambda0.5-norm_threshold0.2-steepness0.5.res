###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 61.881, activation diff: 53.608, ratio: 0.866
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 114.449, activation diff: 53.719, ratio: 0.469
#   pulse 4: activated nodes: 10620, borderline nodes: 7489, overall activation: 425.324, activation diff: 310.986, ratio: 0.731
#   pulse 5: activated nodes: 10995, borderline nodes: 4594, overall activation: 912.399, activation diff: 487.075, ratio: 0.534
#   pulse 6: activated nodes: 11291, borderline nodes: 3198, overall activation: 1659.254, activation diff: 746.855, ratio: 0.450
#   pulse 7: activated nodes: 11391, borderline nodes: 1269, overall activation: 2497.669, activation diff: 838.415, ratio: 0.336
#   pulse 8: activated nodes: 11411, borderline nodes: 606, overall activation: 3252.806, activation diff: 755.136, ratio: 0.232
#   pulse 9: activated nodes: 11427, borderline nodes: 325, overall activation: 3825.938, activation diff: 573.133, ratio: 0.150
#   pulse 10: activated nodes: 11436, borderline nodes: 192, overall activation: 4217.619, activation diff: 391.680, ratio: 0.093
#   pulse 11: activated nodes: 11443, borderline nodes: 146, overall activation: 4472.439, activation diff: 254.820, ratio: 0.057
#   pulse 12: activated nodes: 11445, borderline nodes: 126, overall activation: 4633.919, activation diff: 161.480, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 4633.9
#   number of spread. activ. pulses: 12
#   running time: 1450

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9640942   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8475801   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8419078   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7903402   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7813345   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   6   0.77625334   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.77581245   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.77575606   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7753003   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.7751514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7746535   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.77436334   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.7733715   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   14   0.7733116   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.77298164   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   16   0.7729385   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.77270806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.77253747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.77121806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7706773   REFERENCES:SIMLOC
