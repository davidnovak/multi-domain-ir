###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 337.813, activation diff: 353.832, ratio: 1.047
#   pulse 3: activated nodes: 9138, borderline nodes: 5077, overall activation: 234.461, activation diff: 565.875, ratio: 2.414
#   pulse 4: activated nodes: 11134, borderline nodes: 5978, overall activation: 2329.496, activation diff: 2453.990, ratio: 1.053
#   pulse 5: activated nodes: 11354, borderline nodes: 1665, overall activation: 1746.790, activation diff: 2822.205, ratio: 1.616
#   pulse 6: activated nodes: 11420, borderline nodes: 608, overall activation: 4231.987, activation diff: 2970.060, ratio: 0.702
#   pulse 7: activated nodes: 11436, borderline nodes: 208, overall activation: 4553.240, activation diff: 674.585, ratio: 0.148
#   pulse 8: activated nodes: 11445, borderline nodes: 120, overall activation: 4845.656, activation diff: 319.646, ratio: 0.066
#   pulse 9: activated nodes: 11445, borderline nodes: 93, overall activation: 4894.915, activation diff: 56.305, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 4894.9
#   number of spread. activ. pulses: 9
#   running time: 1368

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9701854   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8623711   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8603727   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7946387   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7905751   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   6   0.78579   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.78483874   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   8   0.78475326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7847281   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7847245   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   11   0.784369   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7843688   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.7840528   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.78370506   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.7823409   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.782291   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   17   0.7818534   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.7816192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7812973   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7806773   REFERENCES:SIMLOC
