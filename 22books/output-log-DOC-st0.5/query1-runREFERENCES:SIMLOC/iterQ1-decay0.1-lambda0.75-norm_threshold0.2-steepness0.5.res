###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 14.022, activation diff: 7.452, ratio: 0.531
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 30.912, activation diff: 18.255, ratio: 0.591
#   pulse 4: activated nodes: 8899, borderline nodes: 6663, overall activation: 71.055, activation diff: 40.999, ratio: 0.577
#   pulse 5: activated nodes: 10111, borderline nodes: 6925, overall activation: 157.157, activation diff: 86.534, ratio: 0.551
#   pulse 6: activated nodes: 10808, borderline nodes: 6035, overall activation: 311.982, activation diff: 154.969, ratio: 0.497
#   pulse 7: activated nodes: 11137, borderline nodes: 4763, overall activation: 562.350, activation diff: 250.379, ratio: 0.445
#   pulse 8: activated nodes: 11263, borderline nodes: 3364, overall activation: 931.248, activation diff: 368.898, ratio: 0.396
#   pulse 9: activated nodes: 11330, borderline nodes: 1959, overall activation: 1412.828, activation diff: 481.580, ratio: 0.341
#   pulse 10: activated nodes: 11397, borderline nodes: 1191, overall activation: 1964.557, activation diff: 551.729, ratio: 0.281
#   pulse 11: activated nodes: 11409, borderline nodes: 712, overall activation: 2536.738, activation diff: 572.180, ratio: 0.226
#   pulse 12: activated nodes: 11423, borderline nodes: 463, overall activation: 3091.609, activation diff: 554.871, ratio: 0.179
#   pulse 13: activated nodes: 11431, borderline nodes: 282, overall activation: 3600.594, activation diff: 508.985, ratio: 0.141
#   pulse 14: activated nodes: 11440, borderline nodes: 194, overall activation: 4049.532, activation diff: 448.939, ratio: 0.111
#   pulse 15: activated nodes: 11444, borderline nodes: 145, overall activation: 4435.632, activation diff: 386.100, ratio: 0.087

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11444
#   final overall activation: 4435.6
#   number of spread. activ. pulses: 15
#   running time: 1559

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8964428   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.82314354   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.8053471   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   4   0.79447293   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   5   0.7908003   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   6   0.78897494   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   7   0.78776526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.7859956   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7855922   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.78430104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   11   0.78350276   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   12   0.7834929   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.7827681   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   14   0.7803298   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.7780441   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7777478   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   17   0.7777038   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   18   0.7774986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   19   0.7770394   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.775298   REFERENCES:SIMLOC
