###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 19.103, activation diff: 12.083, ratio: 0.632
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 38.707, activation diff: 20.884, ratio: 0.540
#   pulse 4: activated nodes: 9743, borderline nodes: 5903, overall activation: 78.543, activation diff: 40.569, ratio: 0.517
#   pulse 5: activated nodes: 10299, borderline nodes: 5239, overall activation: 142.888, activation diff: 64.679, ratio: 0.453
#   pulse 6: activated nodes: 10625, borderline nodes: 4766, overall activation: 230.860, activation diff: 88.057, ratio: 0.381
#   pulse 7: activated nodes: 10716, borderline nodes: 4407, overall activation: 339.957, activation diff: 109.101, ratio: 0.321
#   pulse 8: activated nodes: 10789, borderline nodes: 4064, overall activation: 465.055, activation diff: 125.098, ratio: 0.269
#   pulse 9: activated nodes: 10848, borderline nodes: 3803, overall activation: 598.507, activation diff: 133.452, ratio: 0.223
#   pulse 10: activated nodes: 10868, borderline nodes: 3684, overall activation: 734.482, activation diff: 135.974, ratio: 0.185
#   pulse 11: activated nodes: 10874, borderline nodes: 3617, overall activation: 869.306, activation diff: 134.824, ratio: 0.155
#   pulse 12: activated nodes: 10884, borderline nodes: 3592, overall activation: 1000.602, activation diff: 131.296, ratio: 0.131
#   pulse 13: activated nodes: 10887, borderline nodes: 3563, overall activation: 1126.193, activation diff: 125.591, ratio: 0.112
#   pulse 14: activated nodes: 10889, borderline nodes: 3545, overall activation: 1243.811, activation diff: 117.619, ratio: 0.095
#   pulse 15: activated nodes: 10890, borderline nodes: 3533, overall activation: 1351.568, activation diff: 107.757, ratio: 0.080

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10890
#   final overall activation: 1351.6
#   number of spread. activ. pulses: 15
#   running time: 1620

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89802825   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.7302617   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.7162312   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.6799326   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.58075106   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.50570846   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.40797162   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.40142623   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.39172888   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.39096862   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.38378114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.37776512   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.3775465   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.37455273   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   15   0.37190673   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.37168464   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   17   0.37131494   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.37127456   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.37060696   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.37043992   REFERENCES:SIMLOC
