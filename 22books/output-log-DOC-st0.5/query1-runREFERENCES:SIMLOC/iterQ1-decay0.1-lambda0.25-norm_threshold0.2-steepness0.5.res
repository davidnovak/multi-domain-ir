###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 188.801, activation diff: 189.502, ratio: 1.004
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 221.237, activation diff: 207.770, ratio: 0.939
#   pulse 4: activated nodes: 11027, borderline nodes: 6682, overall activation: 1497.368, activation diff: 1317.326, ratio: 0.880
#   pulse 5: activated nodes: 11284, borderline nodes: 2389, overall activation: 2708.347, activation diff: 1218.131, ratio: 0.450
#   pulse 6: activated nodes: 11398, borderline nodes: 1049, overall activation: 4380.827, activation diff: 1672.480, ratio: 0.382
#   pulse 7: activated nodes: 11430, borderline nodes: 306, overall activation: 5396.200, activation diff: 1015.373, ratio: 0.188
#   pulse 8: activated nodes: 11445, borderline nodes: 113, overall activation: 5878.460, activation diff: 482.260, ratio: 0.082
#   pulse 9: activated nodes: 11447, borderline nodes: 68, overall activation: 6086.590, activation diff: 208.130, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 6086.6
#   number of spread. activ. pulses: 9
#   running time: 1405

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96906614   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8966094   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8949375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8937744   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8914392   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   6   0.8911889   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   7   0.8911549   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.8910615   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.8909881   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.89094114   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   11   0.890931   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8905824   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.89034986   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.890172   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.88916504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   16   0.8888468   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   17   0.88878715   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.8884709   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.88842344   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   20   0.8884134   REFERENCES:SIMLOC
