###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 38.754, activation diff: 32.117, ratio: 0.829
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 67.254, activation diff: 30.386, ratio: 0.452
#   pulse 4: activated nodes: 9779, borderline nodes: 7874, overall activation: 296.216, activation diff: 229.523, ratio: 0.775
#   pulse 5: activated nodes: 10591, borderline nodes: 5817, overall activation: 686.776, activation diff: 390.560, ratio: 0.569
#   pulse 6: activated nodes: 11158, borderline nodes: 4782, overall activation: 1446.454, activation diff: 759.678, ratio: 0.525
#   pulse 7: activated nodes: 11314, borderline nodes: 2417, overall activation: 2464.168, activation diff: 1017.713, ratio: 0.413
#   pulse 8: activated nodes: 11397, borderline nodes: 1103, overall activation: 3505.441, activation diff: 1041.273, ratio: 0.297
#   pulse 9: activated nodes: 11416, borderline nodes: 547, overall activation: 4368.605, activation diff: 863.164, ratio: 0.198
#   pulse 10: activated nodes: 11434, borderline nodes: 279, overall activation: 4986.580, activation diff: 617.976, ratio: 0.124
#   pulse 11: activated nodes: 11439, borderline nodes: 177, overall activation: 5397.219, activation diff: 410.639, ratio: 0.076
#   pulse 12: activated nodes: 11446, borderline nodes: 129, overall activation: 5660.207, activation diff: 262.988, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 5660.2
#   number of spread. activ. pulses: 12
#   running time: 1364

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9605614   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8914001   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.88548446   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.88400173   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.88199747   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   6   0.8817075   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.8814428   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   8   0.8813125   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.8809121   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.88066834   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.88036734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   12   0.8802887   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.8798345   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.87971836   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.8791772   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.87862444   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.87792313   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8774767   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_597   19   0.87663734   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   20   0.87658703   REFERENCES:SIMLOC
