###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 72.401, activation diff: 74.488, ratio: 1.029
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 54.795, activation diff: 68.601, ratio: 1.252
#   pulse 4: activated nodes: 9753, borderline nodes: 6964, overall activation: 360.076, activation diff: 322.281, ratio: 0.895
#   pulse 5: activated nodes: 10235, borderline nodes: 4977, overall activation: 532.800, activation diff: 185.375, ratio: 0.348
#   pulse 6: activated nodes: 10661, borderline nodes: 5026, overall activation: 877.829, activation diff: 345.029, ratio: 0.393
#   pulse 7: activated nodes: 10789, borderline nodes: 4064, overall activation: 1143.800, activation diff: 265.971, ratio: 0.233
#   pulse 8: activated nodes: 10845, borderline nodes: 3781, overall activation: 1372.041, activation diff: 228.240, ratio: 0.166
#   pulse 9: activated nodes: 10870, borderline nodes: 3672, overall activation: 1555.971, activation diff: 183.930, ratio: 0.118
#   pulse 10: activated nodes: 10876, borderline nodes: 3628, overall activation: 1683.642, activation diff: 127.671, ratio: 0.076
#   pulse 11: activated nodes: 10878, borderline nodes: 3600, overall activation: 1759.859, activation diff: 76.218, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10878
#   final overall activation: 1759.9
#   number of spread. activ. pulses: 11
#   running time: 1293

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9648678   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85192615   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8259692   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77159476   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6314478   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.59686506   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4615067   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.45085192   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4455941   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.43812147   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4340068   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   12   0.43355572   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.43258443   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.42725402   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.42634845   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.42506775   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.42293876   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.4220573   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.41778234   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.4168815   REFERENCES:SIMLOC
