###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 35.415, activation diff: 28.777, ratio: 0.813
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 60.126, activation diff: 26.631, ratio: 0.443
#   pulse 4: activated nodes: 9598, borderline nodes: 7711, overall activation: 246.994, activation diff: 187.478, ratio: 0.759
#   pulse 5: activated nodes: 10508, borderline nodes: 5973, overall activation: 550.111, activation diff: 303.124, ratio: 0.551
#   pulse 6: activated nodes: 11129, borderline nodes: 5095, overall activation: 1096.279, activation diff: 546.168, ratio: 0.498
#   pulse 7: activated nodes: 11280, borderline nodes: 2982, overall activation: 1811.294, activation diff: 715.015, ratio: 0.395
#   pulse 8: activated nodes: 11376, borderline nodes: 1484, overall activation: 2581.445, activation diff: 770.151, ratio: 0.298
#   pulse 9: activated nodes: 11401, borderline nodes: 802, overall activation: 3262.601, activation diff: 681.156, ratio: 0.209
#   pulse 10: activated nodes: 11417, borderline nodes: 489, overall activation: 3779.556, activation diff: 516.955, ratio: 0.137
#   pulse 11: activated nodes: 11425, borderline nodes: 313, overall activation: 4135.432, activation diff: 355.876, ratio: 0.086
#   pulse 12: activated nodes: 11428, borderline nodes: 233, overall activation: 4369.479, activation diff: 234.047, ratio: 0.054
#   pulse 13: activated nodes: 11434, borderline nodes: 187, overall activation: 4519.636, activation diff: 150.157, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 4519.6
#   number of spread. activ. pulses: 13
#   running time: 1471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9634438   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8420283   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8357563   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7909633   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.78202504   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   6   0.7765373   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.77617586   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   8   0.7759772   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7757142   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.77552927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.77506655   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.77462244   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.7736598   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   14   0.7734539   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   15   0.7730762   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.77280533   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.77248895   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7717563   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7710738   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7694286   REFERENCES:SIMLOC
