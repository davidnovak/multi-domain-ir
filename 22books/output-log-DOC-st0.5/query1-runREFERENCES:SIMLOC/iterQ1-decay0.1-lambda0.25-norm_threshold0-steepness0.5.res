###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 392.266, activation diff: 389.043, ratio: 0.992
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 694.695, activation diff: 402.318, ratio: 0.579
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 2860.101, activation diff: 2165.427, ratio: 0.757
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 4625.484, activation diff: 1765.383, ratio: 0.382
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 5731.889, activation diff: 1106.405, ratio: 0.193
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 6236.255, activation diff: 504.366, ratio: 0.081
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 6447.472, activation diff: 211.218, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6447.5
#   number of spread. activ. pulses: 8
#   running time: 1261

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9714192   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8965211   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8954998   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8944688   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8925104   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.89190435   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.89181495   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   8   0.8917434   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8916806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.89157164   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.8915234   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.89124966   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.8910793   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.8908125   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.89027417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   16   0.8899754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8897292   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.88958484   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.8895253   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.8893703   REFERENCES:SIMLOC
