###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 68.371, activation diff: 60.098, ratio: 0.879
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 130.158, activation diff: 62.928, ratio: 0.483
#   pulse 4: activated nodes: 10643, borderline nodes: 7474, overall activation: 519.995, activation diff: 389.935, ratio: 0.750
#   pulse 5: activated nodes: 11084, borderline nodes: 4379, overall activation: 1162.872, activation diff: 642.876, ratio: 0.553
#   pulse 6: activated nodes: 11313, borderline nodes: 2753, overall activation: 2205.312, activation diff: 1042.440, ratio: 0.473
#   pulse 7: activated nodes: 11400, borderline nodes: 948, overall activation: 3340.845, activation diff: 1135.534, ratio: 0.340
#   pulse 8: activated nodes: 11427, borderline nodes: 420, overall activation: 4307.593, activation diff: 966.747, ratio: 0.224
#   pulse 9: activated nodes: 11441, borderline nodes: 181, overall activation: 5004.666, activation diff: 697.073, ratio: 0.139
#   pulse 10: activated nodes: 11446, borderline nodes: 110, overall activation: 5466.816, activation diff: 462.150, ratio: 0.085
#   pulse 11: activated nodes: 11446, borderline nodes: 82, overall activation: 5761.185, activation diff: 294.370, ratio: 0.051
#   pulse 12: activated nodes: 11447, borderline nodes: 63, overall activation: 5945.046, activation diff: 183.861, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 5945.0
#   number of spread. activ. pulses: 12
#   running time: 1449

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9649597   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89343786   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8896081   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8883126   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   5   0.88581777   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   6   0.8858169   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.88560843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   8   0.8855514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.8854108   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   10   0.8853551   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   11   0.8852628   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.88524085   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.88467574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.88461703   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.88368404   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.8830206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.88292336   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.8822535   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.8820857   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.88195485   REFERENCES:SIMLOC
