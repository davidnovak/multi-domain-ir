###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 379.722, activation diff: 395.741, ratio: 1.042
#   pulse 3: activated nodes: 9138, borderline nodes: 5077, overall activation: 298.980, activation diff: 671.223, ratio: 2.245
#   pulse 4: activated nodes: 11151, borderline nodes: 5915, overall activation: 3061.985, activation diff: 3206.465, ratio: 1.047
#   pulse 5: activated nodes: 11366, borderline nodes: 1431, overall activation: 2513.819, activation diff: 3504.291, ratio: 1.394
#   pulse 6: activated nodes: 11433, borderline nodes: 408, overall activation: 5526.979, activation diff: 3540.429, ratio: 0.641
#   pulse 7: activated nodes: 11447, borderline nodes: 127, overall activation: 5935.287, activation diff: 681.156, ratio: 0.115
#   pulse 8: activated nodes: 11449, borderline nodes: 68, overall activation: 6181.279, activation diff: 274.209, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6181.3
#   number of spread. activ. pulses: 8
#   running time: 1370

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9704556   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8972725   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89667267   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89552206   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   5   0.8937387   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   6   0.8936823   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.89292514   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8929168   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8927843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.89263695   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   11   0.892603   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8924451   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.89236355   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.8918897   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.89151174   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.8914359   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_352   17   0.89105254   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.89099246   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.89094824   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   20   0.8907728   REFERENCES:SIMLOC
