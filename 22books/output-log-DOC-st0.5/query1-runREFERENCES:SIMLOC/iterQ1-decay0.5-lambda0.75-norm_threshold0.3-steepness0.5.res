###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 9.214, activation diff: 3.096, ratio: 0.336
#   pulse 3: activated nodes: 7552, borderline nodes: 7231, overall activation: 12.569, activation diff: 4.807, ratio: 0.382
#   pulse 4: activated nodes: 7938, borderline nodes: 6772, overall activation: 18.449, activation diff: 7.120, ratio: 0.386
#   pulse 5: activated nodes: 8523, borderline nodes: 7012, overall activation: 31.228, activation diff: 13.781, ratio: 0.441
#   pulse 6: activated nodes: 8881, borderline nodes: 6707, overall activation: 55.072, activation diff: 24.464, ratio: 0.444
#   pulse 7: activated nodes: 9281, borderline nodes: 6445, overall activation: 93.034, activation diff: 38.239, ratio: 0.411
#   pulse 8: activated nodes: 9734, borderline nodes: 6062, overall activation: 145.954, activation diff: 53.016, ratio: 0.363
#   pulse 9: activated nodes: 10143, borderline nodes: 5712, overall activation: 212.930, activation diff: 66.994, ratio: 0.315
#   pulse 10: activated nodes: 10420, borderline nodes: 5406, overall activation: 292.907, activation diff: 79.977, ratio: 0.273
#   pulse 11: activated nodes: 10586, borderline nodes: 5088, overall activation: 385.089, activation diff: 92.182, ratio: 0.239
#   pulse 12: activated nodes: 10674, borderline nodes: 4801, overall activation: 485.713, activation diff: 100.624, ratio: 0.207
#   pulse 13: activated nodes: 10709, borderline nodes: 4529, overall activation: 589.649, activation diff: 103.936, ratio: 0.176
#   pulse 14: activated nodes: 10762, borderline nodes: 4252, overall activation: 693.328, activation diff: 103.680, ratio: 0.150
#   pulse 15: activated nodes: 10794, borderline nodes: 4026, overall activation: 794.800, activation diff: 101.471, ratio: 0.128

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10794
#   final overall activation: 794.8
#   number of spread. activ. pulses: 15
#   running time: 1610

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8078687   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.59124845   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.55016243   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.4914323   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.46324074   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   6   0.35064846   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   7   0.34659564   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   8   0.3452623   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.34169787   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   10   0.33603513   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.33602917   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.33243078   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.3265825   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.3248837   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   15   0.32397   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.32344434   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   17   0.3194039   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.3160578   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.31597674   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.3150178   REFERENCES:SIMLOC
