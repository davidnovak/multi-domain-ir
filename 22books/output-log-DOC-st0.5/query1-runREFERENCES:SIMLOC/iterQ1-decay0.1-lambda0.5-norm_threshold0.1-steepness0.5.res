###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 112.942, activation diff: 103.095, ratio: 0.913
#   pulse 3: activated nodes: 9187, borderline nodes: 4792, overall activation: 232.198, activation diff: 120.145, ratio: 0.517
#   pulse 4: activated nodes: 11116, borderline nodes: 5778, overall activation: 848.741, activation diff: 616.581, ratio: 0.726
#   pulse 5: activated nodes: 11318, borderline nodes: 2093, overall activation: 1827.167, activation diff: 978.426, ratio: 0.535
#   pulse 6: activated nodes: 11411, borderline nodes: 831, overall activation: 3052.625, activation diff: 1225.458, ratio: 0.401
#   pulse 7: activated nodes: 11436, borderline nodes: 259, overall activation: 4147.340, activation diff: 1094.716, ratio: 0.264
#   pulse 8: activated nodes: 11447, borderline nodes: 106, overall activation: 4956.110, activation diff: 808.769, ratio: 0.163
#   pulse 9: activated nodes: 11448, borderline nodes: 57, overall activation: 5496.319, activation diff: 540.210, ratio: 0.098
#   pulse 10: activated nodes: 11449, borderline nodes: 43, overall activation: 5840.454, activation diff: 344.135, ratio: 0.059
#   pulse 11: activated nodes: 11450, borderline nodes: 28, overall activation: 6054.664, activation diff: 214.209, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 6054.7
#   number of spread. activ. pulses: 11
#   running time: 1295

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96439815   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8919067   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.88795877   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.88666683   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   5   0.8841538   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   6   0.88397896   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   7   0.88392794   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8838379   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   9   0.88361216   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.88355297   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.88353765   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.8832144   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.8827198   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.88271964   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.88195634   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.881271   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.88114285   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   18   0.88062716   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.88024026   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   20   0.88003296   REFERENCES:SIMLOC
