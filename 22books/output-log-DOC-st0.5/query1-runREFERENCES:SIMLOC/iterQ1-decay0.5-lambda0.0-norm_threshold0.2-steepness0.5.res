###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 212.085, activation diff: 228.105, ratio: 1.076
#   pulse 3: activated nodes: 9138, borderline nodes: 5077, overall activation: 87.398, activation diff: 296.614, ratio: 3.394
#   pulse 4: activated nodes: 10488, borderline nodes: 6017, overall activation: 885.767, activation diff: 945.559, ratio: 1.068
#   pulse 5: activated nodes: 10767, borderline nodes: 4044, overall activation: 365.315, activation diff: 1076.343, ratio: 2.946
#   pulse 6: activated nodes: 10818, borderline nodes: 3790, overall activation: 1377.317, activation diff: 1225.815, ratio: 0.890
#   pulse 7: activated nodes: 10877, borderline nodes: 3623, overall activation: 1287.798, activation diff: 611.191, ratio: 0.475
#   pulse 8: activated nodes: 10887, borderline nodes: 3588, overall activation: 1776.641, activation diff: 519.385, ratio: 0.292
#   pulse 9: activated nodes: 10889, borderline nodes: 3563, overall activation: 1849.359, activation diff: 93.816, ratio: 0.051
#   pulse 10: activated nodes: 10889, borderline nodes: 3554, overall activation: 1895.399, activation diff: 46.727, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10889
#   final overall activation: 1895.4
#   number of spread. activ. pulses: 10
#   running time: 1367

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9681149   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86174625   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8476093   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.78370506   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.65102214   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6164273   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.46429637   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4551861   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   9   0.4495052   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.44232938   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.43787163   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.43778586   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   13   0.43741104   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.43472576   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.43213362   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.43048468   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   17   0.42814645   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.42600244   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_224   19   0.42417192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.42185026   REFERENCES:SIMLOC
