###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 12.065, activation diff: 5.495, ratio: 0.455
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 21.487, activation diff: 10.786, ratio: 0.502
#   pulse 4: activated nodes: 8780, borderline nodes: 6555, overall activation: 39.608, activation diff: 19.020, ratio: 0.480
#   pulse 5: activated nodes: 9305, borderline nodes: 6433, overall activation: 73.806, activation diff: 34.722, ratio: 0.470
#   pulse 6: activated nodes: 9825, borderline nodes: 5930, overall activation: 127.267, activation diff: 53.696, ratio: 0.422
#   pulse 7: activated nodes: 10238, borderline nodes: 5459, overall activation: 199.523, activation diff: 72.331, ratio: 0.363
#   pulse 8: activated nodes: 10563, borderline nodes: 5115, overall activation: 288.985, activation diff: 89.462, ratio: 0.310
#   pulse 9: activated nodes: 10673, borderline nodes: 4762, overall activation: 393.609, activation diff: 104.624, ratio: 0.266
#   pulse 10: activated nodes: 10724, borderline nodes: 4419, overall activation: 508.116, activation diff: 114.506, ratio: 0.225
#   pulse 11: activated nodes: 10785, borderline nodes: 4132, overall activation: 626.675, activation diff: 118.559, ratio: 0.189
#   pulse 12: activated nodes: 10836, borderline nodes: 3902, overall activation: 745.363, activation diff: 118.688, ratio: 0.159
#   pulse 13: activated nodes: 10847, borderline nodes: 3764, overall activation: 861.948, activation diff: 116.585, ratio: 0.135
#   pulse 14: activated nodes: 10864, borderline nodes: 3704, overall activation: 975.332, activation diff: 113.384, ratio: 0.116
#   pulse 15: activated nodes: 10872, borderline nodes: 3666, overall activation: 1084.687, activation diff: 109.355, ratio: 0.101

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10872
#   final overall activation: 1084.7
#   number of spread. activ. pulses: 15
#   running time: 1610

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8671922   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.67770106   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.63041145   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.625966   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.5445839   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.441708   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.38581622   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.38266683   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.37325177   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.370062   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   11   0.36248302   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.362392   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   13   0.3581655   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.35467494   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.35425222   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   16   0.3529807   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   17   0.35286993   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.35258693   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.35027125   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.3498558   REFERENCES:SIMLOC
