###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 9.388, activation diff: 3.270, ratio: 0.348
#   pulse 3: activated nodes: 7552, borderline nodes: 7231, overall activation: 14.907, activation diff: 6.970, ratio: 0.468
#   pulse 4: activated nodes: 7938, borderline nodes: 6772, overall activation: 25.312, activation diff: 11.589, ratio: 0.458
#   pulse 5: activated nodes: 8610, borderline nodes: 7039, overall activation: 51.985, activation diff: 27.543, ratio: 0.530
#   pulse 6: activated nodes: 9432, borderline nodes: 6999, overall activation: 104.159, activation diff: 52.620, ratio: 0.505
#   pulse 7: activated nodes: 10303, borderline nodes: 6916, overall activation: 192.684, activation diff: 88.667, ratio: 0.460
#   pulse 8: activated nodes: 10797, borderline nodes: 6117, overall activation: 328.469, activation diff: 135.823, ratio: 0.414
#   pulse 9: activated nodes: 11075, borderline nodes: 5195, overall activation: 524.219, activation diff: 195.750, ratio: 0.373
#   pulse 10: activated nodes: 11205, borderline nodes: 4141, overall activation: 788.271, activation diff: 264.052, ratio: 0.335
#   pulse 11: activated nodes: 11286, borderline nodes: 2984, overall activation: 1111.887, activation diff: 323.616, ratio: 0.291
#   pulse 12: activated nodes: 11359, borderline nodes: 2012, overall activation: 1481.989, activation diff: 370.102, ratio: 0.250
#   pulse 13: activated nodes: 11383, borderline nodes: 1374, overall activation: 1875.825, activation diff: 393.836, ratio: 0.210
#   pulse 14: activated nodes: 11396, borderline nodes: 970, overall activation: 2269.190, activation diff: 393.365, ratio: 0.173
#   pulse 15: activated nodes: 11407, borderline nodes: 730, overall activation: 2643.319, activation diff: 374.128, ratio: 0.142

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11407
#   final overall activation: 2643.3
#   number of spread. activ. pulses: 15
#   running time: 1563

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84963965   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.6802732   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.66327864   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   4   0.6541608   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   5   0.63028634   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   6   0.6243493   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   7   0.6242199   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   8   0.6240137   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.6234753   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   10   0.6229994   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   11   0.6188159   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.6181208   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.615849   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.61114115   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   15   0.60915685   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   16   0.60814077   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.60794204   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.6067548   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   19   0.60557246   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   20   0.60528153   REFERENCES:SIMLOC
