###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 46.666, activation diff: 39.202, ratio: 0.840
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 108.221, activation diff: 62.751, ratio: 0.580
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 247.994, activation diff: 140.328, ratio: 0.566
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 476.809, activation diff: 228.981, ratio: 0.480
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 797.797, activation diff: 321.002, ratio: 0.402
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 1191.842, activation diff: 394.046, ratio: 0.331
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 1620.988, activation diff: 429.145, ratio: 0.265
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 2049.927, activation diff: 428.940, ratio: 0.209
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 2453.257, activation diff: 403.330, ratio: 0.164
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 2815.845, activation diff: 362.588, ratio: 0.129
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 3132.152, activation diff: 316.307, ratio: 0.101
#   pulse 13: activated nodes: 11455, borderline nodes: 19, overall activation: 3402.571, activation diff: 270.419, ratio: 0.079
#   pulse 14: activated nodes: 11455, borderline nodes: 19, overall activation: 3630.474, activation diff: 227.903, ratio: 0.063
#   pulse 15: activated nodes: 11455, borderline nodes: 19, overall activation: 3820.526, activation diff: 190.052, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 3820.5
#   number of spread. activ. pulses: 15
#   running time: 1458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9262239   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.7951962   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.791368   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.73013943   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.69740975   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.6710826   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.67074466   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.6690564   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.6669556   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   10   0.6659355   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.6653925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.6622925   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.661813   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   14   0.6609878   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.66057897   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.65986514   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.6573062   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.6567976   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.65678334   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   20   0.6561948   REFERENCES:SIMLOC
