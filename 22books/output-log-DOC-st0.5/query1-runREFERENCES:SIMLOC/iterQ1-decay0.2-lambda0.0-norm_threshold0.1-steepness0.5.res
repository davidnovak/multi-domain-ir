###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 455.404, activation diff: 472.250, ratio: 1.037
#   pulse 3: activated nodes: 9505, borderline nodes: 3995, overall activation: 357.309, activation diff: 790.541, ratio: 2.212
#   pulse 4: activated nodes: 11276, borderline nodes: 3904, overall activation: 2877.461, activation diff: 2949.736, ratio: 1.025
#   pulse 5: activated nodes: 11411, borderline nodes: 780, overall activation: 2586.502, activation diff: 2579.441, ratio: 0.997
#   pulse 6: activated nodes: 11442, borderline nodes: 190, overall activation: 4656.832, activation diff: 2343.439, ratio: 0.503
#   pulse 7: activated nodes: 11448, borderline nodes: 81, overall activation: 4914.000, activation diff: 366.829, ratio: 0.075
#   pulse 8: activated nodes: 11448, borderline nodes: 53, overall activation: 5037.133, activation diff: 133.557, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 5037.1
#   number of spread. activ. pulses: 8
#   running time: 1363

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97173196   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8686431   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8676286   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79492235   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.79380417   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7910748   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.7865817   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   8   0.7860064   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.78597987   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.78572375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.78556526   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7852057   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   13   0.78499085   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.784618   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   15   0.78349775   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.7832578   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   17   0.7828502   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.78245014   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.78220046   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7817952   REFERENCES:SIMLOC
