###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 316.858, activation diff: 332.877, ratio: 1.051
#   pulse 3: activated nodes: 9138, borderline nodes: 5077, overall activation: 204.922, activation diff: 515.926, ratio: 2.518
#   pulse 4: activated nodes: 11132, borderline nodes: 6019, overall activation: 2009.745, activation diff: 2123.817, ratio: 1.057
#   pulse 5: activated nodes: 11349, borderline nodes: 1753, overall activation: 1416.059, activation diff: 2479.895, ratio: 1.751
#   pulse 6: activated nodes: 11414, borderline nodes: 724, overall activation: 3633.669, activation diff: 2673.096, ratio: 0.736
#   pulse 7: activated nodes: 11429, borderline nodes: 303, overall activation: 3898.587, activation diff: 676.569, ratio: 0.174
#   pulse 8: activated nodes: 11432, borderline nodes: 195, overall activation: 4222.273, activation diff: 352.623, ratio: 0.084
#   pulse 9: activated nodes: 11436, borderline nodes: 157, overall activation: 4274.554, activation diff: 59.928, ratio: 0.014

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 4274.6
#   number of spread. activ. pulses: 9
#   running time: 1278

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96985596   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86236703   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8590241   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7843514   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.74251354   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7342663   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   7   0.7296842   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.7289192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.7280192   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.72784126   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.72718716   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7269882   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7263943   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   14   0.72487175   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   15   0.72437644   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.72387594   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.72319806   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   18   0.7231873   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   19   0.72102284   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7206991   REFERENCES:SIMLOC
