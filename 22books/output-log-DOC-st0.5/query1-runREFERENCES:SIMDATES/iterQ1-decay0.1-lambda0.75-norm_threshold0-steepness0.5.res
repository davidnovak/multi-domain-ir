###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 51.485, activation diff: 44.020, ratio: 0.855
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 121.506, activation diff: 71.220, ratio: 0.586
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 287.097, activation diff: 166.131, ratio: 0.579
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 550.397, activation diff: 263.452, ratio: 0.479
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 903.759, activation diff: 353.377, ratio: 0.391
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 1314.195, activation diff: 410.435, ratio: 0.312
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 1745.042, activation diff: 430.847, ratio: 0.247
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 2167.698, activation diff: 422.656, ratio: 0.195
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 2563.640, activation diff: 395.942, ratio: 0.154
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 2922.755, activation diff: 359.115, ratio: 0.123
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 3240.958, activation diff: 318.202, ratio: 0.098
#   pulse 13: activated nodes: 11348, borderline nodes: 0, overall activation: 3518.090, activation diff: 277.132, ratio: 0.079
#   pulse 14: activated nodes: 11348, borderline nodes: 0, overall activation: 3756.333, activation diff: 238.243, ratio: 0.063
#   pulse 15: activated nodes: 11348, borderline nodes: 0, overall activation: 3959.102, activation diff: 202.769, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 3959.1
#   number of spread. activ. pulses: 15
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9283203   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.85135156   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8378719   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   4   0.8350317   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   5   0.83383477   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   6   0.8321717   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   7   0.8321433   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   8   0.8311902   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   9   0.8306043   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   10   0.8299063   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   11   0.8297122   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8292729   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   13   0.82893395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.82846546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.82704246   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   16   0.825693   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.82558274   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.8250726   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   19   0.82367253   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.82295567   REFERENCES:SIMDATES
