###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 276.519, activation diff: 291.587, ratio: 1.054
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 196.238, activation diff: 472.757, ratio: 2.409
#   pulse 4: activated nodes: 9670, borderline nodes: 5871, overall activation: 2133.976, activation diff: 2319.088, ratio: 1.087
#   pulse 5: activated nodes: 10979, borderline nodes: 3460, overall activation: 923.712, activation diff: 3045.735, ratio: 3.297
#   pulse 6: activated nodes: 11170, borderline nodes: 1529, overall activation: 3071.195, activation diff: 3929.514, ratio: 1.279
#   pulse 7: activated nodes: 11250, borderline nodes: 1101, overall activation: 1209.571, activation diff: 4154.438, ratio: 3.435
#   pulse 8: activated nodes: 11299, borderline nodes: 612, overall activation: 3281.236, activation diff: 4123.265, ratio: 1.257
#   pulse 9: activated nodes: 11314, borderline nodes: 557, overall activation: 2744.170, activation diff: 2651.183, ratio: 0.966
#   pulse 10: activated nodes: 11315, borderline nodes: 514, overall activation: 3985.517, activation diff: 1973.922, ratio: 0.495
#   pulse 11: activated nodes: 11315, borderline nodes: 505, overall activation: 4207.595, activation diff: 538.839, ratio: 0.128
#   pulse 12: activated nodes: 11316, borderline nodes: 501, overall activation: 4333.103, activation diff: 204.277, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 4333.1
#   number of spread. activ. pulses: 12
#   running time: 571

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9686809   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.89718896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.89539015   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.89413226   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.8928558   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.8919711   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.89139205   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.8913202   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.8907106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.8905803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.8902787   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8900643   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.8900441   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.88997084   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   15   0.88996875   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.88946944   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8890096   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.88888663   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.88784593   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.88759506   REFERENCES:SIMDATES
