###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 416.272, activation diff: 433.891, ratio: 1.042
#   pulse 3: activated nodes: 8496, borderline nodes: 3466, overall activation: 240.768, activation diff: 656.610, ratio: 2.727
#   pulse 4: activated nodes: 10632, borderline nodes: 3722, overall activation: 1819.320, activation diff: 2038.474, ratio: 1.120
#   pulse 5: activated nodes: 11199, borderline nodes: 2136, overall activation: 654.976, activation diff: 2414.510, ratio: 3.686
#   pulse 6: activated nodes: 11274, borderline nodes: 741, overall activation: 2167.120, activation diff: 2632.657, ratio: 1.215
#   pulse 7: activated nodes: 11303, borderline nodes: 591, overall activation: 1634.696, activation diff: 1795.862, ratio: 1.099
#   pulse 8: activated nodes: 11306, borderline nodes: 557, overall activation: 2578.541, activation diff: 1428.877, ratio: 0.554
#   pulse 9: activated nodes: 11314, borderline nodes: 533, overall activation: 2728.123, activation diff: 356.607, ratio: 0.131
#   pulse 10: activated nodes: 11316, borderline nodes: 528, overall activation: 2812.013, activation diff: 130.045, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 2812.0
#   number of spread. activ. pulses: 10
#   running time: 552

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97124404   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.868639   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.84982544   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7840703   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7434169   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.732636   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.73042816   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7289351   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7244113   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7243388   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   11   0.7211227   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7210885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.7197638   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.7197319   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.7190739   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.71833444   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.7163632   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.71569395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   19   0.71514046   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   20   0.7146823   REFERENCES:SIMDATES
