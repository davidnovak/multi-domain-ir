###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 13.252, activation diff: 6.681, ratio: 0.504
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 27.131, activation diff: 15.246, ratio: 0.562
#   pulse 4: activated nodes: 7490, borderline nodes: 5288, overall activation: 57.363, activation diff: 31.111, ratio: 0.542
#   pulse 5: activated nodes: 8690, borderline nodes: 5672, overall activation: 117.314, activation diff: 60.417, ratio: 0.515
#   pulse 6: activated nodes: 9649, borderline nodes: 5419, overall activation: 213.089, activation diff: 95.962, ratio: 0.450
#   pulse 7: activated nodes: 10225, borderline nodes: 5006, overall activation: 346.430, activation diff: 133.373, ratio: 0.385
#   pulse 8: activated nodes: 10599, borderline nodes: 4352, overall activation: 514.305, activation diff: 167.875, ratio: 0.326
#   pulse 9: activated nodes: 10855, borderline nodes: 3668, overall activation: 708.419, activation diff: 194.113, ratio: 0.274
#   pulse 10: activated nodes: 11006, borderline nodes: 3093, overall activation: 916.465, activation diff: 208.047, ratio: 0.227
#   pulse 11: activated nodes: 11099, borderline nodes: 2604, overall activation: 1126.178, activation diff: 209.712, ratio: 0.186
#   pulse 12: activated nodes: 11162, borderline nodes: 2232, overall activation: 1328.148, activation diff: 201.970, ratio: 0.152
#   pulse 13: activated nodes: 11195, borderline nodes: 1892, overall activation: 1516.323, activation diff: 188.175, ratio: 0.124
#   pulse 14: activated nodes: 11213, borderline nodes: 1573, overall activation: 1687.309, activation diff: 170.986, ratio: 0.101
#   pulse 15: activated nodes: 11226, borderline nodes: 1386, overall activation: 1839.743, activation diff: 152.435, ratio: 0.083

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11226
#   final overall activation: 1839.7
#   number of spread. activ. pulses: 15
#   running time: 540

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8872904   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.70607495   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.6982471   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.6637112   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   5   0.6374703   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   6   0.63136387   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.6309695   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   8   0.6008291   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.5990584   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   10   0.5965822   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   11   0.59496677   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   12   0.5947604   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   13   0.59448117   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.59265316   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   15   0.5914122   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   16   0.59137964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   17   0.59100163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   18   0.58859175   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.58803916   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   20   0.58326983   REFERENCES:SIMDATES
