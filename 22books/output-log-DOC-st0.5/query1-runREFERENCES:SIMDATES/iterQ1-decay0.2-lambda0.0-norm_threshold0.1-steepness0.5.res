###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 443.843, activation diff: 461.462, ratio: 1.040
#   pulse 3: activated nodes: 8496, borderline nodes: 3466, overall activation: 285.506, activation diff: 728.912, ratio: 2.553
#   pulse 4: activated nodes: 10641, borderline nodes: 3675, overall activation: 2124.990, activation diff: 2383.074, ratio: 1.121
#   pulse 5: activated nodes: 11205, borderline nodes: 2070, overall activation: 834.129, activation diff: 2870.748, ratio: 3.442
#   pulse 6: activated nodes: 11283, borderline nodes: 589, overall activation: 2586.708, activation diff: 3127.375, ratio: 1.209
#   pulse 7: activated nodes: 11318, borderline nodes: 449, overall activation: 2241.342, activation diff: 1909.425, ratio: 0.852
#   pulse 8: activated nodes: 11320, borderline nodes: 321, overall activation: 3159.549, activation diff: 1394.775, ratio: 0.441
#   pulse 9: activated nodes: 11320, borderline nodes: 287, overall activation: 3323.175, activation diff: 344.570, ratio: 0.104
#   pulse 10: activated nodes: 11321, borderline nodes: 275, overall activation: 3397.932, activation diff: 117.513, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 3397.9
#   number of spread. activ. pulses: 10
#   running time: 473

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97147757   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8686418   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8508842   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7951984   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.78829855   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   6   0.78628093   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.7859725   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7849023   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.7830674   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.78203243   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.7818683   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.7815375   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.78121346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.78039855   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7803337   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.7795948   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.7789565   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.77808887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.7764289   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   20   0.7761392   REFERENCES:SIMDATES
