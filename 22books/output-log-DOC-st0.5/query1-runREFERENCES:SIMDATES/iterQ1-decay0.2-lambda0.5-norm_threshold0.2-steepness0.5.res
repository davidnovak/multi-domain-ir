###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 61.540, activation diff: 53.433, ratio: 0.868
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 110.597, activation diff: 50.287, ratio: 0.455
#   pulse 4: activated nodes: 9113, borderline nodes: 6061, overall activation: 389.815, activation diff: 279.370, ratio: 0.717
#   pulse 5: activated nodes: 10309, borderline nodes: 4495, overall activation: 782.398, activation diff: 392.583, ratio: 0.502
#   pulse 6: activated nodes: 10845, borderline nodes: 3655, overall activation: 1304.399, activation diff: 522.001, ratio: 0.400
#   pulse 7: activated nodes: 11131, borderline nodes: 2543, overall activation: 1815.713, activation diff: 511.314, ratio: 0.282
#   pulse 8: activated nodes: 11213, borderline nodes: 1678, overall activation: 2243.315, activation diff: 427.603, ratio: 0.191
#   pulse 9: activated nodes: 11230, borderline nodes: 1152, overall activation: 2567.573, activation diff: 324.257, ratio: 0.126
#   pulse 10: activated nodes: 11274, borderline nodes: 900, overall activation: 2799.286, activation diff: 231.713, ratio: 0.083
#   pulse 11: activated nodes: 11286, borderline nodes: 763, overall activation: 2958.830, activation diff: 159.544, ratio: 0.054
#   pulse 12: activated nodes: 11291, borderline nodes: 693, overall activation: 3066.088, activation diff: 107.258, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11291
#   final overall activation: 3066.1
#   number of spread. activ. pulses: 12
#   running time: 508

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9634883   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8475793   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.82511425   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7904274   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.77866423   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.7781335   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.77343047   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.7713659   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.7711178   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7707672   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.76948655   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.76864207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.7682551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.7676681   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7674495   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.76637244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.7662772   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   18   0.76522547   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   19   0.7628106   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   20   0.76221955   REFERENCES:SIMDATES
