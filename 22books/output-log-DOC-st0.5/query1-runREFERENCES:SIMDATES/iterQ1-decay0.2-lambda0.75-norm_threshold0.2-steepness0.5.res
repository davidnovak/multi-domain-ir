###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 13.496, activation diff: 6.926, ratio: 0.513
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 28.300, activation diff: 16.171, ratio: 0.571
#   pulse 4: activated nodes: 7536, borderline nodes: 5333, overall activation: 61.398, activation diff: 33.971, ratio: 0.553
#   pulse 5: activated nodes: 8762, borderline nodes: 5714, overall activation: 128.237, activation diff: 67.295, ratio: 0.525
#   pulse 6: activated nodes: 9677, borderline nodes: 5368, overall activation: 237.495, activation diff: 109.435, ratio: 0.461
#   pulse 7: activated nodes: 10306, borderline nodes: 4884, overall activation: 393.709, activation diff: 156.237, ratio: 0.397
#   pulse 8: activated nodes: 10689, borderline nodes: 4178, overall activation: 594.908, activation diff: 201.199, ratio: 0.338
#   pulse 9: activated nodes: 10922, borderline nodes: 3419, overall activation: 830.752, activation diff: 235.844, ratio: 0.284
#   pulse 10: activated nodes: 11072, borderline nodes: 2840, overall activation: 1084.990, activation diff: 254.238, ratio: 0.234
#   pulse 11: activated nodes: 11155, borderline nodes: 2401, overall activation: 1341.836, activation diff: 256.846, ratio: 0.191
#   pulse 12: activated nodes: 11197, borderline nodes: 1982, overall activation: 1589.520, activation diff: 247.685, ratio: 0.156
#   pulse 13: activated nodes: 11218, borderline nodes: 1565, overall activation: 1820.335, activation diff: 230.815, ratio: 0.127
#   pulse 14: activated nodes: 11230, borderline nodes: 1302, overall activation: 2030.015, activation diff: 209.680, ratio: 0.103
#   pulse 15: activated nodes: 11232, borderline nodes: 1119, overall activation: 2216.838, activation diff: 186.823, ratio: 0.084

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11232
#   final overall activation: 2216.8
#   number of spread. activ. pulses: 15
#   running time: 554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89034617   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.71726626   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7118038   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   4   0.70654315   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   5   0.6891372   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.68599766   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   7   0.6628244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.6619193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.6592782   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.65877897   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.65662026   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   12   0.6546328   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.6545046   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   14   0.6543924   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.6541703   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   16   0.6534761   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.65220064   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   18   0.65208316   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   19   0.6511835   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   20   0.6479369   REFERENCES:SIMDATES
