###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 641.410, activation diff: 660.106, ratio: 1.029
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 520.938, activation diff: 1148.535, ratio: 2.205
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3111.659, activation diff: 3510.285, ratio: 1.128
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1960.375, activation diff: 3651.515, ratio: 1.863
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4114.293, activation diff: 3467.299, ratio: 0.843
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4574.176, activation diff: 1089.268, ratio: 0.238
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 4858.053, activation diff: 419.438, ratio: 0.086
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 4925.794, activation diff: 108.684, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 4925.8
#   number of spread. activ. pulses: 9
#   running time: 1505

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9731512   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.89760005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8961195   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8951307   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89386094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.89317256   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.8927178   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.89259475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.89206475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.8919975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.8918005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.89160174   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.8914938   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.89147776   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   15   0.8914255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.8910081   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8905611   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.8905335   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.88954675   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.88942313   REFERENCES:SIMDATES
