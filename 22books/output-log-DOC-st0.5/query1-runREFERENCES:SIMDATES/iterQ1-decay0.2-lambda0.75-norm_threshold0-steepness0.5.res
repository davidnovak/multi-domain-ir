###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 46.981, activation diff: 39.516, ratio: 0.841
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 107.029, activation diff: 61.247, ratio: 0.572
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 237.564, activation diff: 131.087, ratio: 0.552
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 435.254, activation diff: 197.858, ratio: 0.455
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 691.395, activation diff: 256.158, ratio: 0.370
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 984.327, activation diff: 292.932, ratio: 0.298
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 1290.168, activation diff: 305.841, ratio: 0.237
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 1589.944, activation diff: 299.776, ratio: 0.189
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 1871.185, activation diff: 281.241, ratio: 0.150
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 2126.939, activation diff: 255.753, ratio: 0.120
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 2354.264, activation diff: 227.325, ratio: 0.097
#   pulse 13: activated nodes: 11342, borderline nodes: 14, overall activation: 2552.885, activation diff: 198.621, ratio: 0.078
#   pulse 14: activated nodes: 11342, borderline nodes: 14, overall activation: 2724.157, activation diff: 171.272, ratio: 0.063
#   pulse 15: activated nodes: 11342, borderline nodes: 14, overall activation: 2870.334, activation diff: 146.177, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 2870.3
#   number of spread. activ. pulses: 15
#   running time: 525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.92610735   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.79716974   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7805096   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.74907386   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.7300132   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.72466624   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.7199209   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.7172081   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.7156822   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.71519417   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   11   0.7139836   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7139489   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.7134193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   14   0.7131638   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   15   0.71293914   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7094329   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   17   0.7093303   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   18   0.70924413   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.70746285   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7067695   REFERENCES:SIMDATES
