###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 24.275, activation diff: 17.255, ratio: 0.711
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 55.076, activation diff: 32.083, ratio: 0.583
#   pulse 4: activated nodes: 9400, borderline nodes: 5556, overall activation: 128.703, activation diff: 74.323, ratio: 0.577
#   pulse 5: activated nodes: 10257, borderline nodes: 4923, overall activation: 254.763, activation diff: 126.353, ratio: 0.496
#   pulse 6: activated nodes: 10736, borderline nodes: 4157, overall activation: 437.302, activation diff: 182.583, ratio: 0.418
#   pulse 7: activated nodes: 10997, borderline nodes: 3254, overall activation: 669.966, activation diff: 232.664, ratio: 0.347
#   pulse 8: activated nodes: 11146, borderline nodes: 2466, overall activation: 936.128, activation diff: 266.162, ratio: 0.284
#   pulse 9: activated nodes: 11198, borderline nodes: 1840, overall activation: 1215.989, activation diff: 279.860, ratio: 0.230
#   pulse 10: activated nodes: 11229, borderline nodes: 1275, overall activation: 1492.786, activation diff: 276.797, ratio: 0.185
#   pulse 11: activated nodes: 11269, borderline nodes: 951, overall activation: 1754.836, activation diff: 262.050, ratio: 0.149
#   pulse 12: activated nodes: 11278, borderline nodes: 763, overall activation: 1995.195, activation diff: 240.358, ratio: 0.120
#   pulse 13: activated nodes: 11288, borderline nodes: 649, overall activation: 2210.537, activation diff: 215.343, ratio: 0.097
#   pulse 14: activated nodes: 11297, borderline nodes: 578, overall activation: 2400.066, activation diff: 189.529, ratio: 0.079
#   pulse 15: activated nodes: 11306, borderline nodes: 530, overall activation: 2564.602, activation diff: 164.535, ratio: 0.064

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11306
#   final overall activation: 2564.6
#   number of spread. activ. pulses: 15
#   running time: 543

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91286427   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.76263326   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7529598   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.737649   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.71396816   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   6   0.7083716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7014252   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.6955925   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   9   0.6947154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.6929738   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.6928777   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.6925406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.6924807   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.6901543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   15   0.6877512   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.68725204   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.68669134   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.68586075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   19   0.6850478   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.6826656   REFERENCES:SIMDATES
