###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 311.129, activation diff: 308.712, ratio: 0.992
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 420.073, activation diff: 222.721, ratio: 0.530
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1388.297, activation diff: 968.394, ratio: 0.698
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2062.514, activation diff: 674.217, ratio: 0.327
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2533.315, activation diff: 470.801, ratio: 0.186
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2779.740, activation diff: 246.425, ratio: 0.089
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 2897.168, activation diff: 117.428, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 2897.2
#   number of spread. activ. pulses: 8
#   running time: 421

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9700488   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8699584   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.84901565   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.78317916   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7422992   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.73128974   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7280575   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7258122   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.72221553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.72181827   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.71857554   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.71773267   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.71698713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   14   0.71603316   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7154355   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7144892   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.71351635   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.71263075   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.7125417   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7121402   REFERENCES:SIMDATES
