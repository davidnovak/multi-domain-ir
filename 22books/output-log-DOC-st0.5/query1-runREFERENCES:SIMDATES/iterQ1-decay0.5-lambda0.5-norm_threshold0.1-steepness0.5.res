###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 66.828, activation diff: 57.082, ratio: 0.854
#   pulse 3: activated nodes: 8123, borderline nodes: 3942, overall activation: 110.813, activation diff: 44.911, ratio: 0.405
#   pulse 4: activated nodes: 8396, borderline nodes: 4149, overall activation: 265.411, activation diff: 154.679, ratio: 0.583
#   pulse 5: activated nodes: 8884, borderline nodes: 3664, overall activation: 426.985, activation diff: 161.574, ratio: 0.378
#   pulse 6: activated nodes: 8911, borderline nodes: 3631, overall activation: 569.861, activation diff: 142.876, ratio: 0.251
#   pulse 7: activated nodes: 8928, borderline nodes: 3613, overall activation: 679.229, activation diff: 109.368, ratio: 0.161
#   pulse 8: activated nodes: 8929, borderline nodes: 3613, overall activation: 755.772, activation diff: 76.543, ratio: 0.101
#   pulse 9: activated nodes: 8931, borderline nodes: 3611, overall activation: 806.169, activation diff: 50.396, ratio: 0.063
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 838.003, activation diff: 31.835, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 838.0
#   number of spread. activ. pulses: 10
#   running time: 423

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9465358   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8128084   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.79085505   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.71558726   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.62841284   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5548013   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.45277324   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4323144   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.42052984   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.41210395   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.40428934   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.39385384   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.38594115   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.37989146   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.37616187   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.37299746   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.36764097   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.36725718   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.36630088   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.36482498   REFERENCES:SIMDATES
