###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 570.486, activation diff: 589.183, ratio: 1.033
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 386.641, activation diff: 945.274, ratio: 2.445
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2355.056, activation diff: 2657.264, ratio: 1.128
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1320.901, activation diff: 2733.914, ratio: 2.070
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2997.153, activation diff: 2644.808, ratio: 0.882
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3286.241, activation diff: 827.794, ratio: 0.252
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 3521.858, activation diff: 347.631, ratio: 0.099
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 3573.682, activation diff: 86.072, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 3573.7
#   number of spread. activ. pulses: 9
#   running time: 484

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9727293   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8746459   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8566987   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.79541665   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   5   0.7937874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.788851   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.7866516   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7855066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.78391135   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7828023   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.7826998   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.7823233   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.78174084   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.781275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7809021   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.7804204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.77982706   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.77918285   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.77733195   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   20   0.77724004   REFERENCES:SIMDATES
