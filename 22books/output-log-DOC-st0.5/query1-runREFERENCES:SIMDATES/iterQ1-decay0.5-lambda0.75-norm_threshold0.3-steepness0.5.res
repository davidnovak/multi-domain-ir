###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 9.180, activation diff: 3.063, ratio: 0.334
#   pulse 3: activated nodes: 5588, borderline nodes: 5267, overall activation: 12.450, activation diff: 4.723, ratio: 0.379
#   pulse 4: activated nodes: 6310, borderline nodes: 5146, overall activation: 18.147, activation diff: 6.969, ratio: 0.384
#   pulse 5: activated nodes: 6545, borderline nodes: 5048, overall activation: 30.417, activation diff: 13.317, ratio: 0.438
#   pulse 6: activated nodes: 6940, borderline nodes: 4800, overall activation: 52.764, activation diff: 23.011, ratio: 0.436
#   pulse 7: activated nodes: 7404, borderline nodes: 4679, overall activation: 86.692, activation diff: 34.248, ratio: 0.395
#   pulse 8: activated nodes: 7875, borderline nodes: 4505, overall activation: 130.866, activation diff: 44.279, ratio: 0.338
#   pulse 9: activated nodes: 8178, borderline nodes: 4250, overall activation: 182.617, activation diff: 51.776, ratio: 0.284
#   pulse 10: activated nodes: 8377, borderline nodes: 4112, overall activation: 238.622, activation diff: 56.008, ratio: 0.235
#   pulse 11: activated nodes: 8498, borderline nodes: 3961, overall activation: 295.938, activation diff: 57.317, ratio: 0.194
#   pulse 12: activated nodes: 8611, borderline nodes: 3851, overall activation: 352.345, activation diff: 56.406, ratio: 0.160
#   pulse 13: activated nodes: 8724, borderline nodes: 3796, overall activation: 406.238, activation diff: 53.894, ratio: 0.133
#   pulse 14: activated nodes: 8769, borderline nodes: 3717, overall activation: 456.497, activation diff: 50.259, ratio: 0.110
#   pulse 15: activated nodes: 8820, borderline nodes: 3687, overall activation: 502.363, activation diff: 45.866, ratio: 0.091

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8820
#   final overall activation: 502.4
#   number of spread. activ. pulses: 15
#   running time: 517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.80198616   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.5712726   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   3   0.5207591   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   4   0.48772186   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.4407578   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   6   0.33142635   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   7   0.3300377   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.32901147   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   9   0.3274151   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   10   0.32293907   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.30925792   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   12   0.3079544   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.30454022   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   14   0.2992754   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.29759568   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.29658493   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.29522794   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   18   0.29504654   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   19   0.29080427   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.2888356   REFERENCES:SIMDATES
