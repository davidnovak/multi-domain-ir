###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 357.717, activation diff: 376.413, ratio: 1.052
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 75.501, activation diff: 426.483, ratio: 5.649
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 836.132, activation diff: 889.004, ratio: 1.063
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 190.973, activation diff: 806.920, ratio: 4.225
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 904.424, activation diff: 787.756, ratio: 0.871
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 848.913, activation diff: 131.848, ratio: 0.155
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 941.606, activation diff: 96.598, ratio: 0.103
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 941.768, activation diff: 4.260, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 941.8
#   number of spread. activ. pulses: 9
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96791637   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.87236077   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.83780044   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.76469874   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6693569   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.62182516   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.47250965   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.45615697   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.43745327   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.42718592   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.42229417   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   12   0.41628212   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4066617   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.4020923   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.40077716   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.39503175   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   17   0.39216396   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   18   0.3906084   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   19   0.38888216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_177   20   0.38642862   REFERENCES:SIMDATES
