###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 42.142, activation diff: 34.035, ratio: 0.808
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 68.302, activation diff: 27.500, ratio: 0.403
#   pulse 4: activated nodes: 7714, borderline nodes: 4914, overall activation: 187.492, activation diff: 119.486, ratio: 0.637
#   pulse 5: activated nodes: 8515, borderline nodes: 3976, overall activation: 323.844, activation diff: 136.354, ratio: 0.421
#   pulse 6: activated nodes: 8677, borderline nodes: 3845, overall activation: 459.049, activation diff: 135.205, ratio: 0.295
#   pulse 7: activated nodes: 8849, borderline nodes: 3687, overall activation: 572.790, activation diff: 113.741, ratio: 0.199
#   pulse 8: activated nodes: 8911, borderline nodes: 3637, overall activation: 659.264, activation diff: 86.474, ratio: 0.131
#   pulse 9: activated nodes: 8922, borderline nodes: 3609, overall activation: 720.194, activation diff: 60.930, ratio: 0.085
#   pulse 10: activated nodes: 8929, borderline nodes: 3613, overall activation: 760.759, activation diff: 40.565, ratio: 0.053
#   pulse 11: activated nodes: 8929, borderline nodes: 3613, overall activation: 786.700, activation diff: 25.942, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8929
#   final overall activation: 786.7
#   number of spread. activ. pulses: 11
#   running time: 425

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9477317   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8038527   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7841371   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7087518   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.61806464   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5387646   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4537409   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.43218565   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4201026   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.41125697   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4033764   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.39214063   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.38429445   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.37802607   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.37372854   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.3706639   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.3650676   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.36455262   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.36365938   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.3621183   REFERENCES:SIMDATES
