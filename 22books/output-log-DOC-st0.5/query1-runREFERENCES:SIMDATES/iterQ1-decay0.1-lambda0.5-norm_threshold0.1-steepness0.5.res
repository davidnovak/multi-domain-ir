###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 111.451, activation diff: 101.705, ratio: 0.913
#   pulse 3: activated nodes: 8123, borderline nodes: 3942, overall activation: 221.261, activation diff: 110.736, ratio: 0.500
#   pulse 4: activated nodes: 10122, borderline nodes: 5112, overall activation: 753.835, activation diff: 532.642, ratio: 0.707
#   pulse 5: activated nodes: 11023, borderline nodes: 3206, overall activation: 1476.628, activation diff: 722.793, ratio: 0.489
#   pulse 6: activated nodes: 11191, borderline nodes: 1738, overall activation: 2300.612, activation diff: 823.984, ratio: 0.358
#   pulse 7: activated nodes: 11276, borderline nodes: 815, overall activation: 3022.076, activation diff: 721.464, ratio: 0.239
#   pulse 8: activated nodes: 11302, borderline nodes: 446, overall activation: 3575.895, activation diff: 553.819, ratio: 0.155
#   pulse 9: activated nodes: 11317, borderline nodes: 287, overall activation: 3970.693, activation diff: 394.798, ratio: 0.099
#   pulse 10: activated nodes: 11322, borderline nodes: 207, overall activation: 4240.388, activation diff: 269.695, ratio: 0.064
#   pulse 11: activated nodes: 11326, borderline nodes: 153, overall activation: 4420.064, activation diff: 179.676, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 4420.1
#   number of spread. activ. pulses: 11
#   running time: 497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9639652   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8918331   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8868163   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8851032   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.88298845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   6   0.8828425   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   7   0.8823499   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.88197374   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.88190275   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.8815193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   11   0.8813516   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.88128346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.88127327   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.8810438   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   15   0.8805141   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.8797191   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   17   0.87895167   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.87829256   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.87799156   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.87777036   REFERENCES:SIMDATES
