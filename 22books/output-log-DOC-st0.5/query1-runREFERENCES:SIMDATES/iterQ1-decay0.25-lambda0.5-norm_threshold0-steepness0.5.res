###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 146.956, activation diff: 135.796, ratio: 0.924
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 287.039, activation diff: 140.771, ratio: 0.490
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 733.732, activation diff: 446.720, ratio: 0.609
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1253.743, activation diff: 520.010, ratio: 0.415
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 1746.369, activation diff: 492.626, ratio: 0.282
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2139.729, activation diff: 393.361, ratio: 0.184
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 2426.243, activation diff: 286.514, ratio: 0.118
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 2624.009, activation diff: 197.766, ratio: 0.075
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 2756.070, activation diff: 132.061, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 2756.1
#   number of spread. activ. pulses: 10
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9600315   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.85212207   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8306627   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7618268   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7340511   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.720431   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.71419996   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.71103466   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7085634   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7075904   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7042401   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.7025759   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.7023176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   14   0.7007453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.70068824   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.69938266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.69914937   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.6989519   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.6987767   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   20   0.696299   REFERENCES:SIMDATES
