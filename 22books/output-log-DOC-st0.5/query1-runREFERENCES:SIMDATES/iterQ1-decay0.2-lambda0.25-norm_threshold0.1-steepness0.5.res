###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 240.060, activation diff: 239.539, ratio: 0.998
#   pulse 3: activated nodes: 8357, borderline nodes: 3642, overall activation: 289.477, activation diff: 214.742, ratio: 0.742
#   pulse 4: activated nodes: 10452, borderline nodes: 4371, overall activation: 1294.124, activation diff: 1026.891, ratio: 0.794
#   pulse 5: activated nodes: 11145, borderline nodes: 2793, overall activation: 2005.313, activation diff: 711.678, ratio: 0.355
#   pulse 6: activated nodes: 11258, borderline nodes: 1042, overall activation: 2672.389, activation diff: 667.076, ratio: 0.250
#   pulse 7: activated nodes: 11298, borderline nodes: 616, overall activation: 3055.543, activation diff: 383.153, ratio: 0.125
#   pulse 8: activated nodes: 11310, borderline nodes: 437, overall activation: 3248.516, activation diff: 192.973, ratio: 0.059
#   pulse 9: activated nodes: 11319, borderline nodes: 346, overall activation: 3340.286, activation diff: 91.770, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11319
#   final overall activation: 3340.3
#   number of spread. activ. pulses: 9
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97025794   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8661213   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.84590715   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.794635   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.786778   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7850482   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7828915   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.78117245   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7802786   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   10   0.78024817   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7801563   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.7794727   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.7786239   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.77799803   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.7778172   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.77757186   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.7768109   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.77666336   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.7747489   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   20   0.77392673   REFERENCES:SIMDATES
