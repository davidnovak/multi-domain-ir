###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 106.795, activation diff: 108.188, ratio: 1.013
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 81.488, activation diff: 78.508, ratio: 0.963
#   pulse 4: activated nodes: 8111, borderline nodes: 4519, overall activation: 388.421, activation diff: 317.563, ratio: 0.818
#   pulse 5: activated nodes: 8817, borderline nodes: 3702, overall activation: 534.853, activation diff: 150.599, ratio: 0.282
#   pulse 6: activated nodes: 8818, borderline nodes: 3701, overall activation: 688.536, activation diff: 153.683, ratio: 0.223
#   pulse 7: activated nodes: 8924, borderline nodes: 3611, overall activation: 765.821, activation diff: 77.285, ratio: 0.101
#   pulse 8: activated nodes: 8929, borderline nodes: 3613, overall activation: 802.312, activation diff: 36.491, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8929
#   final overall activation: 802.3
#   number of spread. activ. pulses: 8
#   running time: 396

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.954785   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8231026   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7956011   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.719451   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6257331   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5515581   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.46029723   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4400888   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.42516187   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.41563946   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.40862522   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.39567757   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.38890964   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.3841641   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.38209906   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.3785653   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.36881512   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.3686248   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.3676303   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.36571062   REFERENCES:SIMDATES
