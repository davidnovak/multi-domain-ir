###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 105.370, activation diff: 107.910, ratio: 1.024
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 84.940, activation diff: 110.771, ratio: 1.304
#   pulse 4: activated nodes: 9019, borderline nodes: 6026, overall activation: 631.780, activation diff: 584.870, ratio: 0.926
#   pulse 5: activated nodes: 10324, borderline nodes: 4389, overall activation: 931.617, activation diff: 337.063, ratio: 0.362
#   pulse 6: activated nodes: 10742, borderline nodes: 3800, overall activation: 1588.419, activation diff: 657.054, ratio: 0.414
#   pulse 7: activated nodes: 11081, borderline nodes: 2745, overall activation: 2022.177, activation diff: 433.758, ratio: 0.215
#   pulse 8: activated nodes: 11170, borderline nodes: 1901, overall activation: 2288.081, activation diff: 265.904, ratio: 0.116
#   pulse 9: activated nodes: 11219, borderline nodes: 1466, overall activation: 2428.734, activation diff: 140.653, ratio: 0.058
#   pulse 10: activated nodes: 11224, borderline nodes: 1238, overall activation: 2499.347, activation diff: 70.613, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11224
#   final overall activation: 2499.3
#   number of spread. activ. pulses: 10
#   running time: 461

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9665375   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8533516   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.82546896   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.75450706   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7420522   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7297578   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.72532   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7236917   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.71925515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7186725   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7151936   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.71412385   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.7131046   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   14   0.71259093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.71169865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7101481   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.7095485   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.7087504   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.70848054   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   20   0.7076121   REFERENCES:SIMDATES
