###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 9.354, activation diff: 3.237, ratio: 0.346
#   pulse 3: activated nodes: 5588, borderline nodes: 5267, overall activation: 14.762, activation diff: 6.861, ratio: 0.465
#   pulse 4: activated nodes: 6310, borderline nodes: 5146, overall activation: 24.900, activation diff: 11.350, ratio: 0.456
#   pulse 5: activated nodes: 7088, borderline nodes: 5531, overall activation: 50.701, activation diff: 26.711, ratio: 0.527
#   pulse 6: activated nodes: 7942, borderline nodes: 5565, overall activation: 100.244, activation diff: 50.027, ratio: 0.499
#   pulse 7: activated nodes: 8981, borderline nodes: 5713, overall activation: 181.228, activation diff: 81.135, ratio: 0.448
#   pulse 8: activated nodes: 9652, borderline nodes: 5355, overall activation: 299.019, activation diff: 117.838, ratio: 0.394
#   pulse 9: activated nodes: 10214, borderline nodes: 4964, overall activation: 455.994, activation diff: 156.977, ratio: 0.344
#   pulse 10: activated nodes: 10589, borderline nodes: 4360, overall activation: 649.605, activation diff: 193.611, ratio: 0.298
#   pulse 11: activated nodes: 10819, borderline nodes: 3740, overall activation: 870.558, activation diff: 220.954, ratio: 0.254
#   pulse 12: activated nodes: 10972, borderline nodes: 3209, overall activation: 1105.089, activation diff: 234.531, ratio: 0.212
#   pulse 13: activated nodes: 11073, borderline nodes: 2776, overall activation: 1340.077, activation diff: 234.988, ratio: 0.175
#   pulse 14: activated nodes: 11123, borderline nodes: 2455, overall activation: 1565.680, activation diff: 225.603, ratio: 0.144
#   pulse 15: activated nodes: 11167, borderline nodes: 2132, overall activation: 1775.633, activation diff: 209.953, ratio: 0.118

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11167
#   final overall activation: 1775.6
#   number of spread. activ. pulses: 15
#   running time: 556

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84716153   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.676218   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   3   0.653653   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   4   0.64365375   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.6314519   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   6   0.6131016   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   7   0.6094463   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   8   0.60378605   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   9   0.6029213   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   10   0.6028231   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   11   0.60003406   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   12   0.5965842   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.5959214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   14   0.5952255   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.5940802   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.59175014   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   17   0.59090954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.59062445   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   19   0.58917236   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   20   0.5888059   REFERENCES:SIMDATES
