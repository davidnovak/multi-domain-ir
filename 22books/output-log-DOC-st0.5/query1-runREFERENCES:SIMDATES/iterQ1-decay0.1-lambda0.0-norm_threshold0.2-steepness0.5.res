###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 375.290, activation diff: 391.740, ratio: 1.044
#   pulse 3: activated nodes: 8045, borderline nodes: 4151, overall activation: 280.390, activation diff: 655.613, ratio: 2.338
#   pulse 4: activated nodes: 10123, borderline nodes: 5059, overall activation: 2509.584, activation diff: 2763.183, ratio: 1.101
#   pulse 5: activated nodes: 11110, borderline nodes: 2891, overall activation: 1085.061, activation diff: 3531.451, ratio: 3.255
#   pulse 6: activated nodes: 11268, borderline nodes: 849, overall activation: 3308.664, activation diff: 4121.747, ratio: 1.246
#   pulse 7: activated nodes: 11314, borderline nodes: 585, overall activation: 2559.299, activation diff: 3033.155, ratio: 1.185
#   pulse 8: activated nodes: 11316, borderline nodes: 353, overall activation: 4065.580, activation diff: 2398.194, ratio: 0.590
#   pulse 9: activated nodes: 11318, borderline nodes: 313, overall activation: 4354.673, activation diff: 665.140, ratio: 0.153
#   pulse 10: activated nodes: 11320, borderline nodes: 282, overall activation: 4509.714, activation diff: 245.810, ratio: 0.055
#   pulse 11: activated nodes: 11320, borderline nodes: 278, overall activation: 4548.667, activation diff: 72.016, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 4548.7
#   number of spread. activ. pulses: 11
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9702347   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8973316   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8956368   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8944849   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89318585   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.89238745   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.89185   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.89176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.89116776   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.891079   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.89078474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8905954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.8905066   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   14   0.8904825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.89046305   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.8900085   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8895142   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.88944095   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.888435   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.88816375   REFERENCES:SIMDATES
