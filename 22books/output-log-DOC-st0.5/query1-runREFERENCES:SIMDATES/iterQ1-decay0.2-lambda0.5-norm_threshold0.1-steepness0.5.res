###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 100.295, activation diff: 90.549, ratio: 0.903
#   pulse 3: activated nodes: 8123, borderline nodes: 3942, overall activation: 190.709, activation diff: 91.338, ratio: 0.479
#   pulse 4: activated nodes: 10108, borderline nodes: 5155, overall activation: 592.278, activation diff: 401.640, ratio: 0.678
#   pulse 5: activated nodes: 10975, borderline nodes: 3442, overall activation: 1113.202, activation diff: 520.924, ratio: 0.468
#   pulse 6: activated nodes: 11172, borderline nodes: 2067, overall activation: 1691.051, activation diff: 577.849, ratio: 0.342
#   pulse 7: activated nodes: 11233, borderline nodes: 1092, overall activation: 2195.353, activation diff: 504.301, ratio: 0.230
#   pulse 8: activated nodes: 11279, borderline nodes: 685, overall activation: 2583.917, activation diff: 388.564, ratio: 0.150
#   pulse 9: activated nodes: 11301, borderline nodes: 547, overall activation: 2862.441, activation diff: 278.523, ratio: 0.097
#   pulse 10: activated nodes: 11310, borderline nodes: 446, overall activation: 3053.537, activation diff: 191.097, ratio: 0.063
#   pulse 11: activated nodes: 11318, borderline nodes: 397, overall activation: 3181.097, activation diff: 127.559, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11318
#   final overall activation: 3181.1
#   number of spread. activ. pulses: 11
#   running time: 490

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9629998   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8514017   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8304547   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7889899   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.77724826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.7766926   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7717999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.77009594   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.7696444   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7694737   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.76810753   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.7671849   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.76682043   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   14   0.7664207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.76609623   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.765182   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.7649944   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   18   0.76393557   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   19   0.7623323   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   20   0.76232505   REFERENCES:SIMDATES
