###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 246.021, activation diff: 261.088, ratio: 1.061
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 148.553, activation diff: 394.574, ratio: 2.656
#   pulse 4: activated nodes: 9653, borderline nodes: 5885, overall activation: 1595.712, activation diff: 1739.384, ratio: 1.090
#   pulse 5: activated nodes: 10912, borderline nodes: 3589, overall activation: 611.108, activation diff: 2201.931, ratio: 3.603
#   pulse 6: activated nodes: 11108, borderline nodes: 1976, overall activation: 2241.412, activation diff: 2821.280, ratio: 1.259
#   pulse 7: activated nodes: 11218, borderline nodes: 1466, overall activation: 783.086, activation diff: 2981.438, ratio: 3.807
#   pulse 8: activated nodes: 11258, borderline nodes: 955, overall activation: 2335.493, activation diff: 3016.339, ratio: 1.292
#   pulse 9: activated nodes: 11269, borderline nodes: 930, overall activation: 1071.223, activation diff: 2767.747, ratio: 2.584
#   pulse 10: activated nodes: 11269, borderline nodes: 880, overall activation: 2570.284, activation diff: 2546.105, ratio: 0.991
#   pulse 11: activated nodes: 11272, borderline nodes: 873, overall activation: 2686.271, activation diff: 938.925, ratio: 0.350
#   pulse 12: activated nodes: 11273, borderline nodes: 870, overall activation: 3011.485, activation diff: 501.406, ratio: 0.166
#   pulse 13: activated nodes: 11273, borderline nodes: 867, overall activation: 3071.384, activation diff: 128.752, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11273
#   final overall activation: 3071.4
#   number of spread. activ. pulses: 13
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9675624   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.85581654   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.82938737   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7945408   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.78650373   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7843191   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7822687   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.78045744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.77931553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7791545   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   11   0.7790461   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.77742684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   13   0.7772588   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7764564   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7764559   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.775776   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.775531   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   18   0.7730809   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.772893   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.7713667   REFERENCES:SIMDATES
