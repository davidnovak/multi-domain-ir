###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 154.526, activation diff: 169.593, ratio: 1.098
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 47.044, activation diff: 201.570, ratio: 4.285
#   pulse 4: activated nodes: 7960, borderline nodes: 4813, overall activation: 581.286, activation diff: 627.565, ratio: 1.080
#   pulse 5: activated nodes: 8836, borderline nodes: 3701, overall activation: 82.637, activation diff: 663.158, ratio: 8.025
#   pulse 6: activated nodes: 8837, borderline nodes: 3700, overall activation: 673.518, activation diff: 750.074, ratio: 1.114
#   pulse 7: activated nodes: 8924, borderline nodes: 3612, overall activation: 87.704, activation diff: 755.061, ratio: 8.609
#   pulse 8: activated nodes: 8924, borderline nodes: 3612, overall activation: 681.351, activation diff: 761.659, ratio: 1.118
#   pulse 9: activated nodes: 8929, borderline nodes: 3615, overall activation: 88.195, activation diff: 762.049, ratio: 8.641
#   pulse 10: activated nodes: 8929, borderline nodes: 3615, overall activation: 682.006, activation diff: 762.366, ratio: 1.118
#   pulse 11: activated nodes: 8929, borderline nodes: 3615, overall activation: 88.267, activation diff: 762.376, ratio: 8.637
#   pulse 12: activated nodes: 8929, borderline nodes: 3615, overall activation: 682.118, activation diff: 762.352, ratio: 1.118
#   pulse 13: activated nodes: 8929, borderline nodes: 3615, overall activation: 88.288, activation diff: 762.340, ratio: 8.635
#   pulse 14: activated nodes: 8929, borderline nodes: 3615, overall activation: 682.155, activation diff: 762.312, ratio: 1.118
#   pulse 15: activated nodes: 8929, borderline nodes: 3615, overall activation: 88.298, activation diff: 762.303, ratio: 8.633

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8929
#   final overall activation: 88.3
#   number of spread. activ. pulses: 15
#   running time: 495

###################################
# top k results in TREC format: 

1   Q1   englishcyclopae05kniggoog_118   1   0.02081173   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.019179871   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   3   0.016215494   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_280   4   0.015016551   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_177   5   0.013237673   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   6   0.013017694   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_197   7   0.011716122   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.011485439   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_240   9   0.009280798   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_195   10   0.003737318   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_195   11   0.003737318   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_138   12   0.0029871236   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_257   13   0.0029871236   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   14   0.0025698964   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_156   15   6.5861753E-4   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_222   16   6.5861753E-4   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_200   17   0.0   REFERENCES:SIMDATES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_192   19   0.0   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_197   20   0.0   REFERENCES:SIMDATES
