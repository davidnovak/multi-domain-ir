###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 44.729, activation diff: 37.264, ratio: 0.833
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 100.004, activation diff: 56.474, ratio: 0.565
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 215.167, activation diff: 115.723, ratio: 0.538
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 385.015, activation diff: 170.025, ratio: 0.442
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 600.470, activation diff: 215.474, ratio: 0.359
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 844.011, activation diff: 243.540, ratio: 0.289
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 1096.815, activation diff: 252.805, ratio: 0.230
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 1343.882, activation diff: 247.067, ratio: 0.184
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 1575.339, activation diff: 231.457, ratio: 0.147
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 1785.698, activation diff: 210.358, ratio: 0.118
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 1972.662, activation diff: 186.964, ratio: 0.095
#   pulse 13: activated nodes: 11328, borderline nodes: 416, overall activation: 2136.056, activation diff: 163.395, ratio: 0.076
#   pulse 14: activated nodes: 11328, borderline nodes: 416, overall activation: 2277.009, activation diff: 140.952, ratio: 0.062
#   pulse 15: activated nodes: 11328, borderline nodes: 416, overall activation: 2397.365, activation diff: 120.356, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 2397.4
#   number of spread. activ. pulses: 15
#   running time: 545

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.92473346   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.79452854   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.77741575   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.705298   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.6970222   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.6765948   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.66290426   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.66283816   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.6577482   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.65755475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.655336   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.6521406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.64934456   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.64909905   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.6483896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.64789623   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.6464617   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   18   0.6437713   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.6427487   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   20   0.64136326   REFERENCES:SIMDATES
