###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 173.892, activation diff: 162.732, ratio: 0.936
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 367.964, activation diff: 194.757, ratio: 0.529
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1077.775, activation diff: 709.835, ratio: 0.659
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1951.545, activation diff: 873.770, ratio: 0.448
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2800.409, activation diff: 848.864, ratio: 0.303
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 3481.640, activation diff: 681.231, ratio: 0.196
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 3976.886, activation diff: 495.247, ratio: 0.125
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 4317.507, activation diff: 340.620, ratio: 0.079
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 4544.339, activation diff: 226.833, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 4544.3
#   number of spread. activ. pulses: 10
#   running time: 438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96158504   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.888842   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.88376415   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8819196   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.87979263   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.87950426   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   7   0.8791472   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.8785208   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.8784032   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   10   0.87801266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   11   0.87793887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8777071   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.8776786   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.87765527   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   15   0.8767835   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.87609804   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   17   0.87538147   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.8747328   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.8742079   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8741571   REFERENCES:SIMDATES
