###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 333.849, activation diff: 350.299, ratio: 1.049
#   pulse 3: activated nodes: 8045, borderline nodes: 4151, overall activation: 210.237, activation diff: 544.019, ratio: 2.588
#   pulse 4: activated nodes: 10103, borderline nodes: 5111, overall activation: 1882.196, activation diff: 2076.316, ratio: 1.103
#   pulse 5: activated nodes: 11083, borderline nodes: 3040, overall activation: 716.013, activation diff: 2573.251, ratio: 3.594
#   pulse 6: activated nodes: 11242, borderline nodes: 1171, overall activation: 2393.783, activation diff: 3019.950, ratio: 1.262
#   pulse 7: activated nodes: 11285, borderline nodes: 839, overall activation: 1084.080, activation diff: 2903.070, ratio: 2.678
#   pulse 8: activated nodes: 11298, borderline nodes: 625, overall activation: 2670.851, activation diff: 2709.911, ratio: 1.015
#   pulse 9: activated nodes: 11305, borderline nodes: 599, overall activation: 2824.015, activation diff: 1000.263, ratio: 0.354
#   pulse 10: activated nodes: 11305, borderline nodes: 577, overall activation: 3162.538, activation diff: 519.269, ratio: 0.164
#   pulse 11: activated nodes: 11305, borderline nodes: 550, overall activation: 3228.798, activation diff: 130.672, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 3228.8
#   number of spread. activ. pulses: 11
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.969329   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8623657   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8387177   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7948397   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.78730947   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.78512615   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.78335935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.781661   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7804821   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7803982   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   11   0.78017765   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   12   0.7787643   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.77875143   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.77795047   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.777799   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.7771489   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.7768123   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   18   0.77452105   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.7743961   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.7730202   REFERENCES:SIMDATES
