###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 13.985, activation diff: 7.415, ratio: 0.530
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 30.643, activation diff: 18.024, ratio: 0.588
#   pulse 4: activated nodes: 7574, borderline nodes: 5369, overall activation: 69.805, activation diff: 40.025, ratio: 0.573
#   pulse 5: activated nodes: 8840, borderline nodes: 5723, overall activation: 151.862, activation diff: 82.497, ratio: 0.543
#   pulse 6: activated nodes: 9751, borderline nodes: 5284, overall activation: 293.150, activation diff: 141.444, ratio: 0.482
#   pulse 7: activated nodes: 10467, borderline nodes: 4609, overall activation: 505.889, activation diff: 212.755, ratio: 0.421
#   pulse 8: activated nodes: 10816, borderline nodes: 3831, overall activation: 790.375, activation diff: 284.487, ratio: 0.360
#   pulse 9: activated nodes: 11039, borderline nodes: 3065, overall activation: 1129.163, activation diff: 338.788, ratio: 0.300
#   pulse 10: activated nodes: 11149, borderline nodes: 2478, overall activation: 1495.317, activation diff: 366.154, ratio: 0.245
#   pulse 11: activated nodes: 11210, borderline nodes: 1895, overall activation: 1864.833, activation diff: 369.516, ratio: 0.198
#   pulse 12: activated nodes: 11223, borderline nodes: 1436, overall activation: 2219.933, activation diff: 355.101, ratio: 0.160
#   pulse 13: activated nodes: 11239, borderline nodes: 1117, overall activation: 2549.357, activation diff: 329.424, ratio: 0.129
#   pulse 14: activated nodes: 11274, borderline nodes: 912, overall activation: 2847.072, activation diff: 297.714, ratio: 0.105
#   pulse 15: activated nodes: 11290, borderline nodes: 760, overall activation: 3110.910, activation diff: 263.839, ratio: 0.085

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 3110.9
#   number of spread. activ. pulses: 15
#   running time: 577

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89536387   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8218559   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   3   0.80149305   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   4   0.7938372   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   5   0.7870716   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.78362435   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   7   0.7833131   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   8   0.7798801   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   9   0.77900934   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   10   0.7784761   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   11   0.7780272   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.7779443   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.7776986   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   14   0.7768704   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   15   0.773877   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   16   0.77287126   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   17   0.77166367   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.77120996   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.768775   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   20   0.7677757   REFERENCES:SIMDATES
