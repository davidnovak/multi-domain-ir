###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 58.307, activation diff: 50.200, ratio: 0.861
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 103.008, activation diff: 45.943, ratio: 0.446
#   pulse 4: activated nodes: 9109, borderline nodes: 6067, overall activation: 347.995, activation diff: 245.152, ratio: 0.704
#   pulse 5: activated nodes: 10235, borderline nodes: 4636, overall activation: 681.685, activation diff: 333.690, ratio: 0.490
#   pulse 6: activated nodes: 10770, borderline nodes: 3854, overall activation: 1109.581, activation diff: 427.895, ratio: 0.386
#   pulse 7: activated nodes: 11083, borderline nodes: 2733, overall activation: 1524.853, activation diff: 415.272, ratio: 0.272
#   pulse 8: activated nodes: 11175, borderline nodes: 1993, overall activation: 1871.151, activation diff: 346.298, ratio: 0.185
#   pulse 9: activated nodes: 11223, borderline nodes: 1416, overall activation: 2133.725, activation diff: 262.574, ratio: 0.123
#   pulse 10: activated nodes: 11228, borderline nodes: 1070, overall activation: 2321.619, activation diff: 187.894, ratio: 0.081
#   pulse 11: activated nodes: 11258, borderline nodes: 946, overall activation: 2451.220, activation diff: 129.601, ratio: 0.053
#   pulse 12: activated nodes: 11272, borderline nodes: 856, overall activation: 2538.540, activation diff: 87.320, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11272
#   final overall activation: 2538.5
#   number of spread. activ. pulses: 12
#   running time: 494

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9627862   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8464109   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.82236475   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7521076   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.73814714   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.72463596   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7179691   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.71570456   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.71215165   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7112479   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.70805156   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.70607936   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.7045021   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7044507   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   15   0.70435953   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.7028719   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.7024993   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.7024269   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.7017486   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.69983643   REFERENCES:SIMDATES
