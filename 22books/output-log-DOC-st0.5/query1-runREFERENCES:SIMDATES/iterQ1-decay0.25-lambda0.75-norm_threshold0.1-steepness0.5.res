###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 23.400, activation diff: 16.380, ratio: 0.700
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 52.164, activation diff: 30.046, ratio: 0.576
#   pulse 4: activated nodes: 9397, borderline nodes: 5562, overall activation: 118.654, activation diff: 67.192, ratio: 0.566
#   pulse 5: activated nodes: 10232, borderline nodes: 5022, overall activation: 229.563, activation diff: 111.208, ratio: 0.484
#   pulse 6: activated nodes: 10689, borderline nodes: 4308, overall activation: 385.715, activation diff: 156.202, ratio: 0.405
#   pulse 7: activated nodes: 10939, borderline nodes: 3441, overall activation: 580.466, activation diff: 194.751, ratio: 0.336
#   pulse 8: activated nodes: 11104, borderline nodes: 2655, overall activation: 800.598, activation diff: 220.132, ratio: 0.275
#   pulse 9: activated nodes: 11184, borderline nodes: 2086, overall activation: 1030.639, activation diff: 230.042, ratio: 0.223
#   pulse 10: activated nodes: 11217, borderline nodes: 1503, overall activation: 1257.484, activation diff: 226.845, ratio: 0.180
#   pulse 11: activated nodes: 11233, borderline nodes: 1120, overall activation: 1471.961, activation diff: 214.477, ratio: 0.146
#   pulse 12: activated nodes: 11268, borderline nodes: 929, overall activation: 1668.598, activation diff: 196.637, ratio: 0.118
#   pulse 13: activated nodes: 11273, borderline nodes: 782, overall activation: 1844.808, activation diff: 176.210, ratio: 0.096
#   pulse 14: activated nodes: 11282, borderline nodes: 689, overall activation: 1999.980, activation diff: 155.172, ratio: 0.078
#   pulse 15: activated nodes: 11287, borderline nodes: 651, overall activation: 2134.784, activation diff: 134.804, ratio: 0.063

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11287
#   final overall activation: 2134.8
#   number of spread. activ. pulses: 15
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.910875   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.75807416   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.74883056   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.68503964   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   5   0.67745274   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.65990573   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   7   0.6509793   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   8   0.6374189   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.6347656   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.63347644   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.6312454   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.6292511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.62863684   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.6281102   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.6269838   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.6228471   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   17   0.62251496   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.62155735   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.6214701   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   20   0.6204078   REFERENCES:SIMDATES
