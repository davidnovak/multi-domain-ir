###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 225.562, activation diff: 225.041, ratio: 0.998
#   pulse 3: activated nodes: 8357, borderline nodes: 3642, overall activation: 259.531, activation diff: 189.051, ratio: 0.728
#   pulse 4: activated nodes: 10445, borderline nodes: 4412, overall activation: 1113.334, activation diff: 872.455, ratio: 0.784
#   pulse 5: activated nodes: 11119, borderline nodes: 2859, overall activation: 1696.537, activation diff: 583.491, ratio: 0.344
#   pulse 6: activated nodes: 11228, borderline nodes: 1205, overall activation: 2237.110, activation diff: 540.573, ratio: 0.242
#   pulse 7: activated nodes: 11278, borderline nodes: 748, overall activation: 2545.626, activation diff: 308.516, ratio: 0.121
#   pulse 8: activated nodes: 11293, borderline nodes: 633, overall activation: 2700.897, activation diff: 155.271, ratio: 0.057
#   pulse 9: activated nodes: 11301, borderline nodes: 595, overall activation: 2774.634, activation diff: 73.738, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11301
#   final overall activation: 2774.6
#   number of spread. activ. pulses: 9
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9699013   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8659643   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.84412956   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.77725565   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7427492   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7315837   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.72835153   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7265102   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.72240263   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7220541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7188146   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.7180906   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.71705   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   14   0.71622443   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.71565974   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.71528935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.71329355   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.71273804   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.7126368   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   20   0.71249026   REFERENCES:SIMDATES
