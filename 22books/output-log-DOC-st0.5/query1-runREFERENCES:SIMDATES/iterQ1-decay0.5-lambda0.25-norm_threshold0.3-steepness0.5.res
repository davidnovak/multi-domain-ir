###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 71.993, activation diff: 74.533, ratio: 1.035
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 47.766, activation diff: 63.868, ratio: 1.337
#   pulse 4: activated nodes: 7604, borderline nodes: 4920, overall activation: 298.162, activation diff: 265.139, ratio: 0.889
#   pulse 5: activated nodes: 8521, borderline nodes: 3904, overall activation: 399.782, activation diff: 114.309, ratio: 0.286
#   pulse 6: activated nodes: 8572, borderline nodes: 3911, overall activation: 571.338, activation diff: 171.556, ratio: 0.300
#   pulse 7: activated nodes: 8846, borderline nodes: 3672, overall activation: 667.779, activation diff: 96.442, ratio: 0.144
#   pulse 8: activated nodes: 8904, borderline nodes: 3645, overall activation: 723.198, activation diff: 55.419, ratio: 0.077
#   pulse 9: activated nodes: 8921, borderline nodes: 3612, overall activation: 748.799, activation diff: 25.602, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8921
#   final overall activation: 748.8
#   number of spread. activ. pulses: 9
#   running time: 416

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.955134   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.82257164   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.79055005   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7126427   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.61392784   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.538073   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4611978   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.44033194   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.42367703   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.41323555   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.40667328   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.39208835   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.3857287   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.38416472   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.3784062   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.37672317   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.364284   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.3640902   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.36300313   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   20   0.3625344   REFERENCES:SIMDATES
