###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 19.023, activation diff: 12.003, ratio: 0.631
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 38.007, activation diff: 20.265, ratio: 0.533
#   pulse 4: activated nodes: 8095, borderline nodes: 4411, overall activation: 74.563, activation diff: 37.293, ratio: 0.500
#   pulse 5: activated nodes: 8519, borderline nodes: 3977, overall activation: 129.023, activation diff: 54.803, ratio: 0.425
#   pulse 6: activated nodes: 8723, borderline nodes: 3777, overall activation: 196.455, activation diff: 67.529, ratio: 0.344
#   pulse 7: activated nodes: 8832, borderline nodes: 3692, overall activation: 270.829, activation diff: 74.383, ratio: 0.275
#   pulse 8: activated nodes: 8910, borderline nodes: 3645, overall activation: 346.940, activation diff: 76.111, ratio: 0.219
#   pulse 9: activated nodes: 8914, borderline nodes: 3619, overall activation: 420.854, activation diff: 73.915, ratio: 0.176
#   pulse 10: activated nodes: 8928, borderline nodes: 3613, overall activation: 489.874, activation diff: 69.020, ratio: 0.141
#   pulse 11: activated nodes: 8929, borderline nodes: 3613, overall activation: 552.395, activation diff: 62.520, ratio: 0.113
#   pulse 12: activated nodes: 8929, borderline nodes: 3613, overall activation: 607.685, activation diff: 55.290, ratio: 0.091
#   pulse 13: activated nodes: 8929, borderline nodes: 3611, overall activation: 655.655, activation diff: 47.970, ratio: 0.073
#   pulse 14: activated nodes: 8929, borderline nodes: 3611, overall activation: 696.641, activation diff: 40.987, ratio: 0.059
#   pulse 15: activated nodes: 8935, borderline nodes: 3614, overall activation: 731.229, activation diff: 34.588, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 731.2
#   number of spread. activ. pulses: 15
#   running time: 488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89446074   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.71649426   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.70885164   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.6453026   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.57718366   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.4783318   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.40693176   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.38194323   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   9   0.3807613   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.37620613   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.36509368   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.36342382   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.35094112   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.34782204   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.33621365   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   16   0.33592176   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.33530897   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.335207   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   19   0.3252563   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   20   0.3242777   REFERENCES:SIMDATES
