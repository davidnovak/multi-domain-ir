###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 94.717, activation diff: 84.971, ratio: 0.897
#   pulse 3: activated nodes: 8123, borderline nodes: 3942, overall activation: 176.160, activation diff: 82.366, ratio: 0.468
#   pulse 4: activated nodes: 10100, borderline nodes: 5170, overall activation: 522.613, activation diff: 346.525, ratio: 0.663
#   pulse 5: activated nodes: 10933, borderline nodes: 3556, overall activation: 959.211, activation diff: 436.598, ratio: 0.455
#   pulse 6: activated nodes: 11142, borderline nodes: 2274, overall activation: 1432.747, activation diff: 473.536, ratio: 0.331
#   pulse 7: activated nodes: 11227, borderline nodes: 1331, overall activation: 1842.985, activation diff: 410.238, ratio: 0.223
#   pulse 8: activated nodes: 11267, borderline nodes: 870, overall activation: 2158.371, activation diff: 315.387, ratio: 0.146
#   pulse 9: activated nodes: 11281, borderline nodes: 700, overall activation: 2384.401, activation diff: 226.029, ratio: 0.095
#   pulse 10: activated nodes: 11294, borderline nodes: 650, overall activation: 2539.588, activation diff: 155.188, ratio: 0.061
#   pulse 11: activated nodes: 11299, borderline nodes: 623, overall activation: 2643.282, activation diff: 103.694, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11299
#   final overall activation: 2643.3
#   number of spread. activ. pulses: 11
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9623543   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.85047585   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8280784   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7587581   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7367996   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7234366   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.71691704   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.71446216   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7112848   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7104076   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7072025   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.7052965   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.70440006   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.7035953   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   15   0.70349455   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   16   0.70202583   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   17   0.7018611   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.70175344   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.7012537   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.69909394   REFERENCES:SIMDATES
