###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 313.129, activation diff: 329.579, ratio: 1.053
#   pulse 3: activated nodes: 8045, borderline nodes: 4151, overall activation: 178.528, activation diff: 491.590, ratio: 2.754
#   pulse 4: activated nodes: 10100, borderline nodes: 5148, overall activation: 1610.946, activation diff: 1777.447, ratio: 1.103
#   pulse 5: activated nodes: 11067, borderline nodes: 3129, overall activation: 562.405, activation diff: 2157.790, ratio: 3.837
#   pulse 6: activated nodes: 11191, borderline nodes: 1407, overall activation: 2008.382, activation diff: 2523.021, ratio: 1.256
#   pulse 7: activated nodes: 11237, borderline nodes: 1043, overall activation: 696.816, activation diff: 2579.490, ratio: 3.702
#   pulse 8: activated nodes: 11276, borderline nodes: 837, overall activation: 2129.580, activation diff: 2525.139, ratio: 1.186
#   pulse 9: activated nodes: 11286, borderline nodes: 817, overall activation: 1863.373, activation diff: 1379.459, ratio: 0.740
#   pulse 10: activated nodes: 11286, borderline nodes: 806, overall activation: 2522.463, activation diff: 993.301, ratio: 0.394
#   pulse 11: activated nodes: 11286, borderline nodes: 800, overall activation: 2624.480, activation diff: 242.644, ratio: 0.092
#   pulse 12: activated nodes: 11286, borderline nodes: 800, overall activation: 2680.308, activation diff: 88.205, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11286
#   final overall activation: 2680.3
#   number of spread. activ. pulses: 12
#   running time: 597

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96964085   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.86236626   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.84155196   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.77327293   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.74305   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.73171604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7292509   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.72764796   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7229568   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7227781   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7193862   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.71928823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.71788204   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.7177979   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.7173733   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.716504   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.7142001   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.7137207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   19   0.7132654   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.7127633   REFERENCES:SIMDATES
