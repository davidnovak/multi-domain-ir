###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 498.986, activation diff: 516.605, ratio: 1.035
#   pulse 3: activated nodes: 8496, borderline nodes: 3466, overall activation: 383.650, activation diff: 882.184, ratio: 2.299
#   pulse 4: activated nodes: 10643, borderline nodes: 3609, overall activation: 2822.275, activation diff: 3163.248, ratio: 1.121
#   pulse 5: activated nodes: 11209, borderline nodes: 1956, overall activation: 1262.043, activation diff: 3902.887, ratio: 3.093
#   pulse 6: activated nodes: 11292, borderline nodes: 324, overall activation: 3559.366, activation diff: 4220.020, ratio: 1.186
#   pulse 7: activated nodes: 11330, borderline nodes: 176, overall activation: 3574.028, activation diff: 2203.700, ratio: 0.617
#   pulse 8: activated nodes: 11335, borderline nodes: 87, overall activation: 4482.005, activation diff: 1374.893, ratio: 0.307
#   pulse 9: activated nodes: 11336, borderline nodes: 76, overall activation: 4664.513, activation diff: 340.495, ratio: 0.073
#   pulse 10: activated nodes: 11336, borderline nodes: 72, overall activation: 4733.735, activation diff: 108.450, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11336
#   final overall activation: 4733.7
#   number of spread. activ. pulses: 10
#   running time: 675

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97181433   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8974775   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8959113   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.89486086   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.8936014   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.8929072   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.89237225   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   8   0.8922443   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   9   0.8917079   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.89156616   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.8914123   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.89125526   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.8911678   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.89104116   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   15   0.89097625   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.89054227   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8901692   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.8900946   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.88903713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8889768   REFERENCES:SIMDATES
