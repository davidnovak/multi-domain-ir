###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 371.411, activation diff: 368.995, ratio: 0.993
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 571.859, activation diff: 336.407, ratio: 0.588
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2132.738, activation diff: 1561.031, ratio: 0.732
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 3304.869, activation diff: 1172.132, ratio: 0.355
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 4134.320, activation diff: 829.450, ratio: 0.201
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4569.733, activation diff: 435.413, ratio: 0.095
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 4777.963, activation diff: 208.230, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 4778.0
#   number of spread. activ. pulses: 8
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97096854   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8965306   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.89445573   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8933034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89187837   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   6   0.8904794   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.89025205   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   8   0.8902317   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   9   0.8901974   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.8898963   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.88974595   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.88955885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   13   0.88945675   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   14   0.8894164   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.88884765   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.88870376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   17   0.8886235   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.8879467   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.88731545   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.88685244   REFERENCES:SIMDATES
