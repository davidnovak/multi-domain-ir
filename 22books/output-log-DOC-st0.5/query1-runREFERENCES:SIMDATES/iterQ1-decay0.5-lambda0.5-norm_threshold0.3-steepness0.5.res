###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 25.258, activation diff: 18.850, ratio: 0.746
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 38.500, activation diff: 15.443, ratio: 0.401
#   pulse 4: activated nodes: 6767, borderline nodes: 4970, overall activation: 119.400, activation diff: 81.786, ratio: 0.685
#   pulse 5: activated nodes: 7901, borderline nodes: 4365, overall activation: 222.517, activation diff: 103.190, ratio: 0.464
#   pulse 6: activated nodes: 8319, borderline nodes: 4191, overall activation: 342.289, activation diff: 119.772, ratio: 0.350
#   pulse 7: activated nodes: 8571, borderline nodes: 3909, overall activation: 453.406, activation diff: 111.117, ratio: 0.245
#   pulse 8: activated nodes: 8748, borderline nodes: 3767, overall activation: 546.427, activation diff: 93.020, ratio: 0.170
#   pulse 9: activated nodes: 8849, borderline nodes: 3678, overall activation: 618.286, activation diff: 71.859, ratio: 0.116
#   pulse 10: activated nodes: 8903, borderline nodes: 3644, overall activation: 670.151, activation diff: 51.865, ratio: 0.077
#   pulse 11: activated nodes: 8916, borderline nodes: 3627, overall activation: 705.633, activation diff: 35.482, ratio: 0.050
#   pulse 12: activated nodes: 8922, borderline nodes: 3613, overall activation: 728.924, activation diff: 23.291, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8922
#   final overall activation: 728.9
#   number of spread. activ. pulses: 12
#   running time: 447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.94619507   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.7885541   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7725066   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.6968229   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6037874   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.51539916   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4523106   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.42928505   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4176988   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.40854347   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4004555   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.38892388   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.38082725   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.37441337   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.36696613   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.3655177   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.36087397   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.36024415   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.35933453   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.3578174   REFERENCES:SIMDATES
