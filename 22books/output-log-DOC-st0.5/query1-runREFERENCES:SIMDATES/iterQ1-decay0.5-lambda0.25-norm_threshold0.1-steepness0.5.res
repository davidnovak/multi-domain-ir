###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 153.073, activation diff: 152.552, ratio: 0.997
#   pulse 3: activated nodes: 8357, borderline nodes: 3642, overall activation: 134.297, activation diff: 85.080, ratio: 0.634
#   pulse 4: activated nodes: 8622, borderline nodes: 3844, overall activation: 485.682, activation diff: 354.449, ratio: 0.730
#   pulse 5: activated nodes: 8924, borderline nodes: 3608, overall activation: 663.048, activation diff: 177.717, ratio: 0.268
#   pulse 6: activated nodes: 8924, borderline nodes: 3608, overall activation: 790.121, activation diff: 127.072, ratio: 0.161
#   pulse 7: activated nodes: 8929, borderline nodes: 3611, overall activation: 847.793, activation diff: 57.673, ratio: 0.068
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 871.523, activation diff: 23.730, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 871.5
#   number of spread. activ. pulses: 8
#   running time: 396

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9614126   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8500302   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8170854   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.74181175   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6472787   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5887729   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.46611732   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.44795322   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.43108273   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4212963   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.41519362   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.4010038   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.40009826   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.3951429   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.38937947   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.38817412   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   17   0.37803876   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_322   18   0.3768775   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   19   0.3752309   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   20   0.3751329   REFERENCES:SIMDATES
