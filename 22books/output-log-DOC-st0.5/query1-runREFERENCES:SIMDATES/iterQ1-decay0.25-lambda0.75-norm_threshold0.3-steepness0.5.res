###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 9.325, activation diff: 3.208, ratio: 0.344
#   pulse 3: activated nodes: 5588, borderline nodes: 5267, overall activation: 14.377, activation diff: 6.505, ratio: 0.452
#   pulse 4: activated nodes: 6310, borderline nodes: 5146, overall activation: 23.741, activation diff: 10.583, ratio: 0.446
#   pulse 5: activated nodes: 7041, borderline nodes: 5488, overall activation: 47.030, activation diff: 24.216, ratio: 0.515
#   pulse 6: activated nodes: 7734, borderline nodes: 5395, overall activation: 91.281, activation diff: 44.759, ratio: 0.490
#   pulse 7: activated nodes: 8840, borderline nodes: 5662, overall activation: 162.319, activation diff: 71.200, ratio: 0.439
#   pulse 8: activated nodes: 9589, borderline nodes: 5441, overall activation: 263.248, activation diff: 100.986, ratio: 0.384
#   pulse 9: activated nodes: 10064, borderline nodes: 5065, overall activation: 394.476, activation diff: 131.231, ratio: 0.333
#   pulse 10: activated nodes: 10493, borderline nodes: 4613, overall activation: 553.280, activation diff: 158.804, ratio: 0.287
#   pulse 11: activated nodes: 10724, borderline nodes: 4026, overall activation: 732.844, activation diff: 179.565, ratio: 0.245
#   pulse 12: activated nodes: 10877, borderline nodes: 3489, overall activation: 923.087, activation diff: 190.243, ratio: 0.206
#   pulse 13: activated nodes: 10997, borderline nodes: 3063, overall activation: 1113.903, activation diff: 190.816, ratio: 0.171
#   pulse 14: activated nodes: 11066, borderline nodes: 2700, overall activation: 1297.382, activation diff: 183.478, ratio: 0.141
#   pulse 15: activated nodes: 11106, borderline nodes: 2424, overall activation: 1468.350, activation diff: 170.968, ratio: 0.116

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11106
#   final overall activation: 1468.3
#   number of spread. activ. pulses: 15
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8419945   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.63518536   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   3   0.620839   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   4   0.5954649   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   5   0.5867861   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.5747808   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   7   0.5741238   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   8   0.56295943   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.5610819   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   10   0.5429193   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   11   0.5382189   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.5371046   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.53375125   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   14   0.53286713   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   15   0.5325424   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   16   0.5318943   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   17   0.53045255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.53016365   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.5298561   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   20   0.52799964   REFERENCES:SIMDATES
