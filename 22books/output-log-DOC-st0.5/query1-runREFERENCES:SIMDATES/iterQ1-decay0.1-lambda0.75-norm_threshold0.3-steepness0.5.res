###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 9.412, activation diff: 3.295, ratio: 0.350
#   pulse 3: activated nodes: 5588, borderline nodes: 5267, overall activation: 15.533, activation diff: 7.574, ratio: 0.488
#   pulse 4: activated nodes: 6310, borderline nodes: 5146, overall activation: 27.265, activation diff: 12.933, ratio: 0.474
#   pulse 5: activated nodes: 7149, borderline nodes: 5582, overall activation: 58.410, activation diff: 32.023, ratio: 0.548
#   pulse 6: activated nodes: 8089, borderline nodes: 5630, overall activation: 119.662, activation diff: 61.694, ratio: 0.516
#   pulse 7: activated nodes: 9159, borderline nodes: 5696, overall activation: 224.128, activation diff: 104.604, ratio: 0.467
#   pulse 8: activated nodes: 9782, borderline nodes: 5162, overall activation: 383.993, activation diff: 159.894, ratio: 0.416
#   pulse 9: activated nodes: 10396, borderline nodes: 4617, overall activation: 606.977, activation diff: 222.984, ratio: 0.367
#   pulse 10: activated nodes: 10765, borderline nodes: 3951, overall activation: 889.528, activation diff: 282.551, ratio: 0.318
#   pulse 11: activated nodes: 10954, borderline nodes: 3254, overall activation: 1213.666, activation diff: 324.138, ratio: 0.267
#   pulse 12: activated nodes: 11096, borderline nodes: 2743, overall activation: 1556.060, activation diff: 342.393, ratio: 0.220
#   pulse 13: activated nodes: 11159, borderline nodes: 2317, overall activation: 1896.821, activation diff: 340.761, ratio: 0.180
#   pulse 14: activated nodes: 11202, borderline nodes: 1902, overall activation: 2221.874, activation diff: 325.054, ratio: 0.146
#   pulse 15: activated nodes: 11218, borderline nodes: 1562, overall activation: 2522.388, activation diff: 300.514, ratio: 0.119

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11218
#   final overall activation: 2522.4
#   number of spread. activ. pulses: 15
#   running time: 576

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8557513   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.7838334   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   3   0.7672853   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   4   0.7424992   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   5   0.73348117   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   6   0.7283566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   7   0.72610605   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   8   0.72488767   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   9   0.7214119   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   10   0.7171016   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   11   0.7169206   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7166018   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   13   0.7154431   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.7150571   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_14   15   0.71453226   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.71381515   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   17   0.7119377   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_353   18   0.71154106   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.71048474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   20   0.7097894   REFERENCES:SIMDATES
