###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 102.062, activation diff: 90.903, ratio: 0.891
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 170.036, activation diff: 68.665, ratio: 0.404
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 349.753, activation diff: 179.749, ratio: 0.514
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 525.504, activation diff: 175.751, ratio: 0.334
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 667.798, activation diff: 142.295, ratio: 0.213
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 769.676, activation diff: 101.878, ratio: 0.132
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 837.334, activation diff: 67.658, ratio: 0.081
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 880.141, activation diff: 42.807, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 880.1
#   number of spread. activ. pulses: 9
#   running time: 405

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9416491   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8147633   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.7919751   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.71651036   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6340381   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.56288934   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.44887978   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.42891353   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4182591   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.41007888   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.40242583   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.39317656   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.38496307   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.3791604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   15   0.37377787   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.3716973   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.36767957   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.3674998   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.3663129   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   20   0.36504325   REFERENCES:SIMDATES
