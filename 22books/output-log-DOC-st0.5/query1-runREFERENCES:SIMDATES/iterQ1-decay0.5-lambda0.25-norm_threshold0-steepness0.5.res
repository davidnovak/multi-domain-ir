###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 210.658, activation diff: 208.242, ratio: 0.989
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 211.012, activation diff: 76.906, ratio: 0.364
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 580.901, activation diff: 370.126, ratio: 0.637
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 771.954, activation diff: 191.058, ratio: 0.247
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 875.341, activation diff: 103.387, ratio: 0.118
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 918.274, activation diff: 42.932, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 918.3
#   number of spread. activ. pulses: 7
#   running time: 379

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9585581   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8473251   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8180541   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7428173   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6543192   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.592005   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.46329847   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.4453694   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.43085817   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.42183864   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.41540688   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.4032554   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.39663297   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   14   0.39601266   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.39002216   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   16   0.38868183   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   17   0.37821317   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   18   0.37814873   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.37685412   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   20   0.37628126   REFERENCES:SIMDATES
