###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 26.026, activation diff: 19.006, ratio: 0.730
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 60.992, activation diff: 36.248, ratio: 0.594
#   pulse 4: activated nodes: 9420, borderline nodes: 5548, overall activation: 150.320, activation diff: 90.013, ratio: 0.599
#   pulse 5: activated nodes: 10341, borderline nodes: 4767, overall activation: 311.766, activation diff: 161.725, ratio: 0.519
#   pulse 6: activated nodes: 10812, borderline nodes: 3928, overall activation: 558.478, activation diff: 246.744, ratio: 0.442
#   pulse 7: activated nodes: 11077, borderline nodes: 2943, overall activation: 883.830, activation diff: 325.352, ratio: 0.368
#   pulse 8: activated nodes: 11180, borderline nodes: 2140, overall activation: 1261.559, activation diff: 377.729, ratio: 0.299
#   pulse 9: activated nodes: 11227, borderline nodes: 1388, overall activation: 1660.556, activation diff: 398.997, ratio: 0.240
#   pulse 10: activated nodes: 11272, borderline nodes: 922, overall activation: 2055.064, activation diff: 394.508, ratio: 0.192
#   pulse 11: activated nodes: 11288, borderline nodes: 659, overall activation: 2427.628, activation diff: 372.564, ratio: 0.153
#   pulse 12: activated nodes: 11298, borderline nodes: 492, overall activation: 2768.126, activation diff: 340.498, ratio: 0.123
#   pulse 13: activated nodes: 11310, borderline nodes: 393, overall activation: 3071.960, activation diff: 303.835, ratio: 0.099
#   pulse 14: activated nodes: 11317, borderline nodes: 325, overall activation: 3338.296, activation diff: 266.336, ratio: 0.080
#   pulse 15: activated nodes: 11321, borderline nodes: 239, overall activation: 3568.651, activation diff: 230.355, ratio: 0.065

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 3568.7
#   number of spread. activ. pulses: 15
#   running time: 510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9161   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.84077597   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   3   0.82010746   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   4   0.8198401   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.81795406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   6   0.815441   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   7   0.813501   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.81302166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   9   0.81276965   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   10   0.8107053   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.8105435   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.8082191   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   13   0.8082098   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   14   0.80811226   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   15   0.80809903   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   16   0.806758   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   17   0.80671823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   18   0.804273   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.80328685   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8021361   REFERENCES:SIMDATES
