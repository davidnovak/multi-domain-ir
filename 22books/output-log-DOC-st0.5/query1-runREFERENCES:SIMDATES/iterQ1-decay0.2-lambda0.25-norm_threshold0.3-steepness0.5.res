###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 112.046, activation diff: 114.585, ratio: 1.023
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 94.181, activation diff: 121.962, ratio: 1.295
#   pulse 4: activated nodes: 9031, borderline nodes: 6024, overall activation: 726.261, activation diff: 676.206, ratio: 0.931
#   pulse 5: activated nodes: 10408, borderline nodes: 4199, overall activation: 1086.840, activation diff: 405.659, ratio: 0.373
#   pulse 6: activated nodes: 10831, borderline nodes: 3472, overall activation: 1895.998, activation diff: 809.543, ratio: 0.427
#   pulse 7: activated nodes: 11149, borderline nodes: 2517, overall activation: 2438.881, activation diff: 542.884, ratio: 0.223
#   pulse 8: activated nodes: 11208, borderline nodes: 1559, overall activation: 2769.457, activation diff: 330.576, ratio: 0.119
#   pulse 9: activated nodes: 11226, borderline nodes: 1156, overall activation: 2943.496, activation diff: 174.039, ratio: 0.059
#   pulse 10: activated nodes: 11263, borderline nodes: 1020, overall activation: 3030.302, activation diff: 86.806, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11263
#   final overall activation: 3030.3
#   number of spread. activ. pulses: 10
#   running time: 470

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96704227   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8537108   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8280956   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.79414606   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.78441083   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.78361046   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.78092635   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.7788905   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.77808475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7777842   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   11   0.7773044   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.77620983   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.77508056   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7750795   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   15   0.7748935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   16   0.7743061   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.77423847   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   18   0.77199715   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   19   0.7711849   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.769394   REFERENCES:SIMDATES
