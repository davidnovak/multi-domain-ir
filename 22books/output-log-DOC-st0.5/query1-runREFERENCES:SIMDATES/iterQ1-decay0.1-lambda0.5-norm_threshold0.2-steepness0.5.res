###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 68.006, activation diff: 59.899, ratio: 0.881
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 126.456, activation diff: 59.657, ratio: 0.472
#   pulse 4: activated nodes: 9131, borderline nodes: 6044, overall activation: 486.463, activation diff: 360.138, ratio: 0.740
#   pulse 5: activated nodes: 10493, borderline nodes: 4378, overall activation: 1020.630, activation diff: 534.167, ratio: 0.523
#   pulse 6: activated nodes: 10944, borderline nodes: 3363, overall activation: 1769.971, activation diff: 749.341, ratio: 0.423
#   pulse 7: activated nodes: 11182, borderline nodes: 2201, overall activation: 2509.047, activation diff: 739.076, ratio: 0.295
#   pulse 8: activated nodes: 11234, borderline nodes: 1224, overall activation: 3123.784, activation diff: 614.737, ratio: 0.197
#   pulse 9: activated nodes: 11282, borderline nodes: 809, overall activation: 3585.729, activation diff: 461.945, ratio: 0.129
#   pulse 10: activated nodes: 11299, borderline nodes: 588, overall activation: 3912.614, activation diff: 326.885, ratio: 0.084
#   pulse 11: activated nodes: 11308, borderline nodes: 471, overall activation: 4135.799, activation diff: 223.185, ratio: 0.054
#   pulse 12: activated nodes: 11315, borderline nodes: 395, overall activation: 4285.117, activation diff: 149.318, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 4285.1
#   number of spread. activ. pulses: 12
#   running time: 509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9645246   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.89337564   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8884333   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.88682425   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.88469684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   6   0.8845651   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   7   0.8839891   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   8   0.8837537   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   9   0.8837286   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   10   0.8835546   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.88314486   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.88298845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   13   0.88286734   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.8827874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   15   0.88242346   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.8816415   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   17   0.8808968   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.88010037   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   19   0.87980604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.87945616   REFERENCES:SIMDATES
