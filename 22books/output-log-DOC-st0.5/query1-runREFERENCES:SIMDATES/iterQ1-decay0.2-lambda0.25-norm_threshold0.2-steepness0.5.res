###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 166.995, activation diff: 168.388, ratio: 1.008
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 170.313, activation diff: 168.454, ratio: 0.989
#   pulse 4: activated nodes: 9806, borderline nodes: 5583, overall activation: 997.066, activation diff: 870.032, ratio: 0.873
#   pulse 5: activated nodes: 10912, borderline nodes: 3624, overall activation: 1536.203, activation diff: 552.008, ratio: 0.359
#   pulse 6: activated nodes: 11101, borderline nodes: 2243, overall activation: 2291.657, activation diff: 755.477, ratio: 0.330
#   pulse 7: activated nodes: 11221, borderline nodes: 1511, overall activation: 2764.071, activation diff: 472.414, ratio: 0.171
#   pulse 8: activated nodes: 11273, borderline nodes: 887, overall activation: 3019.624, activation diff: 255.553, ratio: 0.085
#   pulse 9: activated nodes: 11290, borderline nodes: 712, overall activation: 3145.911, activation diff: 126.287, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11290
#   final overall activation: 3145.9
#   number of spread. activ. pulses: 9
#   running time: 463

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9679421   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8579632   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8343273   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.79395336   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.7846898   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.78363836   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7804946   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   8   0.778447   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.77791053   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.77760816   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   11   0.77656806   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.7761356   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   13   0.77489173   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7748202   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   15   0.774298   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7742093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   17   0.77388036   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   18   0.77223265   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   19   0.77073354   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7690464   REFERENCES:SIMDATES
