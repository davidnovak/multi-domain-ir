###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 33.607, activation diff: 27.199, ratio: 0.809
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 54.950, activation diff: 23.376, ratio: 0.425
#   pulse 4: activated nodes: 7858, borderline nodes: 5988, overall activation: 210.714, activation diff: 156.450, ratio: 0.742
#   pulse 5: activated nodes: 9296, borderline nodes: 5195, overall activation: 436.742, activation diff: 226.059, ratio: 0.518
#   pulse 6: activated nodes: 10166, borderline nodes: 4969, overall activation: 778.725, activation diff: 341.983, ratio: 0.439
#   pulse 7: activated nodes: 10742, borderline nodes: 3923, overall activation: 1166.103, activation diff: 387.379, ratio: 0.332
#   pulse 8: activated nodes: 11000, borderline nodes: 3035, overall activation: 1533.634, activation diff: 367.531, ratio: 0.240
#   pulse 9: activated nodes: 11116, borderline nodes: 2410, overall activation: 1836.243, activation diff: 302.608, ratio: 0.165
#   pulse 10: activated nodes: 11174, borderline nodes: 1919, overall activation: 2065.471, activation diff: 229.229, ratio: 0.111
#   pulse 11: activated nodes: 11209, borderline nodes: 1568, overall activation: 2230.296, activation diff: 164.825, ratio: 0.074
#   pulse 12: activated nodes: 11226, borderline nodes: 1393, overall activation: 2344.939, activation diff: 114.643, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11226
#   final overall activation: 2344.9
#   number of spread. activ. pulses: 12
#   running time: 494

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9574101   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8278147   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8034923   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.73502296   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   5   0.7317712   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.71926045   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7093446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7067255   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.70378935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7022269   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   11   0.7000541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.6990685   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   13   0.69597423   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.69544184   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.69459033   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   16   0.6938511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   17   0.6936945   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   18   0.6934918   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   19   0.6913768   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   20   0.6890875   REFERENCES:SIMDATES
