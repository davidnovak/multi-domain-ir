###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 535.025, activation diff: 553.721, ratio: 1.035
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 324.773, activation diff: 848.875, ratio: 2.614
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 2019.683, activation diff: 2275.462, ratio: 1.127
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1052.127, activation diff: 2316.814, ratio: 2.202
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2514.146, activation diff: 2255.445, ratio: 0.897
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2726.057, activation diff: 691.751, ratio: 0.254
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 2933.957, activation diff: 304.496, ratio: 0.104
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 2976.831, activation diff: 72.332, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 2976.8
#   number of spread. activ. pulses: 9
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97241193   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8746413   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8551078   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.79095006   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7437069   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7334604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.731298   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.72974825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7255131   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7254192   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7223154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.72200483   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.72120625   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.72047466   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.72037005   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.71957827   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.71712303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.717052   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   19   0.7168366   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.7160307   REFERENCES:SIMDATES
