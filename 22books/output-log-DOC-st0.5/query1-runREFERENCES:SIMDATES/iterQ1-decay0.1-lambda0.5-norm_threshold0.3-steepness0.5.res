###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 38.617, activation diff: 32.208, ratio: 0.834
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 65.626, activation diff: 28.994, ratio: 0.442
#   pulse 4: activated nodes: 7963, borderline nodes: 6066, overall activation: 283.163, activation diff: 218.149, ratio: 0.770
#   pulse 5: activated nodes: 9602, borderline nodes: 5113, overall activation: 628.954, activation diff: 345.800, ratio: 0.550
#   pulse 6: activated nodes: 10446, borderline nodes: 4579, overall activation: 1227.755, activation diff: 598.801, ratio: 0.488
#   pulse 7: activated nodes: 10975, borderline nodes: 3291, overall activation: 1927.639, activation diff: 699.884, ratio: 0.363
#   pulse 8: activated nodes: 11158, borderline nodes: 2398, overall activation: 2587.447, activation diff: 659.808, ratio: 0.255
#   pulse 9: activated nodes: 11218, borderline nodes: 1593, overall activation: 3124.717, activation diff: 537.271, ratio: 0.172
#   pulse 10: activated nodes: 11235, borderline nodes: 1128, overall activation: 3526.053, activation diff: 401.336, ratio: 0.114
#   pulse 11: activated nodes: 11280, borderline nodes: 920, overall activation: 3810.340, activation diff: 284.287, ratio: 0.075
#   pulse 12: activated nodes: 11298, borderline nodes: 755, overall activation: 4005.383, activation diff: 195.043, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11298
#   final overall activation: 4005.4
#   number of spread. activ. pulses: 12
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9600423   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8912353   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.88408446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.88211423   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.88096154   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   6   0.87951905   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   7   0.8792775   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   8   0.87916297   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   9   0.8787566   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   10   0.8786303   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.8782364   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   12   0.8781557   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.87811756   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   14   0.8774508   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.8758543   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.8757164   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   17   0.8750422   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   18   0.87504125   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_575   19   0.87428105   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   20   0.87412244   REFERENCES:SIMDATES
