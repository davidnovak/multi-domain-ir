###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 269.055, activation diff: 268.534, ratio: 0.998
#   pulse 3: activated nodes: 8357, borderline nodes: 3642, overall activation: 353.923, activation diff: 270.679, ratio: 0.765
#   pulse 4: activated nodes: 10466, borderline nodes: 4287, overall activation: 1715.007, activation diff: 1391.528, ratio: 0.811
#   pulse 5: activated nodes: 11163, borderline nodes: 2584, overall activation: 2731.452, activation diff: 1017.666, ratio: 0.373
#   pulse 6: activated nodes: 11276, borderline nodes: 706, overall activation: 3687.543, activation diff: 956.091, ratio: 0.259
#   pulse 7: activated nodes: 11315, borderline nodes: 401, overall activation: 4235.591, activation diff: 548.048, ratio: 0.129
#   pulse 8: activated nodes: 11324, borderline nodes: 210, overall activation: 4509.396, activation diff: 273.806, ratio: 0.061
#   pulse 9: activated nodes: 11332, borderline nodes: 124, overall activation: 4639.216, activation diff: 129.820, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 4639.2
#   number of spread. activ. pulses: 9
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97077   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.89705837   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.89506483   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.8939103   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89248514   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.8912502   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   7   0.89113724   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.8910641   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   9   0.8906991   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.89057744   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.8902453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.8901793   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.8900684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.89003384   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.88961464   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.8893236   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.8891802   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.8886627   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.8877865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8875767   REFERENCES:SIMDATES
