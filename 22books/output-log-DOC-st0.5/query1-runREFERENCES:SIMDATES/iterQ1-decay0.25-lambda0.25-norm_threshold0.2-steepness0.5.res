###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 156.962, activation diff: 158.355, ratio: 1.009
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 152.851, activation diff: 150.806, ratio: 0.987
#   pulse 4: activated nodes: 9798, borderline nodes: 5591, overall activation: 860.721, activation diff: 744.546, ratio: 0.865
#   pulse 5: activated nodes: 10869, borderline nodes: 3736, overall activation: 1306.370, activation diff: 455.907, ratio: 0.349
#   pulse 6: activated nodes: 11050, borderline nodes: 2519, overall activation: 1918.966, activation diff: 612.610, ratio: 0.319
#   pulse 7: activated nodes: 11207, borderline nodes: 1776, overall activation: 2297.359, activation diff: 378.392, ratio: 0.165
#   pulse 8: activated nodes: 11249, borderline nodes: 1118, overall activation: 2502.485, activation diff: 205.126, ratio: 0.082
#   pulse 9: activated nodes: 11253, borderline nodes: 937, overall activation: 2604.328, activation diff: 101.843, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11253
#   final overall activation: 2604.3
#   number of spread. activ. pulses: 9
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96744794   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8577001   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8319404   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7625652   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.74189794   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.72990453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.7254657   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7235089   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.7193553   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.71879715   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.71547174   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.71427166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.7130552   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   14   0.7121698   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7119238   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   16   0.7100105   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   17   0.7098758   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   18   0.70939887   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   19   0.70855874   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   20   0.7083887   REFERENCES:SIMDATES
