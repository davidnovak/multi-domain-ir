###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 278.415, activation diff: 296.034, ratio: 1.063
#   pulse 3: activated nodes: 8496, borderline nodes: 3466, overall activation: 65.865, activation diff: 343.888, ratio: 5.221
#   pulse 4: activated nodes: 8747, borderline nodes: 3656, overall activation: 758.590, activation diff: 818.106, ratio: 1.078
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 90.182, activation diff: 838.292, ratio: 9.296
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 802.369, activation diff: 871.558, ratio: 1.086
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 109.687, activation diff: 855.202, ratio: 7.797
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 824.507, activation diff: 836.368, ratio: 1.014
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 560.803, activation diff: 385.565, ratio: 0.688
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 877.162, activation diff: 333.208, ratio: 0.380
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 873.863, activation diff: 20.658, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 873.9
#   number of spread. activ. pulses: 11
#   running time: 498

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9583303   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8519585   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8077395   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7302114   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.6394735   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.55509275   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4674826   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.45049527   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.43129796   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.42051643   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.41400045   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.40233627   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   13   0.4022918   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   14   0.39669287   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.39185625   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   16   0.38887274   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   17   0.38356912   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   18   0.37904772   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   19   0.37674627   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   20   0.37663835   REFERENCES:SIMDATES
