###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 125.397, activation diff: 127.936, ratio: 1.020
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 114.456, activation diff: 146.137, ratio: 1.277
#   pulse 4: activated nodes: 9040, borderline nodes: 6007, overall activation: 952.008, activation diff: 894.895, ratio: 0.940
#   pulse 5: activated nodes: 10511, borderline nodes: 3987, overall activation: 1460.847, activation diff: 573.580, ratio: 0.393
#   pulse 6: activated nodes: 10940, borderline nodes: 3048, overall activation: 2629.724, activation diff: 1169.452, ratio: 0.445
#   pulse 7: activated nodes: 11197, borderline nodes: 2166, overall activation: 3424.203, activation diff: 794.479, ratio: 0.232
#   pulse 8: activated nodes: 11266, borderline nodes: 1132, overall activation: 3894.408, activation diff: 470.205, ratio: 0.121
#   pulse 9: activated nodes: 11291, borderline nodes: 816, overall activation: 4136.755, activation diff: 242.348, ratio: 0.059
#   pulse 10: activated nodes: 11303, borderline nodes: 641, overall activation: 4256.250, activation diff: 119.494, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11303
#   final overall activation: 4256.2
#   number of spread. activ. pulses: 10
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.967736   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8968222   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8942666   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.89307547   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89160705   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   6   0.890175   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   7   0.89014995   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.89011633   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   9   0.889727   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   10   0.8895892   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.8893508   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.8890097   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.88899845   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.8889432   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   15   0.8883474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   16   0.88822424   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.88800865   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.887349   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.8866173   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8861026   REFERENCES:SIMDATES
