###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 155.934, activation diff: 144.775, ratio: 0.928
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 313.165, activation diff: 157.917, ratio: 0.504
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 838.605, activation diff: 525.466, ratio: 0.627
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1464.042, activation diff: 625.437, ratio: 0.427
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2063.995, activation diff: 599.953, ratio: 0.291
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 2545.374, activation diff: 481.378, ratio: 0.189
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 2896.648, activation diff: 351.274, ratio: 0.121
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 3139.256, activation diff: 242.609, ratio: 0.077
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 3301.312, activation diff: 162.055, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 3301.3
#   number of spread. activ. pulses: 10
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96064717   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.853028   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.83276594   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.78615177   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.7740475   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.77375066   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7681401   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.7670665   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.76656514   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7663591   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   11   0.76513207   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.76478577   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   13   0.7642266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   14   0.76353395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   15   0.76346064   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7627541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   17   0.76223946   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.7617231   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   19   0.7605649   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   20   0.76013434   REFERENCES:SIMDATES
