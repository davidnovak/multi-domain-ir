###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 35.277, activation diff: 28.869, ratio: 0.818
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 58.429, activation diff: 25.164, ratio: 0.431
#   pulse 4: activated nodes: 7869, borderline nodes: 5996, overall activation: 232.944, activation diff: 175.174, ratio: 0.752
#   pulse 5: activated nodes: 9424, borderline nodes: 5172, overall activation: 494.022, activation diff: 261.102, ratio: 0.529
#   pulse 6: activated nodes: 10261, borderline nodes: 4856, overall activation: 908.991, activation diff: 414.968, ratio: 0.457
#   pulse 7: activated nodes: 10843, borderline nodes: 3696, overall activation: 1387.228, activation diff: 478.238, ratio: 0.345
#   pulse 8: activated nodes: 11060, borderline nodes: 2781, overall activation: 1841.715, activation diff: 454.487, ratio: 0.247
#   pulse 9: activated nodes: 11166, borderline nodes: 2141, overall activation: 2215.755, activation diff: 374.040, ratio: 0.169
#   pulse 10: activated nodes: 11214, borderline nodes: 1585, overall activation: 2498.625, activation diff: 282.870, ratio: 0.113
#   pulse 11: activated nodes: 11226, borderline nodes: 1299, overall activation: 2701.550, activation diff: 202.925, ratio: 0.075
#   pulse 12: activated nodes: 11253, borderline nodes: 1133, overall activation: 2842.284, activation diff: 140.734, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11253
#   final overall activation: 2842.3
#   number of spread. activ. pulses: 12
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.958467   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8301996   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.80729544   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.78772485   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   5   0.7739395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   6   0.7714144   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.7658453   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   8   0.76445496   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   9   0.7632364   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   10   0.7631925   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.7621782   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   12   0.7611634   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   13   0.76069474   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   14   0.7599673   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.75987184   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.7590568   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   17   0.75892454   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.7581086   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.75483155   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_307   20   0.7547301   REFERENCES:SIMDATES
