###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 230.772, activation diff: 245.839, ratio: 1.065
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 127.177, activation diff: 357.949, ratio: 2.815
#   pulse 4: activated nodes: 9650, borderline nodes: 5904, overall activation: 1366.838, activation diff: 1490.846, ratio: 1.091
#   pulse 5: activated nodes: 10886, borderline nodes: 3680, overall activation: 481.183, activation diff: 1844.851, ratio: 3.834
#   pulse 6: activated nodes: 11074, borderline nodes: 2292, overall activation: 1882.933, activation diff: 2341.106, ratio: 1.243
#   pulse 7: activated nodes: 11207, borderline nodes: 1766, overall activation: 609.793, activation diff: 2464.393, ratio: 4.041
#   pulse 8: activated nodes: 11242, borderline nodes: 1281, overall activation: 1947.450, activation diff: 2504.465, ratio: 1.286
#   pulse 9: activated nodes: 11246, borderline nodes: 1211, overall activation: 662.857, activation diff: 2481.628, ratio: 3.744
#   pulse 10: activated nodes: 11246, borderline nodes: 1161, overall activation: 2031.793, activation diff: 2407.908, ratio: 1.185
#   pulse 11: activated nodes: 11249, borderline nodes: 1161, overall activation: 1703.525, activation diff: 1371.154, ratio: 0.805
#   pulse 12: activated nodes: 11249, borderline nodes: 1159, overall activation: 2382.142, activation diff: 1022.273, ratio: 0.429
#   pulse 13: activated nodes: 11249, borderline nodes: 1153, overall activation: 2474.780, activation diff: 258.681, ratio: 0.105
#   pulse 14: activated nodes: 11250, borderline nodes: 1146, overall activation: 2540.644, activation diff: 102.891, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11250
#   final overall activation: 2540.6
#   number of spread. activ. pulses: 14
#   running time: 591

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9679359   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8558168   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.8327329   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.76186526   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   5   0.7426595   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   6   0.7307404   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   7   0.72798824   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.7262229   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   9   0.72139823   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   10   0.7210975   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   11   0.71756506   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   12   0.71729976   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   13   0.71586406   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   14   0.7156043   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   15   0.7155423   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   16   0.7145158   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.71182704   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.7115778   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   19   0.71127635   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   20   0.71078545   REFERENCES:SIMDATES
