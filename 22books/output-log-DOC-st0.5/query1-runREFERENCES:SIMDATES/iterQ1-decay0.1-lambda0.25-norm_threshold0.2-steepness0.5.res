###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 187.062, activation diff: 188.455, ratio: 1.007
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 208.284, activation diff: 206.799, ratio: 0.993
#   pulse 4: activated nodes: 9833, borderline nodes: 5563, overall activation: 1319.617, activation diff: 1168.765, ratio: 0.886
#   pulse 5: activated nodes: 10977, borderline nodes: 3469, overall activation: 2086.529, activation diff: 786.209, ratio: 0.377
#   pulse 6: activated nodes: 11146, borderline nodes: 1753, overall activation: 3172.697, activation diff: 1086.226, ratio: 0.342
#   pulse 7: activated nodes: 11241, borderline nodes: 1117, overall activation: 3855.499, activation diff: 682.802, ratio: 0.177
#   pulse 8: activated nodes: 11295, borderline nodes: 563, overall activation: 4219.138, activation diff: 363.639, ratio: 0.086
#   pulse 9: activated nodes: 11308, borderline nodes: 434, overall activation: 4397.357, activation diff: 178.219, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11308
#   final overall activation: 4397.4
#   number of spread. activ. pulses: 9
#   running time: 452

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96863973   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   2   0.8966104   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   3   0.8938604   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   4   0.89251477   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   5   0.89084244   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   6   0.889617   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   7   0.8895203   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   8   0.8893793   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   9   0.8893498   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.88923246   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   11   0.8891539   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   12   0.8886404   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   13   0.8885846   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.88856995   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   15   0.88783395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   16   0.88776165   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   17   0.8875713   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_513   18   0.886723   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_515   19   0.88589984   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_127   20   0.8857446   REFERENCES:SIMDATES
