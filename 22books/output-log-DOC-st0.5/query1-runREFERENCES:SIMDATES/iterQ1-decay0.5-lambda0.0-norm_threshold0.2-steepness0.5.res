###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 209.528, activation diff: 225.978, ratio: 1.079
#   pulse 3: activated nodes: 8045, borderline nodes: 4151, overall activation: 57.013, activation diff: 266.476, ratio: 4.674
#   pulse 4: activated nodes: 8313, borderline nodes: 4354, overall activation: 673.930, activation diff: 728.350, ratio: 1.081
#   pulse 5: activated nodes: 8918, borderline nodes: 3623, overall activation: 86.069, activation diff: 757.357, ratio: 8.799
#   pulse 6: activated nodes: 8918, borderline nodes: 3623, overall activation: 738.379, activation diff: 815.466, ratio: 1.104
#   pulse 7: activated nodes: 8929, borderline nodes: 3611, overall activation: 89.507, activation diff: 817.550, ratio: 9.134
#   pulse 8: activated nodes: 8929, borderline nodes: 3611, overall activation: 743.098, activation diff: 819.328, ratio: 1.103
#   pulse 9: activated nodes: 8931, borderline nodes: 3611, overall activation: 91.383, activation diff: 817.878, ratio: 8.950
#   pulse 10: activated nodes: 8931, borderline nodes: 3611, overall activation: 745.571, activation diff: 815.840, ratio: 1.094
#   pulse 11: activated nodes: 8931, borderline nodes: 3611, overall activation: 97.871, activation diff: 809.406, ratio: 8.270
#   pulse 12: activated nodes: 8931, borderline nodes: 3611, overall activation: 754.108, activation diff: 800.924, ratio: 1.062
#   pulse 13: activated nodes: 8931, borderline nodes: 3611, overall activation: 260.242, activation diff: 638.607, ratio: 2.454
#   pulse 14: activated nodes: 8931, borderline nodes: 3611, overall activation: 798.147, activation diff: 594.626, ratio: 0.745
#   pulse 15: activated nodes: 8931, borderline nodes: 3611, overall activation: 761.083, activation diff: 94.139, ratio: 0.124

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8931
#   final overall activation: 761.1
#   number of spread. activ. pulses: 15
#   running time: 506

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91909945   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.73466897   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.6935538   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.62904316   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.5818817   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.44454208   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.44373155   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.43197656   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.410894   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.40237877   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   11   0.39463416   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   12   0.37908676   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.3778773   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   14   0.3689157   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   15   0.36765134   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   16   0.36615753   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.36528534   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   18   0.36061636   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.3565374   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   20   0.35635233   REFERENCES:SIMDATES
