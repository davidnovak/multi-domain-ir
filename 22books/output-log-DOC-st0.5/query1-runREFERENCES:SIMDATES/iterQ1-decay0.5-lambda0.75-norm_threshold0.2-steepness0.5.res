###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 12.029, activation diff: 5.458, ratio: 0.454
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 21.300, activation diff: 10.638, ratio: 0.499
#   pulse 4: activated nodes: 7016, borderline nodes: 4822, overall activation: 38.790, activation diff: 18.407, ratio: 0.475
#   pulse 5: activated nodes: 7523, borderline nodes: 4735, overall activation: 70.443, activation diff: 32.205, ratio: 0.457
#   pulse 6: activated nodes: 8030, borderline nodes: 4413, overall activation: 116.692, activation diff: 46.493, ratio: 0.398
#   pulse 7: activated nodes: 8390, borderline nodes: 4150, overall activation: 174.098, activation diff: 57.488, ratio: 0.330
#   pulse 8: activated nodes: 8578, borderline nodes: 3962, overall activation: 238.172, activation diff: 64.079, ratio: 0.269
#   pulse 9: activated nodes: 8690, borderline nodes: 3823, overall activation: 304.736, activation diff: 66.564, ratio: 0.218
#   pulse 10: activated nodes: 8794, borderline nodes: 3728, overall activation: 370.592, activation diff: 65.856, ratio: 0.178
#   pulse 11: activated nodes: 8859, borderline nodes: 3679, overall activation: 433.386, activation diff: 62.794, ratio: 0.145
#   pulse 12: activated nodes: 8907, borderline nodes: 3659, overall activation: 491.471, activation diff: 58.085, ratio: 0.118
#   pulse 13: activated nodes: 8911, borderline nodes: 3632, overall activation: 543.869, activation diff: 52.399, ratio: 0.096
#   pulse 14: activated nodes: 8918, borderline nodes: 3620, overall activation: 590.183, activation diff: 46.314, ratio: 0.078
#   pulse 15: activated nodes: 8924, borderline nodes: 3610, overall activation: 630.422, activation diff: 40.238, ratio: 0.064

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8924
#   final overall activation: 630.4
#   number of spread. activ. pulses: 15
#   running time: 450

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8630092   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.66212046   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.6119528   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.5976613   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.54123944   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.41458496   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.3803446   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   8   0.36278954   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.35940665   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   10   0.34998676   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.34842303   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.3447744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.3336236   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.33278552   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   15   0.3215688   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   16   0.3213793   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   17   0.3204459   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   18   0.3203649   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_153   19   0.3114679   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.3110124   REFERENCES:SIMDATES
