###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 33.469, activation diff: 26.004, ratio: 0.777
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 67.021, activation diff: 34.751, ratio: 0.519
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 124.281, activation diff: 57.862, ratio: 0.466
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 198.148, activation diff: 74.092, ratio: 0.374
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 280.884, activation diff: 82.765, ratio: 0.295
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 365.774, activation diff: 84.890, ratio: 0.232
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 447.872, activation diff: 82.098, ratio: 0.183
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 523.974, activation diff: 76.102, ratio: 0.145
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 592.326, activation diff: 68.352, ratio: 0.115
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 652.255, activation diff: 59.929, ratio: 0.092
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 703.827, activation diff: 51.572, ratio: 0.073
#   pulse 13: activated nodes: 8935, borderline nodes: 3614, overall activation: 747.558, activation diff: 43.731, ratio: 0.058
#   pulse 14: activated nodes: 8935, borderline nodes: 3614, overall activation: 784.206, activation diff: 36.648, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 784.2
#   number of spread. activ. pulses: 14
#   running time: 471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89787287   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.7392681   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.73213506   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.6584619   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.5898342   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.50479233   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.4091585   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.38572094   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.38268137   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.37653875   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.36675963   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   12   0.3633834   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.3520847   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   14   0.34825918   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_159   15   0.33723065   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_234   16   0.33712387   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.33646643   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_148   18   0.33591786   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   19   0.32930627   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_711   20   0.3268052   REFERENCES:SIMDATES
