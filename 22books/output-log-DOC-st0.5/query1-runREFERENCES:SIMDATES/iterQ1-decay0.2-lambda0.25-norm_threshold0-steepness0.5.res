###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 331.223, activation diff: 328.807, ratio: 0.993
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 468.654, activation diff: 258.615, ratio: 0.552
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1615.760, activation diff: 1147.269, ratio: 0.710
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2439.580, activation diff: 823.820, ratio: 0.338
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3020.607, activation diff: 581.027, ratio: 0.192
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3326.327, activation diff: 305.719, ratio: 0.092
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 3472.458, activation diff: 146.131, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 3472.5
#   number of spread. activ. pulses: 8
#   running time: 421

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97042036   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.8702718   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.85066366   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   4   0.7941549   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_492   5   0.7863069   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   6   0.78606117   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   7   0.7846714   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   8   0.78210896   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_343   9   0.7807949   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   10   0.7798971   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   11   0.7797292   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_124   12   0.7790452   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_522   13   0.77814907   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   14   0.7772356   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_504   15   0.7770797   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_580   16   0.7769448   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_509   17   0.77653813   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_584   18   0.7763417   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_141   19   0.77467567   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_353   20   0.7733977   REFERENCES:SIMDATES
