###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 379.279, activation diff: 395.875, ratio: 1.044
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 244.173, activation diff: 531.223, ratio: 2.176
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 1384.479, activation diff: 1292.895, ratio: 0.934
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1469.711, activation diff: 620.344, ratio: 0.422
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1998.330, activation diff: 544.426, ratio: 0.272
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 2084.234, activation diff: 93.773, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2084.2
#   number of spread. activ. pulses: 7
#   running time: 1769

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96923584   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.87222373   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8528011   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.80168515   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6717802   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6441725   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48789233   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.47077242   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.460844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.45521012   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4537114   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4524871   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.44584733   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.44564986   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.44367883   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.44211948   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.4421071   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.44079506   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.4400882   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.4389335   REFERENCES:SIMDATES:SIMLOC
