###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 107.450, activation diff: 96.291, ratio: 0.896
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 202.982, activation diff: 96.183, ratio: 0.474
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 475.168, activation diff: 272.195, ratio: 0.573
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 802.943, activation diff: 327.775, ratio: 0.408
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1130.969, activation diff: 328.025, ratio: 0.290
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1419.149, activation diff: 288.181, ratio: 0.203
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 1647.926, activation diff: 228.777, ratio: 0.139
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 1814.673, activation diff: 166.747, ratio: 0.092
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 1928.892, activation diff: 114.219, ratio: 0.059
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 2004.249, activation diff: 75.358, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2004.2
#   number of spread. activ. pulses: 11
#   running time: 1624

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9633354   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8576143   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84233826   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7856591   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6651803   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.62770027   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48338845   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46486098   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45543322   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.44913864   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.44660887   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.44569677   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.44009954   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.43710732   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.43547332   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.43388355   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.43317893   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.431803   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.4305769   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.4298178   REFERENCES:SIMDATES:SIMLOC
