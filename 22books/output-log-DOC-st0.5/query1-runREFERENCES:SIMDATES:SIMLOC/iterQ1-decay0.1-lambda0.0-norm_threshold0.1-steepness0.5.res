###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 512.169, activation diff: 528.698, ratio: 1.032
#   pulse 3: activated nodes: 9517, borderline nodes: 4002, overall activation: 460.266, activation diff: 946.541, ratio: 2.057
#   pulse 4: activated nodes: 11336, borderline nodes: 3859, overall activation: 3777.632, activation diff: 3817.373, ratio: 1.011
#   pulse 5: activated nodes: 11419, borderline nodes: 702, overall activation: 3634.186, activation diff: 3035.814, ratio: 0.835
#   pulse 6: activated nodes: 11448, borderline nodes: 139, overall activation: 6038.334, activation diff: 2609.680, ratio: 0.432
#   pulse 7: activated nodes: 11458, borderline nodes: 44, overall activation: 6309.481, activation diff: 340.630, ratio: 0.054
#   pulse 8: activated nodes: 11458, borderline nodes: 24, overall activation: 6423.818, activation diff: 116.701, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 6423.8
#   number of spread. activ. pulses: 8
#   running time: 1839

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97207546   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89791924   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89720374   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89604586   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89465743   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.894566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.89450043   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8942946   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8938382   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.89347047   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8934695   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.8934685   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.89316255   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.8929266   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.8926937   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.8922805   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.89223   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.8921973   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.89219695   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.89205515   REFERENCES:SIMDATES:SIMLOC
