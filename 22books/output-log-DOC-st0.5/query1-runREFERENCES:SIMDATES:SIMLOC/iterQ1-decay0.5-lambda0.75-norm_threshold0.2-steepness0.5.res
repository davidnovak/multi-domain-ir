###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 12.082, activation diff: 5.511, ratio: 0.456
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 21.536, activation diff: 10.817, ratio: 0.502
#   pulse 4: activated nodes: 8780, borderline nodes: 6555, overall activation: 39.721, activation diff: 19.083, ratio: 0.480
#   pulse 5: activated nodes: 9339, borderline nodes: 6461, overall activation: 74.162, activation diff: 34.961, ratio: 0.471
#   pulse 6: activated nodes: 9947, borderline nodes: 6014, overall activation: 128.451, activation diff: 54.521, ratio: 0.424
#   pulse 7: activated nodes: 10338, borderline nodes: 5486, overall activation: 202.627, activation diff: 74.248, ratio: 0.366
#   pulse 8: activated nodes: 10651, borderline nodes: 5119, overall activation: 295.545, activation diff: 92.918, ratio: 0.314
#   pulse 9: activated nodes: 10755, borderline nodes: 4739, overall activation: 405.391, activation diff: 109.846, ratio: 0.271
#   pulse 10: activated nodes: 10813, borderline nodes: 4409, overall activation: 526.712, activation diff: 121.322, ratio: 0.230
#   pulse 11: activated nodes: 10865, borderline nodes: 4130, overall activation: 653.257, activation diff: 126.545, ratio: 0.194
#   pulse 12: activated nodes: 10909, borderline nodes: 3893, overall activation: 780.542, activation diff: 127.285, ratio: 0.163
#   pulse 13: activated nodes: 10921, borderline nodes: 3759, overall activation: 905.811, activation diff: 125.270, ratio: 0.138
#   pulse 14: activated nodes: 10936, borderline nodes: 3703, overall activation: 1027.528, activation diff: 121.717, ratio: 0.118
#   pulse 15: activated nodes: 10944, borderline nodes: 3661, overall activation: 1144.509, activation diff: 116.981, ratio: 0.102

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10944
#   final overall activation: 1144.5
#   number of spread. activ. pulses: 15
#   running time: 1648

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8679446   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.68052274   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.6376796   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.6310717   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.54524285   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.447665   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4139305   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.38687673   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.38409033   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   10   0.37645772   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.37315768   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.3705151   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.36276567   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   14   0.35818744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.35673156   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.35494924   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.35315585   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.35312423   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   19   0.35301402   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.35074458   REFERENCES:SIMDATES:SIMLOC
