###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 42.479, activation diff: 34.098, ratio: 0.803
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 72.400, activation diff: 31.164, ratio: 0.430
#   pulse 4: activated nodes: 9786, borderline nodes: 6834, overall activation: 215.101, activation diff: 142.899, ratio: 0.664
#   pulse 5: activated nodes: 10361, borderline nodes: 5126, overall activation: 407.397, activation diff: 192.296, ratio: 0.472
#   pulse 6: activated nodes: 10749, borderline nodes: 4824, overall activation: 651.068, activation diff: 243.671, ratio: 0.374
#   pulse 7: activated nodes: 10848, borderline nodes: 4193, overall activation: 904.208, activation diff: 253.140, ratio: 0.280
#   pulse 8: activated nodes: 10917, borderline nodes: 3787, overall activation: 1139.922, activation diff: 235.714, ratio: 0.207
#   pulse 9: activated nodes: 10943, borderline nodes: 3672, overall activation: 1350.778, activation diff: 210.856, ratio: 0.156
#   pulse 10: activated nodes: 10949, borderline nodes: 3614, overall activation: 1531.478, activation diff: 180.700, ratio: 0.118
#   pulse 11: activated nodes: 10959, borderline nodes: 3590, overall activation: 1675.151, activation diff: 143.673, ratio: 0.086
#   pulse 12: activated nodes: 10959, borderline nodes: 3566, overall activation: 1781.447, activation diff: 106.296, ratio: 0.060
#   pulse 13: activated nodes: 10961, borderline nodes: 3556, overall activation: 1855.952, activation diff: 74.505, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10961
#   final overall activation: 1856.0
#   number of spread. activ. pulses: 13
#   running time: 1709

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9627243   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8474493   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.82658637   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7720078   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6404406   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.60149056   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48412317   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4643317   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45324722   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.44680414   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4443783   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.44390267   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.43732947   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.43309236   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.43253586   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.43026936   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.42975676   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.42823106   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.42618275   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.42616642   REFERENCES:SIMDATES:SIMLOC
