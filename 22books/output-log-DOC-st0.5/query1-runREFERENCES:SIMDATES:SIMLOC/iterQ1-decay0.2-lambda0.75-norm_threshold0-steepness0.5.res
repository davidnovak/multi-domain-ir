###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 49.097, activation diff: 41.632, ratio: 0.848
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 116.167, activation diff: 68.266, ratio: 0.588
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 274.614, activation diff: 158.993, ratio: 0.579
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 541.599, activation diff: 267.139, ratio: 0.493
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 924.640, activation diff: 383.053, ratio: 0.414
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 1395.274, activation diff: 470.634, ratio: 0.337
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 1901.633, activation diff: 506.359, ratio: 0.266
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 2402.200, activation diff: 500.567, ratio: 0.208
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 2867.326, activation diff: 465.126, ratio: 0.162
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 3281.440, activation diff: 414.114, ratio: 0.126
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 3640.007, activation diff: 358.567, ratio: 0.099
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 3944.683, activation diff: 304.675, ratio: 0.077
#   pulse 14: activated nodes: 11464, borderline nodes: 0, overall activation: 4200.128, activation diff: 255.446, ratio: 0.061
#   pulse 15: activated nodes: 11464, borderline nodes: 0, overall activation: 4412.208, activation diff: 212.080, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4412.2
#   number of spread. activ. pulses: 15
#   running time: 1731

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9274932   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.79892874   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.79499286   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7536511   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   5   0.7347468   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.73394054   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   7   0.7320747   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.72950923   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.7285396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.72849125   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.72744435   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7262599   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.72590256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.725566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.7247749   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.72436005   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.72427493   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.72333556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.720475   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   20   0.7202506   REFERENCES:SIMDATES:SIMLOC
