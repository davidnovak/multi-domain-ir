###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 13.305, activation diff: 6.734, ratio: 0.506
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 27.427, activation diff: 15.486, ratio: 0.565
#   pulse 4: activated nodes: 8803, borderline nodes: 6572, overall activation: 58.671, activation diff: 32.112, ratio: 0.547
#   pulse 5: activated nodes: 10041, borderline nodes: 6946, overall activation: 122.910, activation diff: 64.693, ratio: 0.526
#   pulse 6: activated nodes: 10746, borderline nodes: 6219, overall activation: 232.007, activation diff: 109.269, ratio: 0.471
#   pulse 7: activated nodes: 11074, borderline nodes: 5213, overall activation: 396.983, activation diff: 164.996, ratio: 0.416
#   pulse 8: activated nodes: 11212, borderline nodes: 4126, overall activation: 626.997, activation diff: 230.014, ratio: 0.367
#   pulse 9: activated nodes: 11296, borderline nodes: 2842, overall activation: 916.052, activation diff: 289.054, ratio: 0.316
#   pulse 10: activated nodes: 11370, borderline nodes: 1786, overall activation: 1249.332, activation diff: 333.281, ratio: 0.267
#   pulse 11: activated nodes: 11400, borderline nodes: 1224, overall activation: 1609.309, activation diff: 359.977, ratio: 0.224
#   pulse 12: activated nodes: 11407, borderline nodes: 836, overall activation: 1974.221, activation diff: 364.912, ratio: 0.185
#   pulse 13: activated nodes: 11419, borderline nodes: 589, overall activation: 2324.693, activation diff: 350.472, ratio: 0.151
#   pulse 14: activated nodes: 11426, borderline nodes: 457, overall activation: 2646.351, activation diff: 321.658, ratio: 0.122
#   pulse 15: activated nodes: 11429, borderline nodes: 337, overall activation: 2931.127, activation diff: 284.775, ratio: 0.097

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11429
#   final overall activation: 2931.1
#   number of spread. activ. pulses: 15
#   running time: 1877

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8894586   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.7181997   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.70355535   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.67396796   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.66598976   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.6455002   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   7   0.64109397   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.62288105   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   9   0.62097204   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   10   0.62028706   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   11   0.61620444   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   12   0.616034   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   13   0.6157076   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   14   0.6154282   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   15   0.6148511   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.6128106   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   17   0.61235476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   18   0.61207753   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   19   0.6111618   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   20   0.6101773   REFERENCES:SIMDATES:SIMLOC
