###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 25.457, activation diff: 18.698, ratio: 0.734
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 40.121, activation diff: 16.722, ratio: 0.417
#   pulse 4: activated nodes: 8917, borderline nodes: 7084, overall activation: 129.356, activation diff: 90.007, ratio: 0.696
#   pulse 5: activated nodes: 9657, borderline nodes: 5796, overall activation: 257.484, activation diff: 128.196, ratio: 0.498
#   pulse 6: activated nodes: 10393, borderline nodes: 5634, overall activation: 440.043, activation diff: 182.559, ratio: 0.415
#   pulse 7: activated nodes: 10713, borderline nodes: 4990, overall activation: 656.756, activation diff: 216.714, ratio: 0.330
#   pulse 8: activated nodes: 10812, borderline nodes: 4456, overall activation: 878.731, activation diff: 221.975, ratio: 0.253
#   pulse 9: activated nodes: 10874, borderline nodes: 4001, overall activation: 1084.193, activation diff: 205.463, ratio: 0.190
#   pulse 10: activated nodes: 10918, borderline nodes: 3775, overall activation: 1269.452, activation diff: 185.258, ratio: 0.146
#   pulse 11: activated nodes: 10934, borderline nodes: 3692, overall activation: 1434.354, activation diff: 164.902, ratio: 0.115
#   pulse 12: activated nodes: 10942, borderline nodes: 3652, overall activation: 1573.100, activation diff: 138.746, ratio: 0.088
#   pulse 13: activated nodes: 10949, borderline nodes: 3628, overall activation: 1681.743, activation diff: 108.643, ratio: 0.065
#   pulse 14: activated nodes: 10949, borderline nodes: 3606, overall activation: 1761.696, activation diff: 79.953, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10949
#   final overall activation: 1761.7
#   number of spread. activ. pulses: 14
#   running time: 1711

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96094453   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8396062   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8146931   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7623622   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6249378   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5847422   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48357219   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46278355   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45085555   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4445292   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4419216   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.44100896   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.4345352   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.42966503   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.42862886   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.4273612   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.42641228   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.4241253   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.42303675   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   20   0.4217943   REFERENCES:SIMDATES:SIMLOC
