###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 126.044, activation diff: 127.943, ratio: 1.015
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 120.507, activation diff: 147.645, ratio: 1.225
#   pulse 4: activated nodes: 10676, borderline nodes: 7607, overall activation: 1042.067, activation diff: 966.681, ratio: 0.928
#   pulse 5: activated nodes: 11029, borderline nodes: 3721, overall activation: 1803.991, activation diff: 802.024, ratio: 0.445
#   pulse 6: activated nodes: 11337, borderline nodes: 2640, overall activation: 3582.961, activation diff: 1778.972, ratio: 0.497
#   pulse 7: activated nodes: 11414, borderline nodes: 787, overall activation: 4859.534, activation diff: 1276.573, ratio: 0.263
#   pulse 8: activated nodes: 11436, borderline nodes: 326, overall activation: 5563.271, activation diff: 703.737, ratio: 0.126
#   pulse 9: activated nodes: 11450, borderline nodes: 147, overall activation: 5887.501, activation diff: 324.229, ratio: 0.055
#   pulse 10: activated nodes: 11452, borderline nodes: 99, overall activation: 6030.487, activation diff: 142.986, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6030.5
#   number of spread. activ. pulses: 10
#   running time: 1639

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96818805   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8973942   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8960659   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8946818   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8931038   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.89302504   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.8928951   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   8   0.89260185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8923114   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8920809   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.89184296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.89179313   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8915068   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.89073306   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.89063823   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.89042926   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.8902879   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.8902838   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   19   0.8902092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.8901829   REFERENCES:SIMDATES:SIMLOC
