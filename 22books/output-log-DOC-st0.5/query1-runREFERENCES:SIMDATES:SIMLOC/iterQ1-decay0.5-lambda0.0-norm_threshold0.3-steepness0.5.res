###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 155.464, activation diff: 170.394, ratio: 1.096
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 72.543, activation diff: 227.396, ratio: 3.135
#   pulse 4: activated nodes: 10300, borderline nodes: 6841, overall activation: 756.512, activation diff: 818.149, ratio: 1.081
#   pulse 5: activated nodes: 10660, borderline nodes: 4519, overall activation: 299.779, activation diff: 983.679, ratio: 3.281
#   pulse 6: activated nodes: 10839, borderline nodes: 4343, overall activation: 1233.862, activation diff: 1230.529, ratio: 0.997
#   pulse 7: activated nodes: 10929, borderline nodes: 3757, overall activation: 824.696, activation diff: 989.853, ratio: 1.200
#   pulse 8: activated nodes: 10938, borderline nodes: 3700, overall activation: 1655.354, activation diff: 950.049, ratio: 0.574
#   pulse 9: activated nodes: 10948, borderline nodes: 3642, overall activation: 1746.155, activation diff: 231.325, ratio: 0.132
#   pulse 10: activated nodes: 10951, borderline nodes: 3621, overall activation: 1882.741, activation diff: 141.516, ratio: 0.075
#   pulse 11: activated nodes: 10951, borderline nodes: 3597, overall activation: 1910.973, activation diff: 31.054, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10951
#   final overall activation: 1911.0
#   number of spread. activ. pulses: 11
#   running time: 1585

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96564305   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85526234   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8357162   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7737996   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.63354677   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6004844   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48662663   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46732977   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4558354   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.4508825   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44738978   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.44656926   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.44090804   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.43887872   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.43824416   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4344538   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.43441844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.4341992   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.4327772   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.43104133   REFERENCES:SIMDATES:SIMLOC
