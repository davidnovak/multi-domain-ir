###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 274.949, activation diff: 273.344, ratio: 0.994
#   pulse 3: activated nodes: 9366, borderline nodes: 4295, overall activation: 397.783, activation diff: 287.190, ratio: 0.722
#   pulse 4: activated nodes: 11278, borderline nodes: 4709, overall activation: 2139.030, activation diff: 1747.374, ratio: 0.817
#   pulse 5: activated nodes: 11394, borderline nodes: 1123, overall activation: 3749.552, activation diff: 1610.582, ratio: 0.430
#   pulse 6: activated nodes: 11444, borderline nodes: 282, overall activation: 5173.874, activation diff: 1424.322, ratio: 0.275
#   pulse 7: activated nodes: 11454, borderline nodes: 77, overall activation: 5899.816, activation diff: 725.942, ratio: 0.123
#   pulse 8: activated nodes: 11455, borderline nodes: 45, overall activation: 6218.376, activation diff: 318.560, ratio: 0.051
#   pulse 9: activated nodes: 11458, borderline nodes: 23, overall activation: 6353.333, activation diff: 134.957, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 6353.3
#   number of spread. activ. pulses: 9
#   running time: 1571

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9711901   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89757824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8965483   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89537615   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8938347   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.89363545   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.8935464   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   8   0.893397   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8930679   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.89279896   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8926283   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.89253044   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8922838   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.8916387   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.89159673   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.8913343   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.8912647   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.89121497   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   19   0.891212   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.89117223   REFERENCES:SIMDATES:SIMLOC
