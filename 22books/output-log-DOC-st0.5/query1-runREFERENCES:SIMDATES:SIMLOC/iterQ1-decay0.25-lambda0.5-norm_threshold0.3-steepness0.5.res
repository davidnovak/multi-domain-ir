###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 33.806, activation diff: 27.047, ratio: 0.800
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 56.902, activation diff: 25.008, ratio: 0.439
#   pulse 4: activated nodes: 9592, borderline nodes: 7708, overall activation: 225.443, activation diff: 169.135, ratio: 0.750
#   pulse 5: activated nodes: 10401, borderline nodes: 5972, overall activation: 494.659, activation diff: 269.227, ratio: 0.544
#   pulse 6: activated nodes: 11105, borderline nodes: 5237, overall activation: 964.334, activation diff: 469.676, ratio: 0.487
#   pulse 7: activated nodes: 11271, borderline nodes: 3296, overall activation: 1571.141, activation diff: 606.807, ratio: 0.386
#   pulse 8: activated nodes: 11371, borderline nodes: 1726, overall activation: 2225.274, activation diff: 654.133, ratio: 0.294
#   pulse 9: activated nodes: 11405, borderline nodes: 969, overall activation: 2822.817, activation diff: 597.543, ratio: 0.212
#   pulse 10: activated nodes: 11418, borderline nodes: 609, overall activation: 3291.975, activation diff: 469.158, ratio: 0.143
#   pulse 11: activated nodes: 11428, borderline nodes: 415, overall activation: 3621.478, activation diff: 329.503, ratio: 0.091
#   pulse 12: activated nodes: 11430, borderline nodes: 317, overall activation: 3840.743, activation diff: 219.265, ratio: 0.057
#   pulse 13: activated nodes: 11434, borderline nodes: 270, overall activation: 3983.063, activation diff: 142.320, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11434
#   final overall activation: 3983.1
#   number of spread. activ. pulses: 13
#   running time: 1602

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.962914   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.84146595   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.83284414   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.76423895   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7421948   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.7293239   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.72885597   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.72760856   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7258278   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.72286767   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7216741   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7202935   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7201402   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7176881   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.7173251   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.71673805   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7167345   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.7162225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7161968   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   20   0.7161876   REFERENCES:SIMDATES:SIMLOC
