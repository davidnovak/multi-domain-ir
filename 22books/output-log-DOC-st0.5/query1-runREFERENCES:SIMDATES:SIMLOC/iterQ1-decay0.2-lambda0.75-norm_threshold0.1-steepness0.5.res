###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 24.397, activation diff: 17.376, ratio: 0.712
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 56.144, activation diff: 33.026, ratio: 0.588
#   pulse 4: activated nodes: 10538, borderline nodes: 6546, overall activation: 135.129, activation diff: 79.675, ratio: 0.590
#   pulse 5: activated nodes: 11047, borderline nodes: 5228, overall activation: 279.424, activation diff: 144.576, ratio: 0.517
#   pulse 6: activated nodes: 11225, borderline nodes: 3904, overall activation: 508.730, activation diff: 229.344, ratio: 0.451
#   pulse 7: activated nodes: 11334, borderline nodes: 2336, overall activation: 832.321, activation diff: 323.591, ratio: 0.389
#   pulse 8: activated nodes: 11398, borderline nodes: 1236, overall activation: 1235.278, activation diff: 402.957, ratio: 0.326
#   pulse 9: activated nodes: 11418, borderline nodes: 700, overall activation: 1686.946, activation diff: 451.668, ratio: 0.268
#   pulse 10: activated nodes: 11431, borderline nodes: 419, overall activation: 2149.294, activation diff: 462.348, ratio: 0.215
#   pulse 11: activated nodes: 11436, borderline nodes: 252, overall activation: 2592.974, activation diff: 443.680, ratio: 0.171
#   pulse 12: activated nodes: 11447, borderline nodes: 177, overall activation: 2997.442, activation diff: 404.469, ratio: 0.135
#   pulse 13: activated nodes: 11452, borderline nodes: 135, overall activation: 3353.082, activation diff: 355.640, ratio: 0.106
#   pulse 14: activated nodes: 11453, borderline nodes: 106, overall activation: 3658.536, activation diff: 305.454, ratio: 0.083
#   pulse 15: activated nodes: 11453, borderline nodes: 91, overall activation: 3916.759, activation diff: 258.223, ratio: 0.066

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 3916.8
#   number of spread. activ. pulses: 15
#   running time: 1813

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91424656   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.76596826   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.7647642   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7432674   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   5   0.7196642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   6   0.7176845   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.71151644   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   8   0.7099181   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.70823264   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.70785236   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.7074857   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   12   0.70698565   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.70686567   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.7046415   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.70413816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.7037841   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.70280933   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.700186   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.6992471   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.69732606   REFERENCES:SIMDATES:SIMLOC
