###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 112.657, activation diff: 114.556, ratio: 1.017
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 101.947, activation diff: 125.706, ratio: 1.233
#   pulse 4: activated nodes: 10646, borderline nodes: 7599, overall activation: 819.463, activation diff: 755.586, ratio: 0.922
#   pulse 5: activated nodes: 10988, borderline nodes: 4103, overall activation: 1374.064, activation diff: 584.767, ratio: 0.426
#   pulse 6: activated nodes: 11314, borderline nodes: 3236, overall activation: 2656.470, activation diff: 1282.424, ratio: 0.483
#   pulse 7: activated nodes: 11398, borderline nodes: 1016, overall activation: 3672.708, activation diff: 1016.238, ratio: 0.277
#   pulse 8: activated nodes: 11426, borderline nodes: 499, overall activation: 4291.923, activation diff: 619.215, ratio: 0.144
#   pulse 9: activated nodes: 11436, borderline nodes: 262, overall activation: 4591.619, activation diff: 299.696, ratio: 0.065
#   pulse 10: activated nodes: 11442, borderline nodes: 189, overall activation: 4727.382, activation diff: 135.763, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 4727.4
#   number of spread. activ. pulses: 10
#   running time: 1632

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9676287   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8538848   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8478499   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7959838   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7908642   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.7879777   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.7870454   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.78633666   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   9   0.78503346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.7850252   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7842025   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.78388596   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.78372526   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.7824536   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.78233266   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   16   0.78210247   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.78205884   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.7816777   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.781515   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7810854   REFERENCES:SIMDATES:SIMLOC
