###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 53.857, activation diff: 46.393, ratio: 0.861
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 131.425, activation diff: 78.764, ratio: 0.599
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 328.498, activation diff: 197.605, ratio: 0.602
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 676.523, activation diff: 348.163, ratio: 0.515
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 1190.487, activation diff: 513.973, ratio: 0.432
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 1811.369, activation diff: 620.882, ratio: 0.343
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 2468.154, activation diff: 656.786, ratio: 0.266
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 3105.003, activation diff: 636.849, ratio: 0.205
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 3685.840, activation diff: 580.838, ratio: 0.158
#   pulse 11: activated nodes: 11464, borderline nodes: 0, overall activation: 4195.581, activation diff: 509.740, ratio: 0.121
#   pulse 12: activated nodes: 11464, borderline nodes: 0, overall activation: 4632.084, activation diff: 436.503, ratio: 0.094
#   pulse 13: activated nodes: 11464, borderline nodes: 0, overall activation: 4999.720, activation diff: 367.637, ratio: 0.074
#   pulse 14: activated nodes: 11464, borderline nodes: 0, overall activation: 5305.774, activation diff: 306.054, ratio: 0.058
#   pulse 15: activated nodes: 11464, borderline nodes: 0, overall activation: 5558.440, activation diff: 252.666, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5558.4
#   number of spread. activ. pulses: 15
#   running time: 1635

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9293633   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8535109   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.84172994   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8383736   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.8379518   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.8370914   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   7   0.83674705   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8363734   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   9   0.83541536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.8349925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   11   0.8346722   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   12   0.83441234   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.8342335   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.8337831   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.8331918   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   16   0.83315563   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.8330551   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   18   0.83091664   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.8308295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   20   0.8302305   REFERENCES:SIMDATES:SIMLOC
