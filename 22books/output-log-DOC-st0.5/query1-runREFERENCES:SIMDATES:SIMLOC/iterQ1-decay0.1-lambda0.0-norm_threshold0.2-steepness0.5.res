###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 379.756, activation diff: 395.715, ratio: 1.042
#   pulse 3: activated nodes: 9141, borderline nodes: 5079, overall activation: 302.848, activation diff: 675.057, ratio: 2.229
#   pulse 4: activated nodes: 11202, borderline nodes: 5966, overall activation: 3090.368, activation diff: 3211.925, ratio: 1.039
#   pulse 5: activated nodes: 11367, borderline nodes: 1432, overall activation: 2593.642, activation diff: 3443.231, ratio: 1.328
#   pulse 6: activated nodes: 11439, borderline nodes: 414, overall activation: 5615.784, activation diff: 3429.584, ratio: 0.611
#   pulse 7: activated nodes: 11453, borderline nodes: 130, overall activation: 6011.302, activation diff: 591.875, ratio: 0.098
#   pulse 8: activated nodes: 11455, borderline nodes: 74, overall activation: 6234.625, activation diff: 230.780, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6234.6
#   number of spread. activ. pulses: 8
#   running time: 1539

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9704692   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89780486   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8970498   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8957899   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89436656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   6   0.89418256   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.89418066   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8939961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8935009   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.89313686   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.8930979   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.8930122   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8927686   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.8924182   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.8920406   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.89187336   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.89176863   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.89175916   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.89175874   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.89154553   REFERENCES:SIMDATES:SIMLOC
