###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 9.461, activation diff: 3.343, ratio: 0.353
#   pulse 3: activated nodes: 7566, borderline nodes: 7245, overall activation: 15.729, activation diff: 7.718, ratio: 0.491
#   pulse 4: activated nodes: 7939, borderline nodes: 6771, overall activation: 27.796, activation diff: 13.225, ratio: 0.476
#   pulse 5: activated nodes: 8662, borderline nodes: 7075, overall activation: 59.985, activation diff: 33.015, ratio: 0.550
#   pulse 6: activated nodes: 9567, borderline nodes: 7016, overall activation: 124.169, activation diff: 64.578, ratio: 0.520
#   pulse 7: activated nodes: 10406, borderline nodes: 6827, overall activation: 237.163, activation diff: 113.123, ratio: 0.477
#   pulse 8: activated nodes: 10852, borderline nodes: 5899, overall activation: 418.753, activation diff: 181.605, ratio: 0.434
#   pulse 9: activated nodes: 11137, borderline nodes: 4776, overall activation: 693.774, activation diff: 275.021, ratio: 0.396
#   pulse 10: activated nodes: 11259, borderline nodes: 3477, overall activation: 1075.517, activation diff: 381.744, ratio: 0.355
#   pulse 11: activated nodes: 11318, borderline nodes: 2270, overall activation: 1552.547, activation diff: 477.029, ratio: 0.307
#   pulse 12: activated nodes: 11384, borderline nodes: 1439, overall activation: 2082.758, activation diff: 530.211, ratio: 0.255
#   pulse 13: activated nodes: 11401, borderline nodes: 936, overall activation: 2625.217, activation diff: 542.460, ratio: 0.207
#   pulse 14: activated nodes: 11417, borderline nodes: 650, overall activation: 3148.436, activation diff: 523.218, ratio: 0.166
#   pulse 15: activated nodes: 11424, borderline nodes: 476, overall activation: 3627.878, activation diff: 479.443, ratio: 0.132

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11424
#   final overall activation: 3627.9
#   number of spread. activ. pulses: 15
#   running time: 1788

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8577783   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.7900904   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.77367246   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   4   0.7482539   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.74537235   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   6   0.73794377   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   7   0.7378142   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   8   0.7348037   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   9   0.7322867   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   10   0.7309061   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7300221   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.72944987   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.7288519   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   14   0.7268046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   15   0.72506714   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7243115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.72390854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   18   0.7237298   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.7226494   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   20   0.72253156   REFERENCES:SIMDATES:SIMLOC
