###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 566.843, activation diff: 583.439, ratio: 1.029
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 547.080, activation diff: 961.321, ratio: 1.757
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3159.122, activation diff: 2919.949, ratio: 0.924
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3665.740, activation diff: 1312.928, ratio: 0.358
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 4552.181, activation diff: 914.275, ratio: 0.201
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 4671.112, activation diff: 125.949, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4671.1
#   number of spread. activ. pulses: 7
#   running time: 1509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9725142   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8746365   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.87011486   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.8028425   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7460482   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7393345   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.73746467   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7366954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.73609996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7337384   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   11   0.73242223   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.73231554   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7315612   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.73101735   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.7306522   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7306361   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7304209   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7298292   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   19   0.72970265   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7283166   REFERENCES:SIMDATES:SIMLOC
