###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 9.374, activation diff: 3.256, ratio: 0.347
#   pulse 3: activated nodes: 7566, borderline nodes: 7245, overall activation: 14.559, activation diff: 6.635, ratio: 0.456
#   pulse 4: activated nodes: 7939, borderline nodes: 6771, overall activation: 24.220, activation diff: 10.833, ratio: 0.447
#   pulse 5: activated nodes: 8558, borderline nodes: 6989, overall activation: 48.423, activation diff: 25.073, ratio: 0.518
#   pulse 6: activated nodes: 9133, borderline nodes: 6734, overall activation: 95.425, activation diff: 47.458, ratio: 0.497
#   pulse 7: activated nodes: 10278, borderline nodes: 6953, overall activation: 174.239, activation diff: 78.962, ratio: 0.453
#   pulse 8: activated nodes: 10762, borderline nodes: 6201, overall activation: 293.385, activation diff: 119.190, ratio: 0.406
#   pulse 9: activated nodes: 11049, borderline nodes: 5391, overall activation: 462.105, activation diff: 168.720, ratio: 0.365
#   pulse 10: activated nodes: 11188, borderline nodes: 4436, overall activation: 687.387, activation diff: 225.281, ratio: 0.328
#   pulse 11: activated nodes: 11273, borderline nodes: 3337, overall activation: 961.274, activation diff: 273.887, ratio: 0.285
#   pulse 12: activated nodes: 11321, borderline nodes: 2295, overall activation: 1271.685, activation diff: 310.411, ratio: 0.244
#   pulse 13: activated nodes: 11375, borderline nodes: 1609, overall activation: 1605.241, activation diff: 333.556, ratio: 0.208
#   pulse 14: activated nodes: 11395, borderline nodes: 1226, overall activation: 1943.979, activation diff: 338.738, ratio: 0.174
#   pulse 15: activated nodes: 11406, borderline nodes: 899, overall activation: 2271.565, activation diff: 327.586, ratio: 0.144

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11406
#   final overall activation: 2271.6
#   number of spread. activ. pulses: 15
#   running time: 1760

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84533703   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.64839935   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   3   0.63615185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   4   0.6123004   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.6027485   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   6   0.5978419   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   7   0.5894091   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.58460474   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.5762241   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   10   0.57283   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.5709816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.5689473   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.5655137   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.5639392   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   15   0.5633522   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.56161565   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.55929106   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.5559179   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   19   0.5550381   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   20   0.5524647   REFERENCES:SIMDATES:SIMLOC
