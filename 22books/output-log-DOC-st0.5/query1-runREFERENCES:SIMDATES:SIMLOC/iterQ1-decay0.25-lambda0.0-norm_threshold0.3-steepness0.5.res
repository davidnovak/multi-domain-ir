###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 232.145, activation diff: 247.075, ratio: 1.064
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 147.187, activation diff: 378.207, ratio: 2.570
#   pulse 4: activated nodes: 10996, borderline nodes: 7205, overall activation: 1635.498, activation diff: 1744.556, ratio: 1.067
#   pulse 5: activated nodes: 11251, borderline nodes: 2782, overall activation: 948.749, activation diff: 2289.813, ratio: 2.414
#   pulse 6: activated nodes: 11364, borderline nodes: 1581, overall activation: 3213.458, activation diff: 2901.392, ratio: 0.903
#   pulse 7: activated nodes: 11418, borderline nodes: 620, overall activation: 3163.194, activation diff: 1385.614, ratio: 0.438
#   pulse 8: activated nodes: 11428, borderline nodes: 367, overall activation: 4053.185, activation diff: 982.982, ratio: 0.243
#   pulse 9: activated nodes: 11432, borderline nodes: 270, overall activation: 4161.023, activation diff: 140.951, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11432
#   final overall activation: 4161.0
#   number of spread. activ. pulses: 9
#   running time: 1613

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9671137   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85576403   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8466561   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77452147   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7453183   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7373134   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.73504555   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.73421556   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7337141   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7305171   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7284459   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.7284064   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7270464   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.72673786   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.72671676   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   16   0.7264029   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7263136   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.7257595   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.7254959   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7242157   REFERENCES:SIMDATES:SIMLOC
