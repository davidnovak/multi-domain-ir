###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 72.495, activation diff: 74.394, ratio: 1.026
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 56.634, activation diff: 70.176, ratio: 1.239
#   pulse 4: activated nodes: 9871, borderline nodes: 7030, overall activation: 364.960, activation diff: 326.949, ratio: 0.896
#   pulse 5: activated nodes: 10336, borderline nodes: 5010, overall activation: 551.440, activation diff: 199.454, ratio: 0.362
#   pulse 6: activated nodes: 10745, borderline nodes: 5026, overall activation: 925.019, activation diff: 373.579, ratio: 0.404
#   pulse 7: activated nodes: 10865, borderline nodes: 4052, overall activation: 1214.186, activation diff: 289.167, ratio: 0.238
#   pulse 8: activated nodes: 10917, borderline nodes: 3774, overall activation: 1459.793, activation diff: 245.608, ratio: 0.168
#   pulse 9: activated nodes: 10942, borderline nodes: 3666, overall activation: 1651.291, activation diff: 191.498, ratio: 0.116
#   pulse 10: activated nodes: 10949, borderline nodes: 3624, overall activation: 1779.660, activation diff: 128.369, ratio: 0.072
#   pulse 11: activated nodes: 10952, borderline nodes: 3603, overall activation: 1853.897, activation diff: 74.237, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10952
#   final overall activation: 1853.9
#   number of spread. activ. pulses: 11
#   running time: 1566

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9652549   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.853392   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8282542   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77171385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.63258433   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5977448   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48632306   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46692634   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45440087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.44940364   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44712046   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4458569   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.43833667   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.43798366   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.4343016   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4337772   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.43317693   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.43115225   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.42960033   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.42938998   REFERENCES:SIMDATES:SIMLOC
