###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 183.387, activation diff: 172.227, ratio: 0.939
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 417.338, activation diff: 234.589, ratio: 0.562
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1355.353, activation diff: 938.017, ratio: 0.692
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2649.934, activation diff: 1294.582, ratio: 0.489
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3920.081, activation diff: 1270.147, ratio: 0.324
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 4890.404, activation diff: 970.323, ratio: 0.198
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5548.266, activation diff: 657.862, ratio: 0.119
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 5970.248, activation diff: 421.982, ratio: 0.071
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 6233.602, activation diff: 263.354, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6233.6
#   number of spread. activ. pulses: 10
#   running time: 1586

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96216035   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8897717   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8858061   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8841152   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   5   0.8821816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   6   0.8818592   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   7   0.88179004   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.88156116   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.88136744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   10   0.881284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.88112676   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8807881   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.8806547   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.88045084   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.880167   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.8797003   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8788768   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.8785618   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.87835014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.87818325   REFERENCES:SIMDATES:SIMLOC
