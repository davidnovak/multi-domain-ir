###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 285.981, activation diff: 302.510, ratio: 1.058
#   pulse 3: activated nodes: 9517, borderline nodes: 4002, overall activation: 143.896, activation diff: 416.750, ratio: 2.896
#   pulse 4: activated nodes: 10784, borderline nodes: 4554, overall activation: 1127.796, activation diff: 1173.291, ratio: 1.040
#   pulse 5: activated nodes: 10939, borderline nodes: 3704, overall activation: 735.236, activation diff: 1077.214, ratio: 1.465
#   pulse 6: activated nodes: 10958, borderline nodes: 3575, overall activation: 1741.418, activation diff: 1125.525, ratio: 0.646
#   pulse 7: activated nodes: 10967, borderline nodes: 3536, overall activation: 1872.186, activation diff: 267.229, ratio: 0.143
#   pulse 8: activated nodes: 10968, borderline nodes: 3531, overall activation: 2024.757, activation diff: 155.894, ratio: 0.077
#   pulse 9: activated nodes: 10969, borderline nodes: 3524, overall activation: 2054.036, activation diff: 30.952, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 2054.0
#   number of spread. activ. pulses: 9
#   running time: 1562

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9695593   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8682858   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.85456014   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7932054   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.665194   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6318059   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48807457   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4705719   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.46041995   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.4557136   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.45246464   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.4518539   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.4478879   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.44491577   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.4441417   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.44062674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   17   0.44054073   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   18   0.44043306   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.4389962   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.43811217   REFERENCES:SIMDATES:SIMLOC
