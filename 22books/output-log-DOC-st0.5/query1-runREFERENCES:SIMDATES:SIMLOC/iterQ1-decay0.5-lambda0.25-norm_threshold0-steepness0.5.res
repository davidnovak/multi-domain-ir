###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 222.788, activation diff: 219.205, ratio: 0.984
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 297.433, activation diff: 133.642, ratio: 0.449
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 896.760, activation diff: 599.388, ratio: 0.668
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1355.555, activation diff: 458.795, ratio: 0.338
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1717.626, activation diff: 362.070, ratio: 0.211
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1932.577, activation diff: 214.952, ratio: 0.111
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2041.866, activation diff: 109.288, ratio: 0.054
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2093.539, activation diff: 51.673, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2093.5
#   number of spread. activ. pulses: 9
#   running time: 1530

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9705451   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.872339   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8592771   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7999772   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.67799354   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6439098   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48818886   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4712999   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.46142903   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.456266   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4542436   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.45328578   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   13   0.44867635   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   14   0.4468256   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.44429943   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.44249886   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.44190422   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.44137144   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   19   0.43968588   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.4395775   REFERENCES:SIMDATES:SIMLOC
