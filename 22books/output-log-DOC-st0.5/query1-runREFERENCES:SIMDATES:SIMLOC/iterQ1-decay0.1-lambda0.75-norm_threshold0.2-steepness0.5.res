###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 14.038, activation diff: 7.468, ratio: 0.532
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 30.969, activation diff: 18.294, ratio: 0.591
#   pulse 4: activated nodes: 8899, borderline nodes: 6663, overall activation: 71.220, activation diff: 41.105, ratio: 0.577
#   pulse 5: activated nodes: 10118, borderline nodes: 6926, overall activation: 157.695, activation diff: 86.905, ratio: 0.551
#   pulse 6: activated nodes: 10814, borderline nodes: 6031, overall activation: 313.580, activation diff: 156.026, ratio: 0.498
#   pulse 7: activated nodes: 11144, borderline nodes: 4757, overall activation: 566.348, activation diff: 252.776, ratio: 0.446
#   pulse 8: activated nodes: 11269, borderline nodes: 3344, overall activation: 939.343, activation diff: 372.995, ratio: 0.397
#   pulse 9: activated nodes: 11339, borderline nodes: 1942, overall activation: 1425.940, activation diff: 486.597, ratio: 0.341
#   pulse 10: activated nodes: 11401, borderline nodes: 1187, overall activation: 1982.643, activation diff: 556.703, ratio: 0.281
#   pulse 11: activated nodes: 11415, borderline nodes: 704, overall activation: 2559.394, activation diff: 576.751, ratio: 0.225
#   pulse 12: activated nodes: 11432, borderline nodes: 454, overall activation: 3118.209, activation diff: 558.815, ratio: 0.179
#   pulse 13: activated nodes: 11440, borderline nodes: 285, overall activation: 3630.514, activation diff: 512.305, ratio: 0.141
#   pulse 14: activated nodes: 11447, borderline nodes: 187, overall activation: 4082.273, activation diff: 451.760, ratio: 0.111
#   pulse 15: activated nodes: 11450, borderline nodes: 147, overall activation: 4470.799, activation diff: 388.526, ratio: 0.087

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 4470.8
#   number of spread. activ. pulses: 15
#   running time: 1668

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89659506   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8256371   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.8065415   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   4   0.79733074   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.79563534   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   6   0.78934574   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   7   0.78909063   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.78908145   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.78755784   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.7865038   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   11   0.78573936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   12   0.7848179   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   13   0.7846837   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   14   0.78459936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.78343   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.7824991   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.7810086   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   18   0.78076375   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_578   19   0.77955544   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7779224   REFERENCES:SIMDATES:SIMLOC
