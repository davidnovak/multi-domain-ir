###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 212.118, activation diff: 228.077, ratio: 1.075
#   pulse 3: activated nodes: 9141, borderline nodes: 5079, overall activation: 99.429, activation diff: 308.612, ratio: 3.104
#   pulse 4: activated nodes: 10572, borderline nodes: 6035, overall activation: 913.892, activation diff: 983.122, ratio: 1.076
#   pulse 5: activated nodes: 10843, borderline nodes: 4054, overall activation: 402.148, activation diff: 1134.801, ratio: 2.822
#   pulse 6: activated nodes: 10898, borderline nodes: 3794, overall activation: 1445.805, activation diff: 1302.814, ratio: 0.901
#   pulse 7: activated nodes: 10955, borderline nodes: 3629, overall activation: 1371.670, activation diff: 650.549, ratio: 0.474
#   pulse 8: activated nodes: 10959, borderline nodes: 3590, overall activation: 1869.304, activation diff: 530.677, ratio: 0.284
#   pulse 9: activated nodes: 10961, borderline nodes: 3563, overall activation: 1940.578, activation diff: 92.127, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10961
#   final overall activation: 1940.6
#   number of spread. activ. pulses: 9
#   running time: 1674

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96421826   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8576337   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8311486   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7833423   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.639682   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6126559   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4860713   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46675992   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45604238   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.44899392   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44846904   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.44697964   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.43936008   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.43703336   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.43697885   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   16   0.4359521   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.43590492   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.4342093   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.4333075   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.4328066   REFERENCES:SIMDATES:SIMLOC
