###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 455.622, activation diff: 472.151, ratio: 1.036
#   pulse 3: activated nodes: 9517, borderline nodes: 4002, overall activation: 366.278, activation diff: 799.249, ratio: 2.182
#   pulse 4: activated nodes: 11334, borderline nodes: 3962, overall activation: 2919.988, activation diff: 2973.397, ratio: 1.018
#   pulse 5: activated nodes: 11417, borderline nodes: 786, overall activation: 2669.336, activation diff: 2544.215, ratio: 0.953
#   pulse 6: activated nodes: 11448, borderline nodes: 195, overall activation: 4735.500, activation diff: 2281.477, ratio: 0.482
#   pulse 7: activated nodes: 11454, borderline nodes: 83, overall activation: 4982.622, activation diff: 331.145, ratio: 0.066
#   pulse 8: activated nodes: 11455, borderline nodes: 57, overall activation: 5099.076, activation diff: 119.139, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5099.1
#   number of spread. activ. pulses: 8
#   running time: 1611

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97174644   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8686433   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8676454   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79670924   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.79380435   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7929768   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.79025924   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.78933805   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.788295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.78810465   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.7877543   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.7877279   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.78695554   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.7866182   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7864382   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.78542393   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.78511816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7847982   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7846623   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.7846278   REFERENCES:SIMDATES:SIMLOC
