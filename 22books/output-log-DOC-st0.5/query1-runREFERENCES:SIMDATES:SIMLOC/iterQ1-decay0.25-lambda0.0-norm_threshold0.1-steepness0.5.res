###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 427.348, activation diff: 443.878, ratio: 1.039
#   pulse 3: activated nodes: 9517, borderline nodes: 4002, overall activation: 322.777, activation diff: 729.079, ratio: 2.259
#   pulse 4: activated nodes: 11321, borderline nodes: 4010, overall activation: 2535.470, activation diff: 2592.332, ratio: 1.022
#   pulse 5: activated nodes: 11409, borderline nodes: 852, overall activation: 2246.102, activation diff: 2284.999, ratio: 1.017
#   pulse 6: activated nodes: 11445, borderline nodes: 257, overall activation: 4130.948, activation diff: 2095.720, ratio: 0.507
#   pulse 7: activated nodes: 11454, borderline nodes: 116, overall activation: 4361.580, activation diff: 320.807, ratio: 0.074
#   pulse 8: activated nodes: 11454, borderline nodes: 94, overall activation: 4480.996, activation diff: 122.143, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 4481.0
#   number of spread. activ. pulses: 8
#   running time: 1587

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9715544   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86864287   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8670381   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7937909   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.74588287   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7389519   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.73712796   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.73614705   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.73551834   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7333323   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   11   0.7318496   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.73177594   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7317137   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.7307704   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.73043346   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7301918   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7295744   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7290195   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   19   0.7287375   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.72767836   REFERENCES:SIMDATES:SIMLOC
