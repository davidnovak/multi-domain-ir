###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 245.359, activation diff: 243.754, ratio: 0.993
#   pulse 3: activated nodes: 9366, borderline nodes: 4295, overall activation: 334.107, activation diff: 235.763, ratio: 0.706
#   pulse 4: activated nodes: 11264, borderline nodes: 4803, overall activation: 1656.302, activation diff: 1327.560, ratio: 0.802
#   pulse 5: activated nodes: 11386, borderline nodes: 1305, overall activation: 2866.779, activation diff: 1210.538, ratio: 0.422
#   pulse 6: activated nodes: 11429, borderline nodes: 402, overall activation: 4011.347, activation diff: 1144.568, ratio: 0.285
#   pulse 7: activated nodes: 11453, borderline nodes: 127, overall activation: 4628.801, activation diff: 617.453, ratio: 0.133
#   pulse 8: activated nodes: 11454, borderline nodes: 71, overall activation: 4909.650, activation diff: 280.849, ratio: 0.057
#   pulse 9: activated nodes: 11456, borderline nodes: 55, overall activation: 5030.109, activation diff: 120.460, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 5030.1
#   number of spread. activ. pulses: 9
#   running time: 1731

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97075695   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8663403   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8629577   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79630214   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.7921453   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.79187673   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7890608   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.78826207   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.78751135   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.78689003   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.78640044   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.7857871   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7857615   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.7852234   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7845053   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.78422666   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.78379154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.7836048   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.7833925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7833322   REFERENCES:SIMDATES:SIMLOC
