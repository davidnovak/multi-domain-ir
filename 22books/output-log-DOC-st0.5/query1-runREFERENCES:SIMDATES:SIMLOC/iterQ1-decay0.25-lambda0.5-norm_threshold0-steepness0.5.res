###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 154.910, activation diff: 143.751, ratio: 0.928
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 330.894, activation diff: 176.624, ratio: 0.534
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 955.938, activation diff: 625.050, ratio: 0.654
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 1799.015, activation diff: 843.077, ratio: 0.469
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 2669.123, activation diff: 870.108, ratio: 0.326
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 3369.915, activation diff: 700.792, ratio: 0.208
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 3862.894, activation diff: 492.979, ratio: 0.128
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 4187.416, activation diff: 324.522, ratio: 0.077
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 4393.580, activation diff: 206.164, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4393.6
#   number of spread. activ. pulses: 10
#   running time: 1622

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96091783   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85297155   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8460096   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7817849   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7376656   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.7248191   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.72442997   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.72243965   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.72142625   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.71829474   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7173897   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.7163726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7163518   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7142241   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7138393   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.71324015   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7131082   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   18   0.712704   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   19   0.71239996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   20   0.71236795   REFERENCES:SIMDATES:SIMLOC
