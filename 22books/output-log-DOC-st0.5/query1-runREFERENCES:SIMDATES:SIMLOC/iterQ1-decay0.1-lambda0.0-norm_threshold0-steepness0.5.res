###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 679.381, activation diff: 695.977, ratio: 1.024
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 772.617, activation diff: 1258.551, ratio: 1.629
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 4595.837, activation diff: 4210.327, ratio: 0.916
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 5415.983, activation diff: 1672.198, ratio: 0.309
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 6453.058, activation diff: 1055.914, ratio: 0.164
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6587.751, activation diff: 138.661, ratio: 0.021

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6587.8
#   number of spread. activ. pulses: 7
#   running time: 2755

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97328085   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8980185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8973083   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8962464   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89488477   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.89468396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   7   0.8946373   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.89454657   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.89408046   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8937875   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.89368314   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.8936521   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8933668   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.89324284   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.89286894   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.8926245   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   17   0.89258796   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.8924928   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   19   0.8924907   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.892369   REFERENCES:SIMDATES:SIMLOC
