###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 101.713, activation diff: 91.816, ratio: 0.903
#   pulse 3: activated nodes: 9190, borderline nodes: 4794, overall activation: 203.139, activation diff: 102.304, ratio: 0.504
#   pulse 4: activated nodes: 11109, borderline nodes: 5831, overall activation: 684.672, activation diff: 481.577, ratio: 0.703
#   pulse 5: activated nodes: 11305, borderline nodes: 2368, overall activation: 1416.113, activation diff: 731.441, ratio: 0.517
#   pulse 6: activated nodes: 11410, borderline nodes: 1041, overall activation: 2344.444, activation diff: 928.331, ratio: 0.396
#   pulse 7: activated nodes: 11436, borderline nodes: 388, overall activation: 3205.237, activation diff: 860.793, ratio: 0.269
#   pulse 8: activated nodes: 11447, borderline nodes: 177, overall activation: 3868.296, activation diff: 663.059, ratio: 0.171
#   pulse 9: activated nodes: 11453, borderline nodes: 105, overall activation: 4323.454, activation diff: 455.159, ratio: 0.105
#   pulse 10: activated nodes: 11454, borderline nodes: 72, overall activation: 4618.977, activation diff: 295.522, ratio: 0.064
#   pulse 11: activated nodes: 11454, borderline nodes: 62, overall activation: 4805.262, activation diff: 186.285, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 4805.3
#   number of spread. activ. pulses: 11
#   running time: 1642

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96362865   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8517977   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84621763   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7912546   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7826015   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.78021276   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7796033   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   8   0.7791915   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7790272   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.77662706   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7766116   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7765304   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7749186   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7746327   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7743884   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.77430505   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7741127   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.77381814   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.7735506   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.77340746   REFERENCES:SIMDATES:SIMLOC
