###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 35.476, activation diff: 28.716, ratio: 0.809
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 60.384, activation diff: 26.801, ratio: 0.444
#   pulse 4: activated nodes: 9603, borderline nodes: 7715, overall activation: 247.970, activation diff: 188.153, ratio: 0.759
#   pulse 5: activated nodes: 10513, borderline nodes: 5970, overall activation: 554.872, activation diff: 306.907, ratio: 0.553
#   pulse 6: activated nodes: 11135, borderline nodes: 5087, overall activation: 1111.623, activation diff: 556.750, ratio: 0.501
#   pulse 7: activated nodes: 11286, borderline nodes: 2941, overall activation: 1841.674, activation diff: 730.051, ratio: 0.396
#   pulse 8: activated nodes: 11380, borderline nodes: 1468, overall activation: 2624.688, activation diff: 783.015, ratio: 0.298
#   pulse 9: activated nodes: 11408, borderline nodes: 794, overall activation: 3314.262, activation diff: 689.574, ratio: 0.208
#   pulse 10: activated nodes: 11423, borderline nodes: 482, overall activation: 3835.882, activation diff: 521.619, ratio: 0.136
#   pulse 11: activated nodes: 11431, borderline nodes: 306, overall activation: 4194.327, activation diff: 358.445, ratio: 0.085
#   pulse 12: activated nodes: 11439, borderline nodes: 234, overall activation: 4429.842, activation diff: 235.515, ratio: 0.053
#   pulse 13: activated nodes: 11443, borderline nodes: 184, overall activation: 4580.882, activation diff: 151.041, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 4580.9
#   number of spread. activ. pulses: 13
#   running time: 1720

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96348834   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.84252346   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.83604705   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7933065   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7849399   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.78204846   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.78196365   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.7811267   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   9   0.778799   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.7784517   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   11   0.77819395   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   12   0.77660394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.77635014   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.77622414   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   15   0.7762232   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.77621675   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7757708   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.77557445   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7754345   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7737278   REFERENCES:SIMDATES:SIMLOC
