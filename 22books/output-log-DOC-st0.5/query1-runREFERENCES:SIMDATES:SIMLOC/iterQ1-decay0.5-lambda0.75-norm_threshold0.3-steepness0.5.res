###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 9.229, activation diff: 3.111, ratio: 0.337
#   pulse 3: activated nodes: 7566, borderline nodes: 7245, overall activation: 12.609, activation diff: 4.830, ratio: 0.383
#   pulse 4: activated nodes: 7939, borderline nodes: 6771, overall activation: 18.520, activation diff: 7.129, ratio: 0.385
#   pulse 5: activated nodes: 8523, borderline nodes: 7009, overall activation: 31.360, activation diff: 13.820, ratio: 0.441
#   pulse 6: activated nodes: 8885, borderline nodes: 6706, overall activation: 55.327, activation diff: 24.573, ratio: 0.444
#   pulse 7: activated nodes: 9316, borderline nodes: 6471, overall activation: 93.630, activation diff: 38.566, ratio: 0.412
#   pulse 8: activated nodes: 9853, borderline nodes: 6140, overall activation: 147.484, activation diff: 53.948, ratio: 0.366
#   pulse 9: activated nodes: 10243, borderline nodes: 5749, overall activation: 216.429, activation diff: 68.960, ratio: 0.319
#   pulse 10: activated nodes: 10515, borderline nodes: 5414, overall activation: 299.796, activation diff: 83.368, ratio: 0.278
#   pulse 11: activated nodes: 10679, borderline nodes: 5086, overall activation: 396.983, activation diff: 97.186, ratio: 0.245
#   pulse 12: activated nodes: 10756, borderline nodes: 4766, overall activation: 504.168, activation diff: 107.186, ratio: 0.213
#   pulse 13: activated nodes: 10798, borderline nodes: 4509, overall activation: 615.851, activation diff: 111.683, ratio: 0.181
#   pulse 14: activated nodes: 10843, borderline nodes: 4242, overall activation: 727.986, activation diff: 112.135, ratio: 0.154
#   pulse 15: activated nodes: 10872, borderline nodes: 4004, overall activation: 838.159, activation diff: 110.173, ratio: 0.131

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10872
#   final overall activation: 838.2
#   number of spread. activ. pulses: 15
#   running time: 1761

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.80866885   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.5944442   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.551031   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   4   0.49210662   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   5   0.47862446   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   6   0.37212095   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   7   0.35508597   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   8   0.35343748   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.3517001   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   10   0.34509438   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   11   0.33609363   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.33303136   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.3268857   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.32657003   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   15   0.32413167   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   16   0.3236163   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.32353726   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.31951317   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.3163664   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   20   0.31625906   REFERENCES:SIMDATES:SIMLOC
