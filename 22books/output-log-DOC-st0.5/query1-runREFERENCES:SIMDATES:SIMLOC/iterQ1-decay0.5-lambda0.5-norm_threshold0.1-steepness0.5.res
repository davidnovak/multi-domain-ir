###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 67.803, activation diff: 57.905, ratio: 0.854
#   pulse 3: activated nodes: 9190, borderline nodes: 4794, overall activation: 121.410, activation diff: 54.489, ratio: 0.449
#   pulse 4: activated nodes: 10578, borderline nodes: 5711, overall activation: 323.257, activation diff: 201.913, ratio: 0.625
#   pulse 5: activated nodes: 10787, borderline nodes: 4350, overall activation: 581.772, activation diff: 258.515, ratio: 0.444
#   pulse 6: activated nodes: 10887, borderline nodes: 3891, overall activation: 872.069, activation diff: 290.297, ratio: 0.333
#   pulse 7: activated nodes: 10944, borderline nodes: 3650, overall activation: 1148.342, activation diff: 276.273, ratio: 0.241
#   pulse 8: activated nodes: 10959, borderline nodes: 3579, overall activation: 1392.998, activation diff: 244.655, ratio: 0.176
#   pulse 9: activated nodes: 10962, borderline nodes: 3540, overall activation: 1595.181, activation diff: 202.183, ratio: 0.127
#   pulse 10: activated nodes: 10965, borderline nodes: 3532, overall activation: 1749.199, activation diff: 154.018, ratio: 0.088
#   pulse 11: activated nodes: 10967, borderline nodes: 3526, overall activation: 1858.715, activation diff: 109.516, ratio: 0.059
#   pulse 12: activated nodes: 10968, borderline nodes: 3525, overall activation: 1933.043, activation diff: 74.328, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 1933.0
#   number of spread. activ. pulses: 12
#   running time: 1594

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9635661   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85340035   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.83557785   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7798871   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.6538563   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.61583555   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48410338   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4650798   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4547961   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.44838846   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4459869   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.44543225   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.43921137   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.43579417   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.43449372   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   16   0.43252394   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.43192592   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.43057305   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   19   0.42898858   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   20   0.42848513   REFERENCES:SIMDATES:SIMLOC
