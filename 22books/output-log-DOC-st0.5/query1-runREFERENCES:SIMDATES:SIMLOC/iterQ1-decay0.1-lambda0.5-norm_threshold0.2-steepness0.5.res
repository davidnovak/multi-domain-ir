###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 68.438, activation diff: 60.058, ratio: 0.878
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 130.794, activation diff: 63.480, ratio: 0.485
#   pulse 4: activated nodes: 10652, borderline nodes: 7482, overall activation: 523.068, activation diff: 392.372, ratio: 0.750
#   pulse 5: activated nodes: 11086, borderline nodes: 4371, overall activation: 1172.817, activation diff: 649.750, ratio: 0.554
#   pulse 6: activated nodes: 11319, borderline nodes: 2723, overall activation: 2225.818, activation diff: 1053.001, ratio: 0.473
#   pulse 7: activated nodes: 11406, borderline nodes: 947, overall activation: 3370.096, activation diff: 1144.277, ratio: 0.340
#   pulse 8: activated nodes: 11433, borderline nodes: 419, overall activation: 4342.788, activation diff: 972.692, ratio: 0.224
#   pulse 9: activated nodes: 11449, borderline nodes: 180, overall activation: 5043.945, activation diff: 701.157, ratio: 0.139
#   pulse 10: activated nodes: 11452, borderline nodes: 109, overall activation: 5509.206, activation diff: 465.261, ratio: 0.084
#   pulse 11: activated nodes: 11453, borderline nodes: 79, overall activation: 5806.073, activation diff: 296.867, ratio: 0.051
#   pulse 12: activated nodes: 11455, borderline nodes: 66, overall activation: 5991.980, activation diff: 185.908, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5992.0
#   number of spread. activ. pulses: 12
#   running time: 1557

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9649843   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8941551   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89027566   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8887601   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   5   0.8870955   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.8870348   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   7   0.8865298   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   8   0.8863709   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.88635415   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   10   0.88626945   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.8859646   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8859372   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.8857894   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.88571036   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.8854396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.8845326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8840109   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.88370156   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.88349223   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.88328576   REFERENCES:SIMDATES:SIMLOC
