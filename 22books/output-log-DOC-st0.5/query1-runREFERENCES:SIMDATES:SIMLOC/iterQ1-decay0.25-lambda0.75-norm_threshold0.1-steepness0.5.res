###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 23.517, activation diff: 16.497, ratio: 0.701
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 53.203, activation diff: 30.965, ratio: 0.582
#   pulse 4: activated nodes: 10532, borderline nodes: 6553, overall activation: 124.909, activation diff: 72.401, ratio: 0.580
#   pulse 5: activated nodes: 11032, borderline nodes: 5295, overall activation: 253.232, activation diff: 128.611, ratio: 0.508
#   pulse 6: activated nodes: 11214, borderline nodes: 4078, overall activation: 451.884, activation diff: 198.695, ratio: 0.440
#   pulse 7: activated nodes: 11324, borderline nodes: 2554, overall activation: 726.889, activation diff: 275.005, ratio: 0.378
#   pulse 8: activated nodes: 11394, borderline nodes: 1402, overall activation: 1064.626, activation diff: 337.738, ratio: 0.317
#   pulse 9: activated nodes: 11411, borderline nodes: 822, overall activation: 1444.322, activation diff: 379.695, ratio: 0.263
#   pulse 10: activated nodes: 11426, borderline nodes: 510, overall activation: 1838.629, activation diff: 394.307, ratio: 0.214
#   pulse 11: activated nodes: 11434, borderline nodes: 361, overall activation: 2222.293, activation diff: 383.664, ratio: 0.173
#   pulse 12: activated nodes: 11437, borderline nodes: 245, overall activation: 2576.885, activation diff: 354.592, ratio: 0.138
#   pulse 13: activated nodes: 11444, borderline nodes: 189, overall activation: 2891.904, activation diff: 315.020, ratio: 0.109
#   pulse 14: activated nodes: 11447, borderline nodes: 163, overall activation: 3164.379, activation diff: 272.474, ratio: 0.086
#   pulse 15: activated nodes: 11448, borderline nodes: 149, overall activation: 3395.951, activation diff: 231.572, ratio: 0.068

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11448
#   final overall activation: 3396.0
#   number of spread. activ. pulses: 15
#   running time: 1685

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91254896   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.76181144   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.7609143   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7042863   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.6927829   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.6677389   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   7   0.6636295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.6527818   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   9   0.6525283   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.65176266   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.65148985   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   12   0.6506427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.6492536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.6477251   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   15   0.64737827   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.64462703   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.64284027   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.64062756   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.6405717   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.6402761   REFERENCES:SIMDATES:SIMLOC
