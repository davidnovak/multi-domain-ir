###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 604.356, activation diff: 620.952, ratio: 1.027
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 618.902, activation diff: 1057.438, ratio: 1.709
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 3612.336, activation diff: 3329.103, ratio: 0.922
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4218.427, activation diff: 1436.087, ratio: 0.340
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5161.332, activation diff: 967.645, ratio: 0.187
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5285.184, activation diff: 129.734, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5285.2
#   number of spread. activ. pulses: 7
#   running time: 1627

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9728142   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.87464494   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8715557   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.8028626   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.79685205   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7932326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7905319   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.7897824   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.78879184   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.7886378   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.78804284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.78774184   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7873664   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.78715414   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7868492   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7859117   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.7856516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.78539866   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.78511214   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.7850479   REFERENCES:SIMDATES:SIMLOC
