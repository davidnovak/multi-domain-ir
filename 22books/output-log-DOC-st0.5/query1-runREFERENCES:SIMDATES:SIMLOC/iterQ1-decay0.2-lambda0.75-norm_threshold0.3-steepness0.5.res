###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 9.403, activation diff: 3.285, ratio: 0.349
#   pulse 3: activated nodes: 7566, borderline nodes: 7245, overall activation: 14.949, activation diff: 6.996, ratio: 0.468
#   pulse 4: activated nodes: 7939, borderline nodes: 6771, overall activation: 25.398, activation diff: 11.616, ratio: 0.457
#   pulse 5: activated nodes: 8615, borderline nodes: 7039, overall activation: 52.167, activation diff: 27.623, ratio: 0.530
#   pulse 6: activated nodes: 9438, borderline nodes: 7000, overall activation: 104.585, activation diff: 52.852, ratio: 0.505
#   pulse 7: activated nodes: 10353, borderline nodes: 6960, overall activation: 193.828, activation diff: 89.382, ratio: 0.461
#   pulse 8: activated nodes: 10809, borderline nodes: 6115, overall activation: 331.364, activation diff: 137.571, ratio: 0.415
#   pulse 9: activated nodes: 11080, borderline nodes: 5181, overall activation: 530.604, activation diff: 199.239, ratio: 0.375
#   pulse 10: activated nodes: 11211, borderline nodes: 4111, overall activation: 800.196, activation diff: 269.592, ratio: 0.337
#   pulse 11: activated nodes: 11292, borderline nodes: 2919, overall activation: 1130.908, activation diff: 330.712, ratio: 0.292
#   pulse 12: activated nodes: 11364, borderline nodes: 1974, overall activation: 1508.536, activation diff: 377.628, ratio: 0.250
#   pulse 13: activated nodes: 11387, borderline nodes: 1352, overall activation: 1909.312, activation diff: 400.775, ratio: 0.210
#   pulse 14: activated nodes: 11401, borderline nodes: 948, overall activation: 2308.632, activation diff: 399.320, ratio: 0.173
#   pulse 15: activated nodes: 11413, borderline nodes: 713, overall activation: 2687.523, activation diff: 378.891, ratio: 0.141

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11413
#   final overall activation: 2687.5
#   number of spread. activ. pulses: 15
#   running time: 1690

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8500132   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.687895   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.666439   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   4   0.6559091   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   5   0.64269763   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   6   0.6324785   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   7   0.62663186   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   8   0.624619   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   9   0.6242178   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   10   0.62386036   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.6222817   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   12   0.62127376   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.6203036   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_14   14   0.6160886   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.61257726   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   16   0.6125685   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   17   0.61227846   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   18   0.6112362   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   19   0.6099739   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   20   0.60949886   REFERENCES:SIMDATES:SIMLOC
