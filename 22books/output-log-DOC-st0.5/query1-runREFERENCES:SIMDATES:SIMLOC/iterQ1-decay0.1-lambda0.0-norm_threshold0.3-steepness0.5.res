###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 278.154, activation diff: 293.084, ratio: 1.054
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 206.801, activation diff: 483.296, ratio: 2.337
#   pulse 4: activated nodes: 11025, borderline nodes: 7181, overall activation: 2462.784, activation diff: 2594.538, ratio: 1.053
#   pulse 5: activated nodes: 11282, borderline nodes: 2351, overall activation: 1721.529, activation diff: 3418.245, ratio: 1.986
#   pulse 6: activated nodes: 11412, borderline nodes: 988, overall activation: 5046.487, activation diff: 4028.896, ratio: 0.798
#   pulse 7: activated nodes: 11438, borderline nodes: 334, overall activation: 5448.087, activation diff: 1131.382, ratio: 0.208
#   pulse 8: activated nodes: 11448, borderline nodes: 146, overall activation: 6018.040, activation diff: 598.077, ratio: 0.099
#   pulse 9: activated nodes: 11452, borderline nodes: 94, overall activation: 6102.221, activation diff: 93.545, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6102.2
#   number of spread. activ. pulses: 9
#   running time: 1788

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9688867   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8976826   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.89686936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.89548755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.8940614   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.8938295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   7   0.8937598   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.8936697   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.89312845   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.89278626   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8926329   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.89261836   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.89229834   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   14   0.8920953   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   15   0.8914403   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   16   0.89139676   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   17   0.8913483   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   18   0.8913011   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.89125735   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   20   0.8910213   REFERENCES:SIMDATES:SIMLOC
