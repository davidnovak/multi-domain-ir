###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 19.121, activation diff: 12.100, ratio: 0.633
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 38.801, activation diff: 20.959, ratio: 0.540
#   pulse 4: activated nodes: 9866, borderline nodes: 6000, overall activation: 79.042, activation diff: 40.972, ratio: 0.518
#   pulse 5: activated nodes: 10392, borderline nodes: 5266, overall activation: 144.607, activation diff: 65.896, ratio: 0.456
#   pulse 6: activated nodes: 10710, borderline nodes: 4768, overall activation: 235.163, activation diff: 90.638, ratio: 0.385
#   pulse 7: activated nodes: 10800, borderline nodes: 4409, overall activation: 348.612, activation diff: 113.453, ratio: 0.325
#   pulse 8: activated nodes: 10866, borderline nodes: 4072, overall activation: 479.867, activation diff: 131.255, ratio: 0.274
#   pulse 9: activated nodes: 10920, borderline nodes: 3800, overall activation: 620.912, activation diff: 141.045, ratio: 0.227
#   pulse 10: activated nodes: 10941, borderline nodes: 3685, overall activation: 765.312, activation diff: 144.400, ratio: 0.189
#   pulse 11: activated nodes: 10952, borderline nodes: 3626, overall activation: 908.764, activation diff: 143.452, ratio: 0.158
#   pulse 12: activated nodes: 10958, borderline nodes: 3593, overall activation: 1048.353, activation diff: 139.589, ratio: 0.133
#   pulse 13: activated nodes: 10959, borderline nodes: 3563, overall activation: 1181.496, activation diff: 133.144, ratio: 0.113
#   pulse 14: activated nodes: 10961, borderline nodes: 3547, overall activation: 1305.687, activation diff: 124.190, ratio: 0.095
#   pulse 15: activated nodes: 10963, borderline nodes: 3539, overall activation: 1418.946, activation diff: 113.260, ratio: 0.080

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10963
#   final overall activation: 1418.9
#   number of spread. activ. pulses: 15
#   running time: 1790

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89866805   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.7325964   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.72398174   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.6804285   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.58140767   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.51000094   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.435299   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   8   0.4053279   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.40492484   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.40204167   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.392086   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.38922662   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.38403192   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.3777886   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.3749261   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.3747145   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.37224045   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   18   0.37139362   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.37132612   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   20   0.37074348   REFERENCES:SIMDATES:SIMLOC
