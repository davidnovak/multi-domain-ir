###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 13.549, activation diff: 6.979, ratio: 0.515
#   pulse 3: activated nodes: 7946, borderline nodes: 6758, overall activation: 28.606, activation diff: 16.421, ratio: 0.574
#   pulse 4: activated nodes: 8848, borderline nodes: 6616, overall activation: 62.753, activation diff: 35.010, ratio: 0.558
#   pulse 5: activated nodes: 10074, borderline nodes: 6956, overall activation: 134.002, activation diff: 71.695, ratio: 0.535
#   pulse 6: activated nodes: 10758, borderline nodes: 6154, overall activation: 257.336, activation diff: 123.495, ratio: 0.480
#   pulse 7: activated nodes: 11096, borderline nodes: 5048, overall activation: 448.136, activation diff: 190.813, ratio: 0.426
#   pulse 8: activated nodes: 11231, borderline nodes: 3841, overall activation: 719.413, activation diff: 271.277, ratio: 0.377
#   pulse 9: activated nodes: 11312, borderline nodes: 2536, overall activation: 1064.846, activation diff: 345.433, ratio: 0.324
#   pulse 10: activated nodes: 11379, borderline nodes: 1538, overall activation: 1466.844, activation diff: 401.997, ratio: 0.274
#   pulse 11: activated nodes: 11404, borderline nodes: 1024, overall activation: 1896.662, activation diff: 429.818, ratio: 0.227
#   pulse 12: activated nodes: 11415, borderline nodes: 686, overall activation: 2324.996, activation diff: 428.334, ratio: 0.184
#   pulse 13: activated nodes: 11426, borderline nodes: 478, overall activation: 2729.384, activation diff: 404.388, ratio: 0.148
#   pulse 14: activated nodes: 11433, borderline nodes: 334, overall activation: 3094.500, activation diff: 365.116, ratio: 0.118
#   pulse 15: activated nodes: 11437, borderline nodes: 256, overall activation: 3413.894, activation diff: 319.394, ratio: 0.094

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 3413.9
#   number of spread. activ. pulses: 15
#   running time: 1785

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.892144   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.7248503   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.72371566   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.7105189   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   5   0.6996491   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.6935072   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   7   0.68097055   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   8   0.67808986   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   9   0.67443806   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   10   0.67407745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.6731014   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   12   0.672747   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.67184913   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   14   0.6715729   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.6710591   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   16   0.6702189   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   17   0.67018   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.66903967   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   19   0.6685021   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   20   0.66822463   REFERENCES:SIMDATES:SIMLOC
