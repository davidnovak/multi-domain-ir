###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 46.716, activation diff: 39.252, ratio: 0.840
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 108.725, activation diff: 63.205, ratio: 0.581
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 249.873, activation diff: 141.700, ratio: 0.567
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 481.885, activation diff: 232.174, ratio: 0.482
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 808.390, activation diff: 326.519, ratio: 0.404
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 1209.563, activation diff: 401.174, ratio: 0.332
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 1646.188, activation diff: 436.625, ratio: 0.265
#   pulse 9: activated nodes: 11464, borderline nodes: 21, overall activation: 2082.053, activation diff: 435.865, ratio: 0.209
#   pulse 10: activated nodes: 11464, borderline nodes: 21, overall activation: 2491.337, activation diff: 409.283, ratio: 0.164
#   pulse 11: activated nodes: 11464, borderline nodes: 21, overall activation: 2858.849, activation diff: 367.513, ratio: 0.129
#   pulse 12: activated nodes: 11464, borderline nodes: 21, overall activation: 3179.150, activation diff: 320.301, ratio: 0.101
#   pulse 13: activated nodes: 11464, borderline nodes: 21, overall activation: 3452.770, activation diff: 273.619, ratio: 0.079
#   pulse 14: activated nodes: 11464, borderline nodes: 21, overall activation: 3683.221, activation diff: 230.451, ratio: 0.063
#   pulse 15: activated nodes: 11464, borderline nodes: 21, overall activation: 3875.294, activation diff: 192.073, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 3875.3
#   number of spread. activ. pulses: 15
#   running time: 1828

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9263613   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.79670084   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.7919481   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7302291   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7033868   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.6831525   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.6770028   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.6745966   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   9   0.67440754   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   10   0.67428565   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.67084205   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.6699561   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.6697062   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.66922426   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.66882   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.66383284   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   17   0.6637776   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   18   0.6617249   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.66126394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.65951645   REFERENCES:SIMDATES:SIMLOC
