###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 188.954, activation diff: 189.350, ratio: 1.002
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 223.433, activation diff: 208.855, ratio: 0.935
#   pulse 4: activated nodes: 11058, borderline nodes: 6713, overall activation: 1511.924, activation diff: 1318.676, ratio: 0.872
#   pulse 5: activated nodes: 11284, borderline nodes: 2385, overall activation: 2747.918, activation diff: 1239.340, ratio: 0.451
#   pulse 6: activated nodes: 11404, borderline nodes: 1041, overall activation: 4426.481, activation diff: 1678.563, ratio: 0.379
#   pulse 7: activated nodes: 11438, borderline nodes: 298, overall activation: 5441.748, activation diff: 1015.267, ratio: 0.187
#   pulse 8: activated nodes: 11451, borderline nodes: 111, overall activation: 5924.878, activation diff: 483.130, ratio: 0.082
#   pulse 9: activated nodes: 11454, borderline nodes: 68, overall activation: 6134.664, activation diff: 209.787, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6134.7
#   number of spread. activ. pulses: 9
#   running time: 1504

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9690918   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89723206   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8955627   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8942326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   5   0.89252764   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   6   0.89249194   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   7   0.8924427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   8   0.8918894   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.891879   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8917444   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.8914949   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.8912799   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8911073   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.89058983   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.89018416   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.8899312   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   17   0.88984394   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.8898255   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   19   0.889707   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.8896009   REFERENCES:SIMDATES:SIMLOC
