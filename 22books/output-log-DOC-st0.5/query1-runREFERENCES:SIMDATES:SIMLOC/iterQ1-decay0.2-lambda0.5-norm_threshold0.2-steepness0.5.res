###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 61.948, activation diff: 53.568, ratio: 0.865
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 115.215, activation diff: 54.402, ratio: 0.472
#   pulse 4: activated nodes: 10626, borderline nodes: 7495, overall activation: 428.734, activation diff: 313.629, ratio: 0.732
#   pulse 5: activated nodes: 11029, borderline nodes: 4611, overall activation: 923.974, activation diff: 495.240, ratio: 0.536
#   pulse 6: activated nodes: 11298, borderline nodes: 3172, overall activation: 1685.953, activation diff: 761.979, ratio: 0.452
#   pulse 7: activated nodes: 11396, borderline nodes: 1264, overall activation: 2538.435, activation diff: 852.482, ratio: 0.336
#   pulse 8: activated nodes: 11418, borderline nodes: 583, overall activation: 3302.946, activation diff: 764.511, ratio: 0.231
#   pulse 9: activated nodes: 11433, borderline nodes: 324, overall activation: 3881.365, activation diff: 578.418, ratio: 0.149
#   pulse 10: activated nodes: 11442, borderline nodes: 192, overall activation: 4276.053, activation diff: 394.688, ratio: 0.092
#   pulse 11: activated nodes: 11449, borderline nodes: 143, overall activation: 4532.683, activation diff: 256.630, ratio: 0.057
#   pulse 12: activated nodes: 11452, borderline nodes: 119, overall activation: 4695.324, activation diff: 162.641, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 4695.3
#   number of spread. activ. pulses: 12
#   running time: 1634

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9641386   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8480927   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8421861   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79265237   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7841859   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.78169763   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.78137326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.7805896   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   9   0.77829826   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.77812004   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   11   0.7779342   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   12   0.77627987   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   13   0.7759191   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.7758052   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   15   0.77576536   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   16   0.77570236   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.7753862   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7751552   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   19   0.7749221   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.7736274   REFERENCES:SIMDATES:SIMLOC
