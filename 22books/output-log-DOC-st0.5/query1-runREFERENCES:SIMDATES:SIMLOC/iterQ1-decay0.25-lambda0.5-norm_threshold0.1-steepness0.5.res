###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 96.062, activation diff: 86.164, ratio: 0.897
#   pulse 3: activated nodes: 9190, borderline nodes: 4794, overall activation: 188.574, activation diff: 93.391, ratio: 0.495
#   pulse 4: activated nodes: 11100, borderline nodes: 5848, overall activation: 609.872, activation diff: 421.346, ratio: 0.691
#   pulse 5: activated nodes: 11296, borderline nodes: 2579, overall activation: 1233.203, activation diff: 623.331, ratio: 0.505
#   pulse 6: activated nodes: 11404, borderline nodes: 1162, overall activation: 2017.922, activation diff: 784.719, ratio: 0.389
#   pulse 7: activated nodes: 11430, borderline nodes: 457, overall activation: 2763.307, activation diff: 745.385, ratio: 0.270
#   pulse 8: activated nodes: 11438, borderline nodes: 237, overall activation: 3351.916, activation diff: 588.609, ratio: 0.176
#   pulse 9: activated nodes: 11447, borderline nodes: 159, overall activation: 3762.674, activation diff: 410.758, ratio: 0.109
#   pulse 10: activated nodes: 11449, borderline nodes: 130, overall activation: 4032.320, activation diff: 269.646, ratio: 0.067
#   pulse 11: activated nodes: 11450, borderline nodes: 110, overall activation: 4203.859, activation diff: 171.539, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 4203.9
#   number of spread. activ. pulses: 11
#   running time: 1564

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96313393   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85100555   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84394866   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7784407   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7402729   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.72781277   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.72708255   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7256805   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7242346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7212509   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.72038805   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   12   0.7190729   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.7190559   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.71679306   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7162111   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   16   0.716028   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   17   0.7158772   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.71576613   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7150665   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   20   0.71488047   REFERENCES:SIMDATES:SIMLOC
