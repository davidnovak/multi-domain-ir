###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 230.564, activation diff: 228.959, ratio: 0.993
#   pulse 3: activated nodes: 9366, borderline nodes: 4295, overall activation: 304.096, activation diff: 211.806, ratio: 0.697
#   pulse 4: activated nodes: 11258, borderline nodes: 4849, overall activation: 1444.463, activation diff: 1145.274, ratio: 0.793
#   pulse 5: activated nodes: 11383, borderline nodes: 1399, overall activation: 2471.093, activation diff: 1026.657, ratio: 0.415
#   pulse 6: activated nodes: 11429, borderline nodes: 490, overall activation: 3477.926, activation diff: 1006.833, ratio: 0.289
#   pulse 7: activated nodes: 11446, borderline nodes: 182, overall activation: 4040.376, activation diff: 562.450, ratio: 0.139
#   pulse 8: activated nodes: 11449, borderline nodes: 119, overall activation: 4301.834, activation diff: 261.458, ratio: 0.061
#   pulse 9: activated nodes: 11450, borderline nodes: 98, overall activation: 4416.035, activation diff: 114.201, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11450
#   final overall activation: 4416.0
#   number of spread. activ. pulses: 9
#   running time: 1673

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97049296   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8661933   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.86153066   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7919653   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.74542755   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7374661   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.73572123   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7352825   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.73420686   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.73174024   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7303345   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.7296091   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7291376   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.72900176   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7287096   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.7285925   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.72788745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7271694   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   19   0.7269168   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7256739   REFERENCES:SIMDATES:SIMLOC
