###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 158.622, activation diff: 159.017, ratio: 1.002
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 171.536, activation diff: 158.784, ratio: 0.926
#   pulse 4: activated nodes: 11025, borderline nodes: 6744, overall activation: 1024.313, activation diff: 876.786, ratio: 0.856
#   pulse 5: activated nodes: 11249, borderline nodes: 2874, overall activation: 1768.139, activation diff: 747.049, ratio: 0.423
#   pulse 6: activated nodes: 11364, borderline nodes: 1641, overall activation: 2874.580, activation diff: 1106.441, ratio: 0.385
#   pulse 7: activated nodes: 11421, borderline nodes: 541, overall activation: 3626.816, activation diff: 752.236, ratio: 0.207
#   pulse 8: activated nodes: 11433, borderline nodes: 273, overall activation: 4025.393, activation diff: 398.577, ratio: 0.099
#   pulse 9: activated nodes: 11436, borderline nodes: 188, overall activation: 4208.722, activation diff: 183.329, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11436
#   final overall activation: 4208.7
#   number of spread. activ. pulses: 9
#   running time: 1672

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9681743   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8579429   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8493772   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.78109026   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.744865   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.73531365   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   7   0.7339295   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7338573   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7321868   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7294866   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7280742   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   12   0.7266401   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   13   0.72637814   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.7263703   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   15   0.7254692   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   16   0.72546434   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.72435236   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.7242813   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   19   0.7241952   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7234471   REFERENCES:SIMDATES:SIMLOC
