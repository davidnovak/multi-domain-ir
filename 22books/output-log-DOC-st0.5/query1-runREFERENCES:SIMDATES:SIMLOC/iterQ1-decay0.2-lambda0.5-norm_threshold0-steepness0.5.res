###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 164.403, activation diff: 153.243, ratio: 0.932
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 358.932, activation diff: 195.169, ratio: 0.544
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1079.599, activation diff: 720.671, ratio: 0.668
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 2064.209, activation diff: 984.610, ratio: 0.477
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 3063.020, activation diff: 998.811, ratio: 0.326
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 3852.408, activation diff: 789.388, ratio: 0.205
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 4400.227, activation diff: 547.819, ratio: 0.124
#   pulse 9: activated nodes: 11464, borderline nodes: 0, overall activation: 4757.210, activation diff: 356.983, ratio: 0.075
#   pulse 10: activated nodes: 11464, borderline nodes: 0, overall activation: 4982.226, activation diff: 225.016, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4982.2
#   number of spread. activ. pulses: 10
#   running time: 1510

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9613917   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85376996   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8479794   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.788553   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   5   0.78259516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7798176   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   7   0.777058   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7762346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7760427   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.773656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7734599   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.77333367   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7719743   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.77180254   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   15   0.77138543   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.7713826   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.77138215   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   18   0.770771   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.77042633   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   20   0.7703277   REFERENCES:SIMDATES:SIMLOC
