###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 316.891, activation diff: 332.851, ratio: 1.050
#   pulse 3: activated nodes: 9141, borderline nodes: 5079, overall activation: 213.403, activation diff: 524.374, ratio: 2.457
#   pulse 4: activated nodes: 11178, borderline nodes: 6065, overall activation: 2044.552, activation diff: 2155.080, ratio: 1.054
#   pulse 5: activated nodes: 11350, borderline nodes: 1750, overall activation: 1460.528, activation diff: 2499.973, ratio: 1.712
#   pulse 6: activated nodes: 11422, borderline nodes: 721, overall activation: 3721.080, activation diff: 2663.600, ratio: 0.716
#   pulse 7: activated nodes: 11437, borderline nodes: 277, overall activation: 4011.443, activation diff: 603.270, ratio: 0.150
#   pulse 8: activated nodes: 11443, borderline nodes: 192, overall activation: 4293.932, activation diff: 294.059, ratio: 0.068
#   pulse 9: activated nodes: 11447, borderline nodes: 153, overall activation: 4342.023, activation diff: 51.644, ratio: 0.012

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11447
#   final overall activation: 4342.0
#   number of spread. activ. pulses: 9
#   running time: 1480

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96992415   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8623706   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8593667   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7843532   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7456517   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7383472   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7363399   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7353796   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.73474467   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.73230696   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.73067486   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.73054385   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.7303887   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.7294308   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.7292108   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.72900057   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.728392   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7278913   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   19   0.72765434   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7264217   REFERENCES:SIMDATES:SIMLOC
