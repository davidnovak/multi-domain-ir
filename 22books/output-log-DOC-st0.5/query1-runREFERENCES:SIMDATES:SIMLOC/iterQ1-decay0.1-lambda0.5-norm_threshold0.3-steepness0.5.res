###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 38.815, activation diff: 32.056, ratio: 0.826
#   pulse 3: activated nodes: 8124, borderline nodes: 6421, overall activation: 67.510, activation diff: 30.554, ratio: 0.453
#   pulse 4: activated nodes: 9786, borderline nodes: 7879, overall activation: 297.201, activation diff: 230.203, ratio: 0.775
#   pulse 5: activated nodes: 10606, borderline nodes: 5817, overall activation: 690.939, activation diff: 393.737, ratio: 0.570
#   pulse 6: activated nodes: 11165, borderline nodes: 4777, overall activation: 1459.123, activation diff: 768.185, ratio: 0.526
#   pulse 7: activated nodes: 11318, borderline nodes: 2398, overall activation: 2487.149, activation diff: 1028.026, ratio: 0.413
#   pulse 8: activated nodes: 11401, borderline nodes: 1096, overall activation: 3536.320, activation diff: 1049.171, ratio: 0.297
#   pulse 9: activated nodes: 11422, borderline nodes: 537, overall activation: 4404.830, activation diff: 868.509, ratio: 0.197
#   pulse 10: activated nodes: 11440, borderline nodes: 276, overall activation: 5026.471, activation diff: 621.642, ratio: 0.124
#   pulse 11: activated nodes: 11446, borderline nodes: 168, overall activation: 5439.909, activation diff: 413.437, ratio: 0.076
#   pulse 12: activated nodes: 11452, borderline nodes: 125, overall activation: 5705.145, activation diff: 265.236, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 5705.1
#   number of spread. activ. pulses: 12
#   running time: 1689

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96059906   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.892225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8863096   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8845397   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   5   0.8826884   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   6   0.8825275   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.8825027   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   8   0.8824636   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.88230824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.8820938   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   11   0.88193905   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   12   0.8818276   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.88148576   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.8810638   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.88010406   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   16   0.879745   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.87965477   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   18   0.8792287   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.87815773   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.8779025   REFERENCES:SIMDATES:SIMLOC
