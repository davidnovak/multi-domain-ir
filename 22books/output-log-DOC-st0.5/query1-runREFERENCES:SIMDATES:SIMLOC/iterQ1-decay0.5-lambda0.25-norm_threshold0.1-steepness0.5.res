###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 156.589, activation diff: 154.983, ratio: 0.990
#   pulse 3: activated nodes: 9366, borderline nodes: 4295, overall activation: 173.450, activation diff: 110.744, ratio: 0.638
#   pulse 4: activated nodes: 10714, borderline nodes: 5049, overall activation: 671.882, activation diff: 500.703, ratio: 0.745
#   pulse 5: activated nodes: 10917, borderline nodes: 3861, overall activation: 1055.086, activation diff: 383.208, ratio: 0.363
#   pulse 6: activated nodes: 10947, borderline nodes: 3659, overall activation: 1440.688, activation diff: 385.602, ratio: 0.268
#   pulse 7: activated nodes: 10961, borderline nodes: 3550, overall activation: 1718.555, activation diff: 277.867, ratio: 0.162
#   pulse 8: activated nodes: 10965, borderline nodes: 3531, overall activation: 1892.014, activation diff: 173.459, ratio: 0.092
#   pulse 9: activated nodes: 10968, borderline nodes: 3526, overall activation: 1984.692, activation diff: 92.678, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 1984.7
#   number of spread. activ. pulses: 9
#   running time: 1465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96801805   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86406094   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8447728   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7890111   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.66110474   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.62666523   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4870995   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.46919298   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.4582665   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.45242247   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.4514485   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.45001423   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.44328   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.4428496   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.4387921   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   16   0.4385066   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.4378677   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.43603194   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.43528506   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   20   0.43437025   REFERENCES:SIMDATES:SIMLOC
