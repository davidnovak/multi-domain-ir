###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 113.017, activation diff: 103.119, ratio: 0.912
#   pulse 3: activated nodes: 9190, borderline nodes: 4794, overall activation: 233.391, activation diff: 121.252, ratio: 0.520
#   pulse 4: activated nodes: 11139, borderline nodes: 5797, overall activation: 854.829, activation diff: 621.476, ratio: 0.727
#   pulse 5: activated nodes: 11319, borderline nodes: 2091, overall activation: 1842.978, activation diff: 988.149, ratio: 0.536
#   pulse 6: activated nodes: 11417, borderline nodes: 829, overall activation: 3077.510, activation diff: 1234.532, ratio: 0.401
#   pulse 7: activated nodes: 11446, borderline nodes: 264, overall activation: 4178.811, activation diff: 1101.301, ratio: 0.264
#   pulse 8: activated nodes: 11453, borderline nodes: 103, overall activation: 4992.220, activation diff: 813.409, ratio: 0.163
#   pulse 9: activated nodes: 11455, borderline nodes: 63, overall activation: 5535.951, activation diff: 543.732, ratio: 0.098
#   pulse 10: activated nodes: 11455, borderline nodes: 43, overall activation: 5882.906, activation diff: 346.954, ratio: 0.059
#   pulse 11: activated nodes: 11457, borderline nodes: 29, overall activation: 6099.403, activation diff: 216.497, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 6099.4
#   number of spread. activ. pulses: 11
#   running time: 1686

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96442485   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.89266026   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8886977   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.88716996   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   5   0.88535905   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.8850705   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   7   0.88490665   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   8   0.88471484   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8846078   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.8845097   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   11   0.88442016   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   12   0.8841021   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.8840724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   14   0.88380516   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.88356304   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.88292927   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   17   0.8822622   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.88197047   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   19   0.8816272   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.88149416   REFERENCES:SIMDATES:SIMLOC
