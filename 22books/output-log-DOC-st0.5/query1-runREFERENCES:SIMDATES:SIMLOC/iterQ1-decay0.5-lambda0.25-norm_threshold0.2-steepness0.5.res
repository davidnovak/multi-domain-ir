###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 108.068, activation diff: 108.463, ratio: 1.004
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 100.260, activation diff: 90.337, ratio: 0.901
#   pulse 4: activated nodes: 10415, borderline nodes: 6474, overall activation: 495.655, activation diff: 408.147, ratio: 0.823
#   pulse 5: activated nodes: 10690, borderline nodes: 4563, overall activation: 781.154, activation diff: 288.476, ratio: 0.369
#   pulse 6: activated nodes: 10838, borderline nodes: 4300, overall activation: 1167.308, activation diff: 386.154, ratio: 0.331
#   pulse 7: activated nodes: 10937, borderline nodes: 3708, overall activation: 1462.301, activation diff: 294.993, ratio: 0.202
#   pulse 8: activated nodes: 10955, borderline nodes: 3618, overall activation: 1690.616, activation diff: 228.316, ratio: 0.135
#   pulse 9: activated nodes: 10959, borderline nodes: 3571, overall activation: 1839.301, activation diff: 148.684, ratio: 0.081
#   pulse 10: activated nodes: 10961, borderline nodes: 3554, overall activation: 1921.642, activation diff: 82.341, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10961
#   final overall activation: 1921.6
#   number of spread. activ. pulses: 10
#   running time: 1578

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96687406   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85929394   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8375368   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7809428   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.647617   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.6128489   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.486853   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.4682799   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.45657736   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   10   0.45123968   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.44947422   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.44819197   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.44105294   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   14   0.44097227   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   15   0.436934   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4365448   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.43574214   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   18   0.43386495   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   19   0.4325184   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   20   0.4321029   REFERENCES:SIMDATES:SIMLOC
