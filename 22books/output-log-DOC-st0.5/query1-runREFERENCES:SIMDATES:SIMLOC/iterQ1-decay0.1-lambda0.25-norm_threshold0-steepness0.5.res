###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 392.778, activation diff: 389.194, ratio: 0.991
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 700.758, activation diff: 403.293, ratio: 0.576
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2885.289, activation diff: 2184.550, ratio: 0.757
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 4665.467, activation diff: 1780.179, ratio: 0.382
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 5773.897, activation diff: 1108.429, ratio: 0.192
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 6280.027, activation diff: 506.130, ratio: 0.081
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 6493.461, activation diff: 213.434, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6493.5
#   number of spread. activ. pulses: 8
#   running time: 1530

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9714532   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.8970883   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   3   0.8959604   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   4   0.8948041   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   5   0.89330685   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   6   0.8929792   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.89296544   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   8   0.89251816   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   9   0.8924968   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   10   0.8923259   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.89202094   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   12   0.89192975   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   13   0.8916812   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.89117396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_506   15   0.89104515   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_353   16   0.8907586   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   17   0.89068556   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   18   0.89063954   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.89058006   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_513   20   0.8905382   REFERENCES:SIMDATES:SIMLOC
