###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 26.155, activation diff: 19.135, ratio: 0.732
#   pulse 3: activated nodes: 8679, borderline nodes: 5777, overall activation: 62.095, activation diff: 37.218, ratio: 0.599
#   pulse 4: activated nodes: 10564, borderline nodes: 6536, overall activation: 156.790, activation diff: 95.374, ratio: 0.608
#   pulse 5: activated nodes: 11071, borderline nodes: 5018, overall activation: 337.298, activation diff: 180.776, ratio: 0.536
#   pulse 6: activated nodes: 11256, borderline nodes: 3530, overall activation: 639.745, activation diff: 302.476, ratio: 0.473
#   pulse 7: activated nodes: 11371, borderline nodes: 1873, overall activation: 1082.149, activation diff: 442.404, ratio: 0.409
#   pulse 8: activated nodes: 11412, borderline nodes: 985, overall activation: 1637.757, activation diff: 555.608, ratio: 0.339
#   pulse 9: activated nodes: 11431, borderline nodes: 484, overall activation: 2244.132, activation diff: 606.375, ratio: 0.270
#   pulse 10: activated nodes: 11443, borderline nodes: 282, overall activation: 2850.781, activation diff: 606.649, ratio: 0.213
#   pulse 11: activated nodes: 11449, borderline nodes: 154, overall activation: 3418.530, activation diff: 567.749, ratio: 0.166
#   pulse 12: activated nodes: 11453, borderline nodes: 102, overall activation: 3925.170, activation diff: 506.640, ratio: 0.129
#   pulse 13: activated nodes: 11453, borderline nodes: 76, overall activation: 4363.809, activation diff: 438.638, ratio: 0.101
#   pulse 14: activated nodes: 11455, borderline nodes: 63, overall activation: 4736.170, activation diff: 372.361, ratio: 0.079
#   pulse 15: activated nodes: 11455, borderline nodes: 57, overall activation: 5048.002, activation diff: 311.832, ratio: 0.062

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5048.0
#   number of spread. activ. pulses: 15
#   running time: 1834

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9170554   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   2   0.84345245   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   3   0.82424676   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   4   0.82260346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   5   0.8221525   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.8221438   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   7   0.8182304   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   8   0.81818604   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   9   0.8179082   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   10   0.8174035   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   11   0.817029   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.81637573   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   13   0.81510395   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.8145859   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.8144078   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_575   16   0.8140983   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   17   0.8136209   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.81133753   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   19   0.81074655   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.809945   REFERENCES:SIMDATES:SIMLOC
