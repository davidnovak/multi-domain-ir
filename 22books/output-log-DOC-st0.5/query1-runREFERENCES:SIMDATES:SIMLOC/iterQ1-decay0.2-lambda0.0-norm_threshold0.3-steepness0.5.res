###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 247.481, activation diff: 262.411, ratio: 1.060
#   pulse 3: activated nodes: 8796, borderline nodes: 5655, overall activation: 165.900, activation diff: 412.094, ratio: 2.484
#   pulse 4: activated nodes: 11006, borderline nodes: 7195, overall activation: 1884.473, activation diff: 2002.105, ratio: 1.062
#   pulse 5: activated nodes: 11263, borderline nodes: 2610, overall activation: 1175.230, activation diff: 2655.649, ratio: 2.260
#   pulse 6: activated nodes: 11372, borderline nodes: 1309, overall activation: 3776.112, activation diff: 3284.391, ratio: 0.870
#   pulse 7: activated nodes: 11423, borderline nodes: 485, overall activation: 3879.225, activation diff: 1328.114, ratio: 0.342
#   pulse 8: activated nodes: 11441, borderline nodes: 261, overall activation: 4679.036, activation diff: 866.978, ratio: 0.185
#   pulse 9: activated nodes: 11445, borderline nodes: 188, overall activation: 4779.084, activation diff: 121.812, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11445
#   final overall activation: 4779.1
#   number of spread. activ. pulses: 9
#   running time: 1514

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9679447   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85581785   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8505737   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.79630387   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7920442   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.7888423   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.7880904   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7868492   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.7861249   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7858655   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.78515255   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   12   0.7849969   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7848998   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.7838838   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   15   0.7832211   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.78320354   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   17   0.7830531   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.782418   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   19   0.78233117   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_127   20   0.7820291   REFERENCES:SIMDATES:SIMLOC
