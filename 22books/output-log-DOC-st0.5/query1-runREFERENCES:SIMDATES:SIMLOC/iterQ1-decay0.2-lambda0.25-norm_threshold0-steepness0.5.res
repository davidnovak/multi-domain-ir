###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 350.281, activation diff: 346.697, ratio: 0.990
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 587.858, activation diff: 324.502, ratio: 0.552
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 2262.946, activation diff: 1675.116, ratio: 0.740
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3659.617, activation diff: 1396.671, ratio: 0.382
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 4581.585, activation diff: 921.968, ratio: 0.201
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 5017.391, activation diff: 435.807, ratio: 0.087
#   pulse 8: activated nodes: 11464, borderline nodes: 0, overall activation: 5204.462, activation diff: 187.070, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 5204.5
#   number of spread. activ. pulses: 8
#   running time: 1477

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9709972   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.87084746   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8666842   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.79933274   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.795858   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7913569   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7884253   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   8   0.7877671   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   9   0.7871443   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.78649104   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   11   0.7858832   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.7852768   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   13   0.78494084   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   14   0.78484225   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7839652   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   16   0.7838776   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.7833713   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   18   0.78336823   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   19   0.7829318   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.7828603   REFERENCES:SIMDATES:SIMLOC
