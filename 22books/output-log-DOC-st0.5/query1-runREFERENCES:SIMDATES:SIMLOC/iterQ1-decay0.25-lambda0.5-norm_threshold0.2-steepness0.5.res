###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 58.703, activation diff: 50.323, ratio: 0.857
#   pulse 3: activated nodes: 8650, borderline nodes: 5892, overall activation: 107.678, activation diff: 50.118, ratio: 0.465
#   pulse 4: activated nodes: 10604, borderline nodes: 7486, overall activation: 386.483, activation diff: 278.921, ratio: 0.722
#   pulse 5: activated nodes: 10978, borderline nodes: 4691, overall activation: 815.903, activation diff: 429.420, ratio: 0.526
#   pulse 6: activated nodes: 11285, borderline nodes: 3479, overall activation: 1456.874, activation diff: 640.971, ratio: 0.440
#   pulse 7: activated nodes: 11387, borderline nodes: 1420, overall activation: 2171.675, activation diff: 714.801, ratio: 0.329
#   pulse 8: activated nodes: 11414, borderline nodes: 733, overall activation: 2833.091, activation diff: 661.416, ratio: 0.233
#   pulse 9: activated nodes: 11427, borderline nodes: 427, overall activation: 3349.599, activation diff: 516.508, ratio: 0.154
#   pulse 10: activated nodes: 11435, borderline nodes: 267, overall activation: 3708.762, activation diff: 359.163, ratio: 0.097
#   pulse 11: activated nodes: 11441, borderline nodes: 206, overall activation: 3945.004, activation diff: 236.242, ratio: 0.060
#   pulse 12: activated nodes: 11442, borderline nodes: 183, overall activation: 4096.274, activation diff: 151.270, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11442
#   final overall activation: 4096.3
#   number of spread. activ. pulses: 12
#   running time: 1697

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9636117   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8471694   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8395654   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.77224076   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.74161017   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   6   0.72914326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   7   0.72851115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   8   0.7272737   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.72560346   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.7227092   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7217245   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   12   0.72022027   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7201936   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   14   0.7178084   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7176205   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.71744096   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   17   0.71693355   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   18   0.7167311   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.7162392   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   20   0.7160333   REFERENCES:SIMDATES:SIMLOC
