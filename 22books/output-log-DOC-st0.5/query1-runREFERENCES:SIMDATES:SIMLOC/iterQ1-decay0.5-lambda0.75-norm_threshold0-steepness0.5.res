###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 34.814, activation diff: 27.350, ratio: 0.786
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 73.400, activation diff: 39.781, ratio: 0.542
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 145.882, activation diff: 73.076, ratio: 0.501
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 250.744, activation diff: 105.075, ratio: 0.419
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 384.039, activation diff: 133.319, ratio: 0.347
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 536.561, activation diff: 152.522, ratio: 0.284
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 698.862, activation diff: 162.301, ratio: 0.232
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 863.590, activation diff: 164.728, ratio: 0.191
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 1024.927, activation diff: 161.337, ratio: 0.157
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 1178.080, activation diff: 153.153, ratio: 0.130
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 1319.418, activation diff: 141.337, ratio: 0.107
#   pulse 13: activated nodes: 10971, borderline nodes: 3512, overall activation: 1446.628, activation diff: 127.210, ratio: 0.088
#   pulse 14: activated nodes: 10971, borderline nodes: 3512, overall activation: 1558.690, activation diff: 112.062, ratio: 0.072
#   pulse 15: activated nodes: 10971, borderline nodes: 3512, overall activation: 1655.676, activation diff: 96.986, ratio: 0.059

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 1655.7
#   number of spread. activ. pulses: 15
#   running time: 1809

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91697115   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.7751294   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.7685543   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7133902   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.60896254   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5536194   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.44752246   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.421812   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.41761482   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4133106   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.40597343   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   12   0.39960152   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   13   0.39803457   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.39285275   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.38911355   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   16   0.38745397   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.38657683   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.38609293   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_77   19   0.38474464   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.38418466   REFERENCES:SIMDATES:SIMLOC
