###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 105.963, activation diff: 107.862, ratio: 1.018
#   pulse 3: activated nodes: 8614, borderline nodes: 5948, overall activation: 93.306, activation diff: 115.368, ratio: 1.236
#   pulse 4: activated nodes: 10636, borderline nodes: 7605, overall activation: 723.910, activation diff: 665.106, ratio: 0.919
#   pulse 5: activated nodes: 10953, borderline nodes: 4308, overall activation: 1198.293, activation diff: 500.125, ratio: 0.417
#   pulse 6: activated nodes: 11301, borderline nodes: 3558, overall activation: 2265.892, activation diff: 1067.602, ratio: 0.471
#   pulse 7: activated nodes: 11387, borderline nodes: 1190, overall activation: 3138.832, activation diff: 872.940, ratio: 0.278
#   pulse 8: activated nodes: 11418, borderline nodes: 617, overall activation: 3706.628, activation diff: 567.796, ratio: 0.153
#   pulse 9: activated nodes: 11427, borderline nodes: 356, overall activation: 3991.072, activation diff: 284.444, ratio: 0.071
#   pulse 10: activated nodes: 11429, borderline nodes: 263, overall activation: 4122.735, activation diff: 131.663, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11429
#   final overall activation: 4122.7
#   number of spread. activ. pulses: 10
#   running time: 1500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96727633   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85366726   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.84551656   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7726317   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7450142   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.7358668   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7342541   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7338711   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7325053   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.72985476   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7281528   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.7267205   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.72663486   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   14   0.7263837   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   15   0.7261783   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.725952   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   17   0.7248389   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   18   0.72472465   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   19   0.7246522   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.7231668   REFERENCES:SIMDATES:SIMLOC
