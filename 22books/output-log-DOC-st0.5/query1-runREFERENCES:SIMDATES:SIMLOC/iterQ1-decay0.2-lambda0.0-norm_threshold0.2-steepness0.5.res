###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 337.846, activation diff: 353.805, ratio: 1.047
#   pulse 3: activated nodes: 9141, borderline nodes: 5079, overall activation: 241.581, activation diff: 572.962, ratio: 2.372
#   pulse 4: activated nodes: 11180, borderline nodes: 6024, overall activation: 2363.116, activation diff: 2478.614, ratio: 1.049
#   pulse 5: activated nodes: 11355, borderline nodes: 1664, overall activation: 1799.365, activation diff: 2820.809, ratio: 1.568
#   pulse 6: activated nodes: 11426, borderline nodes: 611, overall activation: 4320.047, activation diff: 2931.790, ratio: 0.679
#   pulse 7: activated nodes: 11442, borderline nodes: 212, overall activation: 4654.166, activation diff: 596.212, ratio: 0.128
#   pulse 8: activated nodes: 11451, borderline nodes: 122, overall activation: 4910.688, activation diff: 265.607, ratio: 0.054
#   pulse 9: activated nodes: 11453, borderline nodes: 90, overall activation: 4956.409, activation diff: 48.594, ratio: 0.010

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 4956.4
#   number of spread. activ. pulses: 9
#   running time: 1509

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97022426   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.86237174   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.86054695   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7965294   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7925933   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.78968763   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.78877383   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.78765756   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   9   0.78742087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.78699076   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   11   0.786944   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   12   0.78622013   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.78591055   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   14   0.7854399   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   15   0.78454995   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   16   0.78436977   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.78429496   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   18   0.7838842   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   19   0.78388315   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   20   0.78374547   REFERENCES:SIMDATES:SIMLOC
