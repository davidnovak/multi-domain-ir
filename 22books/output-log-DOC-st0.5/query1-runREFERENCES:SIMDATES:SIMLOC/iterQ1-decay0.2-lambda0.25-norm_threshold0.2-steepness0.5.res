###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 168.732, activation diff: 169.128, ratio: 1.002
#   pulse 3: activated nodes: 8879, borderline nodes: 5281, overall activation: 188.096, activation diff: 174.747, ratio: 0.929
#   pulse 4: activated nodes: 11034, borderline nodes: 6735, overall activation: 1170.969, activation diff: 1009.048, ratio: 0.862
#   pulse 5: activated nodes: 11257, borderline nodes: 2674, overall activation: 2060.875, activation diff: 893.204, ratio: 0.433
#   pulse 6: activated nodes: 11396, borderline nodes: 1415, overall activation: 3354.974, activation diff: 1294.099, ratio: 0.386
#   pulse 7: activated nodes: 11430, borderline nodes: 454, overall activation: 4198.181, activation diff: 843.207, ratio: 0.201
#   pulse 8: activated nodes: 11445, borderline nodes: 198, overall activation: 4626.466, activation diff: 428.285, ratio: 0.093
#   pulse 9: activated nodes: 11452, borderline nodes: 128, overall activation: 4818.792, activation diff: 192.325, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 4818.8
#   number of spread. activ. pulses: 9
#   running time: 1576

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9685273   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.85815513   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.8517457   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   4   0.7958257   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   5   0.7902751   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   6   0.7875073   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   7   0.786674   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.786326   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   9   0.784578   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   10   0.7845611   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.78398156   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   12   0.7830987   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   13   0.78298974   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   14   0.78230315   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   15   0.7818452   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_509   16   0.781834   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_584   17   0.78171694   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   18   0.7815674   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   19   0.78143275   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_504   20   0.78124094   REFERENCES:SIMDATES:SIMLOC
