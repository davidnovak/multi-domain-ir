###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 329.032, activation diff: 325.448, ratio: 0.989
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 534.249, activation diff: 287.789, ratio: 0.539
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 1981.189, activation diff: 1446.972, ratio: 0.730
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 3195.202, activation diff: 1214.013, ratio: 0.380
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 4023.242, activation diff: 828.040, ratio: 0.206
#   pulse 7: activated nodes: 11464, borderline nodes: 21, overall activation: 4422.822, activation diff: 399.580, ratio: 0.090
#   pulse 8: activated nodes: 11464, borderline nodes: 21, overall activation: 4597.104, activation diff: 174.282, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 4597.1
#   number of spread. activ. pulses: 8
#   running time: 1555

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9707192   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.8705739   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.865316   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.7990406   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   5   0.7450041   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   6   0.73699045   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_345   7   0.7351166   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   8   0.7349684   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_222   9   0.7337606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_221   10   0.73129606   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.7299714   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   12   0.7292166   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_218   13   0.7287917   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   14   0.7284229   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   15   0.7283611   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_146   16   0.72772765   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_343   17   0.7276691   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_124   18   0.7269087   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_226   19   0.7263707   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_141   20   0.72575355   REFERENCES:SIMDATES:SIMLOC
