###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 246.021, activation diff: 261.088, ratio: 1.061
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 143.555, activation diff: 389.576, ratio: 2.714
#   pulse 4: activated nodes: 9630, borderline nodes: 5862, overall activation: 1573.840, activation diff: 1717.395, ratio: 1.091
#   pulse 5: activated nodes: 10908, borderline nodes: 3608, overall activation: 593.629, activation diff: 2167.469, ratio: 3.651
#   pulse 6: activated nodes: 11090, borderline nodes: 1989, overall activation: 2192.353, activation diff: 2785.982, ratio: 1.271
#   pulse 7: activated nodes: 11212, borderline nodes: 1470, overall activation: 759.542, activation diff: 2951.895, ratio: 3.886
#   pulse 8: activated nodes: 11252, borderline nodes: 957, overall activation: 2257.690, activation diff: 3017.232, ratio: 1.336
#   pulse 9: activated nodes: 11263, borderline nodes: 932, overall activation: 779.546, activation diff: 3037.236, ratio: 3.896
#   pulse 10: activated nodes: 11263, borderline nodes: 880, overall activation: 2264.262, activation diff: 3043.808, ratio: 1.344
#   pulse 11: activated nodes: 11265, borderline nodes: 872, overall activation: 781.816, activation diff: 3046.078, ratio: 3.896
#   pulse 12: activated nodes: 11266, borderline nodes: 868, overall activation: 2265.014, activation diff: 3046.830, ratio: 1.345
#   pulse 13: activated nodes: 11266, borderline nodes: 868, overall activation: 782.108, activation diff: 3047.121, ratio: 3.896
#   pulse 14: activated nodes: 11266, borderline nodes: 868, overall activation: 2265.114, activation diff: 3047.221, ratio: 1.345
#   pulse 15: activated nodes: 11266, borderline nodes: 868, overall activation: 782.153, activation diff: 3047.267, ratio: 3.896

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11266
#   final overall activation: 782.2
#   number of spread. activ. pulses: 15
#   running time: 516

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
