###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 101.902, activation diff: 90.743, ratio: 0.890
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 166.183, activation diff: 64.981, ratio: 0.391
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 337.719, activation diff: 171.567, ratio: 0.508
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 500.190, activation diff: 162.471, ratio: 0.325
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 626.384, activation diff: 126.194, ratio: 0.201
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 713.624, activation diff: 87.241, ratio: 0.122
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 769.980, activation diff: 56.356, ratio: 0.073
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 804.855, activation diff: 34.875, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 804.9
#   number of spread. activ. pulses: 9
#   running time: 366

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9409664   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.80833554   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.7893357   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.71548307   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.63323236   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5578805   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.40470996   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.40393275   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.40217286   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.40145943   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.39317352   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.37909758   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.36767167   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.36733633   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.36715084   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.35898772   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.3541473   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.35398376   REFERENCES
1   Q1   essentialsinmod01howegoog_461   19   0.3528871   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.3525017   REFERENCES
