###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 94.642, activation diff: 84.947, ratio: 0.898
#   pulse 3: activated nodes: 8120, borderline nodes: 3940, overall activation: 174.359, activation diff: 80.652, ratio: 0.463
#   pulse 4: activated nodes: 10080, borderline nodes: 5152, overall activation: 515.170, activation diff: 340.885, ratio: 0.662
#   pulse 5: activated nodes: 10929, borderline nodes: 3603, overall activation: 939.662, activation diff: 424.492, ratio: 0.452
#   pulse 6: activated nodes: 11134, borderline nodes: 2295, overall activation: 1397.909, activation diff: 458.247, ratio: 0.328
#   pulse 7: activated nodes: 11221, borderline nodes: 1338, overall activation: 1795.787, activation diff: 397.878, ratio: 0.222
#   pulse 8: activated nodes: 11237, borderline nodes: 847, overall activation: 2102.901, activation diff: 307.114, ratio: 0.146
#   pulse 9: activated nodes: 11268, borderline nodes: 693, overall activation: 2323.869, activation diff: 220.968, ratio: 0.095
#   pulse 10: activated nodes: 11279, borderline nodes: 648, overall activation: 2476.106, activation diff: 152.237, ratio: 0.061
#   pulse 11: activated nodes: 11285, borderline nodes: 626, overall activation: 2578.131, activation diff: 102.025, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11285
#   final overall activation: 2578.1
#   number of spread. activ. pulses: 11
#   running time: 450

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96226454   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8497578   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.82762057   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.75837016   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.7309561   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7081653   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.70786643   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.70239985   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.70027256   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.6993035   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.69852614   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.6973709   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.69709146   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.69701535   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.6950322   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.69495946   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   17   0.6936639   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.6932421   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.6913792   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.6899139   REFERENCES
