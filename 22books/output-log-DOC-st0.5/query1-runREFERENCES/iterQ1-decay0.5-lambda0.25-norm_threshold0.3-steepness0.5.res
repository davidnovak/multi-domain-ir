###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 71.899, activation diff: 74.627, ratio: 1.038
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 45.933, activation diff: 62.286, ratio: 1.356
#   pulse 4: activated nodes: 7342, borderline nodes: 4710, overall activation: 293.471, activation diff: 260.579, ratio: 0.888
#   pulse 5: activated nodes: 8246, borderline nodes: 3718, overall activation: 383.929, activation diff: 102.760, ratio: 0.268
#   pulse 6: activated nodes: 8292, borderline nodes: 3727, overall activation: 530.281, activation diff: 146.352, ratio: 0.276
#   pulse 7: activated nodes: 8538, borderline nodes: 3488, overall activation: 606.632, activation diff: 76.351, ratio: 0.126
#   pulse 8: activated nodes: 8612, borderline nodes: 3456, overall activation: 646.555, activation diff: 39.923, ratio: 0.062
#   pulse 9: activated nodes: 8629, borderline nodes: 3411, overall activation: 664.112, activation diff: 17.557, ratio: 0.026

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8629
#   final overall activation: 664.1
#   number of spread. activ. pulses: 9
#   running time: 385

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.95392007   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.80981666   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.785699   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7109144   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.6124389   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5288111   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.4069561   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.40673178   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.4066477   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.4063037   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.39208597   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.37834075   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.36457643   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.36427838   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.36385688   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.35764164   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.35067043   REFERENCES
1   Q1   essentialsinmod01howegoog_446   18   0.349778   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.34863174   REFERENCES
1   Q1   politicalsketche00retsrich_148   20   0.34807914   REFERENCES
