###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 166.842, activation diff: 168.541, ratio: 1.010
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 166.986, activation diff: 166.099, ratio: 0.995
#   pulse 4: activated nodes: 9787, borderline nodes: 5564, overall activation: 981.243, activation diff: 863.573, ratio: 0.880
#   pulse 5: activated nodes: 10905, borderline nodes: 3650, overall activation: 1498.889, activation diff: 535.093, ratio: 0.357
#   pulse 6: activated nodes: 11079, borderline nodes: 2269, overall activation: 2237.808, activation diff: 738.977, ratio: 0.330
#   pulse 7: activated nodes: 11215, borderline nodes: 1570, overall activation: 2704.537, activation diff: 466.729, ratio: 0.173
#   pulse 8: activated nodes: 11264, borderline nodes: 893, overall activation: 2958.599, activation diff: 254.063, ratio: 0.086
#   pulse 9: activated nodes: 11280, borderline nodes: 715, overall activation: 3084.575, activation diff: 125.976, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11280
#   final overall activation: 3084.6
#   number of spread. activ. pulses: 9
#   running time: 430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96788985   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8576214   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.83397555   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.79092443   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.7806983   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   6   0.7756562   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.7743683   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.7735775   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.77253556   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   10   0.7717931   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   11   0.7708696   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.7702731   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   13   0.7702034   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.76999366   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.7693535   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.76924235   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.7689309   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   18   0.7682058   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.76819587   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   20   0.765826   REFERENCES
