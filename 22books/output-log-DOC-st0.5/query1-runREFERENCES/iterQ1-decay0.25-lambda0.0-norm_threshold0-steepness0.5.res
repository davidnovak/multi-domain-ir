###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 534.210, activation diff: 553.552, ratio: 1.036
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 307.304, activation diff: 841.514, ratio: 2.738
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1941.809, activation diff: 2249.113, ratio: 1.158
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 680.540, activation diff: 2622.349, ratio: 3.853
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2150.285, activation diff: 2830.824, ratio: 1.316
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 733.242, activation diff: 2883.527, ratio: 3.933
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 2165.526, activation diff: 2898.768, ratio: 1.339
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 737.934, activation diff: 2903.460, ratio: 3.935
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 2166.930, activation diff: 2904.864, ratio: 1.341
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 738.398, activation diff: 2905.328, ratio: 3.935
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 2167.114, activation diff: 2905.512, ratio: 1.341
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 738.456, activation diff: 2905.570, ratio: 3.935
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 2167.152, activation diff: 2905.608, ratio: 1.341
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 738.466, activation diff: 2905.618, ratio: 3.935

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 738.5
#   number of spread. activ. pulses: 15
#   running time: 518

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
