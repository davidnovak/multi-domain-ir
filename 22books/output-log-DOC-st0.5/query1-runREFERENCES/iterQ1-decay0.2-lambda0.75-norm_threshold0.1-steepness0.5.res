###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 24.258, activation diff: 17.237, ratio: 0.711
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 54.938, activation diff: 31.964, ratio: 0.582
#   pulse 4: activated nodes: 9389, borderline nodes: 5553, overall activation: 128.104, activation diff: 73.863, ratio: 0.577
#   pulse 5: activated nodes: 10251, borderline nodes: 4933, overall activation: 252.820, activation diff: 125.012, ratio: 0.494
#   pulse 6: activated nodes: 10730, borderline nodes: 4159, overall activation: 432.350, activation diff: 179.577, ratio: 0.415
#   pulse 7: activated nodes: 10983, borderline nodes: 3290, overall activation: 659.903, activation diff: 227.553, ratio: 0.345
#   pulse 8: activated nodes: 11141, borderline nodes: 2479, overall activation: 919.520, activation diff: 259.617, ratio: 0.282
#   pulse 9: activated nodes: 11191, borderline nodes: 1861, overall activation: 1192.413, activation diff: 272.893, ratio: 0.229
#   pulse 10: activated nodes: 11223, borderline nodes: 1287, overall activation: 1462.553, activation diff: 270.140, ratio: 0.185
#   pulse 11: activated nodes: 11239, borderline nodes: 934, overall activation: 1718.622, activation diff: 256.069, ratio: 0.149
#   pulse 12: activated nodes: 11264, borderline nodes: 773, overall activation: 1953.799, activation diff: 235.177, ratio: 0.120
#   pulse 13: activated nodes: 11275, borderline nodes: 646, overall activation: 2164.756, activation diff: 210.956, ratio: 0.097
#   pulse 14: activated nodes: 11284, borderline nodes: 587, overall activation: 2350.626, activation diff: 185.870, ratio: 0.079
#   pulse 15: activated nodes: 11296, borderline nodes: 543, overall activation: 2512.138, activation diff: 161.512, ratio: 0.064

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11296
#   final overall activation: 2512.1
#   number of spread. activ. pulses: 15
#   running time: 528

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91267365   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.7603306   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.7520894   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.73128885   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   5   0.70518434   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.69910085   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   7   0.69319963   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   8   0.6913388   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.6890656   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.6878765   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.6864741   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.6836186   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   13   0.6825224   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   14   0.6818225   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.6801927   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.6789934   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.67889297   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   18   0.6785617   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   19   0.6779803   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   20   0.6764612   REFERENCES
