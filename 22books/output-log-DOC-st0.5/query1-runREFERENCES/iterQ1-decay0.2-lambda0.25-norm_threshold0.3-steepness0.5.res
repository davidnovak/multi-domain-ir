###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 111.951, activation diff: 114.680, ratio: 1.024
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 92.553, activation diff: 120.669, ratio: 1.304
#   pulse 4: activated nodes: 9019, borderline nodes: 6012, overall activation: 719.019, activation diff: 671.509, ratio: 0.934
#   pulse 5: activated nodes: 10405, borderline nodes: 4233, overall activation: 1066.533, activation diff: 396.025, ratio: 0.371
#   pulse 6: activated nodes: 10817, borderline nodes: 3502, overall activation: 1852.202, activation diff: 786.248, ratio: 0.424
#   pulse 7: activated nodes: 11143, borderline nodes: 2541, overall activation: 2382.942, activation diff: 530.740, ratio: 0.223
#   pulse 8: activated nodes: 11202, borderline nodes: 1589, overall activation: 2709.049, activation diff: 326.107, ratio: 0.120
#   pulse 9: activated nodes: 11219, borderline nodes: 1172, overall activation: 2881.680, activation diff: 172.631, ratio: 0.060
#   pulse 10: activated nodes: 11253, borderline nodes: 1026, overall activation: 2967.959, activation diff: 86.279, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11253
#   final overall activation: 2968.0
#   number of spread. activ. pulses: 10
#   running time: 440

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9670001   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8535047   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.82784224   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.79110146   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.78030443   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   6   0.77618897   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.77413845   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.77368635   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.7727461   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   10   0.7721519   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   11   0.77169645   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   12   0.7709197   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   13   0.77025294   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.76976043   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.7695714   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.7692852   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.76891184   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   18   0.76852727   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.76821476   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   20   0.7631249   REFERENCES
