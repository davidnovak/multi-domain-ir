###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 61.473, activation diff: 53.473, ratio: 0.870
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 109.832, activation diff: 49.608, ratio: 0.452
#   pulse 4: activated nodes: 9107, borderline nodes: 6055, overall activation: 386.495, activation diff: 276.819, ratio: 0.716
#   pulse 5: activated nodes: 10292, borderline nodes: 4494, overall activation: 771.364, activation diff: 384.869, ratio: 0.499
#   pulse 6: activated nodes: 10826, borderline nodes: 3666, overall activation: 1279.658, activation diff: 508.293, ratio: 0.397
#   pulse 7: activated nodes: 11126, borderline nodes: 2553, overall activation: 1778.012, activation diff: 498.354, ratio: 0.280
#   pulse 8: activated nodes: 11207, borderline nodes: 1701, overall activation: 2196.237, activation diff: 418.225, ratio: 0.190
#   pulse 9: activated nodes: 11224, borderline nodes: 1168, overall activation: 2514.391, activation diff: 318.154, ratio: 0.127
#   pulse 10: activated nodes: 11264, borderline nodes: 911, overall activation: 2742.303, activation diff: 227.912, ratio: 0.083
#   pulse 11: activated nodes: 11275, borderline nodes: 762, overall activation: 2899.508, activation diff: 157.205, ratio: 0.054
#   pulse 12: activated nodes: 11281, borderline nodes: 695, overall activation: 3005.315, activation diff: 105.807, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11281
#   final overall activation: 3005.3
#   number of spread. activ. pulses: 12
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9634272   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.84700334   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8247647   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.7870697   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.7736953   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.76871645   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   7   0.7681179   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.766714   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.76576227   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.7639415   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.7639111   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.7633702   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   13   0.76246053   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7620997   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.76170933   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7614759   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.7608941   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   18   0.76081264   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   19   0.76031786   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   20   0.76004434   REFERENCES
