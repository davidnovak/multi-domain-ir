###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 12.012, activation diff: 5.442, ratio: 0.453
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 21.252, activation diff: 10.607, ratio: 0.499
#   pulse 4: activated nodes: 7016, borderline nodes: 4822, overall activation: 38.678, activation diff: 18.345, ratio: 0.474
#   pulse 5: activated nodes: 7476, borderline nodes: 4696, overall activation: 70.097, activation diff: 31.975, ratio: 0.456
#   pulse 6: activated nodes: 7809, borderline nodes: 4228, overall activation: 115.576, activation diff: 45.727, ratio: 0.396
#   pulse 7: activated nodes: 8120, borderline nodes: 3950, overall activation: 171.259, activation diff: 55.768, ratio: 0.326
#   pulse 8: activated nodes: 8304, borderline nodes: 3772, overall activation: 232.319, activation diff: 61.066, ratio: 0.263
#   pulse 9: activated nodes: 8411, borderline nodes: 3646, overall activation: 294.425, activation diff: 62.106, ratio: 0.211
#   pulse 10: activated nodes: 8498, borderline nodes: 3552, overall activation: 354.499, activation diff: 60.074, ratio: 0.169
#   pulse 11: activated nodes: 8568, borderline nodes: 3500, overall activation: 410.525, activation diff: 56.026, ratio: 0.136
#   pulse 12: activated nodes: 8620, borderline nodes: 3464, overall activation: 461.312, activation diff: 50.787, ratio: 0.110
#   pulse 13: activated nodes: 8634, borderline nodes: 3438, overall activation: 506.321, activation diff: 45.009, ratio: 0.089
#   pulse 14: activated nodes: 8640, borderline nodes: 3422, overall activation: 545.502, activation diff: 39.181, ratio: 0.072
#   pulse 15: activated nodes: 8647, borderline nodes: 3407, overall activation: 579.104, activation diff: 33.602, ratio: 0.058

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8647
#   final overall activation: 579.1
#   number of spread. activ. pulses: 15
#   running time: 424

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.86205995   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.65848804   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.5965814   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.59560835   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.5404589   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.40660864   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.353967   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   8   0.3499601   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.34976715   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.3443022   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.33557862   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.33266398   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.32153302   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.31945503   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.31934956   REFERENCES
1   Q1   politicalsketche00retsrich_153   16   0.31146407   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.3111173   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.31094795   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.30732203   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.30403817   REFERENCES
