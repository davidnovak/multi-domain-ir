###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 38.555, activation diff: 32.269, ratio: 0.837
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 65.371, activation diff: 28.832, ratio: 0.441
#   pulse 4: activated nodes: 7956, borderline nodes: 6061, overall activation: 282.202, activation diff: 217.490, ratio: 0.771
#   pulse 5: activated nodes: 9535, borderline nodes: 5062, overall activation: 624.982, activation diff: 342.790, ratio: 0.548
#   pulse 6: activated nodes: 10432, borderline nodes: 4582, overall activation: 1215.878, activation diff: 590.896, ratio: 0.486
#   pulse 7: activated nodes: 10962, borderline nodes: 3300, overall activation: 1906.050, activation diff: 690.172, ratio: 0.362
#   pulse 8: activated nodes: 11154, borderline nodes: 2409, overall activation: 2558.018, activation diff: 651.968, ratio: 0.255
#   pulse 9: activated nodes: 11212, borderline nodes: 1626, overall activation: 3089.318, activation diff: 531.301, ratio: 0.172
#   pulse 10: activated nodes: 11225, borderline nodes: 1135, overall activation: 3486.206, activation diff: 396.887, ratio: 0.114
#   pulse 11: activated nodes: 11274, borderline nodes: 922, overall activation: 3767.154, activation diff: 280.948, ratio: 0.075
#   pulse 12: activated nodes: 11286, borderline nodes: 768, overall activation: 3959.631, activation diff: 192.477, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11286
#   final overall activation: 3959.6
#   number of spread. activ. pulses: 12
#   running time: 480

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.95999897   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.89022446   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.88298535   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.88141584   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   5   0.87849796   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.8784759   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   7   0.8781348   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   8   0.87769943   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.8775207   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.87738013   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   11   0.8771259   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   12   0.8751749   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.8750899   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.8749988   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.87476873   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   16   0.87433755   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.8740841   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   18   0.87297785   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   19   0.8727945   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   20   0.8722793   REFERENCES
