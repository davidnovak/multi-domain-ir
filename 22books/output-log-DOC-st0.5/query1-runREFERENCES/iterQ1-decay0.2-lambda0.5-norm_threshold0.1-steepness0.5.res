###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 100.220, activation diff: 90.525, ratio: 0.903
#   pulse 3: activated nodes: 8120, borderline nodes: 3940, overall activation: 189.072, activation diff: 89.788, ratio: 0.475
#   pulse 4: activated nodes: 10088, borderline nodes: 5137, overall activation: 585.102, activation diff: 396.102, ratio: 0.677
#   pulse 5: activated nodes: 10965, borderline nodes: 3466, overall activation: 1094.398, activation diff: 509.296, ratio: 0.465
#   pulse 6: activated nodes: 11164, borderline nodes: 2086, overall activation: 1658.575, activation diff: 564.177, ratio: 0.340
#   pulse 7: activated nodes: 11227, borderline nodes: 1092, overall activation: 2152.272, activation diff: 493.698, ratio: 0.229
#   pulse 8: activated nodes: 11266, borderline nodes: 681, overall activation: 2533.792, activation diff: 381.520, ratio: 0.151
#   pulse 9: activated nodes: 11287, borderline nodes: 554, overall activation: 2807.901, activation diff: 274.109, ratio: 0.098
#   pulse 10: activated nodes: 11302, borderline nodes: 462, overall activation: 2996.297, activation diff: 188.396, ratio: 0.063
#   pulse 11: activated nodes: 11312, borderline nodes: 397, overall activation: 3122.243, activation diff: 125.945, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11312
#   final overall activation: 3122.2
#   number of spread. activ. pulses: 11
#   running time: 428

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96293974   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.85081017   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.83011323   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.7856179   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.7722864   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.7674774   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   7   0.7666645   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.76537097   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.7643844   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.7628895   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.762462   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.7621915   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   13   0.7620616   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   14   0.7610786   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   15   0.76095915   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.7606628   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.76024866   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7598716   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.7596702   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   20   0.7596474   REFERENCES
