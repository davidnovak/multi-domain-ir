###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 13.480, activation diff: 6.909, ratio: 0.513
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 28.246, activation diff: 16.133, ratio: 0.571
#   pulse 4: activated nodes: 7536, borderline nodes: 5333, overall activation: 61.244, activation diff: 33.874, ratio: 0.553
#   pulse 5: activated nodes: 8755, borderline nodes: 5711, overall activation: 127.699, activation diff: 66.914, ratio: 0.524
#   pulse 6: activated nodes: 9672, borderline nodes: 5372, overall activation: 235.826, activation diff: 108.307, ratio: 0.459
#   pulse 7: activated nodes: 10296, borderline nodes: 4889, overall activation: 389.489, activation diff: 153.690, ratio: 0.395
#   pulse 8: activated nodes: 10681, borderline nodes: 4187, overall activation: 586.173, activation diff: 196.685, ratio: 0.336
#   pulse 9: activated nodes: 10915, borderline nodes: 3464, overall activation: 815.796, activation diff: 229.623, ratio: 0.281
#   pulse 10: activated nodes: 11061, borderline nodes: 2868, overall activation: 1063.098, activation diff: 247.302, ratio: 0.233
#   pulse 11: activated nodes: 11146, borderline nodes: 2420, overall activation: 1313.116, activation diff: 250.019, ratio: 0.190
#   pulse 12: activated nodes: 11191, borderline nodes: 1999, overall activation: 1554.547, activation diff: 241.430, ratio: 0.155
#   pulse 13: activated nodes: 11211, borderline nodes: 1593, overall activation: 1779.863, activation diff: 225.316, ratio: 0.127
#   pulse 14: activated nodes: 11222, borderline nodes: 1325, overall activation: 1984.852, activation diff: 204.989, ratio: 0.103
#   pulse 15: activated nodes: 11224, borderline nodes: 1132, overall activation: 2167.738, activation diff: 182.886, ratio: 0.084

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11224
#   final overall activation: 2167.7
#   number of spread. activ. pulses: 15
#   running time: 502

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.890077   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.71049964   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   3   0.7095766   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.702715   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   5   0.68572193   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.6685925   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   7   0.65889835   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.6542678   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   9   0.6540801   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   10   0.6513567   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   11   0.6508767   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.6501503   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.6497779   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   14   0.6443547   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   15   0.6438308   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   16   0.6428819   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.64129084   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   18   0.6410655   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   19   0.63893616   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   20   0.63880146   REFERENCES
