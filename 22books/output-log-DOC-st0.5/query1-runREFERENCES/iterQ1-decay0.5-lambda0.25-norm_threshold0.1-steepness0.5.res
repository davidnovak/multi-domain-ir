###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 152.895, activation diff: 152.708, ratio: 0.999
#   pulse 3: activated nodes: 8351, borderline nodes: 3639, overall activation: 126.508, activation diff: 78.605, ratio: 0.621
#   pulse 4: activated nodes: 8351, borderline nodes: 3639, overall activation: 464.221, activation diff: 341.408, ratio: 0.735
#   pulse 5: activated nodes: 8649, borderline nodes: 3406, overall activation: 623.544, activation diff: 159.839, ratio: 0.256
#   pulse 6: activated nodes: 8649, borderline nodes: 3406, overall activation: 725.845, activation diff: 102.301, ratio: 0.141
#   pulse 7: activated nodes: 8654, borderline nodes: 3403, overall activation: 769.468, activation diff: 43.623, ratio: 0.057
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 786.449, activation diff: 16.981, ratio: 0.022

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 786.4
#   number of spread. activ. pulses: 8
#   running time: 357

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96063095   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8439778   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.81383663   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7404103   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.6461065   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5834732   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.41637403   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.41564384   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.41543186   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.41491336   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.40099883   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.3881285   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.37550426   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.37522495   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.3749283   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.36903656   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.36278215   REFERENCES
1   Q1   essentialsinmod01howegoog_446   18   0.36173108   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   19   0.36153108   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.36049938   REFERENCES
