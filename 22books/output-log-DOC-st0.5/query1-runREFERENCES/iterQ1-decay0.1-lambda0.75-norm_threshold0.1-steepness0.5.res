###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 26.008, activation diff: 18.988, ratio: 0.730
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 60.853, activation diff: 36.128, ratio: 0.594
#   pulse 4: activated nodes: 9410, borderline nodes: 5544, overall activation: 149.777, activation diff: 89.611, ratio: 0.598
#   pulse 5: activated nodes: 10333, borderline nodes: 4767, overall activation: 310.074, activation diff: 160.579, ratio: 0.518
#   pulse 6: activated nodes: 10805, borderline nodes: 3926, overall activation: 554.188, activation diff: 244.149, ratio: 0.441
#   pulse 7: activated nodes: 11066, borderline nodes: 2966, overall activation: 875.464, activation diff: 321.275, ratio: 0.367
#   pulse 8: activated nodes: 11174, borderline nodes: 2141, overall activation: 1248.437, activation diff: 372.974, ratio: 0.299
#   pulse 9: activated nodes: 11219, borderline nodes: 1391, overall activation: 1642.668, activation diff: 394.231, ratio: 0.240
#   pulse 10: activated nodes: 11263, borderline nodes: 923, overall activation: 2032.706, activation diff: 390.037, ratio: 0.192
#   pulse 11: activated nodes: 11276, borderline nodes: 661, overall activation: 2401.210, activation diff: 368.504, ratio: 0.153
#   pulse 12: activated nodes: 11292, borderline nodes: 493, overall activation: 2738.088, activation diff: 336.879, ratio: 0.123
#   pulse 13: activated nodes: 11303, borderline nodes: 397, overall activation: 3038.732, activation diff: 300.643, ratio: 0.099
#   pulse 14: activated nodes: 11310, borderline nodes: 326, overall activation: 3302.260, activation diff: 263.529, ratio: 0.080
#   pulse 15: activated nodes: 11314, borderline nodes: 241, overall activation: 3530.162, activation diff: 227.902, ratio: 0.065

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11314
#   final overall activation: 3530.2
#   number of spread. activ. pulses: 15
#   running time: 536

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.91598463   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8384874   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   3   0.8186456   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   4   0.81540644   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   5   0.8144114   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   6   0.8143035   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.81205964   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   8   0.811409   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   9   0.81080294   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.80879384   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.80800647   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   12   0.80595255   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   13   0.8056568   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   14   0.8054043   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   15   0.80372036   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.8036348   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   17   0.80222464   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   18   0.8012961   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   19   0.8009874   REFERENCES
1   Q1   encyclopediaame28unkngoog_127   20   0.799036   REFERENCES
