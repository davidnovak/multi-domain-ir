###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 13.969, activation diff: 7.399, ratio: 0.530
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 30.587, activation diff: 17.985, ratio: 0.588
#   pulse 4: activated nodes: 7574, borderline nodes: 5369, overall activation: 69.642, activation diff: 39.921, ratio: 0.573
#   pulse 5: activated nodes: 8834, borderline nodes: 5723, overall activation: 151.335, activation diff: 82.135, ratio: 0.543
#   pulse 6: activated nodes: 9748, borderline nodes: 5289, overall activation: 291.599, activation diff: 140.424, ratio: 0.482
#   pulse 7: activated nodes: 10453, borderline nodes: 4608, overall activation: 502.054, activation diff: 210.473, ratio: 0.419
#   pulse 8: activated nodes: 10808, borderline nodes: 3831, overall activation: 782.711, activation diff: 280.657, ratio: 0.359
#   pulse 9: activated nodes: 11032, borderline nodes: 3086, overall activation: 1116.805, activation diff: 334.094, ratio: 0.299
#   pulse 10: activated nodes: 11145, borderline nodes: 2478, overall activation: 1478.171, activation diff: 361.366, ratio: 0.244
#   pulse 11: activated nodes: 11203, borderline nodes: 1902, overall activation: 1843.171, activation diff: 365.001, ratio: 0.198
#   pulse 12: activated nodes: 11217, borderline nodes: 1450, overall activation: 2194.167, activation diff: 350.995, ratio: 0.160
#   pulse 13: activated nodes: 11230, borderline nodes: 1120, overall activation: 2519.935, activation diff: 325.768, ratio: 0.129
#   pulse 14: activated nodes: 11268, borderline nodes: 914, overall activation: 2814.431, activation diff: 294.496, ratio: 0.105
#   pulse 15: activated nodes: 11278, borderline nodes: 761, overall activation: 3075.449, activation diff: 261.018, ratio: 0.085

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11278
#   final overall activation: 3075.4
#   number of spread. activ. pulses: 15
#   running time: 525

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89519477   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8189263   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   3   0.8001565   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   4   0.78677434   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   5   0.78562176   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   6   0.7814737   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   7   0.78002083   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   8   0.77885807   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   9   0.77730036   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.77610815   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   11   0.77559376   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.77540904   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   13   0.7753739   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   14   0.77161   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   15   0.76928574   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.768023   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   17   0.7651951   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   18   0.7646887   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   19   0.7645411   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   20   0.7639489   REFERENCES
