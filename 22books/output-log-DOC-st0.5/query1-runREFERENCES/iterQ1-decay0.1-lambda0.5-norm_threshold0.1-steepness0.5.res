###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 111.376, activation diff: 101.681, ratio: 0.913
#   pulse 3: activated nodes: 8120, borderline nodes: 3940, overall activation: 220.070, activation diff: 109.633, ratio: 0.498
#   pulse 4: activated nodes: 10094, borderline nodes: 5086, overall activation: 747.893, activation diff: 527.892, ratio: 0.706
#   pulse 5: activated nodes: 11019, borderline nodes: 3217, overall activation: 1461.178, activation diff: 713.284, ratio: 0.488
#   pulse 6: activated nodes: 11185, borderline nodes: 1740, overall activation: 2276.075, activation diff: 814.897, ratio: 0.358
#   pulse 7: activated nodes: 11263, borderline nodes: 810, overall activation: 2990.685, activation diff: 714.610, ratio: 0.239
#   pulse 8: activated nodes: 11295, borderline nodes: 446, overall activation: 3539.405, activation diff: 548.720, ratio: 0.155
#   pulse 9: activated nodes: 11311, borderline nodes: 291, overall activation: 3930.224, activation diff: 390.819, ratio: 0.099
#   pulse 10: activated nodes: 11315, borderline nodes: 202, overall activation: 4196.655, activation diff: 266.431, ratio: 0.063
#   pulse 11: activated nodes: 11320, borderline nodes: 149, overall activation: 4373.564, activation diff: 176.909, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 4373.6
#   number of spread. activ. pulses: 11
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9639349   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8909155   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.8858398   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.88445807   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.8815149   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.88140106   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.8812121   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   8   0.88069695   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   9   0.8806256   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.8805278   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.8799269   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   12   0.8793958   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.8789269   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.8787729   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.8787515   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.87820566   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.8779651   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   18   0.87699604   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   19   0.8766899   REFERENCES
1   Q1   encyclopediaame28unkngoog_127   20   0.8762294   REFERENCES
