###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 370.899, activation diff: 368.843, ratio: 0.994
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 565.794, activation diff: 335.824, ratio: 0.594
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2102.537, activation diff: 1536.903, ratio: 0.731
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3253.592, activation diff: 1151.056, ratio: 0.354
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 4076.311, activation diff: 822.719, ratio: 0.202
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 4509.372, activation diff: 433.061, ratio: 0.096
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 4715.404, activation diff: 206.031, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 4715.4
#   number of spread. activ. pulses: 8
#   running time: 407

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.970928   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8958204   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.8937503   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.89281887   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.8908501   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.88944936   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   7   0.8893739   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.88932383   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   9   0.889318   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.8887775   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   11   0.8881969   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.88812965   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   13   0.8877183   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   14   0.8875992   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.887305   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.8871412   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   17   0.8870286   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   18   0.88664746   REFERENCES
1   Q1   encyclopediaame28unkngoog_515   19   0.8861572   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   20   0.8860962   REFERENCES
