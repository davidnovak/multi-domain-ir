###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 155.725, activation diff: 144.565, ratio: 0.928
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 310.192, activation diff: 155.162, ratio: 0.500
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 824.785, activation diff: 514.619, ratio: 0.624
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1433.684, activation diff: 608.899, ratio: 0.425
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2017.185, activation diff: 583.501, ratio: 0.289
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 2486.097, activation diff: 468.912, ratio: 0.189
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 2828.837, activation diff: 342.739, ratio: 0.121
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 3065.841, activation diff: 237.005, ratio: 0.077
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 3224.262, activation diff: 158.421, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 3224.3
#   number of spread. activ. pulses: 10
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96058106   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.852337   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8324324   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.7826624   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.7692551   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   6   0.7648803   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.7642225   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   8   0.763551   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.7619917   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.7610702   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   11   0.7599213   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.7592203   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.7587166   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   14   0.7580837   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   15   0.757686   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.7576001   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   17   0.7575125   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7568922   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   19   0.7568625   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   20   0.7563039   REFERENCES
