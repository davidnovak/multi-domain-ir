###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 23.382, activation diff: 16.362, ratio: 0.700
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 52.030, activation diff: 29.931, ratio: 0.575
#   pulse 4: activated nodes: 9387, borderline nodes: 5559, overall activation: 118.046, activation diff: 66.720, ratio: 0.565
#   pulse 5: activated nodes: 10225, borderline nodes: 5032, overall activation: 227.569, activation diff: 109.826, ratio: 0.483
#   pulse 6: activated nodes: 10667, borderline nodes: 4301, overall activation: 380.676, activation diff: 153.160, ratio: 0.402
#   pulse 7: activated nodes: 10929, borderline nodes: 3482, overall activation: 570.169, activation diff: 189.492, ratio: 0.332
#   pulse 8: activated nodes: 11096, borderline nodes: 2684, overall activation: 783.313, activation diff: 213.144, ratio: 0.272
#   pulse 9: activated nodes: 11178, borderline nodes: 2099, overall activation: 1005.651, activation diff: 222.338, ratio: 0.221
#   pulse 10: activated nodes: 11211, borderline nodes: 1521, overall activation: 1224.946, activation diff: 219.295, ratio: 0.179
#   pulse 11: activated nodes: 11227, borderline nodes: 1151, overall activation: 1432.529, activation diff: 207.583, ratio: 0.145
#   pulse 12: activated nodes: 11238, borderline nodes: 915, overall activation: 1623.145, activation diff: 190.616, ratio: 0.117
#   pulse 13: activated nodes: 11263, borderline nodes: 784, overall activation: 1794.244, activation diff: 171.099, ratio: 0.095
#   pulse 14: activated nodes: 11269, borderline nodes: 683, overall activation: 1945.166, activation diff: 150.921, ratio: 0.078
#   pulse 15: activated nodes: 11273, borderline nodes: 646, overall activation: 2076.483, activation diff: 131.318, ratio: 0.063

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11273
#   final overall activation: 2076.5
#   number of spread. activ. pulses: 15
#   running time: 517

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9106259   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.75530577   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.74775773   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.6770354   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.6752486   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   6   0.6463456   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.638169   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.62349373   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   9   0.62325346   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.6221034   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.62115896   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.6211255   REFERENCES
1   Q1   europesincenapol00leveuoft_16   13   0.61980116   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   14   0.6132158   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   15   0.61224186   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   16   0.61085314   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.6105768   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   18   0.61057657   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   19   0.61013055   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   20   0.60837114   REFERENCES
