###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 310.672, activation diff: 308.616, ratio: 0.993
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 410.931, activation diff: 217.894, ratio: 0.530
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1348.236, activation diff: 937.490, ratio: 0.695
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1996.723, activation diff: 648.487, ratio: 0.325
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 2452.541, activation diff: 455.818, ratio: 0.186
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2693.075, activation diff: 240.533, ratio: 0.089
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 2808.223, activation diff: 115.148, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 2808.2
#   number of spread. activ. pulses: 8
#   running time: 412

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.969978   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8693785   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8486683   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.782787   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.73733675   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7205956   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.7174722   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.71434194   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   9   0.7119877   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   10   0.7116298   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.711604   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.70916396   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.7087978   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.7083192   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.7083139   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   16   0.7080772   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   17   0.7079849   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.70632637   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   19   0.70625055   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.70545536   REFERENCES
