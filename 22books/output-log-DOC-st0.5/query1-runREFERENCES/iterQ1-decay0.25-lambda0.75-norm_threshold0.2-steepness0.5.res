###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.628, activation diff: 5.628, ratio: 0.652
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 13.235, activation diff: 6.665, ratio: 0.504
#   pulse 3: activated nodes: 6340, borderline nodes: 5152, overall activation: 27.078, activation diff: 15.210, ratio: 0.562
#   pulse 4: activated nodes: 7490, borderline nodes: 5288, overall activation: 57.215, activation diff: 31.019, ratio: 0.542
#   pulse 5: activated nodes: 8687, borderline nodes: 5673, overall activation: 116.787, activation diff: 60.040, ratio: 0.514
#   pulse 6: activated nodes: 9642, borderline nodes: 5416, overall activation: 211.430, activation diff: 94.834, ratio: 0.449
#   pulse 7: activated nodes: 10215, borderline nodes: 5014, overall activation: 342.231, activation diff: 130.837, ratio: 0.382
#   pulse 8: activated nodes: 10574, borderline nodes: 4359, overall activation: 505.595, activation diff: 163.364, ratio: 0.323
#   pulse 9: activated nodes: 10842, borderline nodes: 3719, overall activation: 693.279, activation diff: 187.684, ratio: 0.271
#   pulse 10: activated nodes: 10989, borderline nodes: 3139, overall activation: 893.817, activation diff: 200.538, ratio: 0.224
#   pulse 11: activated nodes: 11092, borderline nodes: 2642, overall activation: 1095.892, activation diff: 202.076, ratio: 0.184
#   pulse 12: activated nodes: 11155, borderline nodes: 2262, overall activation: 1290.719, activation diff: 194.827, ratio: 0.151
#   pulse 13: activated nodes: 11188, borderline nodes: 1936, overall activation: 1472.538, activation diff: 181.819, ratio: 0.123
#   pulse 14: activated nodes: 11207, borderline nodes: 1598, overall activation: 1638.049, activation diff: 165.510, ratio: 0.101
#   pulse 15: activated nodes: 11219, borderline nodes: 1412, overall activation: 1785.879, activation diff: 147.831, ratio: 0.083

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11219
#   final overall activation: 1785.9
#   number of spread. activ. pulses: 15
#   running time: 519

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.8869482   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.7045075   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.69363457   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.65219176   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   5   0.6370052   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   6   0.62649363   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.6061817   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.597879   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.5906197   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.5889744   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.5876202   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   12   0.5843518   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   13   0.5837932   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   14   0.5829968   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.5781653   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   16   0.577752   REFERENCES
1   Q1   essentialsinmod01howegoog_14   17   0.57599115   REFERENCES
1   Q1   europesincenapol00leveuoft_353   18   0.5752442   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   19   0.5742144   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   20   0.57203496   REFERENCES
