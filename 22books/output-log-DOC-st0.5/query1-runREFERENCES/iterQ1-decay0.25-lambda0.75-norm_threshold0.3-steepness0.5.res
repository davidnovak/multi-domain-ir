###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 9.310, activation diff: 3.193, ratio: 0.343
#   pulse 3: activated nodes: 5574, borderline nodes: 5253, overall activation: 14.335, activation diff: 6.479, ratio: 0.452
#   pulse 4: activated nodes: 6309, borderline nodes: 5147, overall activation: 23.658, activation diff: 10.561, ratio: 0.446
#   pulse 5: activated nodes: 7040, borderline nodes: 5492, overall activation: 46.860, activation diff: 24.145, ratio: 0.515
#   pulse 6: activated nodes: 7726, borderline nodes: 5393, overall activation: 90.895, activation diff: 44.556, ratio: 0.490
#   pulse 7: activated nodes: 8837, borderline nodes: 5664, overall activation: 161.298, activation diff: 70.569, ratio: 0.438
#   pulse 8: activated nodes: 9583, borderline nodes: 5446, overall activation: 260.691, activation diff: 99.452, ratio: 0.381
#   pulse 9: activated nodes: 10057, borderline nodes: 5067, overall activation: 388.863, activation diff: 128.175, ratio: 0.330
#   pulse 10: activated nodes: 10481, borderline nodes: 4625, overall activation: 542.666, activation diff: 153.803, ratio: 0.283
#   pulse 11: activated nodes: 10710, borderline nodes: 4072, overall activation: 715.533, activation diff: 172.868, ratio: 0.242
#   pulse 12: activated nodes: 10861, borderline nodes: 3543, overall activation: 898.222, activation diff: 182.688, ratio: 0.203
#   pulse 13: activated nodes: 10984, borderline nodes: 3117, overall activation: 1081.471, activation diff: 183.250, ratio: 0.169
#   pulse 14: activated nodes: 11053, borderline nodes: 2747, overall activation: 1257.936, activation diff: 176.464, ratio: 0.140
#   pulse 15: activated nodes: 11098, borderline nodes: 2467, overall activation: 1422.696, activation diff: 164.760, ratio: 0.116

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11098
#   final overall activation: 1422.7
#   number of spread. activ. pulses: 15
#   running time: 471

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.84148777   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.6329378   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   3   0.6067544   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   4   0.59035754   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.578943   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   6   0.5735545   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.5597134   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.5525403   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   9   0.5461451   REFERENCES
1   Q1   essentialsinmod01howegoog_14   10   0.536065   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.53496367   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   12   0.5331164   REFERENCES
1   Q1   essentialsinmod01howegoog_474   13   0.5322436   REFERENCES
1   Q1   europesincenapol00leveuoft_353   14   0.5300512   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   15   0.5292785   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.5274379   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   17   0.5270715   REFERENCES
1   Q1   essentialsinmod01howegoog_471   18   0.52558684   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   19   0.52386534   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.5207468   REFERENCES
