###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 46.929, activation diff: 39.464, ratio: 0.841
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 106.559, activation diff: 60.830, ratio: 0.571
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 235.628, activation diff: 129.625, ratio: 0.550
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 429.890, activation diff: 194.433, ratio: 0.452
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 680.081, activation diff: 250.209, ratio: 0.368
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 965.233, activation diff: 285.151, ratio: 0.295
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 1262.636, activation diff: 297.403, ratio: 0.236
#   pulse 9: activated nodes: 11335, borderline nodes: 30, overall activation: 1554.192, activation diff: 291.556, ratio: 0.188
#   pulse 10: activated nodes: 11335, borderline nodes: 30, overall activation: 1827.903, activation diff: 273.711, ratio: 0.150
#   pulse 11: activated nodes: 11335, borderline nodes: 30, overall activation: 2077.013, activation diff: 249.110, ratio: 0.120
#   pulse 12: activated nodes: 11335, borderline nodes: 30, overall activation: 2298.616, activation diff: 221.603, ratio: 0.096
#   pulse 13: activated nodes: 11335, borderline nodes: 30, overall activation: 2492.387, activation diff: 193.771, ratio: 0.078
#   pulse 14: activated nodes: 11335, borderline nodes: 30, overall activation: 2659.592, activation diff: 167.205, ratio: 0.063
#   pulse 15: activated nodes: 11335, borderline nodes: 30, overall activation: 2802.384, activation diff: 142.793, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 2802.4
#   number of spread. activ. pulses: 15
#   running time: 531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.92596334   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.7957444   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.77992356   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.74356174   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.7179543   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   6   0.71690613   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.71688676   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   8   0.7109719   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.71009135   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.7099972   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   11   0.70895046   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.70884216   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   13   0.70730287   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7065528   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.7037384   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.703467   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   17   0.70267594   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.7013309   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   19   0.70080644   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   20   0.69957584   REFERENCES
