###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 443.625, activation diff: 461.560, ratio: 1.040
#   pulse 3: activated nodes: 8476, borderline nodes: 3451, overall activation: 276.558, activation diff: 720.183, ratio: 2.604
#   pulse 4: activated nodes: 10583, borderline nodes: 3617, overall activation: 2083.201, activation diff: 2359.759, ratio: 1.133
#   pulse 5: activated nodes: 11199, borderline nodes: 2064, overall activation: 785.738, activation diff: 2868.939, ratio: 3.651
#   pulse 6: activated nodes: 11277, borderline nodes: 584, overall activation: 2437.936, activation diff: 3223.674, ratio: 1.322
#   pulse 7: activated nodes: 11312, borderline nodes: 449, overall activation: 879.760, activation diff: 3317.696, ratio: 3.771
#   pulse 8: activated nodes: 11314, borderline nodes: 320, overall activation: 2466.943, activation diff: 3346.703, ratio: 1.357
#   pulse 9: activated nodes: 11314, borderline nodes: 288, overall activation: 889.200, activation diff: 3356.143, ratio: 3.774
#   pulse 10: activated nodes: 11315, borderline nodes: 276, overall activation: 2470.359, activation diff: 3359.559, ratio: 1.360
#   pulse 11: activated nodes: 11315, borderline nodes: 276, overall activation: 890.384, activation diff: 3360.743, ratio: 3.774
#   pulse 12: activated nodes: 11315, borderline nodes: 275, overall activation: 2471.094, activation diff: 3361.477, ratio: 1.360
#   pulse 13: activated nodes: 11315, borderline nodes: 275, overall activation: 890.612, activation diff: 3361.706, ratio: 3.775
#   pulse 14: activated nodes: 11315, borderline nodes: 275, overall activation: 2471.326, activation diff: 3361.938, ratio: 1.360
#   pulse 15: activated nodes: 11315, borderline nodes: 275, overall activation: 890.676, activation diff: 3362.002, ratio: 3.775

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 890.7
#   number of spread. activ. pulses: 15
#   running time: 513

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
