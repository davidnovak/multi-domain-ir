###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 58.240, activation diff: 50.240, ratio: 0.863
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 102.211, activation diff: 45.230, ratio: 0.443
#   pulse 4: activated nodes: 9103, borderline nodes: 6062, overall activation: 344.651, activation diff: 242.613, ratio: 0.704
#   pulse 5: activated nodes: 10225, borderline nodes: 4640, overall activation: 670.426, activation diff: 325.774, ratio: 0.486
#   pulse 6: activated nodes: 10756, borderline nodes: 3874, overall activation: 1083.579, activation diff: 413.154, ratio: 0.381
#   pulse 7: activated nodes: 11071, borderline nodes: 2768, overall activation: 1483.954, activation diff: 400.375, ratio: 0.270
#   pulse 8: activated nodes: 11169, borderline nodes: 2023, overall activation: 1819.067, activation diff: 335.113, ratio: 0.184
#   pulse 9: activated nodes: 11217, borderline nodes: 1446, overall activation: 2074.323, activation diff: 255.256, ratio: 0.123
#   pulse 10: activated nodes: 11222, borderline nodes: 1092, overall activation: 2257.754, activation diff: 183.431, ratio: 0.081
#   pulse 11: activated nodes: 11248, borderline nodes: 953, overall activation: 2384.735, activation diff: 126.981, ratio: 0.053
#   pulse 12: activated nodes: 11257, borderline nodes: 858, overall activation: 2470.556, activation diff: 85.822, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11257
#   final overall activation: 2470.6
#   number of spread. activ. pulses: 12
#   running time: 476

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9626912   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.84568334   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.82186997   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.75166786   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.7322451   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7089747   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.70864797   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.7030998   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.7002145   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.69997996   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.6992661   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.6980476   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.69744176   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.6974191   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.69546545   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.6952058   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   17   0.69409615   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.69348913   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.69147515   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.6903857   REFERENCES
