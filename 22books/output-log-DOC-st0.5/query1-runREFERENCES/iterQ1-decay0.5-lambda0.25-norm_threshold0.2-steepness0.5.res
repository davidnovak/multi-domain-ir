###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 106.642, activation diff: 108.341, ratio: 1.016
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 77.006, activation diff: 74.699, ratio: 0.970
#   pulse 4: activated nodes: 7841, borderline nodes: 4313, overall activation: 376.490, activation diff: 308.496, ratio: 0.819
#   pulse 5: activated nodes: 8533, borderline nodes: 3511, overall activation: 507.931, activation diff: 136.041, ratio: 0.268
#   pulse 6: activated nodes: 8533, borderline nodes: 3511, overall activation: 633.887, activation diff: 125.956, ratio: 0.199
#   pulse 7: activated nodes: 8647, borderline nodes: 3408, overall activation: 693.336, activation diff: 59.450, ratio: 0.086
#   pulse 8: activated nodes: 8653, borderline nodes: 3406, overall activation: 719.791, activation diff: 26.454, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8653
#   final overall activation: 719.8
#   number of spread. activ. pulses: 8
#   running time: 368

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9538161   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8122288   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.79170644   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7180288   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.6245023   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5437536   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.40970907   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.4090503   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.40827066   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.40824878   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.39566994   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.38202804   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   13   0.3688185   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   14   0.36880347   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.36828312   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.36135012   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.35511738   REFERENCES
1   Q1   essentialsinmod01howegoog_446   18   0.35413784   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.35340387   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.3534019   REFERENCES
