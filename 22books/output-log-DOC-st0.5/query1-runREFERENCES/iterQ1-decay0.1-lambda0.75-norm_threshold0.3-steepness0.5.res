###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 9.397, activation diff: 3.280, ratio: 0.349
#   pulse 3: activated nodes: 5574, borderline nodes: 5253, overall activation: 15.490, activation diff: 7.546, ratio: 0.487
#   pulse 4: activated nodes: 6309, borderline nodes: 5147, overall activation: 27.175, activation diff: 12.902, ratio: 0.475
#   pulse 5: activated nodes: 7141, borderline nodes: 5579, overall activation: 58.218, activation diff: 31.934, ratio: 0.549
#   pulse 6: activated nodes: 8082, borderline nodes: 5628, overall activation: 119.216, activation diff: 61.448, ratio: 0.515
#   pulse 7: activated nodes: 9148, borderline nodes: 5695, overall activation: 223.022, activation diff: 103.945, ratio: 0.466
#   pulse 8: activated nodes: 9774, borderline nodes: 5163, overall activation: 381.324, activation diff: 158.334, ratio: 0.415
#   pulse 9: activated nodes: 10391, borderline nodes: 4623, overall activation: 601.340, activation diff: 220.016, ratio: 0.366
#   pulse 10: activated nodes: 10752, borderline nodes: 3956, overall activation: 879.583, activation diff: 278.243, ratio: 0.316
#   pulse 11: activated nodes: 10946, borderline nodes: 3269, overall activation: 1198.894, activation diff: 319.311, ratio: 0.266
#   pulse 12: activated nodes: 11091, borderline nodes: 2756, overall activation: 1536.612, activation diff: 337.718, ratio: 0.220
#   pulse 13: activated nodes: 11155, borderline nodes: 2330, overall activation: 1873.086, activation diff: 336.474, ratio: 0.180
#   pulse 14: activated nodes: 11194, borderline nodes: 1913, overall activation: 2194.319, activation diff: 321.233, ratio: 0.146
#   pulse 15: activated nodes: 11211, borderline nodes: 1580, overall activation: 2491.458, activation diff: 297.139, ratio: 0.119

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11211
#   final overall activation: 2491.5
#   number of spread. activ. pulses: 15
#   running time: 489

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.85545874   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.77972   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   3   0.7657027   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   4   0.7331056   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   5   0.7314468   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   6   0.7267149   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   7   0.72460186   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.72356623   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   9   0.71791637   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7141444   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   11   0.71343327   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   12   0.71309334   REFERENCES
1   Q1   essentialsinmod01howegoog_14   13   0.7116796   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   14   0.7114266   REFERENCES
1   Q1   europesincenapol00leveuoft_353   15   0.7112757   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   16   0.7109391   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.7102893   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.7066884   REFERENCES
1   Q1   englishcyclopae05kniggoog_440   19   0.70658976   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   20   0.7062646   REFERENCES
