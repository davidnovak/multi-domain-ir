###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.984, activation diff: 5.984, ratio: 0.666
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 19.006, activation diff: 11.985, ratio: 0.631
#   pulse 3: activated nodes: 7485, borderline nodes: 4624, overall activation: 37.913, activation diff: 20.190, ratio: 0.533
#   pulse 4: activated nodes: 7900, borderline nodes: 4242, overall activation: 74.070, activation diff: 36.896, ratio: 0.498
#   pulse 5: activated nodes: 8248, borderline nodes: 3770, overall activation: 127.362, activation diff: 53.639, ratio: 0.421
#   pulse 6: activated nodes: 8451, borderline nodes: 3592, overall activation: 192.406, activation diff: 65.144, ratio: 0.339
#   pulse 7: activated nodes: 8549, borderline nodes: 3492, overall activation: 262.867, activation diff: 70.471, ratio: 0.268
#   pulse 8: activated nodes: 8626, borderline nodes: 3447, overall activation: 333.556, activation diff: 70.689, ratio: 0.212
#   pulse 9: activated nodes: 8640, borderline nodes: 3422, overall activation: 400.852, activation diff: 67.296, ratio: 0.168
#   pulse 10: activated nodes: 8649, borderline nodes: 3407, overall activation: 462.537, activation diff: 61.685, ratio: 0.133
#   pulse 11: activated nodes: 8653, borderline nodes: 3405, overall activation: 517.502, activation diff: 54.965, ratio: 0.106
#   pulse 12: activated nodes: 8654, borderline nodes: 3405, overall activation: 565.424, activation diff: 47.922, ratio: 0.085
#   pulse 13: activated nodes: 8654, borderline nodes: 3403, overall activation: 606.499, activation diff: 41.074, ratio: 0.068
#   pulse 14: activated nodes: 8654, borderline nodes: 3403, overall activation: 641.226, activation diff: 34.728, ratio: 0.054
#   pulse 15: activated nodes: 8660, borderline nodes: 3406, overall activation: 670.266, activation diff: 29.039, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 670.3
#   number of spread. activ. pulses: 15
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89354426   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.713253   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.69729257   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.64422584   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.57638824   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.4716615   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.3708256   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.3683264   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.364716   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.3634094   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.3601722   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.3477208   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.33619025   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.33528394   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.33519995   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.32522747   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.32479393   REFERENCES
1   Q1   politicalsketche00retsrich_153   18   0.3227147   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.3224635   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.32083362   REFERENCES
