###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 498.760, activation diff: 516.695, ratio: 1.036
#   pulse 3: activated nodes: 8476, borderline nodes: 3451, overall activation: 378.889, activation diff: 877.649, ratio: 2.316
#   pulse 4: activated nodes: 10585, borderline nodes: 3551, overall activation: 2785.159, activation diff: 3164.048, ratio: 1.136
#   pulse 5: activated nodes: 11203, borderline nodes: 1955, overall activation: 1183.290, activation diff: 3968.450, ratio: 3.354
#   pulse 6: activated nodes: 11286, borderline nodes: 318, overall activation: 3310.642, activation diff: 4493.932, ratio: 1.357
#   pulse 7: activated nodes: 11322, borderline nodes: 175, overall activation: 1328.487, activation diff: 4639.129, ratio: 3.492
#   pulse 8: activated nodes: 11328, borderline nodes: 86, overall activation: 3355.688, activation diff: 4684.174, ratio: 1.396
#   pulse 9: activated nodes: 11330, borderline nodes: 71, overall activation: 1343.977, activation diff: 4699.665, ratio: 3.497
#   pulse 10: activated nodes: 11330, borderline nodes: 67, overall activation: 3361.316, activation diff: 4705.294, ratio: 1.400
#   pulse 11: activated nodes: 11330, borderline nodes: 67, overall activation: 1346.164, activation diff: 4707.481, ratio: 3.497
#   pulse 12: activated nodes: 11330, borderline nodes: 65, overall activation: 3362.060, activation diff: 4708.224, ratio: 1.400
#   pulse 13: activated nodes: 11330, borderline nodes: 64, overall activation: 1346.547, activation diff: 4708.607, ratio: 3.497
#   pulse 14: activated nodes: 11330, borderline nodes: 64, overall activation: 3362.191, activation diff: 4708.738, ratio: 1.400
#   pulse 15: activated nodes: 11330, borderline nodes: 64, overall activation: 1346.622, activation diff: 4708.813, ratio: 3.497

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11330
#   final overall activation: 1346.6
#   number of spread. activ. pulses: 15
#   running time: 649

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
