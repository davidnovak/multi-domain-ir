###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 173.665, activation diff: 162.506, ratio: 0.936
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 365.744, activation diff: 192.771, ratio: 0.527
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1066.914, activation diff: 701.195, ratio: 0.657
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1928.085, activation diff: 861.171, ratio: 0.447
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2765.831, activation diff: 837.746, ratio: 0.303
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 3438.548, activation diff: 672.717, ratio: 0.196
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 3927.457, activation diff: 488.910, ratio: 0.124
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 4263.361, activation diff: 335.904, ratio: 0.079
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 4486.664, activation diff: 223.303, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 4486.7
#   number of spread. activ. pulses: 10
#   running time: 451

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9615457   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8878439   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.8827299   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.88124377   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.87840354   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.878158   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.8776682   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   8   0.87732375   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   9   0.877103   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.8768835   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.87631834   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   12   0.87560546   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   13   0.8753208   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.87530255   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   15   0.87492085   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.87447464   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.8741729   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   18   0.8733355   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   19   0.87306213   REFERENCES
1   Q1   encyclopediaame28unkngoog_127   20   0.8725605   REFERENCES
