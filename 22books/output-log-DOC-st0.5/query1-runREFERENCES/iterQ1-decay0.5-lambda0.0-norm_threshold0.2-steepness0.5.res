###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 209.495, activation diff: 226.005, ratio: 1.079
#   pulse 3: activated nodes: 8042, borderline nodes: 4149, overall activation: 44.982, activation diff: 254.478, ratio: 5.657
#   pulse 4: activated nodes: 8042, borderline nodes: 4149, overall activation: 644.471, activation diff: 689.454, ratio: 1.070
#   pulse 5: activated nodes: 8641, borderline nodes: 3423, overall activation: 56.998, activation diff: 701.469, ratio: 12.307
#   pulse 6: activated nodes: 8641, borderline nodes: 3423, overall activation: 677.298, activation diff: 734.296, ratio: 1.084
#   pulse 7: activated nodes: 8654, borderline nodes: 3403, overall activation: 57.929, activation diff: 735.227, ratio: 12.692
#   pulse 8: activated nodes: 8654, borderline nodes: 3403, overall activation: 678.660, activation diff: 736.589, ratio: 1.085
#   pulse 9: activated nodes: 8654, borderline nodes: 3403, overall activation: 58.008, activation diff: 736.668, ratio: 12.699
#   pulse 10: activated nodes: 8654, borderline nodes: 3403, overall activation: 678.760, activation diff: 736.768, ratio: 1.085
#   pulse 11: activated nodes: 8654, borderline nodes: 3403, overall activation: 58.016, activation diff: 736.776, ratio: 12.700
#   pulse 12: activated nodes: 8654, borderline nodes: 3403, overall activation: 678.770, activation diff: 736.786, ratio: 1.085
#   pulse 13: activated nodes: 8654, borderline nodes: 3403, overall activation: 58.017, activation diff: 736.787, ratio: 12.700
#   pulse 14: activated nodes: 8654, borderline nodes: 3403, overall activation: 678.771, activation diff: 736.788, ratio: 1.085
#   pulse 15: activated nodes: 8654, borderline nodes: 3403, overall activation: 58.017, activation diff: 736.788, ratio: 12.700

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8654
#   final overall activation: 58.0
#   number of spread. activ. pulses: 15
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
