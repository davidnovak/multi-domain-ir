###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.342, activation diff: 25.342, ratio: 1.310
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 640.497, activation diff: 659.839, ratio: 1.030
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 508.999, activation diff: 1149.496, ratio: 2.258
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3032.511, activation diff: 3541.510, ratio: 1.168
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1293.403, activation diff: 4325.913, ratio: 3.345
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 3438.284, activation diff: 4731.687, ratio: 1.376
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 1403.405, activation diff: 4841.689, ratio: 3.450
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 3470.386, activation diff: 4873.791, ratio: 1.404
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 1414.320, activation diff: 4884.706, ratio: 3.454
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 3473.662, activation diff: 4887.982, ratio: 1.407
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 1415.725, activation diff: 4889.388, ratio: 3.454
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 3474.099, activation diff: 4889.824, ratio: 1.408
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 1415.944, activation diff: 4890.042, ratio: 3.454
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 3474.172, activation diff: 4890.116, ratio: 1.408
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 1415.981, activation diff: 4890.154, ratio: 3.454

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 1416.0
#   number of spread. activ. pulses: 15
#   running time: 1578

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
