###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 154.526, activation diff: 169.593, ratio: 1.098
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 39.440, activation diff: 193.966, ratio: 4.918
#   pulse 4: activated nodes: 7690, borderline nodes: 4607, overall activation: 562.011, activation diff: 601.451, ratio: 1.070
#   pulse 5: activated nodes: 8536, borderline nodes: 3506, overall activation: 55.509, activation diff: 617.521, ratio: 11.125
#   pulse 6: activated nodes: 8536, borderline nodes: 3506, overall activation: 615.192, activation diff: 670.702, ratio: 1.090
#   pulse 7: activated nodes: 8638, borderline nodes: 3410, overall activation: 57.266, activation diff: 672.458, ratio: 11.743
#   pulse 8: activated nodes: 8638, borderline nodes: 3410, overall activation: 618.707, activation diff: 675.973, ratio: 1.093
#   pulse 9: activated nodes: 8642, borderline nodes: 3410, overall activation: 57.423, activation diff: 676.130, ratio: 11.774
#   pulse 10: activated nodes: 8642, borderline nodes: 3410, overall activation: 618.934, activation diff: 676.357, ratio: 1.093
#   pulse 11: activated nodes: 8642, borderline nodes: 3410, overall activation: 57.440, activation diff: 676.374, ratio: 11.775
#   pulse 12: activated nodes: 8642, borderline nodes: 3410, overall activation: 618.958, activation diff: 676.398, ratio: 1.093
#   pulse 13: activated nodes: 8642, borderline nodes: 3410, overall activation: 57.442, activation diff: 676.400, ratio: 11.775
#   pulse 14: activated nodes: 8642, borderline nodes: 3410, overall activation: 618.961, activation diff: 676.403, ratio: 1.093
#   pulse 15: activated nodes: 8642, borderline nodes: 3410, overall activation: 57.443, activation diff: 676.403, ratio: 11.775

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8642
#   final overall activation: 57.4
#   number of spread. activ. pulses: 15
#   running time: 467

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
