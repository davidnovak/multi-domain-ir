###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 44.679, activation diff: 37.215, ratio: 0.833
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 99.499, activation diff: 56.020, ratio: 0.563
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 213.131, activation diff: 114.194, ratio: 0.536
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 379.445, activation diff: 166.494, ratio: 0.439
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 588.728, activation diff: 209.303, ratio: 0.356
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 823.989, activation diff: 235.261, ratio: 0.286
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 1067.588, activation diff: 243.599, ratio: 0.228
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1305.532, activation diff: 237.944, ratio: 0.182
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 1528.548, activation diff: 223.016, ratio: 0.146
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1731.425, activation diff: 202.877, ratio: 0.117
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 1911.941, activation diff: 180.516, ratio: 0.094
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 2069.885, activation diff: 157.944, ratio: 0.076
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 2206.291, activation diff: 136.407, ratio: 0.062
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 2322.894, activation diff: 116.603, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 2322.9
#   number of spread. activ. pulses: 15
#   running time: 523

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.92454284   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.7928151   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.7766772   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.704913   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.6883829   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   6   0.65847087   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.6570729   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   8   0.6510324   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.6458847   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.64440215   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   11   0.64383113   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.64242065   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.63740087   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   14   0.63669187   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.63658285   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   16   0.6360251   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   17   0.6359678   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.6350501   REFERENCES
1   Q1   europesincenapol00leveuoft_16   19   0.6337477   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   20   0.6317872   REFERENCES
