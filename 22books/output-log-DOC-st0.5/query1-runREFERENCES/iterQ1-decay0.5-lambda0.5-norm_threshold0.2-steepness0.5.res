###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 42.075, activation diff: 34.075, ratio: 0.810
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 67.640, activation diff: 26.934, ratio: 0.398
#   pulse 4: activated nodes: 7467, borderline nodes: 4712, overall activation: 185.061, activation diff: 117.751, ratio: 0.636
#   pulse 5: activated nodes: 8243, borderline nodes: 3777, overall activation: 315.295, activation diff: 130.235, ratio: 0.413
#   pulse 6: activated nodes: 8397, borderline nodes: 3656, overall activation: 438.433, activation diff: 123.139, ratio: 0.281
#   pulse 7: activated nodes: 8552, borderline nodes: 3499, overall activation: 536.722, activation diff: 98.289, ratio: 0.183
#   pulse 8: activated nodes: 8636, borderline nodes: 3447, overall activation: 607.997, activation diff: 71.274, ratio: 0.117
#   pulse 9: activated nodes: 8647, borderline nodes: 3407, overall activation: 656.405, activation diff: 48.408, ratio: 0.074
#   pulse 10: activated nodes: 8653, borderline nodes: 3406, overall activation: 687.743, activation diff: 31.338, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8653
#   final overall activation: 687.7
#   number of spread. activ. pulses: 10
#   running time: 394

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9344248   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.75751734   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.7514   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.6866957   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.6032516   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5014707   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.39878517   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.396782   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.3946814   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.39130187   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.38744   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.37228382   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.36004448   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.35921764   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.3586524   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.3489131   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.34570962   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.34540555   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.34337217   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.3432874   REFERENCES
