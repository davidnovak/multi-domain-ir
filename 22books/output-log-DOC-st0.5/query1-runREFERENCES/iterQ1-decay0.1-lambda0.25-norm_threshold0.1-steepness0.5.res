###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 268.868, activation diff: 268.680, ratio: 0.999
#   pulse 3: activated nodes: 8351, borderline nodes: 3639, overall activation: 350.353, activation diff: 269.370, ratio: 0.769
#   pulse 4: activated nodes: 10423, borderline nodes: 4244, overall activation: 1694.625, activation diff: 1386.493, ratio: 0.818
#   pulse 5: activated nodes: 11159, borderline nodes: 2581, overall activation: 2685.478, activation diff: 992.326, ratio: 0.370
#   pulse 6: activated nodes: 11270, borderline nodes: 704, overall activation: 3637.534, activation diff: 952.056, ratio: 0.262
#   pulse 7: activated nodes: 11308, borderline nodes: 404, overall activation: 4186.819, activation diff: 549.285, ratio: 0.131
#   pulse 8: activated nodes: 11318, borderline nodes: 208, overall activation: 4460.221, activation diff: 273.402, ratio: 0.061
#   pulse 9: activated nodes: 11326, borderline nodes: 122, overall activation: 4588.977, activation diff: 128.756, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11326
#   final overall activation: 4589.0
#   number of spread. activ. pulses: 9
#   running time: 488

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9707485   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8963746   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.8943809   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.8934367   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.89147055   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   6   0.8904342   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   7   0.8900691   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.89004034   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   9   0.88993186   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.8894832   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.88905495   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.8887014   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.88845575   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.8883876   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.88792765   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   16   0.88778275   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.8876778   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   18   0.88740885   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   19   0.8869219   REFERENCES
1   Q1   encyclopediaame28unkngoog_515   20   0.88667923   REFERENCES
