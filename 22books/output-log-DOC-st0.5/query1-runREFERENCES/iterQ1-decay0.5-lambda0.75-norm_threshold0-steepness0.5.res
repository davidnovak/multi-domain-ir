###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 33.430, activation diff: 25.965, ratio: 0.777
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 66.465, activation diff: 34.236, ratio: 0.515
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 122.419, activation diff: 56.558, ratio: 0.462
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 193.683, activation diff: 71.492, ratio: 0.369
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 272.196, activation diff: 78.543, ratio: 0.289
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 351.300, activation diff: 79.104, ratio: 0.225
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 426.444, activation diff: 75.144, ratio: 0.176
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 494.975, activation diff: 68.531, ratio: 0.138
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 555.662, activation diff: 60.687, ratio: 0.109
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 608.232, activation diff: 52.571, ratio: 0.086
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 653.010, activation diff: 44.778, ratio: 0.069
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 690.648, activation diff: 37.638, ratio: 0.054
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 721.950, activation diff: 31.302, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 721.9
#   number of spread. activ. pulses: 14
#   running time: 449

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.89705753   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.7308099   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.7294004   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.65744174   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.58909065   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.49947453   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.37139556   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.3693815   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.36643356   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   10   0.3633709   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   11   0.36332363   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.34816298   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.3371013   REFERENCES
1   Q1   politicalsketche00retsrich_159   14   0.3366651   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   15   0.3365748   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   16   0.32678932   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.32591727   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.3251239   REFERENCES
1   Q1   politicalsketche00retsrich_153   19   0.32329127   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.32219037   REFERENCES
