###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 276.519, activation diff: 291.587, ratio: 1.054
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 193.453, activation diff: 469.972, ratio: 2.429
#   pulse 4: activated nodes: 9640, borderline nodes: 5841, overall activation: 2116.741, activation diff: 2310.193, ratio: 1.091
#   pulse 5: activated nodes: 10979, borderline nodes: 3460, overall activation: 914.022, activation diff: 3030.762, ratio: 3.316
#   pulse 6: activated nodes: 11164, borderline nodes: 1527, overall activation: 3021.735, activation diff: 3935.757, ratio: 1.302
#   pulse 7: activated nodes: 11244, borderline nodes: 1100, overall activation: 1169.419, activation diff: 4191.154, ratio: 3.584
#   pulse 8: activated nodes: 11293, borderline nodes: 607, overall activation: 3110.421, activation diff: 4279.840, ratio: 1.376
#   pulse 9: activated nodes: 11308, borderline nodes: 553, overall activation: 1199.731, activation diff: 4310.152, ratio: 3.593
#   pulse 10: activated nodes: 11308, borderline nodes: 515, overall activation: 3121.595, activation diff: 4321.326, ratio: 1.384
#   pulse 11: activated nodes: 11308, borderline nodes: 508, overall activation: 1204.358, activation diff: 4325.953, ratio: 3.592
#   pulse 12: activated nodes: 11309, borderline nodes: 502, overall activation: 3123.878, activation diff: 4328.236, ratio: 1.386
#   pulse 13: activated nodes: 11309, borderline nodes: 502, overall activation: 1205.429, activation diff: 4329.307, ratio: 3.592
#   pulse 14: activated nodes: 11310, borderline nodes: 502, overall activation: 3124.435, activation diff: 4329.864, ratio: 1.386
#   pulse 15: activated nodes: 11310, borderline nodes: 502, overall activation: 1205.736, activation diff: 4330.171, ratio: 3.591

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11310
#   final overall activation: 1205.7
#   number of spread. activ. pulses: 15
#   running time: 599

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
