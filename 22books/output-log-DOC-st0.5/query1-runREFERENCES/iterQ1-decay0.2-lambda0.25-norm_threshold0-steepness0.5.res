###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 330.748, activation diff: 328.692, ratio: 0.994
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 460.421, activation diff: 255.073, ratio: 0.554
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 1577.775, activation diff: 1117.527, ratio: 0.708
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2376.735, activation diff: 798.960, ratio: 0.336
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 2945.550, activation diff: 568.814, ratio: 0.193
#   pulse 7: activated nodes: 11335, borderline nodes: 30, overall activation: 3246.744, activation diff: 301.194, ratio: 0.093
#   pulse 8: activated nodes: 11335, borderline nodes: 30, overall activation: 3390.826, activation diff: 144.082, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11335
#   final overall activation: 3390.8
#   number of spread. activ. pulses: 8
#   running time: 416

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97036576   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8697144   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8503872   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.7913593   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   5   0.7857777   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.78267395   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   7   0.7782632   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.7761074   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.77576673   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.7749462   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.7741706   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   12   0.7738515   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   13   0.773447   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   14   0.77281   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   15   0.7725917   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.7722416   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.77220374   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   18   0.7715032   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   19   0.7711526   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.7710682   REFERENCES
