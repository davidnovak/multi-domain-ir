###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 156.809, activation diff: 158.508, ratio: 1.011
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 149.086, activation diff: 147.962, ratio: 0.992
#   pulse 4: activated nodes: 9780, borderline nodes: 5573, overall activation: 844.896, activation diff: 736.513, ratio: 0.872
#   pulse 5: activated nodes: 10863, borderline nodes: 3766, overall activation: 1270.273, activation diff: 438.943, ratio: 0.346
#   pulse 6: activated nodes: 11030, borderline nodes: 2555, overall activation: 1862.750, activation diff: 592.498, ratio: 0.318
#   pulse 7: activated nodes: 11196, borderline nodes: 1811, overall activation: 2232.433, activation diff: 369.683, ratio: 0.166
#   pulse 8: activated nodes: 11219, borderline nodes: 1116, overall activation: 2435.023, activation diff: 202.590, ratio: 0.083
#   pulse 9: activated nodes: 11246, borderline nodes: 944, overall activation: 2536.390, activation diff: 101.367, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11246
#   final overall activation: 2536.4
#   number of spread. activ. pulses: 9
#   running time: 429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9673679   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.85729146   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8314653   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.76205385   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.7364986   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7172589   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.71486425   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.71086437   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   9   0.70822763   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.70768416   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   11   0.7071508   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.7049552   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.7043876   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.7042153   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   15   0.7041509   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.70374316   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   17   0.7023859   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   18   0.70213664   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.7017734   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.70053124   REFERENCES
