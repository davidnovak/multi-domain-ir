###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 35.216, activation diff: 28.930, ratio: 0.822
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 58.172, activation diff: 24.999, ratio: 0.430
#   pulse 4: activated nodes: 7864, borderline nodes: 5992, overall activation: 231.992, activation diff: 174.522, ratio: 0.752
#   pulse 5: activated nodes: 9419, borderline nodes: 5174, overall activation: 489.535, activation diff: 257.570, ratio: 0.526
#   pulse 6: activated nodes: 10224, borderline nodes: 4835, overall activation: 894.722, activation diff: 405.186, ratio: 0.453
#   pulse 7: activated nodes: 10829, borderline nodes: 3725, overall activation: 1358.885, activation diff: 464.163, ratio: 0.342
#   pulse 8: activated nodes: 11050, borderline nodes: 2806, overall activation: 1800.890, activation diff: 442.005, ratio: 0.245
#   pulse 9: activated nodes: 11159, borderline nodes: 2160, overall activation: 2166.014, activation diff: 365.124, ratio: 0.169
#   pulse 10: activated nodes: 11208, borderline nodes: 1623, overall activation: 2443.039, activation diff: 277.025, ratio: 0.113
#   pulse 11: activated nodes: 11219, borderline nodes: 1327, overall activation: 2642.295, activation diff: 199.256, ratio: 0.075
#   pulse 12: activated nodes: 11220, borderline nodes: 1132, overall activation: 2780.784, activation diff: 138.489, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11220
#   final overall activation: 2780.8
#   number of spread. activ. pulses: 12
#   running time: 474

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.958375   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.82919365   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.80675656   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.78391385   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.76611924   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.7628375   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   7   0.75965095   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.7589431   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   9   0.75826645   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.7573463   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   11   0.75597966   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   12   0.75584495   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.754824   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.7545694   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.7528111   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   16   0.7526868   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   17   0.7525826   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.7521248   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   19   0.7512474   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   20   0.7490761   REFERENCES
