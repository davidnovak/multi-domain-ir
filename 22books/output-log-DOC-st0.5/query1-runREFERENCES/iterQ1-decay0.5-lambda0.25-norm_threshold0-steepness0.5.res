###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.007, activation diff: 19.007, ratio: 1.187
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 210.294, activation diff: 208.238, ratio: 0.990
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 199.549, activation diff: 68.065, ratio: 0.341
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 549.518, activation diff: 350.268, ratio: 0.637
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 721.032, activation diff: 171.519, ratio: 0.238
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 804.287, activation diff: 83.255, ratio: 0.104
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 836.779, activation diff: 32.492, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 836.8
#   number of spread. activ. pulses: 7
#   running time: 356

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9578884   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8413867   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.81522214   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7416693   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.6533095   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.58693093   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.41642734   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.41605714   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   9   0.41557008   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.41515583   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.40324956   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.3899692   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.378204   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.37812594   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.37781206   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.37117878   REFERENCES
1   Q1   essentialsinmod01howegoog_461   17   0.3651476   REFERENCES
1   Q1   essentialsinmod01howegoog_446   18   0.3642748   REFERENCES
1   Q1   politicalsketche00retsrich_148   19   0.3638593   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   20   0.36378032   REFERENCES
