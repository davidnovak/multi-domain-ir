###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 105.276, activation diff: 108.004, ratio: 1.026
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 83.127, activation diff: 109.279, ratio: 1.315
#   pulse 4: activated nodes: 9007, borderline nodes: 6014, overall activation: 624.595, activation diff: 579.516, ratio: 0.928
#   pulse 5: activated nodes: 10320, borderline nodes: 4410, overall activation: 911.189, activation diff: 325.699, ratio: 0.357
#   pulse 6: activated nodes: 10726, borderline nodes: 3818, overall activation: 1541.834, activation diff: 630.959, ratio: 0.409
#   pulse 7: activated nodes: 11076, borderline nodes: 2768, overall activation: 1960.652, activation diff: 418.818, ratio: 0.214
#   pulse 8: activated nodes: 11161, borderline nodes: 1931, overall activation: 2220.897, activation diff: 260.245, ratio: 0.117
#   pulse 9: activated nodes: 11208, borderline nodes: 1505, overall activation: 2359.792, activation diff: 138.895, ratio: 0.059
#   pulse 10: activated nodes: 11217, borderline nodes: 1260, overall activation: 2429.963, activation diff: 70.171, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11217
#   final overall activation: 2430.0
#   number of spread. activ. pulses: 10
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.96646374   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8530613   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.82507896   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7540194   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.7365507   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.71691084   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.7142774   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.7105229   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   9   0.708052   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.7073725   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   11   0.7071563   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.70442694   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.70400214   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   14   0.7033645   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.70320064   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   16   0.7025469   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   17   0.70245284   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   18   0.7014784   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   19   0.70119524   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.70083094   REFERENCES
