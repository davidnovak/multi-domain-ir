###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 9.336, activation diff: 6.336, ratio: 0.679
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 51.429, activation diff: 43.964, ratio: 0.855
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 121.129, activation diff: 70.900, ratio: 0.585
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 285.532, activation diff: 164.945, ratio: 0.578
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 546.032, activation diff: 260.656, ratio: 0.477
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 894.772, activation diff: 348.755, ratio: 0.390
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 1299.567, activation diff: 404.795, ratio: 0.311
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 1724.552, activation diff: 424.985, ratio: 0.246
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 2141.566, activation diff: 417.014, ratio: 0.195
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 2532.300, activation diff: 390.733, ratio: 0.154
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 2886.724, activation diff: 354.424, ratio: 0.123
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 3200.767, activation diff: 314.043, ratio: 0.098
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 3474.246, activation diff: 273.479, ratio: 0.079
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 3709.298, activation diff: 235.052, ratio: 0.063
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 3909.288, activation diff: 199.990, ratio: 0.051

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 3909.3
#   number of spread. activ. pulses: 15
#   running time: 503

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9282348   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8494495   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.83596116   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.8326197   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   5   0.8311944   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.830593   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.8302062   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.83000064   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.82950115   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.82790107   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   11   0.82736784   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   12   0.8266451   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   13   0.8257194   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.8255059   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.8247404   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   16   0.8238581   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   17   0.82255757   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   18   0.82241374   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.8216435   REFERENCES
1   Q1   encyclopediaame28unkngoog_127   20   0.8204521   REFERENCES
