###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.801, activation diff: 15.801, ratio: 1.234
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 125.302, activation diff: 128.031, ratio: 1.022
#   pulse 3: activated nodes: 7342, borderline nodes: 4710, overall activation: 113.381, activation diff: 145.425, ratio: 1.283
#   pulse 4: activated nodes: 9027, borderline nodes: 5994, overall activation: 945.471, activation diff: 893.004, ratio: 0.945
#   pulse 5: activated nodes: 10509, borderline nodes: 4009, overall activation: 1440.795, activation diff: 569.047, ratio: 0.395
#   pulse 6: activated nodes: 10920, borderline nodes: 3059, overall activation: 2594.295, activation diff: 1154.540, ratio: 0.445
#   pulse 7: activated nodes: 11187, borderline nodes: 2181, overall activation: 3383.017, activation diff: 788.722, ratio: 0.233
#   pulse 8: activated nodes: 11257, borderline nodes: 1135, overall activation: 3850.291, activation diff: 467.274, ratio: 0.121
#   pulse 9: activated nodes: 11278, borderline nodes: 828, overall activation: 4090.820, activation diff: 240.529, ratio: 0.059
#   pulse 10: activated nodes: 11295, borderline nodes: 651, overall activation: 4208.651, activation diff: 117.831, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11295
#   final overall activation: 4208.7
#   number of spread. activ. pulses: 10
#   running time: 447

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9677199   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.89608896   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.89356047   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.89262664   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.89059734   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   6   0.88926995   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   7   0.88903284   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   8   0.88903123   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   9   0.88893855   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.888419   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.88796014   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   12   0.88752866   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.8871193   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.8870481   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.88666654   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   16   0.8866097   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   17   0.8865873   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   18   0.886017   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   19   0.88565594   REFERENCES
1   Q1   encyclopediaame28unkngoog_515   20   0.8855688   REFERENCES
