###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.671, activation diff: 12.671, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 146.754, activation diff: 135.595, ratio: 0.924
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 283.772, activation diff: 137.713, ratio: 0.485
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 719.182, activation diff: 435.437, ratio: 0.605
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 1221.627, activation diff: 502.445, ratio: 0.411
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 1695.852, activation diff: 474.225, ratio: 0.280
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 2075.024, activation diff: 379.172, ratio: 0.183
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 2351.877, activation diff: 276.853, ratio: 0.118
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 2543.445, activation diff: 191.568, ratio: 0.075
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 2671.647, activation diff: 128.202, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 2671.6
#   number of spread. activ. pulses: 10
#   running time: 455

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.95993924   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.85132754   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.830228   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.76147807   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.728114   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7054627   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.7049679   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.6994411   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   9   0.69856346   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.6964363   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.6952392   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   12   0.69468564   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   13   0.69465274   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   14   0.6943573   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.69245434   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.6921735   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   17   0.6911714   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.690863   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   19   0.68874216   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.68743503   REFERENCES
