###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 13.883, activation diff: 16.883, ratio: 1.216
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 186.909, activation diff: 188.608, ratio: 1.009
#   pulse 3: activated nodes: 7841, borderline nodes: 4313, overall activation: 206.108, activation diff: 205.694, ratio: 0.998
#   pulse 4: activated nodes: 9802, borderline nodes: 5532, overall activation: 1305.597, activation diff: 1168.030, ratio: 0.895
#   pulse 5: activated nodes: 10977, borderline nodes: 3473, overall activation: 2046.928, activation diff: 768.963, ratio: 0.376
#   pulse 6: activated nodes: 11140, borderline nodes: 1761, overall activation: 3124.917, activation diff: 1078.098, ratio: 0.345
#   pulse 7: activated nodes: 11234, borderline nodes: 1136, overall activation: 3808.322, activation diff: 683.405, ratio: 0.179
#   pulse 8: activated nodes: 11288, borderline nodes: 570, overall activation: 4171.737, activation diff: 363.415, ratio: 0.087
#   pulse 9: activated nodes: 11302, borderline nodes: 436, overall activation: 4348.795, activation diff: 177.059, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11302
#   final overall activation: 4348.8
#   number of spread. activ. pulses: 9
#   running time: 500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9686103   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.89583576   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.89299124   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.8919088   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.8896122   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   6   0.8884927   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   7   0.8884915   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   8   0.88836336   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   9   0.8883465   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   10   0.8879539   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   11   0.88727295   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.8872282   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   13   0.88653564   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   14   0.886438   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.8864224   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   16   0.8862115   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   17   0.88580203   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   18   0.88531494   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   19   0.8852294   REFERENCES
1   Q1   encyclopediaame28unkngoog_515   20   0.8846078   REFERENCES
