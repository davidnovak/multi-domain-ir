###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 313.096, activation diff: 329.606, ratio: 1.053
#   pulse 3: activated nodes: 8042, borderline nodes: 4149, overall activation: 170.047, activation diff: 483.142, ratio: 2.841
#   pulse 4: activated nodes: 10054, borderline nodes: 5102, overall activation: 1577.706, activation diff: 1747.753, ratio: 1.108
#   pulse 5: activated nodes: 11066, borderline nodes: 3132, overall activation: 539.049, activation diff: 2116.755, ratio: 3.927
#   pulse 6: activated nodes: 11184, borderline nodes: 1416, overall activation: 1948.106, activation diff: 2487.155, ratio: 1.277
#   pulse 7: activated nodes: 11230, borderline nodes: 1048, overall activation: 634.345, activation diff: 2582.451, ratio: 4.071
#   pulse 8: activated nodes: 11236, borderline nodes: 811, overall activation: 1982.160, activation diff: 2616.505, ratio: 1.320
#   pulse 9: activated nodes: 11246, borderline nodes: 796, overall activation: 644.702, activation diff: 2626.862, ratio: 4.075
#   pulse 10: activated nodes: 11246, borderline nodes: 784, overall activation: 1985.445, activation diff: 2630.148, ratio: 1.325
#   pulse 11: activated nodes: 11246, borderline nodes: 782, overall activation: 645.810, activation diff: 2631.255, ratio: 4.074
#   pulse 12: activated nodes: 11246, borderline nodes: 782, overall activation: 1985.794, activation diff: 2631.604, ratio: 1.325
#   pulse 13: activated nodes: 11246, borderline nodes: 782, overall activation: 645.939, activation diff: 2631.733, ratio: 4.074
#   pulse 14: activated nodes: 11246, borderline nodes: 782, overall activation: 1985.835, activation diff: 2631.774, ratio: 1.325
#   pulse 15: activated nodes: 11246, borderline nodes: 782, overall activation: 645.957, activation diff: 2631.792, ratio: 4.074

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11246
#   final overall activation: 646.0
#   number of spread. activ. pulses: 15
#   running time: 648

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
