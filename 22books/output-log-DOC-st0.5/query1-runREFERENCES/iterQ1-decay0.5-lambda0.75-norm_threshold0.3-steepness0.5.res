###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 9.165, activation diff: 3.048, ratio: 0.333
#   pulse 3: activated nodes: 5574, borderline nodes: 5253, overall activation: 12.411, activation diff: 4.699, ratio: 0.379
#   pulse 4: activated nodes: 6309, borderline nodes: 5147, overall activation: 18.076, activation diff: 6.961, ratio: 0.385
#   pulse 5: activated nodes: 6545, borderline nodes: 5050, overall activation: 30.287, activation diff: 13.280, ratio: 0.438
#   pulse 6: activated nodes: 6936, borderline nodes: 4801, overall activation: 52.518, activation diff: 22.911, ratio: 0.436
#   pulse 7: activated nodes: 7348, borderline nodes: 4630, overall activation: 86.144, activation diff: 33.958, ratio: 0.394
#   pulse 8: activated nodes: 7681, borderline nodes: 4345, overall activation: 129.506, activation diff: 43.470, ratio: 0.336
#   pulse 9: activated nodes: 7914, borderline nodes: 4047, overall activation: 179.597, activation diff: 50.119, ratio: 0.279
#   pulse 10: activated nodes: 8098, borderline nodes: 3906, overall activation: 232.830, activation diff: 53.235, ratio: 0.229
#   pulse 11: activated nodes: 8216, borderline nodes: 3778, overall activation: 286.112, activation diff: 53.283, ratio: 0.186
#   pulse 12: activated nodes: 8318, borderline nodes: 3673, overall activation: 337.250, activation diff: 51.137, ratio: 0.152
#   pulse 13: activated nodes: 8428, borderline nodes: 3640, overall activation: 384.872, activation diff: 47.622, ratio: 0.124
#   pulse 14: activated nodes: 8472, borderline nodes: 3547, overall activation: 428.222, activation diff: 43.350, ratio: 0.101
#   pulse 15: activated nodes: 8511, borderline nodes: 3508, overall activation: 466.921, activation diff: 38.699, ratio: 0.083

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8511
#   final overall activation: 466.9
#   number of spread. activ. pulses: 15
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.80110055   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.5674774   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.51963216   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   4   0.48694625   REFERENCES
1   Q1   cambridgemodern09protgoog_449   5   0.42250156   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   6   0.32734853   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.3247162   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.3174959   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   9   0.31519723   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   10   0.30863646   REFERENCES
1   Q1   politicalsketche00retsrich_154   11   0.3077923   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   12   0.2975193   REFERENCES
1   Q1   politicalsketche00retsrich_153   13   0.29502687   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   14   0.2937362   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.29367316   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   16   0.2933651   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.2896536   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.28872162   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.28636032   REFERENCES
1   Q1   essentialsinmod01howegoog_37   20   0.28200978   REFERENCES
