###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 239.875, activation diff: 239.687, ratio: 0.999
#   pulse 3: activated nodes: 8351, borderline nodes: 3639, overall activation: 284.092, activation diff: 211.377, ratio: 0.744
#   pulse 4: activated nodes: 10416, borderline nodes: 4335, overall activation: 1270.062, activation diff: 1016.688, ratio: 0.801
#   pulse 5: activated nodes: 11143, borderline nodes: 2791, overall activation: 1957.204, activation diff: 687.810, ratio: 0.351
#   pulse 6: activated nodes: 11248, borderline nodes: 1038, overall activation: 2613.852, activation diff: 656.649, ratio: 0.251
#   pulse 7: activated nodes: 11280, borderline nodes: 611, overall activation: 2994.567, activation diff: 380.714, ratio: 0.127
#   pulse 8: activated nodes: 11303, borderline nodes: 448, overall activation: 3186.792, activation diff: 192.226, ratio: 0.060
#   pulse 9: activated nodes: 11313, borderline nodes: 374, overall activation: 3278.003, activation diff: 91.210, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11313
#   final overall activation: 3278.0
#   number of spread. activ. pulses: 9
#   running time: 433

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.97022307   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.86585927   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8456861   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   4   0.791854   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   5   0.783213   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   6   0.77996624   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   7   0.7786785   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   8   0.776409   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.7763144   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.77548134   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   11   0.775032   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   12   0.7745657   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   13   0.77430785   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   14   0.77292913   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   15   0.7729016   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   16   0.77269566   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   17   0.7725841   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   18   0.77208704   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   19   0.7716298   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   20   0.7713827   REFERENCES
