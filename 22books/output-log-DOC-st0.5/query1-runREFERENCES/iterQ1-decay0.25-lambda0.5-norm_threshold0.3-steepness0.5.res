###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 33.546, activation diff: 27.260, ratio: 0.813
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 54.698, activation diff: 23.215, ratio: 0.424
#   pulse 4: activated nodes: 7853, borderline nodes: 5984, overall activation: 209.796, activation diff: 155.824, ratio: 0.743
#   pulse 5: activated nodes: 9287, borderline nodes: 5194, overall activation: 432.218, activation diff: 222.460, ratio: 0.515
#   pulse 6: activated nodes: 10151, borderline nodes: 4964, overall activation: 764.240, activation diff: 332.022, ratio: 0.434
#   pulse 7: activated nodes: 10733, borderline nodes: 3960, overall activation: 1136.399, activation diff: 372.158, ratio: 0.327
#   pulse 8: activated nodes: 10985, borderline nodes: 3083, overall activation: 1489.267, activation diff: 352.868, ratio: 0.237
#   pulse 9: activated nodes: 11111, borderline nodes: 2437, overall activation: 1781.011, activation diff: 291.744, ratio: 0.164
#   pulse 10: activated nodes: 11169, borderline nodes: 1941, overall activation: 2003.071, activation diff: 222.060, ratio: 0.111
#   pulse 11: activated nodes: 11202, borderline nodes: 1615, overall activation: 2163.438, activation diff: 160.367, ratio: 0.074
#   pulse 12: activated nodes: 11216, borderline nodes: 1421, overall activation: 2275.417, activation diff: 111.978, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11216
#   final overall activation: 2275.4
#   number of spread. activ. pulses: 12
#   running time: 475

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.95726925   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.82654184   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.80273676   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.73124987   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.7283813   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.7015362   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   7   0.69883525   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.6955153   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   9   0.6933825   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.6903533   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   11   0.68910027   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   12   0.6880082   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   13   0.6879903   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   14   0.6864191   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   15   0.68422365   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   16   0.6840663   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   17   0.68388224   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.6828934   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   19   0.6779734   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.6771264   REFERENCES
