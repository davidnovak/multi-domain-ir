###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 416.057, activation diff: 433.993, ratio: 1.043
#   pulse 3: activated nodes: 8476, borderline nodes: 3451, overall activation: 229.985, activation diff: 646.042, ratio: 2.809
#   pulse 4: activated nodes: 10579, borderline nodes: 3669, overall activation: 1776.392, activation diff: 2006.377, ratio: 1.129
#   pulse 5: activated nodes: 11193, borderline nodes: 2130, overall activation: 614.166, activation diff: 2390.559, ratio: 3.892
#   pulse 6: activated nodes: 11259, borderline nodes: 740, overall activation: 2052.830, activation diff: 2666.996, ratio: 1.299
#   pulse 7: activated nodes: 11288, borderline nodes: 595, overall activation: 685.381, activation diff: 2738.210, ratio: 3.995
#   pulse 8: activated nodes: 11289, borderline nodes: 569, overall activation: 2075.326, activation diff: 2760.707, ratio: 1.330
#   pulse 9: activated nodes: 11296, borderline nodes: 542, overall activation: 692.358, activation diff: 2767.684, ratio: 3.997
#   pulse 10: activated nodes: 11296, borderline nodes: 538, overall activation: 2077.453, activation diff: 2769.812, ratio: 1.333
#   pulse 11: activated nodes: 11297, borderline nodes: 537, overall activation: 693.077, activation diff: 2770.530, ratio: 3.997
#   pulse 12: activated nodes: 11297, borderline nodes: 537, overall activation: 2077.714, activation diff: 2770.791, ratio: 1.334
#   pulse 13: activated nodes: 11297, borderline nodes: 536, overall activation: 693.165, activation diff: 2770.879, ratio: 3.997
#   pulse 14: activated nodes: 11297, borderline nodes: 536, overall activation: 2077.762, activation diff: 2770.927, ratio: 1.334
#   pulse 15: activated nodes: 11297, borderline nodes: 536, overall activation: 693.179, activation diff: 2770.941, ratio: 3.997

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11297
#   final overall activation: 693.2
#   number of spread. activ. pulses: 15
#   running time: 599

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
