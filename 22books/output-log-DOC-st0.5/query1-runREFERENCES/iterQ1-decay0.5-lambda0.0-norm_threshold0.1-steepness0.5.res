###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.935, activation diff: 23.935, ratio: 1.335
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 278.219, activation diff: 296.154, ratio: 1.064
#   pulse 3: activated nodes: 8476, borderline nodes: 3451, overall activation: 49.427, activation diff: 327.645, ratio: 6.629
#   pulse 4: activated nodes: 8476, borderline nodes: 3451, overall activation: 719.284, activation diff: 768.710, ratio: 1.069
#   pulse 5: activated nodes: 8660, borderline nodes: 3406, overall activation: 57.925, activation diff: 777.209, ratio: 13.418
#   pulse 6: activated nodes: 8660, borderline nodes: 3406, overall activation: 737.095, activation diff: 795.020, ratio: 1.079
#   pulse 7: activated nodes: 8660, borderline nodes: 3406, overall activation: 58.473, activation diff: 795.568, ratio: 13.606
#   pulse 8: activated nodes: 8660, borderline nodes: 3406, overall activation: 737.775, activation diff: 796.249, ratio: 1.079
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 58.517, activation diff: 796.293, ratio: 13.608
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 737.825, activation diff: 796.343, ratio: 1.079
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 58.522, activation diff: 796.347, ratio: 13.608
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 737.830, activation diff: 796.352, ratio: 1.079
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 58.523, activation diff: 796.353, ratio: 13.608
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 737.831, activation diff: 796.354, ratio: 1.079
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 58.523, activation diff: 796.354, ratio: 13.608

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 58.5
#   number of spread. activ. pulses: 15
#   running time: 464

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   2   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   3   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   4   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_180   6   0.0   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.0   REFERENCES
1   Q1   historyofuniteds07good_347   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_342   9   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_222   10   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_177   11   0.0   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_181   13   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   14   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   15   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
