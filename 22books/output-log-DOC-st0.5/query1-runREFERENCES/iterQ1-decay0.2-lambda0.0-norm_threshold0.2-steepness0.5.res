###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 333.816, activation diff: 350.326, ratio: 1.049
#   pulse 3: activated nodes: 8042, borderline nodes: 4149, overall activation: 203.117, activation diff: 536.933, ratio: 2.643
#   pulse 4: activated nodes: 10057, borderline nodes: 5065, overall activation: 1850.094, activation diff: 2053.211, ratio: 1.110
#   pulse 5: activated nodes: 11082, borderline nodes: 3041, overall activation: 693.853, activation diff: 2543.947, ratio: 3.666
#   pulse 6: activated nodes: 11208, borderline nodes: 1148, overall activation: 2321.580, activation diff: 3015.433, ratio: 1.299
#   pulse 7: activated nodes: 11251, borderline nodes: 820, overall activation: 819.590, activation diff: 3141.170, ratio: 3.833
#   pulse 8: activated nodes: 11288, borderline nodes: 629, overall activation: 2363.996, activation diff: 3183.586, ratio: 1.347
#   pulse 9: activated nodes: 11295, borderline nodes: 599, overall activation: 833.147, activation diff: 3197.144, ratio: 3.837
#   pulse 10: activated nodes: 11299, borderline nodes: 579, overall activation: 2368.222, activation diff: 3201.370, ratio: 1.352
#   pulse 11: activated nodes: 11299, borderline nodes: 578, overall activation: 834.775, activation diff: 3202.997, ratio: 3.837
#   pulse 12: activated nodes: 11300, borderline nodes: 576, overall activation: 2368.778, activation diff: 3203.553, ratio: 1.352
#   pulse 13: activated nodes: 11300, borderline nodes: 575, overall activation: 835.056, activation diff: 3203.834, ratio: 3.837
#   pulse 14: activated nodes: 11300, borderline nodes: 575, overall activation: 2368.885, activation diff: 3203.941, ratio: 1.353
#   pulse 15: activated nodes: 11300, borderline nodes: 575, overall activation: 835.131, activation diff: 3204.016, ratio: 3.837

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11300
#   final overall activation: 835.1
#   number of spread. activ. pulses: 15
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
