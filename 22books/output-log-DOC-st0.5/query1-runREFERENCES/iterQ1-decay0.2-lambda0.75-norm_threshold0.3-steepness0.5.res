###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.2, 0.75, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 8.267, activation diff: 5.267, ratio: 0.637
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 9.339, activation diff: 3.222, ratio: 0.345
#   pulse 3: activated nodes: 5574, borderline nodes: 5253, overall activation: 14.720, activation diff: 6.835, ratio: 0.464
#   pulse 4: activated nodes: 6309, borderline nodes: 5147, overall activation: 24.815, activation diff: 11.325, ratio: 0.456
#   pulse 5: activated nodes: 7087, borderline nodes: 5535, overall activation: 50.523, activation diff: 26.633, ratio: 0.527
#   pulse 6: activated nodes: 7942, borderline nodes: 5568, overall activation: 99.834, activation diff: 49.809, ratio: 0.499
#   pulse 7: activated nodes: 8978, borderline nodes: 5717, overall activation: 180.153, activation diff: 80.474, ratio: 0.447
#   pulse 8: activated nodes: 9641, borderline nodes: 5355, overall activation: 296.345, activation diff: 116.242, ratio: 0.392
#   pulse 9: activated nodes: 10196, borderline nodes: 4966, overall activation: 450.163, activation diff: 153.820, ratio: 0.342
#   pulse 10: activated nodes: 10581, borderline nodes: 4383, overall activation: 638.721, activation diff: 188.559, ratio: 0.295
#   pulse 11: activated nodes: 10809, borderline nodes: 3791, overall activation: 853.218, activation diff: 214.497, ratio: 0.251
#   pulse 12: activated nodes: 10958, borderline nodes: 3247, overall activation: 1080.816, activation diff: 227.597, ratio: 0.211
#   pulse 13: activated nodes: 11065, borderline nodes: 2810, overall activation: 1309.156, activation diff: 228.341, ratio: 0.174
#   pulse 14: activated nodes: 11110, borderline nodes: 2478, overall activation: 1528.759, activation diff: 219.603, ratio: 0.144
#   pulse 15: activated nodes: 11161, borderline nodes: 2164, overall activation: 1733.505, activation diff: 204.746, ratio: 0.118

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11161
#   final overall activation: 1733.5
#   number of spread. activ. pulses: 15
#   running time: 513

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.846738   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.6663637   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   3   0.6499359   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   4   0.641696   REFERENCES
1   Q1   europesincenapol00leveuoft_16   5   0.6105044   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   6   0.61038864   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   7   0.6023151   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   8   0.5991598   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   9   0.5990692   REFERENCES
1   Q1   cambridgemodern09protgoog_449   10   0.5962582   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   11   0.59610665   REFERENCES
1   Q1   essentialsinmod01howegoog_14   12   0.59451973   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.5934304   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.5914806   REFERENCES
1   Q1   europesincenapol00leveuoft_353   15   0.59054387   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   16   0.59043616   REFERENCES
1   Q1   essentialsinmod01howegoog_471   17   0.5842194   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   18   0.5813917   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   19   0.5811639   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   20   0.580379   REFERENCES
