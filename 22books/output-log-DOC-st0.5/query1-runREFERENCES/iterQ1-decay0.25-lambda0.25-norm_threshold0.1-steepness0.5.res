###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.25, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 14.952, activation diff: 17.952, ratio: 1.201
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 225.378, activation diff: 225.191, ratio: 0.999
#   pulse 3: activated nodes: 8351, borderline nodes: 3639, overall activation: 253.413, activation diff: 184.832, ratio: 0.729
#   pulse 4: activated nodes: 10409, borderline nodes: 4376, overall activation: 1088.099, activation diff: 860.062, ratio: 0.790
#   pulse 5: activated nodes: 11117, borderline nodes: 2860, overall activation: 1647.832, activation diff: 560.149, ratio: 0.340
#   pulse 6: activated nodes: 11198, borderline nodes: 1186, overall activation: 2174.035, activation diff: 526.203, ratio: 0.242
#   pulse 7: activated nodes: 11244, borderline nodes: 727, overall activation: 2477.663, activation diff: 303.628, ratio: 0.123
#   pulse 8: activated nodes: 11254, borderline nodes: 617, overall activation: 2631.549, activation diff: 153.886, ratio: 0.058
#   pulse 9: activated nodes: 11263, borderline nodes: 583, overall activation: 2704.974, activation diff: 73.426, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11263
#   final overall activation: 2705.0
#   number of spread. activ. pulses: 9
#   running time: 415

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9698491   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8656893   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.8438293   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.77685493   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   5   0.73776144   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   6   0.7209231   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   7   0.71753955   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.7146281   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   9   0.71260774   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   10   0.7118223   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   11   0.71169114   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   12   0.70936465   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   13   0.7091298   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   14   0.70876884   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   15   0.7083077   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   16   0.7079588   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   17   0.70686793   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   18   0.70622617   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   19   0.7059392   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   20   0.7055775   REFERENCES
