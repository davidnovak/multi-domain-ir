###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.510, activation diff: 22.510, ratio: 1.363
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 375.256, activation diff: 391.766, ratio: 1.044
#   pulse 3: activated nodes: 8042, borderline nodes: 4149, overall activation: 276.522, activation diff: 651.778, ratio: 2.357
#   pulse 4: activated nodes: 10072, borderline nodes: 5008, overall activation: 2482.178, activation diff: 2758.701, ratio: 1.111
#   pulse 5: activated nodes: 11109, borderline nodes: 2890, overall activation: 1057.583, activation diff: 3539.761, ratio: 3.347
#   pulse 6: activated nodes: 11262, borderline nodes: 844, overall activation: 3173.970, activation diff: 4231.552, ratio: 1.333
#   pulse 7: activated nodes: 11306, borderline nodes: 580, overall activation: 1249.656, activation diff: 4423.626, ratio: 3.540
#   pulse 8: activated nodes: 11308, borderline nodes: 350, overall activation: 3234.549, activation diff: 4484.205, ratio: 1.386
#   pulse 9: activated nodes: 11312, borderline nodes: 312, overall activation: 1271.325, activation diff: 4505.874, ratio: 3.544
#   pulse 10: activated nodes: 11314, borderline nodes: 280, overall activation: 3242.552, activation diff: 4513.878, ratio: 1.392
#   pulse 11: activated nodes: 11314, borderline nodes: 279, overall activation: 1274.659, activation diff: 4517.211, ratio: 3.544
#   pulse 12: activated nodes: 11314, borderline nodes: 279, overall activation: 3243.912, activation diff: 4518.571, ratio: 1.393
#   pulse 13: activated nodes: 11314, borderline nodes: 279, overall activation: 1275.332, activation diff: 4519.245, ratio: 3.544
#   pulse 14: activated nodes: 11314, borderline nodes: 279, overall activation: 3244.170, activation diff: 4519.502, ratio: 1.393
#   pulse 15: activated nodes: 11314, borderline nodes: 278, overall activation: 1275.486, activation diff: 4519.656, ratio: 3.543

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11314
#   final overall activation: 1275.5
#   number of spread. activ. pulses: 15
#   running time: 604

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
