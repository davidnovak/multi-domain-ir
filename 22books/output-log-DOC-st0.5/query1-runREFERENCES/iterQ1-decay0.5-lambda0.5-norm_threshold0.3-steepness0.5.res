###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 10.534, activation diff: 10.534, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 25.197, activation diff: 18.911, ratio: 0.751
#   pulse 3: activated nodes: 6693, borderline nodes: 4990, overall activation: 38.304, activation diff: 15.350, ratio: 0.401
#   pulse 4: activated nodes: 6723, borderline nodes: 4930, overall activation: 118.823, activation diff: 81.440, ratio: 0.685
#   pulse 5: activated nodes: 7748, borderline nodes: 4230, overall activation: 219.332, activation diff: 100.586, ratio: 0.459
#   pulse 6: activated nodes: 8049, borderline nodes: 3984, overall activation: 332.192, activation diff: 112.860, ratio: 0.340
#   pulse 7: activated nodes: 8292, borderline nodes: 3732, overall activation: 431.437, activation diff: 99.245, ratio: 0.230
#   pulse 8: activated nodes: 8456, borderline nodes: 3593, overall activation: 509.511, activation diff: 78.074, ratio: 0.153
#   pulse 9: activated nodes: 8545, borderline nodes: 3500, overall activation: 566.627, activation diff: 57.115, ratio: 0.101
#   pulse 10: activated nodes: 8607, borderline nodes: 3456, overall activation: 606.190, activation diff: 39.563, ratio: 0.065
#   pulse 11: activated nodes: 8620, borderline nodes: 3433, overall activation: 632.479, activation diff: 26.289, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8620
#   final overall activation: 632.5
#   number of spread. activ. pulses: 11
#   running time: 409

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.93400973   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.7460173   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.72756493   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.67689145   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.5908487   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.47637135   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.39688706   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.39480442   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.39292532   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.38833785   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.3854151   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.36993524   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.35706326   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.356162   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.3554725   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.34553057   REFERENCES
1   Q1   politicalsketche00retsrich_148   17   0.3421198   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   18   0.3413169   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.33998662   REFERENCES
1   Q1   essentialsinmod01howegoog_461   20   0.3395695   REFERENCES
