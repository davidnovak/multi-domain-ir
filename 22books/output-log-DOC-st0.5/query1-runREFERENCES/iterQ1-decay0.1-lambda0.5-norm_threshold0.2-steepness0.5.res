###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.1, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.255, activation diff: 11.255, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 67.939, activation diff: 59.939, ratio: 0.882
#   pulse 3: activated nodes: 7424, borderline nodes: 4704, overall activation: 125.822, activation diff: 59.109, ratio: 0.470
#   pulse 4: activated nodes: 9120, borderline nodes: 6034, overall activation: 483.458, activation diff: 357.769, ratio: 0.740
#   pulse 5: activated nodes: 10491, borderline nodes: 4390, overall activation: 1011.041, activation diff: 527.583, ratio: 0.522
#   pulse 6: activated nodes: 10933, borderline nodes: 3392, overall activation: 1750.769, activation diff: 739.728, ratio: 0.423
#   pulse 7: activated nodes: 11177, borderline nodes: 2208, overall activation: 2481.654, activation diff: 730.884, ratio: 0.295
#   pulse 8: activated nodes: 11225, borderline nodes: 1228, overall activation: 3090.260, activation diff: 608.606, ratio: 0.197
#   pulse 9: activated nodes: 11269, borderline nodes: 812, overall activation: 3547.649, activation diff: 457.389, ratio: 0.129
#   pulse 10: activated nodes: 11292, borderline nodes: 594, overall activation: 3871.124, activation diff: 323.474, ratio: 0.084
#   pulse 11: activated nodes: 11302, borderline nodes: 469, overall activation: 4091.668, activation diff: 220.544, ratio: 0.054
#   pulse 12: activated nodes: 11309, borderline nodes: 398, overall activation: 4238.914, activation diff: 147.246, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11309
#   final overall activation: 4238.9
#   number of spread. activ. pulses: 12
#   running time: 479

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9644968   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   2   0.8925008   REFERENCES
1   Q1   encyclopediaame28unkngoog_492   3   0.88753426   REFERENCES
1   Q1   encyclopediaame28unkngoog_343   4   0.88624763   REFERENCES
1   Q1   encyclopediaame28unkngoog_124   5   0.883333   REFERENCES
1   Q1   encyclopediaame28unkngoog_504   6   0.8831035   REFERENCES
1   Q1   encyclopediaame28unkngoog_509   7   0.8830843   REFERENCES
1   Q1   encyclopediaame28unkngoog_522   8   0.8824376   REFERENCES
1   Q1   encyclopediaame28unkngoog_584   9   0.8824353   REFERENCES
1   Q1   encyclopediaame28unkngoog_141   10   0.88232476   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.8816631   REFERENCES
1   Q1   encyclopediaame28unkngoog_580   12   0.8813205   REFERENCES
1   Q1   encyclopediaame28unkngoog_345   13   0.8810202   REFERENCES
1   Q1   encyclopediaame28unkngoog_222   14   0.88053787   REFERENCES
1   Q1   encyclopediaame28unkngoog_218   15   0.88041246   REFERENCES
1   Q1   encyclopediaame28unkngoog_221   16   0.8801749   REFERENCES
1   Q1   ouroldworldbackg00bearrich_307   17   0.87977755   REFERENCES
1   Q1   encyclopediaame28unkngoog_353   18   0.8790047   REFERENCES
1   Q1   encyclopediaame28unkngoog_513   19   0.8785509   REFERENCES
1   Q1   encyclopediaame28unkngoog_515   20   0.87807393   REFERENCES
