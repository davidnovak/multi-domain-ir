###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.25, 0.0, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 15.067, activation diff: 21.067, ratio: 1.398
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 230.772, activation diff: 245.839, ratio: 1.065
#   pulse 3: activated nodes: 7690, borderline nodes: 4607, overall activation: 121.306, activation diff: 352.077, ratio: 2.902
#   pulse 4: activated nodes: 9628, borderline nodes: 5882, overall activation: 1344.377, activation diff: 1465.682, ratio: 1.090
#   pulse 5: activated nodes: 10877, borderline nodes: 3713, overall activation: 460.269, activation diff: 1804.646, ratio: 3.921
#   pulse 6: activated nodes: 11051, borderline nodes: 2333, overall activation: 1832.509, activation diff: 2292.778, ratio: 1.251
#   pulse 7: activated nodes: 11201, borderline nodes: 1782, overall activation: 585.261, activation diff: 2417.770, ratio: 4.131
#   pulse 8: activated nodes: 11212, borderline nodes: 1257, overall activation: 1885.808, activation diff: 2471.069, ratio: 1.310
#   pulse 9: activated nodes: 11216, borderline nodes: 1189, overall activation: 600.890, activation diff: 2486.697, ratio: 4.138
#   pulse 10: activated nodes: 11216, borderline nodes: 1144, overall activation: 1891.360, activation diff: 2492.249, ratio: 1.318
#   pulse 11: activated nodes: 11217, borderline nodes: 1142, overall activation: 602.648, activation diff: 2494.008, ratio: 4.138
#   pulse 12: activated nodes: 11217, borderline nodes: 1137, overall activation: 1891.975, activation diff: 2494.623, ratio: 1.319
#   pulse 13: activated nodes: 11217, borderline nodes: 1136, overall activation: 602.850, activation diff: 2494.825, ratio: 4.138
#   pulse 14: activated nodes: 11217, borderline nodes: 1136, overall activation: 1892.047, activation diff: 2494.897, ratio: 1.319
#   pulse 15: activated nodes: 11217, borderline nodes: 1136, overall activation: 602.874, activation diff: 2494.921, ratio: 4.138

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11217
#   final overall activation: 602.9
#   number of spread. activ. pulses: 15
#   running time: 549

###################################
# top k results in TREC format: 

1   Q1   miningcampsstudy00shin_200   1   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_202   2   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_192   3   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_193   4   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_195   5   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_196   6   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_206   7   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_188   8   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_214   9   0.0   REFERENCES
1   Q1   historyofuniteds07good_350   10   0.0   REFERENCES
1   Q1   historyofuniteds07good_351   11   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_190   12   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_197   13   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_204   14   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_205   15   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_208   16   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_191   17   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_203   18   0.0   REFERENCES
1   Q1   bostoncollegebul0405bost_199   19   0.0   REFERENCES
1   Q1   miningcampsstudy00shin_201   20   0.0   REFERENCES
