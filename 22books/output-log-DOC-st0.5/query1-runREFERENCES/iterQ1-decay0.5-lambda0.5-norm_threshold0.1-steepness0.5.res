###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES
#   steepness: 0.5

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSALogNormFunction(norm_function, 0.5, 0.5, DOCUMENT)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 15
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 0.5)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.968, activation diff: 11.968, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 66.754, activation diff: 57.059, ratio: 0.855
#   pulse 3: activated nodes: 8120, borderline nodes: 3940, overall activation: 108.794, activation diff: 42.977, ratio: 0.395
#   pulse 4: activated nodes: 8120, borderline nodes: 3940, overall activation: 258.631, activation diff: 149.919, ratio: 0.580
#   pulse 5: activated nodes: 8605, borderline nodes: 3471, overall activation: 410.073, activation diff: 151.441, ratio: 0.369
#   pulse 6: activated nodes: 8637, borderline nodes: 3437, overall activation: 537.804, activation diff: 127.731, ratio: 0.238
#   pulse 7: activated nodes: 8653, borderline nodes: 3408, overall activation: 631.399, activation diff: 93.596, ratio: 0.148
#   pulse 8: activated nodes: 8654, borderline nodes: 3405, overall activation: 694.608, activation diff: 63.209, ratio: 0.091
#   pulse 9: activated nodes: 8656, borderline nodes: 3403, overall activation: 735.067, activation diff: 40.459, ratio: 0.055
#   pulse 10: activated nodes: 8660, borderline nodes: 3406, overall activation: 760.044, activation diff: 24.976, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8660
#   final overall activation: 760.0
#   number of spread. activ. pulses: 10
#   running time: 406

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.94571954   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.8047931   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.7876363   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.7143661   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.627439   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.5488074   REFERENCES
1   Q1   europesincenapol00leveuoft_16   7   0.40647474   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   8   0.40544134   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.40399745   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   10   0.40329283   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   11   0.39384985   REFERENCES
1   Q1   politicalsketche00retsrich_154   12   0.3798271   REFERENCES
1   Q1   shorthistoryofmo00haslrich_234   13   0.36763278   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   14   0.3673547   REFERENCES
1   Q1   politicalsketche00retsrich_159   15   0.3669106   REFERENCES
1   Q1   politicalsketche00retsrich_96   16   0.359039   REFERENCES
1   Q1   generalhistoryfo00myerrich_711   17   0.3532514   REFERENCES
1   Q1   politicalsketche00retsrich_148   18   0.35322398   REFERENCES
1   Q1   essentialsinmod01howegoog_461   19   0.35301536   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.35207483   REFERENCES
