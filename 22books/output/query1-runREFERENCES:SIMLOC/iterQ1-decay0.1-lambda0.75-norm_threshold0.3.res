###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 107.534, activation diff: 96.916, ratio: 0.901
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 327.558, activation diff: 220.100, ratio: 0.672
#   pulse 4: activated nodes: 10935, borderline nodes: 6843, overall activation: 997.768, activation diff: 670.210, ratio: 0.672
#   pulse 5: activated nodes: 11252, borderline nodes: 3598, overall activation: 1970.972, activation diff: 973.205, ratio: 0.494
#   pulse 6: activated nodes: 11387, borderline nodes: 1503, overall activation: 3060.629, activation diff: 1089.656, ratio: 0.356
#   pulse 7: activated nodes: 11415, borderline nodes: 692, overall activation: 4080.701, activation diff: 1020.072, ratio: 0.250
#   pulse 8: activated nodes: 11432, borderline nodes: 318, overall activation: 4954.450, activation diff: 873.750, ratio: 0.176
#   pulse 9: activated nodes: 11441, borderline nodes: 169, overall activation: 5675.029, activation diff: 720.579, ratio: 0.127
#   pulse 10: activated nodes: 11445, borderline nodes: 108, overall activation: 6257.976, activation diff: 582.947, ratio: 0.093
#   pulse 11: activated nodes: 11448, borderline nodes: 73, overall activation: 6724.350, activation diff: 466.373, ratio: 0.069
#   pulse 12: activated nodes: 11449, borderline nodes: 58, overall activation: 7094.732, activation diff: 370.382, ratio: 0.052
#   pulse 13: activated nodes: 11449, borderline nodes: 51, overall activation: 7387.392, activation diff: 292.661, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7387.4
#   number of spread. activ. pulses: 13
#   running time: 1430

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911156   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98714316   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98480886   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98381233   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97533536   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95627177   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.86941266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8685818   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.86769176   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.86751604   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.8664688   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.866464   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.86643934   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.86601305   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.86596876   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.86596525   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.8659204   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8658968   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8656837   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.86564887   REFERENCES:SIMLOC
