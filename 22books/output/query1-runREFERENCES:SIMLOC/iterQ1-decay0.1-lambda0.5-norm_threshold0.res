###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 953.283, activation diff: 933.419, ratio: 0.979
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2209.666, activation diff: 1256.382, ratio: 0.569
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 4736.639, activation diff: 2526.973, ratio: 0.533
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6485.405, activation diff: 1748.766, ratio: 0.270
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 7523.189, activation diff: 1037.784, ratio: 0.138
#   pulse 7: activated nodes: 11457, borderline nodes: 0, overall activation: 8111.826, activation diff: 588.637, ratio: 0.073
#   pulse 8: activated nodes: 11457, borderline nodes: 0, overall activation: 8439.450, activation diff: 327.624, ratio: 0.039

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8439.4
#   number of spread. activ. pulses: 8
#   running time: 1313

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996089   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9958463   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9955529   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99528563   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99182236   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98451316   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8929343   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8928559   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.8927984   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.89279217   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.8926914   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.892629   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.8926021   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89257455   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8925484   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.89253813   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.8925319   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.8924923   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.8924794   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8924725   REFERENCES:SIMLOC
