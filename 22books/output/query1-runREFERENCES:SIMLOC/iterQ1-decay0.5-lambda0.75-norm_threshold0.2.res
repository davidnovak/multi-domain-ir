###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 90.401, activation diff: 79.028, ratio: 0.874
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 230.315, activation diff: 139.963, ratio: 0.608
#   pulse 4: activated nodes: 10492, borderline nodes: 5956, overall activation: 508.048, activation diff: 277.733, ratio: 0.547
#   pulse 5: activated nodes: 10729, borderline nodes: 4418, overall activation: 855.715, activation diff: 347.667, ratio: 0.406
#   pulse 6: activated nodes: 10836, borderline nodes: 3841, overall activation: 1227.779, activation diff: 372.064, ratio: 0.303
#   pulse 7: activated nodes: 10871, borderline nodes: 3632, overall activation: 1586.757, activation diff: 358.977, ratio: 0.226
#   pulse 8: activated nodes: 10887, borderline nodes: 3562, overall activation: 1908.324, activation diff: 321.567, ratio: 0.169
#   pulse 9: activated nodes: 10887, borderline nodes: 3545, overall activation: 2181.075, activation diff: 272.751, ratio: 0.125
#   pulse 10: activated nodes: 10891, borderline nodes: 3530, overall activation: 2405.123, activation diff: 224.048, ratio: 0.093
#   pulse 11: activated nodes: 10894, borderline nodes: 3522, overall activation: 2585.984, activation diff: 180.861, ratio: 0.070
#   pulse 12: activated nodes: 10895, borderline nodes: 3520, overall activation: 2730.473, activation diff: 144.489, ratio: 0.053
#   pulse 13: activated nodes: 10895, borderline nodes: 3520, overall activation: 2845.117, activation diff: 114.644, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10895
#   final overall activation: 2845.1
#   number of spread. activ. pulses: 13
#   running time: 1487

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99143183   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98814154   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9863845   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98512685   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97701585   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9601112   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48329917   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.48283666   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.48248523   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.48245078   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4819736   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.48179397   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.4817927   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.48165092   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.48160318   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.4815945   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.48147562   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.48146433   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.4814392   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48142752   REFERENCES:SIMLOC
