###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 66.072, activation diff: 55.454, ratio: 0.839
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 175.758, activation diff: 109.763, ratio: 0.625
#   pulse 4: activated nodes: 10167, borderline nodes: 6455, overall activation: 413.684, activation diff: 237.926, ratio: 0.575
#   pulse 5: activated nodes: 10608, borderline nodes: 4844, overall activation: 725.918, activation diff: 312.234, ratio: 0.430
#   pulse 6: activated nodes: 10754, borderline nodes: 4319, overall activation: 1073.089, activation diff: 347.171, ratio: 0.324
#   pulse 7: activated nodes: 10839, borderline nodes: 3827, overall activation: 1420.658, activation diff: 347.569, ratio: 0.245
#   pulse 8: activated nodes: 10862, borderline nodes: 3658, overall activation: 1745.265, activation diff: 324.607, ratio: 0.186
#   pulse 9: activated nodes: 10884, borderline nodes: 3598, overall activation: 2030.596, activation diff: 285.331, ratio: 0.141
#   pulse 10: activated nodes: 10887, borderline nodes: 3561, overall activation: 2270.366, activation diff: 239.770, ratio: 0.106
#   pulse 11: activated nodes: 10887, borderline nodes: 3548, overall activation: 2466.575, activation diff: 196.209, ratio: 0.080
#   pulse 12: activated nodes: 10887, borderline nodes: 3540, overall activation: 2624.822, activation diff: 158.247, ratio: 0.060
#   pulse 13: activated nodes: 10892, borderline nodes: 3532, overall activation: 2751.336, activation diff: 126.514, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10892
#   final overall activation: 2751.3
#   number of spread. activ. pulses: 13
#   running time: 1559

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111503   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9870798   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98459166   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9836292   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9749138   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95443296   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.483003   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.4825271   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.48202622   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.48193592   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.48132318   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.4813083   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.48130506   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.48105133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.48099622   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.48098734   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.48092535   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.48091385   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.4809073   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48084244   REFERENCES:SIMLOC
