###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2092.080, activation diff: 2116.977, ratio: 1.012
#   pulse 3: activated nodes: 9395, borderline nodes: 3931, overall activation: 1387.918, activation diff: 3422.392, ratio: 2.466
#   pulse 4: activated nodes: 11308, borderline nodes: 3162, overall activation: 7189.543, activation diff: 6958.242, ratio: 0.968
#   pulse 5: activated nodes: 11426, borderline nodes: 363, overall activation: 7849.819, activation diff: 1524.001, ratio: 0.194
#   pulse 6: activated nodes: 11448, borderline nodes: 73, overall activation: 8427.790, activation diff: 595.676, ratio: 0.071
#   pulse 7: activated nodes: 11451, borderline nodes: 35, overall activation: 8466.464, activation diff: 41.093, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 8466.5
#   number of spread. activ. pulses: 7
#   running time: 1370

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995494   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMLOC
