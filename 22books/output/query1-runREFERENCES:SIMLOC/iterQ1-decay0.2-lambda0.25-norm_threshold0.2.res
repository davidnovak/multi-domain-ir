###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1276.331, activation diff: 1276.328, ratio: 1.000
#   pulse 3: activated nodes: 9457, borderline nodes: 3914, overall activation: 1797.340, activation diff: 956.495, ratio: 0.532
#   pulse 4: activated nodes: 11306, borderline nodes: 3208, overall activation: 4937.264, activation diff: 3144.674, ratio: 0.637
#   pulse 5: activated nodes: 11423, borderline nodes: 432, overall activation: 6300.950, activation diff: 1363.685, ratio: 0.216
#   pulse 6: activated nodes: 11447, borderline nodes: 98, overall activation: 6783.071, activation diff: 482.121, ratio: 0.071
#   pulse 7: activated nodes: 11449, borderline nodes: 51, overall activation: 6945.371, activation diff: 162.301, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6945.4
#   number of spread. activ. pulses: 7
#   running time: 1326

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99965113   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99949676   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9992517   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99687546   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9921906   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7997601   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.79971874   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.7996416   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7996249   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.7996205   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.7996109   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.79959273   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.7995904   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.79958546   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.7995833   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.7995616   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7995558   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7995511   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.79953843   REFERENCES:SIMLOC
