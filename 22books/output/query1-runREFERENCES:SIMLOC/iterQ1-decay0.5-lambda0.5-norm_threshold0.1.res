###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 454.792, activation diff: 436.476, ratio: 0.960
#   pulse 3: activated nodes: 9572, borderline nodes: 3872, overall activation: 856.610, activation diff: 401.818, ratio: 0.469
#   pulse 4: activated nodes: 10794, borderline nodes: 3957, overall activation: 1653.791, activation diff: 797.181, ratio: 0.482
#   pulse 5: activated nodes: 10887, borderline nodes: 3556, overall activation: 2306.643, activation diff: 652.852, ratio: 0.283
#   pulse 6: activated nodes: 10895, borderline nodes: 3518, overall activation: 2729.631, activation diff: 422.988, ratio: 0.155
#   pulse 7: activated nodes: 10897, borderline nodes: 3516, overall activation: 2978.862, activation diff: 249.231, ratio: 0.084
#   pulse 8: activated nodes: 10897, borderline nodes: 3514, overall activation: 3120.867, activation diff: 142.004, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10897
#   final overall activation: 3120.9
#   number of spread. activ. pulses: 8
#   running time: 1371

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99572206   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9952331   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949982   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99109733   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9824772   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49605823   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.4959977   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4959371   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.49593094   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.4958337   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49579978   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.49578482   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.4957814   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49573854   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.4957199   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   17   0.4957143   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.495714   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.49570575   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.49569488   REFERENCES:SIMLOC
