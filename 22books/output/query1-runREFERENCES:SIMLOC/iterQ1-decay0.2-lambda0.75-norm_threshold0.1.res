###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 183.958, activation diff: 171.893, ratio: 0.934
#   pulse 3: activated nodes: 9338, borderline nodes: 3902, overall activation: 502.634, activation diff: 318.702, ratio: 0.634
#   pulse 4: activated nodes: 11256, borderline nodes: 4163, overall activation: 1250.891, activation diff: 748.257, ratio: 0.598
#   pulse 5: activated nodes: 11399, borderline nodes: 1087, overall activation: 2190.499, activation diff: 939.608, ratio: 0.429
#   pulse 6: activated nodes: 11424, borderline nodes: 422, overall activation: 3111.146, activation diff: 920.647, ratio: 0.296
#   pulse 7: activated nodes: 11443, borderline nodes: 205, overall activation: 3912.393, activation diff: 801.247, ratio: 0.205
#   pulse 8: activated nodes: 11447, borderline nodes: 88, overall activation: 4575.796, activation diff: 663.403, ratio: 0.145
#   pulse 9: activated nodes: 11449, borderline nodes: 59, overall activation: 5112.219, activation diff: 536.422, ratio: 0.105
#   pulse 10: activated nodes: 11450, borderline nodes: 51, overall activation: 5540.371, activation diff: 428.152, ratio: 0.077
#   pulse 11: activated nodes: 11452, borderline nodes: 37, overall activation: 5879.395, activation diff: 339.024, ratio: 0.058
#   pulse 12: activated nodes: 11452, borderline nodes: 34, overall activation: 6146.417, activation diff: 267.022, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6146.4
#   number of spread. activ. pulses: 12
#   running time: 1438

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98528683   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9836032   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9818584   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9728817   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9561462   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7648326   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76393384   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.76340866   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.76340216   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7626143   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.76208097   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7620637   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7620099   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.76181954   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.76177174   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.76168305   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   18   0.76161975   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.76151645   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.76149803   REFERENCES:SIMLOC
