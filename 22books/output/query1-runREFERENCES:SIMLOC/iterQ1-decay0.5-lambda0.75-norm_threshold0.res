###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 162.526, activation diff: 149.822, ratio: 0.922
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 387.031, activation diff: 224.512, ratio: 0.580
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 754.796, activation diff: 367.765, ratio: 0.487
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 1169.248, activation diff: 414.453, ratio: 0.354
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 1569.033, activation diff: 399.785, ratio: 0.255
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 1920.524, activation diff: 351.491, ratio: 0.183
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 2213.814, activation diff: 293.290, ratio: 0.132
#   pulse 9: activated nodes: 10899, borderline nodes: 3506, overall activation: 2452.016, activation diff: 238.202, ratio: 0.097
#   pulse 10: activated nodes: 10899, borderline nodes: 3506, overall activation: 2642.606, activation diff: 190.590, ratio: 0.072
#   pulse 11: activated nodes: 10899, borderline nodes: 3506, overall activation: 2793.707, activation diff: 151.101, ratio: 0.054
#   pulse 12: activated nodes: 10899, borderline nodes: 3506, overall activation: 2912.778, activation diff: 119.071, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 2912.8
#   number of spread. activ. pulses: 12
#   running time: 1434

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890304   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98610014   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9848436   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98306596   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9746249   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9598713   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47822982   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.47770768   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.47751266   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4774817   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.47712106   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.47675857   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.47669488   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.47668812   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.47663534   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.47660518   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.4765541   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.47641867   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.47640914   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.47639418   REFERENCES:SIMLOC
