###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1465.249, activation diff: 1461.424, ratio: 0.997
#   pulse 3: activated nodes: 9825, borderline nodes: 3911, overall activation: 2275.617, activation diff: 1049.408, ratio: 0.461
#   pulse 4: activated nodes: 11361, borderline nodes: 2568, overall activation: 5314.827, activation diff: 3039.847, ratio: 0.572
#   pulse 5: activated nodes: 11436, borderline nodes: 232, overall activation: 6539.101, activation diff: 1224.274, ratio: 0.187
#   pulse 6: activated nodes: 11451, borderline nodes: 48, overall activation: 6955.000, activation diff: 415.899, ratio: 0.060
#   pulse 7: activated nodes: 11453, borderline nodes: 27, overall activation: 7091.130, activation diff: 136.130, ratio: 0.019

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11453
#   final overall activation: 7091.1
#   number of spread. activ. pulses: 7
#   running time: 1416

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998076   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997371   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996183   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9995358   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9972692   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9931309   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.799791   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.799766   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.7997276   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7997187   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7997186   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.799716   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   13   0.7997143   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.7996931   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7996844   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.7996841   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   17   0.7996799   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.7996751   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   19   0.79967475   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.79966396   REFERENCES:SIMLOC
