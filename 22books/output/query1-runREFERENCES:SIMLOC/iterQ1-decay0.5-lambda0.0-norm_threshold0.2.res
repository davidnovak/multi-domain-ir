###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1320.858, activation diff: 1346.881, ratio: 1.020
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 450.096, activation diff: 1694.964, ratio: 3.766
#   pulse 4: activated nodes: 10792, borderline nodes: 3910, overall activation: 2906.441, activation diff: 2660.905, ratio: 0.916
#   pulse 5: activated nodes: 10894, borderline nodes: 3546, overall activation: 3040.719, activation diff: 418.821, ratio: 0.138
#   pulse 6: activated nodes: 10895, borderline nodes: 3522, overall activation: 3243.801, activation diff: 203.857, ratio: 0.063
#   pulse 7: activated nodes: 10896, borderline nodes: 3517, overall activation: 3258.279, activation diff: 14.587, ratio: 0.004

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10896
#   final overall activation: 3258.3
#   number of spread. activ. pulses: 7
#   running time: 1299

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9977745   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   10   0.5   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_307   11   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   12   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   13   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   14   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_209   16   0.5   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_213   17   0.5   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.5   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.5   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   20   0.49999997   REFERENCES:SIMLOC
