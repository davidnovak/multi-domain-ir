###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 487.516, activation diff: 473.909, ratio: 0.972
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 1013.249, activation diff: 528.910, ratio: 0.522
#   pulse 4: activated nodes: 11189, borderline nodes: 5585, overall activation: 2808.887, activation diff: 1795.638, ratio: 0.639
#   pulse 5: activated nodes: 11390, borderline nodes: 1334, overall activation: 4397.719, activation diff: 1588.832, ratio: 0.361
#   pulse 6: activated nodes: 11426, borderline nodes: 411, overall activation: 5445.499, activation diff: 1047.780, ratio: 0.192
#   pulse 7: activated nodes: 11438, borderline nodes: 185, overall activation: 6071.577, activation diff: 626.078, ratio: 0.103
#   pulse 8: activated nodes: 11447, borderline nodes: 101, overall activation: 6432.295, activation diff: 360.718, ratio: 0.056
#   pulse 9: activated nodes: 11449, borderline nodes: 71, overall activation: 6636.825, activation diff: 204.530, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 6636.8
#   number of spread. activ. pulses: 9
#   running time: 1343

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9975996   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969782   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9968991   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9933512   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9851137   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7967776   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7967007   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.79655063   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7964952   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.7964071   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.79637957   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   13   0.7963003   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.79628515   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.796283   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.79628026   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.79626703   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.79625607   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.79625046   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.7962413   REFERENCES:SIMLOC
