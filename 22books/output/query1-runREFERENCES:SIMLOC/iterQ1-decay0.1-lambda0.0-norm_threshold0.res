###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2922.914, activation diff: 2949.514, ratio: 1.009
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 3015.020, activation diff: 4124.731, ratio: 1.368
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 8302.361, activation diff: 5629.379, ratio: 0.678
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 8764.270, activation diff: 489.540, ratio: 0.056
#   pulse 6: activated nodes: 11457, borderline nodes: 2, overall activation: 8827.770, activation diff: 65.190, ratio: 0.007

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11457
#   final overall activation: 8827.8
#   number of spread. activ. pulses: 6
#   running time: 2335

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999994   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999666   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9981779   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950548   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   10   0.9   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_348   11   0.9   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_930   12   0.9   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   13   0.9   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_111   14   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   15   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   16   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   17   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_466   18   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_464   19   0.9   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   20   0.9   REFERENCES:SIMLOC
