###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1434.001, activation diff: 1433.998, ratio: 1.000
#   pulse 3: activated nodes: 9457, borderline nodes: 3914, overall activation: 2129.600, activation diff: 1179.392, ratio: 0.554
#   pulse 4: activated nodes: 11308, borderline nodes: 3122, overall activation: 6031.778, activation diff: 3908.212, ratio: 0.648
#   pulse 5: activated nodes: 11426, borderline nodes: 368, overall activation: 7696.699, activation diff: 1664.945, ratio: 0.216
#   pulse 6: activated nodes: 11448, borderline nodes: 70, overall activation: 8281.844, activation diff: 585.145, ratio: 0.071
#   pulse 7: activated nodes: 11452, borderline nodes: 30, overall activation: 8477.702, activation diff: 195.859, ratio: 0.023

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 8477.7
#   number of spread. activ. pulses: 7
#   running time: 1304

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996512   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99949676   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9992517   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99687564   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9921907   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8997309   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8996851   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.89959955   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.8995845   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   11   0.8995763   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.89956224   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.8995453   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   14   0.8995409   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   15   0.8995336   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.89953154   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   17   0.8995079   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.8995069   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.89950013   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8994814   REFERENCES:SIMLOC
