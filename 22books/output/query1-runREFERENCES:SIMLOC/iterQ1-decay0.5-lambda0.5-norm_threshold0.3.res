###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 310.992, activation diff: 297.386, ratio: 0.956
#   pulse 3: activated nodes: 8990, borderline nodes: 4844, overall activation: 567.133, activation diff: 258.154, ratio: 0.455
#   pulse 4: activated nodes: 10585, borderline nodes: 5856, overall activation: 1288.243, activation diff: 721.110, ratio: 0.560
#   pulse 5: activated nodes: 10837, borderline nodes: 3900, overall activation: 1963.085, activation diff: 674.842, ratio: 0.344
#   pulse 6: activated nodes: 10876, borderline nodes: 3625, overall activation: 2465.049, activation diff: 501.964, ratio: 0.204
#   pulse 7: activated nodes: 10887, borderline nodes: 3553, overall activation: 2782.786, activation diff: 317.737, ratio: 0.114
#   pulse 8: activated nodes: 10891, borderline nodes: 3533, overall activation: 2970.801, activation diff: 188.015, ratio: 0.063
#   pulse 9: activated nodes: 10894, borderline nodes: 3524, overall activation: 3079.387, activation diff: 108.585, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10894
#   final overall activation: 3079.4
#   number of spread. activ. pulses: 9
#   running time: 1391

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980163   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99759877   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9969716   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99689883   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9933033   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9848927   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49798596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49793714   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4978438   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.4978094   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.49775186   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.4977365   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.49767795   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   14   0.49767497   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49767435   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.497665   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.49766427   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.49765724   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.4976514   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.4976446   REFERENCES:SIMLOC
