###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1559.738, activation diff: 1552.175, ratio: 0.995
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 2606.120, activation diff: 1106.754, ratio: 0.425
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 5160.821, activation diff: 2554.701, ratio: 0.495
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 6136.263, activation diff: 975.442, ratio: 0.159
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 6460.080, activation diff: 323.816, ratio: 0.050
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 6564.336, activation diff: 104.257, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 6564.3
#   number of spread. activ. pulses: 7
#   running time: 1333

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998145   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997777   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9996884   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99967873   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99759454   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99393445   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74981314   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7498008   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.74978673   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.74978375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.74977994   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.74977803   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.74977493   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.74976754   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   15   0.7497675   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.74976546   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.7497586   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   18   0.7497581   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   19   0.74975157   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_717   20   0.74975   REFERENCES:SIMLOC
