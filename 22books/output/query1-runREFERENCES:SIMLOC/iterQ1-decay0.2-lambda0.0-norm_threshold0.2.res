###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2108.879, activation diff: 2134.902, ratio: 1.012
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 1332.243, activation diff: 3268.085, ratio: 2.453
#   pulse 4: activated nodes: 11325, borderline nodes: 2697, overall activation: 6164.416, activation diff: 5624.212, ratio: 0.912
#   pulse 5: activated nodes: 11433, borderline nodes: 270, overall activation: 6770.706, activation diff: 893.778, ratio: 0.132
#   pulse 6: activated nodes: 11449, borderline nodes: 64, overall activation: 7006.332, activation diff: 242.262, ratio: 0.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 7006.3
#   number of spread. activ. pulses: 6
#   running time: 1232

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   10   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.8   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_348   12   0.8   REFERENCES:SIMLOC
1   Q1   bookman44unkngoog_930   13   0.8   REFERENCES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   14   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   15   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   16   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   17   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   18   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   19   0.8   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   20   0.8   REFERENCES:SIMLOC
