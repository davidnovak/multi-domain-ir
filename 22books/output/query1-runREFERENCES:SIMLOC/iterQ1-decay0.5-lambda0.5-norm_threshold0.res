###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 539.899, activation diff: 520.035, ratio: 0.963
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1042.219, activation diff: 502.320, ratio: 0.482
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 1856.294, activation diff: 814.075, ratio: 0.439
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 2468.172, activation diff: 611.878, ratio: 0.248
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 2844.906, activation diff: 376.734, ratio: 0.132
#   pulse 7: activated nodes: 10899, borderline nodes: 3506, overall activation: 3062.158, activation diff: 217.253, ratio: 0.071
#   pulse 8: activated nodes: 10899, borderline nodes: 3506, overall activation: 3184.311, activation diff: 122.153, ratio: 0.038

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3184.3
#   number of spread. activ. pulses: 8
#   running time: 1274

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.996089   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9958457   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99555284   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.995283   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99179566   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9844748   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49607375   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49602872   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.49599653   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.49599415   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49593583   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.4958952   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.495883   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.4958706   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.4958467   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.49584597   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   17   0.49583453   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.49581987   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   19   0.49581665   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.49581409   REFERENCES:SIMLOC
