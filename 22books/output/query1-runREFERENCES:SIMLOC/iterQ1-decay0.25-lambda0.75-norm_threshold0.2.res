###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 127.868, activation diff: 116.494, ratio: 0.911
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 356.549, activation diff: 228.731, ratio: 0.642
#   pulse 4: activated nodes: 11109, borderline nodes: 6090, overall activation: 921.739, activation diff: 565.190, ratio: 0.613
#   pulse 5: activated nodes: 11312, borderline nodes: 2831, overall activation: 1673.935, activation diff: 752.196, ratio: 0.449
#   pulse 6: activated nodes: 11400, borderline nodes: 1100, overall activation: 2469.641, activation diff: 795.706, ratio: 0.322
#   pulse 7: activated nodes: 11417, borderline nodes: 538, overall activation: 3195.716, activation diff: 726.075, ratio: 0.227
#   pulse 8: activated nodes: 11425, borderline nodes: 321, overall activation: 3810.622, activation diff: 614.906, ratio: 0.161
#   pulse 9: activated nodes: 11432, borderline nodes: 208, overall activation: 4314.255, activation diff: 503.633, ratio: 0.117
#   pulse 10: activated nodes: 11439, borderline nodes: 164, overall activation: 4719.508, activation diff: 405.253, ratio: 0.086
#   pulse 11: activated nodes: 11439, borderline nodes: 144, overall activation: 5042.245, activation diff: 322.736, ratio: 0.064
#   pulse 12: activated nodes: 11439, borderline nodes: 122, overall activation: 5297.577, activation diff: 255.332, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11439
#   final overall activation: 5297.6
#   number of spread. activ. pulses: 12
#   running time: 1547

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885761   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9842215   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9819343   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98030007   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9706369   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95069504   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.71660197   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7156988   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7149956   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7149187   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7139749   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7136597   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7136573   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.71337175   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.71327794   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7132596   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7131969   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.71309525   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.71295965   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7129388   REFERENCES:SIMLOC
