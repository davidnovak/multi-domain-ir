###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 660.642, activation diff: 644.577, ratio: 0.976
#   pulse 3: activated nodes: 9185, borderline nodes: 4938, overall activation: 1445.222, activation diff: 784.581, ratio: 0.543
#   pulse 4: activated nodes: 11259, borderline nodes: 4803, overall activation: 3847.624, activation diff: 2402.401, ratio: 0.624
#   pulse 5: activated nodes: 11415, borderline nodes: 633, overall activation: 5763.311, activation diff: 1915.687, ratio: 0.332
#   pulse 6: activated nodes: 11442, borderline nodes: 150, overall activation: 6962.513, activation diff: 1199.202, ratio: 0.172
#   pulse 7: activated nodes: 11449, borderline nodes: 55, overall activation: 7661.793, activation diff: 699.280, ratio: 0.091
#   pulse 8: activated nodes: 11451, borderline nodes: 41, overall activation: 8058.546, activation diff: 396.753, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 8058.5
#   number of spread. activ. pulses: 8
#   running time: 1327

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960693   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9955247   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9946981   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9945977   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9903182   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98023164   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8928534   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.89271355   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.89251983   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.89247257   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.8922359   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   12   0.89218843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.89218265   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.89211005   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89206386   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8920325   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.8920294   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   18   0.8920224   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.89199734   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   20   0.89195096   REFERENCES:SIMLOC
