###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 234.910, activation diff: 222.206, ratio: 0.946
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 618.720, activation diff: 383.817, ratio: 0.620
#   pulse 4: activated nodes: 11436, borderline nodes: 537, overall activation: 1400.493, activation diff: 781.773, ratio: 0.558
#   pulse 5: activated nodes: 11455, borderline nodes: 19, overall activation: 2301.529, activation diff: 901.036, ratio: 0.391
#   pulse 6: activated nodes: 11455, borderline nodes: 19, overall activation: 3139.353, activation diff: 837.824, ratio: 0.267
#   pulse 7: activated nodes: 11455, borderline nodes: 19, overall activation: 3851.837, activation diff: 712.484, ratio: 0.185
#   pulse 8: activated nodes: 11455, borderline nodes: 19, overall activation: 4435.263, activation diff: 583.426, ratio: 0.132
#   pulse 9: activated nodes: 11455, borderline nodes: 19, overall activation: 4903.957, activation diff: 468.695, ratio: 0.096
#   pulse 10: activated nodes: 11455, borderline nodes: 19, overall activation: 5276.358, activation diff: 372.401, ratio: 0.071
#   pulse 11: activated nodes: 11455, borderline nodes: 19, overall activation: 5570.193, activation diff: 293.834, ratio: 0.053
#   pulse 12: activated nodes: 11455, borderline nodes: 19, overall activation: 5800.945, activation diff: 230.752, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5800.9
#   number of spread. activ. pulses: 12
#   running time: 1540

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890305   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98611015   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9848591   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98310864   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9747834   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96041554   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.71734905   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7165811   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.71628124   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.716238   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7156987   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.7151902   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.715094   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.71507734   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.7150011   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.7149563   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.71487856   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7147769   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7146939   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.7146412   REFERENCES:SIMLOC
