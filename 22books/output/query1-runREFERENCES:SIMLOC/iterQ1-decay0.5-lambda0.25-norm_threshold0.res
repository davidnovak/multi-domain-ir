###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1046.723, activation diff: 1039.160, ratio: 0.993
#   pulse 3: activated nodes: 10899, borderline nodes: 3506, overall activation: 1465.400, activation diff: 466.471, ratio: 0.318
#   pulse 4: activated nodes: 10899, borderline nodes: 3506, overall activation: 2636.059, activation diff: 1170.660, ratio: 0.444
#   pulse 5: activated nodes: 10899, borderline nodes: 3506, overall activation: 3103.156, activation diff: 467.096, ratio: 0.151
#   pulse 6: activated nodes: 10899, borderline nodes: 3506, overall activation: 3260.003, activation diff: 156.847, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10899
#   final overall activation: 3260.0
#   number of spread. activ. pulses: 6
#   running time: 1288

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99925816   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9991124   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9988562   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99871683   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.995926   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9907967   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4994997   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49946356   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   9   0.4994179   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.4994165   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.49940938   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49938893   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   13   0.49938634   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   14   0.49937156   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49936062   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   16   0.49934876   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   17   0.4993409   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   18   0.49932978   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.49931407   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   20   0.49930984   REFERENCES:SIMLOC
