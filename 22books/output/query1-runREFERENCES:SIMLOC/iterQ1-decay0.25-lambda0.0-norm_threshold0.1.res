###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 2203.461, activation diff: 2230.369, ratio: 1.012
#   pulse 3: activated nodes: 9901, borderline nodes: 3849, overall activation: 1488.540, activation diff: 3227.485, ratio: 2.168
#   pulse 4: activated nodes: 11373, borderline nodes: 2098, overall activation: 5836.395, activation diff: 4863.318, ratio: 0.833
#   pulse 5: activated nodes: 11441, borderline nodes: 158, overall activation: 6313.515, activation diff: 564.736, ratio: 0.089
#   pulse 6: activated nodes: 11446, borderline nodes: 66, overall activation: 6415.144, activation diff: 104.510, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11446
#   final overall activation: 6415.1
#   number of spread. activ. pulses: 6
#   running time: 1318

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999934   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999631   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.75   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.75   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   10   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   11   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_470   12   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   13   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_807   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_805   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   17   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   18   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   19   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   20   0.75   REFERENCES:SIMLOC
