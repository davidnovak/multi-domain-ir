###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1031.937, activation diff: 1034.604, ratio: 1.003
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 1272.443, activation diff: 788.566, ratio: 0.620
#   pulse 4: activated nodes: 11257, borderline nodes: 4915, overall activation: 4075.207, activation diff: 2818.992, ratio: 0.692
#   pulse 5: activated nodes: 11407, borderline nodes: 748, overall activation: 5407.015, activation diff: 1331.809, ratio: 0.246
#   pulse 6: activated nodes: 11425, borderline nodes: 247, overall activation: 5902.708, activation diff: 495.693, ratio: 0.084
#   pulse 7: activated nodes: 11437, borderline nodes: 143, overall activation: 6074.391, activation diff: 171.683, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11437
#   final overall activation: 6074.4
#   number of spread. activ. pulses: 7
#   running time: 1337

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99950397   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992944   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.99883974   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9964073   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9911139   REFERENCES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74970675   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7496574   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.7495413   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.74950546   REFERENCES:SIMLOC
1   Q1   politicalsketche00retsrich_154   11   0.7494843   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   12   0.7494841   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74947673   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7494731   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7494616   REFERENCES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.74945235   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7494259   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.74941534   REFERENCES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.74940133   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.7494012   REFERENCES:SIMLOC
