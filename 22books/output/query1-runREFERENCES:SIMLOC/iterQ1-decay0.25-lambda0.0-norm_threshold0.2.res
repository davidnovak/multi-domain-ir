###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7393, borderline nodes: 7324, overall activation: 1977.542, activation diff: 2003.566, ratio: 1.013
#   pulse 3: activated nodes: 9506, borderline nodes: 3851, overall activation: 1165.623, activation diff: 2990.936, ratio: 2.566
#   pulse 4: activated nodes: 11324, borderline nodes: 2728, overall activation: 5520.257, activation diff: 5052.027, ratio: 0.915
#   pulse 5: activated nodes: 11430, borderline nodes: 291, overall activation: 6042.331, activation diff: 802.938, ratio: 0.133
#   pulse 6: activated nodes: 11440, borderline nodes: 107, overall activation: 6266.275, activation diff: 229.788, ratio: 0.037

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11440
#   final overall activation: 6266.3
#   number of spread. activ. pulses: 6
#   running time: 1319

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995923   REFERENCES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.75   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.75   REFERENCES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   10   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   11   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_470   12   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   13   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_800   15   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_807   16   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_805   17   0.75   REFERENCES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   18   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   19   0.75   REFERENCES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   20   0.75   REFERENCES:SIMLOC
