###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 270.131, activation diff: 257.427, ratio: 0.953
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 716.983, activation diff: 446.862, ratio: 0.623
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1614.675, activation diff: 897.692, ratio: 0.556
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2593.787, activation diff: 979.112, ratio: 0.377
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 3501.704, activation diff: 907.917, ratio: 0.259
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 4288.664, activation diff: 786.960, ratio: 0.183
#   pulse 8: activated nodes: 11348, borderline nodes: 0, overall activation: 4948.121, activation diff: 659.457, ratio: 0.133
#   pulse 9: activated nodes: 11348, borderline nodes: 0, overall activation: 5490.079, activation diff: 541.958, ratio: 0.099
#   pulse 10: activated nodes: 11348, borderline nodes: 0, overall activation: 5930.016, activation diff: 439.937, ratio: 0.074
#   pulse 11: activated nodes: 11348, borderline nodes: 0, overall activation: 6284.179, activation diff: 354.163, ratio: 0.056
#   pulse 12: activated nodes: 11348, borderline nodes: 0, overall activation: 6567.621, activation diff: 283.442, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 6567.6
#   number of spread. activ. pulses: 12
#   running time: 566

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9860642   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9849103   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9829289   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97481596   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9601859   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.86081004   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8598708   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.859511   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.8594967   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.8588097   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   12   0.85809827   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.8580662   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.85804796   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   15   0.85803473   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.8579372   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.85777223   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.8577454   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.8575336   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.8575198   REFERENCES:SIMDATES
