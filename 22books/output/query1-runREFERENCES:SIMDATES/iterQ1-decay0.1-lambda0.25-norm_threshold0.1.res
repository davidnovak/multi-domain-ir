###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1613.724, activation diff: 1612.096, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 2332.297, activation diff: 1160.327, ratio: 0.498
#   pulse 4: activated nodes: 10841, borderline nodes: 2481, overall activation: 5190.024, activation diff: 2878.346, ratio: 0.555
#   pulse 5: activated nodes: 11274, borderline nodes: 778, overall activation: 6611.931, activation diff: 1421.941, ratio: 0.215
#   pulse 6: activated nodes: 11334, borderline nodes: 109, overall activation: 7167.546, activation diff: 555.615, ratio: 0.078
#   pulse 7: activated nodes: 11341, borderline nodes: 32, overall activation: 7370.058, activation diff: 202.512, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7370.1
#   number of spread. activ. pulses: 7
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99967957   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9996146   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994483   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972363   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9929799   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8997542   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89970464   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.89968705   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8996321   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.8996134   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.8996031   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.8996031   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.89959   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.8995805   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.8995776   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.899577   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.8995683   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.8995505   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.89954835   REFERENCES:SIMDATES
