###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 228.048, activation diff: 215.345, ratio: 0.944
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 571.522, activation diff: 343.483, ratio: 0.601
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1183.676, activation diff: 612.154, ratio: 0.517
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 1837.606, activation diff: 653.930, ratio: 0.356
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 2439.674, activation diff: 602.068, ratio: 0.247
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 2959.466, activation diff: 519.792, ratio: 0.176
#   pulse 8: activated nodes: 11328, borderline nodes: 416, overall activation: 3393.739, activation diff: 434.274, ratio: 0.128
#   pulse 9: activated nodes: 11328, borderline nodes: 416, overall activation: 3749.722, activation diff: 355.982, ratio: 0.095
#   pulse 10: activated nodes: 11328, borderline nodes: 416, overall activation: 4038.012, activation diff: 288.290, ratio: 0.071
#   pulse 11: activated nodes: 11328, borderline nodes: 416, overall activation: 4269.566, activation diff: 231.554, ratio: 0.054
#   pulse 12: activated nodes: 11328, borderline nodes: 416, overall activation: 4454.460, activation diff: 184.894, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4454.5
#   number of spread. activ. pulses: 12
#   running time: 561

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98606044   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9849067   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9829074   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97475946   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9599326   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.71734047   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7165575   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.7162541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.71624154   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7156641   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   12   0.7150651   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.7150359   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   14   0.7150215   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7150103   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.71493334   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.7147848   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.7147684   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.7145811   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7145719   REFERENCES:SIMDATES
