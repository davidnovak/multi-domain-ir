###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2336.756, activation diff: 2365.018, ratio: 1.012
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1372.615, activation diff: 3704.172, ratio: 2.699
#   pulse 4: activated nodes: 10837, borderline nodes: 2501, overall activation: 5332.060, activation diff: 6477.332, ratio: 1.215
#   pulse 5: activated nodes: 11270, borderline nodes: 809, overall activation: 3575.704, activation diff: 5473.211, ratio: 1.531
#   pulse 6: activated nodes: 11332, borderline nodes: 122, overall activation: 6651.227, activation diff: 4328.776, ratio: 0.651
#   pulse 7: activated nodes: 11338, borderline nodes: 54, overall activation: 7219.758, activation diff: 755.075, ratio: 0.105
#   pulse 8: activated nodes: 11339, borderline nodes: 30, overall activation: 7308.622, activation diff: 127.963, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11339
#   final overall activation: 7308.6
#   number of spread. activ. pulses: 8
#   running time: 554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.999999   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999591   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMDATES
