###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 310.055, activation diff: 296.908, ratio: 0.958
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 515.069, activation diff: 206.927, ratio: 0.402
#   pulse 4: activated nodes: 8604, borderline nodes: 4434, overall activation: 1007.816, activation diff: 492.747, ratio: 0.489
#   pulse 5: activated nodes: 8929, borderline nodes: 3613, overall activation: 1380.577, activation diff: 372.761, ratio: 0.270
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1617.522, activation diff: 236.945, ratio: 0.146
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1758.717, activation diff: 141.195, ratio: 0.080
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1840.114, activation diff: 81.396, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1840.1
#   number of spread. activ. pulses: 8
#   running time: 432

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99602765   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.995082   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9939936   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99360764   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9891802   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.976255   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49596238   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49585655   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49570048   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.49558535   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.49542367   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.49541566   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   13   0.49531877   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.49531507   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.49530023   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.49527824   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.49522835   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.4952067   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.49518684   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.4951836   REFERENCES:SIMDATES
