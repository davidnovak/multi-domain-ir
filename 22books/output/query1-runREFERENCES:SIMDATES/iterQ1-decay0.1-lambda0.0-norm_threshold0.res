###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2793.332, activation diff: 2824.769, ratio: 1.011
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 1753.401, activation diff: 4332.293, ratio: 2.471
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 5898.523, activation diff: 6435.501, ratio: 1.091
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 7183.688, activation diff: 1992.699, ratio: 0.277
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 7580.438, activation diff: 475.580, ratio: 0.063
#   pulse 7: activated nodes: 11348, borderline nodes: 6, overall activation: 7632.084, activation diff: 63.873, ratio: 0.008

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11348
#   final overall activation: 7632.1
#   number of spread. activ. pulses: 7
#   running time: 1377

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996656   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981779   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950548   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMDATES
