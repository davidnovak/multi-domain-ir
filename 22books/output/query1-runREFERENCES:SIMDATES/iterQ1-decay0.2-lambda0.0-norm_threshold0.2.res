###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2077.824, activation diff: 2106.086, ratio: 1.014
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 998.721, activation diff: 3071.988, ratio: 3.076
#   pulse 4: activated nodes: 10837, borderline nodes: 2501, overall activation: 4274.889, activation diff: 5139.584, ratio: 1.202
#   pulse 5: activated nodes: 11270, borderline nodes: 827, overall activation: 2393.521, activation diff: 4507.005, ratio: 1.883
#   pulse 6: activated nodes: 11322, borderline nodes: 251, overall activation: 5113.733, activation diff: 3764.931, ratio: 0.736
#   pulse 7: activated nodes: 11325, borderline nodes: 196, overall activation: 5576.483, activation diff: 621.092, ratio: 0.111
#   pulse 8: activated nodes: 11327, borderline nodes: 180, overall activation: 5652.376, activation diff: 101.907, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327
#   final overall activation: 5652.4
#   number of spread. activ. pulses: 8
#   running time: 465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999989   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995905   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_169   15   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_71   16   0.8   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_75   17   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   18   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_807   19   0.8   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_805   20   0.8   REFERENCES:SIMDATES
