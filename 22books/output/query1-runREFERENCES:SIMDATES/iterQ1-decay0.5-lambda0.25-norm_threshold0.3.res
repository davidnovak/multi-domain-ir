###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 688.499, activation diff: 692.870, ratio: 1.006
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 536.196, activation diff: 282.735, ratio: 0.527
#   pulse 4: activated nodes: 8604, borderline nodes: 4434, overall activation: 1430.475, activation diff: 894.373, ratio: 0.625
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1777.327, activation diff: 346.853, ratio: 0.195
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1892.078, activation diff: 114.751, ratio: 0.061
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1927.570, activation diff: 35.492, ratio: 0.018

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1927.6
#   number of spread. activ. pulses: 7
#   running time: 429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99967897   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99933946   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9990612   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99894714   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99633676   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.990782   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49977785   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49972722   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4996717   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   10   0.49960297   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   11   0.49959916   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   12   0.4995825   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   13   0.49957046   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   14   0.49956194   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.4995581   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   16   0.49955574   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   17   0.49954307   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   18   0.49953192   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.49952555   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.4995026   REFERENCES:SIMDATES
