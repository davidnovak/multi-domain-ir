###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 521.459, activation diff: 501.594, ratio: 0.962
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 863.286, activation diff: 341.828, ratio: 0.396
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1323.601, activation diff: 460.314, ratio: 0.348
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1642.647, activation diff: 319.046, ratio: 0.194
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1838.113, activation diff: 195.467, ratio: 0.106
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1952.113, activation diff: 114.000, ratio: 0.058
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 2016.865, activation diff: 64.752, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2016.9
#   number of spread. activ. pulses: 8
#   running time: 445

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9958169   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9955956   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99514127   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99176514   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9841582   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49607214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.49602145   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49599606   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4959866   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49592286   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   12   0.49585214   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.49584478   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.49583954   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   15   0.4958379   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.49581367   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.49581364   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.4957962   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   19   0.4957908   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.49577773   REFERENCES:SIMDATES
