###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2147.355, activation diff: 2177.298, ratio: 1.014
#   pulse 3: activated nodes: 8775, borderline nodes: 3503, overall activation: 930.351, activation diff: 3031.114, ratio: 3.258
#   pulse 4: activated nodes: 10878, borderline nodes: 2211, overall activation: 3942.183, activation diff: 4560.630, ratio: 1.157
#   pulse 5: activated nodes: 11310, borderline nodes: 516, overall activation: 4332.401, activation diff: 1672.772, ratio: 0.386
#   pulse 6: activated nodes: 11317, borderline nodes: 484, overall activation: 4943.052, activation diff: 739.809, ratio: 0.150
#   pulse 7: activated nodes: 11321, borderline nodes: 438, overall activation: 5010.323, activation diff: 84.581, ratio: 0.017

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 5010.3
#   number of spread. activ. pulses: 7
#   running time: 551

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999905   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999629   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9979864   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99453604   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_470   7   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_800   10   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_578   14   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_576   15   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_541   16   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_540   17   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_546   18   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_545   19   0.75   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_549   20   0.75   REFERENCES:SIMDATES
