###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1498.361, activation diff: 1492.917, ratio: 0.996
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 2089.335, activation diff: 766.489, ratio: 0.367
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 3900.693, activation diff: 1811.358, ratio: 0.464
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 4702.468, activation diff: 801.775, ratio: 0.171
#   pulse 6: activated nodes: 11328, borderline nodes: 416, overall activation: 4994.390, activation diff: 291.921, ratio: 0.058
#   pulse 7: activated nodes: 11328, borderline nodes: 416, overall activation: 5095.072, activation diff: 100.683, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 5095.1
#   number of spread. activ. pulses: 7
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.999748   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9997192   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99958247   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756914   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99381316   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.74981034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.749785   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7497816   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   10   0.7497612   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.7497611   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7497427   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.74973166   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   14   0.7497314   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.74972695   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   16   0.7497215   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   17   0.7497145   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.7497115   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   19   0.7497102   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.749707   REFERENCES:SIMDATES
