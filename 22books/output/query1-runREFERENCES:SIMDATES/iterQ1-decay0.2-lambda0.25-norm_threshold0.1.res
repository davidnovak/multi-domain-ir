###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1436.268, activation diff: 1434.640, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 1927.393, activation diff: 884.423, ratio: 0.459
#   pulse 4: activated nodes: 10841, borderline nodes: 2481, overall activation: 4127.734, activation diff: 2214.628, ratio: 0.537
#   pulse 5: activated nodes: 11274, borderline nodes: 801, overall activation: 5175.599, activation diff: 1047.869, ratio: 0.202
#   pulse 6: activated nodes: 11325, borderline nodes: 198, overall activation: 5574.997, activation diff: 399.397, ratio: 0.072
#   pulse 7: activated nodes: 11331, borderline nodes: 119, overall activation: 5717.712, activation diff: 142.715, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11331
#   final overall activation: 5717.7
#   number of spread. activ. pulses: 7
#   running time: 473

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99967945   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9996146   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994481   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99723613   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9929796   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7997813   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7997376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79972184   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7996731   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.7996541   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.79964733   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7996473   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.79963565   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.79962707   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7996243   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.79962385   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.79961497   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.79960054   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.7995986   REFERENCES:SIMDATES
