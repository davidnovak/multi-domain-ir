###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 705.554, activation diff: 687.415, ratio: 0.974
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1422.970, activation diff: 717.416, ratio: 0.504
#   pulse 4: activated nodes: 10829, borderline nodes: 2852, overall activation: 2896.024, activation diff: 1473.054, ratio: 0.509
#   pulse 5: activated nodes: 11246, borderline nodes: 1086, overall activation: 4014.638, activation diff: 1118.614, ratio: 0.279
#   pulse 6: activated nodes: 11318, borderline nodes: 382, overall activation: 4736.015, activation diff: 721.376, ratio: 0.152
#   pulse 7: activated nodes: 11324, borderline nodes: 181, overall activation: 5173.929, activation diff: 437.914, ratio: 0.085
#   pulse 8: activated nodes: 11329, borderline nodes: 132, overall activation: 5432.202, activation diff: 258.273, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 5432.2
#   number of spread. activ. pulses: 8
#   running time: 485

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9956769   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9953048   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9948098   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9911033   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.982228   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79368854   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.793583   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.79350686   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7934699   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.79330957   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7932271   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7932266   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.7931329   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.79312766   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.7931155   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.7931149   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.7931086   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7931067   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7931051   REFERENCES:SIMDATES
