###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1301.029, activation diff: 1329.291, ratio: 1.022
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 95.761, activation diff: 1393.823, ratio: 14.555
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1914.356, activation diff: 1982.220, ratio: 1.035
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 237.150, activation diff: 1841.458, ratio: 7.765
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1985.367, activation diff: 1770.826, ratio: 0.892
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1978.854, activation diff: 29.153, ratio: 0.015

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1978.9
#   number of spread. activ. pulses: 7
#   running time: 441

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999991   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999902   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99971765   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99657726   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9701316   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999994   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   16   0.49999985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   17   0.49999982   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   18   0.49999967   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.49999955   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_190   20   0.499999   REFERENCES:SIMDATES
