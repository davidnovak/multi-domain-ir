###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 107.426, activation diff: 96.809, ratio: 0.901
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 323.372, activation diff: 216.026, ratio: 0.668
#   pulse 4: activated nodes: 9900, borderline nodes: 5857, overall activation: 957.494, activation diff: 634.122, ratio: 0.662
#   pulse 5: activated nodes: 10907, borderline nodes: 3937, overall activation: 1785.906, activation diff: 828.412, ratio: 0.464
#   pulse 6: activated nodes: 11164, borderline nodes: 2492, overall activation: 2637.288, activation diff: 851.382, ratio: 0.323
#   pulse 7: activated nodes: 11227, borderline nodes: 1425, overall activation: 3420.813, activation diff: 783.525, ratio: 0.229
#   pulse 8: activated nodes: 11246, borderline nodes: 806, overall activation: 4104.220, activation diff: 683.407, ratio: 0.167
#   pulse 9: activated nodes: 11295, borderline nodes: 583, overall activation: 4682.385, activation diff: 578.165, ratio: 0.123
#   pulse 10: activated nodes: 11314, borderline nodes: 445, overall activation: 5162.445, activation diff: 480.060, ratio: 0.093
#   pulse 11: activated nodes: 11319, borderline nodes: 352, overall activation: 5556.181, activation diff: 393.736, ratio: 0.071
#   pulse 12: activated nodes: 11320, borderline nodes: 286, overall activation: 5876.443, activation diff: 320.262, ratio: 0.054
#   pulse 13: activated nodes: 11321, borderline nodes: 236, overall activation: 6135.385, activation diff: 258.942, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11321
#   final overall activation: 6135.4
#   number of spread. activ. pulses: 13
#   running time: 576

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111164   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98706275   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.98489505   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.983555   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9753157   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95556533   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8693994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.86856496   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8677075   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.86748827   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.8664347   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.8664093   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.8664061   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.86594105   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   15   0.8659377   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   16   0.8659332   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.86588037   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   18   0.86577404   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.8656461   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.86560273   REFERENCES:SIMDATES
