###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1155.829, activation diff: 1182.231, ratio: 1.023
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 94.555, activation diff: 1250.021, ratio: 13.220
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 1854.858, activation diff: 1933.738, ratio: 1.043
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 138.061, activation diff: 1891.201, ratio: 13.698
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1913.827, activation diff: 1832.985, ratio: 0.958
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1831.914, activation diff: 139.169, ratio: 0.076
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1942.346, activation diff: 110.668, ratio: 0.057
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1942.362, activation diff: 0.230, ratio: 0.000

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1942.4
#   number of spread. activ. pulses: 9
#   running time: 461

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999986   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999953   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975356   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99332815   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999997   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.49999997   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.4999999   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.4999999   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.49999985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.49999985   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   18   0.49999985   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.49999958   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.4999993   REFERENCES:SIMDATES
