###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1347.540, activation diff: 1345.912, ratio: 0.999
#   pulse 3: activated nodes: 8765, borderline nodes: 3495, overall activation: 1732.964, activation diff: 754.439, ratio: 0.435
#   pulse 4: activated nodes: 10841, borderline nodes: 2481, overall activation: 3629.323, activation diff: 1907.741, ratio: 0.526
#   pulse 5: activated nodes: 11273, borderline nodes: 815, overall activation: 4509.107, activation diff: 879.785, ratio: 0.195
#   pulse 6: activated nodes: 11318, borderline nodes: 515, overall activation: 4838.883, activation diff: 329.776, ratio: 0.068
#   pulse 7: activated nodes: 11322, borderline nodes: 459, overall activation: 4955.353, activation diff: 116.470, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11322
#   final overall activation: 4955.4
#   number of spread. activ. pulses: 7
#   running time: 468

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99967945   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9996146   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999448   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.997236   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9929793   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7497948   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74975395   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.74973917   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.74969345   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   11   0.7496747   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7496693   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7496692   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.74965835   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   15   0.74965024   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7496477   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.74964726   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   18   0.74963844   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.74962544   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   20   0.74962366   REFERENCES:SIMDATES
