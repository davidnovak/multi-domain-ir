###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 242.076, activation diff: 229.372, ratio: 0.948
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 619.086, activation diff: 377.020, ratio: 0.609
#   pulse 4: activated nodes: 10912, borderline nodes: 1977, overall activation: 1320.922, activation diff: 701.836, ratio: 0.531
#   pulse 5: activated nodes: 11328, borderline nodes: 416, overall activation: 2076.967, activation diff: 756.045, ratio: 0.364
#   pulse 6: activated nodes: 11342, borderline nodes: 14, overall activation: 2775.258, activation diff: 698.291, ratio: 0.252
#   pulse 7: activated nodes: 11342, borderline nodes: 14, overall activation: 3379.249, activation diff: 603.991, ratio: 0.179
#   pulse 8: activated nodes: 11342, borderline nodes: 14, overall activation: 3884.607, activation diff: 505.358, ratio: 0.130
#   pulse 9: activated nodes: 11342, borderline nodes: 14, overall activation: 4299.373, activation diff: 414.766, ratio: 0.096
#   pulse 10: activated nodes: 11342, borderline nodes: 14, overall activation: 4635.643, activation diff: 336.270, ratio: 0.073
#   pulse 11: activated nodes: 11342, borderline nodes: 14, overall activation: 4906.012, activation diff: 270.369, ratio: 0.055
#   pulse 12: activated nodes: 11342, borderline nodes: 14, overall activation: 5122.108, activation diff: 216.096, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11342
#   final overall activation: 5122.1
#   number of spread. activ. pulses: 12
#   running time: 554

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890294   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9860618   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.98490816   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98291534   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9747807   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.96002805   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.76516354   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7643288   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   9   0.76400656   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.7639933   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7633793   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   12   0.762744   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   13   0.76271445   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   14   0.7626924   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   15   0.7626912   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.7626028   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.76244944   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   18   0.7624287   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.76223403   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7622179   REFERENCES:SIMDATES
