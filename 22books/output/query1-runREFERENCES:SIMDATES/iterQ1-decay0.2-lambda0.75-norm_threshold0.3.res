###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 97.061, activation diff: 86.443, ratio: 0.891
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 282.769, activation diff: 185.788, ratio: 0.657
#   pulse 4: activated nodes: 9833, borderline nodes: 5841, overall activation: 782.677, activation diff: 499.908, ratio: 0.639
#   pulse 5: activated nodes: 10818, borderline nodes: 4208, overall activation: 1426.137, activation diff: 643.460, ratio: 0.451
#   pulse 6: activated nodes: 11055, borderline nodes: 3033, overall activation: 2082.028, activation diff: 655.891, ratio: 0.315
#   pulse 7: activated nodes: 11198, borderline nodes: 1884, overall activation: 2683.705, activation diff: 601.678, ratio: 0.224
#   pulse 8: activated nodes: 11232, borderline nodes: 1215, overall activation: 3207.334, activation diff: 523.629, ratio: 0.163
#   pulse 9: activated nodes: 11272, borderline nodes: 831, overall activation: 3649.707, activation diff: 442.372, ratio: 0.121
#   pulse 10: activated nodes: 11280, borderline nodes: 697, overall activation: 4016.537, activation diff: 366.830, ratio: 0.091
#   pulse 11: activated nodes: 11304, borderline nodes: 601, overall activation: 4316.959, activation diff: 300.422, ratio: 0.070
#   pulse 12: activated nodes: 11312, borderline nodes: 543, overall activation: 4560.879, activation diff: 243.920, ratio: 0.053
#   pulse 13: activated nodes: 11316, borderline nodes: 474, overall activation: 4757.682, activation diff: 196.803, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11316
#   final overall activation: 4757.7
#   number of spread. activ. pulses: 13
#   running time: 526

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911116   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98705167   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.98486465   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9835188   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97524756   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95518684   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7727992   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.77205586   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7712878   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7710932   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.77013975   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   12   0.7701258   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.77010745   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.7696919   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   15   0.7696903   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   16   0.76968765   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.76965046   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   18   0.76954544   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7694429   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   20   0.76938874   REFERENCES:SIMDATES
