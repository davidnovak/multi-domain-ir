###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 586.269, activation diff: 570.552, ratio: 0.973
#   pulse 3: activated nodes: 8336, borderline nodes: 4231, overall activation: 1176.754, activation diff: 590.485, ratio: 0.502
#   pulse 4: activated nodes: 10502, borderline nodes: 4434, overall activation: 2634.691, activation diff: 1457.937, ratio: 0.553
#   pulse 5: activated nodes: 11211, borderline nodes: 1718, overall activation: 3778.442, activation diff: 1143.751, ratio: 0.303
#   pulse 6: activated nodes: 11280, borderline nodes: 699, overall activation: 4532.101, activation diff: 753.658, ratio: 0.166
#   pulse 7: activated nodes: 11316, borderline nodes: 436, overall activation: 4995.283, activation diff: 463.182, ratio: 0.093
#   pulse 8: activated nodes: 11321, borderline nodes: 314, overall activation: 5270.608, activation diff: 275.325, ratio: 0.052
#   pulse 9: activated nodes: 11323, borderline nodes: 257, overall activation: 5431.329, activation diff: 160.720, ratio: 0.030

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11323
#   final overall activation: 5431.3
#   number of spread. activ. pulses: 9
#   running time: 495

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99772495   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9974083   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9971398   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99395597   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98668945   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79681885   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7967508   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7966819   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7966393   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.7965096   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.79650927   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7965089   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7964382   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7964361   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.79641855   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.79641616   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   18   0.7964033   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   19   0.79639935   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.7963979   REFERENCES:SIMDATES
