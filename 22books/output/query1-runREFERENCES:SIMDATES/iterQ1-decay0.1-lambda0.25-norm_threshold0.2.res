###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1419.390, activation diff: 1421.603, ratio: 1.002
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 1898.420, activation diff: 1137.517, ratio: 0.599
#   pulse 4: activated nodes: 10830, borderline nodes: 2816, overall activation: 4827.105, activation diff: 2988.485, ratio: 0.619
#   pulse 5: activated nodes: 11260, borderline nodes: 1164, overall activation: 6347.828, activation diff: 1521.345, ratio: 0.240
#   pulse 6: activated nodes: 11320, borderline nodes: 238, overall activation: 6965.295, activation diff: 617.466, ratio: 0.089
#   pulse 7: activated nodes: 11329, borderline nodes: 126, overall activation: 7195.173, activation diff: 229.878, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11329
#   final overall activation: 7195.2
#   number of spread. activ. pulses: 7
#   running time: 460

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9995415   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9993981   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9992298   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99683344   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9920057   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8997034   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.89963055   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8995812   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   10   0.89947367   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.89947367   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   12   0.8994545   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8994454   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   14   0.8994305   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.89942473   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.8994173   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.8993925   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_293   18   0.89939237   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_328   19   0.8993922   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_154   20   0.89938563   REFERENCES:SIMDATES
