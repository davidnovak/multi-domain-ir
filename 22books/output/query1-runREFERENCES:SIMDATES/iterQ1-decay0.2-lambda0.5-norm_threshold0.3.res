###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 486.293, activation diff: 473.146, ratio: 0.973
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 966.936, activation diff: 483.704, ratio: 0.500
#   pulse 4: activated nodes: 10421, borderline nodes: 4918, overall activation: 2390.914, activation diff: 1423.978, ratio: 0.596
#   pulse 5: activated nodes: 11152, borderline nodes: 2621, overall activation: 3546.579, activation diff: 1155.665, ratio: 0.326
#   pulse 6: activated nodes: 11237, borderline nodes: 1006, overall activation: 4329.426, activation diff: 782.847, ratio: 0.181
#   pulse 7: activated nodes: 11287, borderline nodes: 656, overall activation: 4818.542, activation diff: 489.116, ratio: 0.102
#   pulse 8: activated nodes: 11314, borderline nodes: 487, overall activation: 5112.881, activation diff: 294.340, ratio: 0.058
#   pulse 9: activated nodes: 11320, borderline nodes: 402, overall activation: 5286.421, activation diff: 173.540, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 5286.4
#   number of spread. activ. pulses: 9
#   running time: 524

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.9975413   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9969965   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9967901   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99332786   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.984856   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.79677   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.7966862   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.796561   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.7964691   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   11   0.79635656   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   12   0.7963561   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7962752   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   14   0.79625714   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7962482   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.796227   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.7962092   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   18   0.79619014   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_319   19   0.7961669   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   20   0.79616356   REFERENCES:SIMDATES
