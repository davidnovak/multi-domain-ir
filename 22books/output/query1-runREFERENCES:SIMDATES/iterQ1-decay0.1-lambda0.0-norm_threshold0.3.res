###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 2075.646, activation diff: 2102.048, ratio: 1.013
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 1230.115, activation diff: 3305.372, ratio: 2.687
#   pulse 4: activated nodes: 10833, borderline nodes: 2800, overall activation: 5204.375, activation diff: 6280.752, ratio: 1.207
#   pulse 5: activated nodes: 11265, borderline nodes: 1139, overall activation: 2520.477, activation diff: 6301.912, ratio: 2.500
#   pulse 6: activated nodes: 11324, borderline nodes: 206, overall activation: 6185.932, activation diff: 5516.967, ratio: 0.892
#   pulse 7: activated nodes: 11329, borderline nodes: 124, overall activation: 6960.627, activation diff: 1146.262, ratio: 0.165
#   pulse 8: activated nodes: 11332, borderline nodes: 100, overall activation: 7138.605, activation diff: 237.543, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11332
#   final overall activation: 7138.6
#   number of spread. activ. pulses: 8
#   running time: 563

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999887   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999548   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMDATES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMDATES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMDATES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMDATES
