###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 120.582, activation diff: 108.516, ratio: 0.900
#   pulse 3: activated nodes: 8667, borderline nodes: 3411, overall activation: 282.211, activation diff: 161.659, ratio: 0.573
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 527.823, activation diff: 245.611, ratio: 0.465
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 782.112, activation diff: 254.289, ratio: 0.325
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1013.939, activation diff: 231.827, ratio: 0.229
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1213.597, activation diff: 199.658, ratio: 0.165
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1380.372, activation diff: 166.775, ratio: 0.121
#   pulse 9: activated nodes: 8935, borderline nodes: 3614, overall activation: 1517.108, activation diff: 136.737, ratio: 0.090
#   pulse 10: activated nodes: 8935, borderline nodes: 3614, overall activation: 1627.839, activation diff: 110.731, ratio: 0.068
#   pulse 11: activated nodes: 8935, borderline nodes: 3614, overall activation: 1716.729, activation diff: 88.890, ratio: 0.052
#   pulse 12: activated nodes: 8935, borderline nodes: 3614, overall activation: 1787.623, activation diff: 70.893, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1787.6
#   number of spread. activ. pulses: 12
#   running time: 498

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888481   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.98520124   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9836301   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98151565   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.97260314   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95435566   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.47801286   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.47742134   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.4771199   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.47709522   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.4765869   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.47611663   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.47609508   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.4760798   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   15   0.47603834   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.47599095   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   17   0.47590905   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.4758615   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   19   0.47584784   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   20   0.47577205   REFERENCES:SIMDATES
