###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 373.356, activation diff: 357.639, ratio: 0.958
#   pulse 3: activated nodes: 8336, borderline nodes: 4231, overall activation: 621.520, activation diff: 248.164, ratio: 0.399
#   pulse 4: activated nodes: 8610, borderline nodes: 4405, overall activation: 1112.090, activation diff: 490.570, ratio: 0.441
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 1469.915, activation diff: 357.825, ratio: 0.243
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 1693.942, activation diff: 224.027, ratio: 0.132
#   pulse 7: activated nodes: 8935, borderline nodes: 3614, overall activation: 1826.278, activation diff: 132.336, ratio: 0.072
#   pulse 8: activated nodes: 8935, borderline nodes: 3614, overall activation: 1902.110, activation diff: 75.832, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 1902.1
#   number of spread. activ. pulses: 8
#   running time: 459

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960674   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99545   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99481726   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99430984   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99021184   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97951895   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.49602354   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.4959376   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.49585176   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.4957984   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.49563518   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.49561888   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.4956119   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   14   0.49553752   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   15   0.49552375   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   16   0.49551892   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   17   0.4954933   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   18   0.49549127   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   19   0.49549013   REFERENCES:SIMDATES
1   Q1   politicalsketche00retsrich_96   20   0.49547377   REFERENCES:SIMDATES
