###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 791.173, activation diff: 773.034, ratio: 0.977
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1675.053, activation diff: 883.880, ratio: 0.528
#   pulse 4: activated nodes: 10830, borderline nodes: 2808, overall activation: 3599.350, activation diff: 1924.297, ratio: 0.535
#   pulse 5: activated nodes: 11261, borderline nodes: 905, overall activation: 5084.958, activation diff: 1485.609, ratio: 0.292
#   pulse 6: activated nodes: 11322, borderline nodes: 216, overall activation: 6052.233, activation diff: 967.275, ratio: 0.160
#   pulse 7: activated nodes: 11337, borderline nodes: 81, overall activation: 6643.319, activation diff: 591.086, ratio: 0.089
#   pulse 8: activated nodes: 11340, borderline nodes: 43, overall activation: 6994.011, activation diff: 350.693, ratio: 0.050
#   pulse 9: activated nodes: 11341, borderline nodes: 24, overall activation: 7198.938, activation diff: 204.927, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7198.9
#   number of spread. activ. pulses: 9
#   running time: 486

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980413   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.997838   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.99765205   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.997385   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.99448764   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98821807   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.8964498   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.8963904   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.8963476   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.8963268   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.89623666   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.8961903   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.89619017   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.89613724   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.8961344   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   16   0.89612806   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   17   0.89612746   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.89612365   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.8961232   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.8961201   REFERENCES:SIMDATES
