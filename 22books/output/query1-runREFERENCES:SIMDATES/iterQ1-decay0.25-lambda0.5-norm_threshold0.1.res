###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 662.745, activation diff: 644.606, ratio: 0.973
#   pulse 3: activated nodes: 8679, borderline nodes: 3420, overall activation: 1301.280, activation diff: 638.535, ratio: 0.491
#   pulse 4: activated nodes: 10827, borderline nodes: 2930, overall activation: 2568.066, activation diff: 1266.786, ratio: 0.493
#   pulse 5: activated nodes: 11242, borderline nodes: 1113, overall activation: 3519.707, activation diff: 951.641, ratio: 0.270
#   pulse 6: activated nodes: 11308, borderline nodes: 555, overall activation: 4129.573, activation diff: 609.866, ratio: 0.148
#   pulse 7: activated nodes: 11315, borderline nodes: 509, overall activation: 4497.871, activation diff: 368.298, ratio: 0.082
#   pulse 8: activated nodes: 11317, borderline nodes: 469, overall activation: 4714.185, activation diff: 216.314, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11317
#   final overall activation: 4714.2
#   number of spread. activ. pulses: 8
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   2   0.99567676   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   3   0.9953048   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99480873   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9911003   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98221856   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.7440829   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   8   0.74398404   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   9   0.7439126   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   10   0.743878   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   11   0.74372756   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_318   12   0.7436503   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_323   13   0.7436494   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   14   0.7435621   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_326   15   0.7435571   REFERENCES:SIMDATES
1   Q1   essentialsinmod01howegoog_474   16   0.7435458   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   17   0.74354446   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_17   18   0.7435391   REFERENCES:SIMDATES
1   Q1   ouroldworldbackg00bearrich_446   19   0.74353635   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_23   20   0.74353445   REFERENCES:SIMDATES
