###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5321, borderline nodes: 5252, overall activation: 1555.006, activation diff: 1586.443, ratio: 1.020
#   pulse 3: activated nodes: 8935, borderline nodes: 3614, overall activation: 141.196, activation diff: 1599.407, ratio: 11.328
#   pulse 4: activated nodes: 8935, borderline nodes: 3614, overall activation: 2068.393, activation diff: 1983.309, ratio: 0.959
#   pulse 5: activated nodes: 8935, borderline nodes: 3614, overall activation: 2021.986, activation diff: 102.817, ratio: 0.051
#   pulse 6: activated nodes: 8935, borderline nodes: 3614, overall activation: 2096.590, activation diff: 74.735, ratio: 0.036

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8935
#   final overall activation: 2096.6
#   number of spread. activ. pulses: 6
#   running time: 439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999991   REFERENCES:SIMDATES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999657   REFERENCES:SIMDATES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981766   REFERENCES:SIMDATES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950544   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_118   8   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_345   9   0.5   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999997   REFERENCES:SIMDATES
1   Q1   europesincenapol00leveuoft_16   11   0.49999997   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_222   12   0.49999994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_327   13   0.49999994   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_218   14   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_440   15   0.49999994   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_88   16   0.49999988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_146   17   0.49999988   REFERENCES:SIMDATES
1   Q1   englishcyclopae05kniggoog_267   18   0.49999988   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_221   19   0.4999997   REFERENCES:SIMDATES
1   Q1   encyclopediaame28unkngoog_367   20   0.4999995   REFERENCES:SIMDATES
