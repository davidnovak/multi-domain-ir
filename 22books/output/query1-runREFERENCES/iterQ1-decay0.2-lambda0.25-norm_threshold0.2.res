###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1262.628, activation diff: 1265.948, ratio: 1.003
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1535.202, activation diff: 883.250, ratio: 0.575
#   pulse 4: activated nodes: 10788, borderline nodes: 2897, overall activation: 3787.690, activation diff: 2307.013, ratio: 0.609
#   pulse 5: activated nodes: 11237, borderline nodes: 1312, overall activation: 4917.828, activation diff: 1130.298, ratio: 0.230
#   pulse 6: activated nodes: 11300, borderline nodes: 422, overall activation: 5361.824, activation diff: 443.996, ratio: 0.083
#   pulse 7: activated nodes: 11315, borderline nodes: 241, overall activation: 5522.580, activation diff: 160.756, ratio: 0.029

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 5522.6
#   number of spread. activ. pulses: 7
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99976575   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99951136   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   3   0.99922943   REFERENCES
1   Q1   cambridgemodern09protgoog_449   4   0.9991933   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99683297   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9918895   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7996986   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.79967165   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7995893   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   10   0.79953223   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.7995322   REFERENCES
1   Q1   europesincenapol00leveuoft_16   12   0.7995151   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.7995068   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7994887   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.7994807   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   16   0.7994598   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.79945093   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.79945076   REFERENCES
1   Q1   politicalsketche00retsrich_154   19   0.7994502   REFERENCES
1   Q1   europesincenapol00leveuoft_17   20   0.79942113   REFERENCES
