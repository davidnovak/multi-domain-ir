###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 120.524, activation diff: 108.459, ratio: 0.900
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 277.350, activation diff: 156.857, ratio: 0.566
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 508.768, activation diff: 231.419, ratio: 0.455
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 747.061, activation diff: 238.292, ratio: 0.319
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 964.936, activation diff: 217.875, ratio: 0.226
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1153.477, activation diff: 188.541, ratio: 0.163
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1311.733, activation diff: 158.256, ratio: 0.121
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 1442.073, activation diff: 130.340, ratio: 0.090
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1548.058, activation diff: 105.984, ratio: 0.068
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 1633.452, activation diff: 85.395, ratio: 0.052
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1701.787, activation diff: 68.334, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1701.8
#   number of spread. activ. pulses: 12
#   running time: 456

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888481   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98518586   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9835423   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9815152   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9726013   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95421255   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4780013   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.47740835   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.47709787   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.47709128   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.47657642   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.47609687   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.47609508   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.47606736   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.47599095   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.47591025   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.47585404   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.47584784   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   19   0.4758256   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.47570485   REFERENCES
