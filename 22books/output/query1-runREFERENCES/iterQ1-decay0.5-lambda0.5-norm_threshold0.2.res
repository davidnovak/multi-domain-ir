###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 373.121, activation diff: 357.593, ratio: 0.958
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 604.340, activation diff: 231.219, ratio: 0.383
#   pulse 4: activated nodes: 8333, borderline nodes: 4229, overall activation: 1056.310, activation diff: 451.970, ratio: 0.428
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1392.711, activation diff: 336.401, ratio: 0.242
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1606.227, activation diff: 213.516, ratio: 0.133
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1733.619, activation diff: 127.392, ratio: 0.073
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1807.179, activation diff: 73.560, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1807.2
#   number of spread. activ. pulses: 8
#   running time: 398

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960674   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99543166   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9946587   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9943098   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9902113   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.97931373   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4960158   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49593621   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49583757   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49579814   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.4956166   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.4956119   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.4956119   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   14   0.49551892   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.49551782   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.49549586   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.49549013   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.49548915   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.49548307   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.49545184   REFERENCES
