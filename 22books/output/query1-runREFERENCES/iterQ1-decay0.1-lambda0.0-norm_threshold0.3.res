###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2075.451, activation diff: 2102.183, ratio: 1.013
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1223.517, activation diff: 3298.969, ratio: 2.696
#   pulse 4: activated nodes: 10794, borderline nodes: 2761, overall activation: 5117.949, activation diff: 6341.467, ratio: 1.239
#   pulse 5: activated nodes: 11256, borderline nodes: 1140, overall activation: 1877.544, activation diff: 6995.494, ratio: 3.726
#   pulse 6: activated nodes: 11318, borderline nodes: 200, overall activation: 5212.159, activation diff: 7089.704, ratio: 1.360
#   pulse 7: activated nodes: 11324, borderline nodes: 121, overall activation: 1898.699, activation diff: 7110.858, ratio: 3.745
#   pulse 8: activated nodes: 11326, borderline nodes: 97, overall activation: 5218.653, activation diff: 7117.352, ratio: 1.364
#   pulse 9: activated nodes: 11327, borderline nodes: 92, overall activation: 1900.939, activation diff: 7119.591, ratio: 3.745
#   pulse 10: activated nodes: 11327, borderline nodes: 91, overall activation: 5219.867, activation diff: 7120.805, ratio: 1.364
#   pulse 11: activated nodes: 11327, borderline nodes: 91, overall activation: 1901.353, activation diff: 7121.220, ratio: 3.745
#   pulse 12: activated nodes: 11327, borderline nodes: 91, overall activation: 5220.143, activation diff: 7121.496, ratio: 1.364
#   pulse 13: activated nodes: 11327, borderline nodes: 90, overall activation: 1901.455, activation diff: 7121.598, ratio: 3.745
#   pulse 14: activated nodes: 11327, borderline nodes: 90, overall activation: 5220.215, activation diff: 7121.670, ratio: 1.364
#   pulse 15: activated nodes: 11327, borderline nodes: 90, overall activation: 1901.484, activation diff: 7121.699, ratio: 3.745
#   pulse 16: activated nodes: 11327, borderline nodes: 90, overall activation: 5220.235, activation diff: 7121.719, ratio: 1.364
#   pulse 17: activated nodes: 11327, borderline nodes: 90, overall activation: 1901.493, activation diff: 7121.728, ratio: 3.745
#   pulse 18: activated nodes: 11327, borderline nodes: 90, overall activation: 5220.241, activation diff: 7121.734, ratio: 1.364
#   pulse 19: activated nodes: 11327, borderline nodes: 90, overall activation: 1901.497, activation diff: 7121.737, ratio: 3.745
#   pulse 20: activated nodes: 11327, borderline nodes: 90, overall activation: 5220.243, activation diff: 7121.739, ratio: 1.364

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11327
#   final overall activation: 5220.2
#   number of spread. activ. pulses: 20
#   running time: 744

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999887   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999548   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES
