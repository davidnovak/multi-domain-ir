###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 903.171, activation diff: 902.723, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 824.161, activation diff: 185.425, ratio: 0.225
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1542.749, activation diff: 718.588, ratio: 0.466
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1822.760, activation diff: 280.011, ratio: 0.154
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1915.099, activation diff: 92.339, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1915.1
#   number of spread. activ. pulses: 6
#   running time: 380

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99921596   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9986546   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9980116   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99790347   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9950739   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98816174   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.4994107   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.49934316   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49925357   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.4991824   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.49910748   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.49910748   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   13   0.4990446   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.49904454   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.4990345   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.4990242   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.49899036   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.49899036   REFERENCES
1   Q1   politicalsketche00retsrich_154   19   0.49898082   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   20   0.49896258   REFERENCES
