###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 662.457, activation diff: 644.386, ratio: 0.973
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1289.377, activation diff: 626.920, ratio: 0.486
#   pulse 4: activated nodes: 10788, borderline nodes: 2891, overall activation: 2539.301, activation diff: 1249.924, ratio: 0.492
#   pulse 5: activated nodes: 11236, borderline nodes: 1120, overall activation: 3482.326, activation diff: 943.025, ratio: 0.271
#   pulse 6: activated nodes: 11293, borderline nodes: 566, overall activation: 4087.037, activation diff: 604.711, ratio: 0.148
#   pulse 7: activated nodes: 11299, borderline nodes: 527, overall activation: 4451.832, activation diff: 364.795, ratio: 0.082
#   pulse 8: activated nodes: 11301, borderline nodes: 488, overall activation: 4665.686, activation diff: 213.854, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11301
#   final overall activation: 4665.7
#   number of spread. activ. pulses: 8
#   running time: 465

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99566513   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9952085   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99480873   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9911003   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98204243   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7440762   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74398404   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7439004   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.743878   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.74371135   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7436502   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7436494   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.74356204   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.743557   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.7435458   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   17   0.74353516   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.74353445   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.7435285   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.743492   REFERENCES
