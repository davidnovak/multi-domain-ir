###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 17.583, activation diff: 17.583, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 550.548, activation diff: 535.020, ratio: 0.972
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1065.593, activation diff: 515.045, ratio: 0.483
#   pulse 4: activated nodes: 10431, borderline nodes: 4436, overall activation: 2308.571, activation diff: 1242.978, ratio: 0.538
#   pulse 5: activated nodes: 11203, borderline nodes: 1885, overall activation: 3276.527, activation diff: 967.956, ratio: 0.295
#   pulse 6: activated nodes: 11231, borderline nodes: 796, overall activation: 3910.687, activation diff: 634.161, ratio: 0.162
#   pulse 7: activated nodes: 11255, borderline nodes: 627, overall activation: 4297.978, activation diff: 387.291, ratio: 0.090
#   pulse 8: activated nodes: 11262, borderline nodes: 573, overall activation: 4526.806, activation diff: 228.828, ratio: 0.051
#   pulse 9: activated nodes: 11262, borderline nodes: 548, overall activation: 4659.530, activation diff: 132.725, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11262
#   final overall activation: 4659.5
#   number of spread. activ. pulses: 9
#   running time: 482

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9980337   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9977157   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.997329   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9971389   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9939533   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9865751   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7470119   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.74695385   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7468792   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7468493   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.74672735   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.74672675   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7467138   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.7466592   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.7466587   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7466411   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.74663246   REFERENCES
1   Q1   essentialsinmod01howegoog_474   18   0.746628   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   19   0.7466229   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.746609   REFERENCES
