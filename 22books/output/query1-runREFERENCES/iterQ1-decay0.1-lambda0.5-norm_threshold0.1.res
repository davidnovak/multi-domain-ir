###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 790.877, activation diff: 772.806, ratio: 0.977
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1667.123, activation diff: 876.246, ratio: 0.526
#   pulse 4: activated nodes: 10791, borderline nodes: 2769, overall activation: 3579.027, activation diff: 1911.904, ratio: 0.534
#   pulse 5: activated nodes: 11255, borderline nodes: 906, overall activation: 5057.563, activation diff: 1478.537, ratio: 0.292
#   pulse 6: activated nodes: 11316, borderline nodes: 211, overall activation: 6017.997, activation diff: 960.434, ratio: 0.160
#   pulse 7: activated nodes: 11330, borderline nodes: 76, overall activation: 6603.002, activation diff: 585.005, ratio: 0.089
#   pulse 8: activated nodes: 11334, borderline nodes: 44, overall activation: 6948.780, activation diff: 345.777, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 6948.8
#   number of spread. activ. pulses: 8
#   running time: 462

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608254   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99566543   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9952085   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99481153   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99110734   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9820642   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8928914   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.8927808   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.8926807   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.89265364   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.8924539   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.8923806   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.89238036   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.8922745   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.8922687   REFERENCES
1   Q1   essentialsinmod01howegoog_474   16   0.89225495   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   17   0.8922465   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   18   0.89224625   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.89223456   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.89219105   REFERENCES
