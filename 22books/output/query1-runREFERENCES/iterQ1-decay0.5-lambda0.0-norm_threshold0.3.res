###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1155.647, activation diff: 1182.379, ratio: 1.023
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.736, activation diff: 1217.383, ratio: 19.719
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1778.923, activation diff: 1840.659, ratio: 1.035
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.073, activation diff: 1840.996, ratio: 29.658
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.268, activation diff: 1841.341, ratio: 1.035
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.088, activation diff: 1841.356, ratio: 29.657
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.364, ratio: 1.035
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.365, ratio: 29.657
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.366, ratio: 29.657
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.366, ratio: 29.657
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.366, ratio: 29.657
#   pulse 16: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035
#   pulse 17: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.366, ratio: 29.657
#   pulse 18: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035
#   pulse 19: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.089, activation diff: 1841.366, ratio: 29.657
#   pulse 20: activated nodes: 8664, borderline nodes: 3409, overall activation: 1779.276, activation diff: 1841.366, ratio: 1.035

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1779.3
#   number of spread. activ. pulses: 20
#   running time: 511

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999987   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995327   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99753827   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99332964   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.49999994   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49999985   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999985   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.49999958   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.49999958   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   13   0.49999887   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.49999696   REFERENCES
1   Q1   europesincenapol00leveuoft_17   15   0.49999171   REFERENCES
1   Q1   bostoncollegebul0405bost_26   16   0.49999171   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.49999171   REFERENCES
1   Q1   essentialsinmod01howegoog_461   18   0.49999171   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   19   0.49999171   REFERENCES
1   Q1   politicalsketche00retsrich_96   20   0.49999171   REFERENCES
