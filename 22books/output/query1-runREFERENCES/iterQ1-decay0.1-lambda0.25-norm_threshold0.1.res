###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 25.124, activation diff: 28.124, ratio: 1.119
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1612.885, activation diff: 1612.436, ratio: 1.000
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2307.044, activation diff: 1170.050, ratio: 0.507
#   pulse 4: activated nodes: 10802, borderline nodes: 2442, overall activation: 5136.566, activation diff: 2855.630, ratio: 0.556
#   pulse 5: activated nodes: 11265, borderline nodes: 789, overall activation: 6562.133, activation diff: 1425.602, ratio: 0.217
#   pulse 6: activated nodes: 11327, borderline nodes: 107, overall activation: 7113.228, activation diff: 551.094, ratio: 0.077
#   pulse 7: activated nodes: 11334, borderline nodes: 37, overall activation: 7311.377, activation diff: 198.149, ratio: 0.027

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11334
#   final overall activation: 7311.4
#   number of spread. activ. pulses: 7
#   running time: 428

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.999804   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.999663   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99950236   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9994483   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9972363   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99288017   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.89973474   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89970464   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89966416   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8996321   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   11   0.8996031   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.8996031   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8995776   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.8995749   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   15   0.8995683   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   16   0.899561   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.8995505   REFERENCES
1   Q1   politicalsketche00retsrich_96   18   0.8995505   REFERENCES
1   Q1   encyclopediaame28unkngoog_328   19   0.89954543   REFERENCES
1   Q1   politicalsketche00retsrich_154   20   0.89953613   REFERENCES
