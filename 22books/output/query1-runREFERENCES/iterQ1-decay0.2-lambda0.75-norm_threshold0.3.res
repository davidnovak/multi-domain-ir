###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 97.009, activation diff: 86.392, ratio: 0.891
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 280.848, activation diff: 183.920, ratio: 0.655
#   pulse 4: activated nodes: 9826, borderline nodes: 5834, overall activation: 774.310, activation diff: 493.462, ratio: 0.637
#   pulse 5: activated nodes: 10808, borderline nodes: 4214, overall activation: 1410.855, activation diff: 636.545, ratio: 0.451
#   pulse 6: activated nodes: 11045, borderline nodes: 3097, overall activation: 2060.805, activation diff: 649.950, ratio: 0.315
#   pulse 7: activated nodes: 11192, borderline nodes: 1901, overall activation: 2657.700, activation diff: 596.894, ratio: 0.225
#   pulse 8: activated nodes: 11223, borderline nodes: 1219, overall activation: 3177.491, activation diff: 519.791, ratio: 0.164
#   pulse 9: activated nodes: 11238, borderline nodes: 807, overall activation: 3616.712, activation diff: 439.221, ratio: 0.121
#   pulse 10: activated nodes: 11274, borderline nodes: 694, overall activation: 3980.872, activation diff: 364.160, ratio: 0.091
#   pulse 11: activated nodes: 11291, borderline nodes: 602, overall activation: 4279.012, activation diff: 298.140, ratio: 0.070
#   pulse 12: activated nodes: 11301, borderline nodes: 556, overall activation: 4520.963, activation diff: 241.951, ratio: 0.054
#   pulse 13: activated nodes: 11304, borderline nodes: 475, overall activation: 4716.065, activation diff: 195.102, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11304
#   final overall activation: 4716.1
#   number of spread. activ. pulses: 13
#   running time: 535

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911116   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.987033   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9847502   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9835187   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9752468   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95505416   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7727814   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7720555   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.77126306   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.77109194   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.77013886   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.77010745   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.770104   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7696857   REFERENCES
1   Q1   europesincenapol00leveuoft_17   15   0.76967955   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   16   0.7696762   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.76965046   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.7695429   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.7694429   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.76938874   REFERENCES
