###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 486.073, activation diff: 473.216, ratio: 0.974
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 957.635, activation diff: 474.866, ratio: 0.496
#   pulse 4: activated nodes: 10384, borderline nodes: 4881, overall activation: 2366.626, activation diff: 1408.991, ratio: 0.595
#   pulse 5: activated nodes: 11143, borderline nodes: 2675, overall activation: 3514.856, activation diff: 1148.230, ratio: 0.327
#   pulse 6: activated nodes: 11231, borderline nodes: 1004, overall activation: 4292.894, activation diff: 778.038, ratio: 0.181
#   pulse 7: activated nodes: 11279, borderline nodes: 663, overall activation: 4778.160, activation diff: 485.266, ratio: 0.102
#   pulse 8: activated nodes: 11303, borderline nodes: 491, overall activation: 5069.420, activation diff: 291.260, ratio: 0.057
#   pulse 9: activated nodes: 11312, borderline nodes: 401, overall activation: 5240.606, activation diff: 171.187, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11312
#   final overall activation: 5240.6
#   number of spread. activ. pulses: 9
#   running time: 442

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9975271   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99686885   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9967901   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99332774   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98474264   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.79675794   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7966862   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7965436   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7964691   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.79635656   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7963561   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.79627466   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7962481   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.7962322   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.796227   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.7961981   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.7961669   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7961656   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.79616356   REFERENCES
