###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 172.554, activation diff: 160.489, ratio: 0.930
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 446.535, activation diff: 274.012, ratio: 0.614
#   pulse 4: activated nodes: 10724, borderline nodes: 4058, overall activation: 996.147, activation diff: 549.613, ratio: 0.552
#   pulse 5: activated nodes: 11175, borderline nodes: 2149, overall activation: 1613.624, activation diff: 617.476, ratio: 0.383
#   pulse 6: activated nodes: 11224, borderline nodes: 1083, overall activation: 2199.281, activation diff: 585.658, ratio: 0.266
#   pulse 7: activated nodes: 11266, borderline nodes: 694, overall activation: 2714.055, activation diff: 514.773, ratio: 0.190
#   pulse 8: activated nodes: 11292, borderline nodes: 588, overall activation: 3149.298, activation diff: 435.243, ratio: 0.138
#   pulse 9: activated nodes: 11298, borderline nodes: 539, overall activation: 3509.163, activation diff: 359.865, ratio: 0.103
#   pulse 10: activated nodes: 11299, borderline nodes: 527, overall activation: 3802.532, activation diff: 293.369, ratio: 0.077
#   pulse 11: activated nodes: 11301, borderline nodes: 523, overall activation: 4039.428, activation diff: 236.896, ratio: 0.059
#   pulse 12: activated nodes: 11301, borderline nodes: 495, overall activation: 4229.457, activation diff: 190.029, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11301
#   final overall activation: 4229.5
#   number of spread. activ. pulses: 12
#   running time: 483

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888482   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98520625   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98357886   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98160875   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9728305   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9553335   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7170104   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.71616936   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.71568346   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7156669   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.714905   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7143934   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7143637   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.7142538   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.71417654   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.7140562   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.71403503   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   18   0.71400285   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   19   0.7138608   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   20   0.7138226   REFERENCES
