###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2336.144, activation diff: 2365.309, ratio: 1.012
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1364.029, activation diff: 3700.173, ratio: 2.713
#   pulse 4: activated nodes: 10798, borderline nodes: 2462, overall activation: 5208.488, activation diff: 6572.517, ratio: 1.262
#   pulse 5: activated nodes: 11261, borderline nodes: 815, overall activation: 1960.360, activation diff: 7168.848, ratio: 3.657
#   pulse 6: activated nodes: 11326, borderline nodes: 116, overall activation: 5288.088, activation diff: 7248.448, ratio: 1.371
#   pulse 7: activated nodes: 11333, borderline nodes: 50, overall activation: 1978.669, activation diff: 7266.757, ratio: 3.673
#   pulse 8: activated nodes: 11334, borderline nodes: 27, overall activation: 5293.912, activation diff: 7272.581, ratio: 1.374
#   pulse 9: activated nodes: 11338, borderline nodes: 27, overall activation: 1980.695, activation diff: 7274.607, ratio: 3.673
#   pulse 10: activated nodes: 11338, borderline nodes: 27, overall activation: 5294.887, activation diff: 7275.582, ratio: 1.374
#   pulse 11: activated nodes: 11338, borderline nodes: 27, overall activation: 1981.127, activation diff: 7276.014, ratio: 3.673
#   pulse 12: activated nodes: 11338, borderline nodes: 25, overall activation: 5295.099, activation diff: 7276.226, ratio: 1.374
#   pulse 13: activated nodes: 11338, borderline nodes: 25, overall activation: 1981.263, activation diff: 7276.361, ratio: 3.673
#   pulse 14: activated nodes: 11338, borderline nodes: 25, overall activation: 5295.159, activation diff: 7276.422, ratio: 1.374
#   pulse 15: activated nodes: 11338, borderline nodes: 25, overall activation: 1981.314, activation diff: 7276.473, ratio: 3.673
#   pulse 16: activated nodes: 11338, borderline nodes: 25, overall activation: 5295.180, activation diff: 7276.494, ratio: 1.374
#   pulse 17: activated nodes: 11338, borderline nodes: 25, overall activation: 1981.336, activation diff: 7276.516, ratio: 3.673
#   pulse 18: activated nodes: 11338, borderline nodes: 25, overall activation: 5295.188, activation diff: 7276.524, ratio: 1.374
#   pulse 19: activated nodes: 11338, borderline nodes: 25, overall activation: 1981.346, activation diff: 7276.534, ratio: 3.673
#   pulse 20: activated nodes: 11338, borderline nodes: 25, overall activation: 5295.191, activation diff: 7276.537, ratio: 1.374

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11338
#   final overall activation: 5295.2
#   number of spread. activ. pulses: 20
#   running time: 680

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.999999   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999591   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES
