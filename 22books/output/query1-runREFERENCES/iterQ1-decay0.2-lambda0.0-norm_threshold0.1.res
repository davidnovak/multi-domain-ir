###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2288.692, activation diff: 2320.192, ratio: 1.014
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1087.543, activation diff: 3376.235, ratio: 3.104
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 4258.685, activation diff: 5346.228, ratio: 1.255
#   pulse 5: activated nodes: 11301, borderline nodes: 513, overall activation: 1421.311, activation diff: 5679.996, ratio: 3.996
#   pulse 6: activated nodes: 11323, borderline nodes: 146, overall activation: 4296.787, activation diff: 5718.098, ratio: 1.331
#   pulse 7: activated nodes: 11327, borderline nodes: 102, overall activation: 1429.106, activation diff: 5725.894, ratio: 4.007
#   pulse 8: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.689, activation diff: 5727.796, ratio: 1.332
#   pulse 9: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.639, activation diff: 5728.328, ratio: 4.007
#   pulse 10: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.899, activation diff: 5728.537, ratio: 1.333
#   pulse 11: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.703, activation diff: 5728.602, ratio: 4.007
#   pulse 12: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.940, activation diff: 5728.644, ratio: 1.333
#   pulse 13: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.715, activation diff: 5728.655, ratio: 4.007
#   pulse 14: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.951, activation diff: 5728.666, ratio: 1.333
#   pulse 15: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.718, activation diff: 5728.669, ratio: 4.007
#   pulse 16: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.954, activation diff: 5728.672, ratio: 1.333
#   pulse 17: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.718, activation diff: 5728.672, ratio: 4.007
#   pulse 18: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.955, activation diff: 5728.673, ratio: 1.333
#   pulse 19: activated nodes: 11328, borderline nodes: 86, overall activation: 1429.718, activation diff: 5728.674, ratio: 4.007
#   pulse 20: activated nodes: 11328, borderline nodes: 86, overall activation: 4298.955, activation diff: 5728.674, ratio: 1.333

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11328
#   final overall activation: 4299.0
#   number of spread. activ. pulses: 20
#   running time: 658

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999991   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.999963   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES
1   Q1   historyofuniteds07good_169   15   0.8   REFERENCES
1   Q1   historyofuniteds07good_71   16   0.8   REFERENCES
1   Q1   historyofuniteds07good_75   17   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_800   18   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_807   19   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_805   20   0.8   REFERENCES
