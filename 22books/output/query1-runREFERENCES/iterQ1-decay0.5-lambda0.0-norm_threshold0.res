###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1552.665, activation diff: 1586.394, ratio: 1.022
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.203, activation diff: 1614.868, ratio: 25.961
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.813, activation diff: 2008.016, ratio: 1.032
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.311, activation diff: 2008.124, ratio: 32.228
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.860, activation diff: 2008.171, ratio: 1.032
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.174, ratio: 32.227
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 16: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 17: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 18: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032
#   pulse 19: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.314, activation diff: 2008.175, ratio: 32.227
#   pulse 20: activated nodes: 8664, borderline nodes: 3409, overall activation: 1945.861, activation diff: 2008.175, ratio: 1.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1945.9
#   number of spread. activ. pulses: 20
#   running time: 574

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999991   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999657   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981765   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99505436   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.49999997   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49999988   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999988   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.4999997   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.4999997   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   13   0.49999917   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.49999774   REFERENCES
1   Q1   europesincenapol00leveuoft_17   15   0.49999386   REFERENCES
1   Q1   bostoncollegebul0405bost_26   16   0.49999386   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.49999386   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.49999386   REFERENCES
1   Q1   essentialsinmod01howegoog_461   19   0.49999386   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.49999386   REFERENCES
