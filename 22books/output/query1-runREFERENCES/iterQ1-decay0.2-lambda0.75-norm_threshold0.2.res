###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 135.125, activation diff: 123.752, ratio: 0.916
#   pulse 3: activated nodes: 8066, borderline nodes: 4568, overall activation: 372.806, activation diff: 237.735, ratio: 0.638
#   pulse 4: activated nodes: 10310, borderline nodes: 5383, overall activation: 939.805, activation diff: 566.999, ratio: 0.603
#   pulse 5: activated nodes: 10998, borderline nodes: 3465, overall activation: 1618.991, activation diff: 679.186, ratio: 0.420
#   pulse 6: activated nodes: 11193, borderline nodes: 1883, overall activation: 2286.701, activation diff: 667.710, ratio: 0.292
#   pulse 7: activated nodes: 11224, borderline nodes: 1088, overall activation: 2886.390, activation diff: 599.689, ratio: 0.208
#   pulse 8: activated nodes: 11271, borderline nodes: 747, overall activation: 3400.660, activation diff: 514.270, ratio: 0.151
#   pulse 9: activated nodes: 11298, borderline nodes: 584, overall activation: 3830.273, activation diff: 429.613, ratio: 0.112
#   pulse 10: activated nodes: 11306, borderline nodes: 477, overall activation: 4183.365, activation diff: 353.092, ratio: 0.084
#   pulse 11: activated nodes: 11312, borderline nodes: 396, overall activation: 4470.398, activation diff: 287.033, ratio: 0.064
#   pulse 12: activated nodes: 11315, borderline nodes: 350, overall activation: 4701.934, activation diff: 231.536, ratio: 0.049

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 4701.9
#   number of spread. activ. pulses: 12
#   running time: 520

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885731   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.98412347   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9819174   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.98002744   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97064865   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9499835   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.764348   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7633961   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7626539   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.76255405   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7615228   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7612022   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.76116794   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.7607928   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   15   0.76078796   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.76073533   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   17   0.7607235   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   18   0.76061237   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.76044726   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.76041824   REFERENCES
