###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1730.525, activation diff: 1757.257, ratio: 1.015
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 721.140, activation diff: 2451.665, ratio: 3.400
#   pulse 4: activated nodes: 10788, borderline nodes: 2927, overall activation: 3650.311, activation diff: 4371.451, ratio: 1.198
#   pulse 5: activated nodes: 11233, borderline nodes: 1346, overall activation: 1045.551, activation diff: 4695.861, ratio: 4.491
#   pulse 6: activated nodes: 11280, borderline nodes: 608, overall activation: 3691.267, activation diff: 4736.818, ratio: 1.283
#   pulse 7: activated nodes: 11298, borderline nodes: 547, overall activation: 1055.062, activation diff: 4746.329, ratio: 4.499
#   pulse 8: activated nodes: 11299, borderline nodes: 546, overall activation: 3693.757, activation diff: 4748.818, ratio: 1.286
#   pulse 9: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.732, activation diff: 4749.489, ratio: 4.499
#   pulse 10: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.169, activation diff: 4749.901, ratio: 1.286
#   pulse 11: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.810, activation diff: 4749.979, ratio: 4.499
#   pulse 12: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.246, activation diff: 4750.056, ratio: 1.286
#   pulse 13: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.821, activation diff: 4750.067, ratio: 4.499
#   pulse 14: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.260, activation diff: 4750.081, ratio: 1.286
#   pulse 15: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.823, activation diff: 4750.083, ratio: 4.499
#   pulse 16: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.263, activation diff: 4750.085, ratio: 1.286
#   pulse 17: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.823, activation diff: 4750.086, ratio: 4.499
#   pulse 18: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.263, activation diff: 4750.086, ratio: 1.286
#   pulse 19: activated nodes: 11299, borderline nodes: 522, overall activation: 1055.823, activation diff: 4750.086, ratio: 4.499
#   pulse 20: activated nodes: 11299, borderline nodes: 522, overall activation: 3694.263, activation diff: 4750.086, ratio: 1.286

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11299
#   final overall activation: 3694.3
#   number of spread. activ. pulses: 20
#   running time: 664

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999988   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995464   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9975411   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES
1   Q1   essentialsinmod01howegoog_473   7   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES
1   Q1   historyofuniteds07good_71   10   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_560   14   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_575   15   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_579   16   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   17   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_576   18   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_541   19   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_540   20   0.75   REFERENCES
