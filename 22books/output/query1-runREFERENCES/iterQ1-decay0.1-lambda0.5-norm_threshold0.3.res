###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 16.366, activation diff: 16.366, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 544.819, activation diff: 531.962, ratio: 0.976
#   pulse 3: activated nodes: 8333, borderline nodes: 4229, overall activation: 1130.260, activation diff: 589.159, ratio: 0.521
#   pulse 4: activated nodes: 10394, borderline nodes: 4792, overall activation: 2957.477, activation diff: 1827.218, ratio: 0.618
#   pulse 5: activated nodes: 11185, borderline nodes: 2254, overall activation: 4472.597, activation diff: 1515.120, ratio: 0.339
#   pulse 6: activated nodes: 11264, borderline nodes: 741, overall activation: 5507.480, activation diff: 1034.883, ratio: 0.188
#   pulse 7: activated nodes: 11309, borderline nodes: 415, overall activation: 6156.801, activation diff: 649.321, ratio: 0.105
#   pulse 8: activated nodes: 11314, borderline nodes: 272, overall activation: 6548.577, activation diff: 391.776, ratio: 0.060
#   pulse 9: activated nodes: 11320, borderline nodes: 186, overall activation: 6780.246, activation diff: 231.669, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11320
#   final overall activation: 6780.2
#   number of spread. activ. pulses: 9
#   running time: 500

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99801385   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99752736   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99686885   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9967923   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9933335   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98478556   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.89635265   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89627194   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89611167   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8960278   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.89590114   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.89590096   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   13   0.8958106   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.8957792   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   15   0.8957614   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.89575857   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.895723   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   18   0.89568794   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.8956872   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.895684   REFERENCES
