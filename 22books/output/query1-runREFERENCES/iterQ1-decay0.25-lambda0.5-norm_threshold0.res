###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 770.166, activation diff: 750.301, ratio: 0.974
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1515.940, activation diff: 745.774, ratio: 0.492
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 2768.496, activation diff: 1252.556, ratio: 0.452
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 3678.512, activation diff: 910.016, ratio: 0.247
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4249.923, activation diff: 571.411, ratio: 0.134
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 4589.957, activation diff: 340.034, ratio: 0.074
#   pulse 8: activated nodes: 11305, borderline nodes: 469, overall activation: 4787.151, activation diff: 197.194, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4787.2
#   number of spread. activ. pulses: 8
#   running time: 458

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99581   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9955376   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9951482   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9917921   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.984079   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.7441038   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7440329   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7439861   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.7439804   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.7438763   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.7437849   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.7437843   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.74375975   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.7437339   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.7437211   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.743706   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   18   0.74370515   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.74370253   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.7436888   REFERENCES
