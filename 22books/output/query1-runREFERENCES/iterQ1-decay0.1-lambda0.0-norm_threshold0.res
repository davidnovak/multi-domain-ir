###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2790.034, activation diff: 2823.764, ratio: 1.012
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1637.620, activation diff: 4427.654, ratio: 2.704
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 5368.050, activation diff: 7005.670, ratio: 1.305
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 2119.527, activation diff: 7487.578, ratio: 3.533
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 5428.801, activation diff: 7548.328, ratio: 1.390
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 2132.798, activation diff: 7561.599, ratio: 3.545
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 5432.750, activation diff: 7565.548, ratio: 1.393
#   pulse 9: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.336, activation diff: 7567.086, ratio: 3.545
#   pulse 10: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.269, activation diff: 7567.605, ratio: 1.393
#   pulse 11: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.660, activation diff: 7567.929, ratio: 3.545
#   pulse 12: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.365, activation diff: 7568.025, ratio: 1.393
#   pulse 13: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.746, activation diff: 7568.111, ratio: 3.545
#   pulse 14: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.387, activation diff: 7568.133, ratio: 1.393
#   pulse 15: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.770, activation diff: 7568.157, ratio: 3.545
#   pulse 16: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.393, activation diff: 7568.163, ratio: 1.393
#   pulse 17: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.777, activation diff: 7568.170, ratio: 3.545
#   pulse 18: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.395, activation diff: 7568.171, ratio: 1.393
#   pulse 19: activated nodes: 11341, borderline nodes: 0, overall activation: 2134.779, activation diff: 7568.174, ratio: 3.545
#   pulse 20: activated nodes: 11341, borderline nodes: 0, overall activation: 5433.395, activation diff: 7568.174, ratio: 1.393

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5433.4
#   number of spread. activ. pulses: 20
#   running time: 1707

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996656   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9981779   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950548   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES
