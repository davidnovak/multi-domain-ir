###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1496.731, activation diff: 1492.526, ratio: 0.997
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 2052.672, activation diff: 759.485, ratio: 0.370
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3820.716, activation diff: 1768.044, ratio: 0.463
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 4611.572, activation diff: 790.856, ratio: 0.171
#   pulse 6: activated nodes: 11305, borderline nodes: 469, overall activation: 4897.863, activation diff: 286.291, ratio: 0.058
#   pulse 7: activated nodes: 11305, borderline nodes: 469, overall activation: 4995.733, activation diff: 97.870, ratio: 0.020

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 4995.7
#   number of spread. activ. pulses: 7
#   running time: 423

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99981356   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99973917   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.99966073   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99958247   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99756914   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.993729   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.74980235   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.7497848   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.7497705   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.74976104   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.74973154   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.7497314   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.7497307   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   14   0.7497181   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   15   0.749714   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   16   0.7497115   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   17   0.7497067   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.7497065   REFERENCES
1   Q1   politicalsketche00retsrich_96   19   0.7497065   REFERENCES
1   Q1   essentialsinmod01howegoog_474   20   0.74970263   REFERENCES
