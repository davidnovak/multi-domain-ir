###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 107.375, activation diff: 96.757, ratio: 0.901
#   pulse 3: activated nodes: 7815, borderline nodes: 5098, overall activation: 322.026, activation diff: 214.733, ratio: 0.667
#   pulse 4: activated nodes: 9893, borderline nodes: 5850, overall activation: 952.130, activation diff: 630.104, ratio: 0.662
#   pulse 5: activated nodes: 10902, borderline nodes: 3940, overall activation: 1776.184, activation diff: 824.054, ratio: 0.464
#   pulse 6: activated nodes: 11158, borderline nodes: 2494, overall activation: 2623.418, activation diff: 847.234, ratio: 0.323
#   pulse 7: activated nodes: 11217, borderline nodes: 1427, overall activation: 3403.030, activation diff: 779.612, ratio: 0.229
#   pulse 8: activated nodes: 11240, borderline nodes: 810, overall activation: 4082.650, activation diff: 679.620, ratio: 0.166
#   pulse 9: activated nodes: 11289, borderline nodes: 583, overall activation: 4657.122, activation diff: 574.472, ratio: 0.123
#   pulse 10: activated nodes: 11307, borderline nodes: 444, overall activation: 5133.655, activation diff: 476.533, ratio: 0.093
#   pulse 11: activated nodes: 11312, borderline nodes: 349, overall activation: 5524.078, activation diff: 390.423, ratio: 0.071
#   pulse 12: activated nodes: 11313, borderline nodes: 284, overall activation: 5841.260, activation diff: 317.182, ratio: 0.054
#   pulse 13: activated nodes: 11315, borderline nodes: 232, overall activation: 6097.405, activation diff: 256.145, ratio: 0.042

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11315
#   final overall activation: 6097.4
#   number of spread. activ. pulses: 13
#   running time: 531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111164   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9870445   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.98478276   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.983555   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.97531515   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.955444   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8693795   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.86856496   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.86768246   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.8674878   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   11   0.8664342   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.8664061   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   13   0.86638486   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   14   0.8659368   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   15   0.86593103   REFERENCES
1   Q1   europesincenapol00leveuoft_17   16   0.8659259   REFERENCES
1   Q1   essentialsinmod01howegoog_474   17   0.86588037   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   18   0.86577284   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   19   0.8656461   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.86560273   REFERENCES
