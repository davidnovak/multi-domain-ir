###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 919.723, activation diff: 899.859, ratio: 0.978
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1960.906, activation diff: 1041.182, ratio: 0.531
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3897.968, activation diff: 1937.063, ratio: 0.497
#   pulse 5: activated nodes: 11305, borderline nodes: 469, overall activation: 5340.960, activation diff: 1442.992, ratio: 0.270
#   pulse 6: activated nodes: 11335, borderline nodes: 30, overall activation: 6258.908, activation diff: 917.948, ratio: 0.147
#   pulse 7: activated nodes: 11341, borderline nodes: 6, overall activation: 6810.114, activation diff: 551.205, ratio: 0.081
#   pulse 8: activated nodes: 11341, borderline nodes: 0, overall activation: 7132.391, activation diff: 322.278, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 7132.4
#   number of spread. activ. pulses: 8
#   running time: 429

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.99581015   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9955376   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99515   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9917967   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9840888   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.8929245   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.89283943   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.89278346   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.89277655   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.89265174   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   12   0.8925421   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   13   0.89254194   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.8925117   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.89248073   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   16   0.8924656   REFERENCES
1   Q1   europesincenapol00leveuoft_17   17   0.8924476   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   18   0.89244735   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   19   0.89244616   REFERENCES
1   Q1   ouroldworldbackg00bearrich_446   20   0.8924298   REFERENCES
