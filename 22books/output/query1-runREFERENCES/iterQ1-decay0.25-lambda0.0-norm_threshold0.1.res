###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2146.020, activation diff: 2177.519, ratio: 1.015
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 891.877, activation diff: 3037.897, ratio: 3.406
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 3774.477, activation diff: 4666.354, ratio: 1.236
#   pulse 5: activated nodes: 11301, borderline nodes: 514, overall activation: 1144.552, activation diff: 4919.029, ratio: 4.298
#   pulse 6: activated nodes: 11301, borderline nodes: 514, overall activation: 3800.417, activation diff: 4944.970, ratio: 1.301
#   pulse 7: activated nodes: 11305, borderline nodes: 471, overall activation: 1150.148, activation diff: 4950.565, ratio: 4.304
#   pulse 8: activated nodes: 11305, borderline nodes: 471, overall activation: 3801.869, activation diff: 4952.017, ratio: 1.303
#   pulse 9: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.470, activation diff: 4952.339, ratio: 4.305
#   pulse 10: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.975, activation diff: 4952.445, ratio: 1.303
#   pulse 11: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.501, activation diff: 4952.475, ratio: 4.305
#   pulse 12: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.986, activation diff: 4952.487, ratio: 1.303
#   pulse 13: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.505, activation diff: 4952.491, ratio: 4.305
#   pulse 14: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.988, activation diff: 4952.493, ratio: 1.303
#   pulse 15: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.505, activation diff: 4952.493, ratio: 4.305
#   pulse 16: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.989, activation diff: 4952.494, ratio: 1.303
#   pulse 17: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.505, activation diff: 4952.494, ratio: 4.305
#   pulse 18: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.989, activation diff: 4952.494, ratio: 1.303
#   pulse 19: activated nodes: 11305, borderline nodes: 469, overall activation: 1150.505, activation diff: 4952.494, ratio: 4.305
#   pulse 20: activated nodes: 11305, borderline nodes: 469, overall activation: 3801.989, activation diff: 4952.494, ratio: 1.303

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11305
#   final overall activation: 3802.0
#   number of spread. activ. pulses: 20
#   running time: 767

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999905   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999629   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES
1   Q1   essentialsinmod01howegoog_470   7   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_471   8   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_474   9   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_800   10   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_816   11   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_460   12   0.75   REFERENCES
1   Q1   essentialsinmod01howegoog_472   13   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_579   14   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_578   15   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_576   16   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_541   17   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_540   18   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_546   19   0.75   REFERENCES
1   Q1   encyclopediaame28unkngoog_545   20   0.75   REFERENCES
