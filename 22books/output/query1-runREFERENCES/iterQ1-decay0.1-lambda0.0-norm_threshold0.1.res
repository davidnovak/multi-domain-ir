###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0.1
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 31.499, activation diff: 37.499, ratio: 1.190
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2574.037, activation diff: 2605.536, ratio: 1.012
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 1502.739, activation diff: 4076.776, ratio: 2.713
#   pulse 4: activated nodes: 10836, borderline nodes: 2172, overall activation: 5290.926, activation diff: 6793.666, ratio: 1.284
#   pulse 5: activated nodes: 11301, borderline nodes: 505, overall activation: 2041.845, activation diff: 7332.771, ratio: 3.591
#   pulse 6: activated nodes: 11329, borderline nodes: 70, overall activation: 5360.568, activation diff: 7402.412, ratio: 1.381
#   pulse 7: activated nodes: 11338, borderline nodes: 17, overall activation: 2057.608, activation diff: 7418.176, ratio: 3.605
#   pulse 8: activated nodes: 11340, borderline nodes: 14, overall activation: 5365.546, activation diff: 7423.154, ratio: 1.383
#   pulse 9: activated nodes: 11341, borderline nodes: 14, overall activation: 2059.435, activation diff: 7424.981, ratio: 3.605
#   pulse 10: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.285, activation diff: 7425.720, ratio: 1.384
#   pulse 11: activated nodes: 11341, borderline nodes: 10, overall activation: 2059.860, activation diff: 7426.146, ratio: 3.605
#   pulse 12: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.442, activation diff: 7426.303, ratio: 1.384
#   pulse 13: activated nodes: 11341, borderline nodes: 10, overall activation: 2060.003, activation diff: 7426.445, ratio: 3.605
#   pulse 14: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.488, activation diff: 7426.491, ratio: 1.384
#   pulse 15: activated nodes: 11341, borderline nodes: 10, overall activation: 2060.059, activation diff: 7426.547, ratio: 3.605
#   pulse 16: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.504, activation diff: 7426.564, ratio: 1.384
#   pulse 17: activated nodes: 11341, borderline nodes: 10, overall activation: 2060.083, activation diff: 7426.588, ratio: 3.605
#   pulse 18: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.511, activation diff: 7426.594, ratio: 1.384
#   pulse 19: activated nodes: 11341, borderline nodes: 10, overall activation: 2060.094, activation diff: 7426.604, ratio: 3.605
#   pulse 20: activated nodes: 11341, borderline nodes: 10, overall activation: 5366.513, activation diff: 7426.607, ratio: 1.384

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11341
#   final overall activation: 5366.5
#   number of spread. activ. pulses: 20
#   running time: 793

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.99999934   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999991   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996305   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99798644   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9945361   REFERENCES
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES
