###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 1300.488, activation diff: 1329.653, ratio: 1.022
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 61.939, activation diff: 1362.427, ratio: 21.996
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.601, activation diff: 1899.540, ratio: 1.034
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.162, activation diff: 1899.763, ratio: 30.562
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.772, activation diff: 1899.934, ratio: 1.034
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.943, ratio: 30.560
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.946, ratio: 1.034
#   pulse 9: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.946, ratio: 30.560
#   pulse 10: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034
#   pulse 11: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.947, ratio: 30.560
#   pulse 12: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034
#   pulse 13: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.947, ratio: 30.560
#   pulse 14: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034
#   pulse 15: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.947, ratio: 30.560
#   pulse 16: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034
#   pulse 17: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.947, ratio: 30.560
#   pulse 18: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034
#   pulse 19: activated nodes: 8664, borderline nodes: 3409, overall activation: 62.171, activation diff: 1899.947, ratio: 30.560
#   pulse 20: activated nodes: 8664, borderline nodes: 3409, overall activation: 1837.775, activation diff: 1899.947, ratio: 1.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1837.8
#   number of spread. activ. pulses: 20
#   running time: 582

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.99999887   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995786   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.99777263   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939626   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.5   REFERENCES
1   Q1   europesincenapol00leveuoft_16   8   0.49999994   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   9   0.49999985   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   10   0.49999985   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   11   0.4999996   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   12   0.4999996   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   13   0.499999   REFERENCES
1   Q1   essentialsinmod01howegoog_474   14   0.49999723   REFERENCES
1   Q1   europesincenapol00leveuoft_17   15   0.4999925   REFERENCES
1   Q1   bostoncollegebul0405bost_26   16   0.4999925   REFERENCES
1   Q1   europesincenapol00leveuoft_353   17   0.4999925   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.4999925   REFERENCES
1   Q1   essentialsinmod01howegoog_461   19   0.4999925   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   20   0.4999925   REFERENCES
