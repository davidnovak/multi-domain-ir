###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 19.865, activation diff: 19.865, ratio: 1.000
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 520.903, activation diff: 501.038, ratio: 0.962
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 843.908, activation diff: 323.005, ratio: 0.383
#   pulse 4: activated nodes: 8664, borderline nodes: 3409, overall activation: 1270.365, activation diff: 426.457, ratio: 0.336
#   pulse 5: activated nodes: 8664, borderline nodes: 3409, overall activation: 1570.953, activation diff: 300.588, ratio: 0.191
#   pulse 6: activated nodes: 8664, borderline nodes: 3409, overall activation: 1757.413, activation diff: 186.460, ratio: 0.106
#   pulse 7: activated nodes: 8664, borderline nodes: 3409, overall activation: 1867.176, activation diff: 109.763, ratio: 0.059
#   pulse 8: activated nodes: 8664, borderline nodes: 3409, overall activation: 1929.972, activation diff: 62.796, ratio: 0.033

###################################
# spreading activation process summary: 
#   final number of activated nodes: 8664
#   final overall activation: 1930.0
#   number of spread. activ. pulses: 8
#   running time: 414

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9960887   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   2   0.9958095   REFERENCES
1   Q1   cambridgemodern09protgoog_449   3   0.9955375   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99514127   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9917649   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9840058   REFERENCES
1   Q1   englishcyclopae05kniggoog_147   7   0.49606913   REFERENCES
1   Q1   encyclopediaame28unkngoog_327   8   0.4960205   REFERENCES
1   Q1   encyclopediaame28unkngoog_325   9   0.49598998   REFERENCES
1   Q1   europesincenapol00leveuoft_16   10   0.49598646   REFERENCES
1   Q1   englishcyclopae05kniggoog_88   11   0.49591655   REFERENCES
1   Q1   encyclopediaame28unkngoog_323   12   0.49583954   REFERENCES
1   Q1   encyclopediaame28unkngoog_318   13   0.49583954   REFERENCES
1   Q1   encyclopediaame28unkngoog_367   14   0.4958351   REFERENCES
1   Q1   essentialsinmod01howegoog_474   15   0.49581367   REFERENCES
1   Q1   englishcyclopae05kniggoog_118   16   0.49580082   REFERENCES
1   Q1   shorthistoryofmo00haslrich_23   17   0.4957962   REFERENCES
1   Q1   encyclopediaame28unkngoog_326   18   0.4957958   REFERENCES
1   Q1   europesincenapol00leveuoft_17   19   0.4957857   REFERENCES
1   Q1   encyclopediaame28unkngoog_319   20   0.49576718   REFERENCES
