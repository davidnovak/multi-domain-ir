###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 5255, borderline nodes: 5186, overall activation: 2077.230, activation diff: 2106.395, ratio: 1.014
#   pulse 3: activated nodes: 8664, borderline nodes: 3409, overall activation: 983.843, activation diff: 3061.073, ratio: 3.111
#   pulse 4: activated nodes: 10798, borderline nodes: 2462, overall activation: 4190.385, activation diff: 5174.228, ratio: 1.235
#   pulse 5: activated nodes: 11261, borderline nodes: 827, overall activation: 1362.312, activation diff: 5552.696, ratio: 4.076
#   pulse 6: activated nodes: 11316, borderline nodes: 246, overall activation: 4236.495, activation diff: 5598.807, ratio: 1.322
#   pulse 7: activated nodes: 11320, borderline nodes: 197, overall activation: 1372.111, activation diff: 5608.606, ratio: 4.088
#   pulse 8: activated nodes: 11321, borderline nodes: 181, overall activation: 4238.925, activation diff: 5611.036, ratio: 1.324
#   pulse 9: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.798, activation diff: 5611.724, ratio: 4.088
#   pulse 10: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.159, activation diff: 5611.957, ratio: 1.324
#   pulse 11: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.883, activation diff: 5612.042, ratio: 4.088
#   pulse 12: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.206, activation diff: 5612.089, ratio: 1.324
#   pulse 13: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.899, activation diff: 5612.105, ratio: 4.088
#   pulse 14: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.220, activation diff: 5612.119, ratio: 1.324
#   pulse 15: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.904, activation diff: 5612.123, ratio: 4.088
#   pulse 16: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.225, activation diff: 5612.128, ratio: 1.324
#   pulse 17: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.905, activation diff: 5612.130, ratio: 4.088
#   pulse 18: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.227, activation diff: 5612.132, ratio: 1.324
#   pulse 19: activated nodes: 11322, borderline nodes: 172, overall activation: 1372.906, activation diff: 5612.132, ratio: 4.088
#   pulse 20: activated nodes: 11322, borderline nodes: 172, overall activation: 4239.228, activation diff: 5612.133, ratio: 1.324

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11322
#   final overall activation: 4239.2
#   number of spread. activ. pulses: 20
#   running time: 610

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES
1   Q1   selwynhouseschoo1940selw_21   3   0.9999989   REFERENCES
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995905   REFERENCES
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES
1   Q1   essentialsinmod01howegoog_474   7   0.8   REFERENCES
1   Q1   historyofuniteds07good_349   8   0.8   REFERENCES
1   Q1   historyofuniteds07good_345   9   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_465   10   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_472   11   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_473   12   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_470   13   0.8   REFERENCES
1   Q1   essentialsinmod01howegoog_471   14   0.8   REFERENCES
1   Q1   historyofuniteds07good_17   15   0.8   REFERENCES
1   Q1   historyofuniteds07good_71   16   0.8   REFERENCES
1   Q1   historyofuniteds07good_75   17   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_800   18   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_807   19   0.8   REFERENCES
1   Q1   encyclopediaame28unkngoog_805   20   0.8   REFERENCES
