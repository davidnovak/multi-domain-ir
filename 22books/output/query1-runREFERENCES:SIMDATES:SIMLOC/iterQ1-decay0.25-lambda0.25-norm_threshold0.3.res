###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1032.354, activation diff: 1034.187, ratio: 1.002
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 1292.621, activation diff: 793.223, ratio: 0.614
#   pulse 4: activated nodes: 11322, borderline nodes: 4980, overall activation: 4122.407, activation diff: 2838.320, ratio: 0.689
#   pulse 5: activated nodes: 11413, borderline nodes: 747, overall activation: 5456.413, activation diff: 1334.006, ratio: 0.244
#   pulse 6: activated nodes: 11439, borderline nodes: 247, overall activation: 5955.384, activation diff: 498.971, ratio: 0.084
#   pulse 7: activated nodes: 11452, borderline nodes: 131, overall activation: 6129.814, activation diff: 174.431, ratio: 0.028

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11452
#   final overall activation: 6129.8
#   number of spread. activ. pulses: 7
#   running time: 1377

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.999527   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9992944   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9990801   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9964075   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99122125   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7497369   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7496574   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.7495413   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   10   0.7495361   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   11   0.74952835   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.7494843   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.74947673   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.74947435   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.7494731   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7494525   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7494259   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   18   0.7494216   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.74941534   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   20   0.74940133   REFERENCES:SIMDATES:SIMLOC
