###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1321.398, activation diff: 1346.519, ratio: 1.019
#   pulse 3: activated nodes: 9517, borderline nodes: 3858, overall activation: 487.371, activation diff: 1722.907, ratio: 3.535
#   pulse 4: activated nodes: 10867, borderline nodes: 3919, overall activation: 2956.756, activation diff: 2693.161, ratio: 0.911
#   pulse 5: activated nodes: 10966, borderline nodes: 3552, overall activation: 3132.657, activation diff: 398.177, ratio: 0.127
#   pulse 6: activated nodes: 10967, borderline nodes: 3528, overall activation: 3302.899, activation diff: 170.822, ratio: 0.052
#   pulse 7: activated nodes: 10968, borderline nodes: 3523, overall activation: 3316.713, activation diff: 13.892, ratio: 0.004

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10968
#   final overall activation: 3316.7
#   number of spread. activ. pulses: 7
#   running time: 1292

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995923   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99777454   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   8   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   9   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   10   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   11   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_516   12   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   13   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   14   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   15   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   17   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   18   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_873   19   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   20   0.5   REFERENCES:SIMDATES:SIMLOC
