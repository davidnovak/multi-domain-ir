###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 107.585, activation diff: 96.968, ratio: 0.901
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 328.903, activation diff: 221.392, ratio: 0.673
#   pulse 4: activated nodes: 10942, borderline nodes: 6850, overall activation: 1003.197, activation diff: 674.295, ratio: 0.672
#   pulse 5: activated nodes: 11256, borderline nodes: 3591, overall activation: 1981.232, activation diff: 978.035, ratio: 0.494
#   pulse 6: activated nodes: 11393, borderline nodes: 1501, overall activation: 3075.712, activation diff: 1094.480, ratio: 0.356
#   pulse 7: activated nodes: 11425, borderline nodes: 692, overall activation: 4100.324, activation diff: 1024.612, ratio: 0.250
#   pulse 8: activated nodes: 11438, borderline nodes: 315, overall activation: 4978.435, activation diff: 878.111, ratio: 0.176
#   pulse 9: activated nodes: 11447, borderline nodes: 169, overall activation: 5703.195, activation diff: 724.760, ratio: 0.127
#   pulse 10: activated nodes: 11452, borderline nodes: 112, overall activation: 6290.091, activation diff: 586.897, ratio: 0.093
#   pulse 11: activated nodes: 11454, borderline nodes: 76, overall activation: 6760.122, activation diff: 470.030, ratio: 0.070
#   pulse 12: activated nodes: 11455, borderline nodes: 60, overall activation: 7133.840, activation diff: 373.718, ratio: 0.052
#   pulse 13: activated nodes: 11455, borderline nodes: 56, overall activation: 7429.501, activation diff: 295.661, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 7429.5
#   number of spread. activ. pulses: 13
#   running time: 1575

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911156   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98716056   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9849203   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98381245   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9753359   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95638853   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.86943126   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8685819   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.86771643   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8675165   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.8664688   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.86646426   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   13   0.86646295   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.8660244   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   15   0.8659727   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   16   0.8659673   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.86592126   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8658968   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8656837   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.8656611   REFERENCES:SIMDATES:SIMLOC
