###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 184.016, activation diff: 171.951, ratio: 0.934
#   pulse 3: activated nodes: 9341, borderline nodes: 3904, overall activation: 505.606, activation diff: 321.615, ratio: 0.636
#   pulse 4: activated nodes: 11279, borderline nodes: 4186, overall activation: 1261.422, activation diff: 755.816, ratio: 0.599
#   pulse 5: activated nodes: 11405, borderline nodes: 1084, overall activation: 2208.212, activation diff: 946.790, ratio: 0.429
#   pulse 6: activated nodes: 11433, borderline nodes: 427, overall activation: 3134.965, activation diff: 926.753, ratio: 0.296
#   pulse 7: activated nodes: 11450, borderline nodes: 209, overall activation: 3941.390, activation diff: 806.424, ratio: 0.205
#   pulse 8: activated nodes: 11454, borderline nodes: 90, overall activation: 4609.225, activation diff: 667.835, ratio: 0.145
#   pulse 9: activated nodes: 11455, borderline nodes: 65, overall activation: 5149.451, activation diff: 540.226, ratio: 0.105
#   pulse 10: activated nodes: 11456, borderline nodes: 55, overall activation: 5580.870, activation diff: 431.419, ratio: 0.077
#   pulse 11: activated nodes: 11457, borderline nodes: 38, overall activation: 5922.693, activation diff: 341.823, ratio: 0.058
#   pulse 12: activated nodes: 11458, borderline nodes: 35, overall activation: 6192.108, activation diff: 269.415, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 6192.1
#   number of spread. activ. pulses: 12
#   running time: 1537

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9853007   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98368734   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9818584   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9728819   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9562513   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7648433   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76393396   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7634272   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.76340264   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.76263005   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.76208097   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7620643   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7620109   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.76181954   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.7617813   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.76169914   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7616875   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7615253   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.76151955   REFERENCES:SIMDATES:SIMLOC
