###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.932, activation diff: 9.932, ratio: 0.768
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 162.659, activation diff: 149.956, ratio: 0.922
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 393.034, activation diff: 230.381, ratio: 0.586
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 774.138, activation diff: 381.104, ratio: 0.492
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 1200.692, activation diff: 426.553, ratio: 0.355
#   pulse 6: activated nodes: 10971, borderline nodes: 3512, overall activation: 1609.215, activation diff: 408.524, ratio: 0.254
#   pulse 7: activated nodes: 10971, borderline nodes: 3512, overall activation: 1966.548, activation diff: 357.333, ratio: 0.182
#   pulse 8: activated nodes: 10971, borderline nodes: 3512, overall activation: 2263.627, activation diff: 297.079, ratio: 0.131
#   pulse 9: activated nodes: 10971, borderline nodes: 3512, overall activation: 2504.234, activation diff: 240.607, ratio: 0.096
#   pulse 10: activated nodes: 10971, borderline nodes: 3512, overall activation: 2696.310, activation diff: 192.077, ratio: 0.071
#   pulse 11: activated nodes: 10971, borderline nodes: 3512, overall activation: 2848.294, activation diff: 151.984, ratio: 0.053
#   pulse 12: activated nodes: 10971, borderline nodes: 3512, overall activation: 2967.856, activation diff: 119.562, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 2967.9
#   number of spread. activ. pulses: 12
#   running time: 1700

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9890304   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98611104   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9849076   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98306596   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9746256   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95997643   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.4782372   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.47771108   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   9   0.47751388   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4774958   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.47712833   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.47676307   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.47669488   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.47669098   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   15   0.47668174   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.47663534   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.4765591   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.47646254   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.47642964   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.47639418   REFERENCES:SIMDATES:SIMLOC
