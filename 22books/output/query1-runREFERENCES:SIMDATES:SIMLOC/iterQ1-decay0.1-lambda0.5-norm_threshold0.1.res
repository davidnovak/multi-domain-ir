###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 802.024, activation diff: 783.641, ratio: 0.977
#   pulse 3: activated nodes: 9582, borderline nodes: 3878, overall activation: 1801.939, activation diff: 999.915, ratio: 0.555
#   pulse 4: activated nodes: 11356, borderline nodes: 3023, overall activation: 4295.240, activation diff: 2493.301, ratio: 0.580
#   pulse 5: activated nodes: 11436, borderline nodes: 311, overall activation: 6141.250, activation diff: 1846.009, ratio: 0.301
#   pulse 6: activated nodes: 11455, borderline nodes: 72, overall activation: 7264.859, activation diff: 1123.609, ratio: 0.155
#   pulse 7: activated nodes: 11459, borderline nodes: 31, overall activation: 7912.298, activation diff: 647.439, ratio: 0.082
#   pulse 8: activated nodes: 11460, borderline nodes: 15, overall activation: 8277.363, activation diff: 365.066, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 8277.4
#   number of spread. activ. pulses: 8
#   running time: 1340

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957318   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9953252   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9950031   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99113846   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9827293   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89291155   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8927995   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.892702   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8926778   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.89252186   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8924512   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.8924275   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.89240944   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.89235055   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.89234334   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.8923046   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.8922991   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.8922769   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.89227664   REFERENCES:SIMDATES:SIMLOC
