###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 127.922, activation diff: 116.549, ratio: 0.911
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 359.409, activation diff: 231.535, ratio: 0.644
#   pulse 4: activated nodes: 11127, borderline nodes: 6108, overall activation: 932.808, activation diff: 573.399, ratio: 0.615
#   pulse 5: activated nodes: 11317, borderline nodes: 2815, overall activation: 1693.664, activation diff: 760.857, ratio: 0.449
#   pulse 6: activated nodes: 11406, borderline nodes: 1091, overall activation: 2496.761, activation diff: 803.097, ratio: 0.322
#   pulse 7: activated nodes: 11423, borderline nodes: 538, overall activation: 3228.803, activation diff: 732.042, ratio: 0.227
#   pulse 8: activated nodes: 11434, borderline nodes: 323, overall activation: 3848.553, activation diff: 619.750, ratio: 0.161
#   pulse 9: activated nodes: 11441, borderline nodes: 210, overall activation: 4356.146, activation diff: 507.593, ratio: 0.117
#   pulse 10: activated nodes: 11449, borderline nodes: 165, overall activation: 4764.656, activation diff: 408.510, ratio: 0.086
#   pulse 11: activated nodes: 11449, borderline nodes: 141, overall activation: 5090.079, activation diff: 325.422, ratio: 0.064
#   pulse 12: activated nodes: 11449, borderline nodes: 119, overall activation: 5347.624, activation diff: 257.545, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11449
#   final overall activation: 5347.6
#   number of spread. activ. pulses: 12
#   running time: 1567

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885761   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98423964   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9820474   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98030007   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97063744   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95082736   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7166163   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7156992   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7150195   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.71491975   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7139949   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7136597   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.71365833   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.71337384   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.7132888   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.7132596   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7132057   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.7131101   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.71295965   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.71295846   REFERENCES:SIMDATES:SIMLOC
