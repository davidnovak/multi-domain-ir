###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 97.220, activation diff: 86.602, ratio: 0.891
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 289.341, activation diff: 192.196, ratio: 0.664
#   pulse 4: activated nodes: 10920, borderline nodes: 6878, overall activation: 833.557, activation diff: 544.216, ratio: 0.653
#   pulse 5: activated nodes: 11233, borderline nodes: 3953, overall activation: 1603.416, activation diff: 769.859, ratio: 0.480
#   pulse 6: activated nodes: 11350, borderline nodes: 1949, overall activation: 2475.283, activation diff: 871.867, ratio: 0.352
#   pulse 7: activated nodes: 11416, borderline nodes: 921, overall activation: 3306.140, activation diff: 830.857, ratio: 0.251
#   pulse 8: activated nodes: 11431, borderline nodes: 493, overall activation: 4025.927, activation diff: 719.787, ratio: 0.179
#   pulse 9: activated nodes: 11438, borderline nodes: 292, overall activation: 4622.924, activation diff: 596.997, ratio: 0.129
#   pulse 10: activated nodes: 11445, borderline nodes: 186, overall activation: 5107.563, activation diff: 484.639, ratio: 0.095
#   pulse 11: activated nodes: 11451, borderline nodes: 140, overall activation: 5496.147, activation diff: 388.584, ratio: 0.071
#   pulse 12: activated nodes: 11452, borderline nodes: 116, overall activation: 5805.351, activation diff: 309.204, ratio: 0.053
#   pulse 13: activated nodes: 11454, borderline nodes: 92, overall activation: 6050.130, activation diff: 244.779, ratio: 0.040

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6050.1
#   number of spread. activ. pulses: 13
#   running time: 1587

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111557   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.987151   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9848905   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9837836   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9752693   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9560876   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7728277   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.7720715   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7712988   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.77112156   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.77018   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7701786   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.7701784   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.76978534   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.76972646   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.76972616   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.76969147   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.76967466   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.76948696   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.76945436   REFERENCES:SIMDATES:SIMLOC
