###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 173.559, activation diff: 161.493, ratio: 0.930
#   pulse 3: activated nodes: 9341, borderline nodes: 3904, overall activation: 470.332, activation diff: 296.799, ratio: 0.631
#   pulse 4: activated nodes: 11265, borderline nodes: 4226, overall activation: 1140.521, activation diff: 670.189, ratio: 0.588
#   pulse 5: activated nodes: 11401, borderline nodes: 1157, overall activation: 1976.005, activation diff: 835.483, ratio: 0.423
#   pulse 6: activated nodes: 11428, borderline nodes: 466, overall activation: 2800.105, activation diff: 824.100, ratio: 0.294
#   pulse 7: activated nodes: 11443, borderline nodes: 256, overall activation: 3520.645, activation diff: 720.539, ratio: 0.205
#   pulse 8: activated nodes: 11449, borderline nodes: 146, overall activation: 4118.627, activation diff: 597.983, ratio: 0.145
#   pulse 9: activated nodes: 11450, borderline nodes: 100, overall activation: 4602.789, activation diff: 484.162, ratio: 0.105
#   pulse 10: activated nodes: 11450, borderline nodes: 84, overall activation: 4989.537, activation diff: 386.748, ratio: 0.078
#   pulse 11: activated nodes: 11451, borderline nodes: 80, overall activation: 5295.965, activation diff: 306.428, ratio: 0.058
#   pulse 12: activated nodes: 11451, borderline nodes: 68, overall activation: 5537.464, activation diff: 241.499, ratio: 0.044

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11451
#   final overall activation: 5537.5
#   number of spread. activ. pulses: 12
#   running time: 1531

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98529863   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9836839   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98184997   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9728545   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95614564   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7170404   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.71618724   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7157115   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7156887   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.71496296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.7144437   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.71443063   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.714379   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.71420187   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.714164   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.7140924   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.7140732   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.7139173   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.71391475   REFERENCES:SIMDATES:SIMLOC
