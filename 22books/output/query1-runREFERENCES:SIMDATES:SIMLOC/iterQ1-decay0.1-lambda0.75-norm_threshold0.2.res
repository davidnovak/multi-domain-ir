###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 150.402, activation diff: 139.029, ratio: 0.924
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 439.689, activation diff: 289.335, ratio: 0.658
#   pulse 4: activated nodes: 11145, borderline nodes: 6033, overall activation: 1242.465, activation diff: 802.776, ratio: 0.646
#   pulse 5: activated nodes: 11340, borderline nodes: 2167, overall activation: 2337.245, activation diff: 1094.780, ratio: 0.468
#   pulse 6: activated nodes: 11418, borderline nodes: 853, overall activation: 3466.099, activation diff: 1128.854, ratio: 0.326
#   pulse 7: activated nodes: 11440, borderline nodes: 320, overall activation: 4473.504, activation diff: 1007.405, ratio: 0.225
#   pulse 8: activated nodes: 11448, borderline nodes: 150, overall activation: 5318.134, activation diff: 844.631, ratio: 0.159
#   pulse 9: activated nodes: 11454, borderline nodes: 90, overall activation: 6006.710, activation diff: 688.576, ratio: 0.115
#   pulse 10: activated nodes: 11455, borderline nodes: 58, overall activation: 6559.709, activation diff: 552.999, ratio: 0.084
#   pulse 11: activated nodes: 11455, borderline nodes: 53, overall activation: 6999.766, activation diff: 440.057, ratio: 0.063
#   pulse 12: activated nodes: 11456, borderline nodes: 45, overall activation: 7347.867, activation diff: 348.100, ratio: 0.047

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11456
#   final overall activation: 7347.9
#   number of spread. activ. pulses: 12
#   running time: 1566

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885762   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9842496   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98207074   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9803361   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9707373   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95125514   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8599397   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8588412   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8580308   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8579103   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.85680723   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.8564193   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.85640943   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.85607445   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.8559694   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.85592854   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.85588276   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.8557819   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.85558707   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.8555685   REFERENCES:SIMDATES:SIMLOC
