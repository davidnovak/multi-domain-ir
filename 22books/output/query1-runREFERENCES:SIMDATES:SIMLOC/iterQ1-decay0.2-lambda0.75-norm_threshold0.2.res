###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.75
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.791, activation diff: 8.791, ratio: 0.746
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 135.416, activation diff: 124.042, ratio: 0.916
#   pulse 3: activated nodes: 8888, borderline nodes: 5364, overall activation: 385.779, activation diff: 250.412, ratio: 0.649
#   pulse 4: activated nodes: 11138, borderline nodes: 6088, overall activation: 1030.551, activation diff: 644.772, ratio: 0.626
#   pulse 5: activated nodes: 11321, borderline nodes: 2546, overall activation: 1896.873, activation diff: 866.322, ratio: 0.457
#   pulse 6: activated nodes: 11408, borderline nodes: 996, overall activation: 2805.038, activation diff: 908.165, ratio: 0.324
#   pulse 7: activated nodes: 11431, borderline nodes: 451, overall activation: 3626.555, activation diff: 821.517, ratio: 0.227
#   pulse 8: activated nodes: 11445, borderline nodes: 259, overall activation: 4319.672, activation diff: 693.117, ratio: 0.160
#   pulse 9: activated nodes: 11450, borderline nodes: 144, overall activation: 4886.457, activation diff: 566.784, ratio: 0.116
#   pulse 10: activated nodes: 11453, borderline nodes: 99, overall activation: 5342.371, activation diff: 455.914, ratio: 0.085
#   pulse 11: activated nodes: 11455, borderline nodes: 72, overall activation: 5705.536, activation diff: 363.166, ratio: 0.064
#   pulse 12: activated nodes: 11455, borderline nodes: 62, overall activation: 5992.995, activation diff: 287.459, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11455
#   final overall activation: 5993.0
#   number of spread. activ. pulses: 12
#   running time: 1439

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9885762   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9842434   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9820565   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98031366   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9706746   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9509844   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7643907   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.76341337   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.7626899   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.7625832   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.7615988   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.76124656   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.76124257   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.7609408   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.76084954   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.76081663   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.7607667   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   18   0.7606683   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.7605012   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.7604954   REFERENCES:SIMDATES:SIMLOC
