###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 21.549, activation diff: 24.549, ratio: 1.139
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 692.579, activation diff: 694.412, ratio: 1.003
#   pulse 3: activated nodes: 9162, borderline nodes: 4949, overall activation: 726.759, activation diff: 395.252, ratio: 0.544
#   pulse 4: activated nodes: 10736, borderline nodes: 5621, overall activation: 2120.144, activation diff: 1394.390, ratio: 0.658
#   pulse 5: activated nodes: 10933, borderline nodes: 3689, overall activation: 2833.719, activation diff: 713.575, ratio: 0.252
#   pulse 6: activated nodes: 10959, borderline nodes: 3561, overall activation: 3120.218, activation diff: 286.498, ratio: 0.092
#   pulse 7: activated nodes: 10964, borderline nodes: 3533, overall activation: 3223.919, activation diff: 103.702, ratio: 0.032

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10964
#   final overall activation: 3223.9
#   number of spread. activ. pulses: 7
#   running time: 1241

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99970335   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9995268   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99929386   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9990801   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9963948   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99118507   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49982354   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49976844   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   9   0.4996888   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.4996856   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.4996801   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   12   0.49965277   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.4996511   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.49964312   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.49964228   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.4996328   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.49961668   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   18   0.49960893   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   19   0.4996009   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_328   20   0.4996007   REFERENCES:SIMDATES:SIMLOC
