###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 29.165, activation diff: 35.165, ratio: 1.206
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1978.127, activation diff: 2003.248, ratio: 1.013
#   pulse 3: activated nodes: 9517, borderline nodes: 3858, overall activation: 1189.773, activation diff: 2999.185, ratio: 2.521
#   pulse 4: activated nodes: 11360, borderline nodes: 2764, overall activation: 5609.620, activation diff: 5023.039, ratio: 0.895
#   pulse 5: activated nodes: 11439, borderline nodes: 293, overall activation: 6130.749, activation diff: 719.215, ratio: 0.117
#   pulse 6: activated nodes: 11454, borderline nodes: 99, overall activation: 6324.598, activation diff: 195.273, ratio: 0.031

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6324.6
#   number of spread. activ. pulses: 6
#   running time: 1279

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999992   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.9999992   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995923   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9977749   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9939632   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_470   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_800   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_807   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_805   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_461   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   20   0.75   REFERENCES:SIMDATES:SIMLOC
