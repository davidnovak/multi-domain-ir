###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1630.419, activation diff: 1654.726, ratio: 1.015
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 1047.018, activation diff: 1779.129, ratio: 1.699
#   pulse 4: activated nodes: 10971, borderline nodes: 3512, overall activation: 3254.654, activation diff: 2251.176, ratio: 0.692
#   pulse 5: activated nodes: 10971, borderline nodes: 3512, overall activation: 3361.746, activation diff: 115.560, ratio: 0.034

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10971
#   final overall activation: 3361.7
#   number of spread. activ. pulses: 5
#   running time: 1251

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999905   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99996656   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9979679   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950462   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   7   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_492   8   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   9   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_580   10   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_267   11   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_440   12   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   14   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   15   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   17   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_83   18   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_353   19   0.5   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_488   20   0.5   REFERENCES:SIMDATES:SIMLOC
