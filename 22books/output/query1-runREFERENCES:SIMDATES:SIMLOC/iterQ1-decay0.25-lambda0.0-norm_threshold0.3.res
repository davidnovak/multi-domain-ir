###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.0
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.732, activation diff: 32.732, ratio: 1.224
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1744.724, activation diff: 1769.292, ratio: 1.014
#   pulse 3: activated nodes: 9398, borderline nodes: 3933, overall activation: 967.683, activation diff: 2669.453, ratio: 2.759
#   pulse 4: activated nodes: 11342, borderline nodes: 3359, overall activation: 5330.494, activation diff: 5115.829, ratio: 0.960
#   pulse 5: activated nodes: 11428, borderline nodes: 469, overall activation: 5750.078, activation diff: 1088.782, ratio: 0.189
#   pulse 6: activated nodes: 11447, borderline nodes: 164, overall activation: 6212.996, activation diff: 469.856, ratio: 0.076
#   pulse 7: activated nodes: 11454, borderline nodes: 97, overall activation: 6246.514, activation diff: 34.313, ratio: 0.005

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11454
#   final overall activation: 6246.5
#   number of spread. activ. pulses: 7
#   running time: 1327

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.99999917   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999917   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.99995494   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9975412   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99333036   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_472   10   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_473   11   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_470   12   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_471   13   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   14   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_75   15   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_800   16   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_807   17   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_805   18   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_816   19   0.75   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_462   20   0.75   REFERENCES:SIMDATES:SIMLOC
