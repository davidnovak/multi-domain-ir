###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 66.123, activation diff: 55.505, ratio: 0.839
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 178.155, activation diff: 112.107, ratio: 0.629
#   pulse 4: activated nodes: 10273, borderline nodes: 6500, overall activation: 425.856, activation diff: 247.701, ratio: 0.582
#   pulse 5: activated nodes: 10691, borderline nodes: 4843, overall activation: 752.274, activation diff: 326.418, ratio: 0.434
#   pulse 6: activated nodes: 10833, borderline nodes: 4324, overall activation: 1112.244, activation diff: 359.970, ratio: 0.324
#   pulse 7: activated nodes: 10911, borderline nodes: 3823, overall activation: 1469.381, activation diff: 357.137, ratio: 0.243
#   pulse 8: activated nodes: 10935, borderline nodes: 3659, overall activation: 1800.372, activation diff: 330.990, ratio: 0.184
#   pulse 9: activated nodes: 10957, borderline nodes: 3601, overall activation: 2089.543, activation diff: 289.172, ratio: 0.138
#   pulse 10: activated nodes: 10959, borderline nodes: 3566, overall activation: 2331.465, activation diff: 241.921, ratio: 0.104
#   pulse 11: activated nodes: 10959, borderline nodes: 3553, overall activation: 2528.797, activation diff: 197.333, ratio: 0.078
#   pulse 12: activated nodes: 10960, borderline nodes: 3545, overall activation: 2687.548, activation diff: 158.751, ratio: 0.059
#   pulse 13: activated nodes: 10964, borderline nodes: 3535, overall activation: 2814.192, activation diff: 126.644, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10964
#   final overall activation: 2814.2
#   number of spread. activ. pulses: 13
#   running time: 1538

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99111503   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98709923   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9847183   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98362935   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9749163   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95461905   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.48301572   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.482531   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4820472   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.48193884   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.48133674   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.4813104   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.48130506   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.48105824   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   15   0.48100027   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   16   0.48098734   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.4809695   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.4809569   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.4809073   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_319   20   0.48084244   REFERENCES:SIMDATES:SIMLOC
