###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1434.590, activation diff: 1433.479, ratio: 0.999
#   pulse 3: activated nodes: 9459, borderline nodes: 3915, overall activation: 2149.387, activation diff: 1172.482, ratio: 0.545
#   pulse 4: activated nodes: 11346, borderline nodes: 3160, overall activation: 6076.233, activation diff: 3929.107, ratio: 0.647
#   pulse 5: activated nodes: 11437, borderline nodes: 366, overall activation: 7738.206, activation diff: 1661.984, ratio: 0.215
#   pulse 6: activated nodes: 11454, borderline nodes: 74, overall activation: 8329.695, activation diff: 591.489, ratio: 0.071
#   pulse 7: activated nodes: 11458, borderline nodes: 34, overall activation: 8531.323, activation diff: 201.628, ratio: 0.024

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11458
#   final overall activation: 8531.3
#   number of spread. activ. pulses: 7
#   running time: 1375

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996677   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.99949676   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9994385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99687564   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99228823   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.89974856   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.8996851   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   9   0.8996087   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   10   0.89960146   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   11   0.89959955   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.8995763   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   13   0.8995668   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.89955926   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   15   0.8995409   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   16   0.8995386   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   17   0.8995336   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   18   0.89953154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.8995079   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   20   0.8995069   REFERENCES:SIMDATES:SIMLOC
