###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.0
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.0)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 33.730, activation diff: 39.730, ratio: 1.178
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 2926.212, activation diff: 2950.518, ratio: 1.008
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 3118.270, activation diff: 4046.007, ratio: 1.298
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 8402.286, activation diff: 5481.479, ratio: 0.652
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 8809.678, activation diff: 422.708, ratio: 0.048

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 8809.7
#   number of spread. activ. pulses: 5
#   running time: 2330

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   1.0   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   2   0.9999994   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   3   0.99999934   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9999666   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9981731   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9950547   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_349   7   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_345   8   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_347   9   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_845   10   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   11   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   historyofuniteds07good_348   12   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_930   13   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   bostoncollegebul0405bost_139   14   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_460   15   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_469   16   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_468   17   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_465   18   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_466   19   0.9   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_464   20   0.9   REFERENCES:SIMDATES:SIMLOC
