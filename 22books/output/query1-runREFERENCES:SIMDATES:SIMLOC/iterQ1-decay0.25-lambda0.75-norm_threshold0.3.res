###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.75
#   norm_threshold: 0.3
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.3, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 11.183, activation diff: 8.183, ratio: 0.732
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 92.037, activation diff: 81.419, ratio: 0.885
#   pulse 3: activated nodes: 8645, borderline nodes: 5928, overall activation: 270.005, activation diff: 178.043, ratio: 0.659
#   pulse 4: activated nodes: 10870, borderline nodes: 6855, overall activation: 755.524, activation diff: 485.519, ratio: 0.643
#   pulse 5: activated nodes: 11219, borderline nodes: 4142, overall activation: 1432.896, activation diff: 677.372, ratio: 0.473
#   pulse 6: activated nodes: 11346, borderline nodes: 2172, overall activation: 2199.641, activation diff: 766.746, ratio: 0.349
#   pulse 7: activated nodes: 11411, borderline nodes: 1165, overall activation: 2937.482, activation diff: 737.841, ratio: 0.251
#   pulse 8: activated nodes: 11423, borderline nodes: 559, overall activation: 3581.125, activation diff: 643.643, ratio: 0.180
#   pulse 9: activated nodes: 11429, borderline nodes: 387, overall activation: 4116.432, activation diff: 535.307, ratio: 0.130
#   pulse 10: activated nodes: 11436, borderline nodes: 250, overall activation: 4551.631, activation diff: 435.199, ratio: 0.096
#   pulse 11: activated nodes: 11438, borderline nodes: 207, overall activation: 4900.781, activation diff: 349.150, ratio: 0.071
#   pulse 12: activated nodes: 11442, borderline nodes: 173, overall activation: 5178.613, activation diff: 277.832, ratio: 0.054
#   pulse 13: activated nodes: 11443, borderline nodes: 161, overall activation: 5398.500, activation diff: 219.887, ratio: 0.041

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11443
#   final overall activation: 5398.5
#   number of spread. activ. pulses: 13
#   running time: 1498

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9911155   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9871452   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9848721   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98376626   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97522986   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.95591295   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.7245259   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.72381604   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.72309005   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.72292423   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.72203887   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   12   0.72203475   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   13   0.72203326   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   14   0.721665   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   15   0.7216033   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   16   0.7216016   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   17   0.7215762   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.7215623   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.7213894   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.72135127   REFERENCES:SIMDATES:SIMLOC
