###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.5
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.5)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 18.750, activation diff: 18.750, ratio: 1.000
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 455.066, activation diff: 436.683, ratio: 0.960
#   pulse 3: activated nodes: 9582, borderline nodes: 3878, overall activation: 874.564, activation diff: 419.497, ratio: 0.480
#   pulse 4: activated nodes: 10869, borderline nodes: 3966, overall activation: 1697.216, activation diff: 822.652, ratio: 0.485
#   pulse 5: activated nodes: 10959, borderline nodes: 3562, overall activation: 2360.469, activation diff: 663.253, ratio: 0.281
#   pulse 6: activated nodes: 10967, borderline nodes: 3524, overall activation: 2786.714, activation diff: 426.245, ratio: 0.153
#   pulse 7: activated nodes: 10969, borderline nodes: 3522, overall activation: 3036.594, activation diff: 249.880, ratio: 0.082
#   pulse 8: activated nodes: 10969, borderline nodes: 3520, overall activation: 3178.427, activation diff: 141.833, ratio: 0.045

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 3178.4
#   number of spread. activ. pulses: 8
#   running time: 1248

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99608326   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9957309   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99532515   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9949982   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9910975   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.98264134   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49606115   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.4959978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.49594507   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.49593094   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.49584296   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.49579978   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   13   0.49578494   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.4957816   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   15   0.49574405   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   16   0.4957312   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   17   0.4957217   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   18   0.4957143   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   19   0.49570575   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   20   0.49570325   REFERENCES:SIMDATES:SIMLOC
