###################################
# parameters: 
#   decay: 0.1
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.1, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 204.931, activation diff: 192.866, ratio: 0.941
#   pulse 3: activated nodes: 9341, borderline nodes: 3904, overall activation: 577.729, activation diff: 372.824, ratio: 0.645
#   pulse 4: activated nodes: 11295, borderline nodes: 4119, overall activation: 1521.155, activation diff: 943.426, ratio: 0.620
#   pulse 5: activated nodes: 11411, borderline nodes: 984, overall activation: 2703.688, activation diff: 1182.533, ratio: 0.437
#   pulse 6: activated nodes: 11440, borderline nodes: 331, overall activation: 3844.684, activation diff: 1140.996, ratio: 0.297
#   pulse 7: activated nodes: 11452, borderline nodes: 121, overall activation: 4828.942, activation diff: 984.258, ratio: 0.204
#   pulse 8: activated nodes: 11455, borderline nodes: 64, overall activation: 5640.641, activation diff: 811.698, ratio: 0.144
#   pulse 9: activated nodes: 11456, borderline nodes: 46, overall activation: 6295.877, activation diff: 655.236, ratio: 0.104
#   pulse 10: activated nodes: 11458, borderline nodes: 29, overall activation: 6818.509, activation diff: 522.632, ratio: 0.077
#   pulse 11: activated nodes: 11459, borderline nodes: 20, overall activation: 7232.240, activation diff: 413.731, ratio: 0.057
#   pulse 12: activated nodes: 11460, borderline nodes: 16, overall activation: 7558.108, activation diff: 325.868, ratio: 0.043

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11460
#   final overall activation: 7558.1
#   number of spread. activ. pulses: 12
#   running time: 1497

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.98885006   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98530406   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.9836926   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98187244   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9729281   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9564307   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.8604491   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.85942733   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.8588587   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.8588308   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.85796475   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.85735524   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.8573302   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.8572738   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.8570534   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.857015   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.8569125   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.8569119   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   19   0.8567393   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   20   0.85672987   REFERENCES:SIMDATES:SIMLOC
