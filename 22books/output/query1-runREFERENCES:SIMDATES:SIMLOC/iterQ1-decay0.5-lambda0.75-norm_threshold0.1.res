###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.75
#   norm_threshold: 0.1
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.75)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.1, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 12.375, activation diff: 9.375, ratio: 0.758
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 121.271, activation diff: 109.206, ratio: 0.901
#   pulse 3: activated nodes: 9341, borderline nodes: 3904, overall activation: 302.644, activation diff: 181.398, ratio: 0.599
#   pulse 4: activated nodes: 10796, borderline nodes: 4544, overall activation: 635.125, activation diff: 332.481, ratio: 0.523
#   pulse 5: activated nodes: 10911, borderline nodes: 3805, overall activation: 1031.811, activation diff: 396.685, ratio: 0.384
#   pulse 6: activated nodes: 10955, borderline nodes: 3601, overall activation: 1432.845, activation diff: 401.035, ratio: 0.280
#   pulse 7: activated nodes: 10959, borderline nodes: 3543, overall activation: 1800.320, activation diff: 367.475, ratio: 0.204
#   pulse 8: activated nodes: 10965, borderline nodes: 3527, overall activation: 2115.157, activation diff: 314.837, ratio: 0.149
#   pulse 9: activated nodes: 10968, borderline nodes: 3523, overall activation: 2374.538, activation diff: 259.381, ratio: 0.109
#   pulse 10: activated nodes: 10968, borderline nodes: 3522, overall activation: 2583.832, activation diff: 209.294, ratio: 0.081
#   pulse 11: activated nodes: 10969, borderline nodes: 3522, overall activation: 2750.709, activation diff: 166.877, ratio: 0.061
#   pulse 12: activated nodes: 10969, borderline nodes: 3520, overall activation: 2882.762, activation diff: 132.053, ratio: 0.046

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10969
#   final overall activation: 2882.8
#   number of spread. activ. pulses: 12
#   running time: 1406

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9888499   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.98528206   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.98365074   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.98178315   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.97264093   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.9553555   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.47802615   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.47744805   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.47713363   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   10   0.47711927   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   11   0.47662973   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   12   0.47625634   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   13   0.47625467   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   14   0.47621918   REFERENCES:SIMDATES:SIMLOC
1   Q1   essentialsinmod01howegoog_474   15   0.47610253   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   16   0.4760732   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   17   0.47605386   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   18   0.4759648   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   19   0.47589767   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_23   20   0.47589546   REFERENCES:SIMDATES:SIMLOC
