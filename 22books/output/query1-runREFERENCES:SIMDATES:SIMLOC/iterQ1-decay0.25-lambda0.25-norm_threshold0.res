###################################
# parameters: 
#   decay: 0.25
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.25, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1561.368, activation diff: 1552.566, ratio: 0.994
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2640.667, activation diff: 1127.746, ratio: 0.427
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5207.963, activation diff: 2567.296, ratio: 0.493
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6183.671, activation diff: 975.708, ratio: 0.158
#   pulse 6: activated nodes: 11464, borderline nodes: 21, overall activation: 6508.862, activation diff: 325.191, ratio: 0.050

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 6508.9
#   number of spread. activ. pulses: 6
#   running time: 1304

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.99925816   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.99913013   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99892366   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9988564   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.995931   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99106646   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.74926066   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.74920356   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.74915504   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   10   0.7491508   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   11   0.74913496   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   12   0.7491329   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.7491225   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7490996   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.74909765   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.749092   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   17   0.74908113   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.74907607   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.74904484   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7490347   REFERENCES:SIMDATES:SIMLOC
