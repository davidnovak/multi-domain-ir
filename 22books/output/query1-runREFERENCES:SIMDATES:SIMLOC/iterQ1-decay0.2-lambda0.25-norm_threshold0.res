###################################
# parameters: 
#   decay: 0.2
#   lambda: 0.25
#   norm_threshold: 0
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.2, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 26.797, activation diff: 29.797, ratio: 1.112
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 1664.038, activation diff: 1655.236, ratio: 0.995
#   pulse 3: activated nodes: 10971, borderline nodes: 3512, overall activation: 2886.717, activation diff: 1272.744, ratio: 0.441
#   pulse 4: activated nodes: 11443, borderline nodes: 472, overall activation: 5766.309, activation diff: 2879.593, ratio: 0.499
#   pulse 5: activated nodes: 11464, borderline nodes: 21, overall activation: 6855.093, activation diff: 1088.784, ratio: 0.159
#   pulse 6: activated nodes: 11464, borderline nodes: 0, overall activation: 7217.932, activation diff: 362.839, ratio: 0.050
#   pulse 7: activated nodes: 11464, borderline nodes: 0, overall activation: 7335.671, activation diff: 117.739, ratio: 0.016

###################################
# spreading activation process summary: 
#   final number of activated nodes: 11464
#   final overall activation: 7335.7
#   number of spread. activ. pulses: 7
#   running time: 1337

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9998145   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9997821   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   3   0.99973047   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   4   0.9996884   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.9975946   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99400324   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.799803   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.799788   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.799775   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   10   0.7997746   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.7997701   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   12   0.7997699   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   13   0.79976803   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   14   0.7997607   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   15   0.79976   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_118   16   0.7997592   REFERENCES:SIMDATES:SIMLOC
1   Q1   bookman44unkngoog_717   17   0.7997581   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   18   0.7997546   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_545   19   0.7997482   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   20   0.7997432   REFERENCES:SIMDATES:SIMLOC
