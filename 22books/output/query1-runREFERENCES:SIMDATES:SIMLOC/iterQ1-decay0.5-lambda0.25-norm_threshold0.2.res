###################################
# parameters: 
#   decay: 0.5
#   lambda: 0.25
#   norm_threshold: 0.2
#   relationships: REFERENCES:SIMDATES:SIMLOC

###################################
# configuration: 
#   sa.act_diff_ratio_limit = 0.05
#   sa.function = ciir.multi.sa.DecayedSumSAFunction(norm_function, 0.5, 0.25)
#   sa.id_property_name.DATE = dateID
#   sa.id_property_name.DOCUMENT = docID
#   sa.id_property_name.LOCATION = locID
#   sa.max_pulses = 20
#   sa.norm_function = ciir.multi.sa.SigmoidFunction(0.2, 1.0)
#   sa.relationships_to_use = REFERENCES:SIMDATES:SIMLOC
#   sa.weight_property.REFERENCES = relevance
#   sa.weight_property.SIMDATES = sim
#   sa.weight_property.SIMLOC = sim

###################################
# initial activations: 
#   generalhistoryfo00myerrich_787: 1.0
#   selwynhouseschoo1940selw_21: 1.0
#   lifeofstratfordc02laneuoft_185: 1.0
#   historyofuniteds07good_239: 1.0
#   shorthistoryofmo00haslrich_98: 1.0
#   cambridgemodern09protgoog_449: 1.0

###################################
# spreading activation process log: 
#   pulse 0: activated nodes: 6, borderline nodes: 6, overall activation: 0.000, activation diff: 0.000, ratio: Infinity
#   pulse 1: activated nodes: 69, borderline nodes: 63, overall activation: 23.374, activation diff: 26.374, ratio: 1.128
#   pulse 2: activated nodes: 7459, borderline nodes: 7390, overall activation: 803.893, activation diff: 802.783, ratio: 0.999
#   pulse 3: activated nodes: 9459, borderline nodes: 3915, overall activation: 939.069, activation diff: 404.021, ratio: 0.430
#   pulse 4: activated nodes: 10849, borderline nodes: 4137, overall activation: 2310.465, activation diff: 1371.638, ratio: 0.594
#   pulse 5: activated nodes: 10955, borderline nodes: 3600, overall activation: 2955.991, activation diff: 645.526, ratio: 0.218
#   pulse 6: activated nodes: 10963, borderline nodes: 3532, overall activation: 3194.696, activation diff: 238.705, ratio: 0.075
#   pulse 7: activated nodes: 10967, borderline nodes: 3524, overall activation: 3277.084, activation diff: 82.388, ratio: 0.025

###################################
# spreading activation process summary: 
#   final number of activated nodes: 10967
#   final overall activation: 3277.1
#   number of spread. activ. pulses: 7
#   running time: 1321

###################################
# top k results in TREC format: 

1   Q1   historyofuniteds07good_239   1   0.9997792   REFERENCES:SIMDATES:SIMLOC
1   Q1   selwynhouseschoo1940selw_21   2   0.9996676   REFERENCES:SIMDATES:SIMLOC
1   Q1   shorthistoryofmo00haslrich_98   3   0.9994965   REFERENCES:SIMDATES:SIMLOC
1   Q1   cambridgemodern09protgoog_449   4   0.9994385   REFERENCES:SIMDATES:SIMLOC
1   Q1   generalhistoryfo00myerrich_787   5   0.99686897   REFERENCES:SIMDATES:SIMLOC
1   Q1   lifeofstratfordc02laneuoft_185   6   0.99228144   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_147   7   0.49985877   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_327   8   0.49982154   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_325   9   0.4997784   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_323   10   0.49977094   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_96   11   0.49976906   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_367   12   0.49975705   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_17   13   0.4997475   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_318   14   0.49974063   REFERENCES:SIMDATES:SIMLOC
1   Q1   politicalsketche00retsrich_154   15   0.49974036   REFERENCES:SIMDATES:SIMLOC
1   Q1   europesincenapol00leveuoft_16   16   0.49973887   REFERENCES:SIMDATES:SIMLOC
1   Q1   ouroldworldbackg00bearrich_446   17   0.49972403   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_522   18   0.49972185   REFERENCES:SIMDATES:SIMLOC
1   Q1   encyclopediaame28unkngoog_326   19   0.49972183   REFERENCES:SIMDATES:SIMLOC
1   Q1   englishcyclopae05kniggoog_88   20   0.49971884   REFERENCES:SIMDATES:SIMLOC
