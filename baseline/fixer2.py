relevant = {1: set(), 2: set(), 3: set(), 4: set(), 5:set(),6:set(),7:set(),8:set(),9:set(),10:set()}
for line in open('/home/wem/multi-domain-ir/judgments/doc-binary.rel'):
    qid = int(line.split()[0])
    doc = line.split()[2]
    val = int(line.split()[3])
    if val == 1:
        relevant[qid].add(doc)

output = {1: [], 2: [], 3: [], 4: [], 5:[],6:[],7:[],8:[],9:[],10:[]}
counts = {1: 0, 2: 0, 3: 0, 4: 0, 5:0,6:0,7:0,8:0,9:0,10:0}
for line in open('queries_1-10-top100-ql.res'):
    qid = int(line.split()[0])
    doc = line.split()[2]
    counts[qid] += 1
    if doc in relevant[qid] and counts[qid] < 11:
        output[qid].append('{} 1\n'.format(doc,val))

for i in range(10):
    writer = open('query{:d}-rel-top10.txt'.format(i+1),'w')
    for line in output[i+1]:
        writer.write(line)
    writer.close()
