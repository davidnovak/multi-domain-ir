output = {1: [], 2: [], 3: [], 4: [], 5:[],6:[],7:[],8:[],9:[],10:[]}
for line in open('queries_1-10-top1000-ql.res'):
    qid = int(line.split()[0])
    output[qid].append(line)

for i in range(10):
    writer = open('1000/'+str(i+1),'w')
    for line in output[i+1]:
        writer.write(line)
    writer.close()
