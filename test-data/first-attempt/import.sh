#!/bin/bash

DBDIR=/home/dnovak/neo4j/data/graph.db/
echo "removing database directory $DBDIR in 5 seconds..."
sleep 5
rm -Rf $DBDIR

/home/dnovak/neo4j/bin/neo4j stop
/home/dnovak/neo4j/bin/neo4j-import --into $DBDIR --nodes:DOCUMENTS documents.csv --nodes:PLACES places.csv --relationships:REFERENCES doc-places.csv --relationships:SIMDOC doc-doc.csv --relationships:SIMPLACES place-place.csv --nodes:TIMES times.csv --relationships:REFERENCES doc-times.csv --relationships:SIMTIMES time-time.csv
/home/dnovak/neo4j/bin/neo4j start
